﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
//using i_Reader.Properties;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.IO.Compression;
using Ionic.Zip;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Security;
//using MySql.Data.MySqlClient;
using Program_Base;
using System.Globalization;
using EventBoard;
using DynaVisionGraph.Filter;

namespace Event_Base
{
    public enum DChartDrawTo
    {
        Display,
        Print_color,
        Print_BW
    }
    public enum DRecState
    {
        Unknown = 0,    // ! sequence may not change because it is used in the dBase!
        New,
        Recording,      // 2 recording has started
        Recorded,       // 3 recording had finished
        Received,       // 4 Received events to be triaged
        Processed,      // 5
        Noise,          // 6
        LeadOff,        // 7
        Excluded,         // 8   // old Normal
        BaseLine,       // 9
        Triaged,        // 10 triaged events to be analysed
        Analyzed,       // 11 analyzed events to be reported to client
        Reported,       // 12 
        QCd,            // 13
        NrRecStates     // keep as last enum value
    }
    public enum DImportConverter
    {
        Unknown = 0,    // ! sequence may not change because it is used in the dBase!
        IntriconSironaZip,      // Import of zip file: unpack encrypted zip => import file (HEA, DAT, .ANN)
        IntriconSironaMit,      // Import from directory selecting .HEA file using (DAT and ANN)
        TZAeraScp,               // Import from directory selecting scp
        DV2Scp,
        SironaHttp,         // import hea, dat, atr and json file from the Http Sirona service
        DVX1,               // import DVX using DvxEvtRec
        DvxSpO,             // import DVX SpO2 file
        NrConverters     // keep as last enum value
    };

    public enum DImportMethod
    {
        Unknown = 0,    // ! sequence may not change because it is used in the dBase!
        Manual,         // import by manualy selecting a file
        Automatic,       // import during autoRun

        NrMethods     // keep as last enum value
    }

    public enum DPriority
    {
        Unknown = 0, High, Normal, Low, NrPriorities
    }

    public enum DPLotAnnotations
    {
        Off = 0, BeatsOnly, All
    }

    public enum DMoveTimeZone
    {
        None,
        Move        // move read time zone to given (from dBase Record)
    }

    public enum DPlethSearch
    {
        Range,
        Peak,
        Vally,
        Rise
    };
    public enum DPlethState
    {
        Search,
        RiseLow,
        RiseMid,
        RiseHigh,
        DropMid
    }

    public class CSignalData
    {
        public UInt32 mArraySize = 0;
        public UInt32 mNrValues = 0;
        public Int32[] mValues = null;

        public Int32 mMinValue = Int32.MaxValue;
        public Int32 mMaxValue = Int32.MinValue;


        public bool mbCreateData(UInt32 AArraySize)
        {
            mArraySize = AArraySize;
            mNrValues = 0;
            mValues = mArraySize == 0 ? null : new Int32[AArraySize];
            return mValues != null;
        }
        public void mDispose()
        {
            mArraySize = 0;
            mNrValues = 0;
            mValues = null;
            mMinValue = Int32.MaxValue;
            mMaxValue = Int32.MinValue;
        }
        public void mAddValue(Int32 AValue)
        {
            if (mNrValues < mArraySize)
            {
                mValues[mNrValues++] = AValue;
                if (AValue < mMinValue) mMinValue = AValue;
                if (AValue > mMaxValue) mMaxValue = AValue;
            }
        }
        public bool mbCopyFrom(CSignalData AFrom)
        {
            mArraySize = AFrom.mArraySize;
            mNrValues = AFrom.mNrValues;
            mMinValue = AFrom.mMinValue;
            mMaxValue = AFrom.mMaxValue;
            mValues = mArraySize == 0 ? null : new Int32[mArraySize];
            if (mValues != null && AFrom.mValues != null)
            {
                for (int i = 0; i < mArraySize; ++i)
                {
                    mValues[i] = AFrom.mValues[i];
                }
            }
            return mValues != null;
        }
    }
    public class CRecordP1P2
    {
        // markers on chart
        static public float _sP1AmplitudemV = 0.0F;
        static float _sP1TimeSec = 0.0F;    // from start of strip
        static bool _sbP1Draw = false;
        static float _sP2AmplitudemV = 0.0F;
        static float _sP2TimeSec = 0.0F;    // from start of strip
        static bool _sbP2Draw = false;

        public CRecordP1P2()
        {

        }
        static public void sDisableP1P2()
        {
            _sbP1Draw = false;
            _sbP2Draw = false;
        }
        static public void sDisableP2()
        {
            _sbP2Draw = false;
        }
        static public void sSetStateP1(bool AbDrawState)
        {
            _sbP1Draw = AbDrawState;
        }
        static public void sSetStateP2(bool AbDrawState)
        {
            _sbP2Draw = AbDrawState;
        }
        static public bool sbGetStateP1()
        {
            return _sbP1Draw;
        }
        static public bool sbGetStateP2()
        {
            return _sbP2Draw;
        }

        static public void sSwapP1P2()
        {
            float a = _sP1AmplitudemV;
            float t = _sP1TimeSec;
            bool b = _sbP1Draw;

            _sP1AmplitudemV = _sP2AmplitudemV;
            _sP1TimeSec = _sP2TimeSec;    // from start of strip
            _sbP1Draw = _sbP2Draw;

            _sP2AmplitudemV = a;
            _sP2TimeSec = t;    // from start of strip
            _sbP2Draw = b;
        }

        static public void sSetP1(float ATimeSec, float AAmplitude)
        {
            _sP1AmplitudemV = AAmplitude;
            _sP1TimeSec = ATimeSec;    // from start of strip
            _sbP1Draw = true;
            _sbP2Draw = false;
        }
        static public void sSetP2(float ATimeSec, float AAmplitude)
        {
            _sP2AmplitudemV = AAmplitude;
            _sP2TimeSec = ATimeSec;    // from start of strip
            _sbP2Draw = true;
        }
        static public string sGetP1Label()
        {
            return _sbP1Draw ? "P1" : "p1";
        }
        static public string sGetP2Label()
        {
            return _sbP2Draw ? "P2" : "p2";
        }
        static public bool sbIsP1Set()
        {
            return _sbP1Draw;
        }
        static public bool sbIsP2Set()
        {
            return _sbP2Draw;
        }
        static public bool sbGetP1(out float ArTimeSec, out float ArAmplitude)
        {
            ArTimeSec = _sP1TimeSec;
            ArAmplitude = _sP1AmplitudemV;
            return _sbP1Draw;
        }
        static public bool sbGetP2(out float ArTimeSec, out float ArAmplitude)
        {
            ArTimeSec = _sP2TimeSec;
            ArAmplitude = _sP2AmplitudemV;
            return _sbP2Draw;
        }
    }

    public enum DRecordMitVars
    {
        // primary keys
        ReceivedUTC = 0,
        FileName,
        DeviceID,
        // variables
        TimeZoneOffsetMin,
        DeviceModel,
        Device_IX,
        PatientID_ES,
        PatientTotalName_ES,
        Patient_IX,
        PatientGender,
        PatientBirthDate_EI,
        PatientBirthYear,
        RecLabel,
        RecRemark_ES,
        PhysicianName_ES,
        //        Physician_IX,
        Study_IX,
        StartRecUTC,
        RecDurationSec,
        EventTypeString,
        EventType_IX,
        EventUTC,
        EventTimeSec,
        TransmitUTC,
        RecState_IX,
        ImportConverter,
        ImportMethod,
        NrSignals,
        SignalLabels,
        StoredAt,       // 29 ( 24 left)
                        //        NrSqlVars,       // last release beta 1.0.0.353
        DeviceMode,
        SeqNrInStudy,
        NrAnalysis,
        InvertChannelsMask,
        ExtraData,
        SignalUnits,
        // added 20190225: values copied from study at first assignment to study (only use in triage list)
        StudyPermissions,   // permissions bit mask (used for filtering records on site and operator skill grade
        Trial_IX,           // study belongs to trial
        Client_IX,          // study belongs to client

        NrSqlVars       // keep as last
    }

    public class CRecordMit : CSqlDataTableRow
    {
        public const UInt16 cStoredAtMaxLen = 64;
        public const UInt32 _cMaxLinesAmpl = 200;
        public const UInt32 _cMaxLinesTime = 500;
        public const UInt32 _cMinLineDistance = 3;

        public float mMaxBeatTimeSec = 10.0F;

        public bool mbReadUTC;  // UTC date time is read from file
        public bool mbReadDT;  // local date time is read from file
        public bool mbReadTZ;   // time zone is read from file


        // from header file
        public uint mSampleFrequency;
        private uint mNrSamplesPerSignal;      // currently discard value = 0
        public ushort mDataFormat;              // curently 
        public float mAdcGain;                  // number of units / mV
        public ushort mAdcBits;                 // number of bits in sample 
        private int mAdcZero;                   // curently discarded

        public float mSampleUnitT;             // 1 / mFrequency
        public float mSampleUnitA;            // 1 / mAdcGain
        public uint mNrSamples;                // nr sample values in the file
        //private int mSampleMin;                 // minimum value
        //private int mSampleMax;                 // maximum value

        public ushort mNrSignals;               // curently discards value = 1
        public CSignalData[] mSignals;
        public UInt16 mImportConverter;         // import converter enum
        public UInt16 mImportMethod;            // Import method enum
        //private uint mArraySize;
        //private int[] mSampleArray;             // data aray

        public CAnnotation mAnnotationAtr;     // annotation file

        public const float cReferenceAmplitude = 1.0F;         // hight ofreference pulse is 1 mV
        public const float cMinRangeAmplitude = 0.5F;
        public const float cReferenceTime = 0.4F;             // width of reference pulse is 0.2 sec

        // From Header in dBase 
        public DateTime mReceivedUTC;           // received time = read time
        public Int32 mTimeZoneOffsetMin;       // all values are in UTC, for local add TimeZoneOffset
        public string mFileName;
        public string mStoredAt;
        public DateTime mBaseUTC;                // start time of strip (functions are given in sec. from this point
        public DateTime mEventUTC;              // Event time
        public float mEventTimeSec;            // event time relative to base

        public string mSignalLabels;
        public string mSignalUnits;

        // JSON file    using public for easy acces without get set functions
        public string mDeviceID;
        public string mDeviceModel;
        public CEncryptedString mPatientID;
        public CEncryptedString mPatientTotalName;
        //public CEncryptedString mPatientMiddleInitials;
        //public CEncryptedString mPatientLastName;
        public CEncryptedInt mPatientBirthDate;
        public UInt16 mPatientBirthYear;
        public int mPatientGender;
        public CEncryptedString mPhysicianName;
        public string mRecLabel;
        public CEncryptedString mRecRemark;
        public string mEventTypeString;
        public DateTime mStudyStartRecUTC;
        //public DateTime mStartRecLocalDT; 
        public float mRecDurationSec;
        public DateTime mTransmitUTC;
        //public DateTime mTransmitLocalDT;

        // reference indexes for dBase
        public UInt16 mRecState;
        public UInt32 mDevice_IX;
        public UInt32 mPatient_IX;

        //        public int mPhysician_IX;
        public UInt32 mStudy_IX;
        public UInt16 mEventType_IX;

        public string mDeviceMode;
        public UInt16 mSeqNrInStudy;
        public UInt16 mNrAnalysis;
        public string mExtraData;
        public UInt32 mInvertChannelsMask;

        // added 20190225 values copied from study at first assignment to study (only use in triage list)
        public CBitSet64 _mStudyPermissions;   // permissions bit mask (used for filtering records on site and operator skill grade
        public UInt32 _mStudyTrial_IX;           // study belongs to trial
        public UInt32 _mStudyClient_IX;           // study belongs to client

        // 
        // debug
        bool mbDebugWriteDataCsv = false;
        bool mbDebugWriteAnnCsv = false;

        public static bool sbLimitAmplitudeRange = false;
        public static float sLimitAmplitudeRange = 8.0F;    // mV
        public UInt16 mTimeZoneMoved = 0;

        public static float sGetMaxAmplitudeRange()
        {
            float range = sbLimitAmplitudeRange && sLimitAmplitudeRange >= 1.0 ? sLimitAmplitudeRange : float.MaxValue;

            return range;
        }

        public CRecordMit(CSqlDBaseConnection ASqlConnection) : base(ASqlConnection, "d_record")
        {
            UInt64 maskPrimary = CSqlDataTableRow.sGetMaskRange((UInt16)DRecordMitVars.ReceivedUTC, (UInt16)DRecordMitVars.DeviceID);
            UInt64 maskValid = CSqlDataTableRow.sGetMaskRange((UInt16)0, (UInt16)(DRecordMitVars.NrSqlVars - 1));// mGetValidMask(false);
            mInitTableMasks(maskPrimary, maskValid);  // set primairyInsertFlags

            _mStudyPermissions = new CBitSet64();

            mClear();
        }

        ~CRecordMit()
        {
            mDisposeSignals();
        }

        public override CSqlDataTableRow mCreateNewRow()
        {
            return new CRecordMit(mGetSqlConnection());
        }
        public override bool mbCopyTo(CSqlDataTableRow ATo) // derived  public override void mbCopyTo() must call base.mbCopyTo(ATo)
        {
            bool bOk = false;

            if (ATo != null)
            {
                try
                {
                    CRecordMit to = ATo as CRecordMit;

                    if (to != null)
                    {
                        bOk = base.mbCopyTo(ATo);

                        to.mbReadUTC = mbReadUTC;  // UTC date time is read from file
                        to.mbReadDT = mbReadDT;  // local date time is read from file
                        to.mbReadTZ = mbReadTZ;   // time zone is read from file

                        to.mNrSignals = mNrSignals;
                        to.mSampleFrequency = mSampleFrequency;
                        to.mNrSamplesPerSignal = mNrSamplesPerSignal;
                        to.mDataFormat = mDataFormat;
                        to.mAdcGain = mAdcGain;
                        to.mAdcBits = mAdcBits;
                        to.mAdcZero = mAdcZero;

                        to.mSampleUnitT = mSampleUnitT;
                        to.mSampleUnitA = mSampleUnitA;
                        to.mNrSamples = mNrSamples;

                        to.mBaseUTC = mBaseUTC;

                        // From Header in dBase 
                        to.mFileName = mFileName;
                        to.mStoredAt = mStoredAt;
                        to.mReceivedUTC = mReceivedUTC;
                        to.mTimeZoneOffsetMin = mTimeZoneOffsetMin;
                        to.mEventUTC = mEventUTC;
                        to.mEventTimeSec = mEventTimeSec;
                        to.mSignalLabels = mSignalLabels;
                        to.mSignalUnits = mSignalUnits;

                        to.mImportConverter = mImportConverter;
                        to.mImportMethod = mImportMethod;

                        // from JSon in dBase
                        to.mDeviceID = mDeviceID;
                        to.mDeviceModel = mDeviceModel;
                        to.mPatientID = mPatientID.mCreateCopy();
                        to.mPatientTotalName = mPatientTotalName.mCreateCopy();
                        to.mPatientBirthDate = mPatientBirthDate.mCreateCopy();

                        to.mPhysicianName = mPhysicianName.mCreateCopy();
                        to.mRecRemark = mRecRemark.mCreateCopy();
                        to.mEventTypeString = mEventTypeString;
                        to.mStudyStartRecUTC = mStudyStartRecUTC;

                        to.mRecDurationSec = mRecDurationSec;
                        to.mTransmitUTC = mTransmitUTC;

                        // reference indexes for dBase
                        to.mRecState = mRecState;
                        to.mDevice_IX = mDevice_IX;
                        to.mPatient_IX = mPatient_IX;
                        to.mPatientGender = mPatientGender;
                        to.mPatientBirthYear = mPatientBirthYear;
                        to.mRecLabel = mRecLabel;
                        //                    to.mPhysician_IX = mPhysician_IX;
                        to.mStudy_IX = mStudy_IX;
                        to.mEventType_IX = mEventType_IX;

                        to.mDeviceMode = mDeviceMode;
                        to.mSeqNrInStudy = mSeqNrInStudy;
                        to.mNrAnalysis = mNrAnalysis;

                        to.mExtraData = mExtraData;
                        to.mInvertChannelsMask = mInvertChannelsMask;

                        // signal array
                        to.mNrSignals = mNrSignals;
                        to.mDisposeSignals();

                        to.mAnnotationAtr = mAnnotationAtr;

                        // added 20190225
                        _mStudyPermissions.mCopyTo(ref to._mStudyPermissions);   // permissions bit mask (used for filtering records on site and operator skill grade
                        to._mStudyTrial_IX = _mStudyTrial_IX;           // study belongs to trial
                        to._mStudyClient_IX = _mStudyClient_IX;           // study belongs to trial

                        to.mTimeZoneMoved = mTimeZoneMoved;     // count cals to move time zone
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("CopyTo", ex);
                    bOk = false;
                }

            }
            return bOk;
        }
        public override void mClear()
        {
            base.mClear();
            DEncryptLevel level = DEncryptLevel.L1_Program;    // needed encryption level for all encrypted

            // from Header and dat file
            mbReadUTC = false;  // UTC date time is read from file
            mbReadDT = false;  // local date time is read from file
            mbReadTZ = false;   // time zone is read from file

            mNrSignals = 0;
            mSampleFrequency = 0;
            mNrSamplesPerSignal = 0;      // currently discard value = 0
            mDataFormat = 0;              // curently only reads 311
            mAdcGain = 0;                  // number of units / mV
            mAdcBits = 0;                 // number of bits in sample 
            mAdcZero = 0;                   // curently discarded

            mSampleUnitT = 0;              // 1 / mFrequency
            mSampleUnitA = 0.0F;            // 1 / mAdcGain
            mNrSamples = 0;                // nr sample values in the file

            mBaseUTC = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);

            // From Header in dBase 
            mFileName = "";
            mStoredAt = "";
            mReceivedUTC = mBaseUTC;
            mTimeZoneOffsetMin = 0;
            mEventUTC = mBaseUTC;
            mEventTimeSec = 0.0F;
            mSignalLabels = "";
            mSignalUnits = "";

            mImportConverter = 0;         // import converter enum
            mImportMethod = 0;            // Import method enum

            // from JSon in dBase
            mDeviceID = "";
            mDeviceModel = "";
            mPatientID = new CEncryptedString("Rec.PatientID", level);
            mPatientTotalName = new CEncryptedString("Rec.FirstName", level);
            //mPatientMiddleInitials = new CEncryptedString("Rec.MiddleName", level);
            //mPatientLastName = new CEncryptedString("Rec.LastName", level);
            mPatientBirthDate = new CEncryptedInt("Rec.BirthDate", level);


            mPhysicianName = new CEncryptedString("Rec.PhysName", level);
            mRecRemark = new CEncryptedString("Rec.PatientComment", level);
            mEventTypeString = "";
            mStudyStartRecUTC = DateTime.MinValue; // no study start dinfo mBaseUTC;
            //            mStartRecLocalDT = DateTime.MinValue;
            mRecDurationSec = 0;
            mTransmitUTC = mBaseUTC; ;
            //            mTransmitLocalDT = DateTime.MinValue;

            // reference indexes for dBase
            mRecState = 0;
            mDevice_IX = 0;
            mPatient_IX = 0;
            mPatientGender = 0;
            mPatientBirthYear = 0;
            mRecLabel = "";
            //            mPhysician_IX = 0;
            mStudy_IX = 0;
            mEventType_IX = 0;

            mDeviceMode = "";
            mSeqNrInStudy = 0;
            mNrAnalysis = 0;

            mExtraData = "";
            mInvertChannelsMask = 0;


            // signal array
            mNrSignals = 0;
            mDisposeSignals();

            mAnnotationAtr = null;

            // added 20190225
            _mStudyPermissions.mClear();   // permissions bit mask (used for filtering records on site and operator skill grade
            _mStudyTrial_IX = 0;           // study belongs to trial
            _mStudyClient_IX = 0;

            mTimeZoneMoved = 0;

        }
        public static string sGetLockName()
        {
            return "Record";
        }

        public void mDisposeSignals()
        {
            if (mSignals != null)
            {
                for (int i = 0; i < mNrSignals; ++i)
                {
                    if (mSignals[i] != null)
                    {
                        mSignals[i] = null;
                    }
                }
                mSignals = null;
            }
            if (mAnnotationAtr != null)
            {
                mAnnotationAtr.mDispose();
            }
        }
        public bool mbCreateSignals(ushort ANrSignals)
        {
            mDisposeSignals();
            mSignals = new CSignalData[ANrSignals];

            if (mSignals != null)
            {
                mNrSignals = ANrSignals;
            }
            return mSignals != null;
        }

        public bool mbCreateOneSignalData(ushort ASignalIndex, uint ANrDataValues)
        {
            bool bOk = false;
            if (ASignalIndex < mNrSignals)
            {
                mSignals[ASignalIndex] = new CSignalData();

                if (mSignals[ASignalIndex] != null)
                {
                    bOk = mSignals[ASignalIndex].mbCreateData(ANrDataValues);
                }
            }
            return bOk;
        }
        public bool mbCreateAllSignalData(uint ANrDataValues)
        {
            bool bOk = false;

            if (mNrSignals > 0 && mSignals != null)
            {
                bOk = true;
                for (int i = 0; i < mNrSignals; ++i)
                {
                    mSignals[i] = new CSignalData();

                    bOk &= mSignals[i] != null
                            && mSignals[i].mbCreateData(ANrDataValues);
                }
            }
            return bOk;
        }

        public bool mSetTimeZoneOffset(DateTime AUtc, DateTime ALocalDT)
        {
            return CProgram.sCalcTimeZoneOffset(out mTimeZoneOffsetMin, AUtc, ALocalDT);
        }

        public DateTime mGetDeviceTime(DateTime AUtc)
        {
            return CProgram.sCalcLocalDT(AUtc, mTimeZoneOffsetMin);
        }
        public string mGetDeviceTimeString(DateTime AUtc, DTimeZoneShown AShowType)
        {
            string s = CProgram.sDateTimeToString(CProgram.sCalcLocalDT(AUtc, mTimeZoneOffsetMin));

            s += CProgram.sTimeZoneOffsetString((Int16)mTimeZoneOffsetMin, AShowType);
            return s;
        }


        public override DSqlDataType mVarGetSet(DSqlDataCmd ACmd, UInt16 AVarIndex, ref string AVarSqlName, ref string AStringValue, out bool ArbValid)
        {
            switch ((DRecordMitVars)AVarIndex)
            {
                // primary keys
                case DRecordMitVars.ReceivedUTC:
                    return mSqlGetSetUTC(ref mReceivedUTC, "ReadUTC", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.FileName:
                    return mSqlGetSetString(ref mFileName, "FileName", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);

                case DRecordMitVars.DeviceID:
                    return mSqlGetSetString(ref mDeviceID, "DeviceID", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);
                // variables
                case DRecordMitVars.TimeZoneOffsetMin:
                    return mSqlGetSetInt32(ref mTimeZoneOffsetMin, "TimeZoneOffsetMin", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.DeviceModel:
                    return mSqlGetSetString(ref mDeviceModel, "DeviceModel", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);

                case DRecordMitVars.Device_IX:
                    return mSqlGetSetUInt32(ref mDevice_IX, "Device_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.PatientID_ES:
                    return mSqlGetSetEncryptedString(ref mPatientID, "PatientID_ES", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);

                case DRecordMitVars.PatientTotalName_ES:
                    return mSqlGetSetEncryptedString(ref mPatientTotalName, "PatientTotalName_ES", ACmd, ref AVarSqlName, ref AStringValue, 128, out ArbValid);

                case DRecordMitVars.Patient_IX:
                    return mSqlGetSetUInt32(ref mPatient_IX, "Patient_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.PatientGender:
                    return mSqlGetSetInt32(ref mPatientGender, "PatientGender", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.PatientBirthDate_EI:
                    return mSqlGetSetEncryptedInt(ref mPatientBirthDate, "PatientBirthDate_EI", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.PatientBirthYear:
                    return mSqlGetSetYear(ref mPatientBirthYear, "PatientBirthYear", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.RecLabel:
                    return mSqlGetSetString(ref mRecLabel, "RecLabel", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);

                case DRecordMitVars.RecRemark_ES:
                    return mSqlGetSetEncryptedString(ref mRecRemark, "RecRemark_ES", ACmd, ref AVarSqlName, ref AStringValue, 250, out ArbValid);

                case DRecordMitVars.PhysicianName_ES:
                    return mSqlGetSetEncryptedString(ref mPhysicianName, "PhysicianName_ES", ACmd, ref AVarSqlName, ref AStringValue, 128, out ArbValid);

                //                case DSqlRecordVars.Physician_IX:
                //                    return mSqlGetSetInt32(ref mPhysician_IX, "Physician_IX", ACmd, ref AVarSqlName, ref AStringValue);

                case DRecordMitVars.Study_IX:
                    return mSqlGetSetUInt32(ref mStudy_IX, "Study_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.StartRecUTC:
                    return mSqlGetSetUTC(ref mBaseUTC, "StartRecUTC", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.RecDurationSec:
                    return mSqlGetSetFloat(ref mRecDurationSec, "RecDurationSec", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.EventTypeString:
                    return mSqlGetSetString(ref mEventTypeString, "EventTypeString", ACmd, ref AVarSqlName, ref AStringValue, 128, out ArbValid);

                case DRecordMitVars.EventType_IX:
                    return mSqlGetSetUInt16(ref mEventType_IX, "EventType_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.EventUTC:
                    return mSqlGetSetUTC(ref mEventUTC, "EventUTC", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.EventTimeSec:
                    return mSqlGetSetFloat(ref mEventTimeSec, "EventTimeSec", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.TransmitUTC:
                    return mSqlGetSetUTC(ref mTransmitUTC, "TransmitUTC", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.RecState_IX:
                    return mSqlGetSetUInt16(ref mRecState, "RecState", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.ImportConverter:
                    return mSqlGetSetUInt16(ref mImportConverter, "ImportConverter", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.ImportMethod:
                    return mSqlGetSetUInt16(ref mImportMethod, "ImportMethod", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.NrSignals:
                    return mSqlGetSetUInt16(ref mNrSignals, "NrSignals", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.SignalLabels:
                    return mSqlGetSetString(ref mSignalLabels, "SignalLabels", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DRecordMitVars.SignalUnits:
                    return mSqlGetSetString(ref mSignalUnits, "SignalUnits", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);

                case DRecordMitVars.StoredAt:
                    return mSqlGetSetString(ref mStoredAt, "StoredAt", ACmd, ref AVarSqlName, ref AStringValue, cStoredAtMaxLen, out ArbValid);

                case DRecordMitVars.DeviceMode:
                    return mSqlGetSetString(ref mDeviceMode, "DeviceMode", ACmd, ref AVarSqlName, ref AStringValue, 32, out ArbValid);

                case DRecordMitVars.SeqNrInStudy:
                    return mSqlGetSetUInt16(ref mSeqNrInStudy, "SeqNrInStudy", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DRecordMitVars.NrAnalysis:
                    return mSqlGetSetUInt16(ref mNrAnalysis, "NrAnalysis", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecordMitVars.ExtraData:
                    return mSqlGetSetString(ref mExtraData, "ExtraData", ACmd, ref AVarSqlName, ref AStringValue, 250, out ArbValid);
                case DRecordMitVars.InvertChannelsMask:
                    return mSqlGetSetUInt32(ref mInvertChannelsMask, "InvertChannelsMask", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                // added 20190225
                case DRecordMitVars.StudyPermissions:
                    return mSqlGetSetBitSet64(ref _mStudyPermissions, "StudyPermissions", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecordMitVars.Trial_IX:
                    return mSqlGetSetUInt32(ref _mStudyTrial_IX, "Trial_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DRecordMitVars.Client_IX:
                    return mSqlGetSetUInt32(ref _mStudyClient_IX, "Client_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
            }
            return base.mVarGetSet(ACmd, AVarIndex, ref AVarSqlName, ref AStringValue, out ArbValid);
        }

        /*        public string mGetPatientTotalName()
                {
                    string name = mPatientLastName.mDecrypt();

                    if (mPatientFirstName != null)
                    {
                        name += ", " + mPatientFirstName.mDecrypt();
                        if (mPatientMiddleInitials != null) name += " " + mPatientMiddleInitials.mDecrypt();
                    }
                    else
                    {
                        if (mPatientMiddleInitials != null) name += ", " + mPatientMiddleInitials.mDecrypt();
                    }
                    return name;
                }
        */
        public string mGetAlternateSerialNr()
        {
            string altSerialNr = "";

            if (mDeviceID != null && mDeviceID.Length > 0)
            {
                if (mDeviceModel != null && (mDeviceModel.Contains("TZ") || mDeviceModel.Contains("{SCP}")))
                {
                    altSerialNr = CDvtmsData.sGetTzAltSnrName(mDeviceID);
                }
            }
            return altSerialNr;
        }
        public static string sGetStateUserString(UInt32 AState)
        {
            string s;

            // todo: add static language state strings
            if (AState < (UInt32)DRecState.NrRecStates)
            {
                DRecState state = (DRecState)AState;
                s = state.ToString();
            }
            else
            {
                s = "State=" + AState.ToString();
            }
            return s;
        }

        public static bool sbParseStateUserString(out UInt16 ArState, string AString)
        {
            bool bOk = false;
            UInt16 state = 0;

            // todo: add static language state strings
            if (AString != null && AString.Length > 0)
            {
                for (DRecState i = DRecState.Unknown; i < DRecState.NrRecStates; ++i)
                {
                    if (AString == i.ToString())
                    {
                        state = (UInt16)i;
                        bOk = true;
                        break;
                    }
                }
            }
            ArState = state;
            return bOk;
        }
        public static string sGetConverterUserString(UInt32 AConverter)
        {
            string s = "?";

            // todo: add static language state strings
            if (AConverter < (UInt32)DImportConverter.NrConverters)
            {
                DImportConverter state = (DImportConverter)AConverter;

                switch (state)
                {
                    case DImportConverter.Unknown: s = "Unknown"; break;
                    case DImportConverter.IntriconSironaZip: s = "Sirona_Zip"; break;
                    case DImportConverter.IntriconSironaMit: s = "Sirona_Mit"; break;
                    case DImportConverter.TZAeraScp: s = "TzHttp"; break;
                    case DImportConverter.DV2Scp: s = "DV2_Scp"; break;
                    case DImportConverter.SironaHttp: s = "SironaHttp"; break;
                    case DImportConverter.DVX1: s = "DVX1"; break;
                    case DImportConverter.DvxSpO: s = "DvxSpO"; break;
                }
            }
            else
            {
                s = "Conv=" + AConverter.ToString();
            }
            return s;
        }

        public static bool sbParseConverterUserString(out UInt16 ArState, string AString)
        {
            bool bOk = false;
            UInt16 state = 0;

            // todo: add static language state strings
            if (AString != null && AString.Length > 0)
            {
                for (DImportConverter i = DImportConverter.Unknown; i < DImportConverter.NrConverters; ++i)
                {
                    if (AString == i.ToString())
                    {
                        state = (UInt16)i;
                        bOk = true;
                        break;
                    }
                }
            }

            ArState = state;
            return bOk;
        }
        public static string sGetMethodUserString(UInt32 AMethod)
        {
            string s;

            // todo: add static language state strings
            if (AMethod < (UInt32)DImportMethod.NrMethods)
            {
                DImportMethod state = (DImportMethod)AMethod;
                s = state.ToString();
            }
            else
            {
                s = "State=" + AMethod.ToString();
            }
            return s;
        }

        public static bool sbParseMethodUserString(out UInt16 ArState, string AString)
        {
            bool bOk = false;
            UInt16 state = 0;

            // todo: add static language state strings
            if (AString != null && AString.Length > 0)
            {
                for (DImportMethod i = DImportMethod.Unknown; i < DImportMethod.NrMethods; ++i)
                {
                    if (AString == i.ToString())
                    {
                        state = (UInt16)i;
                        bOk = true;
                        break;
                    }
                }
            }

            ArState = state;
            return bOk;
        }

        public string mNameChannelMECG(UInt16 AChannelNr)
        {
            switch (AChannelNr)
            {
                case 0: return "Lead I";
                case 1: return "Lead II";
                case 2: return "V1";
                case 3: return "V2";
                case 4: return "V3";
                case 5: return "V4";
                case 6: return "V5";
                case 7: return "V6";
            }
            return "Ch " + AChannelNr;
        }
        public bool mbSaveMECG(string AFilePath, bool AbPromptContinue)
        {
            bool bOk = AFilePath != null && AFilePath.Length > 0;

            if (bOk)
            {
                string filePath = Path.ChangeExtension(AFilePath, ".txt");
                string name = Path.GetFileNameWithoutExtension(filePath);

                try
                {
                    /* Whaletec MECG format *.txt, values in uV
                    (int)<sample freq>
                    <N samples>
                    start
                    <name 1>
                    (int)(<value hannel 1> * 1000)

                    <name 2>
                    (int)(<value hannel 2> * 1000)
                    */

                    using (StreamWriter sw = new StreamWriter(filePath))
                    {
                        uint sampleTimeFreq = mSampleUnitT > 0.00001F ? (uint)(1.0 / mSampleUnitT + 0.5) : (uint)0;
                        uint sampleFreq = mSampleFrequency == 0 ? sampleTimeFreq : mSampleFrequency;

                        sw.WriteLine(sampleFreq.ToString());
                        sw.WriteLine(mNrSamples.ToString());
                        sw.WriteLine("Start");

                        Int32[] minValue = new Int32[mNrSignals];
                        Int32[] maxValue = new Int32[mNrSignals];
                        Int32[] offset = new Int32[mNrSignals];
                        int minLimit = -5000;
                        int maxLimit = +5000;
                        Int32 i, j, v;
                        int nOutOfRange = 0;
                        float scale = mSampleUnitA * 1000.0F;
                        float unit = 0.001F;

                        // determin min & max
                        for (j = 0; j < mNrSignals; ++j)
                        {
                            minValue[j] = Int32.MaxValue;
                            maxValue[j] = Int32.MinValue;
                            offset[j] = 0;
                        }

                        for (j = 0; j < mNrSignals; ++j)
                        {
                            Int32[] array = mSignals[j].mValues;

                            for (i = 0; i < mNrSamples; ++i)
                            {
                                v = (int)(array[i] * scale);
                                if (v < minValue[j]) minValue[j] = v;
                                if (v > maxValue[j]) maxValue[j] = v;
                            }
                        }
                        string lineInt = "channels ";
                        string lineFloat = "channels mV ";
                        int min, max, mean;
                        // calculate offset if out of range 
                        for (j = 0; j < mNrSignals; ++j)
                        {
                            min = minValue[j];
                            max = maxValue[j];
                            if (min <= minLimit || max >= maxLimit)
                            {
                                mean = (min + max) / 2;
                                offset[j] = mean;
                                min -= mean;
                                max -= mean;
                            }

                            lineInt += j.ToString() + "(" + min.ToString() + "<" + max.ToString() + ") ";
                            lineFloat += j.ToString() + "(" + (min * unit).ToString("0.000") + "<" + (max * unit).ToString("0.000") + ") ";

                        }
                        CProgram.sLogLine("Saving ECG to MECG " + name + " " + mNrSignals.ToString() + " signals, " + mNrSamples.ToString() + " samples, "
                            + mSampleUnitA.ToString("0.0000") + " mV" + mSampleUnitT.ToString("0.0000") + " sec");
                        CProgram.sLogLine(lineInt);
                        CProgram.sLogLine(lineFloat);
                        // save data to file
                        int jLast = mNrSignals - 1;
                        int iLast = (int)mNrSamples - 1;

                        for (j = 0; j < mNrSignals; ++j)
                        {
                            string label = mNameChannelMECG((UInt16)j);

                            if (label == null || label.Length == 0)
                            {
                                label = "ECG" + j.ToString();
                            }
                            sw.WriteLine(label);
                            for (i = 0; i < mNrSamples; ++i)
                            {
                                v = (int)((mSignals[j].mValues[i] - offset[j]) * scale + 0.5F);
                                if (v < minLimit)
                                {
                                    v = minLimit;
                                    ++nOutOfRange;
                                }
                                else if (v > maxLimit)
                                {
                                    v = maxLimit;
                                    ++nOutOfRange;
                                }
                                if (j == jLast && i == iLast)
                                {
                                    sw.Write(v.ToString()); // Whaletech reader: last line may not contain \r\n
                                }
                                else
                                {
                                    sw.WriteLine(v.ToString());
                                }
                            }
                        }

                        if (nOutOfRange > 0)
                        {
                            CProgram.sLogError("Save ECG to MECG: " + name + " " + nOutOfRange.ToString() + " points  out of range");
                            if (AbPromptContinue)
                            {
                                bOk = CProgram.sbAskYesNo("Save ECG to MECG: " + name, nOutOfRange.ToString() + " points  out of range, Continue?");
                            }
                        }
                        sw.Close();
                    }

                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Write MIT16Header error " + filePath, ex);
                    bOk = false;
                }
            }
            return bOk;
        }
        public bool mbSaveDvx1(string AFilePath, UInt16 ANrBlocksPerMin, UInt16 ANrBits, DateTime AStartUTC, DateTime AEventUTC)
        {
            bool bOk = AFilePath != null && AFilePath.Length > 0;

            string folder = "DVX1_" + CProgram.sDateTimeToYMDHMS(DateTime.Now);
            string folderPath = Path.Combine(AFilePath, folder);

            try
            {
                List<String> fileList = null;

                if (false == Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }

                bOk = mbSaveDvxEcgData(folderPath, ANrBlocksPerMin, ANrBits, AStartUTC, out fileList);

                if (bOk)
                {
                    bOk = mbSaveDvxEcgEvent(folderPath, AStartUTC, AEventUTC, fileList);
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("SaveDvx1 error " + folderPath, ex);
                bOk = false;
            }

            return bOk;
        }

        private void mWriteStreamLine(FileStream AStream, string ALine)
        {
            Byte[] info = new UTF8Encoding(true).GetBytes(ALine + "\r\n");

            AStream.Write(info, 0, info.Length);
        }

        public bool mbSaveDvxEcgDataBlock(string AFilePath, UInt16 ANrBits, DateTime AStartUTC, float AStartOffsetSec, float ABlockSizeSec)
        {
            bool bOk = AFilePath != null && AFilePath.Length > 0;
            int nrBits = ANrBits <= 0 ? 8 : (ANrBits > 32 ? 32 : ANrBits);
            int nrBytes = (UInt16)((ANrBits + 7) / 8);

            if (bOk)
            {
                try
                {
                    // DVX1 foramat
                    // var=value\r\n
                    //                      strip definition values and status info
                    // var=value\r\n
                    // &Data
                    // <$5A><$A5>
                    // <low[sig0][0]><high[sig0][0]><low[sig1][0]><high[sig1][0]>...<low[sigN][0]><high[sigN][0]>
                    //..
                    // <low[sig0][n]><high[sig0][n]><low[sig1][n]><high[sig1][n]>...<low[sigN][n]><high[sigN][n]>

                    string sampleFormat = "RAW" + (nrBytes * 8).ToString();


                    if (File.Exists(AFilePath))
                    {
                        File.Delete(AFilePath);
                    }

                    using (FileStream fs = File.Create(AFilePath))
                    {
                        uint sampleTimeFreq = mSampleUnitT > 0.00001F ? (uint)(1.0 / mSampleUnitT + 0.5) : (uint)0;
                        uint sampleFreq = mSampleFrequency == 0 ? sampleTimeFreq : mSampleFrequency;
                        string units = "", scale = "";
                        int i, j, value;

                        mWriteStreamLine(fs, "snr= " + mDeviceID);
                        mWriteStreamLine(fs, "cpu = Eventboard@" + CProgram.sGetPcName());
                        mWriteStreamLine(fs, "model = DVX1 - sim ");
                        mWriteStreamLine(fs, "Firmware = " + CProgram.sGetProgVersion());
                        mWriteStreamLine(fs, "startUTC = " + AStartUTC.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        mWriteStreamLine(fs, "startTimeZone = " + mTimeZoneOffsetMin.ToString());
                        mWriteStreamLine(fs, "NrChannels = " + mNrSignals.ToString());
                        mWriteStreamLine(fs, "channelNames =");
                        for (i = 1; i <= mNrSignals; ++i)
                        {
                            units += "mV;";
                            scale += mSampleUnitA.ToString("0.00000000", CultureInfo.InvariantCulture) + ";";
                        }
                        mWriteStreamLine(fs, "channelUnits = " + units);
                        mWriteStreamLine(fs, "channelScale = " + scale);
                        mWriteStreamLine(fs, "sampleTimeSec = " + mSampleUnitT.ToString("0.00000000", CultureInfo.InvariantCulture));
                        mWriteStreamLine(fs, "sampleFreqHz= " + sampleFreq.ToString());
                        mWriteStreamLine(fs, "sampleFormat = " + sampleFormat);
                        mWriteStreamLine(fs, "sampleNrBits = " + ANrBits);
                        mWriteStreamLine(fs, "stripDurationSec= " + ABlockSizeSec.ToString("0.000", CultureInfo.InvariantCulture));
                        mWriteStreamLine(fs, "podID = 1 ");
                        mWriteStreamLine(fs, "Mode = POST ");
                        mWriteStreamLine(fs, "leadoffChannels = 0 ");
                        mWriteStreamLine(fs, "FilterLowHz = 0.05 ");
                        mWriteStreamLine(fs, "filterNotch50 = 1 ");
                        mWriteStreamLine(fs, "filterNotch60 = 0 ");
                        mWriteStreamLine(fs, "FilterHighHz = 100 ");
                        mWriteStreamLine(fs, "FilterDCSec = 6.0 ");
                        mWriteStreamLine(fs, "SendReduceFactor = 5 ");
                        mWriteStreamLine(fs, "settingsUTC = ");
                        mWriteStreamLine(fs, "settingResult = ");
                        mWriteStreamLine(fs, "actionUTC = ");
                        mWriteStreamLine(fs, "actionResult= ");

                        mWriteStreamLine(fs, "&Data");
                        fs.WriteByte(0x5A); // start sequence
                        fs.WriteByte(0xA5);

                        for (j = 0; j < mNrSamples; ++j)
                        {
                            for (i = 0; i < mNrSignals; ++i)
                            {
                                value = mSignals[i].mValues[j];
                                fs.WriteByte((byte)(value & 0x0FF));
                                if (nrBytes >= 2)
                                {
                                    value >>= 8;
                                    fs.WriteByte((byte)(value & 0x0FF));
                                    if (nrBytes >= 3)
                                    {
                                        value >>= 8;
                                        fs.WriteByte((byte)(value & 0x0FF));
                                        if (nrBytes >= 4)
                                        {
                                            value >>= 8;
                                            fs.WriteByte((byte)(value & 0x0FF));
                                        }
                                    }
                                }
                            }
                        }
                        fs.Close();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Write DVX1 ecg file error" + AFilePath, ex);
                    bOk = false;
                }
            }
            return bOk;
        }

        public bool mbSaveDvxEcgData(string AFilePath, UInt16 ANrBlocksPerMin, UInt16 ANrBits, DateTime AStartUTC, out List<String> AFileList)
        {
            bool bOk = AFilePath != null && AFilePath.Length > 0;
            List<String> fileList = new List<string>();

            if (bOk && fileList != null)
            {
                DateTime startUtc = AStartUTC;
                float stripDurationSec = mGetSamplesTotalTime();
                float stripStart = 0.0f;
                float stripBlockSec = stripDurationSec;
                int blockSec = ANrBlocksPerMin == 0 ? 9999999 : (ANrBlocksPerMin <= 6 ? 60 / ANrBlocksPerMin : 10);
                DateTime nextUtc;
                string name, filePath, folder, folderPath;

                do
                {
                    if (ANrBlocksPerMin > 0)
                    {
                        int sec = startUtc.Second;
                        int n = sec / blockSec;
                        int nextSec = (n + 1) * blockSec;

                        stripBlockSec = 0.0F;
                        if (startUtc.Millisecond != 0)
                        {
                            stripBlockSec = 0.001F * (1000 - startUtc.Millisecond);
                            ++sec;
                        }
                        stripBlockSec += nextSec - sec;
                        float leftSec = stripDurationSec - stripStart;
                        if (stripBlockSec >= leftSec)
                        {
                            stripBlockSec = leftSec;

                        }
                    }
                    nextUtc = startUtc.AddSeconds(stripBlockSec);

                    name = mDeviceID + "_" + CProgram.sDateTimeToYMDHMS(startUtc)
                        + (mTimeZoneOffsetMin >= 0 ? "p" + mTimeZoneOffsetMin.ToString("D3") : "n" + (-mTimeZoneOffsetMin).ToString("D3")) + ".DvxEcg";


                    folder = "Rec" + CProgram.sDateToYMD(startUtc);
                    folderPath = Path.Combine(AFilePath, folder);
                    if (false == Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    filePath = Path.Combine(folderPath, name);

                    bOk = mbSaveDvxEcgDataBlock(filePath, ANrBits, startUtc, stripStart, stripBlockSec);

                    if (bOk)
                    {
                        fileList.Add(name);
                    }


                    startUtc = nextUtc;
                    stripStart += stripBlockSec;

                } while (stripStart < stripDurationSec);


            }
            AFileList = fileList;
            return bOk;
        }

        public bool mbSaveDvxEcgEvent(string AFilePath, DateTime AStartUTC, DateTime AEventUTC, List<String> AFileList)
        {
            bool bOk = AFilePath != null && AFilePath.Length > 0;

            if (bOk)
            {
                string name = mDeviceID + "_" + CProgram.sDateTimeToYMDHMS(AEventUTC)
                    + (mTimeZoneOffsetMin >= 0 ? "p" + mTimeZoneOffsetMin.ToString("D3") : "n" + (-mTimeZoneOffsetMin).ToString("D3")) + ".DvxEvent";

                string folder = "Rec" + CProgram.sDateToYMD(AEventUTC);
                string folderPath = Path.Combine(AFilePath, folder);
                if (false == Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                string filePath = Path.Combine(folderPath, name);

                try
                {
                    // DVX1 event 
                    // var=value\r\n
                    //                      eventdefinition values and status info
                    // var=value\r\n
                    // header:
                    // <filename> <nr signals> <sample freq> <n samples per signal=0> <base HH:mm:ss> <DD/MM/YYYY>
                    // <filename> <format = 16> <ADC gain> <nr bits> <<ADC zero = 0> <signal name>
                    // # remark

                    using (StreamWriter sw = new StreamWriter(filePath))
                    {
                        sw.WriteLine("snr= " + mDeviceID);
                        sw.WriteLine("cpu = Eventboard@" + CProgram.sGetPcName());
                        sw.WriteLine("model = DVX1 - sim ");
                        sw.WriteLine("Firmware = " + CProgram.sGetProgVersion());
                        sw.WriteLine("eventUTC = " + AEventUTC.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        sw.WriteLine("eventTimeZone = " + mTimeZoneOffsetMin.ToString());

                        sw.WriteLine("stripStartUTC = " + AStartUTC.ToString("yyyy-MM-dd HH:mm:ss.fff"));

                        sw.WriteLine("eventRelSec = " + (AEventUTC - AStartUTC).TotalSeconds.ToString("0.000", CultureInfo.InvariantCulture));
                        sw.WriteLine("podID = 1 ");
                        sw.WriteLine("Mode = POST");

                        sw.WriteLine("eventDetected =" + mEventTypeString);

                        sw.WriteLine("eventValue =");
                        sw.WriteLine("eventLimitMin =");
                        sw.WriteLine("eventLimitMax =");
                        if (AFileList != null)
                        {
                            int n = AFileList.Count;

                            sw.WriteLine("nrFiles=" + n.ToString());
                            for (int i = 0; i < n; ++i)
                            {
                                sw.WriteLine("File=" + AFileList[i]);

                            }
                        }


                        sw.WriteLine("sendUTC=" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        sw.WriteLine("sendBy=WiFi");
                        sw.WriteLine("sendServerIP=");
                        sw.Close();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Write DVX1 event error " + filePath, ex);
                    bOk = false;
                }
            }
            return bOk;
        }

        public bool mbSaveMIT(string AFilePath, UInt16 ANrBits, bool AbPromptContinue,
            float AStartSec = -1, float ADurationSec = -1)   // nrBits should be 8, 16, 24, 32
        {
            bool bOk = AFilePath != null && AFilePath.Length > 0;

            if (bOk)
            {
                string strRecNr = "R" + mIndex_KEY.ToString("D7");
                string strStudy = mStudy_IX == 0 ? "S0" : "S" + mStudy_IX + "." + mSeqNrInStudy;
                string heaRemark = strRecNr + "_" + strStudy + " startSec=" + AStartSec.ToString("#.#")
                    + " ;durSec=" + ADurationSec.ToString("#.#") + " B" + mAdcBits;

                if (mAdcBits > ANrBits)
                {
                    UInt16 diffBits = (UInt16)(mAdcBits - ANrBits);
                    Int32 scale = (Int32)(1 << (diffBits / 2));      // probably 24 bit -> 16 => scale = 16 

                    bOk = mbSaveMITHeader(AFilePath, ANrBits, scale, heaRemark, AStartSec);

                    if (bOk)
                    {
                        bOk = mbSaveMITDataScaled(AFilePath, ANrBits, AbPromptContinue, scale, AStartSec, ADurationSec);
                    }
                }
                else
                {
                    bOk = mbSaveMITHeader(AFilePath, ANrBits, 1, heaRemark, AStartSec);

                    if (bOk)
                    {
                        bOk = mbSaveMITData(AFilePath, ANrBits, AbPromptContinue, AStartSec, ADurationSec);
                    }
                }
            }
            return bOk;
        }

        public bool mbSaveMITHeader(string AFilePath, UInt16 ANrBits, Int32 AScale = 1, string AHeaRemark = "", float AStartSec = -1)
        {
            bool bOk = AFilePath != null && AFilePath.Length > 0;

            if (bOk)
            {
                string filePath = Path.ChangeExtension(AFilePath, ".hea");
                string name = Path.GetFileNameWithoutExtension(filePath);
                int scale = AScale == 0 ? 1 : AScale;

                try
                {
                    // header:
                    // <filename> <nr signals> <sample freq> <n samples per signal=0> <base HH:mm:ss> <DD/MM/YYYY>
                    // <filename> <format = 16> <ADC gain> <nr bits> <<ADC zero = 0> <signal name>
                    // # remark

                    using (StreamWriter sw = new StreamWriter(filePath))
                    {
                        uint sampleTimeFreq = mSampleUnitT > 0.00001F ? (uint)(1.0 / mSampleUnitT + 0.5) : (uint)0;
                        uint sampleFreq = mSampleFrequency == 0 ? sampleTimeFreq : mSampleFrequency;

                        string header = name + " " + mNrSignals + " " + sampleFreq.ToString() + " 0 ";


                        if (mBaseUTC != DateTime.MinValue)
                        {
                            DateTime dt = mGetDeviceTime(mBaseUTC.AddSeconds(AStartSec < 0 ? 0 : AStartSec));
                            header += dt.ToString("HH:mm:ss dd/MM/yyyy");
                        }
                        sw.WriteLine(header);

                        for (int i = 0; i < mNrSignals; ++i)
                        {
                            double gain = Math.Abs(mSampleUnitA) <= 1e-9 ? (mAdcGain > 0 ? mAdcGain : 1e9) : 1.0 / mSampleUnitA;
                            string label = mGetChannelName((UInt16)i);

                            gain /= scale;
                            if (label == null || label.Length == 0)
                            {
                                label = "ECG" + i.ToString();
                            }

                            string line = name + ".dat " + ANrBits.ToString() + " " + gain.ToString("0.00") + "/mV " + ANrBits.ToString() + " 0 0 0 0 " + label;
                            sw.WriteLine(line);
                        }

                        string remark = "#Eventboard timezone offset min = " + mTimeZoneOffsetMin.ToString();
                        sw.WriteLine(remark);
                        if( AHeaRemark.Length > 0)
                        {
                            sw.WriteLine("#" + AHeaRemark);
                        }
                        sw.Close();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Write MIT16Header error " + filePath, ex);
                    bOk = false;
                }
            }
            return bOk;
        }

        public bool mbSaveSironaJson(string AFilePath,
            float AStartSec, float ADurSec,
            string AEventType, string ARemark = "")
        {
            /*
{
  "Device": {
    "Device ID": "76A0100009",
    "Device Address": "EC:FE:7E:14:F4:7A"
  },
  "Patient": {
    "Patient ID": "",
    "Patient First Name": "",
    "Patient Middle Initial": "",
    "Patient Last Name": "",
    "Patient Date of Birth": "1900-01-01",
    "Physician Name": "",
    "Comment": ""
  },
  "Time": {
    "Procedure Start Time": "2019-10-24T10:08:49-0500",
    "Event type": "Manual",
    "Strip Start Time": "2019-10-24T10:10:36-0500",
    "Strip Length": 60,
    "Transmit Time": "2019-10-24T10:12:37-0500"
  },
  "Diary": {
    "Symptom": [
      "Chest Pain"
    ],
    "Activity": [
      "Strenuous exercise"
    ]
  }
}
             */
            // only write labels that are read, do not care about json structure
            DateTime dt = mGetDeviceTime(mBaseUTC);
            string dtStr = dt.ToString("yyyy-MM-ddTHH:mm:ss") + CProgram.sTimeZoneOffsetStringLong((short)mTimeZoneOffsetMin);
            string str =
                "\"progBy\": \"" + CProgram.sGetProgName() + " Save view part MIT16\"" // first line may not be Device ID
                + "\"Device ID\": \"" + mDeviceID + "\"\r\n"
                + "\"Patient ID\": \"" + mPatientID.mDecrypt() + "\"\r\n"
                + "\"Comment\": \"" + ARemark + "\"\r\n"
                + "\"Procedure Start Time\": \"" + dtStr + "\"\r\n"
                + "\"Event type\": \"" + AEventType + "\"\r\n";
            dt = mGetDeviceTime(mBaseUTC.AddSeconds(AStartSec));
            dtStr = dt.ToString("yyyy-MM-ddTHH:mm:ss") + CProgram.sTimeZoneOffsetStringLong((short)mTimeZoneOffsetMin);

            Int32 sec = (Int32)ADurSec;
            str += "\"Strip Start Time\": \"" + dtStr + "\"\r\n"
                + "\"Strip Length\": \"" + sec + "\"\r\n";
            dt = DateTime.Now;
            dtStr = dt.ToString("yyyy-MM-ddTHH:mm:ss") 
                    + CProgram.sTimeZoneOffsetStringLong((short)CProgram.sGetLocalTimeZoneOffsetMin());
            str += "\"Transmit Time\": \"" + dtStr + "\"\r\n"
                + "\"writenBy\": \"" + CProgram.sGetUserAtPcString() + "\"\r\n";

            bool bOk = AFilePath != null && AFilePath.Length > 0;

            if (bOk)
            {
                string filePath = Path.ChangeExtension(AFilePath, ".json");
                string name = Path.GetFileNameWithoutExtension(filePath);

                try
                {
                    using (StreamWriter sw = new StreamWriter(filePath))
                    {
                      sw.WriteLine(str);

                        sw.Close();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Write sirona json error " + filePath, ex);
                    bOk = false;
                }
            }
            return bOk;
        }

        public bool mbSaveMITData(string AFilePath, UInt16 ANrBits, bool AbPromptContinue,
            float AStartSec = -1, float ADurationSec = -1)
        {
            bool bOk = AFilePath != null && AFilePath.Length > 0;

            Int32 iStart = AStartSec <= 0 ? 0 : (Int32)(AStartSec * mSampleFrequency);
            Int32 iN = ADurationSec <= 0.1F ? (Int32)mNrSamples : (Int32)(ADurationSec * mSampleFrequency + 0.5F);
            Int32 iEnd = iStart + iN;


            if (bOk && mNrSignals > 0 && mNrSamples > 0 && mSignals != null && iStart < iEnd)
            {
                string filePath = Path.ChangeExtension(AFilePath, ".dat");
                string name = Path.GetFileNameWithoutExtension(filePath);
                string mitXX = "MIT" + ANrBits.ToString();

                try
                {
                    FileStream fs = File.OpenWrite(filePath);
                    Int32[] minValue = new Int32[mNrSignals];
                    Int32[] maxValue = new Int32[mNrSignals];
                    Int32[] offset = new Int32[mNrSignals];
                    int maxLimit = (1 << (ANrBits - 1)) - 1;
                    int minLimit = -maxLimit;
                    Int32 i, j, v;
                    int nOutOfRange = 0;

                    // determin min & max
                    for (j = 0; j < mNrSignals; ++j)
                    {
                        minValue[j] = Int32.MaxValue;
                        maxValue[j] = Int32.MinValue;
                        offset[j] = 0;
                    }

                    for (j = 0; j < mNrSignals; ++j)
                    {
                        Int32[] array = mSignals[j].mValues;

                        for (i = iStart; i < iEnd; ++i)
                        {
                            v = array[i];
                            if (v < minValue[j]) minValue[j] = v;
                            if (v > maxValue[j]) maxValue[j] = v;
                        }
                    }
                    string lineInt = "channels ";
                    string lineFloat = "channels mV ";
                    int min, max, mean;
                    // calculate offset if out of range 
                    for (j = 0; j < mNrSignals; ++j)
                    {
                        min = minValue[j];
                        max = maxValue[j];
                        if (min <= minLimit || max >= maxLimit)
                        {
                            mean = (min + max) / 2;
                            offset[j] = mean;
                            min -= mean;
                            max -= mean;
                        }

                        lineInt += j.ToString() + "(" + min.ToString() + "<" + max.ToString() + ") ";
                        lineFloat += j.ToString() + "(" + (min * mSampleUnitA).ToString("0.000") + "<" + (max * mSampleUnitA).ToString("0.000") + ") ";
                    }
                    CProgram.sLogLine("Saving ECG to " + mitXX + " " + name + " " + mNrSignals.ToString() + " signals, " + mNrSamples.ToString() + " samples, "
                        + mSampleUnitA.ToString("0.0000") + " mV" + mSampleUnitT.ToString("0.0000") + " sec");
                    CProgram.sLogLine(lineInt);
                    CProgram.sLogLine(lineFloat);
                    // save data to file

                    for (i = iStart; i < iEnd; ++i)
                    {
                        for (j = 0; j < mNrSignals; ++j)
                        {
                            v = mSignals[j].mValues[i] - offset[j];
                            if (v < minLimit)
                            {
                                v = minLimit;
                                ++nOutOfRange;
                            }
                            else if (v > maxLimit)
                            {
                                v = maxLimit;
                                ++nOutOfRange;
                            }
                            fs.WriteByte((byte)(v & 0x00FF));
                            if (ANrBits > 8)
                            {
                                fs.WriteByte((byte)((v >> 8) & 0x00FF));

                                if (ANrBits > 16)
                                {
                                    fs.WriteByte((byte)((v >> 16) & 0x00FF));
                                    if (ANrBits > 24)
                                    {
                                        fs.WriteByte((byte)((v >> 24) & 0x00FF));
                                    }
                                }
                            }
                        }
                    }
                    fs.Close();
                    if (nOutOfRange > 0)
                    {
                        string bitsError = "";
                        CProgram.sLogError("Save ECG to " + mitXX + ": " + name + " " + nOutOfRange.ToString() + " points  out of range");
                        if (mAdcBits > ANrBits)
                        {
                            CProgram.sLogError("Save ECG " + mAdcBits + " bits as " + ANrBits + " bits!");
                            bitsError = "(" + mAdcBits + ">" + ANrBits + " bits)";
                        }
                        if (AbPromptContinue)
                        {
                            bOk = CProgram.sbAskYesNo("Save ECG to " + mitXX + ": " + name + bitsError, nOutOfRange.ToString() + " points  out of range, Continue?");
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Write MIT Header error " + filePath, ex);
                    bOk = false;
                }
            }
            return bOk;
        }
        public bool mbSaveMITDataScaled(string AFilePath, UInt16 ANrBits, bool AbPromptContinue, Int32 AScale,
            float AStartSec = -1, float ADurationSec = -1)
        {
            bool bOk = AFilePath != null && AFilePath.Length > 0;
            Int32 iStart = AStartSec <= 0 ? 0 : (Int32)(AStartSec * mSampleFrequency);
            Int32 iEnd = ADurationSec <= 0.1F ? (Int32)mNrSamples : (Int32)(ADurationSec * mSampleFrequency + 0.5F);


            if (bOk && mNrSignals > 0 && mNrSamples > 0 && mSignals != null && iStart < iEnd)
            {
                string filePath = Path.ChangeExtension(AFilePath, ".dat");
                string name = Path.GetFileNameWithoutExtension(filePath);
                string mitXX = "MIT" + ANrBits.ToString();

                try
                {
                    FileStream fs = File.OpenWrite(filePath);
                    Int32[] minValue = new Int32[mNrSignals];
                    Int32[] maxValue = new Int32[mNrSignals];
                    Int32[] offset = new Int32[mNrSignals];
                    int maxLimit = (1 << (ANrBits - 1)) - 1;
                    int minLimit = -maxLimit;
                    Int32 i, j, v;
                    int nOutOfRange = 0;
                    Int32 scale = AScale == 0 ? 1 : AScale;

                    float unitA = mSampleUnitA * Math.Abs(scale);

                    // determin min & max
                    for (j = 0; j < mNrSignals; ++j)
                    {
                        minValue[j] = Int32.MaxValue;
                        maxValue[j] = Int32.MinValue;
                        offset[j] = 0;
                    }

                    for (j = 0; j < mNrSignals; ++j)
                    {
                        Int32[] array = mSignals[j].mValues;

                        for (i = iStart; i < iEnd; ++i)
                        {
                            v = array[i] / scale;
                            if (v < minValue[j]) minValue[j] = v;
                            if (v > maxValue[j]) maxValue[j] = v;
                        }
                    }
                    string lineInt = "channels ";
                    string lineFloat = "channels mV ";
                    int min, max, mean;
                    // calculate offset if out of range 
                    for (j = 0; j < mNrSignals; ++j)
                    {
                        min = minValue[j];
                        max = maxValue[j];
                        if (min <= minLimit || max >= maxLimit)
                        {
                            mean = (min + max) / 2;
                            offset[j] = mean;
                            min -= mean;
                            max -= mean;
                        }

                        lineInt += j.ToString() + "(" + min.ToString() + "<" + max.ToString() + ") ";
                        lineFloat += j.ToString() + "(" + (min * unitA).ToString("0.000") + "<" + (max * unitA).ToString("0.000") + ") ";
                    }
                    CProgram.sLogLine("Saving ECG scaled(" + scale + ") to " + mitXX + " " + name + " " + mNrSignals.ToString() + " signals, " + mNrSamples.ToString() + " samples, "
                        + unitA.ToString("0.0000") + " mV" + mSampleUnitT.ToString("0.0000") + " sec");
                    CProgram.sLogLine(lineInt);
                    CProgram.sLogLine(lineFloat);
                    // save data to file

                    for (i = iStart; i < iEnd; ++i)
                    {
                        for (j = 0; j < mNrSignals; ++j)
                        {
                            v = (mSignals[j].mValues[i] / scale) - offset[j];
                            if (v < minLimit)
                            {
                                v = minLimit;
                                ++nOutOfRange;
                            }
                            else if (v > maxLimit)
                            {
                                v = maxLimit;
                                ++nOutOfRange;
                            }
                            fs.WriteByte((byte)(v & 0x00FF));
                            if (ANrBits > 8)
                            {
                                fs.WriteByte((byte)((v >> 8) & 0x00FF));

                                if (ANrBits > 16)
                                {
                                    fs.WriteByte((byte)((v >> 16) & 0x00FF));
                                    if (ANrBits > 24)
                                    {
                                        fs.WriteByte((byte)((v >> 24) & 0x00FF));
                                    }
                                }
                            }
                        }
                    }
                    fs.Close();
                    if (nOutOfRange > 0)
                    {
                        string bitsError = "";
                        CProgram.sLogError("Save ECG scaled(" + scale + ") to " + mitXX + ": " + name + " " + nOutOfRange.ToString() + " points  out of range");
                        if (mAdcBits > ANrBits)
                        {
                            CProgram.sLogError("Save ECG scaled(" + scale + ") " + mAdcBits + " bits as " + ANrBits + " bits!");
                            bitsError = "(" + mAdcBits + ">" + ANrBits + " bits)";
                        }
                        if (AbPromptContinue)
                        {
                            bOk = CProgram.sbAskYesNo("Save ECG scaled(" + scale + ") to " + mitXX + ": " + name + bitsError, nOutOfRange.ToString() + " points  out of range, Continue?");
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Write MIT Header error " + filePath, ex);
                    bOk = false;
                }
            }
            return bOk;
        }

        public bool mbSaveDvxRaw(string AFilePath, bool AbAddLOff, UInt16 ANrBits, bool AbPromptContinue)
        {
            bool bOk = AFilePath != null && AFilePath.Length > 0;

            if (bOk && mNrSignals > 0 && mNrSamples > 0 && mSignals != null)
            {
                string filePath = Path.ChangeExtension(AFilePath, CDvxEvtRec._cEcgExtension);
                string name = Path.GetFileNameWithoutExtension(filePath);

                try
                {
                    FileStream fs = File.OpenWrite(filePath);
                    Int32[] minValue = new Int32[mNrSignals];
                    Int32[] maxValue = new Int32[mNrSignals];
                    Int32[] offset = new Int32[mNrSignals];
                    int maxLimit = (1 << (ANrBits - 1)) - 1;
                    int minLimit = -maxLimit;
                    Int32 i, j, v;
                    int nOutOfRange = 0;

                    // determin min & max
                    for (j = 0; j < mNrSignals; ++j)
                    {
                        minValue[j] = Int32.MaxValue;
                        maxValue[j] = Int32.MinValue;
                        offset[j] = 0;
                    }

                    for (j = 0; j < mNrSignals; ++j)
                    {
                        Int32[] array = mSignals[j].mValues;

                        for (i = 0; i < mNrSamples; ++i)
                        {
                            v = array[i];
                            if (v < minValue[j]) minValue[j] = v;
                            if (v > maxValue[j]) maxValue[j] = v;
                        }
                    }
                    string lineInt = "channels ";
                    string lineFloat = "channels mV ";
                    int min, max, mean;
                    // calculate offset if out of range 
                    for (j = 0; j < mNrSignals; ++j)
                    {
                        min = minValue[j];
                        max = maxValue[j];
                        if (min <= minLimit || max >= maxLimit)
                        {
                            mean = (min + max) / 2;
                            offset[j] = mean;
                            min -= mean;
                            max -= mean;
                        }

                        lineInt += j.ToString() + "(" + min.ToString() + "<" + max.ToString() + ") ";
                        lineFloat += j.ToString() + "(" + (min * mSampleUnitA).ToString("0.000") + "<" + (max * mSampleUnitA).ToString("0.000") + ") ";

                    }
                    CProgram.sLogLine("Saving RAW DVX ECG to " + name + " " + mNrSignals.ToString() + " signals, " + mNrSamples.ToString() + " samples, "
                        + mSampleUnitA.ToString("0.0000") + " mV" + mSampleUnitT.ToString("0.0000") + " sec");
                    CProgram.sLogLine(lineInt);
                    CProgram.sLogLine(lineFloat);
                    // save data to file


                    for (i = 0; i < mNrSamples; ++i)
                    {
                        if (AbAddLOff)
                        {
                            if (ANrBits > 8)
                            {
                                if (ANrBits > 16)
                                {
                                    if (ANrBits > 24)
                                    {
                                        fs.WriteByte((byte)0);
                                    }
                                    fs.WriteByte((byte)0);
                                }
                                fs.WriteByte((byte)0);
                            }
                            fs.WriteByte((byte)(0));
                        }
                        for (j = 0; j < mNrSignals; ++j)
                        {
                            v = mSignals[j].mValues[i] - offset[j];
                            if (v < minLimit)
                            {
                                v = minLimit;
                                ++nOutOfRange;
                            }
                            else if (v > maxLimit)
                            {
                                v = maxLimit;
                                ++nOutOfRange;
                            }


                            if (ANrBits > 8)
                            {
                                if (ANrBits > 16)
                                {
                                    if (ANrBits > 24)
                                    {
                                        fs.WriteByte((byte)((v >> 24) & 0x00FF));
                                    }
                                    fs.WriteByte((byte)((v >> 16) & 0x00FF));
                                }
                                fs.WriteByte((byte)((v >> 8) & 0x00FF));
                            }
                            fs.WriteByte((byte)(v & 0x00FF));

                        }
                    }
                    fs.Close();
                    if (nOutOfRange > 0)
                    {

                        CProgram.sLogError("Save DVX RAW ECG to " + name + " " + nOutOfRange.ToString() + " points  out of range");
                        if (AbPromptContinue)
                        {
                            bOk = CProgram.sbAskYesNo("Save DVX RAW ECG to " + name, nOutOfRange.ToString() + " points  out of range, Continue?");
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Write MIT Header error " + filePath, ex);
                    bOk = false;
                }
            }
            return bOk;
        }

        public bool mbSaveDvxRawScaled(string AFilePath, bool AbAddLOff, UInt16 ANrBits, double AScale, bool AbPromptContinue)
        {
            bool bOk = AFilePath != null && AFilePath.Length > 0;

            if (bOk && mNrSignals > 0 && mNrSamples > 0 && mSignals != null)
            {
                string filePath = Path.ChangeExtension(AFilePath, CDvxEvtRec._cEcgExtension);
                string name = Path.GetFileNameWithoutExtension(filePath);

                try
                {
                    FileStream fs = File.OpenWrite(filePath);
                    Int32[] minValue = new Int32[mNrSignals];
                    Int32[] maxValue = new Int32[mNrSignals];
                    Int32[] offset = new Int32[mNrSignals];
                    int maxLimit = (1 << (ANrBits - 1)) - 1;
                    int minLimit = -maxLimit;
                    Int32 i, j, v;
                    int nOutOfRange = 0;
                    double unitA = mSampleUnitA / AScale;

                    // determin min & max
                    for (j = 0; j < mNrSignals; ++j)
                    {
                        minValue[j] = Int32.MaxValue;
                        maxValue[j] = Int32.MinValue;
                        offset[j] = 0;
                    }

                    for (j = 0; j < mNrSignals; ++j)
                    {
                        Int32[] array = mSignals[j].mValues;

                        for (i = 0; i < mNrSamples; ++i)
                        {
                            v = (int)(array[i] * AScale);
                            if (v < minValue[j]) minValue[j] = v;
                            if (v > maxValue[j]) maxValue[j] = v;
                        }
                    }
                    string lineInt = "channels ";
                    string lineFloat = "channels mV ";
                    int min, max, mean;
                    // calculate offset if out of range 
                    for (j = 0; j < mNrSignals; ++j)
                    {
                        min = minValue[j];
                        max = maxValue[j];
                        if (min <= minLimit || max >= maxLimit)
                        {
                            mean = (min + max) / 2;
                            offset[j] = mean;
                            min -= mean;
                            max -= mean;
                        }

                        lineInt += j.ToString() + "(" + min.ToString() + "<" + max.ToString() + ") ";
                        lineFloat += j.ToString() + "(" + (min * unitA).ToString("0.000") + "<" + (max * unitA).ToString("0.000") + ") ";

                    }
                    CProgram.sLogLine("Saving RAW DVX ECG to " + name + " " + mNrSignals.ToString() + " signals, " + mNrSamples.ToString() + " samples, "
                        + mSampleUnitA.ToString("0.0000") + " mV" + mSampleUnitT.ToString("0.0000") + " sec");
                    CProgram.sLogLine(lineInt);
                    CProgram.sLogLine(lineFloat);
                    // save data to file

                    for (i = 0; i < mNrSamples; ++i)
                    {
                        if (AbAddLOff)
                        {
                            if (ANrBits > 8)
                            {
                                if (ANrBits > 16)
                                {
                                    if (ANrBits > 24)
                                    {
                                        fs.WriteByte((byte)0);
                                    }
                                    fs.WriteByte((byte)0);
                                }
                                fs.WriteByte((byte)0);
                            }
                            fs.WriteByte((byte)0);
                        }
                        for (j = 0; j < mNrSignals; ++j)
                        {
                            v = (int)(mSignals[j].mValues[i] * AScale) - offset[j];
                            if (v < minLimit)
                            {
                                v = minLimit;
                                ++nOutOfRange;
                            }
                            else if (v > maxLimit)
                            {
                                v = maxLimit;
                                ++nOutOfRange;
                            }


                            if (ANrBits > 8)
                            {
                                if (ANrBits > 16)
                                {
                                    if (ANrBits > 24)
                                    {
                                        fs.WriteByte((byte)((v >> 24) & 0x00FF));
                                    }
                                    fs.WriteByte((byte)((v >> 16) & 0x00FF));
                                }
                                fs.WriteByte((byte)((v >> 8) & 0x00FF));
                            }
                            fs.WriteByte((byte)(v & 0x00FF));

                        }
                    }
                    fs.Close();
                    if (nOutOfRange > 0)
                    {

                        CProgram.sLogError("Save DVX RAW ECG to " + name + " " + nOutOfRange.ToString() + " points  out of range");
                        if (AbPromptContinue)
                        {
                            bOk = CProgram.sbAskYesNo("Save DVX RAW ECG to " + name, nOutOfRange.ToString() + " points  out of range, Continue?");
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Write MIT Header error " + filePath, ex);
                    bOk = false;
                }
            }
            return bOk;
        }


        private bool mbReadHeaderFile(string AFilePath)
        {
            string fileName = Path.ChangeExtension(AFilePath, "hea");
            bool bOk = false;
            String line;

            // should already be mClear();
            try
            {
                StreamReader sr = new StreamReader(fileName);

                line = sr.ReadLine();
                if (line != null)
                {
                    // Intricon Sirona header:
                    // 76A0002141_172298_20160711155932 1 256 0 15:59:32 11/07/2016
                    // 76A0002141_172298_20160711155932.dat 311 102.4 10 0
                    // # Event 16 at 1468245572000 Automatic - ARRHYTHMIA_AF
                    // # PRE 30 POST 30

                    // first line record: <name> <nr signals> <sample frequency> [<n samples per signal>] <base time><date> 
                    // second line Signal specification: < file name > < format > < ADC gain > < ADC resolution(bits) > < ADC zero >
                    // aftdb-learning-set header:
                    // n01 2 128 7680  1:28:24
                    // n01.dat 16 142.045/mV 0 0 -16 -23572 0 ECG
                    // n01.dat 16 143.062/mV 0 0 -1 11633 0 ECG

                    // aftdb-test-set-a header:
                    // a02 2 128 7680  9:22:02
                    // a02.dat 16 155.039/mV 0 0 188 -4718 0 ECG
                    // a02.dat 16 186.916/mV 0 0 -83 30352 0 ECG
                    // aftdb-test-set-a header:
                    // b02 2 128 7680
                    // b02.dat 16 202.429/mV 0 0 -26 -23886 0 ECG
                    // b02.dat 16 202.429/mV 0 0 -1 -12302 0 ECG

                    // mitdb header:
                    // 103 2 360 650000
                    // 103.dat 212 200 11 1024 949 11457 0 MLII
                    // 103.dat 212 200 11 1024 1034 -31983 0 V2
                    // # -1 M 742 654 x1
                    // # Diapres, Xyloprim

                    //first line record: <name> <nr signals> <sample frequency> [<n samples per signal>] <base time><date> 
                    var parts = line.Split(' ');
                    int nParts = parts.Count();
                    if (nParts < 4)
                    {
                        bOk = false;
                    }
                    else
                    {
                        DateTime baseDT;
                        int iParts;

                        if (mBaseUTC == DateTime.MinValue)
                        {
                            if (mStudyStartRecUTC == DateTime.MinValue)
                            {
                                mBaseUTC = DateTime.UtcNow; // unknown start time use now
                            }
                            else
                            {
                                mBaseUTC = mStudyStartRecUTC;  // unkown start time use from json file
                            }
                        }
                        bOk = ushort.TryParse(parts[1], out mNrSignals);
                        bOk &= uint.TryParse(parts[2], out mSampleFrequency);
                        bOk &= uint.TryParse(parts[3], out mNrSamplesPerSignal);

                        iParts = 4;
                        for (iParts = 4; iParts < nParts; ++iParts)
                        {
                            string s = parts[iParts];

                            if (s.Contains(":"))  // find the time
                            {
                                string format = "H:m:s";    // "HH:mm:ss";

                                if (++iParts < nParts)
                                {
                                    s += " " + parts[iParts];
                                    format += " d/M/yyyy";   //" dd/MM/yyyy";
                                }
                                if (DateTime.TryParseExact(s, format, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out baseDT))
                                //if ( DateTime.TryParseExact( s , out baseDT)) fixed format, goes wrong in month date in us time settings
                                {
                                    mBaseUTC = DateTime.SpecifyKind(baseDT, DateTimeKind.Utc);    // use header time as base time
                                    if (mBaseUTC > DateTime.MinValue) mBaseUTC = DateTime.SpecifyKind(mBaseUTC.AddMinutes(-mTimeZoneOffsetMin), DateTimeKind.Utc);  // correct for timezone

                                    mbReadDT = true;    // UTC time with given TimeZone
                                }
                                break;
                            }
                        }

                        mEventUTC = mBaseUTC;
                        mEventTimeSec = 0.0F;
                        if (bOk)
                        {
                            if (mSampleFrequency == 1)
                            {
                                mSampleFrequency = 256; // fix bug in Sirona, some devices start to give 1
                                CProgram.sLogLine("HEA Sample freq 1 => 256 in file " + fileName);
                            }
                            if (mSampleFrequency > 0)
                            {
                                mSampleUnitT = 1.0F / mSampleFrequency;
                            }
                        }
                    }
                }
                if (bOk)
                {
                    line = sr.ReadLine();
                    if (line == null || line.Length < 4)
                    {
                        line = sr.ReadLine();
                    }
                    if (line != null)
                    {
                        //second line Signal specification: < file name > < format > < ADC gain >[/<unit] < ADC resolution(bits) > < ADC zero >

                        var parts = line.Split(' ');
                        int nParts = parts.Count();
                        if (nParts < 5)
                        {
                            bOk = false;
                        }
                        else
                        {
                            bOk = ushort.TryParse(parts[1], out mDataFormat);
                            string s = parts[2];
                            int iS = s.IndexOf('(');
                            if (iS >= 0)
                            {                       //  123.45(-2345)/mV -> remove after (
                                string s2 = s;
                                s2 = s2.Remove(0, iS + 1);
                                s = s.Substring(0, iS);
                            }
                            iS = s.IndexOf('/');
                            if (iS >= 0)    //  123.45/mV -> remove after /
                            {
                                string s2 = s;
                                s2 = s2.Remove(0, iS + 1);
                                s = s.Substring(0, iS);
                            }
                            s = CProgram.sCorrectFloatToProg(DFloatDecimal.Dot, s);
                            bOk &= float.TryParse(s, out mAdcGain);
                            bOk &= ushort.TryParse(parts[3], out mAdcBits);
                            bOk &= int.TryParse(parts[4], out mAdcZero);
                            if (bOk)
                            {
                                if (mAdcGain < -0.001F || mAdcGain > 0.001F)
                                {
                                    mSampleUnitA = 1.0F / mAdcGain;
                                }
                            }
                        }

                    }
                }
                if (bOk)
                {
                    line = sr.ReadLine();
                    if (line != null)
                    {
                        // does not work because it gives a non valid time

                        // # Event 16 at 1468267876000 Automatic - ARRHYTHMIA_AF 
                        /*                        var parts = line.Split(' ');
                                                if (parts.Count() < 5 || parts[1].CompareTo( "Event" ) != 0 )
                                                {
                                                    bOk = false;
                                                }
                                                else
                                                {
                                                    int nrEvent;
                                                    long eventTicks;

                                                    bOk = int.TryParse(parts[2], out nrEvent);
                                                    bOk &= long.TryParse(parts[4], out eventTicks);
                                                    //bOk &= int.TryParse(parts[4], out mAdcZero);
                                                    if (bOk)
                                                    {
                                                        DateTime eventTime = new DateTime(eventTicks);
                                                        DateTime dt = new DateTime(mBaseDT.Year, mBaseDT.Month, mBaseDT.Day, eventTime.Hour, eventTime.Minute, eventTime.Second);

                                                        TimeSpan ts = new TimeSpan( eventTicks / 10 );


                                                        if ( eventTime.Hour < mBaseDT.Hour)
                                                        {
                                                            dt.AddDays(1);
                                                        }
                        //                                mbSetEventDT(dt);
                                                     }
                                                }
                        /**/
                    }
                }
                sr.Close();

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read error " + AFilePath, ex);
                bOk = false;
            }
            finally
            {
            }

            return bOk;
        }

        private bool mbReadDataFile311(string AFilePath)    //data= s0[0], s1[1] .... s0[n]s1[n]
        {
            string fileName = Path.ChangeExtension(AFilePath, "dat");
            bool bOk = false;

            try
            {
                FileStream fs = File.OpenRead(fileName);
                int fileSize = (int)fs.Length;

                if (fileSize > 0 && mNrSignals > 0)
                {
                    uint nLoop = (uint)fileSize / 4;
                    uint maxIndex = nLoop * 3;
                    mNrSamples = maxIndex / mNrSignals;


                    if (mAdcBits != 10)
                    {
                        int i = mAdcBits;
                    }
                    //                   if (mNrSamplesPerSignal > 0 && mNrSamplesPerSignal < mNrSamples)
                    {
                        //                        mNrSamples = mNrSamplesPerSignal;   // use nr samples in signal line (probably more signals exist
                    }

                    if (mbCreateSignals(mNrSignals)
                        && mbCreateAllSignalData(mNrSamples))
                    {
                        uint buffer = 0;
                        uint index = 0;
                        uint i;
                        int b;
                        int mask = (1 << mAdcBits) - 1;
                        uint flag = (uint)(1 << (mAdcBits - 1));
                        int neg = (int)(0xFFFFFFFF << mAdcBits);

                        for (i = 0; i < nLoop; ++i)
                        {
                            buffer = 0;     // read 4 bytes = 3 samples
                            b = fs.ReadByte();            // read b0-b7
                            if (b >= 0) { buffer = (uint)(b & 0x0FF); }
                            b = fs.ReadByte();             // raed b8-b15
                            if (b >= 0) { buffer |= (uint)((b << 8) & 0x0FF00); }
                            b = fs.ReadByte();              // read b16-b23
                            if (b >= 0) { buffer |= (uint)((b << 16) & 0x0FF0000); }
                            b = fs.ReadByte();              // read b24-b31
                            if (b >= 0) { buffer |= (uint)((b << 24) & 0x0FF000000); }
                            b = (int)(buffer & mask);     // first sample out of byte[4]
                            if ((b & flag) != 0) { b |= neg; }
                            mSignals[index % mNrSignals].mAddValue(b);

                            if (++index == maxIndex)
                            {
                                break;
                            }
                            buffer >>= mAdcBits;
                            b = (int)(buffer & mask);     // second sample out of byte[4]
                            if ((b & flag) != 0) { b |= neg; }
                            mSignals[index % mNrSignals].mAddValue(b);

                            if (++index == maxIndex)
                            {
                                break;
                            }
                            buffer >>= mAdcBits;
                            b = (int)(buffer & mask);     // third sample out of byte[4]
                            if ((b & flag) != 0) { b |= neg; }
                            mSignals[index % mNrSignals].mAddValue(b);

                            if (++index == maxIndex)
                            {
                                break;
                            }
                        }

                        bOk = index == maxIndex;
                    }
                }
                fs.Close();

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read error " + AFilePath, ex);
                bOk = false;
            }
            finally
            {
            }

            return bOk;
        }
        private bool mbReadDataFile311Sequencial(string AFilePath) //data= s0[0], s0[1] .... s0[n]s1[0],s1[1]....s1[n]
        {
            string fileName = Path.ChangeExtension(AFilePath, "dat");
            bool bOk = false;

            try
            {
                FileStream fs = File.OpenRead(fileName);
                int fileSize = (int)fs.Length;

                if (fileSize > 0 && mNrSignals > 0)
                {
                    uint nLoop = (uint)fileSize / mNrSignals / 4;
                    mNrSamples = nLoop * 3;

                    //                   if (mNrSamplesPerSignal > 0 && mNrSamplesPerSignal < mNrSamples)
                    {
                        //                        mNrSamples = mNrSamplesPerSignal;   // use nr samples in signal line (probably more signals exist
                    }
                    if (mAdcBits != 10)
                    {
                        int i = mAdcBits;
                    }


                    if (mbCreateSignals(mNrSignals)
                        && mbCreateAllSignalData(mNrSamples))
                    {
                        uint buffer = 0;
                        uint index = 0;
                        uint i;
                        int b;
                        int mask = (1 << mAdcBits) - 1;
                        uint flag = (uint)(1 << (mAdcBits - 1));
                        int neg = (int)(0xFFFFFFFF << mAdcBits);

                        for (ushort iSignal = 0; iSignal < mNrSignals; ++iSignal)
                        {
                            CSignalData sd = mSignals[iSignal];

                            for (i = 0; i < nLoop; ++i)
                            {
                                buffer = 0;     // read 4 bytes = 3 samples
                                b = fs.ReadByte();            // read b0-b7
                                if (b >= 0) { buffer = (uint)(b & 0x0FF); }
                                b = fs.ReadByte();             // raed b8-b15
                                if (b >= 0) { buffer |= (uint)((b << 8) & 0x0FF00); }
                                b = fs.ReadByte();              // read b16-b23
                                if (b >= 0) { buffer |= (uint)((b << 16) & 0x0FF0000); }
                                b = fs.ReadByte();              // read b24-b31
                                if (b >= 0) { buffer |= (uint)((b << 24) & 0x0FF000000); }
                                b = (int)(buffer & mask);     // first sample out of byte[4]
                                if ((b & flag) != 0) { b |= neg; }
                                sd.mAddValue(b);
                                if (++index == mNrSamples)
                                {
                                    break;
                                }
                                buffer >>= mAdcBits;
                                b = (int)(buffer & mask);     // second sample out of byte[4]
                                if ((b & flag) != 0) { b |= neg; }
                                sd.mAddValue(b);
                                if (++index == mNrSamples)
                                {
                                    break;
                                }
                                buffer >>= mAdcBits;
                                b = (int)(buffer & mask);     // third sample out of byte[4]
                                if ((b & flag) != 0) { b |= neg; }
                                sd.mAddValue(b);
                                if (++index == mNrSamples)
                                {
                                    break;
                                }
                            }

                        }
                        bOk = index == mNrSamples * mNrSignals;
                    }
                }
                fs.Close();

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read error " + AFilePath, ex);
                bOk = false;
            }
            finally
            {
            }

            return bOk;
        }
        private bool mbReadDataFile212(string AFilePath)
        {
            string fileName = Path.ChangeExtension(AFilePath, "dat");
            bool bOk = false;

            try
            {
                FileStream fs = File.OpenRead(fileName);
                int fileSize = (int)fs.Length;

                if (fileSize > 0)
                {
                    int adcBits = mAdcBits;
                    int shiftBits = 12;
                    uint nLoop = (uint)fileSize / 3;
                    mNrSamples = nLoop * 2;
                    nLoop *= 2;

                    if (mNrSamplesPerSignal > 0 && mNrSamplesPerSignal < mNrSamples)
                    {
                        mNrSamples = mNrSamplesPerSignal;   // use nr samples in signal line (probably more signals exist
                    }
                    if (adcBits == 0)
                    {
                        adcBits = 12;
                    }
                    if (mAdcBits != 12)
                    {
                        int i = mAdcBits;
                    }
                    if (mbCreateSignals(2)
                        && mbCreateAllSignalData(mNrSamples))
                    {
                        uint buffer = 0;
                        uint index = 0;
                        uint i;
                        int b;
                        int mask = (1 << adcBits) - 1;
                        uint flag = (uint)(1 << (adcBits - 1));
                        int neg = (int)(0xFFFFFFFF << adcBits);

                        CSignalData sd1 = mSignals[0];
                        CSignalData sd2 = mSignals[1];

                        for (i = 0; i < nLoop; ++i)
                        {
                            buffer = 0;
                            // read 3 bytes = 2 samples
                            b = fs.ReadByte();            // read b0-b7
                            if (b >= 0)
                            {
                                buffer = (uint)(b & 0x0FF);
                            }
                            b = fs.ReadByte();             // raed b8-b15  (b11-b8 sample2)(b11-b8 sample1)
                            if (b >= 0)
                            {
                                buffer |= (uint)((b << 8) & 0x0FF00);
                            }

                            b = (int)(buffer & mask);     // first sample out of byte[2]    
                            if ((b & flag) != 0)
                            {
                                b |= neg;
                            }
                            sd1.mAddValue(b);
                            if (++index == mNrSamples)
                            {
                                break;
                            }
                            buffer >>= shiftBits;
                            buffer <<= 8;

                            b = fs.ReadByte();              // read b7-0 sample 2
                            if (b >= 0)
                            {
                                buffer |= (uint)(b & 0x00FF);
                            }

                            b = (int)(buffer & mask);     // second sample out of byte[3]
                            if ((b & flag) != 0)
                            {
                                b |= neg;
                            }
                            sd2.mAddValue(b);               // second sample is for 2 channel
                        }
                        bOk = index == mNrSamples;
                    }
                }
                fs.Close();

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read error " + AFilePath, ex);
                bOk = false;
            }
            finally
            {
            }

            return bOk;
        }
        private bool mbReadDataFile8(string AFilePath)
        {
            string fileName = Path.ChangeExtension(AFilePath, "dat");
            bool bOk = false;

            try
            {
                FileStream fs = File.OpenRead(fileName);

                int fileSize = (int)fs.Length;

                if (fileSize == 0)
                {
                    CProgram.sLogError("FileSize 0 for " + AFilePath);
                }
                else
                {
                    uint nLoop = (uint)(fileSize / (1 * mNrSignals));
                    mNrSamples = nLoop * 1;

                    if (mNrSamplesPerSignal > 0 && mNrSamplesPerSignal < mNrSamples)
                    {
                        mNrSamples = mNrSamplesPerSignal;   // use nr samples in signal line (probably more signals exist
                    }
                    if (mAdcBits != 8)
                    {
                        int i = mAdcBits;
                    }

                    if (mbCreateSignals(mNrSignals)
                        && mbCreateAllSignalData(mNrSamples))
                    {
                        Int16 buffer = 0;
                        uint index = 0;
                        int i, b;

                        for (i = 0; i < nLoop; ++i)
                        {
                            for (int j = 0; j < mNrSignals; ++j)
                            {
                                buffer = 0;
                                //                            // read 1 byte -> 1 sample
                                b = fs.ReadByte();            // read b0-b7
                                if (b >= 0)
                                {
                                    buffer = (Int16)(b & 0x0FF);
                                }
                                mSignals[j].mAddValue(buffer);
                            }
                            if (++index == mNrSamples)
                            {
                                break;
                            }
                        }
                        bOk = index == mNrSamples;
                    }
                }
                fs.Close();

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read error " + AFilePath, ex);
                bOk = false;
            }
            finally
            {
            }

            return bOk;
        }

        private bool mbReadDataFile16(string AFilePath)
        {
            string fileName = Path.ChangeExtension(AFilePath, "dat");
            bool bOk = false;

            try
            {
                FileStream fs = File.OpenRead(fileName);

                int fileSize = (int)fs.Length;

                if (fileSize == 0)
                {
                    CProgram.sLogError("FileSize 0 for " + AFilePath);
                }
                else
                {
                    uint nLoop = (uint)(fileSize / (2 * mNrSignals));
                    mNrSamples = nLoop * 1;

                    mNrSamplesPerSignal = mNrSamples;
                    /* load all of file , needed for mearge data files                    
                     * if (mNrSamplesPerSignal > 0 && mNrSamplesPerSignal < mNrSamples)
                                       {
                                            mNrSamples = mNrSamplesPerSignal;   // use nr samples in signal line (probably more signals exist
                                        }
                    */
                    if (mAdcBits != 16)
                    {
                        int i = mAdcBits;
                    }
                    if (mbCreateSignals(mNrSignals)
                        && mbCreateAllSignalData(mNrSamples))
                    {
                        Int16 buffer = 0;
                        uint index = 0;
                        int i, b;

                        for (i = 0; i < nLoop; ++i)
                        {
                            for (int j = 0; j < mNrSignals; ++j)
                            {
                                buffer = 0;                   // read 2 bytes -> 1 sample
                                // 
                                b = fs.ReadByte();            // read b0-b7
                                if (b >= 0)
                                {
                                    buffer = (Int16)(b & 0x0FF);
                                }
                                b = fs.ReadByte();             // raed b8-b15
                                if (b >= 0)
                                {
                                    buffer |= (Int16)((b << 8) & 0x0FF00);
                                }
                                mSignals[j].mAddValue(buffer);  // auto sign extend
                            }
                            if (++index == mNrSamples)
                            {
                                break;
                            }
                        }
                        bOk = index == mNrSamples;
                    }
                }
                fs.Close();

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read error " + AFilePath, ex);
                bOk = false;
            }
            finally
            {
            }

            return bOk;
        }

        private bool mbReadDataFile24(string AFilePath)
        {
            string fileName = Path.ChangeExtension(AFilePath, "dat");
            bool bOk = false;

            try
            {
                FileStream fs = File.OpenRead(fileName);

                int fileSize = (int)fs.Length;
                bool bSignExtend = true;

                if (fileSize == 0)
                {
                    CProgram.sLogError("FileSize 0 for " + AFilePath);
                }
                else
                {
                    uint nLoop = (uint)(fileSize / (3 * mNrSignals));
                    mNrSamples = nLoop * 1;

                    if (mNrSamplesPerSignal > 0 && mNrSamplesPerSignal < mNrSamples)
                    {
                        mNrSamples = mNrSamplesPerSignal;   // use nr samples in signal line (probably more signals exist
                    }
                    if (mAdcBits != 24)
                    {
                        int i = mAdcBits;
                    }

                    if (mbCreateSignals(mNrSignals)
                        && mbCreateAllSignalData(mNrSamples))
                    {
                        Int32 buffer = 0;
                        uint index = 0;
                        int i, b;

                        for (i = 0; i < nLoop; ++i)
                        {
                            for (int j = 0; j < mNrSignals; ++j)
                            {
                                buffer = 0;                   // read 3 bytes -> 1 sample
                                // 
                                b = fs.ReadByte();            // read b0-b7
                                if (b >= 0)
                                {
                                    buffer = (Int32)(b & 0x0FF);
                                }
                                b = fs.ReadByte();             // raed b8-b15
                                if (b >= 0)
                                {
                                    buffer |= (Int32)((b << 8) & 0x0FF00);
                                }
                                b = fs.ReadByte();             // raed b16-b23
                                if (b >= 0)
                                {
                                    buffer |= (Int32)((b << 16) & 0x0FF0000);
                                }
                                if (bSignExtend)
                                {
                                    if ((buffer & 0x00800000) != 0)  // sign extend
                                    {
                                        unchecked
                                        {
                                            buffer |= (Int32)0xFF000000L;
                                        }
                                    }
                                }
                                mSignals[j].mAddValue(buffer);
                            }
                            if (++index == mNrSamples)
                            {
                                break;
                            }
                        }
                        bOk = index == mNrSamples;
                    }
                }
                fs.Close();

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read error " + AFilePath, ex);
                bOk = false;
            }
            finally
            {
            }

            return bOk;
        }

        private bool mbReadDataFile24MSB(string AFilePath)
        {
            string fileName = Path.ChangeExtension(AFilePath, "dat");
            bool bOk = false;

            try
            {
                FileStream fs = File.OpenRead(fileName);

                int fileSize = (int)fs.Length;
                bool bSignExtend = true;

                if (fileSize == 0)
                {
                    CProgram.sLogError("FileSize 0 for " + AFilePath);
                }
                else
                {
                    uint nLoop = (uint)(fileSize / (3 * mNrSignals));
                    mNrSamples = nLoop * 1;

                    if (mNrSamplesPerSignal > 0 && mNrSamplesPerSignal < mNrSamples)
                    {
                        mNrSamples = mNrSamplesPerSignal;   // use nr samples in signal line (probably more signals exist
                    }

                    if (mAdcBits != 24)
                    {
                        int i = mAdcBits;
                    }
                    if (mbCreateSignals(mNrSignals)
                        && mbCreateAllSignalData(mNrSamples))
                    {
                        Int32 buffer = 0;
                        uint index = 0;
                        int i, b;

                        for (i = 0; i < nLoop; ++i)
                        {
                            for (int j = 0; j < mNrSignals; ++j)
                            {
                                buffer = 0;                   // read 3 bytes -> revese -> 1 sample
                                // 
                                b = fs.ReadByte();            // read b0-b7
                                if (b >= 0)
                                {
                                    buffer |= (Int32)((b << 16) & 0x0FF0000);
                                }
                                b = fs.ReadByte();             // raed b8-b15
                                if (b >= 0)
                                {
                                    buffer |= (Int32)((b << 8) & 0x0FF00);
                                }
                                b = fs.ReadByte();             // raed b16-b23
                                if (b >= 0)
                                {
                                    buffer = (Int32)(b & 0x0FF);
                                }
                                if (bSignExtend)
                                {
                                    if ((buffer & 0x00800000) != 0)  // sign extend
                                    {
                                        unchecked
                                        {
                                            buffer |= (Int32)0xFF000000L;
                                        }
                                    }
                                }
                                mSignals[j].mAddValue(buffer);
                            }
                            if (++index == mNrSamples)
                            {
                                break;
                            }
                        }
                        bOk = index == mNrSamples;
                    }
                }
                fs.Close();

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read error " + AFilePath, ex);
                bOk = false;
            }
            finally
            {
            }

            return bOk;
        }

        public bool mbParseTokenForDTO(string ATokenString, out DateTimeOffset ArDTO)
        {
            // token = "Transmit Time": "2016-08-10T14:44:33+0200" 
            // toke.First does not give complete string
            // extract DateTimeOffset from last string by cutting up string
            bool bOk = false;
            int l;

            if (ATokenString != null && (l = ATokenString.Length) > 0)
            {
                string s = ATokenString;
                int i;
                char[] c = { '\"', ' ' };

                if (s[l - 1] == '\"') s = s.TrimEnd(c);
                i = s.LastIndexOf('\"');
                if (i >= 0) s = s.Remove(0, i + 1);

                bOk = DateTimeOffset.TryParse(s, out ArDTO);
            }
            else
            {
                ArDTO = DateTimeOffset.MinValue;
            }
            return bOk;
        }

        public bool mbReadJsonValue(string AJsonText, string AName, out string ArValue)
        {
            bool bFound = false;
            string value = "";
            int lText, lName, iStart, iEnd;
            char c;

            if (AJsonText != null && (lText = AJsonText.Length) > 0
                && AName != null && (lName = AName.Length) > 0)
            {
                iStart = AJsonText.IndexOf("\"" + AName + "\"");
                if (iStart > 0)
                {
                    iStart += lName + 1;

                    while (++iStart < lText && AJsonText[iStart] != ':') ;

                    if (iStart < lText)
                    {
                        iEnd = 0;
                        while (++iStart < lText)
                        {
                            c = AJsonText[iStart];

                            if (c == '\"')
                            { // found string
                                iEnd = iStart;
                                while (++iEnd < lText && AJsonText[iEnd] != '\"') ;
                                ++iStart;   // after "
                                break;
                            }
                            else if (char.IsLetterOrDigit(c)
                                || ((c == '+' || c == '-' || c == '.' || c == ',') && iStart + 1 < lText && char.IsDigit(AJsonText[iStart + 1])))   // + or - belongs to float
                            {
                                iEnd = iStart;
                                while (++iEnd < lText)
                                {
                                    c = AJsonText[iEnd];
                                    bool b = char.IsLetterOrDigit(c)
                                        || c == '+' || c == '-' || c == 'e' || c == 'E' //read exponent 1.23e-7
                                        || ((c == '.' || c == ',') && iEnd + 1 < lText && char.IsDigit(AJsonText[iEnd + 1]));   // . or , belongs to float 

                                    if (false == b)
                                    {
                                        break;
                                    }
                                }
                                break;
                            }
                            else if (c == ',')
                            {
                                break;
                            }
                        }
                        if (iEnd > iStart)
                        {
                            value = AJsonText.Substring(iStart, iEnd - iStart);
                            bFound = true;
                        }
                    }
                }
            }
            ArValue = value;
            return bFound;
        }
        public bool mbReadJsonValueArray(string AJsonText, string AName, char AMergeChar, out string ArValue)
        {
            bool bFound = false;
            bool bFoundArray = false;
            string value = "";
            int lText, lName, iStart, iEnd;
            char c;

            if (AJsonText != null && (lText = AJsonText.Length) > 0
                && AName != null && (lName = AName.Length) > 0)
            {
                iStart = AJsonText.IndexOf("\"" + AName + "\"");
                if (iStart > 0)
                {
                    iStart += lName + 1;

                    while (++iStart < lText && AJsonText[iStart] != ':') ;

                    if (iStart < lText)
                    {
                        iEnd = 0;
                        while (++iStart < lText)
                        {
                            c = AJsonText[iStart];

                            if (c == '[')
                            { // found string array [] or [""] or ["", "", ....]
                                UInt32 nParts = 0;
                                bFoundArray = true;

                                while (++iStart < lText)
                                {
                                    c = AJsonText[iStart];
                                    if (c == ']')
                                    {
                                        // end array
                                        break;
                                    }
                                    if (c == '\"')
                                    { // found string and store parts directly in value
                                        if (++nParts > 1)
                                        {
                                            value += AMergeChar;
                                        }
                                        while (++iStart < lText)
                                        {
                                            c = AJsonText[iStart];
                                            if (c == '\t')
                                            {
                                                value += ' ';
                                            }
                                            else if (c == '"' || c < ' ')
                                            {
                                                // end string
                                                break;
                                            }
                                            else
                                            {
                                                value += c;
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                            else if (c == '\"')
                            { // found string
                                iEnd = iStart;
                                while (++iEnd < lText && AJsonText[iEnd] != '\"') ;
                                ++iStart;   // after "
                                break;
                            }
                            else if (char.IsLetterOrDigit(c)
                                || ((c == '+' || c == '-' || c == '.' || c == ',') && iStart + 1 < lText && char.IsDigit(AJsonText[iStart + 1])))   // + or - belongs to float
                            {
                                iEnd = iStart;
                                while (++iEnd < lText)
                                {
                                    c = AJsonText[iEnd];
                                    bool b = char.IsLetterOrDigit(c)
                                        || c == '+' || c == '-' || c == 'e' || c == 'E' //read exponent 1.23e-7
                                        || ((c == '.' || c == ',') && iEnd + 1 < lText && char.IsDigit(AJsonText[iEnd + 1]));   // . or , belongs to float 

                                    if (false == b)
                                    {
                                        break;
                                    }
                                }
                                break;
                            }
                            else if (c == ',')
                            {
                                break;
                            }
                        }
                        if (bFoundArray)
                        {
                            bFound = true;
                        }
                        else if (iEnd > iStart)
                        {
                            value = AJsonText.Substring(iStart, iEnd - iStart);
                            bFound = true;
                        }
                    }
                }
            }
            ArValue = value;
            return bFound;
        }

        public static string sCombineNames(string APatientLastName, string APatientMiddleInitials, string APatientFirstName)
        {
            string totalName = CProgram.sTrimString(APatientLastName);
            string firstName = CProgram.sTrimString(APatientFirstName);
            string middleName = CProgram.sTrimString(APatientMiddleInitials);
            bool bFirst = firstName != null && firstName.Length > 0;
            bool bMiddle = middleName != null && middleName.Length > 0;

            if (bFirst || bMiddle)
            {
                totalName += ", " + middleName + ", " + firstName;
            }
            return totalName;
        }

        public static string sGetConverterTag(DImportConverter AConverter)
        {
            switch (AConverter)
            {
                case DImportConverter.Unknown: return "";
                case DImportConverter.IntriconSironaZip: return "iZip";
                case DImportConverter.IntriconSironaMit: return "iHea";
                case DImportConverter.TZAeraScp: return "tScp";
                case DImportConverter.DV2Scp: return "dScp";
                case DImportConverter.DVX1: return "dvx1";
                case DImportConverter.DvxSpO: return "dSpO";
            }
            return "";
        }

        public static string sMakeManualName(DImportConverter AImportConverter, DImportMethod AImportMethod, string AFromFilePath)
        {
            string manualName = sGetConverterTag(AImportConverter);

            if (manualName != null && manualName.Length > 0) manualName += "-";

            string fromPath = Path.GetDirectoryName(AFromFilePath);
            int pos = fromPath.LastIndexOf('\\');
            if (pos > 0)
            {
                manualName += fromPath.Substring(pos + 1);    // if there is no name and manual use directory (dbase) name.
            }
            return manualName;
        }

        public void mCheckManualName(string AManualName)
        {
            if (mImportMethod == (UInt16)DImportMethod.Manual)
            {
                if (mPatientID.mbIsEmpty() && mPatientTotalName.mbIsEmpty())
                {
                    mPatientID.mbEncrypt(CProgram.sTrimString(AManualName));
                    string rem = mRecRemark.mDecrypt();

                    if (rem != null && rem.Length > 0)
                    {
                        rem += "\n";
                    }
                    rem += Path.GetFileNameWithoutExtension(mFileName);
                    mRecRemark.mbEncrypt(rem);
                }
                /*

                                string totalName = mPatientTotalName.mDecrypt();

                                if( totalName == null || totalName.Length == 0)
                                    {
                                        mPatientTotalName.mbEncrypt(AManualName);

                                    if( mPatientID.mbIsEmpty())
                                    {
                                        mPatientID.mbEncrypt(AManualName);
                                    }
                                }
                */
            }
        }

        public bool mbReadJsonFile(string AFilePath, bool AbWipeJson)
        {
            bool bOk = false;
            string jsonFile = Path.ChangeExtension(AFilePath, "json");

            string patientBirthDateString;
            string patientFirstName = "", patientMiddleInitials = "", patientLastName = "";
            bool bTimeZoneSet = false;
            string addRemark = "";

            //mEventTime = DateTime.MinValue;
            /*  "Device": {
                    "Device ID": "76A0002141"
              },
              "Patient": {
                                    "Patient ID": "172298",
                "Patient First Name": "Annelijn",
                "Patient Middle Initial": "",
                "Patient Last Name": "Mastenbroek",
                "Patient Date of Birth": "1987-06-01",
                "Physician Name": "",
                "Comment": ""
              },
              "Time": {
                "Procedure Start Time": "2016-07-11T16:15:01+0200",
                "Event type": "Automatic - ARRHYTHMIA_AF",
                "Strip Start Time": "2016-07-11T16:09:40+0200",
                "Strip Length": 60,
                "Transmit Time": "2016-07-11T16:20:26+0200"
              }
              */
            try
            {
                string jsonText = File.ReadAllText(jsonFile);
                string value;

                if (jsonText != null && jsonText.Length > 0)
                {
                    if (mbReadJsonValue(jsonText, "Device ID", out value))
                    {
                        mDeviceID = value;
                    }
                    if (mbReadJsonValue(jsonText, "Patient ID", out value))
                    {
                        mPatientID.mbEncrypt(CProgram.sTrimString(value));
                    }
                    if (mbReadJsonValue(jsonText, "Patient First Name", out value))
                    {
                        patientFirstName = value;
                    }
                    if (mbReadJsonValue(jsonText, "Patient Middle Initial", out value))
                    {
                        patientMiddleInitials = value;
                    }
                    if (mbReadJsonValue(jsonText, "Patient Last Name", out value))
                    {
                        patientLastName = value;
                    }
                    if (mbReadJsonValue(jsonText, "Patient Date of Birth", out value))
                    {
                        patientBirthDateString = value;
                        DateTime date;
                        if (DateTime.TryParse(patientBirthDateString, out date))
                        {
                            if (date.Year == 1900 && date.Month == 1 && date.Day == 1)
                            {
                                // default Sirona not valid
                                mPatientBirthDate.mClear();
                                mPatientBirthYear = 0;
                            }
                            else
                            {
                                mPatientBirthDate.mbEncryptDate(date);

                                mPatientBirthYear = (UInt16)(date.Year <= 1900 ? 0 : (date.Year > 2155 ? 0 : date.Year));
                            }
                        }
                    }
                    if (mbReadJsonValue(jsonText, "Physician Name", out value))
                    {
                        mPhysicianName.mbEncrypt(value);
                    }
                    if (mbReadJsonValue(jsonText, "Comment", out value))
                    {
                        mRecRemark.mbEncrypt(value);
                    }
                    if (mbReadJsonValue(jsonText, "Procedure Start Time", out value))
                    {
                        DateTimeOffset dt;

                        if (mbParseTokenForDTO(value, out dt))
                        {
                            mStudyStartRecUTC = dt.UtcDateTime;
                        }
                    }
                    if (mbReadJsonValue(jsonText, "Strip Start Time", out value))
                    {
                        DateTimeOffset dt;

                        if (mbParseTokenForDTO(value, out dt))
                        {
                            mbReadDT = true;
                            mBaseUTC = dt.UtcDateTime;
                            if (bTimeZoneSet == false)
                            {
                                mSetTimeZoneOffset(mBaseUTC, dt.DateTime);
                                bTimeZoneSet = true;
                                mbReadUTC = true;
                                mbReadTZ = true;
                            }
                        }
                    }
                    if (mbReadJsonValue(jsonText, "Transmit Time", out value))
                    {
                        DateTimeOffset dt;

                        if (mbParseTokenForDTO(value, out dt))
                        {
                            mTransmitUTC = dt.UtcDateTime;
                            if (bTimeZoneSet == false)
                            {
                                mSetTimeZoneOffset(mTransmitUTC, dt.DateTime);
                                bTimeZoneSet = true;
                                mbReadTZ = true;
                            }
                        }
                    }
                    if (mbReadJsonValue(jsonText, "Event type", out value))
                    {
                        mEventTypeString = value;
                    }
                    if (mbReadJsonValue(jsonText, "Strip Length", out value))
                    {
                        if (CProgram.sbParseFloat(value, ref mRecDurationSec))
                        {
                            int xxx = (int)mRecDurationSec;
                        }
                    }
                    if (mbReadJsonValueArray(jsonText, "Symptom", '+', out value)
                        || mbReadJsonValueArray(jsonText, "Symptoms", '+', out value)
                        )
                    {
                        if (value.Length > 0)
                        {
                            addRemark += "S=" + value;
                        }
                    }
                    if (mbReadJsonValueArray(jsonText, "Activity", '+', out value)
                        || mbReadJsonValueArray(jsonText, "Activities", '+', out value))
                    {
                        if (value.Length > 0)
                        {
                            if (addRemark.Length > 0)
                            {
                                addRemark += " ";
                            }
                            addRemark += "A=" + value;
                        }
                    }
                    if (mbReadJsonValue(jsonText, "Notes", out value))
                    {
                        if (value.Length > 0)
                        {
                            if (addRemark.Length > 0)
                            {
                                addRemark += " ";
                            }
                            addRemark += "Nt=" + value;
                        }
                    }
                }

                string totalName = CRecordMit.sCombineNames(patientLastName, patientMiddleInitials, patientFirstName);

                mPatientTotalName.mbEncrypt(totalName);
                bOk = mDeviceID != null && mEventTypeString != null;

                if (AbWipeJson)
                {
                    File.Delete(jsonFile);  // delete json file because it contains plain text patient name
                }
                if (addRemark.Length > 0)
                {
                    string oldRemark = mRecRemark.mDecrypt();
                    mRecRemark.mbEncrypt(oldRemark + addRemark + "\r\n");
                }
            }
            catch (Exception)
            {
                bOk = false;
                //                mLogError(e);
            }
            return bOk;
        }

        public bool mbReadJsonFileUsingJObject(string AFilePath, bool AbWipeJson)
        {
            bool bOk = false;
            string jsonFile = Path.ChangeExtension(AFilePath, "json");

            string patientBirthDateString;
            string patientFirstName = "", patientMiddleInitials = "", patientLastName = "";
            bool bTimeZoneSet = false;


            //mEventTime = DateTime.MinValue;
            /*  "Device": {
                    "Device ID": "76A0002141"
              },
              "Patient": {
                                    "Patient ID": "172298",
                "Patient First Name": "Annelijn",
                "Patient Middle Initial": "",
                "Patient Last Name": "Mastenbroek",
                "Patient Date of Birth": "1987-06-01",
                "Physician Name": "",
                "Comment": ""
              },
              "Time": {
                "Procedure Start Time": "2016-07-11T16:15:01+0200",
                "Event type": "Automatic - ARRHYTHMIA_AF",
                "Strip Start Time": "2016-07-11T16:09:40+0200",
                "Strip Length": 60,
                "Transmit Time": "2016-07-11T16:20:26+0200"
              }
              */
            try
            {
                JObject o1 = JObject.Parse(File.ReadAllText(jsonFile));
                JToken token1;

                if (o1.TryGetValue("Device", out token1))
                {
                    JEnumerable<JToken> childList = token1.Children();

                    foreach (JToken token in childList)
                    {
                        string path = token.Path;

                        if (token.First != null)
                        {
                            if (path.Contains("Device ID"))
                            {
                                mDeviceID = token.First.ToString();
                            }
                        }
                    }
                }
                if (o1.TryGetValue("Patient", out token1))
                {
                    JEnumerable<JToken> childList = token1.Children();

                    foreach (JToken token in childList)
                    {
                        string path = token.Path;

                        if (token.First != null)
                        {
                            if (path.Contains("Patient ID"))
                            {
                                mPatientID.mbEncrypt(CProgram.sTrimString(token.First.ToString()));
                            }
                            else if (path.Contains("Patient First Name"))
                            {
                                patientFirstName = token.First.ToString();
                            }
                            else if (path.Contains("Patient Middle Initial"))
                            {
                                patientMiddleInitials = token.First.ToString();
                            }
                            else if (path.Contains("Patient Last Name"))
                            {
                                patientLastName = token.First.ToString();
                            }
                            else if (path.Contains("Patient Date of Birth"))
                            {
                                patientBirthDateString = token.First.ToString();
                                DateTime date;
                                if (DateTime.TryParse(patientBirthDateString, out date))
                                {
                                    mPatientBirthDate.mbEncryptDate(date);
                                    mPatientBirthYear = (UInt16)date.Year;
                                }
                            }
                            else if (path.Contains("Physician Name"))
                            {
                                mPhysicianName.mbEncrypt(token.First.ToString());
                            }
                            else if (path.Contains("Comment"))
                            {
                                mRecRemark.mbEncrypt(token.First.ToString());
                            }
                        }
                    }
                }
                if (o1.TryGetValue("Time", out token1))
                {
                    JEnumerable<JToken> childList = token1.Children();

                    foreach (JToken token in childList)
                    {
                        string path = token.Path;

                        if (token.First != null)
                        {
                            if (path.Contains("Strip Start Time"))
                            {
                                DateTimeOffset dt;
                                string s = token.ToString();

                                if (mbParseTokenForDTO(s, out dt))
                                {
                                    mbReadDT = true;
                                    mBaseUTC = dt.UtcDateTime;
                                    if (bTimeZoneSet == false)
                                    {
                                        mSetTimeZoneOffset(mBaseUTC, dt.DateTime);
                                        bTimeZoneSet = true;
                                        mbReadUTC = true;
                                        mbReadTZ = true;

                                    }
                                }
                            }
                            if (path.Contains("Transmit Time"))
                            {
                                DateTimeOffset dt;
                                string s = token.ToString();

                                if (mbParseTokenForDTO(s, out dt))
                                {
                                    mTransmitUTC = dt.UtcDateTime;
                                    if (bTimeZoneSet == false)
                                    {
                                        mSetTimeZoneOffset(mTransmitUTC, dt.DateTime);
                                        bTimeZoneSet = true;
                                        mbReadTZ = true;
                                    }
                                }
                            }
                            else if (path.Contains("Event type"))
                            {
                                mEventTypeString = token.First.ToString();
                            }
                            else if (path.Contains("Strip Length"))
                            {
                                float.TryParse(token.First.ToString(), out mRecDurationSec);
                            }
                        }
                    }
                }
                mPatientTotalName.mbEncrypt(CRecordMit.sCombineNames(patientLastName, patientMiddleInitials, patientFirstName));
                bOk = mDeviceID != null && mEventTypeString != null;

                if (AbWipeJson)
                {
                    File.Delete(jsonFile);  // delete json file because it contains plain text patient name
                }
            }
            catch (Exception)
            {
                bOk = false;
                //                mLogError(e);
            }
            return bOk;
        }


        public string mGetJsonInfo()
        {
            DateTime dt = mBaseUTC;
            string s = "JSON{" + mPatientID.mDecrypt() + "  " + mPatientTotalName.mDecrypt() + " " + mPatientBirthDate.mDecryptToDate().ToString("yyyy/MM/dd")
                            + " }{ " + mEventTypeString + "  " + dt.ToString() + " O=" + mTimeZoneOffsetMin + "}";
            return s;
        }

        public uint mGetNrSamples()
        {
            return mNrSamples;
        }
        public float mGetSamplesTotalTime()
        {
            return mNrSamples * mSampleUnitT;
        }
        public DateTime mGetSampleUTC(uint AIndex)
        {
            DateTime dt = mBaseUTC.AddSeconds(AIndex * mSampleUnitT);

            return dt;
        }

        public Int32 mGetDataValue(ushort ASignalIndex, uint AIndex)
        {
            Int32 data = 0;

            if (ASignalIndex < mNrSignals)
            {
                CSignalData sd = mSignals[ASignalIndex];

                if (sd != null && AIndex < sd.mNrValues)
                {
                    data = sd.mValues[AIndex];
                }
            }
            return data;
        }
        public float mGetSampleValue(ushort ASignalIndex, uint AIndex)
        {
            float f = 0.0F;

            if (ASignalIndex < mNrSignals)
            {
                CSignalData sd = mSignals[ASignalIndex];

                if (sd != null && AIndex < sd.mNrValues)
                {
                    f = sd.mValues[AIndex] * mSampleUnitA;
                }
            }

            return f;
        }

        public float mGetSampleTime(uint AIndex)
        {
            float t = AIndex * mSampleUnitT;

            return t;
        }

        public float mGetSamplesMinValue(ushort ASignalIndex)
        {
            float min = 1e6F;

            if (ASignalIndex < mNrSignals)
            {
                CSignalData sd = mSignals[ASignalIndex];

                if (sd != null)
                {
                    min = sd.mMinValue * mSampleUnitA;
                }
            }
            return min;
        }

        public float mGetSamplesMaxValue(ushort ASignalIndex)
        {
            float max = -1e6F;

            if (ASignalIndex < mNrSignals)
            {
                CSignalData sd = mSignals[ASignalIndex];

                if (sd != null)
                {
                    max = sd.mMaxValue * mSampleUnitA;
                }
            }
            return max;
        }
        public bool mbWriteDataCsvFile(string AFilePath)
        {
            bool bOk = false;
            string fileName = Path.ChangeExtension(AFilePath, "csv");

            try
            {
                if (mNrSignals > 0)
                {
                    StreamWriter sw = new StreamWriter(fileName);

                    if (sw != null)
                    {
                        DateTime dt;
                        float v, f;
                        int sample;
                        string s1 = "";
                        string s2 = "";

                        for (int j = 0; j < mNrSignals; ++j)
                        {
                            s1 += "\tiCH" + j.ToString();
                            s2 += "\tCH" + j.ToString();
                        }

                        sw.WriteLine("sample\tTime" + s1 + s2);

                        for (uint i = 0; i < mNrSamples; ++i)
                        {
                            s1 = "";
                            s2 = "";
                            for (ushort j = 0; j < mNrSignals; ++j)
                            {
                                sample = mGetDataValue(j, i);
                                v = mGetSampleValue(j, i);

                                s1 += "\t" + sample.ToString();
                                s2 += "\tCH" + v.ToString();
                            }
                            f = i * mSampleUnitT;
                            dt = mGetSampleUTC(i);

                            sw.WriteLine(i.ToString() + "\t " + f.ToString() + "\t '" + dt.ToString("yyyy/MM/dd HH:mm:ss.ffff") + "\'" + s1 + s2);
                        }
                    }
                    sw.Close();
                }

            }
            catch (Exception)
            {
            }
            finally
            {
            }
            return bOk;
        }

        public bool mbReadFile(string AFilePath, UInt32 AInvChannels,
                                DateTime AReceiveTimeUTC, Int32 ADefaultTimeZoneOffsetMin, DMoveTimeZone AMoveTimeZone,
                                bool AbReadJSon, bool AbWipeJson, DImportConverter AImportConverter,
                                DImportMethod AImportMethod, string AManualName)
        {
            bool bOk = false;

            mFileName = Path.GetFileName(AFilePath);

            mTimeZoneOffsetMin = ADefaultTimeZoneOffsetMin;    // use default, can be overwritten by read of json file
            mReceivedUTC = AReceiveTimeUTC;
            mImportConverter = (UInt16)AImportConverter;
            mImportMethod = (UInt16)AImportMethod;

            mInvertChannelsMask = 0;
            if (AbReadJSon)
            {
                mbReadJsonFile(AFilePath, AbWipeJson);  // if present read <file>.json for patient info
            }
            if (mbReadHeaderFile(AFilePath))
            {
                switch (mDataFormat)
                {
                    case 8:
                        bOk = mbReadDataFile8(AFilePath);
                        break;
                    case 16:
                        bOk = mbReadDataFile16(AFilePath);
                        break;
                    case 24:
                        bOk = mbReadDataFile24(AFilePath);
                        break;
                    //case 32:                        break;
                    //case 61:                        break;
                    //case 80:                        break;
                    //case 160:                        break;
                    case 212:
                        bOk = mbReadDataFile212(AFilePath);
                        break;
                    //case 310:                        break;
                    case 311:
                        bOk = mbReadDataFile311(AFilePath);
                        break;
                    default:
                        bOk = false;    // file format is not supported

                        break;
                }

                if (mSignalLabels == null || mSignalLabels.Length == 0)
                {
                    if (mFileName.StartsWith("HR-"))
                    {
                        mSignalLabels = "HR";
                        mSignalUnits = "BPM";
                    }
                    else if (mFileName.StartsWith("SpO2-"))
                    {
                        mSignalLabels = "SpO2";
                        mSignalUnits = "%";
                    }
                    else if (mFileName.StartsWith("pleth-"))
                    {
                        mSignalLabels = "pleth";
                        mSignalUnits = "mV";
                    }
                }

                if (bOk)
                {
                    mCheckManualName(AManualName);  // if manual mode check name and if null use parent (dbase) name

                    if( mRecDurationSec < 1 || mRecDurationSec > 5 * 24 * 3600) // 5 days
                    {
                        float stripLength = mGetSamplesTotalTime();
                        if (stripLength >= 1)
                        {
                            mRecDurationSec = mGetSamplesTotalTime();
                        }
                    }

                    mRecState = (UInt16)DRecState.Received;
                    if (mbDebugWriteDataCsv)
                    {
                        mbWriteDataCsvFile(AFilePath);
                    }
                    mAnnotationAtr = new CAnnotation();

                    if (mAnnotationAtr != null)
                    {
                        bool bAnOk = mAnnotationAtr.mbReadAnnotationFile(AFilePath, "atr", mBaseUTC, (Int16)mTimeZoneOffsetMin, mSampleUnitT);

                        bAnOk = mbSetEventTimeFromAnnotationOrFileName(mFileName);
                        if (bAnOk)
                        {
                            if (mbDebugWriteAnnCsv)
                            {
                                mAnnotationAtr.mbWriteAnnotationTextFile(AFilePath);
                            }
                        }
                    }
                }
                if (AMoveTimeZone == DMoveTimeZone.Move)
                {
                    mbMoveTimeZone((Int16)ADefaultTimeZoneOffsetMin);
                }
                mModifyInvertMask(AInvChannels);
            }
            return bOk;
        }

        public void mModifyInvertMask(UInt32 ANewInvChannelsMask)
        {
            try
            {
                if (mSignals != null)
                {
                    int nrChannels = mSignals.Length;

                    if (mNrSignals != 0)
                    {
                        for (int iChannel = 0; iChannel < nrChannels; ++iChannel)
                        {
                            uint mask = 1U << iChannel;

                            if ((ANewInvChannelsMask & mask) != (mInvertChannelsMask & mask))
                            {
                                // InvertChannelsMask not the same => invert amplitude

                                CSignalData sd = mSignals[iChannel];

                                if (sd != null && sd.mValues != null)
                                {
                                    for (uint i = 0; i < sd.mNrValues; ++i)
                                    {
                                        sd.mValues[i] = -sd.mValues[i];
                                    }
                                }
                            }
                        }
                        mInvertChannelsMask = ANewInvChannelsMask;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Modyfi Invert channels", ex);
            }

        }

        public bool mbSetEventTimeFromAnnotationOrFileName(string AFileName)
        {
            bool bOk = false;

            if (mAnnotationAtr != null && AFileName != null)
            {
                UInt16 code, value;
                float tAnn = mAnnotationAtr.mGetEventTimeSec(out code, out value);
                //              float t = mAnnotationAtr.mGetLastTimeSec();

                float tStrip = mGetSamplesTotalTime();
                if (tAnn >= 0 && (tAnn <= tStrip || tStrip < 1 /* no Samples*/))
                {
                    mbSetEventSec(tAnn);
                    bOk = true;
                }

                if (false == bOk)
                {
                    // annotation file failed => try to get it from filename <snr>_PatID_yyyymmddhhmmss
                    string name = AFileName;
                    int pos = name.IndexOf('_');
                    if (pos > 0)
                    {
                        name = name.Substring(pos + 1);
                        pos = name.IndexOf('_');
                        if (pos >= 0)
                        {
                            name = name.Substring(pos + 1);
                            int len = name == null ? 0 : name.Length;

                            if (len >= 14)
                            {
                                DateTime dtLocal;
                                if (CProgram.sbParseYMDHMS(name, out dtLocal, true))
                                {
                                    DateTime dtUtc = dtLocal.AddMinutes(-mTimeZoneOffsetMin);

                                    float t = (float)(dtUtc - mBaseUTC).TotalSeconds;

                                    if (t >= -1 && t <= tStrip + 1)
                                    {
                                        mbSetEventSec(t);
                                        bOk = true;
                                        if (CLicKeyDev.sbDeviceIsTestOrganisation())
                                        {
                                            CProgram.sLogLine("no  atr file, using event time from filename " + name.Substring(0, 14));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return bOk;
        }

        public bool mbReadAnnotationOnly(string AFilePath, DateTime AReceiveTimeUTC, Int32 ADefaultTimeZoneOffsetMin)
        {
            bool bOk = false;

            mFileName = Path.GetFileName(AFilePath);

            mTimeZoneOffsetMin = ADefaultTimeZoneOffsetMin;    // use default, can be overwritten by read of json file
            mReceivedUTC = AReceiveTimeUTC;
            mImportConverter = 0;
            mImportMethod = 0;

            mbReadJsonFile(AFilePath, false);  // if present read <file>.json for time zone + strip length ?

            if (mbReadHeaderFile(AFilePath))
            {
                mRecState = 0;
                mAnnotationAtr = new CAnnotation();

                if (mAnnotationAtr != null)
                {
                    bool bAnOk = mAnnotationAtr.mbReadAnnotationFile(AFilePath, "atr", mBaseUTC, (Int16)mTimeZoneOffsetMin, mSampleUnitT);

                    bOk = mbSetEventTimeFromAnnotationOrFileName(mFileName);

                    if (bOk)
                    {
                        if (mbDebugWriteAnnCsv)
                        {
                            mAnnotationAtr.mbWriteAnnotationTextFile(AFilePath);
                        }
                    }
                }
            }
            return bOk;
        }

        private uint mGetSampleIndex(float ATimeSec)
        {
            int i = (int)(ATimeSec * mSampleFrequency + 0.5F);
            if (i < 0)
            {
                i = 0;
            }
            else if (i >= mNrSamples)
            {
                i = (int)mNrSamples - 1;
            }
            return (uint)i;
        }

        private void mGetAmplitudeRange(ushort ASignalIndex, uint AStartIndex, uint AStopIndex, out float AMinA, out float AMaxA, float AMaxRange)
        {
            AMinA = AMaxA = 0.0F;

            if (ASignalIndex < mNrSignals && mSignals != null)
            {
                CSignalData sd = mSignals[ASignalIndex];

                if (sd != null && sd.mValues != null)
                {
                    int aMin = int.MaxValue;
                    int aMax = int.MinValue;
                    int a;

                    for (uint i = AStartIndex; i <= AStopIndex; ++i)
                    {
                        if (i < sd.mNrValues)
                        {
                            a = sd.mValues[i];
                            if (a < aMin) aMin = a; else if (a > aMax) aMax = a;
                        }
                    }
                    AMinA = aMin * mSampleUnitA;
                    AMaxA = aMax * mSampleUnitA;

                    if ((AMaxA - AMinA) > AMaxRange)
                    {
                        float average = AMinA + ((AMaxA - AMinA) / 2);
                        int n = 1;

                        for (uint i = AStartIndex; i <= AStopIndex; ++i)
                        {
                            if (i < sd.mNrValues)
                            {
                                a = sd.mValues[i];
                                ++n;
                                average += a;
                            }
                        }
                        average /= n;
                        average *= mSampleUnitA;

                        float min = average - AMaxRange / 2;    // MaxRange around average
                        float max = average + AMaxRange / 2;

                        if (min < AMinA)
                        {
                            AMaxA = min + AMaxRange;    // range clipped above min value
                        }
                        else if (max > AMaxA)
                        {
                            AMinA = max - AMaxRange;    // range clipped below max value
                        }
                        else
                        {
                            AMinA = min;
                            AMaxA = max;
                        }
                    }
                }
            }
        }

        public bool mbSetEventUTC(DateTime AEventUTC)
        {
            bool bOk = false;
            TimeSpan ts = AEventUTC - mBaseUTC;

            mEventUTC = DateTime.SpecifyKind(AEventUTC, DateTimeKind.Utc);
            mEventTimeSec = (float)ts.TotalSeconds;
            float stripSec = mGetSamplesTotalTime();

            if (mEventTimeSec < -1.0)
            {
                CProgram.sLogError("event time " + CProgram.sDateTimeToYMDHMS(mEventUTC) + " "
                    + mEventTimeSec.ToString("0.00") + "sec before base " + CProgram.sDateTimeToYMDHMS(mBaseUTC)
                    + ", strip=" + stripSec.ToString("0.00") + "sec");
            }
            else if (mEventTimeSec > stripSec + 1.0)
            {
                CProgram.sLogError("event time " + CProgram.sDateTimeToYMDHMS(mEventUTC) + " "
                    + (mEventTimeSec - stripSec).ToString("0.00") + "sec after strip " + CProgram.sDateTimeToYMDHMS(mBaseUTC)
                    + ", strip=" + stripSec.ToString("0.00") + "sec");
            }
            else
            {
                bOk = true;
            }
            return bOk;
        }
        public bool mbSetEventSec(float AEventSec)
        {
            bool bOk = false;

            mEventTimeSec = AEventSec;

            mEventUTC = mBaseUTC.AddSeconds(mEventTimeSec);

            if (mEventTimeSec >= 0.0 && mEventTimeSec <= mGetSamplesTotalTime())
            {
                bOk = true;
            }
            return bOk;
        }

        public DateTime mGetEventUTC()
        {
            if (mEventUTC == DateTime.MinValue && mBaseUTC != DateTime.MinValue)
            {
                mEventUTC = mBaseUTC;
            }

            return mEventUTC;
        }
        public float mGetEventSec()
        {
            return mEventTimeSec;
        }
        public bool mbGetAmplitudeRange(ushort ASignalIndex, float AStartTimeSec, float ADurationSec, out float AMinA, out float AMaxA, float AMaxRange)
        {
            bool bOk = false;

            AMinA = AMaxA = 0.0F;
            if (ASignalIndex < mNrSignals && mSignals != null)
            {
                uint iStart = mGetSampleIndex(AStartTimeSec);
                uint iStop = mGetSampleIndex(AStartTimeSec + ADurationSec);

                if (iStop > iStart)
                {
                    mGetAmplitudeRange(ASignalIndex, iStart, iStop, out AMinA, out AMaxA, AMaxRange);
                    bOk = true;
                }
            }
            return bOk;
        }

        private void mPlotSampleRange(ushort ASignalIndex, uint AStartIndex, uint AStopIndex, Graphics AGraphic, Pen APen, CStripChart AChart)
        {
            if (ASignalIndex < mNrSignals && mSignals != null)
            {
                int x0, y0, x1, y1;
                float a, t;
                uint i = AStartIndex;

                t = i * mSampleUnitT;
                a = mGetSampleValue(ASignalIndex, i);
                x0 = AChart.mGetPixelT(t);
                y0 = AChart.mGetPixelA(a);

                while (++i <= AStopIndex)
                {
                    t = i * mSampleUnitT;
                    a = mGetSampleValue(ASignalIndex, i);
                    x1 = AChart.mGetPixelT(t);
                    y1 = AChart.mGetPixelA(a);

                    AGraphic.DrawLine(APen, x0, y0, x1, y1);
                    x0 = x1;
                    y0 = y1;
                }
            }
        }

        private void mPixelSampleRange(ushort ASignalIndex, uint AStartIndex, uint AStopIndex, Bitmap AImage, Color AColor, CStripChart AChart)
        {
            if (ASignalIndex < mNrSignals && mSignals != null)
            {
                int x, y, w, h;
                float a, t;

                w = AImage.Width;
                h = AImage.Height;

                for (uint i = AStartIndex; i <= AStopIndex; ++i)
                {
                    t = i * mSampleUnitT;
                    a = mGetSampleValue(ASignalIndex, i);
                    x = AChart.mGetPixelT(t);
                    y = AChart.mGetPixelA(a);
                    if (x >= 0 && x < w && y >= 0 && y < h)
                    {
                        AImage.SetPixel(x, y, AColor);
                    }
                    else
                    {
                        //string s = "This should not be happening!";
                    }
                }
            }
        }

        private void mPlotUnitLinesTime(Graphics AGraphic, Pen APen, CStripChart AChart)
        {
            if (mNrSignals > 0 && mSignals != null)
            {
                int x, y0, y1;
                float startTime = AChart.mGetStartTimeSec();
                float stripTime = AChart.mGetStripTimeSec();
                float endTime = startTime + stripTime;
                float unitT = AChart.mGetUnitT();
                float t;

                if (unitT > 0.001F)
                {
                    int n = (int)((endTime - startTime) / unitT);
                    if (n == 0 || n > _cMaxLinesTime || n > AChart.mGetAreaWidth() / _cMinLineDistance)
                    {
                        return;
                    }
                    int i = (int)(startTime / unitT + 0.999F);
                    t = i * unitT;

                    y0 = AChart.mGetAreaTop();
                    y1 = y0 + AChart.mGetAreaHeight() - 1;

                    while (t <= endTime)
                    {
                        x = AChart.mGetPixelT(t);
                        if (x >= 0 && x < AChart.mGetAreaWidth())
                        {
                            AGraphic.DrawLine(APen, x, y0, x, y1);
                        }

                        t += unitT;
                    }
                }
            }
        }
        private void mPlotUnitLinesAmplitude(Graphics AGraphic, Pen APen, CStripChart AChart)
        {
            if (mNrSignals > 0 && mSignals != null)
            {
                int x0, x1, y;
                float startA = AChart.mGetStartA();
                float endA = AChart.mGetEndA();
                float unitA = AChart.mGetUnitA();
                float a;
                if (unitA > 0.001F)
                {
                    int n = (int)((endA - startA) / unitA);
                    if (n == 0 || n > _cMaxLinesAmpl || n > AChart.mGetAreaHeight() / _cMinLineDistance)
                    {
                        return;
                    }
                    int i = (int)(startA / unitA - 0.01);
                    a = i * unitA;

                    x0 = AChart.mGetAreaLeft();
                    x1 = x0 + AChart.mGetAreaWidth() - 1;

                    while (a <= endA)
                    {
                        y = AChart.mGetPixelA(a);
                        if (y > 0 && y < AChart.mGetAreaHeight())
                        {
                            AGraphic.DrawLine(APen, x0, y, x1, y);
                        }

                        a += unitA;
                    }
                }
            }
        }

        private void mPlotUnitDots(Bitmap AImage, Color AColor, CStripChart AChart)
        {
            if (mNrSignals > 0 && mSignals != null)
            {
                int x, y;
                float startTime = AChart.mGetStartTimeSec();
                float stripTime = AChart.mGetStripTimeSec();
                float endTime = startTime + stripTime;
                float unitT = AChart.mGetUnitT() * 0.2F;
                float t;
                float startA = AChart.mGetStartA();
                float endA = AChart.mGetEndA();
                float unitA = AChart.mGetUnitA() * 0.2F;
                float a, aStart;

                if (unitT > 0.001F && unitA > 0.001F)
                {
                    int n = (int)((endA - startA) / unitA);
                    if (n == 0 || n > 5 * _cMaxLinesAmpl || n > AChart.mGetAreaHeight() / _cMinLineDistance)
                    {
                        return;
                    }
                    n = (int)((endTime - startTime) / unitT);
                    if (n == 0 || n > 5 * _cMaxLinesTime || n > AChart.mGetAreaWidth() / _cMinLineDistance)
                    {
                        return;
                    }
                    int i = (int)(startTime / unitT + 0.999F);
                    t = i * unitT;
                    i = (int)(startA / unitA - 0.01);
                    aStart = i * unitA;

                    int dX = AChart.mGetPixelT(t + unitT) - AChart.mGetPixelT(t);
                    int dY = AChart.mGetPixelA(aStart) - AChart.mGetPixelA(aStart + unitA);

                    if (dX >= 2 && dY >= 2)
                    {
                        while (t <= endTime)
                        {
                            x = AChart.mGetPixelT(t);
                            if (x > 0 && x < AChart.mGetAreaWidth())
                            {
                                a = aStart;
                                while (a <= endA)
                                {
                                    y = AChart.mGetPixelA(a);
                                    if (y > 0 && y < AChart.mGetAreaHeight())
                                    {
                                        AImage.SetPixel(x, y, AColor);
                                    }

                                    a += unitA;
                                }
                            }
                            t += unitT;
                        }
                    }
                }
            }
        }

        private void mPlotRefUnitLine(Graphics AGraphic, Pen APen, CStripChart AChart, float AReferenceTime, float AReferenceAmplitude)
        {
            if (mNrSignals > 0 && mSignals != null)
            {
                int x0, y0, x1, y1;
                int xOffset = 2;
                float startTime = AChart.mGetStartTimeSec();
                float stripTime = AChart.mGetStripTimeSec();
                float endTime = startTime + stripTime;
                float unitT = AChart.mGetUnitT();
                float meanA = 0.5F * (AChart.mGetStartA() + AChart.mGetEndA());
                float unitA = AChart.mGetUnitA();
                float t, a;

                if (unitT > 0.001F && unitA > 0.001F)
                {
                    t = startTime;              // plot a begin graph and around mid line
                    a = meanA - 0.5F * AReferenceAmplitude;

                    x0 = xOffset + AChart.mGetPixelT(t);
                    y0 = AChart.mGetPixelA(a);

                    x1 = xOffset + AChart.mGetPixelT(t + AReferenceTime * 0.5F);
                    y1 = AChart.mGetPixelA(a);
                    AGraphic.DrawLine(APen, x0, y0, x1, y1);
                    x0 = x1;
                    y0 = y1;
                    y1 = AChart.mGetPixelA(a + AReferenceAmplitude);
                    AGraphic.DrawLine(APen, x0, y0, x1, y1);
                    x0 = x1;
                    y0 = y1;
                    x1 = xOffset + AChart.mGetPixelT(t + AReferenceTime * 1.5F);
                    AGraphic.DrawLine(APen, x0, y0, x1, y1);
                    x0 = x1;
                    y0 = y1;
                    y1 = AChart.mGetPixelA(a);
                    AGraphic.DrawLine(APen, x0, y0, x1, y1);
                    x0 = x1;
                    y0 = y1;
                    x1 = xOffset + AChart.mGetPixelT(t + AReferenceTime * 2.0F);
                    AGraphic.DrawLine(APen, x0, y0, x1, y1);
                }
            }
        }

        private void mPlotEventTime(Graphics AGraphic, Pen APen, CStripChart AChart, bool AbThickLine)
        {

            if (mNrSignals > 0 && mSignals != null)
            {
                int x, y0, y1;

                x = AChart.mGetPixelT(mEventTimeSec);
                if (x > 0 && x < AChart.mGetAreaWidth())
                {
                    y0 = AChart.mGetAreaTop();
                    y1 = y0 + AChart.mGetAreaHeight() - 1;
                    if (AbThickLine && x > 1)
                    {
                        AGraphic.DrawLine(APen, x - 1, y0, x - 1, y1);
                    }
                    AGraphic.DrawLine(APen, x, y0, x, y1);
                    if (AbThickLine && x + 1 < AChart.mGetAreaWidth())
                    {
                        AGraphic.DrawLine(APen, x + 1, y0, x + 1, y1);
                    }
                }
            }
        }
        private void mPlotP1P2Lines(Graphics AGraphic, Color AP1Color, Color AP2Color, CStripChart AChart)
        {
            if (mNrSignals > 0 && mSignals != null)
            {
                int w = AChart.mGetAreaWidth() - 1;
                int h = AChart.mGetAreaHeight() - 1;
                int y0 = AChart.mGetAreaTop();
                int y1 = y0 + h;

                float a, t;

                if (CRecordP1P2.sbGetP2(out t, out a))
                {
                    Pen pen = new Pen(AP2Color);

                    int x = AChart.mGetPixelT(t);
                    if (x > 0 && x <= w)
                    {
                        AGraphic.DrawLine(pen, x, y0, x, y1);
                    }
                    int y = AChart.mGetPixelA(a);
                    if (y > 0 && y <= h)
                    {
                        AGraphic.DrawLine(pen, 0, y, w, y);
                    }
                }
                if (CRecordP1P2.sbGetP1(out t, out a))
                {
                    Pen pen = new Pen(AP1Color);

                    int x = AChart.mGetPixelT(t);
                    if (x > 0 && x <= w)
                    {
                        AGraphic.DrawLine(pen, x, y0, x, y1);
                    }
                    int y = AChart.mGetPixelA(a);
                    if (y > 0 && y <= h)
                    {
                        AGraphic.DrawLine(pen, 0, y, w, y);
                    }
                }
            }
        }
        public static string sGetDefaultChannelName(UInt16 ASignalIndex, UInt16 ANrChannels)
        {
            int i = ASignalIndex + 1;
            string s = "CH " + i.ToString();

            if (ANrChannels == 8)
            {
                switch (i)
                {
                    case 1: s = "Ch 1"; break;    //  "Lead I"
                    case 2: s = "Ch 2"; break;
                    case 3: s = "V1"; break;
                    case 4: s = "V2"; break;
                    case 5: s = "V3"; break;
                    case 6: s = "V4"; break;
                    case 7: s = "V5"; break;
                    case 8: s = "V6"; break;
                }

            }
            else if (ANrChannels == 4)
            {
                switch (i)
                {
                    case 1: s = "Ch 1"; break;
                    case 2: s = "Ch 2"; break;
                    case 3: s = "Ch 3"; break;
                    case 4: s = "V1"; break;
                }
            }
            else
            {
                switch (i)
                {
                    case 1: s = "Ch 1"; break;
                    case 2: s = "Ch 2"; break;
                    case 3: s = "Ch 3"; break;
                    case 4: s = "aVR"; break;
                    case 5: s = "aVL"; break;
                    case 6: s = "aVF"; break;
                    case 7: s = "V1"; break;
                    case 8: s = "V2"; break;
                    case 9: s = "V3"; break;
                    case 10: s = "V4"; break;
                    case 11: s = "V5"; break;
                    case 12: s = "V6"; break;
                }
            }
            //            string s = "ECG " + i.ToString();
            return s;
        }

        public string mGetChannelName(UInt16 ASignalIndex)
        {
            return sGetDefaultChannelName(ASignalIndex, mNrSignals);
        }

        public bool mInitChart(ushort ASignalIndex, CStripChart AChart, float AStartTimeSec, float ATimeDurationSec, float AUnitA, float AUnitT, float AMaxRange)
        {
            bool bOk = false;

            if (ATimeDurationSec > 0.001F && AChart != null && ASignalIndex < mNrSignals && mSignals != null)
            {
                float minA, maxA;

                try
                {
                    uint iStart = mGetSampleIndex(AStartTimeSec);
                    uint iStop = mGetSampleIndex(AStartTimeSec + ATimeDurationSec);
                    if (iStop > iStart)
                    {
                        mGetAmplitudeRange(ASignalIndex, iStart, iStop, out minA, out maxA, AMaxRange);

                        float delta = maxA - minA;
                        delta *= 0.1F;
                        minA -= delta;  // scale to show maximum at 80% screen height
                        maxA += delta;

                        if (AChart.mbSetGraphMaxTA(AStartTimeSec, ATimeDurationSec, minA, maxA, cMinRangeAmplitude))
                        {
                            AChart.mSetUnitT(AUnitT);
                            AChart.mSetUnitA(AUnitA);
                            bOk = true;
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
            return bOk;
        }

        public void mPlotAnnotationText(Image AImage, Graphics AGraphics, UInt16 AStartX, UInt16 AStartY, Font AFont, Int16 AOffsetX, Color AColor, CStripChart AChart, DPLotAnnotations AMode, bool AbPlotBeatTime)
        {
            if (AImage != null && AGraphics != null && AChart != null && mAnnotationAtr != null
                && AMode != DPLotAnnotations.Off && mAnnotationAtr.mAnnotationsList != null)
            {
                UInt32 tTick;
                Int32 x;
                UInt16 code;
                string s;
                float t;
                bool bAll = AMode == DPLotAnnotations.All;
                float tStart = AChart.mGetStartTimeSec();
                float tEnd = tStart + AChart.mGetStripTimeSec();
                Brush brush = new SolidBrush(AColor);
                bool bIsBeat, bDrawBeatTime = false;
                float tPrevBeat = -1e6F;
                float tBeatTimeSec;
                int xMax = AImage.Width - 30;
                int yOffset = AFont.Height;// 10;
                int xOffset = AOffsetX;
                int xLastLetter = -1;
                StringFormat formatNr = new StringFormat();
                StringFormat formatEnd = new StringFormat();
                bool bFirstBeat = true;

                formatNr.Alignment = StringAlignment.Center;
                formatEnd.Alignment = StringAlignment.Far;

                foreach (CAnnotationLine ann in mAnnotationAtr.mAnnotationsList)
                {
                    tTick = ann.mAnnotationTimeTck;
                    t = mAnnotationAtr.mGetTimeSec(tTick);

                    if (t > tStart && t < tEnd)
                    {
                        code = ann.mAnnotationCode;
                        bIsBeat = CAnnotationLine.sbIsBeat(code);
                        if (AbPlotBeatTime)
                        {
                            bDrawBeatTime = bIsBeat;
                            if (bIsBeat)
                            {
                                tBeatTimeSec = t - tPrevBeat;
                                tPrevBeat = t;

                                if (bFirstBeat)
                                {
                                    bFirstBeat = false; // do not draw info because prev beat time is not ok
                                }
                                else
                                {
                                    bDrawBeatTime = tBeatTimeSec <= mMaxBeatTimeSec;

                                    if (bDrawBeatTime)  // write beat time halfway to previous beat
                                    {
                                        x = AChart.mGetPixelT(t - tBeatTimeSec * 0.5F) + xOffset;
                                        if (x > AStartX && x < xMax)
                                        {
                                            // only draw bpm                                            AGraphics.DrawString(((int)(tBeatTimeSec * 1000 + 0.5)).ToString(), AFont, brush, new PointF(x, AStartY), formatNr);

                                            if (tBeatTimeSec > 0.1)
                                            {
                                                int hrBpm = (int)(60.0 / tBeatTimeSec + 0.5);
                                                AGraphics.DrawString(hrBpm.ToString(), AFont, brush, new PointF(x, AStartY/* + yOffset*/), formatNr);
                                            }

                                        }
                                    }
                                }
                            }
                        }

                        x = AChart.mGetPixelT(t) + AOffsetX;
                        if (x > AStartX && x < xMax)
                        {
                            if (bAll || bIsBeat)
                            {
                                s = CAnnotationLine.sGetAnnotationLetter(code, ann.mAnnotationSubCode);

                                switch (code)
                                {
                                    /*Sirona Rev 1.1
                                                    42 – Change in signal type. Each non-zero bit in the ‘subtyp’ field indicates that the corresponding signal is in a lead-off state
                                                            (the least significant bit corresponds to signal 0).
                                                    43 – Patient initiated event, corresponding to when the patient pressed the Record button.
                                                    44 – Arrhythmia start, with ‘subtyp’ field set to ‘t’, ‘b’, ‘p’, or ‘a’ for tachy, brady, pause, or afib respectively. (event mode only) 
                                                    45 – Arrhythmia end, with ‘subtyp’ field set to ‘t’, ‘b’, ‘p’, or ‘a’ for tachy, brady, pause, or afib respectively. (event mode only) 
                                    */

                                    case 42:        //return "¢" + sLeadOffLetters(ASubCode);
                                        AGraphics.DrawString(s, AFont, brush, new PointF(x, AStartY + yOffset));
                                        break;
                                    case 43: //return "§";
                                        AGraphics.DrawString(s, AFont, brush, new PointF(x, AStartY + yOffset));
                                        break;
                                    case 44: //return "«" + sArithmiaLetter(ASubCode);
                                        AGraphics.DrawString(s, AFont, brush, new PointF(x, AStartY + yOffset));
                                        break;
                                    case 45: //return sArithmiaLetter(ASubCode) + "»";
                                        AGraphics.DrawString(s, AFont, brush, new PointF(x - 10, AStartY + yOffset), formatEnd);
                                        break;
                                    case 46: //return "≤";
                                        AGraphics.DrawString(s, AFont, brush, new PointF(x, AStartY + yOffset));
                                        break;
                                    case 47: //return "≥";
                                        AGraphics.DrawString(s, AFont, brush, new PointF(x - 10, AStartY + yOffset), formatEnd);
                                        break;
                                    default:
                                        AGraphics.DrawString(s, AFont, brush, new PointF(x, AStartY));
                                        break;
                                }
                            }
                            xLastLetter = x;
                        }
                    }
                }
                //                AGraphics.DrawString("ms", AFont, brush, new PointF(xMax+2, AStartY));
                AGraphics.DrawString("bpm", AFont, brush, new PointF(xMax + 2, AStartY/*+yOffset*/));
            }
        }
        public void mPlotAnnotationMarker(Image AImage, Graphics AGraphics, UInt16 AStartX, UInt16 AStartY, Int16 ALength, Color AColor, CStripChart AChart, DPLotAnnotations AMode)
        {
            if (AImage != null && AGraphics != null && AChart != null && mAnnotationAtr != null
                  && AMode != DPLotAnnotations.Off && mAnnotationAtr.mAnnotationsList != null)
            {
                UInt32 tTick;
                Int32 x, y2 = AStartY + ALength;
                UInt16 code;
                string s;
                float t;
                bool bAll = AMode == DPLotAnnotations.All;
                float tStart = AChart.mGetStartTimeSec();
                float tEnd = tStart + AChart.mGetStripTimeSec();
                Pen pen = new Pen(AColor);

                foreach (CAnnotationLine ann in mAnnotationAtr.mAnnotationsList)
                {
                    tTick = ann.mAnnotationTimeTck;
                    t = mAnnotationAtr.mGetTimeSec(tTick);

                    if (t > tStart && t < tEnd)
                    {
                        x = AChart.mGetPixelT(t);
                        if (x > AStartX)
                        {
                            code = ann.mAnnotationCode;
                            if (bAll || CAnnotationLine.sbIsBeat(code))
                            {
                                AGraphics.DrawLine(pen, x, AStartY, x, y2);
                            }
                        }
                    }
                }
            }
        }


        public Image mCreateChartImage2(ushort ASignalIndex, CStripChart AChart, int AWidth, int AHeight,
            float ACursorStartT, float ACursorWidthSec, DChartDrawTo ADrawTo,
            bool bPlotBorder = true, DPLotAnnotations ATextMode = DPLotAnnotations.Off, DPLotAnnotations AMarkerMode = DPLotAnnotations.Off)
        {
            Bitmap img = null;

            if (AWidth > 2 && AHeight > 2 && AChart != null)
            {
                if (ASignalIndex < mNrSignals && mSignals != null)
                {
                    try
                    {
                        float startTimeSec = AChart.mGetStartTimeSec();
                        float timeDurationSec = AChart.mGetStripTimeSec();
                        uint iStart = mGetSampleIndex(startTimeSec);
                        uint iStop = mGetSampleIndex(startTimeSec + timeDurationSec);
                        if (iStop > iStart)
                        {
                            img = new Bitmap(AWidth, AHeight);

                            using (Graphics graphics = Graphics.FromImage(img))
                            {

                                Color backGroundColor = Color.White;
                                Color borderColor = Color.Black;
                                Color unitLinesColorT = CDvtmsData.sGetGridColor(ADrawTo); // Color.LightPink;//  Gray;// LightGray; //Color.Red;
                                Color unitLinesColorA = unitLinesColorT;//Gray; // LightGray; //Color.Red;
                                Color unitDotColor = unitLinesColorT;//Gray; //Color.Red;
                                Color pixelColor = CDvtmsData.sGetSignalColor(ADrawTo); // Color.Black;
                                Color plotColor = pixelColor; // Color.Gray;
                                float plotWidth = CDvtmsData.sGetSignalWidth(ADrawTo);
                                Color refUnitColor = CDvtmsData.sGetPrintColor(ADrawTo, Color.DarkBlue);
                                Color eventColor = CDvtmsData.sGetPrintColor(ADrawTo, Color.Red); // DarkBlue;
                                Color channelTextColor = CDvtmsData.sGetPrintColor(ADrawTo, Color.DarkBlue);
                                Color cursorBackground = CDvtmsData.sGetPrintColor(ADrawTo, Color.LightBlue);
                                Color annTextColor = CDvtmsData.sGetPrintColor(ADrawTo, Color.Green);
                                Color annMarkerColor = CDvtmsData.sGetPrintColor(ADrawTo, Color.LightGreen);
                                Color p1Color = Color.Aqua;
                                Color p2Color = Color.LightGray;
                                Pen pen = new Pen(borderColor);

                                int w = img.Width - 1;
                                int h = img.Height - 1;


                                // clear image
                                graphics.Clear(backGroundColor);
                                //graphics.DrawRectangle(pen, 0, 0, AWidth, AHeight); // does not work
                                // draw boundry box
                                if (bPlotBorder)
                                {
                                    graphics.DrawLine(pen, 0, 0, w, 0);
                                    graphics.DrawLine(pen, w, 0, w, h);
                                    graphics.DrawLine(pen, w, h, 0, h);
                                    graphics.DrawLine(pen, 0, h, 0, 0);
                                }
                                // setup chart
                                if (AChart.mbSetGraph(1, 1, w - 1, h - 1, cMinRangeAmplitude))
                                {
                                    int x = AChart.mGetPixelT(AChart.mGetStartTimeSec() + 0.5F * AChart.mGetStripTimeSec());
                                    int y = AChart.mGetPixelA(0.5F * (AChart.mGetEndA() + AChart.mGetStartA()));

                                    //                                    if( ACursorStartT +  >= -0.01F )
                                    if (ACursorWidthSec > 0)
                                    {
                                        float startT = ACursorStartT <= 0 ? 0.0F : ACursorStartT;
                                        float endT = ACursorStartT + ACursorWidthSec;
                                        if (endT > AChart.mGetStripTimeSec()) endT = AChart.mGetStripTimeSec();
                                        int x0 = AChart.mGetPixelT(startT);
                                        int x1 = AChart.mGetPixelT(endT);
                                        if (x0 > 0 && x1 >= x0)
                                        {
                                            Brush brushCursor = new SolidBrush(cursorBackground);
                                            graphics.FillRectangle(brushCursor, x0, 1, x1 - x0 + 1, h - 2);
                                        }
                                    }

                                    mPlotUnitDots(img, unitDotColor, AChart);

                                    pen = new Pen(unitLinesColorT);
                                    mPlotUnitLinesTime(graphics, pen, AChart);

                                    //graphics.DrawLine(pen, 0, y, AWidth - 1, y);    // horizontal line
                                    //graphics.DrawLine(pen, x, 0, x, AHeight - 1);    // vertical line
                                    // plot event time
                                    pen = new Pen(unitLinesColorA);
                                    mPlotUnitLinesAmplitude(graphics, pen, AChart);

                                    pen = new Pen(refUnitColor);
                                    mPlotRefUnitLine(graphics, pen, AChart, cReferenceTime, cReferenceAmplitude);

                                    mPlotP1P2Lines(graphics, p1Color, p2Color, AChart);
                                    pen = new Pen(eventColor);
                                    mPlotEventTime(graphics, pen, AChart, true);

                                    string channel = mGetChannelName(ASignalIndex);

                                    float fontSize = 3.0F * h / 80.0F;
                                    Font font = new Font("Verdana", fontSize);

                                    UInt16 startY = (UInt16)(bPlotBorder ? 1 : 0);
                                    Int16 offsetX = (Int16)(AMarkerMode == DPLotAnnotations.Off ? -3 : 1);
                                    Int16 markerLength = (Int16)(AHeight / 10);
                                    mPlotAnnotationMarker(img, graphics, 20, startY, markerLength, annMarkerColor, AChart, AMarkerMode);
                                    mPlotAnnotationText(img, graphics, 20, startY, font, offsetX, annTextColor, AChart, ATextMode, true);

                                    fontSize = 8.0F * h / 80.0F;
                                    font = new Font("Verdana", fontSize);
                                    Brush brush = new SolidBrush(channelTextColor);
                                    graphics.DrawString(channel, font, brush, new PointF(2, 2));    // draw channel text

                                    // plot graph
                                    //                                    pen = new Pen(plotColor);
                                    pen = new Pen(plotColor, plotWidth);
                                    mPlotSampleRange(ASignalIndex, iStart, iStop, graphics, pen, AChart);
                                    //  mPixelSampleRange(iStart, iStop, img, pixelColor, chart);
                                    graphics.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            return img;
        }


        public Image mCreateChartImage2a(ushort ASignalIndex, float AStartTimeSec, float ATimeDurationSec, int AWidth, int AHeight,
            float AUnitA, float AUnitT, float ACursorStartT, float ACursorWidthSec, DChartDrawTo ADrawTo, float AMaxRange)
        {
            Bitmap img = null;

            if (ATimeDurationSec > 0.001F && AWidth > 2 && AHeight > 2)
            {
                if (ASignalIndex < mNrSignals && mSignals != null)
                {
                    float minA, maxA;

                    try
                    {
                        uint iStart = mGetSampleIndex(AStartTimeSec);
                        uint iStop = mGetSampleIndex(AStartTimeSec + ATimeDurationSec);
                        if (iStop > iStart)
                        {
                            img = new Bitmap(AWidth, AHeight);

                            using (Graphics graphics = Graphics.FromImage(img))
                            {

                                Color backGroundColor = Color.White;
                                Color borderColor = Color.Black;
                                Color unitLinesColorT = CDvtmsData.sGetGridColor(ADrawTo); // Color.LightPink;//  Gray;// LightGray; //Color.Red;
                                Color unitLinesColorA = unitLinesColorT;//Gray; // LightGray; //Color.Red;
                                Color unitDotColor = unitLinesColorT;//Gray; //Color.Red;
                                Color pixelColor = CDvtmsData.sGetSignalColor(ADrawTo); // Color.Black;
                                Color plotColor = pixelColor; // Color.Gray;
                                float plotWidth = CDvtmsData.sGetSignalWidth(ADrawTo);
                                Color refUnitColor = Color.DarkBlue;
                                Color eventColor = Color.Red; // DarkBlue;
                                Color channelTextColor = Color.DarkBlue;
                                Color cursorBackground = Color.LightBlue;
                                Pen pen = new Pen(borderColor);

                                int w = img.Width - 1;
                                int h = img.Height - 1;


                                // clear image
                                graphics.Clear(backGroundColor);
                                //graphics.DrawRectangle(pen, 0, 0, AWidth, AHeight); // does not work
                                // draw boundry box
                                graphics.DrawLine(pen, 0, 0, w, 0);
                                graphics.DrawLine(pen, w, 0, w, h);
                                graphics.DrawLine(pen, w, h, 0, h);
                                graphics.DrawLine(pen, 0, h, 0, 0);

                                // setup chart
                                mGetAmplitudeRange(ASignalIndex, iStart, iStop, out minA, out maxA, AMaxRange);

                                CStripChart chart = new CStripChart();

                                if (chart.mbSetGraphMaxA(1, 1, w - 1, h - 1, AStartTimeSec, ATimeDurationSec, minA, maxA, cMinRangeAmplitude))
                                {
                                    int x = chart.mGetPixelT(AStartTimeSec + 0.5F * ATimeDurationSec);
                                    int y = chart.mGetPixelA(0.5F * (maxA + minA));

                                    chart.mSetUnitT(AUnitT);
                                    chart.mSetUnitA(AUnitA);

                                    if (ACursorStartT >= 0.0F)
                                    {
                                        int x0 = chart.mGetPixelT(ACursorStartT);
                                        int x1 = chart.mGetPixelT(ACursorStartT + ACursorWidthSec);
                                        Brush brushCursor = new SolidBrush(cursorBackground);
                                        graphics.FillRectangle(brushCursor, x0, 1, x1 - x0 + 1, h - 2);
                                    }

                                    mPlotUnitDots(img, unitDotColor, chart);

                                    pen = new Pen(unitLinesColorT);
                                    mPlotUnitLinesTime(graphics, pen, chart);

                                    //graphics.DrawLine(pen, 0, y, AWidth - 1, y);    // horizontal line
                                    //graphics.DrawLine(pen, x, 0, x, AHeight - 1);    // vertical line
                                    // plot event time
                                    pen = new Pen(unitLinesColorA);
                                    mPlotUnitLinesAmplitude(graphics, pen, chart);

                                    pen = new Pen(refUnitColor);
                                    mPlotRefUnitLine(graphics, pen, chart, cReferenceTime, cReferenceAmplitude);

                                    pen = new Pen(eventColor);
                                    mPlotEventTime(graphics, pen, chart, true);

                                    string channel = mGetChannelName(ASignalIndex);

                                    float fontSize = 8.0F * h / 80.0F;
                                    Font font = new Font("Verdana", fontSize);
                                    Brush brush = new SolidBrush(channelTextColor);

                                    graphics.DrawString(channel, font, brush, new PointF(2, 2));

                                    // plot graph
                                    //                                    pen = new Pen(plotColor);
                                    pen = new Pen(plotColor, plotWidth);
                                    mPlotSampleRange(ASignalIndex, iStart, iStop, graphics, pen, chart);
                                    //  mPixelSampleRange(iStart, iStop, img, pixelColor, chart);
                                }
                                graphics.Dispose();
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            return img;
        }
        public Image mCreateChartImage3FixedRangeA(ushort ASignalIndex, CStripChart AChart, int AWidth, int AHeight, float ARangeA,
            float ACursorStartT, float ACursorWidthSec, DChartDrawTo ADrawTo, bool AbBorderLine = true)
        {
            Bitmap img = null;

            if (AWidth > 2 && AHeight > 2 && AChart != null)
            {
                if (ASignalIndex < mNrSignals && mSignals != null)
                {
                    try
                    {
                        float startTimeSec = AChart.mGetStartTimeSec();
                        float timeDurationSec = AChart.mGetStripTimeSec();
                        uint iStart = mGetSampleIndex(startTimeSec);
                        uint iStop = mGetSampleIndex(startTimeSec + timeDurationSec);
                        if (iStop > iStart)
                        {
                            img = new Bitmap(AWidth, AHeight);

                            using (Graphics graphics = Graphics.FromImage(img))
                            {

                                Color backGroundColor = Color.White;
                                Color borderColor = Color.Black;
                                Color unitLinesColorT = CDvtmsData.sGetGridColor(ADrawTo); // Color.LightPink;//  Gray;// LightGray; //Color.Red;
                                Color unitLinesColorA = unitLinesColorT;//Gray; // LightGray; //Color.Red;
                                Color unitDotColor = unitLinesColorT;//Gray; //Color.Red;
                                Color pixelColor = CDvtmsData.sGetSignalColor(ADrawTo); // Color.Black;
                                Color plotColor = pixelColor; // Color.Gray;
                                float plotWidth = CDvtmsData.sGetSignalWidth(ADrawTo);
                                Color refUnitColor = Color.DarkBlue;
                                Color eventColor = Color.Red; // DarkBlue;
                                Color channelTextColor = Color.DarkBlue;
                                Color cursorBackground = Color.LightBlue;
                                Color p1Color = Color.Aqua;
                                Color p2Color = Color.LightGray;

                                Pen pen = new Pen(borderColor);

                                int w = img.Width - 1;
                                int h = img.Height - 1;


                                // clear image
                                graphics.Clear(backGroundColor);
                                //graphics.DrawRectangle(pen, 0, 0, AWidth, AHeight); // does not work
                                // draw boundry box
                                if (AbBorderLine)
                                {
                                    graphics.DrawLine(pen, 0, 0, w, 0);
                                    graphics.DrawLine(pen, w, 0, w, h);
                                    graphics.DrawLine(pen, w, h, 0, h);
                                    graphics.DrawLine(pen, 0, h, 0, 0);
                                }
                                // setup chart
                                if (AChart.mbSetGraphRangeA(1, 1, w - 1, h - 1, cMinRangeAmplitude, ARangeA))
                                {
                                    int x = AChart.mGetPixelT(startTimeSec + 0.5F * timeDurationSec);
                                    int y = AChart.mGetPixelA(0.5F * (AChart.mGetEndA() + AChart.mGetStartA()));

                                    //                                    if( ACursorStartT +  >= -0.01F )
                                    if (ACursorWidthSec > 0)
                                    {
                                        float startT = ACursorStartT <= 0 ? 0.0F : ACursorStartT;
                                        float endT = ACursorStartT + ACursorWidthSec;
                                        if (endT > startTimeSec + timeDurationSec) endT = startTimeSec + timeDurationSec;
                                        int x0 = AChart.mGetPixelT(startT);
                                        int x1 = AChart.mGetPixelT(endT);
                                        if (x0 <= w && x1 >= 0)
                                        {
                                            if (x0 < 0) x0 = 0;
                                            if (x1 > w) x1 = w;
                                            Brush brushCursor = new SolidBrush(cursorBackground);
                                            graphics.FillRectangle(brushCursor, x0, 1, x1 - x0 + 1, h - 2);
                                        }
                                    }

                                    mPlotUnitDots(img, unitDotColor, AChart);

                                    pen = new Pen(unitLinesColorT);
                                    mPlotUnitLinesTime(graphics, pen, AChart);

                                    //graphics.DrawLine(pen, 0, y, AWidth - 1, y);    // horizontal line
                                    //graphics.DrawLine(pen, x, 0, x, AHeight - 1);    // vertical line
                                    // plot event time
                                    pen = new Pen(unitLinesColorA);
                                    mPlotUnitLinesAmplitude(graphics, pen, AChart);

                                    pen = new Pen(refUnitColor);
                                    mPlotRefUnitLine(graphics, pen, AChart, cReferenceTime, cReferenceAmplitude);

                                    mPlotP1P2Lines(graphics, p1Color, p2Color, AChart);

                                    pen = new Pen(eventColor);
                                    mPlotEventTime(graphics, pen, AChart, true);

                                    string channel = mGetChannelName(ASignalIndex);

                                    float fontSize = 8.0F * h / 80.0F;
                                    Font font = new Font("Verdana", fontSize);
                                    Brush brush = new SolidBrush(channelTextColor);

                                    graphics.DrawString(channel, font, brush, new PointF(2, 2));

                                    // plot graph
                                    //                                    pen = new Pen(plotColor);
                                    pen = new Pen(plotColor, plotWidth);
                                    mPlotSampleRange(ASignalIndex, iStart, iStop, graphics, pen, AChart);
                                    //  mPixelSampleRange(iStart, iStop, img, pixelColor, chart);
                                }
                                graphics.Dispose();
                            }

                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            return img;
        }


        public Image mCreateChartImage(ushort ASignalIndex, float AStartTimeSec, float ATimeDurationSec, int AWidth, int AHeight,
                    float AUnitT, float ACursorStartT, float ACursorWidthSec, DChartDrawTo ADrawTo, float AMaxRange)
        {
            Bitmap img = null;

            if (ATimeDurationSec > 0.001F && AWidth > 2 && AHeight > 2)
            {
                if (ASignalIndex < mNrSignals && mSignals != null)
                {
                    float minA, maxA;

                    try
                    {
                        uint iStart = mGetSampleIndex(AStartTimeSec);
                        uint iStop = mGetSampleIndex(AStartTimeSec + ATimeDurationSec);
                        if (iStop > iStart)
                        {
                            img = new Bitmap(AWidth, AHeight);

                            using (Graphics graphics = Graphics.FromImage(img))
                            {

                                Color backGroundColor = Color.White;
                                Color borderColor = Color.Black;
                                Color unitLinesColorT = CDvtmsData.sGetGridColor(ADrawTo); // Color.LightPink;//  Gray;// LightGray; //Color.Red;
                                Color unitLinesColorA = unitLinesColorT;//Gray; // LightGray; //Color.Red;
                                Color unitDotColor = unitLinesColorT;//Gray; //Color.Red;
                                Color pixelColor = CDvtmsData.sGetSignalColor(ADrawTo); // Color.Black;
                                Color plotColor = pixelColor; // Color.Gray;
                                float plotWidth = CDvtmsData.sGetSignalWidth(ADrawTo);
                                Color refUnitColor = Color.DarkBlue;
                                Color eventColor = Color.Red; // DarkBlue;
                                Color cursorBackground = Color.LightBlue;
                                Pen pen = new Pen(borderColor);

                                int w = img.Width - 1;
                                int h = img.Height - 1;


                                // clear image
                                graphics.Clear(backGroundColor);
                                //graphics.DrawRectangle(pen, 0, 0, AWidth, AHeight); // does not work
                                // draw boundry box
                                graphics.DrawLine(pen, 0, 0, w, 0);
                                graphics.DrawLine(pen, w, 0, w, h);
                                graphics.DrawLine(pen, w, h, 0, h);
                                graphics.DrawLine(pen, 0, h, 0, 0);

                                // setup chart
                                mGetAmplitudeRange(ASignalIndex, iStart, iStop, out minA, out maxA, AMaxRange);

                                CStripChart chart = new CStripChart();

                                if (chart.mbSetGraphMaxA(1, 1, w - 1, h - 1, AStartTimeSec, ATimeDurationSec, minA, maxA, cMinRangeAmplitude))
                                {
                                    int x = chart.mGetPixelT(AStartTimeSec + 0.5F * ATimeDurationSec);
                                    int y = chart.mGetPixelA(0.5F * (maxA + minA));

                                    chart.mSetUnitT(AUnitT);

                                    if (ACursorStartT >= 0.0F)
                                    {
                                        int x0 = chart.mGetPixelT(ACursorStartT);
                                        int x1 = chart.mGetPixelT(ACursorStartT + ACursorWidthSec);
                                        Brush brushCursor = new SolidBrush(cursorBackground);
                                        graphics.FillRectangle(brushCursor, x0, 1, x1 - x0 + 1, h - 2);
                                    }

                                    pen = new Pen(unitLinesColorT);
                                    //                                    mPlotUnitLinesTime(graphics, pen, chart);

                                    //graphics.DrawLine(pen, 0, y, AWidth - 1, y);    // horizontal line
                                    //graphics.DrawLine(pen, x, 0, x, AHeight - 1);    // vertical line
                                    // plot event time
                                    pen = new Pen(unitLinesColorA);
                                    //mPlotUnitLinesAmplitude(graphics, pen, chart);

                                    pen = new Pen(refUnitColor);
                                    mPlotRefUnitLine(graphics, pen, chart, cReferenceTime, cReferenceAmplitude);
                                    pen = new Pen(eventColor);
                                    mPlotEventTime(graphics, pen, chart, true);

                                    // plot graph
                                    //                                    pen = new Pen(plotColor);
                                    pen = new Pen(plotColor, plotWidth);
                                    mPlotSampleRange(ASignalIndex, iStart, iStop, graphics, pen, chart);
                                    //  mPixelSampleRange(iStart, iStop, img, pixelColor, chart);
                                }
                                graphics.Dispose();
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            return img;
        }


        public static bool sbIsStateAnalyzed(UInt16 AState, bool AbIncludeExcluded)
        {
            return AState == (UInt16)DRecState.Analyzed || AState == (UInt16)DRecState.Reported
                || AState == (UInt16)DRecState.BaseLine || AState == (UInt16)DRecState.QCd
                || AbIncludeExcluded && AState == (UInt16)DRecState.Excluded;
        }
        public static bool sbIsStateToDo(UInt16 AState)
        {
            return AState <= (UInt16)DRecState.Processed || AState == (UInt16)DRecState.Triaged || AState >= (UInt16)DRecState.NrRecStates;
        }

        public static string sGetRecStateChar(UInt16 AState)
        {
            if (AState < (UInt16)DRecState.NrRecStates)
            {
                switch ((DRecState)AState)
                {
                    case DRecState.Unknown: return "?";
                    case DRecState.Recording: return "⬤";
                    case DRecState.Recorded: return "📼";
                    case DRecState.Received: return "⬗";
                    case DRecState.Processed: return "";
                    case DRecState.Noise: return "✗";
                    case DRecState.LeadOff: return "✗";
                    case DRecState.Excluded: return "-";
                    case DRecState.BaseLine: return "=";
                    case DRecState.Triaged: return "T";
                    case DRecState.Analyzed: return "A";
                    case DRecState.Reported: return "R";
                    case DRecState.QCd: return "Q";
                }
            }
            return "?";
        }
        public static string sGetRecStateUserString(UInt16 AState)
        {
            if (AState < (UInt16)DRecState.NrRecStates)
            {
                switch ((DRecState)AState)
                {
                    case DRecState.Unknown: return "Unknown";
                    case DRecState.Recording: return "Recording";
                    case DRecState.Recorded: return "Recorded";
                    case DRecState.Received: return "Received";
                    case DRecState.Processed: return "Processed";
                    case DRecState.Noise: return "Noise";
                    case DRecState.LeadOff: return "Lead Off";
                    case DRecState.Excluded: return "Excluded";
                    case DRecState.BaseLine: return "BaseLine";
                    case DRecState.Triaged: return "Triaged";
                    case DRecState.Analyzed: return "Analyzed";
                    case DRecState.Reported: return "Reported";
                    case DRecState.QCd: return "QCd";
                }
            }
            return "?";
        }
        public static string sGetRecStateUserAbreviation(UInt16 AState)
        {
            if (AState < (UInt16)DRecState.NrRecStates)
            {
                switch ((DRecState)AState)
                {
                    case DRecState.Unknown: return "Ukn";
                    case DRecState.Recording: return "Rec";
                    case DRecState.Recorded: return "Rcd";
                    case DRecState.Received: return "Rvd";
                    case DRecState.Processed: return "Prs";
                    case DRecState.Noise: return "Noi";
                    case DRecState.LeadOff: return "Lof";
                    case DRecState.Excluded: return "Exc";
                    case DRecState.BaseLine: return "Bln";
                    case DRecState.Triaged: return "Trg";
                    case DRecState.Analyzed: return "Anl";
                    case DRecState.Reported: return "Rep";
                    case DRecState.QCd: return "QC";
                }
            }
            return "?";
        }
        public static Color sGetRecStateBgColor(UInt16 AState)
        {
            if (AState < (UInt16)DRecState.NrRecStates)
            {
                switch ((DRecState)AState)
                {
                    case DRecState.Unknown: return Color.White;
                    case DRecState.Recording: return Color.MediumPurple;
                    case DRecState.Recorded: return Color.MistyRose;
                    case DRecState.Received: return Color.White;
                    case DRecState.Processed: return Color.White;
                    case DRecState.Noise: return SystemColors.InactiveCaption; //Color.LightBlue;
                    case DRecState.LeadOff: return SystemColors.InactiveCaption; // Color.LightBlue;
                    case DRecState.Excluded: return SystemColors.InactiveCaption; // Color.LightBlue;
                    case DRecState.BaseLine: return Color.Orange;
                    case DRecState.Triaged: return Color.LightGreen;
                    case DRecState.Analyzed: return Color.LightYellow;
                    case DRecState.Reported: return Color.LightGray;
                    case DRecState.QCd: return Color.LightBlue;
                }
            }
            return Color.White;
        }

        public static string sGetPriorityLetters(UInt16 APriority)
        {
            string s = "";

            if (APriority < (UInt16)DPriority.NrPriorities)
            {
                switch ((DPriority)APriority)
                {
                    case DPriority.Unknown: s = ""; break;
                    case DPriority.High: s = "Crit"; break;
                    case DPriority.Normal: s = "Med"; break;
                    case DPriority.Low: s = "Low"; break;
                }
            }
            return s;
        }
        public static string sGetPriorityText(UInt16 APriority)
        {
            string s = "";

            if (APriority < (UInt16)DPriority.NrPriorities)
            {
                switch ((DPriority)APriority)
                {
                    case DPriority.Unknown: s = ""; break;
                    case DPriority.High: s = "Critical"; break;
                    case DPriority.Normal: s = "Medium"; break;
                    case DPriority.Low: s = "Low"; break;
                }
            }
            return s;
        }

        public static int sGetPriorityValue(UInt16 APriority)
        {
            int i = 0;

            if (APriority < (UInt16)DPriority.NrPriorities)
            {
                switch ((DPriority)APriority)
                {
                    case DPriority.Unknown: i = 0; break;
                    case DPriority.High: i = 3; break;
                    case DPriority.Normal: i = 2; break;
                    case DPriority.Low: i = 1; break;
                }
            }
            return i;
        }
        public bool mbGetCasePath(out string ArCasePath)
        {
            bool bOk = false;
            string s = "";

            if (mReceivedUTC > DateTime.MinValue && mStoredAt.Length > 0)
            {
                string ym, d;

                ym = "YM" + mReceivedUTC.Year.ToString() + mReceivedUTC.Month.ToString("00");
                d = "D" + mReceivedUTC.Day.ToString("00");

                s = ym + "\\" + d + "\\" + mStoredAt + "\\";
                //s = /*Path.Combine*/(mRecordingDir, ym, d, ARec.mStoredAt);
                bOk = true;
            }
            ArCasePath = s;
            return bOk;
        }

        public int mGetAgeYears()
        {
            int age = -1;
            int birthDay = mPatientBirthDate.mDecrypt();

            if (birthDay > 0)
            {
                age = CProgram.sCalcAgeYears((UInt32)birthDay, DateTime.Now);
            }
            return age;
        }
        public DateTime mGetBirthDate()
        {
            DateTime dt = mPatientBirthDate.mDecryptToDate();

            return dt;
        }
        public string mGetShowAmplitudeString(float AUnitA)
        {
            string s = "?";
            if (AUnitA > 0.0001)
            {
                float amp = 5.0F / AUnitA;

                if (amp >= 4.5F)
                {
                    s = ((int)(amp + 0.5F)).ToString();
                }
                else if (amp >= 0.05F)
                {
                    s = amp.ToString("0.0");
                }
                else
                {
                    s = amp.ToString("0.00");
                }
                s += " mm/mV (";

                if (AUnitA >= 9.95F)
                {
                    s += ((int)(AUnitA + 0.5F)).ToString();
                }
                else
                {
                    s += AUnitA.ToString("0.0");
                }
                s += " mV)";
            }
            return s;
        }
        public float mGetAmplitudeFromShowString(string AAmpString)
        {
            float ampUnit = 0.0F;
            float amp = 0.0F;

            string s = "";
            int n = AAmpString == null ? 0 : AAmpString.Length;
            int i = 0;
            while (i < n && AAmpString[i] <= ' ') ++i;

            while (i < n)
            {
                char c = AAmpString[i];

                if (char.IsDigit(c))
                {
                    s += c;
                    ++i;
                }
                else if (c == '.' || c == ',')
                {
                    s += '.';
                    ++i;
                }
                else
                {
                    break;
                }
            }
            if (CProgram.sbParseFloat(DFloatDecimal.Dot, s, ref amp) && amp > 0.001F)
            {
                ampUnit = 5.0F / amp;
            }
            return ampUnit;
        }
        public string mGetShowSpeedString(float AUnitT)
        {
            string s = "?";
            if (AUnitT > 0.0001)
            {
                float speed = 5.0F / AUnitT;

                if (speed >= 4.5F)
                {
                    s = ((int)(speed + 0.5F)).ToString();
                }
                else if (speed >= 0.05F)
                {
                    s = speed.ToString("0.0");
                }
                else
                {
                    s = speed.ToString("0.00");
                }
                s += " mm/s (";

                if (AUnitT >= 0.95F)
                {
                    s += ((int)(AUnitT + 0.5F)).ToString();
                }
                else
                {
                    s += AUnitT.ToString("0.00");
                }
                s += " Sec)";
            }
            return s;
        }
        public float mGetSpeedFromShowString(string ATimeString)
        {
            float timeUnit = 0.0F;
            float time = 0.0F;

            string s = "";
            int n = ATimeString == null ? 0 : ATimeString.Length;
            int i = 0;
            while (i < n && ATimeString[i] <= ' ') ++i;

            while (i < n)
            {
                char c = ATimeString[i];

                if (char.IsDigit(c))
                {
                    s += c;
                    ++i;
                }
                else if (c == '.' || c == ',')
                {
                    s += '.';
                    ++i;
                }
                else
                {
                    break;
                }
            }
            if (CProgram.sbParseFloat(DFloatDecimal.Dot, s, ref time) && time > 0.001F)
            {
                timeUnit = 5.0F / time;
            }
            return timeUnit;
        }
        public bool mbWriteRefFile(string ARecordPath)
        {
            bool bOk = false;
            string fileName = "R" + mIndex_KEY.ToString("D08") + "_D" + mDevice_IX.ToString("D05") + "_S" + mStudy_IX.ToString("D08") + "_N" + mSeqNrInStudy.ToString("D05") + "_P" + mPatient_IX.ToString("D08") + ".RefRecord";

            try
            {
                if (ARecordPath != null && ARecordPath.Length > 3)
                {
                    if (CLicKeyDev.sbDeviceIsProgrammer())
                    {
                        CProgram.sLogLine("Record:" + fileName);
                    }

                    if( mRecDurationSec < 0.1 )
                    {
                        mRecDurationSec = mGetSamplesTotalTime();
                    }
                    string filePath = Path.Combine(ARecordPath, fileName);
                    using (StreamWriter fs = new StreamWriter(filePath))
                    {
                        string dt = CProgram.sDateTimeToYMDHMS(DateTime.Now)
                            + CProgram.sTimeZoneOffsetString(CProgram.sGetLocalTimeZoneOffsetMin(), DTimeZoneShown.Long);

                        fs.WriteLine("ReceivedUTC= " + mReceivedUTC.ToString());
                        fs.WriteLine("eventUTC= " + mGetEventUTC().ToString());
                        fs.WriteLine("timeZoneMin= " + mTimeZoneOffsetMin.ToString());
                        fs.WriteLine("startRecUTC= " + mBaseUTC.ToString());
                        fs.WriteLine("RecLengthSec= " + mRecDurationSec.ToString("0.0"));
                        fs.WriteLine("nrSignals= " + mNrSignals.ToString());
                        fs.WriteLine("DdeviceID= " + mDeviceID);
                        fs.WriteLine("file= " + mFileName);
                        fs.WriteLine("StoredAt= " + mStoredAt);
                        fs.WriteLine("ImportMethod=" + sGetMethodUserString(mImportMethod));
                        fs.WriteLine("ImportConverter=" + sGetConverterUserString(mImportConverter));
                        fs.WriteLine("Now=" + dt);
                        fs.WriteLine("By=" + CProgram.sGetProgNameVersionNow());
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Write error " + fileName, ex);
            }
            return bOk;
        }
        public static string sGetPrintStudyRecord(CRecordMit ARecord)
        {
            string s = "";

            if (ARecord != null)
            {
                if (ARecord.mStudy_IX != 0)
                {
                    s = CDvtmsData.sGetPrintStudyCode() /*+ ": "*/ + ARecord.mStudy_IX.ToString() + "." + ARecord.mSeqNrInStudy.ToString();
                }
                else
                {
                    s = "R#" + ARecord.mIndex_KEY.ToString();
                }
            }
            return s;
        }
        public bool mbMoveTimeZone(Int16 ANewTimeZoneMin)
        {
            bool bMoved = false;
            Int16 utcDeltaMin = (Int16)(mTimeZoneOffsetMin - ANewTimeZoneMin);

            if (utcDeltaMin != 0 && mBaseUTC != null && mEventUTC != null)
            {
                if (mBaseUTC != DateTime.MinValue)
                {
                    mBaseUTC = mBaseUTC.AddMinutes(utcDeltaMin);
                    if (mEventUTC != DateTime.MinValue)
                    {
                        mEventUTC = mEventUTC.AddMinutes(utcDeltaMin);
                    }
                    CProgram.sLogLine("R" + mIndex_KEY.ToString() + " moveTZone " + CProgram.sTimeZoneOffsetStringLong((Int16)mTimeZoneOffsetMin)
                        + "->" + CProgram.sTimeZoneOffsetStringLong((Int16)ANewTimeZoneMin) + "UTC" + CProgram.sTimeZoneOffsetStringLong((Int16)utcDeltaMin)
                        + " " + mFileName);
                    mTimeZoneOffsetMin = ANewTimeZoneMin;
                    bMoved = true;
                    ++mTimeZoneMoved;
                }
                else
                {
                    CProgram.sLogLine("R" + mIndex_KEY.ToString() + " moveTZone " + CProgram.sTimeZoneOffsetStringLong((Int16)mTimeZoneOffsetMin)
                        + "->" + CProgram.sTimeZoneOffsetStringLong((Int16)ANewTimeZoneMin) + "UTC not set!"
                        + " " + mFileName);
                }
            }
            return bMoved;
        }
        public string mGetTimeZoneMovedChar()
        {
            string s = "";

            if (mTimeZoneMoved > 0)
            {
                string nrs = "⁰¹²³⁴⁵⁶⁷⁹";
                s = mTimeZoneMoved > 9 ? "ⁿ" : "" + nrs[mTimeZoneMoved];
            }
            return s;
        }

        public string mGetEventTimeCheckedTimeZone(CRecordMit AFileRec, string LogHeader)
        {
            DateTime dt = mGetDeviceTime(mEventUTC);
            string strEvent = CProgram.sDateTimeToString(dt) + " " + CProgram.sTimeZoneOffsetString((Int16)mTimeZoneOffsetMin, DTimeZoneShown.Long);
            if (AFileRec != null)
            {
                strEvent += AFileRec.mGetTimeZoneMovedChar();

                double stripSec = AFileRec.mGetSamplesTotalTime();
                double secRel = (mEventUTC - AFileRec.mBaseUTC).TotalSeconds;// mBaseUTC is not in RecordDB
                int deltaZ = AFileRec.mTimeZoneOffsetMin - mTimeZoneOffsetMin;
                bool bLog = false;

                if (secRel > stripSec + 2 || secRel < -2)
                {
                    strEvent += "Δe";
                    bLog = true;
                }
                if (deltaZ != 0)
                {
                    strEvent += "Δz";
                    bLog = true;
                }
                if (bLog)
                {
                    CProgram.sLogError(LogHeader + " R" + mIndex_KEY.ToString() + " eUTC" + CProgram.sDateTimeToYMDHMS(mEventUTC)
                        + " " + CProgram.sTimeZoneOffsetString((Int16)mTimeZoneOffsetMin, DTimeZoneShown.Long)
                        + " file " + CProgram.sDateTimeToYMDHMS(AFileRec.mBaseUTC) + " " + stripSec.ToString("0.0") + " Δt=" + secRel.ToString("0.0")
                        + " ΔZ=" + deltaZ.ToString() + " " + mFileName
                        );
                }
            }
            else
            {
                strEvent += mGetTimeZoneMovedChar();
            }
            return strEvent;
        }

        public static bool sbIsManualEvent(string AEventTypeString)
        {
            bool bManual = false;

            if (AEventTypeString != null && AEventTypeString.Length > 0)
            {
                string lower = AEventTypeString.ToLower();

                bManual = lower.Contains("manual");
            }
            return bManual;

        }
        public bool mbIsManualEventType()
        {
            return CRecordMit.sbIsManualEvent(mEventTypeString);
        }

        public static void sSplitFullName(String AFullName, out string ArFirstName, out string ArMiddleName, out string ArLastName)
        {
            string firstName = "";
            string middleName = "";
            string lastName = "";

            if (AFullName != null && AFullName.Length > 0)
            {
                var parts = AFullName.Split(',');
                int nParts = parts.Count();
                if (nParts >= 1)
                {
                    lastName = parts[0] == null ? "" : parts[0].ToString().Trim();
                }
                if (nParts == 2)
                {
                    firstName = parts[1] == null ? "" : parts[1].ToString().Trim();
                }
                else if (nParts >= 3)
                {
                    middleName = parts[1] == null ? "" : parts[1].ToString().Trim();
                    firstName = (parts[2] == null ? "" : parts[2].ToString()).Trim();
                }
            }
            ArFirstName = firstName;
            ArMiddleName = middleName;
            ArLastName = lastName;

        }
        public void mGetPatientName(out string ArFirstName, out string ArMiddleName, out string ArLastName)
        {
            sSplitFullName(mPatientTotalName.mDecrypt(), out ArFirstName, out ArMiddleName, out ArLastName);

        }
        public string mGetDataInfo(string AHeader, float AStartSec, float AEndSec)
        {
            string s = AHeader + " t(";

            try
            {
                float totalLengthSec = mGetSamplesTotalTime();
                float startSec = AStartSec < 0.0F ? 0.0F : AStartSec;
                float endSec = AEndSec < 0.0F ? totalLengthSec : (AEndSec < totalLengthSec ? AEndSec : totalLengthSec);
                float stripSec = endSec - startSec;
                float samplesPerSec = mSampleFrequency;

                UInt32 startIndex = mGetSampleIndex(startSec);
                UInt32 endIndex = mGetSampleIndex(endSec);
                Int32 nrPoints = (Int32)(endIndex - startIndex);
                DateTime startUtc = mGetSampleUTC(startIndex);
                double amplifier = mSampleUnitA <= 0.000001 ? 0 : (1.0 / mSampleUnitA + 0.5);

                s += startSec.ToString("0.0000") + "..." + endSec.ToString("0.0000") + "= " + stripSec.ToString("0.0000") + " sec)["
                    + startIndex.ToString() + "..." + endIndex.ToString() + "= " + nrPoints.ToString() + "] @"
                    + samplesPerSec.ToString() + " Sps "
                    + mSampleUnitA.ToString("0.000000") + " mV/1= " + amplifier.ToString("0.0") + "/mV "
                    + mAdcBits.ToString() + " bits(" + (1 << mAdcBits).ToString() + ") "
                    + mGetDeviceTimeString(startUtc, DTimeZoneShown.Long);

                if (nrPoints <= 0)
                {
                    s += "\r\n no time range!";
                }
                else
                {
                    for (int iChannel = 0; iChannel < mNrSignals; ++iChannel)
                    {
                        string line = "\r\n" + AHeader + "[" + iChannel.ToString() + " " + mGetChannelName((UInt16)iChannel) + "]:";

                        CSignalData signal = mSignals[iChannel];


                        int[] values = signal == null ? null : signal.mValues;
                        if (values == null)
                        {
                            line += " No signal";
                        }
                        else
                        {

                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;

                            for (UInt32 i = startIndex; i <= endIndex; ++i)
                            {
                                v = values[i];
                                if (v < minValue) minValue = v;
                                if (v > maxValue) maxValue = v;
                            }
                            Int32 rangeValue = (maxValue - minValue + 1);

                            float minA = minValue * mSampleUnitA;
                            float maxA = maxValue * mSampleUnitA;
                            float rangeA = rangeValue * mSampleUnitA;

                            line += " A[" + minValue.ToString() + "..." + maxValue.ToString() + "= " + rangeValue.ToString()
                                + "]=(" + minA.ToString("0.00000") + "..." + maxA.ToString("0.00000") + "= " + rangeA.ToString("0.00000") + " mV)";

                        }
                        s += line;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Record Data Info error " + s, ex);

            }
            return s;
        }
        public string mGetCountInfo(string AHeader, float AStartSec, float AEndSec,
            bool AbCountDelta, float ATestLevel, float AHysteryLevel, float ASmoothPeriodSec, UInt16 ADoChannel)
        {
            string mode = AbCountDelta ? "Δ" : "";
            string s = "Count" + mode + " " + AHeader + " t(";

            try
            {
                float totalLengthSec = mGetSamplesTotalTime();
                float startSec = AStartSec < 0.0F ? 0.0F : AStartSec;
                float endSec = AEndSec < 0.0F ? totalLengthSec : (AEndSec < totalLengthSec ? AEndSec : totalLengthSec);
                float stripSec = endSec - startSec;
                float samplesPerSec = mSampleFrequency;
                float levelHigh = ATestLevel + AHysteryLevel * 0.5F;
                float levelLow = ATestLevel - AHysteryLevel * 0.5F;

                Int32 firstChannel = ADoChannel >= mNrSignals ? 0 : ADoChannel;
                Int32 lastChannel = ADoChannel >= mNrSignals ? mNrSignals - 1 : ADoChannel;

                Int32 valueHigh = (Int32)(levelHigh * mAdcGain + 0.5);
                Int32 valueLow = (Int32)(levelLow * mAdcGain + 0.5);

                UInt32 smoothN = (UInt32)(ASmoothPeriodSec * samplesPerSec);

                UInt32 startIndex = mGetSampleIndex(startSec);
                UInt32 endIndex = mGetSampleIndex(endSec);
                Int32 nrPoints = (Int32)(endIndex - startIndex);
                DateTime startUtc = mGetSampleUTC(startIndex);
                double amplifier = mSampleUnitA <= 0.000001 ? 0 : (1.0 / mSampleUnitA + 0.5);

                s += startSec.ToString("0.0000") + "..." + endSec.ToString("0.0000") + "= " + stripSec.ToString("0.0000") + " sec)["
                    + startIndex.ToString() + "..." + endIndex.ToString() + "= " + nrPoints.ToString() + "] @"
                    + samplesPerSec.ToString() + " Sps "
                    + ",  test level " + levelHigh.ToString("0.0000") + "..." + levelLow.ToString("0.0000") + " mV[" + levelHigh + "..." + levelLow + "]"
                    + ", smooth " + ASmoothPeriodSec.ToString("0.00") + "sec [" + smoothN + "]";

                if (nrPoints <= 0)
                {
                    s += "\r\n no time range!";
                }
                else
                {
                    for (int iChannel = firstChannel; iChannel <= lastChannel; ++iChannel)
                    {
                        string line = "\r\nCount" + mode + " " + AHeader + "[" + iChannel.ToString() + " " + mGetChannelName((UInt16)iChannel) + "]:";

                        CSignalData signal = mSignals[iChannel];


                        int[] values = signal == null ? null : signal.mValues;
                        if (values == null)
                        {
                            line += " No signal";
                        }
                        else
                        {
                            Int32 avgValue = values[0];
                            Int64 avgSum = avgValue * smoothN;
                            Int32 prevValue = avgValue;

                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;

                            UInt32 countToHigh = 0;
                            UInt32 countToLow = 0;
                            UInt32 countPulse = 0;
                            UInt32 indexFirstToHigh = 0;
                            UInt32 indexFirstToLow = 0;
                            UInt32 indexLastToHigh = 0;
                            UInt32 indexLastToLow = 0;
                            bool state = avgValue >= valueHigh;


                            //                            UInt64 sumHighPeriod = 0;
                            //                          UInt64 sumLowPeriod = 0;
                            UInt64 sumPulsePeriod = 0;

                            //                            UInt32 nHighPeriod = 0;
                            //                          UInt32 nLowPeriod = 0;
                            UInt32 nPulsePeriod = 0;


                            for (UInt32 i = 0; i <= endIndex; ++i)
                            {
                                v = values[i];

                                if (smoothN > 0)
                                {
                                    avgSum -= avgValue;         // calculate smoothed value
                                    avgSum += v;
                                    avgValue = (Int32)(avgSum / smoothN);
                                    v -= avgValue;              // signal relative to smoothed value
                                }

                                if (AbCountDelta)
                                {
                                    Int32 vDelta = v - prevValue;   // count  derived delta
                                    prevValue = v;
                                    v = vDelta;
                                }

                                if (i >= startIndex)
                                {
                                    if (v < minValue) minValue = v; // min max over area
                                    if (v > maxValue) maxValue = v;

                                    if (state)
                                    {   // high
                                        if (v < valueLow)
                                        {
                                            state = false;  // high -> low
                                            ++countToLow;
                                            if (indexFirstToLow == 0) indexFirstToLow = i;
                                            indexLastToLow = i;

                                            if (indexLastToHigh > 0)
                                            {
                                                uint p = i - indexLastToHigh;

                                                sumPulsePeriod += p;
                                                ++nPulsePeriod;
                                            }
                                        }
                                    }
                                    else
                                    {   // low
                                        if (v > valueHigh)
                                        {
                                            state = true;  // low -> high
                                            ++countToHigh;
                                            if (indexFirstToHigh == 0) indexFirstToHigh = i;
                                            indexLastToHigh = i;
                                        }
                                    }

                                }
                            }
                            Int32 rangeValue = (maxValue - minValue + 1);

                            float minA = minValue * mSampleUnitA;
                            float maxA = maxValue * mSampleUnitA;
                            float rangeA = rangeValue * mSampleUnitA;
                            float t, tAvg;

                            line += " A[" + minValue.ToString() + "..." + maxValue.ToString() + "= " + rangeValue.ToString()
                                + "]=(" + minA.ToString("0.00000") + "..." + maxA.ToString("0.00000") + "= " + rangeA.ToString("0.00000") + " mV)";
                            t = (indexLastToHigh - indexFirstToHigh) / (float)mSampleFrequency;
                            tAvg = countToHigh == 0 ? 0.0F : t / countToHigh;
                            line += "\r\n\t\tLow2High(" + countToHigh + ", " + (countToHigh == 0 ? "" : t.ToString("0.000") + "sec, ") + tAvg.ToString("0.000") + " sec)";

                            t = (indexLastToLow - indexFirstToLow) / (float)mSampleFrequency;
                            tAvg = countToLow == 0 ? 0.0F : t / countToLow;
                            line += ", High2Low(" + countToLow + ", " + (countToLow == 0 ? "" : t.ToString("0.000") + "sec, ") + tAvg.ToString("0.000") + " sec)";

                            t = (sumPulsePeriod) / (float)mSampleFrequency;
                            tAvg = nPulsePeriod == 0 ? 0.0F : t / nPulsePeriod;
                            line += ", Pulse(" + nPulsePeriod + ", " + tAvg.ToString("0.000") + " sec)";

                        }
                        s += line;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Record Data Info error " + s, ex);

            }
            return s;
        }
        public string mGetCompressInfo(uint ADevider)
        {
            string firstLine = "Compress(" + ADevider + ") file ";
            string header = "            ";
            string s = "";
            Int32 devider = ADevider >= 1 ? (Int32)ADevider : 1;

            if (ADevider >= 10) header += " ";

            try
            {
                UInt32 nrChannels = mNrSignals;
                UInt32 nrSamples = mNrSamples;
                UInt32 orgNrBytes = nrSamples * 3;
                UInt32 orgFileBytes = nrChannels * orgNrBytes;

                UInt32 totalCount1Byte = 0;  // count 1 byte needed
                UInt32 totalCount2Byte = 0;  // count 2 byte needed
                UInt32 totalCount3Byte = 0;  // count 3 byte needed
                UInt32 totalCountXByte = 0;  // count more bytes needed
                UInt32 totalCountYByte = 0;  // count nr values needed to catch up
                UInt32 totalNewNrBytes = 0;
                UInt64 totalDelta = 0;

                Int32 totalMinDelta = Int32.MaxValue, totalMaxDelta = Int32.MinValue;

                UInt32 max1Byte = (UInt32)((1 << 6) - 1);
                UInt32 max2Byte = (UInt32)((1 << 13) - 1);
                UInt32 max3Byte = (UInt32)((1 << 20) - 1);

                double unitValue = mSampleUnitA * ADevider;
                double valueMax1Byte = max1Byte * unitValue;
                double valueMax2Byte = max2Byte * unitValue;
                double valueMax3Byte = max3Byte * unitValue;

                firstLine += unitValue.ToString("0.00000") + "mV/1 1{" + max1Byte + "=" + valueMax1Byte.ToString("0.0000")
                            + "mV} 2{" + max2Byte + "=" + valueMax2Byte.ToString("0.0000")
                            + "mV} 3{" + max3Byte + "=" + valueMax3Byte.ToString("0.0000") + "mV}"
                            + " " + nrChannels + "x" + orgNrBytes + "=" + orgFileBytes + "B";
                s = firstLine;

                for (int iChannel = 0; iChannel < nrChannels; ++iChannel)
                {
                    string line = "\r\n" + header + "[" + iChannel.ToString() + " " + mGetChannelName((UInt16)iChannel) + "]: ";

                    CSignalData signal = mSignals[iChannel];


                    int[] values = signal == null ? null : signal.mValues;
                    if (values == null)
                    {
                        line += " No signal";
                    }
                    else
                    {

                        UInt32 count1Byte = 0;  // count 1 byte needed
                        UInt32 count2Byte = 0;  // count 2 byte needed
                        UInt32 count3Byte = 0;  // count 3 byte needed
                        UInt32 countXByte = 0;  // count more bytes needed
                        UInt32 countYByte = 0;  // count nr values needed to catch up
                        UInt32 newNrBytes = 7;
                        UInt64 sumDelta = 0;

                        Int32 prevValue = values[0];
                        int v, delta;

                        Int32 minDelta = Int32.MaxValue, maxDelta = Int32.MinValue;

                        for (UInt32 i = 1; i < nrSamples; ++i)
                        {
                            v = values[i];

                            delta = (v - prevValue) / devider;

                            if (delta < minDelta)
                            {
                                minDelta = delta;
                            }
                            if (delta > maxDelta)
                            {
                                maxDelta = delta;
                            }

                            if (delta < 0)
                            {
                                delta = -delta;
                            }
                            sumDelta += (UInt32)delta;

                            if (delta <= max1Byte)
                            {
                                ++count1Byte;
                            }
                            else if (delta <= max2Byte)
                            {
                                ++count2Byte;
                            }
                            else if (delta <= max3Byte)
                            {
                                ++count3Byte;
                            }
                            else
                            {
                                ++countXByte;

                                long y = delta / max3Byte;

                                countYByte += (UInt32)(1 + y);    // extra values neaded to get onto value
                            }

                            prevValue = v;
                        }
                        double per1 = count1Byte / (double)nrSamples * 100;
                        double per2 = count2Byte / (double)nrSamples * 100;
                        double per3 = count3Byte / (double)nrSamples * 100;
                        double perX = countXByte / (double)nrSamples * 100;
                        double avgDelta = sumDelta / (double)nrSamples;
                        double avgVal = avgDelta * unitValue;

                        newNrBytes += count1Byte + count2Byte * 2 + count3Byte * 3;
                        double comprPer = newNrBytes / (double)orgNrBytes * 100;
                        line += "123XY{" + count1Byte + ", " + count2Byte + ", " + count3Byte + ", " + countXByte + ", ++" + +countYByte + "}";
                        line += "%{" + per1.ToString("0.0") + ", " + per2.ToString("0.0") + ", " + per3.ToString("0.0") + ", " + perX.ToString("0.0") + "} ";
                        line += minDelta + "< " + maxDelta + " avg=" + avgDelta.ToString("0.0") + "=" + avgVal.ToString("0.0000") + "mV ";
                        line += newNrBytes + "Bytes " + comprPer.ToString("0.0") + "%";

                        s += line;


                        totalCount1Byte += count1Byte;  // count 1 byte needed
                        totalCount2Byte += count2Byte;  // count 2 byte needed
                        totalCount3Byte += count3Byte;  // count 3 byte needed
                        totalCountXByte += countXByte;  // count more bytes needed
                        totalCountYByte += countYByte;  // count nr values needed to catch up
                        totalNewNrBytes += newNrBytes;
                        totalDelta += sumDelta;

                        if (minDelta < totalMinDelta)
                        {
                            totalMinDelta = minDelta;
                        }
                        if (maxDelta > totalMaxDelta)
                        {
                            totalMaxDelta = maxDelta;
                        }
                    }
                }
                if (totalNewNrBytes > 0)
                {
                    UInt32 n = nrSamples * nrChannels;

                    double per1 = totalCount1Byte / (double)n * 100;
                    double per2 = totalCount2Byte / (double)n * 100;
                    double per3 = totalCount3Byte / (double)n * 100;
                    double perX = totalCountXByte / (double)n * 100;

                    double avgDelta = totalDelta / (double)n;
                    double avgVal = avgDelta * unitValue;
                    double minVal = totalMinDelta * unitValue;
                    double maxVal = totalMaxDelta * unitValue;

                    double comprPer = totalNewNrBytes / (double)orgFileBytes * 100;
                    double comprFactor = orgFileBytes / (double)totalNewNrBytes;
                    string line = "\r\n" + header + "file ";
                    line += "123XY{" + totalCount1Byte + ", " + totalCount2Byte + ", " + totalCount3Byte + ", " + totalCountXByte + ", ++" + +totalCountYByte + "}";
                    line += "%{" + per1.ToString("0.0") + ", " + per2.ToString("0.0") + ", " + per3.ToString("0.0") + ", " + perX.ToString("0.0") + "} ";
                    line += minVal.ToString("0.0000") + "mV < " + maxVal.ToString("0.0000") + "mV avg=" + avgDelta.ToString("0.0") + "=" + avgVal.ToString("0.0000") + "mV ";
                    line += totalNewNrBytes + "Bytes " + comprPer.ToString("0.0") + "% compr=" + comprFactor.ToString("0.000");

                    s += line;
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Record Data compress error " + s, ex);

            }
            return s;
        }
        static public bool sbCheckDoFillGap(double AGapSec, UInt32 AIndex, UInt16 AMaxHalfBlockSize = 10, float ABlockDiffSec = 0.5F)
        {
            float maxSec = 3.0F + (AIndex + 1) * ABlockDiffSec;

            if (maxSec > AMaxHalfBlockSize)
            {
                maxSec = AMaxHalfBlockSize;
            }
            return AGapSec > maxSec;
        }

        public Image mCreateHrChartImage(CAnnotation AAnotation, CStripChart AChart, int AWidth, int AHeight,
            UInt16 AMinHR, UInt16 ALowHR, UInt16 ABaseHR, UInt16 AHighHR, UInt16 AMaxHR,
            float ACursorStartT, float ACursorWidthSec, DChartDrawTo ADrawTo, bool bPlotBorder,
            out float ArHrMean, out float ArTotalBurden,
            out float ArHrLowBurden, out float ArHrHighBurden, out float ArMinHrBpm, out float ArMaxHrBpm)
        {
            Bitmap img = null;
            float highHrBurden = 0.0F;
            float lowHrBurden = 0.0F;
            float minHR = 9999;
            float maxHR = 0;
            float meanHR = 0;
            float totalBurden = 0;

            if (AWidth > 2 && AHeight > 2 && AChart != null && ALowHR > 0 && AHighHR > 0)
            {
                if (AAnotation != null && AAnotation.mAnnotationsList != null)
                {
                    try
                    {
                        float startTimeSec = AChart.mGetStartTimeSec();
                        float timeDurationSec = AChart.mGetStripTimeSec();
                        float highHrSec = 60.0F / AHighHR;
                        float lowHrSec = 60.0F / ALowHR;
                        float highHrTotalSec = 0.0F;
                        float lowHrTotalSec = 0.0F;
                        float normalHrTotalSec = 0.0F;
                        int highHrCount = 0;
                        int lowHrCount = 0;
                        int normalHrCount = 0;

                        img = new Bitmap(AWidth, AHeight);

                        using (Graphics graphics = Graphics.FromImage(img))
                        {

                            Color backGroundColor = Color.White;
                            Color borderColor = Color.Black;
                            float plotWidth = CDvtmsData.sGetSignalWidth(ADrawTo);
                            Color eventColor = CDvtmsData.sGetPrintColor(ADrawTo, Color.Red); // DarkBlue;
                            Color cursorBackground = CDvtmsData.sGetPrintColor(ADrawTo, Color.LightBlue);
                            Color colorHighHr = Color.Red; Pen penHighHr = new Pen(colorHighHr);
                            Color colorNormalHr = Color.Black; Pen penNormalHr = new Pen(colorNormalHr);
                            Color colorBaseHr = Color.LightGreen; Pen penBaseHr = new Pen(colorBaseHr);
                            Color colorLowHr = Color.Blue; Pen penLowHr = new Pen(colorLowHr);
                            Pen pen = new Pen(borderColor);

                            int w = img.Width - 1;
                            int h = img.Height - 1;
                            // clear image
                            graphics.Clear(backGroundColor);
                            // draw boundry box
                            if (bPlotBorder)
                            {
                                graphics.DrawLine(pen, 0, 0, w, 0);
                                graphics.DrawLine(pen, w, 0, w, h);
                                graphics.DrawLine(pen, w, h, 0, h);
                                graphics.DrawLine(pen, 0, h, 0, 0);
                            }
                            // setup chart
                            if (AChart.mbSetGraphMaxA(1, 1, w - 1, h - 1, startTimeSec, timeDurationSec, AMinHR, AMaxHR, 10))
                            {

                                int x = AChart.mGetPixelT(AChart.mGetStartTimeSec() + 0.5F * AChart.mGetStripTimeSec());
                                int y = AChart.mGetPixelA(0.5F * (AChart.mGetEndA() + AChart.mGetStartA()));

                                //                                    if( ACursorStartT +  >= -0.01F )
                                if (ACursorWidthSec > 0)    // draw cursor
                                {
                                    float startT = ACursorStartT <= 0 ? 0.0F : ACursorStartT;
                                    float endT = ACursorStartT + ACursorWidthSec;
                                    if (endT > AChart.mGetStripTimeSec()) endT = AChart.mGetStripTimeSec();
                                    int x0 = AChart.mGetPixelT(startT);
                                    int x1 = AChart.mGetPixelT(endT);
                                    if (x0 > 0 && x1 >= x0)
                                    {
                                        Brush brushCursor = new SolidBrush(cursorBackground);
                                        graphics.FillRectangle(brushCursor, x0, 1, x1 - x0 + 1, h - 2);
                                    }
                                }
                                y = AChart.mGetPixelA(ABaseHR);     // baseHR line
                                graphics.DrawLine(penBaseHr, 0, y, w, y);
                                x = AChart.mGetPixelT(mGetEventSec());
                                if (x > 0 && x < w)
                                {
                                    graphics.DrawLine(penBaseHr, x, 1, x, w - 1);   // event

                                }
                                // plot HR graph
                                //                                   
                                Int32 nBeats = 0;// first beat the RR time can not be calculated
                                int prevX = 0;
                                int prevY = 0;
                                float prevRSec = -9999;
                                float prevHR = 0;
                                float rrSec = 0;
                                float rrHr = 0;

                                UInt32 tTick;
                                UInt16 code;
                                float t;
                                float tStart = AChart.mGetStartTimeSec();
                                float tEnd = tStart + AChart.mGetStripTimeSec();


                                foreach (CAnnotationLine ann in mAnnotationAtr.mAnnotationsList)
                                {
                                    code = ann.mAnnotationCode;
                                    if (CAnnotationLine.sbIsBeat(code))
                                    {
                                        tTick = ann.mAnnotationTimeTck;
                                        t = mAnnotationAtr.mGetTimeSec(tTick);

                                        if (++nBeats > 1)
                                        {
                                            rrSec = t - prevRSec;
                                            if (rrSec > 0.01)
                                            {
                                                rrHr = 60.0F / rrSec;

                                                if (t > tStart && t < tEnd)
                                                {
                                                    if (rrHr > maxHR)
                                                    {
                                                        maxHR = rrHr;
                                                    }
                                                    if (rrHr < minHR)
                                                    {
                                                        minHR = rrHr;
                                                    }
                                                    pen = penNormalHr;

                                                    if (rrHr > AHighHR)
                                                    {
                                                        highHrTotalSec += rrSec;
                                                        ++highHrCount;
                                                        pen = penHighHr;
                                                        if (rrHr > AMaxHR)
                                                        {
                                                            rrHr = AMaxHR;
                                                        }
                                                    }
                                                    else if (rrHr < ALowHR)
                                                    {
                                                        lowHrTotalSec += rrSec;
                                                        ++lowHrCount;
                                                        pen = penLowHr;
                                                        if (rrHr < AMinHR)
                                                        {
                                                            rrHr = AMinHR;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        normalHrTotalSec += rrSec;
                                                        ++normalHrCount;
                                                        pen = penNormalHr;
                                                    }
                                                    x = AChart.mGetPixelT(t);
                                                    if (x > 0 && x < w)
                                                    {
                                                        y = AChart.mGetPixelA(rrHr);

                                                        if (rrSec > 10)
                                                        {
                                                            prevX = x - 1;  // prev R to far away, draw from HR 0
                                                            prevY = h;
                                                        }
                                                        else if (nBeats == 2)
                                                        {
                                                            // first draw
                                                            prevX = x - 1;
                                                            prevY = y;
                                                        }
                                                        graphics.DrawLine(pen, prevX, prevY, x, y);     // thick line
                                                        if (rrHr > ABaseHR)
                                                        {
                                                            graphics.DrawLine(pen, prevX, prevY - 1, x, y - 1);
                                                        }
                                                        else
                                                        {
                                                            graphics.DrawLine(pen, prevX, prevY + 1, x, y + 1);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            x = AChart.mGetPixelT(t);
                                            y = AChart.mGetPixelA(rrHr);
                                        }
                                        prevX = x;
                                        prevY = y;
                                        prevHR = rrHr;
                                        prevRSec = t;
                                    }
                                }
                                graphics.Dispose();
                            }
                        }
                        float tTotal = normalHrTotalSec + highHrTotalSec + lowHrTotalSec;
                        if (tTotal > 1)
                        {
                            int nrBeats = normalHrCount + lowHrCount + highHrCount;
                            if (highHrCount > 0)
                            {
                                highHrBurden = highHrTotalSec / tTotal * 100.0F;
                            }
                            if (lowHrCount > 0)
                            {
                                lowHrBurden = lowHrTotalSec / tTotal * 100.0F;
                            }
                            totalBurden = tTotal / timeDurationSec * 100.0F;
                            meanHR = nrBeats == 0 ? 0 : 60.0F / (tTotal / nrBeats);
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            ArHrLowBurden = lowHrBurden;
            ArHrHighBurden = highHrBurden;
            ArMinHrBpm = minHR;
            ArMaxHrBpm = maxHR;
            ArHrMean = meanHR;
            ArTotalBurden = totalBurden;
            return img;
        }

        public bool mbDoHighPassFilter(float AFreq, bool AbAllChannels, UInt16 AChannelNr)
        {
            bool bOk = false;

            try
            {
                float samplesPerSec = mSampleFrequency;

                Int32 firstChannel = AbAllChannels ? 0 : AChannelNr;
                Int32 lastChannel = AbAllChannels ? mNrSignals - 1 : AChannelNr;
                Int32 nrPoints = (Int32)(mNrSamples);

                if (nrPoints > 0)
                {
                    HighPass1RCFilter filter = new HighPass1RCFilter(AFreq, mSampleFrequency);

                    if (filter == null)
                    {
                        return false;
                    }

                    for (int iChannel = firstChannel; iChannel <= lastChannel; ++iChannel)
                    {
                        CSignalData signal = mSignals[iChannel];

                        int[] values = signal == null ? null : signal.mValues;
                        if (values == null)
                        {
                            return false;
                        }
                        else
                        {
                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;

                            filter.mReset();

                            for (UInt32 i = 0; i < nrPoints; ++i)
                            {
                                v = values[i];

                                v = filter.mDoFilter(v);
                                values[i] = v;
                                if (v < minValue) minValue = v; // min max over area
                                if (v > maxValue) maxValue = v;
                            }
                            signal.mMinValue = minValue;
                            signal.mMaxValue = maxValue;
                        }
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("High Pass Filter", ex);
            }

            return bOk;

        }
        public bool mbDoLowPassFilter(float AFreq, bool AbAllChannels, UInt16 AChannelNr)
        {
            bool bOk = false;

            try
            {
                float samplesPerSec = mSampleFrequency;

                Int32 firstChannel = AbAllChannels ? 0 : AChannelNr;
                Int32 lastChannel = AbAllChannels ? mNrSignals - 1 : AChannelNr;


                Int32 nrPoints = (Int32)(mNrSamples);


                if (nrPoints > 0)
                {
                    LowPass1RCFilter filter = new LowPass1RCFilter(AFreq, mSampleFrequency);

                    if (filter == null)
                    {
                        return false;
                    }


                    for (int iChannel = firstChannel; iChannel <= lastChannel; ++iChannel)
                    {
                        CSignalData signal = mSignals[iChannel];

                        int[] values = signal == null ? null : signal.mValues;
                        if (values == null)
                        {
                            return false;
                        }
                        else
                        {
                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;

                            filter.mReset();

                            for (UInt32 i = 0; i < nrPoints; ++i)
                            {
                                v = values[i];

                                v = filter.mDoFilter(v);
                                values[i] = v;
                                if (v < minValue) minValue = v; // min max over area
                                if (v > maxValue) maxValue = v;

                            }
                            signal.mMinValue = minValue;
                            signal.mMaxValue = maxValue;



                        }
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Low Pass Filter", ex);
            }

            return bOk;

        }
        public bool mbDoButherworth3Filter(DFilterType AFilterType, float AFreq, bool AbAllChannels, UInt16 AChannelNr)
        {
            bool bOk = false;

            try
            {
                float samplesPerSec = mSampleFrequency;

                Int32 firstChannel = AbAllChannels ? 0 : AChannelNr;
                Int32 lastChannel = AbAllChannels ? mNrSignals - 1 : AChannelNr;
                Int32 nrPoints = (Int32)(mNrSamples);

                if (nrPoints > 0)
                {
                    CButherworth3Filter filter = new CButherworth3Filter(AFilterType, AFreq, mSampleFrequency);

                    if (filter == null)
                    {
                        return false;
                    }

                    for (int iChannel = firstChannel; iChannel <= lastChannel; ++iChannel)
                    {
                        CSignalData signal = mSignals[iChannel];

                        int[] values = signal == null ? null : signal.mValues;
                        if (values == null)
                        {
                            return false;
                        }
                        else
                        {
                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;

                            filter.mReset();

                            for (UInt32 i = 0; i < nrPoints; ++i)
                            {
                                v = values[i];

                                v = filter.mDoFilter(v);
                                values[i] = v;
                                if (v < minValue) minValue = v; // min max over area
                                if (v > maxValue) maxValue = v;
                            }
                            signal.mMinValue = minValue;
                            signal.mMaxValue = maxValue;
                        }
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("High Pass Filter", ex);
            }

            return bOk;

        }

        public bool mbDoBandBlockAvgHistoryFilter(float AFreq, bool AbAllChannels, UInt16 AChannelNr)
        {
            bool bOk = false;

            try
            {
                float samplesPerSec = mSampleFrequency;

                Int32 firstChannel = AbAllChannels ? 0 : AChannelNr;
                Int32 lastChannel = AbAllChannels ? mNrSignals - 1 : AChannelNr;
                Int32 nrPoints = (Int32)(mNrSamples);
                UInt16 nrAvgBlocks = 3; // at 200/1000 sps the 60Hz and 50 Hz the 3 blocks are on exact nr samples
                                        //     3 * sps/f     10 / 50 and  12 / 60        
                float avgBlocksFactor = 1.0F;
                UInt16 avgNPoints = 6;

                if (nrPoints > 0)
                {
                    CBandBlockAvgHistFilter filter = new CBandBlockAvgHistFilter(AFreq, (UInt32)samplesPerSec, nrAvgBlocks, avgBlocksFactor, avgNPoints);

                    if (filter == null)
                    {
                        return false;
                    }

                    for (int iChannel = firstChannel; iChannel <= lastChannel; ++iChannel)
                    {
                        CSignalData signal = mSignals[iChannel];

                        int[] values = signal == null ? null : signal.mValues;
                        if (values == null)
                        {
                            return false;
                        }
                        else
                        {
                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;

                            filter.mReset();

                            for (UInt32 i = 0; i < nrPoints; ++i)
                            {
                                v = values[i];

                                v = filter.mDoFilter(v);
                                values[i] = v;
                                if (v < minValue) minValue = v; // min max over area
                                if (v > maxValue) maxValue = v;
                            }
                            signal.mMinValue = minValue;
                            signal.mMaxValue = maxValue;
                        }
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Band Stop AvgHistory Filter", ex);
            }

            return bOk;

        }

        public bool mbDoClipResetFilter(float AClipAmplitudemV, bool AbAllChannels, UInt16 AChannelNr, float AResetAmplitudemV)
        {
            bool bOk = false;

            try
            {
                UInt32 samplesPerSec = (UInt32)(mSampleFrequency + 0.5);

                Int32 firstChannel = AbAllChannels ? 0 : AChannelNr;
                Int32 lastChannel = AbAllChannels ? mNrSignals - 1 : AChannelNr;
                Int32 nrPoints = (Int32)(mNrSamples);

                if (nrPoints > 0)
                {
                    CClipResetFilter filter = new CClipResetFilter(AClipAmplitudemV, AResetAmplitudemV, mAdcGain);

                    if (filter == null)
                    {
                        return false;
                    }

                    for (int iChannel = firstChannel; iChannel <= lastChannel; ++iChannel)
                    {
                        CSignalData signal = mSignals[iChannel];

                        int[] values = signal == null ? null : signal.mValues;
                        if (values == null)
                        {
                            return false;
                        }
                        else
                        {
                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;

                            filter.mReset();

                            for (UInt32 i = 0; i < nrPoints; ++i)
                            {
                                v = values[i];

                                v = filter.mDoFilter(v);
                                values[i] = v;
                                if (v < minValue) minValue = v; // min max over area
                                if (v > maxValue) maxValue = v;
                            }
                            signal.mMinValue = minValue;
                            signal.mMaxValue = maxValue;
                        }
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("ClipReset Filter", ex);
            }

            return bOk;

        }

        public bool mbDoShiftTimeSignal(CSignalData ASignal, float AShiftTimeSec)
        {
            bool bOk = false;

            try
            {
                UInt32 samplesPerSec = (UInt32)(mSampleFrequency + 0.5);

                UInt32 nrPoints = (UInt32)(ASignal != null ? ASignal.mNrValues : 0);
                Int32 sampleShift = (Int32)(AShiftTimeSec * mSampleFrequency + 0.5);
                bool bBack = sampleShift < 0;

                if (bBack)
                {
                    sampleShift = -sampleShift;
                }
                if (nrPoints > sampleShift && sampleShift > 0)
                {
                    int[] values = ASignal.mValues;
                    if (values == null)
                    {
                        return false;
                    }
                    else
                    {
                        Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;
                        if (bBack)
                        {
                            Int32 iFrom;

                            v = 0;
                            for (Int32 i = 0; i < nrPoints; ++i)
                            {
                                iFrom = i + sampleShift;
                                if (iFrom < nrPoints)
                                {
                                    v = values[iFrom];
                                }
                                else
                                {
                                    // fill last point with last value
                                }
                                values[i] = v;
                                if (v < minValue)
                                {
                                    minValue = v;
                                }// min max over area
                                if (v > maxValue)
                                {
                                    maxValue = v;
                                }
                            }
                        }
                        else
                        {
                            Int32 v0 = values[0];

                            for (UInt32 i = nrPoints; --i > 0;)
                            {
                                if (i > sampleShift)
                                {
                                    v = values[i - sampleShift];
                                }
                                else
                                {
                                    v = v0;
                                }
                                values[i] = v;
                                if (v < minValue)
                                {
                                    minValue = v;
                                }// min max over area
                                if (v > maxValue)
                                {
                                    maxValue = v;
                                }
                            }
                        }
                        ASignal.mMinValue = minValue;
                        ASignal.mMaxValue = maxValue;
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Time shift Signal", ex);
            }
            return bOk;
        }

        public bool mbDoShiftTime(float AShiftTimeSec, bool AbAllChannels, UInt16 AChannelNr)
        {
            bool bOk = false;

            try
            {
                UInt32 samplesPerSec = (UInt32)(mSampleFrequency + 0.5);

                Int32 firstChannel = AbAllChannels ? 0 : AChannelNr;
                Int32 lastChannel = AbAllChannels ? mNrSignals - 1 : AChannelNr;
                UInt32 nrPoints = (UInt32)(mNrSamples);
                Int32 sampleShift = (Int32)(AShiftTimeSec * mSampleFrequency + 0.5);
                bool bBack = sampleShift < 0;

                if (bBack)
                {
                    sampleShift = -sampleShift;
                }
                if (nrPoints > sampleShift && sampleShift > 0)
                {
                    for (int iChannel = firstChannel; iChannel <= lastChannel; ++iChannel)
                    {
                        CSignalData signal = mSignals[iChannel];

                        int[] values = signal == null ? null : signal.mValues;
                        if (values == null)
                        {
                            return false;
                        }
                        else
                        {
                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;
                            if (bBack)
                            {
                                Int32 iFrom;

                                v = 0;
                                for (Int32 i = 0; i < nrPoints; ++i)
                                {
                                    iFrom = i + sampleShift;
                                    if (iFrom < nrPoints)
                                    {
                                        v = values[iFrom];
                                    }
                                    else
                                    {
                                        // fill last point with last value
                                    }
                                    values[i] = v;
                                    if (v < minValue)
                                    {
                                        minValue = v;
                                    }// min max over area
                                    if (v > maxValue)
                                    {
                                        maxValue = v;
                                    }
                                }
                            }
                            else
                            {
                                Int32 v0 = values[0];

                                for (UInt32 i = nrPoints; --i > 0;)
                                {
                                    if (i > sampleShift)
                                    {
                                        v = values[i - sampleShift];
                                    }
                                    else
                                    {
                                        v = v0;
                                    }
                                    values[i] = v;
                                    if (v < minValue)
                                    {
                                        minValue = v;
                                    }// min max over area
                                    if (v > maxValue)
                                    {
                                        maxValue = v;
                                    }
                                }
                            }
                            signal.mMinValue = minValue;
                            signal.mMaxValue = maxValue;
                        }
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Time shift", ex);
            }

            return bOk;

        }

        public bool mbDoAvgHistoryFilter(bool AbUseStoredHistory, float AAvgTimeSec, bool AbAllChannels, UInt16 AChannelNr,
            float AApplyDeltamV, float AApplyFactors)
        {
            bool bOk = false;

            try
            {
                UInt32 samplesPerSec = (UInt32)(mSampleFrequency + 0.5);

                Int32 firstChannel = AbAllChannels ? 0 : AChannelNr;
                Int32 lastChannel = AbAllChannels ? mNrSignals - 1 : AChannelNr;
                Int32 nrPoints = (Int32)(mNrSamples);

                if (nrPoints > 0)
                {
                    CAvgHistoryFilter filter = new CAvgHistoryFilter(AbUseStoredHistory, AAvgTimeSec, samplesPerSec, AApplyDeltamV, mAdcGain, AApplyFactors);

                    if (filter == null)
                    {
                        return false;
                    }

                    for (int iChannel = firstChannel; iChannel <= lastChannel; ++iChannel)
                    {
                        CSignalData signal = mSignals[iChannel];

                        int[] values = signal == null ? null : signal.mValues;
                        if (values == null)
                        {
                            return false;
                        }
                        else
                        {
                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;

                            filter.mReset();

                            for (UInt32 i = 0; i < nrPoints; ++i)
                            {
                                v = values[i];

                                v = filter.mDoFilter(v);
                                values[i] = v;
                                if (v < minValue) minValue = v; // min max over area
                                if (v > maxValue) maxValue = v;
                            }
                            signal.mMinValue = minValue;
                            signal.mMaxValue = maxValue;
                        }
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("AvgHistory Filter", ex);
            }

            return bOk;

        }
        public bool mbDoCopyChannel(UInt16 AChannelNr)
        {
            bool bOk = false;

            try
            {
                if (AChannelNr < mNrSignals)
                {
                    Int32 nrPoints = (Int32)(mNrSamples);
                    UInt16 lastChannel = mNrSignals;
                    UInt16 newNrSignals = (UInt16)(mNrSignals + 1);


                    if (nrPoints > 0)
                    {
                        CSignalData[] newSignalsArray = new CSignalData[newNrSignals];

                        for (int iChannel = 0; iChannel < mNrSignals; ++iChannel)
                        {
                            newSignalsArray[iChannel] = mSignals[iChannel];
                        }

                        CSignalData sourceSignal = mSignals[AChannelNr];
                        CSignalData copySignal = new CSignalData();

                        copySignal.mbCopyFrom(sourceSignal);
                        newSignalsArray[lastChannel] = copySignal;

                        /*
                                                int[] values = signal == null ? null : signal.mValues;
                                                if (values == null)
                                                {
                                                    return false;
                                                }
                                                else
                                                {
                                                    Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;

                                                    filter.mReset();

                                                    for (UInt32 i = 0; i < nrPoints; ++i)
                                                    {
                                                        v = values[i];

                                                        v = filter.mDoFilter(v);
                                                        values[i] = v;
                                                        if (v < minValue) minValue = v; // min max over area
                                                        if (v > maxValue) maxValue = v;
                                                    }
                                                    signal.mMinValue = minValue;
                                                    signal.mMaxValue = maxValue;
                                                }
                                            }
                                                bOk &= rec.mSignals[j].mbCopyFrom(scp.mSignals[j]);
                        */
                        mSignals = newSignalsArray;
                        ++mNrSignals;
                        bOk = true;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Copy channel " + AChannelNr, ex);
            }

            return bOk;

        }
        public bool mbDoDeltaChannel(UInt16 AChannelNr1, UInt16 AChannelNr2)
        {
            bool bOk = false;

            try
            {
                if (AChannelNr1 < mNrSignals && AChannelNr2 < mNrSignals)
                {
                    Int32 nrPoints = (Int32)(mNrSamples);
                    UInt16 lastChannel = mNrSignals;
                    UInt16 newNrSignals = (UInt16)(mNrSignals + 1);


                    if (nrPoints > 0)
                    {
                        CSignalData[] newSignalsArray = new CSignalData[newNrSignals];

                        for (int iChannel = 0; iChannel < mNrSignals; ++iChannel)
                        {
                            newSignalsArray[iChannel] = mSignals[iChannel];
                        }

                        CSignalData sourceSignal = mSignals[AChannelNr1];
                        CSignalData copySignal = new CSignalData();

                        copySignal.mbCopyFrom(sourceSignal);
                        newSignalsArray[lastChannel] = copySignal;

                        CSignalData sourceSignal2 = mSignals[AChannelNr2];



                        int[] values1 = copySignal == null ? null : copySignal.mValues;
                        int[] values2 = sourceSignal2 == null ? null : sourceSignal2.mValues;
                        if (values1 == null || values2 == null)
                        {
                            return false;
                        }
                        else
                        {
                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;


                            for (UInt32 i = 0; i < nrPoints; ++i)
                            {
                                v = values1[i] - values2[i];    // Channel1 - channel2

                                values1[i] = v;
                                if (v < minValue) minValue = v; // min max over area
                                if (v > maxValue) maxValue = v;
                            }
                            copySignal.mMinValue = minValue;
                            copySignal.mMaxValue = maxValue;
                        }

                        mSignals = newSignalsArray;
                        ++mNrSignals;
                        bOk = true;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Delta channel " + AChannelNr1 + " - " + AChannelNr2, ex);
            }

            return bOk;

        }
        public bool mbDoDeriveChannel(UInt16 AChannelNr)
        {
            bool bOk = false;

            try
            {
                if (AChannelNr < mNrSignals)
                {
                    Int32 nrPoints = (Int32)(mNrSamples);
                    UInt16 lastChannel = mNrSignals;
                    UInt16 newNrSignals = (UInt16)(mNrSignals + 1);


                    if (nrPoints > 0)
                    {
                        CSignalData[] newSignalsArray = new CSignalData[newNrSignals];

                        for (int iChannel = 0; iChannel < mNrSignals; ++iChannel)
                        {
                            newSignalsArray[iChannel] = mSignals[iChannel];
                        }

                        CSignalData sourceSignal = mSignals[AChannelNr];
                        CSignalData copySignal = new CSignalData();

                        copySignal.mbCopyFrom(sourceSignal);
                        newSignalsArray[lastChannel] = copySignal;

                        int[] values1 = copySignal == null ? null : copySignal.mValues;

                        if (values1 == null)
                        {
                            return false;
                        }
                        else
                        {
                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v, vOld;
                            int prevValue = values1[0];


                            for (UInt32 i = 0; i < nrPoints; ++i)
                            {
                                vOld = values1[i];
                                v = vOld - prevValue;
                                prevValue = vOld;
                                values1[i] = v;
                                if (v < minValue) minValue = v; // min max over area
                                if (v > maxValue) maxValue = v;
                            }
                            copySignal.mMinValue = minValue;
                            copySignal.mMaxValue = maxValue;
                        }

                        mSignals = newSignalsArray;
                        ++mNrSignals;
                        bOk = true;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Derived channel " + AChannelNr, ex);
            }

            return bOk;

        }
        public bool mbDoDeleteChannel(UInt16 AChannelNr)
        {
            bool bOk = false;

            try
            {
                if (AChannelNr < mNrSignals && mNrSignals > 1)
                {
                    Int32 nrPoints = (Int32)(mNrSamples);
                    UInt16 lastChannel = mNrSignals;
                    UInt16 newNrSignals = (UInt16)(mNrSignals - 1);


                    if (nrPoints > 0)
                    {
                        CSignalData[] newSignalsArray = new CSignalData[newNrSignals];
                        int iStore = 0;
                        CSignalData selectedSignal = null;

                        for (int iChannel = 0; iChannel < mNrSignals; ++iChannel)
                        {
                            if (iChannel == AChannelNr)
                            {
                                selectedSignal = mSignals[iChannel];
                            }
                            else
                            {
                                newSignalsArray[iStore] = mSignals[iChannel];
                                ++iStore;
                            }

                        }
                        if (selectedSignal != null)
                        {
                            selectedSignal.mDispose();
                        }

                        mSignals = newSignalsArray;
                        --mNrSignals;
                        bOk = true;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Copy channel " + AChannelNr, ex);
            }

            return bOk;

        }

        public bool mbDoMoveChannel(UInt16 AChannelNr, UInt16 AToChannelNr)
        {
            bool bOk = false;

            try
            {
                if (AChannelNr < mNrSignals && AChannelNr != AToChannelNr)
                {
                    Int32 nrPoints = (Int32)(mNrSamples);
                    UInt16 toChannelNr = AToChannelNr;
                    UInt16 newNrSignals = mNrSignals;

                    if (toChannelNr >= mNrSignals)
                    {
                        toChannelNr = (UInt16)(mNrSignals - 1); // move to last
                    }
                    if (nrPoints > 0)
                    {
                        CSignalData[] newSignalsArray = new CSignalData[newNrSignals];
                        int iStore = 0;
                        CSignalData selectedSignal = mSignals[AChannelNr];

                        // revoe AChannel nr from list
                        for (int iChannel = 0; iChannel < mNrSignals; ++iChannel)
                        {
                            if (iChannel == AChannelNr)
                            {
                                // skip selected channel
                            }
                            else
                            {
                                newSignalsArray[iStore] = mSignals[iChannel];
                                ++iStore;
                            }
                        }
                        // iStore = last = mNrSignals-1
                        // move behind to position to next to make room
                        for (int iChannel = iStore; iChannel > toChannelNr; --iChannel)
                        {
                            newSignalsArray[iChannel] = newSignalsArray[iChannel - 1];
                        }
                        // set channel at to position
                        newSignalsArray[toChannelNr] = selectedSignal;

                        mSignals = newSignalsArray;
                        bOk = true;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Move channel " + AChannelNr + " to " + AToChannelNr, ex);
            }

            return bOk;

        }
        public bool mbDoCalcDerivedChannels()
        {
            bool bOk = false;

            try
            {
                if (mNrSignals >= 3)
                {
                    Int32 nrPoints = (Int32)(mNrSamples);
                    UInt16 newNrSignals = (UInt16)(mNrSignals + 3);

                    // leadI = LA-RA = Ch1
                    // leadII = LL-RA = Ch2
                    // leadIII = LL-LA = leadII - leadI = Ch3
                    // assume RA is 0
                    // aVR = RA - (LA+LL)/2 = -(leadI+leadII)/2 = -(Ch1+ch2)/2
                    // aVL = LA - (LL+RA)/2 = Ch1 - Ch2/2;
                    // aVF = LL - (LA+RA)/2 = Ch2 - Ch1/2;

                    if (nrPoints > 0)
                    {
                        CSignalData[] newSignalsArray = new CSignalData[newNrSignals];

                        CSignalData leadI = mSignals[0]; newSignalsArray[0] = leadI;
                        CSignalData leadII = mSignals[1]; newSignalsArray[1] = leadII;
                        CSignalData leadIII = mSignals[2]; newSignalsArray[2] = leadIII;
                        CSignalData aVR = new CSignalData(); newSignalsArray[3] = aVR;
                        CSignalData aVL = new CSignalData(); newSignalsArray[4] = aVL;
                        CSignalData aVF = new CSignalData(); newSignalsArray[5] = aVF;

                        int iStore = 6;

                        for (int iChannel = 3; iChannel < mNrSignals; ++iChannel)
                        {
                            newSignalsArray[iStore] = mSignals[iChannel];
                            ++iStore;
                        }

                        aVR.mbCopyFrom(leadI);
                        aVL.mbCopyFrom(leadI);
                        aVF.mbCopyFrom(leadI);

                        int[] values1 = aVR == null ? null : aVR.mValues;
                        int[] values2 = leadII == null ? null : leadII.mValues;
                        if (values1 == null || values2 == null)
                        {
                            return false;
                        }
                        else
                        {
                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;

                            for (UInt32 i = 0; i < nrPoints; ++i)
                            {
                                // aVR = RA - (LA+LL)/2 = -(leadI+leadII)/2 = -(Ch1+ch2)/2
                                v = (values1[i] + values2[i]) / -2;

                                values1[i] = v;
                                if (v < minValue) minValue = v; // min max over area
                                if (v > maxValue) maxValue = v;
                            }
                            aVR.mMinValue = minValue;
                            aVR.mMaxValue = maxValue;
                        }

                        values1 = aVL == null ? null : aVL.mValues;
                        //                        values2 = leadII == null ? null : leadII.mValues;
                        if (values1 == null || values2 == null)
                        {
                            return false;
                        }
                        else
                        {
                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;

                            for (UInt32 i = 0; i < nrPoints; ++i)
                            {
                                // aVL = LA - (LL+RA)/2 = Ch1 - Ch2/2;
                                v = values1[i] - values2[i] / 2;

                                values1[i] = v;
                                if (v < minValue) minValue = v; // min max over area
                                if (v > maxValue) maxValue = v;
                            }
                            aVL.mMinValue = minValue;
                            aVL.mMaxValue = maxValue;
                        }
                        values1 = aVF == null ? null : aVF.mValues;
                        //                        values2 = leadII == null ? null : leadII.mValues;
                        if (values1 == null || values2 == null)
                        {
                            return false;
                        }
                        else
                        {
                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;

                            for (UInt32 i = 0; i < nrPoints; ++i)
                            {
                                // aVF = LL - (LA+RA)/2 = Ch2 - Ch1/2;
                                v = values2[i] - values1[i] / 2;

                                values1[i] = v;
                                if (v < minValue) minValue = v; // min max over area
                                if (v > maxValue) maxValue = v;
                            }
                            aVF.mMinValue = minValue;
                            aVF.mMaxValue = maxValue;
                        }

                        mSignals = newSignalsArray;
                        mNrSignals = newNrSignals;
                        bOk = true;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Calc derived channels", ex);
            }
            return bOk;
        }
        public bool mbMeasureACDC(out float ArPeriodSec, out float ArPulseSec, out UInt32 ArNPulses,
                out float ArDCmV, out float AcACmVrms, out float ArMinmV, out float ArMaxmV,
                out string ArHeaderLine, out string ArValuesLine,
                UInt16 AChannel, float AStartSec, float ADurationSec, float AThresholdFactor)
        {
            bool bOk = false;

            float periodSec = 0;
            float pulseSec = 0;
            UInt32 nPulses = 0;
            UInt32 nPeriods = 0;
            float dcmV = 0;
            float acmVrms = 0;
            float minmV = 0;
            float maxmV = 0;
            string headerLine = "";
            string valuesLine = "";

            if (mNrSignals > 0 && mSignals != null && AChannel < mNrSignals && mNrSamples > 0)
            {
                try
                {
                    UInt32 startIndex = mGetSampleIndex(AStartSec);
                    UInt32 endIndex = mGetSampleIndex(AStartSec + ADurationSec);
                    Int32 nrPoints = (Int32)(endIndex - startIndex);
                    DateTime startDT = mGetDeviceTime(mGetSampleUTC(startIndex));
                    double amplifier = mSampleUnitA <= 0.000001 ? 0 : (1.0 / mSampleUnitA + 0.5);
                    string tab = "\t";
                    string tail = tab + mSampleUnitT.ToString("0.00000") + tab + mSampleUnitA.ToString("0.000000");

                    headerLine = "yyyyMMddHHmmss" + tab + "CH" + tab + "startSec" + tab + "lengthSec"
                        + tab + "FreqHz"
                        + tab + "PeriodSec" + tab + "PulseSec" + tab + "n"
                        + tab + "DCmV" + tab + "ACmVrms" + "MinmV" + tab + "MaxmV" + tab + "dtSec" + tab + "dAmV";

                    valuesLine = "\"_" + CProgram.sDateTimeToYMDHMS(startDT) + "\"" + tab + (AChannel + 1).ToString()
                        + tab + AStartSec.ToString("0.0000") + tab + ADurationSec.ToString("0.0000");

                    if (nrPoints <= 0)
                    {
                        valuesLine += "\r\n no time range!";
                    }
                    else
                    {
                        CSignalData signal = mSignals[AChannel];


                        int[] values = signal == null ? null : signal.mValues;
                        if (values == null)
                        {
                            valuesLine += " No signal";
                        }
                        else
                        {
                            Int64 avgSum = 0;
                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;
                            Int32 n = 0;
                            Int32 avgValue;

                            // determin min/max and avarage over period
                            for (UInt32 i = startIndex; i <= endIndex; ++i)
                            {
                                v = values[i];

                                if (v < minValue) minValue = v; // min max over area
                                if (v > maxValue) maxValue = v;

                                avgSum += v;

                                ++n;
                            }

                            avgValue = (Int32)((avgSum + n / 2) / n);

                            dcmV = avgValue * mSampleUnitA;
                            minmV = minValue * mSampleUnitA;
                            maxmV = maxValue * mSampleUnitA;

                            avgSum = 0;
                            Int32 posTest = (Int32)((maxValue - avgValue) * AThresholdFactor);
                            Int32 negTest = (Int32)((minValue - avgValue) * AThresholdFactor);

                            Int64 sumPeriod = 0;
                            Int64 sumPulse = 0;
                            UInt32 nOn = 0;
                            UInt32 nOff = 0;
                            UInt32 iLastOn = 0;

                            bool bState = false;

                            // sum rms2 and period and pulse width
                            n = 0;
                            for (UInt32 i = startIndex; i <= endIndex; ++i)
                            {
                                v = values[i] - avgValue;

                                if (bState)
                                {
                                    // is already on

                                    if (v <= negTest)
                                    {
                                        // on to off
                                        if (nOn > 0)
                                        {
                                            // calculate pulse time
                                            sumPulse += i - iLastOn;
                                            ++nPulses;
                                        }
                                        ++nOff;
                                        bState = false;
                                    }
                                }
                                else
                                {
                                    // is off 
                                    if (v >= posTest)
                                    {
                                        // off to on
                                        if (nOn > 0)
                                        {
                                            // calculate period time
                                            sumPeriod += i - iLastOn;
                                            ++nPeriods;
                                        }
                                        ++nOn;
                                        bState = true;
                                        iLastOn = i;
                                    }
                                }
                                avgSum += (Int64)v * (Int64)v;

                                ++n;
                            }

                            if (n > 0)
                            {
                                // determin rms and period and pulse width
                                float freq = 0;

                                double rms = Math.Sqrt((double)avgSum / (double)n) * mSampleUnitA;

                                acmVrms = (float)rms;

                                if (nPeriods > 0)
                                {
                                    periodSec = (float)((double)sumPeriod / (double)nPeriods) * mSampleUnitT;

                                    if (periodSec > 0.00001)
                                    {
                                        freq = 1.0F / periodSec;
                                    }

                                }
                                if (nPulses > 0)
                                {
                                    pulseSec = (float)((double)sumPulse / (double)nPulses) * mSampleUnitT;

                                }


                                valuesLine += tab + freq.ToString("0.000") + tab + periodSec.ToString("0.00000") + tab + pulseSec.ToString("0.00000")
                                            + tab + nPeriods + tab + dcmV.ToString("0.00000") + tab + acmVrms.ToString("0.00000")
                                            + tab + minmV.ToString("0.00000") + tab + maxmV.ToString("0.00000")
                                            + tab + tail;

                                bOk = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Measure ACDC", ex);
                }

            }
            ArPeriodSec = periodSec;
            ArPulseSec = pulseSec;
            ArNPulses = nPulses;
            ArDCmV = dcmV;
            AcACmVrms = acmVrms;
            ArMinmV = minmV;
            ArMaxmV = maxmV;
            ArHeaderLine = headerLine;
            ArValuesLine = valuesLine;

            return bOk;

        }
        public bool mbDoCalcRR(UInt16 AChannelMaskRR, bool AbAddChannelRR, UInt16 AChannelMaskPri, bool AbAddChannelPri, float APreAvgTimeSec, float APostAvgTimeSec, 
            float AStartPeak, float AThresholdFactor, UInt16 AMaxBpm, float AHighPassFreqHz, bool AbQuadraticDelta)
        {
            bool bOk = false;

            try
            {
                if (AChannelMaskRR != 0 && AChannelMaskPri != 0)
                {
                    UInt32 nrPoints = mNrSamples;
                    UInt16 shiftBits = (UInt16)(mAdcBits < 16 ? 0 : (mAdcBits - 15) / 2);   // reduce delta to hold delta * delta in Int32
                    UInt16 maxNrChannels = mNrSignals;
                    UInt16 nrChannelsRR = 0;
                    UInt16 nrChannelsPri = 0;
                    CSignalData rrSignal = new CSignalData();
                    CSignalData priSignal = new CSignalData();
                    Int32 v, vPrev, vDelta;
                    UInt32 tShiftBackSamplesRR = 0;
                    UInt32 tShiftBackSamplesPri = 0;
                    double freqRatio = 0.5 + mSampleFrequency / 100;
                    Int32 deltaRatio = (Int32)freqRatio;
                    UInt32 nStart = (UInt32)(12.0 * mSampleFrequency);
                    Int32 maxStart = Int32.MinValue;
                    UInt32 minBeatTime = (UInt32)(0.1F * mSampleFrequency);

                    if (deltaRatio < 1) deltaRatio = 1;

                    UInt32 samplesPerSec = (UInt32)(mSampleFrequency + 0.5);
                    Int32 nSearchPeak = 2 + (Int32)((APostAvgTimeSec + APreAvgTimeSec) * mSampleFrequency * 0.5 + 0.5);

                    if (nrPoints > 0 && rrSignal.mbCreateData(nrPoints) && priSignal.mbCreateData(nrPoints))
                    {
                        int[] rrValues = rrSignal.mValues;  // used for adding delta's for finding R tops
                        int[] priValues = priSignal.mValues; // filtered primary signalfor measuring the exact values 
                        bool bUseStored = true;

                        for (UInt32 i = 0; i < nrPoints; ++i)
                        {
                            rrValues[i] = 0;
                            priValues[i] = 0;
                        }
                        rrSignal.mNrValues = nrPoints;
                        priSignal.mNrValues = nrPoints;

                        tShiftBackSamplesPri = (UInt32)(APreAvgTimeSec * mSampleFrequency * 0.5);  // rrrSignal filter shifts by PreAvgTime
                        tShiftBackSamplesRR = tShiftBackSamplesPri;
                        minBeatTime += tShiftBackSamplesPri;

                        for (UInt16 iChannel = 0; iChannel < maxNrChannels; ++iChannel)
                        {
                            if (0 != ((1 << iChannel) & AChannelMaskRR))
                            {
                                // averidge input (best with noise period) to remove noise
                                // add all delta * delta of selected channels in rrSignal 
                                // to get a strong peaks at R top regardless of positive negative R top
                                bool bAddPri = 0 != ((1 << iChannel) & AChannelMaskPri);

                                ++nrChannelsRR;
                                if (bAddPri)
                                {
                                    ++nrChannelsPri;
                                }
                                int[] values = mSignals[iChannel].mValues;
                                CAvgHistoryFilter preFilter = new CAvgHistoryFilter(bUseStored, APreAvgTimeSec, samplesPerSec, 0.0F, mAdcGain, 0.0F);

                                vPrev = 0;
                                for (UInt32 i = 0; i < nrPoints; ++i)
                                {
                                    v = preFilter.mDoFilter(values[i]);
                                    if (bAddPri)
                                    {
                                        priValues[i] += v;
                                    }
                                    vDelta = i <= minBeatTime ? 0 : v - vPrev;
                                    vPrev = v;

                                    if (shiftBits > 0)
                                    {
                                        vDelta = vDelta >> shiftBits;

                                        if (vDelta >= Int16.MaxValue) vDelta = Int16.MaxValue;  // check overflow conditions 16->32 bit
                                        else if (vDelta < Int16.MinValue) vDelta = Int16.MinValue;
                                    }
                                    vDelta *= deltaRatio;
                                    if (AbQuadraticDelta)
                                    {
                                        rrValues[i] += vDelta * vDelta; 
                                    }
                                    else
                                    {
                                        vDelta *= deltaRatio;   // make signal bigger for plotting
                                        rrValues[i] += vDelta < 0 ? -vDelta : vDelta;
                                    }
                                }
                            }
                        }
                        if(nrChannelsRR == 0 || nrChannelsPri == 0)
                        {
                            return false;   // mask wrong
                        }
                        double startPeak = AStartPeak / freqRatio / 10;
                        Int32 startPeakValue = (Int32)(startPeak * mAdcGain);// * startPeak; // higher sample rate means slower delta per point //;
                        if (startPeak < 2) startPeak = 2;
                        // smooth-out rrSignal to get one top

                        Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue;
                        CAvgHistoryFilter postFilter = new CAvgHistoryFilter(bUseStored, APostAvgTimeSec, samplesPerSec, 0.0F, mAdcGain, 0.0F);
                        Int64 sum = 0;

                        tShiftBackSamplesRR += (UInt32)(APostAvgTimeSec * mSampleFrequency * 0.5);  // rrSignal shifts with PostAvgTime

                        for (UInt32 i = 0; i < nrPoints; ++i)
                        {
                            v = postFilter.mDoFilter(rrValues[i]);      // avarage to get one peak (up + down flank R top)
                                                                        // off v = rrValues[i];      // avarage to get one peak (up + down flank R top)

                            rrValues[i] = v;
                            sum += v;
                            if (v < minValue) minValue = v; // min max over area
                            if (v > maxValue) maxValue = v;
                            if (i <= nStart && v > maxStart) maxStart = v;
                        }
                        rrSignal.mMinValue = minValue;
                        rrSignal.mMaxValue = maxValue;

                        // threshold for peak detection
                        Int32 avgRR = (Int32)(sum / nrPoints);
                        Int32 minPeakRR = avgRR; // peak should be a 2 * least average                        
//                        Int32 avgMaxPeakRR = (Int32)((startPeakValue + maxStart /*maxValue*/ + minPeakRR + minPeakRR) / 4);
                        Int32 avgMaxPeakRR = (Int32)(((1+AThresholdFactor ) * minPeakRR));
                        Int32 maxPeakRR = (maxValue / 4) * 3;
                        if (avgMaxPeakRR > maxPeakRR)
                        {
                            avgMaxPeakRR = maxPeakRR;
                        }
                        if (avgMaxPeakRR < minPeakRR)
                        {
                            avgMaxPeakRR = minPeakRR;
                        }

                        // HP filter to get primairy signal around 0 (near base line)
                        minValue = Int32.MaxValue;
                        maxValue = Int32.MinValue;
                        Int32 halfPriN = nrChannelsPri / 2;
                        sum = 0;
                        bool bPriFilter = AHighPassFreqHz > 0.0001;
                        CButherworth3Filter hpFilter = new CButherworth3Filter(DFilterType.HighPass, AHighPassFreqHz, samplesPerSec);

                        for (UInt32 i = 0; i < nrPoints; ++i)
                        {
                            v = priValues[i];
                            v += halfPriN;          // avarage the primary to be the avarage of the primary mask channels
                            v /= nrChannelsPri;     // filter with high pass to get  a smoot base line
                            if (bPriFilter)
                            {
                                v = hpFilter.mDoFilter(v);
                            }
                            priValues[i] = v;
                            sum += v;
                            if (v < minValue) minValue = v; // min max over area
                            if (v > maxValue) maxValue = v;
                        }
                        priSignal.mMinValue = minValue;
                        priSignal.mMaxValue = maxValue;

                        Int32 avgPri = (Int32)(sum / nrPoints); // avarage primairy should be around 0

                        // determin pulse RR signal
                        float thresholdDelta = (1 - AThresholdFactor) * 0.05F;
                        Int32 thresholdOn = minPeakRR + (Int32)((avgMaxPeakRR- minPeakRR) * (AThresholdFactor + thresholdDelta));
                        Int32 thresholdOff = minPeakRR + (Int32)((avgMaxPeakRR - minPeakRR )* (AThresholdFactor - thresholdDelta));

                        UInt32 iOn = 0;
                        UInt32 iOff = 0;
                        Int32 vOn = 0;
                        Int32 vOff = 0;
                        UInt32 iMaxPeak = 0;
                        Int32 maxPeak = 0;
                        UInt32 iLastBeat = 0;
                        Int32 maxBpm = AMaxBpm > 0 ? AMaxBpm : 1;
                        UInt32 minRRtime = (UInt32)(mSampleFrequency * 60.0 / maxBpm);

                        CAnnotation atr = new CAnnotation();

                        atr.mAnnotationsList = new List<CAnnotationLine>();

                        atr.mSetAnnotationTime(mBaseUTC, (Int16)mTimeZoneOffsetMin, mSampleUnitT);

                        UInt32 nBeats = 0;
                        UInt32 nDebugBeats = 2;
                        UInt16 beatCode = CAnnotationLine._cNormalBeat;
                        UInt32 beatTicks = 0;   // R beat position
                        UInt16 beatNum = 0;
                        UInt16 beatChannel = 0;

                        v = rrValues[0];
                        bool bPeakState = v > thresholdOn;

                        // find beats by finding period above trreshold 
                        for (UInt32 i = 0; i < nrPoints; ++i)
                        {
                            v = rrValues[i];

                            if (bPeakState)
                            {
                                if (v > thresholdOff)
                                {
                                    // above threshold, still in peak
                                    if (v > maxPeak)
                                    {
                                        maxPeak = v;
                                        iMaxPeak = i;
                                    }
                                }
                                else
                                {
                                    // diving below threshold -> end peak -> search for beat
                                    bPeakState = false;
                                    iOff = i;
                                    vOff = v;

                                    if (nBeats < nDebugBeats)
                                    {
                                        UInt32 i2 = iOff;
                                    }
                                    // we have a peak
                                    UInt32 iBeatRR = (iOn + iOn + iMaxPeak + iOff) >> 2;
                                    beatTicks = iBeatRR <= tShiftBackSamplesRR ? 0
                                        : (UInt32)(iBeatRR - tShiftBackSamplesRR);  // shift to original signal position
                                    UInt32 shiftPriRR = tShiftBackSamplesRR - tShiftBackSamplesPri;
                                    UInt32 iBeatPri = iBeatRR <= shiftPriRR ? 0
                                        : (UInt32)(iBeatRR - shiftPriRR);  // shift to filtered primary signal position

                                    // search heighest in pri around beat
                                    Int32 vMaxPriPeak = Int32.MinValue;
                                    Int32 vMaxPriValue = 0;
                                    UInt32 iMaxPriPeak = iBeatPri;
                                    for (Int32 dJ = 1; dJ <= nSearchPeak; ++dJ)
                                    {
                                        Int32 j = (Int32)(iBeatPri - dJ);

                                        if (j >= 0 && j < nrPoints)
                                        {
                                            Int32 v2 = priValues[j] - avgPri;
                                            Int32 v2Org = v2;
                                            if (v2 < 0) v2 = -v2;

                                            if (v2 > vMaxPriPeak) // find peak in  smooth primairy (peak can be upside down)
                                            {
                                                vMaxPriPeak = v2;
                                                vMaxPriValue = v2Org;
                                                iMaxPriPeak = (UInt32)j;
                                            }
                                        }

                                        j = (Int32)(iBeatPri + dJ);

                                        if (j >= 0 && j < nrPoints)
                                        {
                                            Int32 v2 = priValues[j] - avgPri;
                                            Int32 v2Org = v2;
                                            if (v2 < 0) v2 = -v2;

                                            if (v2 > vMaxPriPeak) // find peak in primairy smoother (peak can be upside down)
                                            {
                                                vMaxPriPeak = v2;
                                                vMaxPriValue = v2Org;
                                                iMaxPriPeak = (UInt32)j;
                                            }
                                        }

                                    }
                                    beatTicks = (UInt32)(iMaxPriPeak < tShiftBackSamplesPri ? 0
                                        : iMaxPriPeak - tShiftBackSamplesPri); // shift to original time

                                    if (beatTicks >= minBeatTime)   // prevent wrong beat at start signal
                                    {
                                        bool bNormalBeat = nBeats == 0 || (beatTicks - iLastBeat) >= minRRtime; // 

                                        beatCode = bNormalBeat ? CAnnotationLine._cNormalBeat : CAnnotationLine._cUnclassifiableBeat;
                                        CAnnotationLine annBeat = bNormalBeat ? new CAnnotationLine(beatCode, beatTicks, beatNum, beatChannel, vMaxPriValue)// extra value to draw R peak amplitude
                                            : new CAnnotationLine(beatCode, beatTicks, beatNum, beatChannel);

                                        atr.mAnnotationsList.Add(annBeat);
                                        ++nBeats;

                                        if (bNormalBeat)
                                        {
                                            // calculate next threshold (move with peak height)
                                            avgMaxPeakRR = (Int32)(avgMaxPeakRR * 3 + maxPeak) / 4;
                                            if (avgMaxPeakRR > maxPeakRR)
                                            {
                                                avgMaxPeakRR = maxPeakRR;
                                            }
                                            if (avgMaxPeakRR < minPeakRR)
                                            {
                                                avgMaxPeakRR = minPeakRR;
                                            }
                                            thresholdOn = minPeakRR + (Int32)((avgMaxPeakRR - minPeakRR) * (AThresholdFactor + thresholdDelta));
                                            thresholdOff = minPeakRR + (Int32)((avgMaxPeakRR - minPeakRR) * (AThresholdFactor - thresholdDelta));

                                            iLastBeat = beatTicks;
                                        }
                                    }
                                    // reset for next beat detection
                                    maxPeak = 0;
                                }
                            }
                            else
                            {
                                // bPeakState = false

                                if (v >= thresholdOn)
                                {
                                    // jumping above threshold
                                    bPeakState = true;
                                    maxPeak = v;
                                    iMaxPeak = i;
                                    iOn = i;
                                    vOn = v;
                                }
                                else
                                {
                                    // below threshold
                                }
                            }
                        }
                        if (nBeats > 0)
                        {
                            mAnnotationAtr = atr;
                        }
                    }

                    if (AbAddChannelRR)
                    {
                        mbAddSignal(rrSignal);
                        mbDoShiftTimeSignal(rrSignal, -tShiftBackSamplesRR * mSampleUnitT); // shift back delay of filters
                    }
                    if (AbAddChannelPri)
                    {
                        mbAddSignal(priSignal);
                        mbDoShiftTimeSignal(priSignal, -tShiftBackSamplesPri * mSampleUnitT); // shift back delay of pre filter
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Calc RR $" + AChannelMaskRR.ToString("X"), ex);
            }

            return bOk;
        }

        public bool mbAddSignal(CSignalData ASignal)
        {
            bool bOk = false;

            try
            {
                if (ASignal != null)
                {
                    if (mSignals == null)
                    {
                        CSignalData[] newSignalsArray = new CSignalData[1];

                        newSignalsArray[1] = ASignal;
                        mNrSignals = 1;
                    }
                    else
                    {
                        UInt16 lastChannel = mNrSignals;
                        UInt16 newNrSignals = (UInt16)(mNrSignals + 1);
                        CSignalData[] newSignalsArray = new CSignalData[newNrSignals];

                        for (int iChannel = 0; iChannel < mNrSignals; ++iChannel)
                        {
                            newSignalsArray[iChannel] = mSignals[iChannel];
                        }

                        newSignalsArray[lastChannel] = ASignal;
                        mSignals = newSignalsArray;
                        ++mNrSignals;
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Add Signal", ex);
            }
            return bOk;
        }

        public bool mbDoAddNoise(bool AbAllChannels, UInt16 AChannelNr, float ANoiseFreqHz, float ANoiseAmpmV, float APhaseDeg)
        {
            bool bOk = false;

            try
            {
                UInt32 samplesPerSec = (UInt32)(mSampleFrequency + 0.5);

                Int32 firstChannel = AbAllChannels ? 0 : AChannelNr;
                Int32 lastChannel = AbAllChannels ? mNrSignals - 1 : AChannelNr;
                Int32 nrPoints = (Int32)(mNrSamples);
                double tStart = Math.PI * APhaseDeg / 180;
                double aMax = mAdcGain * ANoiseAmpmV;

                if (nrPoints > 0 && mSampleFrequency > 0)
                {
                    for (int iChannel = firstChannel; iChannel <= lastChannel; ++iChannel)
                    {
                        CSignalData signal = mSignals[iChannel];
                        int[] values = signal == null ? null : signal.mValues;
                        if (values == null)
                        {
                            return false;
                        }
                        else
                        {
                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;
                            double dRad = Math.PI * 2.0 * ANoiseFreqHz / mSampleFrequency;
                            double t, a;


                            for (UInt32 i = 0; i < nrPoints; ++i)
                            {
                                t = tStart + i * dRad;
                                a = aMax * Math.Sin(t);
                                v = values[i];

                                v += (Int32)(a + 0.5);

                                values[i] = v;
                                if (v < minValue) minValue = v; // min max over area
                                if (v > maxValue) maxValue = v;
                            }
                            signal.mMinValue = minValue;
                            signal.mMaxValue = maxValue;
                        }
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("AddNoise", ex);
            }

            return bOk;

        }
        public bool mbDoAltAmpl(bool AbAllChannels, UInt16 AChannelNr, float ABaseAmplmV, float AScaleAmpl, float AOffsetAmpl)
        {
            bool bOk = false;

            try
            {
                UInt32 samplesPerSec = (UInt32)(mSampleFrequency + 0.5);

                Int32 firstChannel = AbAllChannels ? 0 : AChannelNr;
                Int32 lastChannel = AbAllChannels ? mNrSignals - 1 : AChannelNr;
                Int32 nrPoints = (Int32)(mNrSamples);
                Int32 baseA = (Int32)(ABaseAmplmV * mAdcGain + 0.5);
                Int32 offsetA = (Int32)(AOffsetAmpl * mAdcGain + 0.5);

                if (nrPoints > 0 && mSampleFrequency > 0)
                {
                    for (int iChannel = firstChannel; iChannel <= lastChannel; ++iChannel)
                    {
                        CSignalData signal = mSignals[iChannel];
                        int[] values = signal == null ? null : signal.mValues;
                        if (values == null)
                        {
                            return false;
                        }
                        else
                        {
                            Int32 minValue = Int32.MaxValue, maxValue = Int32.MinValue, v;

                            for (UInt32 i = 0; i < nrPoints; ++i)
                            {
                                v = values[i] - baseA;
                                v = (Int32)(v * AScaleAmpl + 0.5);
                                v += offsetA;

                                values[i] = v;
                                if (v < minValue) minValue = v; // min max over area
                                if (v > maxValue) maxValue = v;
                            }
                            signal.mMinValue = minValue;
                            signal.mMaxValue = maxValue;
                        }
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("AltAmpl", ex);
            }

            return bOk;

        }

        /*
    public enum DPlethSearch
    {
        Range,
        Peak,
        vally,
        Rise
    };

    public enum DPlethState
    {
        Search,
        RiseLow,
        RiseMid,
        RiseHigh,
        DropMid
    }
         */

        public bool mbDoCalcPlethRR(UInt16 AChannelNr, bool AbAddChannelRR, float APreAvgTimeSec, float AStartTimeSec, float AMinPeak, float AThresholdFactor, UInt16 AMaxBpm)
        {
            bool bOk = false;

            try
            {
                if (AChannelNr < mNrSignals)
                {
                    UInt32 nrPoints = mNrSamples;
                    CSignalData rrSignal = new CSignalData();
                    Int32 v, vPrev, vDelta;
                    UInt32 tShiftBackSamplesRR = 0;
                    UInt32 iFillRR = 0;
                    UInt32 nBeats = 0;
                    UInt16 beatCodePeak = CAnnotationLine._cPlethSystole;   //50
                    UInt16 beatCodeValley = CAnnotationLine._cPlethDiastole;// 51;
                    UInt16 beatCodeTrans = CAnnotationLine._cPlethTrans;// 52;

                    UInt32 samplesPerSec = (UInt32)(mSampleFrequency + 0.5);
                    float sampleUnitSec = 1.0F / samplesPerSec;

                    Int32 nSearchPeak = 1 + (Int32)(AStartTimeSec * mSampleFrequency + 0.5);
                    bool bFilter = APreAvgTimeSec > sampleUnitSec;
                    bool bUseStored = true;

                    bool bAddRBeat = mAnnotationAtr == null || mAnnotationAtr.mAnnotationsList == null || mAnnotationAtr.mAnnotationsList.Count < 1;

                    DPlethSearch plethSearch = DPlethSearch.Range;
//                    DPlethState plethState = DPlethState.Search;
                    Int32 curMaxValue = Int32.MinValue;
                    Int32 curMinValue = Int32.MaxValue;
                    UInt32 curMaxIndex = 0;
                    UInt32 curMinIndex = 0;
                    Int32 curRiseValue = 0;
                    UInt32 curRiseIndex = 0;
                    UInt32 curTransIndex = 0;
                    UInt32 startSearchIndex = 0;
                    int curRange = 0, testRangeRise = 0, testRangeTresh = 0, testValueThres = 0, testValueRise = 0;
                    int minRange = (Int32)(AMinPeak * mAdcGain);


                    if (nrPoints > 0 && rrSignal.mbCreateData(nrPoints))
                    {
                        int[] rrValues = rrSignal.mValues;  // used for storing adding delta's for finding R tops
                        int[] values = mSignals[AChannelNr].mValues; // filtered primary signalfor measuring the exact values 

                        for (UInt32 i = 0; i < nrPoints; ++i)
                        {
                            rrValues[i] = 0;
                        }
                        rrSignal.mNrValues = nrPoints;

                        tShiftBackSamplesRR = (UInt32)(APreAvgTimeSec * mSampleFrequency * 0.5);  // rrSignal filter shifts by PreAvgTime

                        CAvgHistoryFilter preFilter = new CAvgHistoryFilter(bUseStored, APreAvgTimeSec, samplesPerSec, 0.0F, mAdcGain, 0.0F);

                        if (bAddRBeat)
                        {
                            mAnnotationAtr = new CAnnotation();

                            mAnnotationAtr.mNewList();

                            mAnnotationAtr.mSetAnnotationTime(mBaseUTC, (Int16)mTimeZoneOffsetMin, mSampleUnitT);
                        }
                        vPrev = 0;
                        for (UInt32 i = 0; i < nrPoints; ++i)
                        {
                            v = values[i];
                            if (bFilter)
                            {
                                v = preFilter.mDoFilter(v);
                            }

                            vDelta = i <= 1 ? 0 : v - vPrev;
                            vPrev = v;

                            if (plethSearch == DPlethSearch.Range)
                            {
                                if (v < curMinValue)
                                {
                                    curMinValue = v;
                                    curMinIndex = i;
                                }
                                if (v > curMaxValue)
                                {
                                    curMaxValue = v;
                                    curMaxIndex = i;
                                }
                                curRange = curMaxValue - curMinValue;

                                if (curRange > minRange && curMinIndex > curMaxIndex && i - startSearchIndex >= nSearchPeak)  // min/max have a good range and vally is after peak
                                {
                                    testRangeTresh = (Int32)(curRange * AThresholdFactor);
                                    testRangeRise = testRangeTresh / 2;
                                    testValueThres = curMinValue + testRangeTresh;
                                    testValueRise = curMinValue + testRangeRise;

                                    if (v >= testValueRise && vDelta > 0)
                                    {
                                        float t = i * mSampleUnitT;
                                        // rising from vally => search for peak
                                        plethSearch = DPlethSearch.Peak;
                                        curMaxValue = Int32.MinValue;

                                        while (iFillRR <= i)
                                        {
                                            rrValues[iFillRR++] = testValueRise;   // fill first part with min
                                        }
                                    }
                                }
                            }
                            else if (plethSearch == DPlethSearch.Peak)
                            {
                                if (v > curMaxValue)
                                {
                                    curMaxValue = v;
                                    curMaxIndex = i;
                                }
                                else if (v < testValueThres && vDelta < 0)
                                {
                                    curRange = curMaxValue - curMinValue;
                                    if (curRange < minRange)
                                    {
                                        curRange = minRange;
                                    }
                                    testRangeTresh = (Int32)(curRange * AThresholdFactor);
                                    testRangeRise = testRangeTresh / 2;
                                    testValueThres = curMinValue + testRangeTresh;
                                    testValueRise = curMinValue + testRangeRise;

                                    // dropping after peak
                                    plethSearch = DPlethSearch.Vally;
                                    curMinValue = Int32.MaxValue;

                                    while (iFillRR < curMaxIndex)
                                    {
                                        rrValues[iFillRR++] = testValueThres;   // fill until peak with half
                                    }
                                    while (iFillRR <= i)
                                    {
                                        rrValues[iFillRR++] = curMaxValue;   // fill until now with peak
                                    }
                                    UInt32 beatTicks = curMaxIndex <= tShiftBackSamplesRR ? 0
                                        : (UInt32)(curMaxIndex - tShiftBackSamplesRR);  // shift to original signal position
                                    CAnnotationLine annBeat = new CAnnotationLine(beatCodePeak, beatTicks, 0, 0, curMaxValue);// extra value to draw peak/valley

                                    mAnnotationAtr.mbAddSorted(annBeat);
                                    ++nBeats;
                                }
                            }
                            else if (plethSearch == DPlethSearch.Vally)
                            {
                                if (v < curMinValue)
                                {
                                    curMinValue = v;
                                    curMinIndex = i;
                                    testValueRise = curMinValue + testRangeRise;

                                }
                                else if (v > testValueRise && vDelta > 0)
                                {
                                    curRiseValue = v;
                                    curRiseIndex = i;

                                    curRange = curMaxValue - curMinValue;
                                    if (curRange < minRange)
                                    {
                                        curRange = minRange;
                                    }
                                    testRangeTresh = (Int32)(curRange * AThresholdFactor);
                                    testRangeRise = testRangeTresh / 2;
                                    testValueThres = curMinValue + testRangeTresh;
                                    testValueRise = curMinValue + testRangeRise;

                                    // rising after peak
                                    plethSearch = DPlethSearch.Rise;
                                    curMaxValue = Int32.MinValue;

                                    while (iFillRR < curMinIndex)
                                    {
                                        rrValues[iFillRR++] = testValueThres;   // fill until valley with half
                                    }
                                    while (iFillRR <= curMinIndex + 1 && iFillRR <= i)
                                    {
                                        rrValues[iFillRR++] = curMinValue;   // fill vally+1
                                    }
                                    UInt32 beatTicks = curMinIndex <= tShiftBackSamplesRR ? 0
                                        : (UInt32)(curMinIndex - tShiftBackSamplesRR);  // shift to original signal position

                                    CAnnotationLine annBeat = new CAnnotationLine(beatCodeValley, beatTicks, 0, 0, curMinValue);    // extra value to draw peak/valley

                                    mAnnotationAtr.mbAddSorted(annBeat);
                                }

                            }
                            else if (plethSearch == DPlethSearch.Rise)
                            {
                                if (v > curMaxValue)
                                {
                                    curMaxValue = v;
                                    curMaxIndex = i;
                                }
                                if (v >= testValueThres && vDelta > 0)
                                {
                                    // rising slope after valley and nearing top => calc transient point
                                    plethSearch = DPlethSearch.Peak;

                                    Int32 dValue = v - curRiseValue;
                                    UInt32 dIndex = i - curRiseIndex;
                                    double riseRate = 0;

                                    curTransIndex = curRiseIndex;

                                    if (dIndex > 0)
                                    {
                                        riseRate = dValue / (double)dIndex;

                                        if (riseRate >= 1.0)
                                        {
                                            Int32 transDelta = (Int32)((curRiseValue - curMinValue) / riseRate);
                                            curTransIndex -= (UInt32)(transDelta);
                                        }
                                    }
                                    if (curTransIndex <= curMinIndex)
                                    {
                                        curTransIndex = curMinIndex + 1;
                                    }

                                    while (iFillRR <= curTransIndex)
                                    {
                                        rrValues[iFillRR++] = curMinValue;   // fill valley until transit
                                    }
                                    while (iFillRR <= curRiseIndex)
                                    {
                                        rrValues[iFillRR++] = curRiseValue;   // fill transit part with min
                                    }
                                    while (iFillRR <= i)
                                    {

                                        rrValues[iFillRR++] = curRiseValue + (Int32)((i - curRiseIndex) * riseRate);   // fill min to test with rate flank
                                    }

                                    UInt32 beatTicks = curTransIndex <= tShiftBackSamplesRR ? 0
                                        : (UInt32)(curTransIndex - tShiftBackSamplesRR);  // shift to original signal position
                                    if (bAddRBeat)
                                    {
                                        // no ECG R beats-> add normal beat for tansition time calculation and show of HR line
                                        CAnnotationLine rBeat = new CAnnotationLine(CAnnotationLine._cNormalBeat, beatTicks, 0, 0);

                                        mAnnotationAtr.mbAddSorted(rBeat);
                                    }
                                    CAnnotationLine annBeat = new CAnnotationLine(beatCodeTrans, beatTicks, 0, 0);

                                    mAnnotationAtr.mbAddSorted(annBeat);
                                }
                            }
                            else
                            {
                                plethSearch = DPlethSearch.Range;    // should not happen
//                                plethState = DPlethState.Search;
                                startSearchIndex = i;
                            }
                        }
                        while (iFillRR < nrPoints)
                        {
                            rrValues[iFillRR++] = testValueThres;   // fill end record with half value
                        }

                        if (AbAddChannelRR)
                        {
                            mbAddSignal(rrSignal);
                            if (bFilter) mbDoShiftTimeSignal(rrSignal, -tShiftBackSamplesRR * mSampleUnitT); // shift back delay of filters
                        }
                    }

                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Calc Pleath RR " + AChannelNr, ex);
            }

            return bOk;
        }

        public void mPlotAnnotationImage(Image AImage, CAnnotation AAnnotation,
            Color AColor, CStripChart AChart,
            UInt16 ABeatCodeHigh, UInt16 ABeatCodeLow,
            double AMinValue, double AMaxValue,
            double ALineHighValue, double ALineLowValue, double ALineMeanValue, DChartDrawTo ADrawTo,
            UInt16 ANrAvgPoints)
        {

            if (AImage != null && AChart != null && AAnnotation != null && AAnnotation.mAnnotationsList != null)
            {

                int w = AImage.Width;
                int h = AImage.Height;

                if (w > 1 && h > 1)
                {
                    try
                    {
                        float startTimeSec = AChart.mGetStartTimeSec();
                        float timeDurationSec = AChart.mGetStripTimeSec();
                        bool bDoFilter = ANrAvgPoints > 1;

                        --w;
                        --h;

                        using (Graphics graphics = Graphics.FromImage(AImage))
                        {

                            float plotWidth = CDvtmsData.sGetSignalWidth(ADrawTo);
                            Color eventColor = CDvtmsData.sGetPrintColor(ADrawTo, Color.Red); // DarkBlue;
                            Color colorHigh = Color.Red; Pen penHigh = new Pen(colorHigh);
                            Color colorNormal = AColor; Pen penNormal = new Pen(colorNormal);
                            Color colorBase = Color.LightGreen; Pen penBase = new Pen(colorBase);
                            Color colorLow = Color.Blue; Pen penLow = new Pen(colorLow);

                            // setup chart
                            if (AChart.mbSetGraphMaxA(1, 1, w - 1, h - 1, startTimeSec, timeDurationSec, (float)AMinValue, (float)AMaxValue, 0.010F))
                            {

                                int x = AChart.mGetPixelT(AChart.mGetStartTimeSec() + 0.5F * AChart.mGetStripTimeSec());
                                int y = AChart.mGetPixelA(0.5F * (AChart.mGetEndA() + AChart.mGetStartA()));

                                if (ALineHighValue > ALineLowValue)
                                {
                                    y = AChart.mGetPixelA((float)ALineHighValue);     // high line
                                    graphics.DrawLine(penHigh, 0, y, w, y);
                                    y = AChart.mGetPixelA((float)ALineLowValue);     // low line
                                    graphics.DrawLine(penLow, 0, y, w, y);
                                }
                                if (ALineMeanValue > ALineLowValue)
                                {
                                    y = AChart.mGetPixelA((float)ALineMeanValue);     // mean line
                                    graphics.DrawLine(penBase, 0, y, w, y);
                                    x = AChart.mGetPixelT(mGetEventSec());
                                    if (x > 0 && x < w)
                                    {
                                        graphics.DrawLine(penBase, x, 1, x, w - 1);   // event
                                    }
                                }
                                // plot graph
                                //                                   
                                Int32 nCount = 0;// first beat the line is not drawn
                                int prevX = 0;
                                int prevY = 0;

                                UInt32 tTick;
                                UInt32 maxTick = AAnnotation.mUnitT > 0.0001 ? (UInt32)(4 / AAnnotation.mUnitT) : 40000;  // 15 BPM beat is to far appart
                                UInt32 tLowTick = UInt32.MaxValue;
                                UInt32 tPrevTick = UInt32.MaxValue;
                                double lowValue = AMinValue, v;
                                UInt16 code;
                                float t;
                                bool bUseHistory = true;
                                CAvgPointDblFilter avgHigh = new CAvgPointDblFilter(bUseHistory, ANrAvgPoints);
                                CAvgPointDblFilter avgLow = new CAvgPointDblFilter(bUseHistory, ANrAvgPoints);
                                //                                float tStart = AChart.mGetStartTimeSec();
                                //                              float tEnd = tStart + AChart.mGetStripTimeSec();
                                bool bPlotDelta = ABeatCodeLow != 0 && ABeatCodeHigh != ABeatCodeLow;
                                bool bPlotAll = ABeatCodeHigh == 0;

                                double vTest = AMinValue + 0.05 * (AMaxValue - AMinValue);

                                foreach (CAnnotationLine ann in mAnnotationAtr.mAnnotationsList)
                                {
                                    if (ann.mbExtraValueSet)
                                    {
                                        code = ann.mAnnotationCode;
                                        if (bPlotAll || code == ABeatCodeHigh)
                                        {
                                            tTick = ann.mAnnotationTimeTck;
                                            t = mAnnotationAtr.mGetTimeSec(tTick);
                                            x = AChart.mGetPixelT(t);

                                            v = ann.mExtraValue * mSampleUnitA;
                                            if (bDoFilter)
                                            {
                                                v = avgHigh.mDoFilter(v);
                                            }
                                            if (bPlotDelta)
                                            {
                                                if (tTick - tLowTick < maxTick)
                                                {
                                                    v += -lowValue + AMinValue;
                                                }
                                                else
                                                {
                                                    v = AMinValue;  // low is to far away -> plot at bottom
                                                    prevX = x;
                                                    prevY = h + 1;
                                                }
                                            }
                                            if (tTick - tPrevTick > maxTick)
                                            {
                                                prevX = x;
                                                prevY = h + 1;
                                            }
                                            if (v < vTest)
                                            {
                                                int g = x;
                                            }
                                            y = AChart.mGetPixelA((float)v);
                                            if (++nCount > 1)
                                            {
                                                graphics.DrawLine(penNormal, prevX, prevY, x, y);
                                            }
                                            prevX = x;
                                            prevY = y;
                                            tPrevTick = tTick;
                                        }
                                        else if (bPlotDelta && code == ABeatCodeLow)
                                        {
                                            lowValue = ann.mExtraValue * mSampleUnitA;
                                            if (bDoFilter)
                                            {
                                                lowValue = avgLow.mDoFilter(lowValue);
                                            }
                                            tLowTick = ann.mAnnotationTimeTck;
                                        }
                                    }
                                }
                                graphics.Dispose();
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }
        public string mInfoAnnotationCsv(string ARecordID, CAnnotation AAnnotation,
            UInt16 ABeatCodeHigh, UInt16 ABeatCodeLow,
            UInt16 ANrAvgPoints,
            float ARegionStartSec, float ARegionDurationSec, string ALabelName, string ALabelUnit, string ASeperator = "\t")
        {
            string result = "";

            if (AAnnotation != null && AAnnotation.mAnnotationsList != null)
            {
                string header = "";
                string newLine = "\r\n";
                string table = "";
                string equal = ASeperator + "=" + ASeperator;
                float unitT = AAnnotation.mUnitT;
                float sampleFreq = unitT < 0.0001 ? 0 : 1.0F / unitT;
                string unitLabel = "(" + ALabelUnit + ")";
                float durationSec = mGetSamplesTotalTime();


                try
                {
                    header = ARecordID + ASeperator + CProgram.sDateTimeToString(mBaseUTC) + " U" + CProgram.sTimeZoneOffsetStringLong((Int16)mTimeZoneOffsetMin) + newLine;
                    header += "var" + equal + ALabelName + ASeperator + ALabelUnit + newLine;
                    header += "sampleUnitT" + equal + AAnnotation.mUnitT.ToString("0.00000") + ASeperator + "sec" + ASeperator + sampleFreq.ToString( "0.0" ) + ASeperator + "Sps" + newLine;
                    header += "avgN" + equal + ANrAvgPoints + newLine;
                    header += newLine;
                    // min, max, avg, n
                    // rrTime min, max, avg

                    table = "(sec)" + ASeperator + "(sec)" + ASeperator + "(sec)" + ASeperator + "(sec)"
                        + unitLabel + ASeperator + unitLabel + ASeperator + unitLabel + ASeperator + "BPM"+ ASeperator + "01" + ASeperator + unitLabel + unitLabel + ASeperator + ASeperator + unitLabel + newLine;
                    table = "tLow" + ASeperator + "tTrans" + ASeperator + "tHigh" + ASeperator + "tRR" + ASeperator + "HR" + ASeperator
                        + "low" + ASeperator + "high" + ASeperator + "hlDelta" + ASeperator + "Region" + ASeperator + "lowAvg" + ASeperator + "highAvg" + ASeperator + "hlAvg" + newLine;

                    UInt32 regionStartTick = ARegionStartSec <= 0 ? 0 : (UInt32)(ARegionStartSec * sampleFreq);
                    UInt32 regionEndTick = ARegionDurationSec < unitT ? 0 : (UInt32)((ARegionStartSec + ARegionDurationSec) * sampleFreq);
                    Int32 nCount = 0;
                    double lowMin = 1e9;
                    double lowMax = -1e9;
                    double lowSum = 0;
                    double highMin = 1e9;
                    double highMax = -1e9;
                    double highSum = 0;
                    double hlMin = 1e9;
                    double hlMax = -1e9;
                    double hlSum = 0;
                    double lowAvgMin = 1e9;
                    double lowAvgMax = -1e9;
                    double lowAvgSum = 0;
                    double highAvgMin = 1e9;
                    double highAvgMax = -1e9;
                    double highAvgSum = 0;
                    double hlAvgMin = 1e9;
                    double hlAvgMax = -1e9;
                    double hlAvgSum = 0;

                    Int32 regionNCount = 0;
                    double regionLowMin = 1e9;
                    double regionLowMax = -1e9;
                    double regionLowSum = 0;
                    double regionHighMin = 1e9;
                    double regionHighMax = -1e9;
                    double regionHighSum = 0;
                    double regionHlMin = 1e9;
                    double regionHlMax = -1e9;
                    double regionHlSum = 0;
                    double regionAvgLowMin = 1e9;
                    double regionAvgLowMax = -1e9;
                    double regionAvgLowSum = 0;
                    double regionAvgHighMin = 1e9;
                    double regionAvgHighMax = -1e9;
                    double regionAvgHighSum = 0;
                    double regionHlAvgMin = 1e9;
                    double regionHlAvgMax = -1e9;
                    double regionHlAvgSum = 0;

                    UInt32 rrCount = 0;
                    double rrTimeMin = 1e9;
                    double rrTimeMax = -1e9;
                    double rrSum = 0;
                    UInt32 regionRrCount = 0;
                    double regionRrTimeMin = 1e9;
                    double regionRrTimeMax = -1e9;
                    double regionRrSum = 0;
                    UInt32 tTransTick = 0;

                    bool bDoFilter = ANrAvgPoints > 1;

                    UInt32 tTick;
                    UInt32 maxTick = AAnnotation.mUnitT > 0.0001 ? (UInt32)(4 / AAnnotation.mUnitT) : 40000;  // 15 BPM beat is to far appart
                    UInt32 tLowTick = UInt32.MaxValue;
                    UInt32 tPrevTick = UInt32.MaxValue;
                    double lowValue = 0, lowOrg = 0, highValue, highOrg = 0, avg, hrMin, hrMax, hrAvg;
                    UInt16 code;
                    float t;
                    bool bUseHistory = true;
                    CAvgPointDblFilter avgHigh = new CAvgPointDblFilter(bUseHistory, ANrAvgPoints);
                    CAvgPointDblFilter avgLow = new CAvgPointDblFilter(bUseHistory, ANrAvgPoints);
                    //                                float tStart = AChart.mGetStartTimeSec();
                    //                              float tEnd = tStart + AChart.mGetStripTimeSec();

                    foreach (CAnnotationLine ann in mAnnotationAtr.mAnnotationsList)
                    {
                        if (ann.mbExtraValueSet)
                        {
                            code = ann.mAnnotationCode;
                            if (code == ABeatCodeHigh)
                            {
                                tTick = ann.mAnnotationTimeTck;
                                t = mAnnotationAtr.mGetTimeSec(tTick);
                                highValue = ann.mExtraValue * mSampleUnitA;
                                highOrg = highValue;
                                if (bDoFilter)
                                {
                                    highValue = avgHigh.mDoFilter(highValue);
                                }
                                if (tTick - tLowTick <= maxTick)
                                {
                                    // log values
                                    float tLow = tLowTick * unitT;
                                    float tTrans = tTransTick * unitT;
                                    float tHigh = tTick * unitT;
                                    float tRR = (nCount == 0 ? 0 : tTick - tPrevTick) * unitT;
                                    float bpm = tRR <= 0.001 ? 0 : 60.0F / tRR;
                                    double hl = highOrg - lowOrg;
                                    double hlAvg = highValue - lowValue;
                                    bool bRegion = tTick >= regionStartTick && tTick < regionEndTick;

                                    string line = tLow.ToString("0.000" ) + ASeperator + tTrans.ToString("0.000") + ASeperator + tHigh.ToString("0.000") + ASeperator 
                                        + tRR.ToString("0.000") + ASeperator + bpm.ToString("0.0") + ASeperator
                                        + lowOrg.ToString("0.000") + ASeperator + highOrg.ToString("0.000") + ASeperator + hl.ToString("0.000") + ASeperator + (bRegion ? "1": "0") + ASeperator 
                                        + lowValue.ToString("0.000") + ASeperator + highValue.ToString("0.000") + ASeperator + hlAvg.ToString("0.000");

                                    table += line + newLine;

                                    // calc min, max, sum

                                    ++nCount;
                                    if (lowOrg < lowMin) lowMin = lowOrg;
                                    if (lowOrg > lowMax) lowMax = lowOrg;
                                    lowSum += lowOrg;
                                    if (highOrg < highMin) highMin = highOrg;
                                    if (highOrg > highMax) highMax = highOrg;
                                    highSum += highOrg;
                                    if (hl < hlMin) hlMin = hl;
                                    if (hl > hlMax) hlMax = hl;
                                    hlSum += hl;

                                    if (lowValue < lowAvgMin) lowAvgMin = lowValue;
                                    if (lowValue > lowAvgMax) lowAvgMax = lowValue;
                                    lowSum += lowValue;
                                    if (highValue < highAvgMin) highAvgMin = highValue;
                                    if (highValue > highAvgMax) highAvgMax = highValue;
                                    highAvgSum += highValue;
                                    if (hlAvg < hlAvgMin) hlAvgMin = hlAvg;
                                    if (hlAvg > hlAvgMax) hlAvgMax = hlAvg;
                                    hlSum += hl;

                                    if ( tRR > 0.01 )
                                    {
                                        ++rrCount;
                                        if (tRR < rrTimeMin) rrTimeMin = tRR;
                                        if (tRR > rrTimeMax) rrTimeMax = tRR;
                                        rrSum += tRR;
                                    }
                                    if( bRegion )
                                    {
                                        ++regionNCount;
                                        if (lowOrg < regionLowMin) regionLowMin = lowOrg;
                                        if (lowOrg > regionLowMax) regionLowMax = lowOrg;
                                        regionLowSum += lowOrg;
                                        if (highOrg < regionHighMin) regionHighMin = highOrg;
                                        if (highOrg > regionHighMax) regionHighMax = highOrg;
                                        regionHighSum += highOrg;
                                        if (hl < regionHlMin) regionHlMin = hl;
                                        if (hl > regionHlMax) regionHlMax = hl;
                                        regionHlSum += hl;

                                        if (lowValue < regionAvgLowMin) regionAvgLowMin = lowValue;
                                        if (lowValue > regionAvgLowMax) regionAvgLowMax = lowValue;
                                        regionAvgLowSum += lowValue;
                                        if (highValue < regionAvgHighMin) regionAvgHighMin = highValue;
                                        if (highValue > regionAvgHighMax) regionAvgHighMax = highValue;
                                        regionAvgHighSum += highValue;
                                        if (hlAvg < regionHlAvgMin) regionHlAvgMin = hlAvg;
                                        if (hlAvg > regionHlAvgMax) regionHlAvgMax = hlAvg;
                                        regionHlAvgSum += hlAvg;

                                        if (tRR > 0.01)
                                        {
                                            ++regionRrCount;
                                            if (tRR < regionRrTimeMin) regionRrTimeMin = tRR;
                                            if (tRR > regionRrTimeMax) regionRrTimeMax = tRR;
                                            regionRrSum += tRR;
                                        }
                                    }

                                }
                                tPrevTick = tTick;
                            }
                            else if (code == ABeatCodeLow)
                            {
                                lowValue = ann.mExtraValue * mSampleUnitA;
                                lowOrg = lowValue;
                                if (bDoFilter)
                                {
                                    lowValue = avgLow.mDoFilter(lowValue);
                                }
                                tLowTick = ann.mAnnotationTimeTck;
                            }
                        }
                        else if (ann.mAnnotationCode == CAnnotationLine._cPlethTrans)
                        {
                            tTransTick = ann.mAnnotationTimeTck;
                        }
                    }
                    header += newLine;
                    header += "Strip" + ASeperator + "start"+ equal + "0" + ASeperator + "duration" + equal + durationSec.ToString("0.000") + ASeperator + "sec" + newLine;
                    header += "" + ASeperator + "N" + ASeperator + "min" + ASeperator + "max" + ASeperator + "avg" + ASeperator + "unit" + newLine;

                    avg = nCount == 0 ? 0 : lowSum / nCount;
                    header += "lowOrg" + ASeperator + nCount + ASeperator + lowMin.ToString("0.000") + ASeperator + lowMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + ALabelUnit + newLine;
                    avg = nCount == 0 ? 0 : highSum / nCount;
                    header += "highOrg" + ASeperator + nCount + ASeperator + highMin.ToString("0.000") + ASeperator + highMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + ALabelUnit + newLine;
                    avg = nCount == 0 ? 0 : hlSum / nCount;
                    header += "h-l" + ASeperator + nCount + ASeperator + hlMin.ToString("0.000") + ASeperator + hlMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + ALabelUnit + newLine;
                    avg = nCount == 0 ? 0 : lowAvgSum / nCount;
                    header += "lowAvg" + ASeperator + nCount + ASeperator + lowAvgMin.ToString("0.000") + ASeperator + lowAvgMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + ALabelUnit + newLine;
                    avg = nCount == 0 ? 0 : highAvgSum / nCount;
                    header += "highAvg" + ASeperator + nCount + ASeperator + highAvgMin.ToString("0.000") + ASeperator + highAvgMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + ALabelUnit + newLine;
                    avg = nCount == 0 ? 0 : hlAvgSum / nCount;
                    header += "h-l_Avg" + ASeperator + nCount + ASeperator + hlAvgMin.ToString("0.000") + ASeperator + hlAvgMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + ALabelUnit + newLine;

                    avg = rrCount == 0 ? 0 : rrSum / rrCount;
                    header += "rr" + ASeperator + rrCount + ASeperator + rrTimeMin.ToString("0.000") + ASeperator + rrTimeMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + "sec" + newLine;

                    hrMin = rrCount == 0 || rrTimeMax < 0.01 ? 0 : 60.0 / rrTimeMax;
                    hrMax = rrCount == 0 || rrTimeMin < 0.01 ? 0 : 60.0 / rrTimeMin;
                    hrAvg = rrCount == 0 || rrSum < 0.01 ? 0 : 60.0 / (rrSum / rrCount);
                    header += "HR" + ASeperator + rrCount + ASeperator + hrMin.ToString("0.000") + ASeperator + hrMax.ToString("0.000") + ASeperator + hrAvg.ToString("0.000") + ASeperator + "BPM" + newLine;

                    if( ARegionDurationSec > 0 && regionNCount > 0)

                    {
                        header += newLine;
                        header += "Region" + ASeperator + "start" + equal + ARegionStartSec.ToString("0.000") + ASeperator + "duration" + equal + ARegionDurationSec.ToString("0.000") + ASeperator + "sec" + newLine;

                        header += "" + ASeperator + "N" + ASeperator + "min" + ASeperator + "max" + ASeperator + "avg" + ASeperator + "unit" + newLine;

                        avg = regionNCount == 0 ? 0 : regionLowSum / regionNCount;
                        header += "lowOrg" + ASeperator + regionNCount + ASeperator + regionLowMin.ToString("0.000") + ASeperator + regionLowMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + ALabelUnit + newLine;
                        avg = regionNCount == 0 ? 0 : regionHighSum / regionNCount;
                        header += "highOrg" + ASeperator + regionNCount + ASeperator + regionHighMin.ToString("0.000") + ASeperator + regionHighMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + ALabelUnit + newLine;
                        avg = regionNCount == 0 ? 0 : regionHlSum / regionNCount;
                        header += "h-l" + ASeperator + regionNCount + ASeperator + regionHlMin.ToString("0.000") + ASeperator + regionHlMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + ALabelUnit + newLine;
                        avg = regionNCount == 0 ? 0 : regionAvgLowSum / nCount;
                        header += "lowAvg" + ASeperator + regionNCount + ASeperator + regionAvgLowMin.ToString("0.000") + ASeperator + regionAvgLowMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + ALabelUnit + newLine;
                        avg = regionNCount == 0 ? 0 : regionAvgHighSum / regionNCount;
                        header += "highAvg" + ASeperator + regionNCount + ASeperator + regionAvgHighMin.ToString("0.000") + ASeperator + regionAvgHighMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + ALabelUnit + newLine;
                        avg = regionNCount == 0 ? 0 : regionHlAvgSum / regionNCount;
                        header += "h-l_Avg" + ASeperator + regionNCount + ASeperator + regionHlAvgMin.ToString("0.000") + ASeperator + regionHlAvgMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + ALabelUnit + newLine;

                        avg = regionRrCount == 0 ? 0 : regionRrSum / regionRrCount;
                        header += "rr" + ASeperator + regionRrCount + ASeperator + regionRrTimeMin.ToString("0.000") + ASeperator + regionRrTimeMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + "sec" + newLine;

                        hrMin = regionRrCount == 0 || regionRrTimeMax < 0.01 ? 0 : 60.0 / regionRrTimeMax;
                        hrMax = regionRrCount == 0 || regionRrTimeMin < 0.01 ? 0 : 60.0 / regionRrTimeMin;
                        hrAvg = regionRrCount == 0 || regionRrSum < 0.01 ? 0 : 60.0 / (regionRrSum / regionRrCount);
                        header += "HR" + ASeperator + regionRrCount + ASeperator + hrMin.ToString("0.000") + ASeperator + hrMax.ToString("0.000") + ASeperator + hrAvg.ToString("0.000") + ASeperator + "BPM" + newLine;
                    }
                    result = header + newLine + table;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("", ex);
                }
            }
            return result;
        }
        public string mInfoAnnotationCsv(string ARecordID, CAnnotation AAnnotation,
            UInt16 ABeatCode,
            UInt16 ANrAvgPoints,
            float ARegionStartSec, float ARegionDurationSec, string ALabelName, string ALabelUnit, string ASeperator = "\t")
        {
            string result = "";

            if (AAnnotation != null && AAnnotation.mAnnotationsList != null)
            {
                string header = "";
                string newLine = "\r\n";
                string table = "";
                string equal = ASeperator + "=" + ASeperator;
                float unitT = AAnnotation.mUnitT;
                float sampleFreq = unitT < 0.0001 ? 0 : 1.0F / unitT;
                string unitLabel = "(" + ALabelUnit + ")";
                float durationSec = mGetSamplesTotalTime();


                try
                {
                    header = ARecordID + ASeperator + CProgram.sDateTimeToString(mBaseUTC) + " U" + CProgram.sTimeZoneOffsetStringLong((Int16)mTimeZoneOffsetMin) + newLine;
                    header += "var" + equal + ALabelName + ASeperator + ALabelUnit + newLine;
                    header += "sampleUnitT" + equal + AAnnotation.mUnitT.ToString("0.00000") + ASeperator + "sec" + ASeperator + sampleFreq.ToString("0.0") + ASeperator + "Sps" + newLine;
                    header += "avgN" + equal + ANrAvgPoints + newLine;
                    header += newLine;
                    // min, max, avg, n
                    // rrTime min, max, avg

                    table = "(sec)" + ASeperator + "(sec)" + ASeperator + "BPM" + ASeperator + unitLabel + ASeperator + "01"+ ASeperator + unitLabel + newLine;
                    table += "t" + ASeperator + "tRR" + ASeperator + "HR" + ASeperator
                        + "A" + ASeperator + "Region" + ASeperator + "AAvg" + newLine;

                    UInt32 regionStartTick = ARegionStartSec <= 0 ? 0 : (UInt32)(ARegionStartSec * sampleFreq);
                    UInt32 regionEndTick = ARegionDurationSec < unitT ? 0 : (UInt32)((ARegionStartSec + ARegionDurationSec) * sampleFreq);
                    Int32 nCount = 0;
                    double valueMin = 1e9;
                    double valueMax = -1e9;
                    double valueSum = 0;
                    double avgMin = 1e9;
                    double avgMax = -1e9;
                    double avgSum = 0;

                    Int32 regionNCount = 0;
                    double regionMin = 1e9;
                    double regionMax = -1e9;
                    double regionSum = 0;
                    double regionAvgMin = 1e9;
                    double regionAvgMax = -1e9;
                    double regionAvgSum = 0;

                    UInt32 rrCount = 0;
                    double rrTimeMin = 1e9;
                    double rrTimeMax = -1e9;
                    double rrSum = 0;
                    UInt32 regionRrCount = 0;
                    double regionRrTimeMin = 1e9;
                    double regionRrTimeMax = -1e9;
                    double regionRrSum = 0;

                    bool bDoFilter = ANrAvgPoints > 1;

                    UInt32 tTick;
                    UInt32 maxTick = AAnnotation.mUnitT > 0.0001 ? (UInt32)(4 / AAnnotation.mUnitT) : 40000;  // 15 BPM beat is to far appart
                    UInt32 tPrevTick = UInt32.MaxValue;
                    double value = 0, valueOrg = 0, avg, hrMin, hrMax, hrAvg;
                    UInt16 code;
                    float t;
                    bool bUseHistory = true;
                    CAvgPointDblFilter avgLow = new CAvgPointDblFilter(bUseHistory, ANrAvgPoints);

                    foreach (CAnnotationLine ann in mAnnotationAtr.mAnnotationsList)
                    {
                        if (ann.mbExtraValueSet)
                        {
                            code = ann.mAnnotationCode;
                            if (code == ABeatCode)
                            {
                                tTick = ann.mAnnotationTimeTck;
                                t = mAnnotationAtr.mGetTimeSec(tTick);
                                value = ann.mExtraValue * mSampleUnitA;
                                valueOrg = value;
                                if (bDoFilter)
                                {
                                    value = avgLow.mDoFilter(value);
                                }
                                {
                                    // log values
                                    float tValue = tTick * unitT;
                                    float tRR = (nCount == 0 ? 0 : tTick - tPrevTick) * unitT;
                                    float bpm = tRR <= 0.001 ? 0 : 60.0F / tRR;
                                    bool bRegion = tTick >= regionStartTick && tTick < regionEndTick;

                                    string line = tValue.ToString("0.000") + ASeperator
                                        + tRR.ToString("0.000") + ASeperator + bpm.ToString("0.0") + ASeperator
                                        + valueOrg.ToString("0.000") + ASeperator + (bRegion ? "1" : "0") + ASeperator
                                        + value.ToString("0.000");

                                    table += line + newLine;

                                    // calc min, max, sum

                                    ++nCount;
                                    if (valueOrg < valueMin) valueMin = valueOrg;
                                    if (valueOrg > valueMax) valueMax = valueOrg;
                                    valueSum += valueOrg;

                                    if (value < avgMin) avgMin = value;
                                    if (value > avgMax) avgMax = value;
                                    avgSum += value;

                                    if (tRR > 0.01)
                                    {
                                        ++rrCount;
                                        if (tRR < rrTimeMin) rrTimeMin = tRR;
                                        if (tRR > rrTimeMax) rrTimeMax = tRR;
                                        rrSum += tRR;
                                    }
                                    if (bRegion)
                                    {
                                        ++regionNCount;
                                        if (valueOrg < regionMin) regionMin = valueOrg;
                                        if (valueOrg > regionMax) regionMax = valueOrg;
                                        regionSum += valueOrg;

                                        if (value < regionAvgMin) regionAvgMin = value;
                                        if (value > regionAvgMax) regionAvgMax = value;
                                        regionAvgSum += value;

                                        if (tRR > 0.01)
                                        {
                                            ++regionRrCount;
                                            if (tRR < regionRrTimeMin) regionRrTimeMin = tRR;
                                            if (tRR > regionRrTimeMax) regionRrTimeMax = tRR;
                                            regionRrSum += tRR;
                                        }
                                    }

                                }
                                tPrevTick = tTick;
                            }
                        }
                    }
                    header += newLine;
                    header += "Strip" + ASeperator + "start" + equal + "0" + ASeperator + "duration" + equal + durationSec.ToString("0.000") + ASeperator + "sec" + newLine;
                    header += "" + ASeperator + "N" + ASeperator + "min" + ASeperator + "max" + ASeperator + "avg" + ASeperator + "unit" + newLine;

                    avg = nCount == 0 ? 0 : valueSum / nCount;
                    header += "valueOrg" + ASeperator + nCount + ASeperator + valueMin.ToString("0.000") + ASeperator + valueMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + ALabelUnit + newLine;
                    avg = nCount == 0 ? 0 : avgSum / nCount;
                    header += "valueAvg" + ASeperator + nCount + ASeperator + avgMin.ToString("0.000") + ASeperator + avgMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + ALabelUnit + newLine;

                    avg = rrCount == 0 ? 0 : rrSum / rrCount;
                    header += "rr" + ASeperator + rrCount + ASeperator + rrTimeMin.ToString("0.000") + ASeperator + rrTimeMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + "sec" + newLine;

                    hrMin = rrCount == 0 || rrTimeMax < 0.01 ? 0 : 60.0 / rrTimeMax;
                    hrMax = rrCount == 0 || rrTimeMin < 0.01 ? 0 : 60.0 / rrTimeMin;
                    hrAvg = rrCount == 0 || rrSum < 0.01 ? 0 : 60.0 / (rrSum / rrCount);
                    header += "HR" + ASeperator + rrCount + ASeperator + hrMin.ToString("0.000") + ASeperator + hrMax.ToString("0.000") + ASeperator + hrAvg.ToString("0.000") + ASeperator + "BPM" + newLine;

                    if (ARegionDurationSec > 0 && regionNCount > 0)

                    {
                        header += newLine;
                        header += "Region" + ASeperator + "start" + equal + ARegionStartSec.ToString("0.000") + ASeperator + "duration" + equal + ARegionDurationSec.ToString("0.000") + ASeperator + "sec" + newLine;

                        header += "" + ASeperator + "N" + ASeperator + "min" + ASeperator + "max" + ASeperator + "avg" + ASeperator + "unit" + newLine;

                        avg = regionNCount == 0 ? 0 : regionSum / regionNCount;
                        header += "valueOrg" + ASeperator + regionNCount + ASeperator + regionMin.ToString("0.000") + ASeperator + regionMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + ALabelUnit + newLine;
                        avg = regionNCount == 0 ? 0 : regionAvgSum / nCount;
                        header += "valueAvg" + ASeperator + regionNCount + ASeperator + regionAvgMin.ToString("0.000") + ASeperator + regionAvgMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + ALabelUnit + newLine;
                        avg = regionRrCount == 0 ? 0 : regionRrSum / regionRrCount;
                        header += "rr" + ASeperator + regionRrCount + ASeperator + regionRrTimeMin.ToString("0.000") + ASeperator + regionRrTimeMax.ToString("0.000") + ASeperator + avg.ToString("0.000") + ASeperator + "sec" + newLine;

                        hrMin = regionRrCount == 0 || regionRrTimeMax < 0.01 ? 0 : 60.0 / regionRrTimeMax;
                        hrMax = regionRrCount == 0 || regionRrTimeMin < 0.01 ? 0 : 60.0 / regionRrTimeMin;
                        hrAvg = regionRrCount == 0 || regionRrSum < 0.01 ? 0 : 60.0 / (regionRrSum / regionRrCount);
                        header += "HR" + ASeperator + regionRrCount + ASeperator + hrMin.ToString("0.000") + ASeperator + hrMax.ToString("0.000") + ASeperator + hrAvg.ToString("0.000") + ASeperator + "BPM" + newLine;
                    }
                    result = header + newLine + table;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("", ex);
                }
            }
            return result;
        }
        private void mResampleSignal(CSignalData AFromSignal, DateTime AFromUTC, double AFromSampleFreq,
            CSignalData AToSignal, UInt32 AToNrSamples, DateTime AToStartUTC, double AToSamplesPerSec, Int32 ALowDiscard)
        {
            if (AFromSignal != null && AFromSignal.mNrValues > 0 && AFromSampleFreq > 0.01 
                && AToSignal != null  && AToSignal.mArraySize >= AToNrSamples && AToSamplesPerSec > 0.01)
            {
                try
                {
                    UInt32 iFromPrev = 0;
                    UInt32 iFromLast = AFromSignal.mNrValues - 1;
                    Int32 v, vNext, vDelta;

                    Int32[] fromValues = AFromSignal.mValues;
                    Int32[] toValues = AToSignal.mValues;

                    double fromUnitT = 1.0 / AFromSampleFreq;
                    double toUnitT = 1.0 / AToSamplesPerSec;
                    double tFromTo = (AFromUTC - AToStartUTC).TotalSeconds;
                    double tTo, tFrom, iFrom, f;

                    for( UInt32 iTo = 0; iTo < AToNrSamples; ++iTo )
                    {
                        tTo = iTo * toUnitT;
                        tFrom = tTo - tFromTo;
                        iFrom = tFrom * AFromSampleFreq;

                        iFromPrev = iFrom <= 0 ? 0 : (UInt32)iFrom;
                        if (iFromPrev > iFromLast) iFromPrev = iFromLast;

                        v = fromValues[iFromPrev];  // get value from from 
                        if( iFromPrev < iFromLast && v > ALowDiscard)       // do not interpolate ivalid values like pleth = 0
                        {
                            vNext = fromValues[iFromPrev+1];  // interpolate to get smooth graph
                            if (vNext > ALowDiscard)
                            {
                                vDelta = vNext - v;
                                f = iFrom - iFromPrev;
                                v += (Int32)(f * vDelta);
                            }
                        }
                        AToSignal.mAddValue(v);
                    }

                    if( AToSignal.mNrValues != AToNrSamples)
                    {
                        UInt32 n = AToSignal.mNrValues;
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("", ex);
                }
            }


        }

        public CRecordMit mResampleRecord( double AFromSampleFreq, DateTime AToStartUTC, UInt32 AToNrSamples, double AToSamplesPerSec, Int32 ALowDiscard)
        {
            CRecordMit rec = null;
            bool bOk = false;

            if (mNrSignals > 0 && mSignals != null && mNrSamples > 0 && AToNrSamples > 0 && AFromSampleFreq > 0.01 && AToSamplesPerSec > 0.01)
            {
                try
                {
                    rec = new CRecordMit(mGetSqlConnection());

                    if( rec != null )
                    {
                        if( rec.mbCreateSignals(mNrSignals) && rec.mbCreateAllSignalData(AToNrSamples))
                        {
                            for( UInt16 iSignal = 0; iSignal < mNrSignals; ++iSignal)
                            {
                                mResampleSignal(mSignals[iSignal], mBaseUTC, AFromSampleFreq,
                                    rec.mSignals[iSignal], AToNrSamples, AToStartUTC, AToSamplesPerSec, ALowDiscard);
                                rec.mNrSamplesPerSignal = rec.mSignals[iSignal].mNrValues;
                            }
                        }
                        rec.mNrSamples = rec.mNrSamplesPerSignal;
                        bOk = rec.mNrSamples > 0;
                    }

                }
                catch (Exception ex)
                {
                    CProgram.sLogException("", ex);
                }
            }


            return bOk ? rec : null;
        }
    }
}
