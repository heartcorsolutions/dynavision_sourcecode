﻿using Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventBoard.Event_Base
{
    public enum DTzCollectFileFormat
    {
        D6,
        YYYYMMDD_HH

    };

    public enum DTzCollectStudyMode
    {
        No,
        All,
        One
    };
    public enum DTzCollectStartMode
    {
        Start,
        First,
        Nr
    };
    public enum DTzCollectStopMode
    {
        End,
        Last,
        Nr
    };

    public class CTzCollectItem
    {
        public const string _cFID = "FID";
        public UInt32 _mFileID;
        public bool _mbPresent;
        public UInt16 _mTryRequestCounter;
        public DateTime _mLastRequestUTC;
        public UInt32 _mLastActionID;


        // vars for last scan
        public UInt16 _mFileCount;
        public string _mFileName;
        public DateTime _mFileUTC;
        public UInt32 _mFileSize;

        public bool _mbDoCollect;

        public CTzCollectItem()
        {
            _mFileID = 0;
            _mbPresent = false;
            _mTryRequestCounter = 0;
            _mLastRequestUTC = DateTime.MinValue;
            _mLastActionID = 0;
            _mFileCount = 0;
            _mFileName = "";
            _mFileUTC = DateTime.MinValue;
            _mbDoCollect = false;
        }
        public void mClear()
        {
            _mFileID = 0;
            _mbPresent = false;
            _mTryRequestCounter = 0;
            _mLastActionID = 0;
            _mLastRequestUTC = DateTime.MinValue;
            _mFileCount = 0;
            _mFileName = "";
            _mFileUTC = DateTime.MinValue;
            _mbDoCollect = false;
        }
        public void mResetBeforeScan()
        {
            _mFileCount = 0;
  //          _mFileName = "";
  //          _mFileUTC = DateTime.MinValue;
            _mbDoCollect = false;
            _mbPresent = false;
        }
        public bool mbReCalcPresent()
        {
            _mbPresent = _mFileCount > 0;
            return _mbPresent;
        }

        public bool mbMakeVarItem(out string ArVarName, out string ArVarValue)
        {
            string varName = "";
            string varValue = "";
            bool bOk = false;

            if (_mFileID > 0)
            {
                varName = _cFID + _mFileID.ToString("00000000");

                varValue = CItemKey.sMakeBoolTextString(_mbPresent)
                    + CItemKey._cCharSplit + _mTryRequestCounter.ToString()
                    + CItemKey._cCharSplit + CItemKey.sMakeDateTimeString(_mLastRequestUTC)
                    + CItemKey._cCharSplit + _mFileCount.ToString()
                    + CItemKey._cCharSplit + _mFileName
                    + CItemKey._cCharSplit + CItemKey.sMakeDateTimeString(_mFileUTC)
                    + CItemKey._cCharSplit + _mFileSize.ToString()
                    + CItemKey._cCharSplit + CItemKey.sMakeBoolTextString(_mbDoCollect)
                    + CItemKey._cCharSplit + _mLastActionID.ToString()
                    ;
                bOk = true;
            }

            ArVarName = varName;
            ArVarValue = varValue;
            return bOk;
        }

        public static CTzCollectItem sParseVarItem(string AVarName, string AVarValue)
        {
            CTzCollectItem item = null;

            if (AVarName != null && AVarValue != null && AVarName.Length > 0 && AVarValue.Length > 0)
            {
                string varName = AVarName.ToUpper().Trim();
                if (varName.StartsWith(_cFID))
                {
                    UInt32 fileID;
                    if (UInt32.TryParse(varName.Substring(3), out fileID)
                        && fileID > 0)
                    {
                        string[] varValues = AVarValue.Split(CItemKey._cCharSplit);
                        int nValues = varValues == null ? 0 : varValues.Length;
                        bool bParsed = false;
                        int iParse = 0;

                        if (nValues > 1)
                        {
                            item = new CTzCollectItem();

                            if (item != null)
                            {
                                item._mFileID = fileID;
                                bParsed = true;

                                if (iParse < nValues) bParsed &= CItemKey.sbParseBoolString(out item._mbPresent, varValues[iParse++]);
                                if (iParse < nValues) bParsed &= UInt16.TryParse(varValues[iParse++], out item._mTryRequestCounter);
                                if (iParse < nValues) bParsed &= CItemKey.sbParseDateTimeString(out item._mLastRequestUTC, varValues[iParse++]);
                                if (iParse < nValues) bParsed &= UInt16.TryParse(varValues[iParse++], out item._mFileCount);
                                if (iParse < nValues) item._mFileName = varValues[iParse++].Trim();
                                if (iParse < nValues) bParsed &= CItemKey.sbParseDateTimeString(out item._mFileUTC, varValues[iParse++]);
                                if (iParse < nValues) bParsed &= UInt32.TryParse(varValues[iParse++], out item._mFileSize);
                                if (iParse < nValues) bParsed &= CItemKey.sbParseBoolString(out item._mbDoCollect, varValues[iParse++]);
                                if (iParse < nValues) bParsed &= UInt32.TryParse(varValues[iParse++], out item._mLastActionID);
                            }
                        }
                    }
                }
            }
            return item;
        }
    }
    public class CTzAutoCollect
    {
        public const UInt32 _cMaxFileID = 2147123112;  // max month fitting in UINT32 2 ^31 = 2147483648

        public static UInt32 _sMinFileSize = 10;        // only add files that are a minimum size
        public static UInt32 _sReqAgeMin = 120;   // Only request for files that are at least x minutes old
        public static UInt32 _sScanTimeMin = 15;      // wait to do a scan after last scan
        public static UInt32 _sMaxTryRequest = 5;       // maximum number of request for a file
        public static UInt32 _sWaitTryRequestMin = 120; // wait time before requesting the same file again

        public /*static */const bool _cbReqScpOneByOne = false; // Request unsend SCP not allowed 

        public static UInt32 _sActionMaxNrReq = 250;                  // max nr File request in a action
        public static UInt16 _sActionMaxNrInBlock = 200;   // must be <= 250 _ 
        public static UInt16 _sActionMaxNrOfBlocks = 200;   // must be <= 250 _ 
        public static UInt32 _sMaxActionsInQueue = 1;   // do not add action if queue is full occupied 


        // definition type on constructor
        public string _mName;
        public DTzCollectFileFormat _mFileFormat;
        public string _mFileExtention;

        // variables
        public bool _mbEnabled;
        public DTzCollectStudyMode _mStudyMode;
        public UInt32 _mStudy_IX;
        public DTzCollectStartMode _mStartMode;
        public UInt32 _mStartFileNr;
        public DTzCollectStopMode _mStopMode;
        public UInt32 _mStopFileNr;

        // file load status
        public DateTime _mParamFileUTC;
        public bool _mbParamFileLoaded;
        public string _mParamSerialNr;

        // scanned from Study Recorder => <deviceID>.collectTzr
        public bool _mbScanFileLoaded;
        public UInt32 _mScanNrFilesLoaded = 0;
        public UInt32 _mScanNrFilesPresent = 0;
        public UInt32 _mScanNrFilesAdded = 0;
        public UInt32 _mScanNrFilesTotal = 0;

//        public string _mLastScanUserPC = "";
//        public DateTime _mLastScanUTC = DateTime.MinValue;

//        public UInt32 _mScanNrFilesFolder = 0;

//        public UInt32 _mFoundFirstFileID = 0;
//        public DateTime _mFoundFirstFileUTC = DateTime.MinValue;

        //public UInt32 _mFoundLastFileID = 0;
//        public DateTime _mFoundLastFileUTC = DateTime.MinValue;

        // calc missing parameters
        public UInt32 _mCalcStartFileID;
        public UInt32 _mCalcEndFileID;
        public UInt32 _mCalcFoundPresent;
        public UInt32 _mCalcFoundMissing;
        public UInt32 _mCalcFoundNewPresent;
        public UInt32 _mCalcFoundNewMissing;

        // calc Range and run values
        public UInt32 _mFirstPresentFileID = UInt32.MaxValue;        // find range of present FileID
        DateTime _mFirstPresentFileUTC = DateTime.MaxValue;
        public UInt32 _mLastPresentFileID = UInt32.MinValue;
        DateTime _mLastPresentFileUTC = DateTime.MinValue;

        UInt32 _mFirstNormalFileID = UInt32.MaxValue;         // find range FileID of normal receceived (not requested )
        DateTime _mFirstNormalFileUTC = DateTime.MaxValue;
        UInt32 _mLastNormalFileID = UInt32.MinValue;
        DateTime _mLastNormalFileUTC = DateTime.MinValue;

        UInt32 _mFirstRecvFileID = UInt32.MaxValue;           // find received first / last
        DateTime _mFirstRecvFileUTC = DateTime.MaxValue;
        UInt32 _mLastRecvFileID = UInt32.MinValue;
        DateTime _mLastRecvFileUTC = DateTime.MinValue;

        List<string> _mScanCalcResult = null;

        // run range calc missing
        public UInt32 _mRunStartFileID = 0;
        public UInt32 _mRunEndFileID = 0;

        UInt32 _mRunNTotal = 0;
        UInt32 _mRunNPresent = 0;
        UInt32 _mRunNMissing = 0;
        UInt32 _mRunNFailed = 0;
        UInt32 _mRunNWait = 0;
        UInt32 _mRunNCollectNow = 0;

        List<String> _mRunCollect = null;
        List<String> _mRunMissing = null;


        // action
        public bool _mbActionGenerated;
        public DateTime _mActionSendUTC;
        public UInt32 _mActionSeqNr;
        public UInt32 _mActionReqNrFiles;

 
        public List<CTzCollectItem> _mScanFileIdList;
        public uint _mScanListCount = 0;
        public uint _mScanListLastID = 0;

        // scanned from Study Recorder => <deviceID>.missingTzr
        public bool _mbMissingLoaded;
        public DateTime _mMissingFileDT;
        public string[] _mMissingList;
        public UInt32 _mMissingCount;

        public const string _cAutoCollectGroup = "AutoCollect";
        public const string _cScanCollectGroup = "ScanCollect";

        public const UInt32 _cFileSeed = 0x54132523;

        public CTzDeviceSettings _mTzSettings = null;



        public CTzAutoCollect(string AName, DTzCollectFileFormat AFileFormat, string AFileExtention)
        {
            _mName = AName;
            _mFileFormat = AFileFormat;
            _mFileExtention = AFileExtention.ToLower();
            _mbEnabled = false;
            _mStudyMode = DTzCollectStudyMode.One;
            _mStudy_IX = 0;
            _mStartMode = DTzCollectStartMode.Start;
            _mStartFileNr = 0;
            _mStopMode = DTzCollectStopMode.End;
            _mStopFileNr = 0;

            // status load
            _mParamFileUTC = DateTime.MinValue;
            _mbParamFileLoaded = false;
            _mParamSerialNr = "";

            // scanned from Study Recorder => <deviceID>.collectList
            _mbScanFileLoaded = false;
//            _mFoundFirstFileID = 0;
//            _mFoundLastFileID = 0;
            _mScanFileIdList = null;

            _mCalcStartFileID = 0;
            _mCalcEndFileID = 0;
            _mCalcFoundPresent = 0;
            _mCalcFoundMissing = 0;
            _mCalcFoundNewPresent = 0;
            _mCalcFoundNewMissing = 0;


            _mbActionGenerated = false;
            _mActionSendUTC = DateTime.MinValue;
            _mActionSeqNr = 0;
            _mActionReqNrFiles = 0;

            // scanned from Study Recorder => <deviceID>.missingList
            _mbMissingLoaded = false;
            _mMissingFileDT = DateTime.MinValue;
            _mMissingList = null;
            _mMissingCount = 0;
        }

           public string mMakeShowString(UInt32 AValue)
        {
            string s = _mFileFormat == DTzCollectFileFormat.YYYYMMDD_HH
                ? sMakeShowYMDH(AValue)
                : AValue.ToString("000000");    // D6
            return s;
        }
        public string mMakeFileString(UInt32 AValue)
        {
            string s = _mFileFormat == DTzCollectFileFormat.YYYYMMDD_HH
                ? mMakeFileYMDH(AValue)
                : AValue.ToString("000000");    // D6
            return s;
        }

        public static UInt32 sMakeUInt(UInt16 AYear, UInt16 AMonth, UInt16 ADay, uint AHour)
        {
            // 2^31 = 2147483648 = 2147/48/36-48 max date 2147/12/31-23
            return (UInt32)(((AYear * 100 + AMonth) * 100 + ADay) * 100 + AHour);
        }
        public static UInt32 sMakeUInt(DateTime ADate)
        {
            // 2^31 = 2147483648 = 2147/48/36-48 max date 2147/12/31-23
            return (UInt32)(((ADate.Year * 100 + ADate.Month) * 100 + ADate.Day) * 100 + ADate.Hour);
        }
        public static string sMakeShowYMDH(UInt32 AValueYMDH)
        {
            UInt32 i = AValueYMDH;
            UInt32 hour = i % 100;
            i = i / 100;
            UInt32 day = i % 100;
            i = i / 100;
            UInt32 month = i % 100;
            UInt32 year = i / 100;

            return year.ToString("0000") + "/" + month.ToString("00") + "/" + day.ToString("00") + "-" + hour.ToString("00");
        }
        public static string mMakeFileYMDH(UInt32 AValueYMDH)
        {
            UInt32 i = AValueYMDH;
            UInt32 hour = i % 100;
            i = i / 100;

            return i.ToString("00000000") + "_" + hour.ToString("00");
        }
        public static bool sbMakeDate(out DateTime ArDateTime, UInt32 AValueYMDH)
        {
            bool bOk = false;
            DateTime dt = DateTime.MinValue;

            try
            {
                UInt32 i = AValueYMDH;
                UInt32 hour = i % 100;
                i = i / 100;
                UInt32 day = i % 100;
                i = i / 100;
                UInt32 month = i % 100;
                UInt32 year = i / 100;

                dt = new DateTime((int)year, (int)month, (int)day, (int)hour, 0, 0);
                bOk = true;
            }
            catch (Exception)
            {

            }
            ArDateTime = dt;
            return bOk;
        }
        public static bool sbSplitYMDH(out UInt16 ArYear, out UInt16 ArMonth, out UInt16 ArDay, out UInt16 ArHour, UInt32 AValueYMDH)
        {
            bool bOk = false;
            UInt16 year = 0;
            UInt16 month = 0;
            UInt16 day = 0;
            UInt16 hour = 0;

            try
            {
                UInt32 i = AValueYMDH;
                hour = (UInt16)(i % 100);
                i = i / 100;
                day = (UInt16)(i % 100);
                i = i / 100;
                month = (UInt16)(i % 100);
                year = (UInt16)(i / 100);

                DateTime dt = new DateTime((int)year, (int)month, (int)day, (int)hour, 0, 0);   // check date is ok
                bOk = true;
            }
            catch (Exception)
            {

            }
            ArYear = year;
            ArMonth = month;
            ArDay = day;
            ArHour = hour;
            return bOk;
        }
        public bool mbIsNextValue(UInt32 AFirstValue, UInt32 ANextValue)
        {
            bool bIsNext = false;

            if (_mFileFormat == DTzCollectFileFormat.YYYYMMDD_HH)
            {
                DateTime firstDT;
                if (sbMakeDate(out firstDT, AFirstValue))
                {
                    DateTime nextDT = firstDT.AddHours(1);  // calc next hour
                    UInt32 nextYMDH = sMakeUInt(nextDT);
                    bIsNext = nextYMDH == ANextValue;
                }
            }
            else
            {
                bIsNext = AFirstValue + 1 == ANextValue;
            }

            return bIsNext;
        }
        public UInt32 mGetNextValue(UInt32 AValue)
        {
            UInt32 nextValue = UInt32.MaxValue;

            if (_mFileFormat == DTzCollectFileFormat.YYYYMMDD_HH)
            {
                DateTime firstDT;
                if (sbMakeDate(out firstDT, AValue))
                {
                    DateTime nextDT = firstDT.AddHours(1);  // calc next hour
                    nextValue = sMakeUInt(nextDT);
                }
                
            }
            else
            {
                nextValue = AValue + 1;
            }

            return nextValue;
        }

        public string mMakeParamFileName(string ASerialNr)
        {
            return ASerialNr + "_TzAutoCollect" + _mName + ".param";
        }
        public string mMakeScanFileName(string ASerialNr, UInt32 AStudyIX)
        {
            return ASerialNr + "_TzAutoCollect" + _mName + "_S" + AStudyIX.ToString() + ".scan";
        }
        public string mMakeMissingFileName(string ASerialNr, UInt32 AStudyIX)
        {
            return ASerialNr + "_TzAutoCollect" + _mName + "_S" + AStudyIX.ToString() + ".missing";
        }

        public bool mbParseFileID(out UInt32 ArFileID, string AFileName)
        {
            bool bID = false;
            UInt32 fileID  = 0;
            int len;

            if( AFileName != null && (len=AFileName.Length) > 3)
            {
                string fileName = AFileName.ToLower();
                string snr = _mParamSerialNr.ToLower() + "_";

                if( fileName.EndsWith( _mFileExtention))
                {
                    if( fileName.StartsWith( snr))
                    {
                        string s = fileName.Substring(snr.Length, len - snr.Length - _mFileExtention.Length);
                        len = s.Length;                       
                            
                        switch(_mFileFormat)
                        {
                            case DTzCollectFileFormat.D6:
                                if( len >= 6 )
                                {
                                    bID = UInt32.TryParse(s.Substring(0,6), out fileID);
                                    if( bID )
                                    {
                                        bID = fileID > 0;
                                    }
                                }

                                break;
                            case DTzCollectFileFormat.YYYYMMDD_HH:
                                if( len >= 11)
                                {
                                    int ymd = -1, h = -1;

                                    bID = int.TryParse(s.Substring(0, 8), out ymd) && int.TryParse(s.Substring(9, 2), out h);
                                    if( bID )
                                    {
                                        bID = ymd > 20000000 && ymd < 21470000 && h >= 0 && h < 24;
                                        fileID = (UInt32)(ymd * 100 + h);
                                    }
                                }
                                break;

                        }
                    }
                }
            }


            ArFileID = fileID;
            return bID;
        }

        public string mParamFileInfoLoadNew(string ADevicePath, string ASerialNr, bool AbLoadNew)
        {
            string info = "";
            string fileName = mMakeParamFileName(ASerialNr);
            string filePath = Path.Combine(ADevicePath, fileName);

            try
            {
                FileInfo fi = new FileInfo(filePath);

                if (false == fi.Exists)
                {
                    info = "not present";
                    _mbParamFileLoaded = false;
                    _mParamSerialNr = "";
                }
                else
                {
                    info = CProgram.sDateTimeToString(fi.LastWriteTime);

                    if (AbLoadNew)
                    {
                        if (false == _mbParamFileLoaded || fi.LastWriteTimeUtc > _mParamFileUTC)
                        {
                            bool bPresent = false;
                            bool bOk = mbLoadParamFile(out bPresent, ADevicePath, ASerialNr);

                            info += bOk ? " load ok" : (bPresent ? " load failed!" : "missing file" );
                            _mParamSerialNr = bOk ? ASerialNr : "";
                        }
                    }
                }
            }
            catch (Exception)
            {
                info = "error on find " + fileName;
            }
            return info;
        }

        public string mScanInfoLoadMissing(string ADevicePath, string ASerialNr, UInt32 AStudyIX, bool AbLoadMissing)
        {
            string info = "";
            string fileName = mMakeScanFileName(ASerialNr, AStudyIX);
            string studyPath;

            try
            {
                _mbMissingLoaded = false;
                _mMissingCount = 0;
                _mMissingList = null;

                if (AStudyIX == 0)
                {
                    info = "S0 no study";
                }
                else
                {
                    info = "S" + AStudyIX + ": ";

                    if (false == CDvtmsData.sbGetStudyRecorderDir(out studyPath, AStudyIX, true))
                    {
                        info += "no recordings";
                    }
                    else
                    {
                        string filePath = Path.Combine(studyPath, fileName);
                        FileInfo fi = new FileInfo(filePath);

                        if (false == fi.Exists)
                        {
                            info += "not scanned";
                        }
                        else
                        {
                            info += CProgram.sDateTimeToString(fi.LastWriteTime);

                            fileName = mMakeMissingFileName(ASerialNr, AStudyIX);

                            filePath = Path.Combine(studyPath, fileName);
                            fi = new FileInfo(filePath);

                            if (false == fi.Exists)
                            {
                                info += " no missing";
                            }
                            else
                            {
                                _mMissingList = File.ReadAllLines(filePath);

                                _mMissingCount = (UInt32)(_mMissingList == null ? 0 : _mMissingList.Length);

                                info += " missing " + _mMissingCount.ToString();
                                //                            fileName = mMakeMissingFileName
                            }

                        }
                    }
                }
            }
            catch (Exception)
            {
                info += ", error on find " + fileName;
            }
            return info;
        }

        public bool mbSaveParamFile(string ADevicePath, string ASerialNr)
        {
            bool bOk = false;

            string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);

            string fileName = mMakeParamFileName(storeSnr);
            string toFile = Path.Combine(ADevicePath, fileName);

            try
            {
                if (ASerialNr == null || ASerialNr.Length == 0)
                {
                    CProgram.sLogError("Collect save param No Serial");
                }
                else
                {
                    if (_mParamSerialNr == null || _mParamSerialNr.Length == 0)
                    {
                        _mParamSerialNr = ASerialNr;
                    }
                    if (_mParamSerialNr != ASerialNr)
                    {
                        CProgram.sLogError("Collect save param Serial " + _mParamSerialNr + "!=" + _mParamSerialNr);
                    }
                    else
                    {
                        CItemKey ik = new CItemKey(_cAutoCollectGroup + _mName);

                        if (ik != null)
                        {
                            ik.mbAddItemString("Name", _mName);  // just for info, is not read
                            ik.mbAddItemString("SerialNr", _mParamSerialNr);  // just for info, is not read
                            ik.mbAddItemString("FileFormat", _mFileFormat.ToString());  // just for info, is not read
                            ik.mbAddItemString("U_PC", CProgram.sGetUserAtPcString());
                            ik.mbAddItemDateTimeZone("saveUtcZ", DateTime.UtcNow, CProgram.sGetLocalTimeZoneOffsetMin());

                            ik.mbAddItemBoolText("Enabled", _mbEnabled);
                            ik.mbAddItemUInt32("StudyMode", (UInt32)_mStudyMode);
                            ik.mbAddItemUInt32("StudyIX", _mStudy_IX);
                            ik.mbAddItemUInt32("StartMode", (UInt32)_mStartMode);
                            ik.mbAddItemUInt32("StartFileID", _mStartFileNr);
                            ik.mbAddItemUInt32("StopMode", (UInt32)_mStopMode);
                            ik.mbAddItemUInt32("StopFileID", _mStopFileNr);

                            bOk = ik.mbWriteToFile(toFile, _cFileSeed, CProgram.sGetOrganisationLabel(), true, 0);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Collect save " + toFile, ex);
            }

            return bOk;
        }
        public bool mbLoadParamFile(out bool ArbPresent, string ADevicePath, string ASerialNr)
        {
            bool bOk = false;
            bool bPresent = false;
            string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);

            string fileName = mMakeParamFileName(storeSnr);
            string fromFile = Path.Combine(ADevicePath, fileName);


            try
            {
                _mbParamFileLoaded = false;
                FileInfo fi = new FileInfo(fromFile);

                if (fi == null)
                {
                    _mParamFileUTC = DateTime.MinValue;
                }
                else
                {
                    _mParamFileUTC = fi.LastWriteTimeUtc;
                    bPresent = fi.Exists;

                    CItemKey ik = new CItemKey(_cAutoCollectGroup + _mName);

                    if (ik != null && bPresent)
                    {
                        bool bChecksum;

                        bOk = ik.mbReadFromFile(out bChecksum, fromFile, _cFileSeed, CProgram.sGetOrganisationLabel(), false);

                        if (bOk)
                        {
                            UInt32 value;

                            bOk &= ik.mbGetItemBool(ref _mbEnabled, "Enabled");
                            value = 0;
                            bOk &= ik.mbGetItemUInt32(ref value, "StudyMode"); _mStudyMode = (DTzCollectStudyMode)value;
                            bOk &= ik.mbGetItemUInt32(ref _mStudy_IX, "StudyIX");
                            value = 0;
                            bOk &= ik.mbGetItemUInt32(ref value, "StartMode"); _mStartMode = (DTzCollectStartMode)value;
                            bOk &= ik.mbGetItemUInt32(ref _mStartFileNr, "StartFileID");
                            value = 0;
                            bOk &= ik.mbGetItemUInt32(ref value, "StopMode"); _mStopMode = (DTzCollectStopMode)value;
                            bOk &= ik.mbGetItemUInt32(ref _mStopFileNr, "StopFileID");
                        }
                        if (bOk)
                        {
                            _mbParamFileLoaded = true;
                            _mParamSerialNr = ASerialNr;
                        }
                    }
                }
                bOk = true;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Collect load " + fromFile, ex);
            }
            ArbPresent = bPresent;
            return bOk;
        }
        public bool mbScanFilesStudyRecord(ref string ArResult, out FileInfo[] ArFileList, string ASerialNr, UInt32 AStudyIX)
        {
            bool bOk = false;
            FileInfo[] fileList = null;
            string studyPath;

            try
            {
                if (ArResult.Length > 0) ArResult += ", ";

                if (AStudyIX == 0)
                {
                    ArResult += "S0 no study";
                }
                else
                {
                    ArResult = "S" + AStudyIX + ": ";

                    if (false == CDvtmsData.sbGetStudyRecorderDir(out studyPath, AStudyIX, true))
                    {
                        ArResult += "no recordings";
                    }
                    else
                    {
                        DirectoryInfo di = new DirectoryInfo(studyPath);

                        if (di != null)
                        {
                            fileList = di.GetFiles();
                            int n = fileList == null ? 0 : fileList.Length;

                            ArResult += n.ToString() + " files";
                            bOk = n > 0;
                        }
                        else
                        {
                            ArResult += " failed scan files";
                        }
                    }
                }
            }
            catch (Exception)
            {
                ArResult += ", error on scan files S" + AStudyIX.ToString();
            }
            ArFileList = fileList;
            return bOk;
        }

        public bool mbSaveScanFile(string ASerialNr, UInt32 AStudyIX, bool AbRunAutomatic, string AResult)
        {
            bool bOk = false;
            string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);


            string fileName = mMakeScanFileName(storeSnr, AStudyIX);
            string toFile = "";

            string studyPath;

            try
            {
                if (AStudyIX > 0 && CDvtmsData.sbGetStudyRecorderDir(out studyPath, AStudyIX, true))
                {
                    toFile = Path.Combine(studyPath, fileName);

                    CItemKey ik = new CItemKey(_cScanCollectGroup + _mName);

                    if (ik != null)
                    {
                        // add the settings used, only for info
                        ik.mbAddItemString("Name", _mName);  // just for info, is not read
                        ik.mbAddItemString("FileFormat", _mFileFormat.ToString());  // just for info, is not read
                        ik.mbAddItemDateTime("ParamFileUTC", _mParamFileUTC);
                        ik.mbAddItemString("U_PC", CProgram.sGetUserAtPcString());
                        ik.mbAddItemString("U_Prog", CProgram.sMakeUniqueProgName());
                        ik.mbAddItemDateTimeZone("saveUtcZ", DateTime.UtcNow, CProgram.sGetLocalTimeZoneOffsetMin());
                        ik.mbAddItemBoolText("Enabled", _mbEnabled);
                        ik.mbAddItemUInt32("StudyMode", (UInt32)_mStudyMode);
                        ik.mbAddItemUInt32("StudyIX", _mStudy_IX);
                        ik.mbAddItemUInt32("StartMode", (UInt32)_mStartMode);
                        ik.mbAddItemUInt32("StartFileID", _mStartFileNr);
                        ik.mbAddItemUInt32("StopMode", (UInt32)_mStopMode);
                        ik.mbAddItemUInt32("StopFileID", _mStopFileNr);
                        ik.mbAddItemBoolText("runAutomatic", AbRunAutomatic);

                        ik.mbAddItemString("SerialNR", ASerialNr);


                        // scan items
                        int n = _mScanFileIdList == null ? 0 : _mScanFileIdList.Count;
                        int i = 0;
                        string varName, varValue;
                        ik.mbAddItemInt32("nItems", n);
                        if (n > 0)
                        {
                            foreach (CTzCollectItem item in _mScanFileIdList)
                            {
                                if (item.mbMakeVarItem(out varName, out varValue))
                                {
                                    ik.mbAddItemString(varName, varValue);
                                    ++i;
                                }
                            }
                        }
                        // save scan settings

                        ik.mbAddItemUInt32("MinFileSize", _sMinFileSize);
                        ik.mbAddItemUInt32("AskBeforeNowMin", _sReqAgeMin);
                        ik.mbAddItemUInt32("ScanTimeMin", _sScanTimeMin);

                        // scan status
                        ik.mbAddItemUInt32("ScanNrFilesLoaded", _mScanNrFilesLoaded);
                        ik.mbAddItemUInt32("ScanNrFilesPresent", _mScanNrFilesPresent);
                        ik.mbAddItemUInt32("ScanNrFilesAdded", _mScanNrFilesAdded);
                        ik.mbAddItemUInt32("ScanNrFilesTotal", _mScanNrFilesTotal);

                        // calc Range and run values
                        ik.mbAddItemUInt32("firstPresentFileID", _mFirstPresentFileID);
                        ik.mbAddItemDateTime("firstPresentFileUTC", _mFirstPresentFileUTC);
                        ik.mbAddItemUInt32("lastPresentFileID", _mLastPresentFileID);
                        ik.mbAddItemDateTime("lastPresentFileUTC", _mLastPresentFileUTC);

                        ik.mbAddItemUInt32("firstNormalFileID", _mFirstNormalFileID);
                        ik.mbAddItemDateTime("firstNormalFileUTC", _mFirstNormalFileUTC);
                        ik.mbAddItemUInt32("lastNormalFileID", _mLastNormalFileID);
                        ik.mbAddItemDateTime("lastNormalFileUTC", _mLastNormalFileUTC);

                        ik.mbAddItemUInt32("firstRecvFileID", _mFirstRecvFileID);
                        ik.mbAddItemDateTime("firstRecvFileUTC", _mFirstRecvFileUTC);
                        ik.mbAddItemUInt32("lastRecvFileID", _mLastRecvFileID);
                        ik.mbAddItemDateTime("lastRecvFileUTC", _mLastRecvFileUTC);

                        ik.mbAddItemUInt32("CalcStartFileID", _mCalcStartFileID);
                        ik.mbAddItemUInt32("CalcEndFileID", _mCalcEndFileID);
                        ik.mbAddItemUInt32("CalcFoundPresent", _mCalcFoundPresent);
                        ik.mbAddItemUInt32("CalcFoundMissing", _mCalcFoundMissing);
                        ik.mbAddItemUInt32("CalcFoundNewPresent", _mCalcFoundNewMissing);
                        ik.mbAddItemUInt32("CalcFoundNewMissing", _mCalcFoundNewMissing);

                        ik.mbAddItemBoolText("ActionGenerated", _mbActionGenerated);
                        ik.mbAddItemDateTime("ActionSendUTC", _mActionSendUTC);
                        ik.mbAddItemUInt32("ActionSeqNr", _mActionSeqNr);
                        ik.mbAddItemUInt32("ActionReqNrFiles", _mActionReqNrFiles);

                        int iLine = 0;
                        foreach( string s in _mScanCalcResult)
                        {
                            ++iLine;
                            ik.mbAddItemString("scanResult"+ iLine.ToString( "00" ), s);
                        }
                        ik.mbAddItemString("scanTestResult", AResult);
  
                        bOk = ik.mbWriteToFile(toFile, _cFileSeed, CProgram.sGetOrganisationLabel(), true, 0);
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Collect scan save " + toFile, ex);
            }

            return bOk;
        }

        public bool mbAddScanItemSorted( CTzCollectItem AItem )
        {
            bool bOk = false;

            if (_mScanFileIdList != null && AItem != null)
            {
                uint fileID = AItem._mFileID;
                if( fileID > 0 )
                {
                    if (fileID > _mScanListLastID)    // items should be sorted from file and this is quicker then finding the end
                    {
                        _mScanListLastID = fileID;
                        _mScanFileIdList.Add(AItem);
                        ++_mScanListCount;
                        bOk = true;
                    }
                    else
                    {
                        for (int i = 0; i < _mScanListCount; ++i)
                        {
                            if (fileID == _mScanFileIdList[i]._mFileID)
                            {
                                CProgram.sLogError(_mName + " Collect double item " + fileID.ToString());
                            }
                            else if (fileID < _mScanFileIdList[i]._mFileID)
                            {
                                _mScanFileIdList.Insert(i, AItem);
                                ++_mScanListCount;
                                bOk = true;
                            }
                        }
                    }
                  
                }
            }
            return bOk;
        }

        public CTzCollectItem mFindCreateNew(out bool ArbCreatedNew, UInt32 AFindFileID, bool AbInsertNew)
        {
            CTzCollectItem foundItem = null;
            bool bNew = false;

            try
            {
                if (_mScanFileIdList == null)
                {
                    _mScanFileIdList = new List<CTzCollectItem>();
                    _mScanListLastID = 0;
                    _mScanListCount = 0;
                    _mScanListLastID = 0;
                }

                if (_mScanFileIdList != null && AFindFileID != 0)
                {

                    if (AFindFileID > _mScanListLastID)    // items should be sorted from file and this is quicker then finding the end
                    {
                        if (AbInsertNew)
                        {
                            foundItem = new CTzCollectItem();

                            if (foundItem != null)
                            {
                                _mScanListLastID = AFindFileID;
                                foundItem._mFileID = AFindFileID;
                                _mScanFileIdList.Add(foundItem);
                                ++_mScanListCount;
                                bNew = true;
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < _mScanListCount; ++i)
                        {
                            if (AFindFileID == _mScanFileIdList[i]._mFileID)
                            {
                                foundItem = _mScanFileIdList[i];
                                break;
                            }
                            else if (AFindFileID < _mScanFileIdList[i]._mFileID)
                            {
                                if (AbInsertNew)
                                {
                                    foundItem = new CTzCollectItem();

                                    if (foundItem != null)
                                    {
                                        foundItem._mFileID = AFindFileID;

                                        _mScanFileIdList.Insert(i, foundItem);
                                        ++_mScanListCount;
                                        bNew = true;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Collect scan find fileID " + AFindFileID, ex);
            }
            ArbCreatedNew = bNew;
            return foundItem;
        }

        public bool mbLoadScanFile(out bool ArbPresent, out bool ArbWait, string ASerialNr, UInt32 AStudyIX, bool AbCheckLastScanTime)
        {
            bool bOk = false;
            bool bPresent = false;
            bool bWait = false;
            string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);


            string fileName = mMakeScanFileName(storeSnr, AStudyIX);
            string fromFile = "";

            string studyPath;

            _mScanNrFilesLoaded = 0;
            _mScanNrFilesPresent = 0;
            _mScanListCount = 0;
            _mScanListLastID = 0;
            _mScanFileIdList = new List<CTzCollectItem>();    // empty list

            try
            {
                if (AStudyIX > 0 && CDvtmsData.sbGetStudyRecorderDir(out studyPath, AStudyIX, true))
                {
                    fromFile = Path.Combine(studyPath, fileName);

                    FileInfo fi = new FileInfo(fromFile);

                    bPresent = fi != null && fi.Exists;

                    if( bPresent && AbCheckLastScanTime )
                    {
                        double min = (DateTime.UtcNow - fi.LastWriteTimeUtc).TotalMinutes;
                        bWait = min < _sScanTimeMin;
                    }
                    
                    if( false == bWait && bPresent )
                    {
                        CItemKey ik = new CItemKey(_cScanCollectGroup + _mName);

                        if (ik != null && _mScanFileIdList != null)
                        {
                            bool bChecksum;

                            bOk = ik.mbReadFromFile(out bChecksum, fromFile, _cFileSeed, CProgram.sGetOrganisationLabel(), false);

                            if (bOk)
                            {
                                UInt32 value;

                                /*                           bOk &= ik.mbGetItemBool(out _mbEnabled, "Enabled");
                                                           bOk &= ik.mbGetItemUInt32(out value, "StudyMode"); _mStudyMode = (DTzCollectStudyMode)value;
                                                           bOk &= ik.mbGetItemUInt32(out _mStudy_IX, "StudyIX");
                                                           bOk &= ik.mbGetItemUInt32(out value, "StartMode"); _mStartMode = (DTzCollectStartMode)value;
                                                           bOk &= ik.mbGetItemUInt32(out _mStartFileNr, "StartFileID");
                                                           bOk &= ik.mbGetItemUInt32(out value, "StopMode"); _mStopMode = (DTzCollectStopMode)value;
                                                           bOk &= ik.mbGetItemUInt32(out _mStopFileNr, "StopFileID");
                               */
                                List<string> lines = ik.mGetLinesList();
                                int n = lines == null ? 0 : lines.Count;
                                string varName, varValue;
                                CTzCollectItem item = null;
                                char codeChar;

                                if (n > 0)
                                {
                                    // load previous generated action (store old if no action is generated at this time
                                    bool bRead = ik.mbGetItemDateTime(ref _mActionSendUTC, "ActionSendUTC");
                                    bRead &= ik.mbGetItemUInt32(ref _mActionSeqNr, "ActionSeqNr");
                                    bRead &= ik.mbGetItemUInt32(ref _mActionReqNrFiles, "ActionReqNrFiles");

                                    foreach (string s in lines)
                                    {
                                        if (ik.mbSplitItem(s, out codeChar, out varName, out varValue))
                                        {
                                            if (codeChar == CItemKey._cCharValid || codeChar == '\0')
                                            {
                                                item = CTzCollectItem.sParseVarItem(varName, varValue);
                                                if (item != null)
                                                {
                                                    mbAddScanItemSorted(item);
                                                    ++_mScanNrFilesLoaded;
                                                    if (item._mbPresent)
                                                    {
                                                        ++_mScanNrFilesPresent;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                     }
                    bOk = true;
                }
               
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Collect scan load " + fromFile, ex);
            }
            ArbPresent = bPresent;
            ArbWait = bWait;
            return bOk;
        }

        public bool mbAddAllFileInfos( FileInfo[] ArFileList, UInt32 AMinFileSize)
        {
            bool bOk = true;

            try
            {
                _mScanNrFilesAdded = 0;
                _mScanNrFilesTotal = 0;

                if (ArFileList != null )
                {
                    string fileName;
                    UInt32 fileID;
                    bool bNew;

                    foreach( CTzCollectItem item in _mScanFileIdList)
                    {
                        item.mResetBeforeScan();    // reset present and count etc
                    }

                    bOk = true;

                    foreach( FileInfo fi in ArFileList )
                    {
                        if (fi.Exists && fi.Length >= AMinFileSize)
                        {
                            fileName = fi.Name;

                            if (mbParseFileID(out fileID, fileName))
                            {
                                CTzCollectItem foundItem = mFindCreateNew(out bNew, fileID, true);

                                if (foundItem != null)
                                {
                                    if (bNew)
                                    {
                                        ++_mScanNrFilesAdded;
                                        foundItem._mFileCount = 1;
                                        foundItem._mFileSize = (UInt32)fi.Length;
                                        foundItem._mFileUTC = fi.LastWriteTimeUtc;
                                        foundItem._mFileName = fileName;
                                        foundItem._mbPresent = foundItem._mFileSize > AMinFileSize;
                                    }
                                    else if(foundItem._mFileCount == 0 )
                                    {
                                        foundItem._mFileCount = 1;
                                        foundItem._mFileSize = (UInt32)fi.Length;
                                        foundItem._mFileUTC = fi.LastWriteTimeUtc;
                                        foundItem._mFileName = fileName;
                                        foundItem._mbPresent = foundItem._mFileSize > AMinFileSize;
                                    }
                                    else
                                    {
                                        ++foundItem._mFileCount;
                                        
                                        if(foundItem._mFileUTC < fi.LastWriteTimeUtc)
                                        {
                                            foundItem._mFileSize = (UInt32)fi.Length;
                                            foundItem._mFileUTC = fi.LastWriteTimeUtc;
                                            foundItem._mFileName = fileName;
                                        }
                                        foundItem._mbPresent = foundItem._mFileSize > AMinFileSize;
                                    }
                                    ++_mScanNrFilesTotal;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("error on adding file info " + _mParamSerialNr + " S" + _mStudy_IX.ToString(), ex);
                bOk = false;
            }
            return bOk;
        }

        public bool mbCalcCurrentRange(ref string ArResult )
        {
            bool bOk = false;

            _mRunStartFileID = 0;
            _mRunEndFileID = 0;

            _mFirstPresentFileID = UInt32.MaxValue;        // find range of present FileID
            _mFirstPresentFileUTC = DateTime.MaxValue;
            _mLastPresentFileID = UInt32.MinValue;
            _mLastPresentFileUTC = DateTime.MinValue;

            _mFirstNormalFileID = UInt32.MaxValue;         // find range FileID of normal receceived (not requested )
            _mFirstNormalFileUTC = DateTime.MaxValue;
            _mLastNormalFileID = UInt32.MinValue;
            _mLastNormalFileUTC = DateTime.MinValue;

            _mFirstRecvFileID = UInt32.MaxValue;           // find received first / last
            _mFirstRecvFileUTC = DateTime.MaxValue;
            _mLastRecvFileID = UInt32.MinValue;
            _mLastRecvFileUTC = DateTime.MinValue;

            _mScanCalcResult = new List<string>();

            UInt32 nPresent = 0, nNormal = 0;

            if (_mScanFileIdList != null)
            {
                foreach (CTzCollectItem item in _mScanFileIdList)
                {
                    if (item._mbPresent)
                    {
                        ++nPresent;
                        if (item._mFileID < _mFirstPresentFileID) { _mFirstPresentFileID = item._mFileID; _mFirstPresentFileUTC = item._mFileUTC; }
                        if (item._mFileID > _mLastPresentFileID) { _mLastPresentFileID = item._mFileID; _mLastPresentFileUTC = item._mFileUTC; }

                        if (item._mTryRequestCounter == 0)
                        {
                            ++nNormal;
                            if (item._mFileID < _mFirstNormalFileID) { _mFirstNormalFileID = item._mFileID; _mFirstNormalFileUTC = item._mFileUTC; }
                            if (item._mFileID > _mLastNormalFileID) { _mLastNormalFileID = item._mFileID; _mLastNormalFileUTC = item._mFileUTC; }
                        }
                        if (item._mFileUTC < _mFirstRecvFileUTC) { _mFirstRecvFileUTC = item._mFileUTC; _mFirstRecvFileID = item._mFileID; }
                        if (item._mFileUTC > _mLastRecvFileUTC) { _mLastRecvFileUTC = item._mFileUTC; _mLastRecvFileID = item._mFileID; }
                    }
                }
                if( ArResult.Length > 0  ) ArResult += ", ";
                    ArResult += "last=" + _mLastPresentFileID.ToString() + " " + CProgram.sDateTimeToString(CProgram.sDateTimeToLocal(_mLastPresentFileUTC));
                if (_mScanCalcResult != null)
                {
                    _mScanCalcResult.Add("scan loaded " + _mScanNrFilesPresent.ToString() + ", added " + _mScanNrFilesAdded.ToString()
                        + " files, present = " + nPresent.ToString() + ", nNormal=  " + nNormal.ToString());
                    _mScanCalcResult.Add("firstPresentFileID=" + _mFirstPresentFileID.ToString() + " " + CProgram.sDateTimeToString(_mFirstPresentFileUTC)
                        + "UTC , LastPresentFileID=" + _mLastPresentFileID.ToString()+ " " + CProgram.sDateTimeToString(_mLastPresentFileUTC) + " UTC" );
                    _mScanCalcResult.Add("FirstNormalFileID=" + _mFirstNormalFileID.ToString() + " " + CProgram.sDateTimeToString(_mFirstNormalFileUTC)
                        + " UTC, LastNormalFileID=" + _mLastNormalFileID.ToString() + " " + CProgram.sDateTimeToString(_mLastNormalFileUTC) + "UTC ");
                    _mScanCalcResult.Add("FirstRecvFileID=" + _mFirstRecvFileID.ToString() + " " + CProgram.sDateTimeToString(_mFirstNormalFileUTC)
                        + " UTC, LastRecvFileID=" + _mLastRecvFileID.ToString() + " _mLastRecvFileUTC" + CProgram.sDateTimeToString(_mLastRecvFileUTC) + "UTC ");
                    //_mScanCalcResult.Add("=" + .ToString() + ", =" + .ToString());
                }
                bOk = true;
            }
            return bOk;
        }

        public bool mbCalcRunRangeStudy(ref string ArResult, out bool ArbRun, out string ArRunResult, bool AbRunAutomatic, UInt32 AStudyIX, DateTime AStartStudyUtc, DateTime AEndStudyUtc)
        {
            bool bOk = true;
            bool bRun = false;
            string scanResult = "";
            DateTime nowUTC = DateTime.UtcNow;

            _mRunStartFileID = 0;
            _mRunEndFileID = 0;
           
            if (AbRunAutomatic)
            {
                scanResult += "Automatic: ";
                if (false == _mbEnabled)
                {
                    if (ArResult.Length > 0) ArResult += ", ";
                    ArResult += "not enabled";
                    scanResult = "not enabled!";
                    bOk = false;
                }
                else if (nowUTC >= AEndStudyUtc)
                {
                    if (ArResult.Length > 0) ArResult += ", ";
                    ArResult += "End study";
                    scanResult += "end study " + CProgram.sDateTimeToString(CProgram.sDateTimeToLocal(AEndStudyUtc));
                    bOk = false;
                }
            }
            else
            {
                scanResult += "Manual: ";
            }
            if (bOk && AStudyIX == 0)
            {
                if (ArResult.Length > 0) ArResult += ", ";
                ArResult += "No Study";
                scanResult += "No Study! ";

                bOk = false;
            }
            if (bOk)
            {
                switch (_mStudyMode)
                {
                    case DTzCollectStudyMode.No:
                        if (ArResult.Length > 0) ArResult += ", ";
                        ArResult += "Study Mode off";
                        scanResult += "Study Mode off ";
                        bOk = false;
                        break;
                    case DTzCollectStudyMode.One:
                        if (_mStudy_IX == 0)
                        {
                            if (ArResult.Length > 0) ArResult += ", ";
                            ArResult += "Study Nr not set";
                            scanResult += "Study Nr not Set ";
                            bOk = false;
                        }
                        else if (_mStudy_IX != AStudyIX)
                        {
                            if (ArResult.Length > 0) ArResult += ", ";
                            ArResult += "Study Nr not equal!";
                            scanResult += "Study Nr not equal " + _mStudy_IX.ToString() + " != " + AStudyIX.ToString();
                            bOk = false;
                        }
                        else
                        {
                            if (ArResult.Length > 0) ArResult += ", ";
                            ArResult += "S" + AStudyIX.ToString() + " ";
                            scanResult += "S" + AStudyIX.ToString() + " ";
                            bRun = true;
                        }
                        break;
                    case DTzCollectStudyMode.All:
                        if (ArResult.Length > 0) ArResult += ", ";
                        ArResult += "*S" + AStudyIX.ToString();
                        scanResult += "S" + AStudyIX.ToString() + " ";

                        bRun = true;
                        break;
                }
            }
            ArbRun = bRun;
            ArRunResult = scanResult;
            return bOk;
        }

            public bool mbCalcRunRangeD6(ref string ArResult, out bool ArbRun, bool AbRunAutomatic, UInt32 AStudyIX, DateTime AStartStudyUtc, DateTime AEndStudyUtc)
        {
            bool bOk = true;
            bool bRun = false;
            string scanResult = _mName + " D6 ";
            DateTime nowUTC = DateTime.UtcNow;

            _mRunStartFileID = 0;
            _mRunEndFileID = 0;

            bOk = mbCalcRunRangeStudy(ref ArResult, out bRun, out scanResult, AbRunAutomatic, AStudyIX, AStartStudyUtc, AEndStudyUtc);
  
            if( bOk && bRun )
            {
                switch( _mStartMode)
                {
                    case DTzCollectStartMode.Start:
                        _mRunStartFileID = 1;
                        break;
                    case DTzCollectStartMode.First:
                        _mRunStartFileID = _mFirstPresentFileID;
                        if(_mFirstPresentFileUTC == DateTime.MaxValue)
                        {
                            if (ArResult.Length > 0) ArResult += ", ";
                            ArResult += "No First file";
                            scanResult += "No First file! ";
                            bOk = false;
                            bRun = false;
                        }
                        break;
                    case DTzCollectStartMode.Nr:
                        _mRunStartFileID = _mStartFileNr;
                        break;
                }
                switch( _mStopMode)
                {
                    case DTzCollectStopMode.End:
                        {
                            _mRunEndFileID = _mLastPresentFileID;   // still need to 
                            if (_mLastPresentFileUTC == DateTime.MinValue)
                            {
                                if (ArResult.Length > 0) ArResult += ", ";
                                ArResult += "No Last file";
                                scanResult += " No Last file";
                                bOk = false;
                                bRun = false;
                            }
                            else
                            {
                                if (_mFirstNormalFileUTC != DateTime.MaxValue)  // determin base {FileID, UTC}
                                {
                                    DateTime baseUTC = _mFirstNormalFileUTC;
                                    UInt32 baseFileID = _mFirstNormalFileID;
                                    UInt32 blockLengthSec = _mTzSettings.mGetSettingUInt32(DTzSettingTags.SCP_LENGTH);

                                    if (blockLengthSec > 0)
                                    {
                                        if (_mLastNormalFileUTC != DateTime.MinValue) // everage base {FileID, UTC}
                                        {
                                            UInt32 scpDif = _mLastNormalFileID - _mFirstNormalFileID;
                                            double secDif = (_mLastNormalFileUTC - _mFirstNormalFileUTC).TotalSeconds;

                                            baseUTC = _mFirstNormalFileUTC.AddSeconds(secDif * 0.5F);
                                            baseFileID = _mFirstNormalFileID + scpDif / 2;

                                            if (scpDif > 0)
                                            {
                                                double scpBlock = secDif / scpDif;
                                            }
                                        }
                                        DateTime targetUTC = DateTime.UtcNow.AddMinutes(-_sReqAgeMin);
                                        double secOffset = (targetUTC - baseUTC).TotalSeconds;  // calc estimate {FileID, UTC}
                                        UInt32 targetFileID = (UInt32)(baseFileID + secOffset / blockLengthSec + 0.5);

                                        if (targetFileID > _mRunEndFileID)
                                        {
                                            _mRunEndFileID = targetFileID;      // use estimate fileID
                                        }
                                        //                                            CProgram.sLogLine( "baseID="+ baseFileID.ToString() + " "+ CProgram.sDateTimeToYMDHMS(baseUTC)   + " block=" + blockLengthSec.toDString                                      
                                    }
                                }
                            }
                        }
                            break;
                    case DTzCollectStopMode.Last:
                        _mRunEndFileID = _mLastPresentFileID;
                        if (_mLastPresentFileUTC == DateTime.MinValue)
                        {
                            if (ArResult.Length > 0) ArResult += ", ";
                            ArResult += "No Last file";
                            scanResult += " No Last file";
                            bOk = false;
                            bRun = false;
                        }
                        break;
                        
                    case DTzCollectStopMode.Nr:
                        _mRunEndFileID = _mStopFileNr;
                        break;
                }
                if( bRun)
                {
                    scanResult += " run start= " + _mRunStartFileID.ToString() + " " + _mStartMode.ToString() 
                        + ", end= " + _mRunEndFileID.ToString() + " " + _mStopMode.ToString();
                }
            }
            if (_mScanCalcResult != null)
            {
                _mScanCalcResult.Add(scanResult);
            }

            ArbRun = bRun;
            return bOk;
        }


        public bool mbCalcRunRangeYYYYMMDDHH(ref string ArResult, out bool ArbRun, bool AbRunAutomatic, UInt32 AStudyIX, DateTime AStartStudyUtc, DateTime AEndStudyUtc)
        {
            bool bOk = true;
            bool bRun = false;
            string scanResult = "";
            DateTime nowUTC = DateTime.UtcNow;

            _mRunStartFileID = 0;
            _mRunEndFileID = 0;

            bOk = mbCalcRunRangeStudy(ref ArResult, out bRun, out scanResult, AbRunAutomatic, AStudyIX, AStartStudyUtc, AEndStudyUtc);

            if (bOk && bRun)
            {
                UInt32 startID = sMakeUInt(AStartStudyUtc);

                switch (_mStartMode)
                {
                    case DTzCollectStartMode.Start:
                        _mRunStartFileID = startID;
                        break;
                    case DTzCollectStartMode.First:
                        _mRunStartFileID = _mFirstPresentFileID;
                       if (_mFirstPresentFileUTC == DateTime.MaxValue)
                        {
                            if (ArResult.Length > 0) ArResult += ", ";
                            ArResult += "No First file";
                            scanResult += "No First file! ";
                            bOk = false;
                            bRun = false;
                        }
                        break;
                    case DTzCollectStartMode.Nr:
                        _mRunStartFileID = _mStartFileNr;
                        break;
                }
                if(_mRunStartFileID < startID )
                {
                    _mRunStartFileID = startID;
                }
                switch (_mStopMode)
                {
                    case DTzCollectStopMode.End:
                        {
                            DateTime utc = AEndStudyUtc;
                            if( AEndStudyUtc > nowUTC)
                            {
                                utc = nowUTC.AddMinutes(-_sReqAgeMin);
                            }
                            
                            _mRunEndFileID = sMakeUInt(utc);                             
                        }
                        break;
                    case DTzCollectStopMode.Last:
                        _mRunEndFileID = _mLastPresentFileID;
                        if (_mLastPresentFileUTC == DateTime.MinValue)
                        {
                            if (ArResult.Length > 0) ArResult += ", ";
                            ArResult += "No Last file";
                            scanResult += " No Last file";
                            bOk = false;
                            bRun = false;
                        }
                        break;

                    case DTzCollectStopMode.Nr:
                        _mRunEndFileID = _mStopFileNr;
                        break;
                }
                if (bRun)
                {
                    scanResult += " run start= " + _mRunStartFileID.ToString() + " " + _mStartMode.ToString()
                        + ", end= " + _mRunEndFileID.ToString() + " " + _mStopMode.ToString();
                }
            }
            if (_mScanCalcResult != null)
            {
                 _mScanCalcResult.Add(scanResult);
            }
            ArbRun = bRun;
            return bOk;
        }

        public bool mbRunRangeCalcMissing(ref string ArResult, ref bool ArbRun, out string ArSummery)
        {
            bool bOk = true;
            bool bRun = false;
            string scanResult = "";
            string summery = "";

            try
            {
                _mRunNTotal = 0;
                _mRunNPresent = 0;
                _mRunNMissing = 0;
                _mRunNFailed = 0;
                _mRunNWait = 0;
                _mRunNCollectNow = 0;
                _mRunCollect = new List<String>();
                _mRunMissing = new List<String>();

                if(_mRunCollect == null || _mRunMissing == null)
                {
                    ArResult += " error";
                    scanResult += " error";
                    bOk = false;
                    bRun = false;
                }
                else if (_mRunStartFileID == 0 && _mRunEndFileID == 0)
                {
                    if (ArResult.Length > 0) ArResult += ", ";
                    ArResult += "No range";
                    scanResult += " No Range";
                    bOk = false;
                    bRun = false;
                }
                else
                {
                    UInt32 fileID;
                    bool bNew, bMissing, bCollect;
                    DateTime utcNow = DateTime.UtcNow;
                    string state = "";
                    UInt32 maxMissing = _sActionMaxNrReq * 10;

                    for (fileID = _mRunStartFileID; fileID <= _mRunEndFileID; fileID = mGetNextValue(fileID))
                    {
                        CTzCollectItem foundItem = mFindCreateNew(out bNew, fileID, true);

                        if (foundItem == null)
                        {
                            CProgram.sLogError("Can't create Collect Item for " + fileID.ToString());
                            state = "-?";
                        }
                        else
                        {
                            bMissing = bCollect = false;
                            if (bNew)
                            {
                                bMissing = bCollect = true;
                                state = "_";
                            }
                            else if (foundItem._mbPresent)
                            {
                                ++_mRunNPresent;
                                state = "+";
                            }
                            else
                            {
                                bMissing = true;

                                if (foundItem._mTryRequestCounter < _sMaxTryRequest)   // maximum number of request for a file
                                {
                                    bCollect = foundItem._mTryRequestCounter == 0;

                                    if (false == bCollect)
                                    {
                                        double min = (utcNow - foundItem._mLastRequestUTC).TotalMinutes;

                                        if( min >= _sWaitTryRequestMin)// wait time before requesting the same file again
                                        {
                                            bCollect = true;
                                            state = "-@";
                                        }
                                        else
                                        {
                                            ++_mRunNWait;
                                            state = "-&";
                                        }
                                    }
                                    else
                                    {
                                        state = "-_";
                                    }
                                }
                                else
                                {
                                    ++_mRunNFailed;
                                    state = "-!";
                                }
                            }
                            if (bMissing)
                            {
                                ++_mRunNMissing;
                                _mRunMissing.Add(fileID.ToString());

                                if (bCollect)
                                {
                                    ++_mRunNCollectNow;
                                    
                                    /// ++foundItem._mTrRequestyCounter;
                                   // incr at write action foundItem._mLastRequestUTC = utcNow;
                                    _mRunCollect.Add(fileID.ToString());

                                    if(_mRunNCollectNow > maxMissing)
                                    {
                                        break; // prevent a overload of files, can not be collected at once anyway
                                    }
                                }
                            }
                            ++_mRunNTotal;
                        }
                        summery += state + mMakeShowString(fileID);
                    }
                   
                    scanResult += "collectNow=" + _mRunNCollectNow.ToString() + " wait=" + _mRunNWait.ToString()
                        + " failed=" + _mRunNFailed.ToString() + " missing=" + _mRunNMissing.ToString()
                        + " present =" + _mRunNPresent.ToString() + " N=" + _mRunNTotal.ToString();
                }
                if ( bOk )
                {
                    _mMissingCount = (UInt32)_mRunMissing.Count;
                    if(_mMissingCount == 0)
                    {
                        _mMissingList = null;
                    }
                    else
                    {
                        _mMissingList = new String[_mMissingCount];

                        int i = 0;
                        foreach( string s in _mRunMissing)
                        {
                            _mMissingList[i++] = s;
                        }
                    }
                    _mMissingFileDT = DateTime.MinValue;

                    ArResult += ", collect " + _mRunNCollectNow.ToString() + "("+ _mRunNWait.ToString() + ")/" + _mRunNMissing.ToString();

                    bRun = _mRunNCollectNow > 0;
                }

                if (_mScanCalcResult != null)
                {
                    _mScanCalcResult.Add((ArbRun ? "+" : "-")+scanResult);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("RunRangeCalcMissing " + scanResult, ex);
                ArResult += "!ex!";
                bOk = false;
            }
            ArbRun = bRun;
            ArSummery = summery;
            return bOk;
        }

        public bool mbLoadDeviceSettings(string ADevicePath, string ASerialNr)
        {
            bool bOk = false;

            string fileName = "";
            string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);

            try
            {
                if (_mTzSettings == null)
                {
                    _mTzSettings = new CTzDeviceSettings();
                }
                if (_mTzSettings != null)
                {
                    if (CTzDeviceSettings.sbMakeSettingFileName(out fileName, storeSnr))
                    {
                        fileName = Path.Combine(ADevicePath, fileName);

                        bOk = _mTzSettings.mbReadSettings(fileName, true, true, storeSnr); // needs a default to read file
                    }
                    _mTzSettings.serialNumber = ASerialNr;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read failed from " + fileName, ex);
                bOk = false;
            }           
            return bOk;
        }

        bool mbMakeActionFile(ref string ArResult, string ADevicePath, string ASerialNr, bool AbReqTrend)
        {
            bool bOk = _mTzSettings != null;
            string actionResult = "";
            string actionList = "Action "+ _mRunNCollectNow.ToString() + "reqested, blocks: ";
            Int32 nBlocks = 0;
            UInt16 nInBlock = 0;
            UInt32 fileID, lastFileID = 0, startIDInBlock = 0;
            UInt32 maxNrInBlock = (UInt32)(AbReqTrend ? 1 
                            : (_cbReqScpOneByOne ? 1 : _sActionMaxNrInBlock));
            bool bIsNext;
            bool bLimit = false;
            string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);

            _mbActionGenerated = false;
            _mActionSendUTC = DateTime.UtcNow;
            _mActionSeqNr = 0;
            _mActionReqNrFiles = 0;

            //_sActionMaxNrReq


            if (bOk)
            {
                bOk = CTzDeviceSettings.sbIncActionSeqNr(out _mActionSeqNr, ADevicePath, storeSnr);

                actionResult = bOk ? "Action #" + _mActionSeqNr.ToString() : "Action bad seqNr";
            }
            if (bOk)
            {
                string fileName = "";
                DateTime utcNow = DateTime.UtcNow;
                bool bNew;
                UInt32 index;
                Byte[] buffer = null;
                try
                {
                    bOk = false;

                    if (CTzDeviceSettings.sbMakeActionFileName(out fileName, storeSnr))
                    {
                        fileName = Path.Combine(ADevicePath, fileName);
                        buffer = _mTzSettings.mActionCreateBuffer(out index, storeSnr, (UInt16)_mActionSeqNr);

                        if (buffer != null)
                        {
                            //bOk &= _mSettings.mbActionRequestScpBlock(buffer, ref index, scpStart1, (UInt16)scpCount1);
                            //bOk &= _mSettings.mbActionRequestTzrFile(buffer, ref index, (UInt16)tzrYear, (UInt16)tzrMonth, (UInt16)tzrDay, (UInt16)tzrHour);

                            // change list of FileID to a action request 
                            bOk = true;

                            foreach( string s in _mRunCollect )
                            {
                                if( UInt32.TryParse( s, out fileID ))
                                {
                                    if(_mActionReqNrFiles  >= _sActionMaxNrReq)
                                    {
                                        bLimit = true;
                                        break;
                                    }
                                    if( nBlocks == 0 )
                                    {
                                        bIsNext = false;    // first
                                        nBlocks = 1;
                                        nInBlock = 0;
                                    }
                                    else if( nInBlock >= maxNrInBlock)
                                    {
                                        bIsNext = false;    // block is full
                                    }
                                    else
                                    {
                                        bIsNext =  mbIsNextValue(lastFileID, fileID);

                                    }
                                    if (nInBlock > 0 && false == bIsNext)   // add request bloock start FileID and number of files
                                    {
                                        actionList += startIDInBlock.ToString() + "n" + nInBlock.ToString() + "; ";
                                        if (AbReqTrend)
                                        {
                                            UInt16 year, month, day, hour;

                                            bOk &= sbSplitYMDH(out year, out month, out day, out hour, startIDInBlock);
                                            if (bOk)
                                            {
                                                bOk &= _mTzSettings.mbActionRequestTzrFile(buffer, ref index, year, month, day, hour);
                                            }
                                        }
                                        else
                                        {
                                            bOk &= _cbReqScpOneByOne ? _mTzSettings.mbActionRetransmitScpFile(buffer, ref index, startIDInBlock )
                                                : _mTzSettings.mbActionRequestScpBlock(buffer, ref index, startIDInBlock, (UInt16)nInBlock);
                                        }
                                        nInBlock = 0;
                                        ++nBlocks;

                                        if( nBlocks >= _sActionMaxNrOfBlocks)
                                        {
                                            bLimit = true;
                                            break;
                                        }
                                    }
                                   CTzCollectItem foundItem = mFindCreateNew(out bNew, fileID, true);

                                    if (foundItem == null)
                                    {
                                        CProgram.sLogError("Can't create Collect Item at write action for " + fileID.ToString());
                                    }
                                    else
                                    {
                                        if (nInBlock == 0)
                                        {
                                            startIDInBlock = fileID;
                                        }
                                        ++foundItem._mTryRequestCounter;
                                        foundItem._mLastRequestUTC = utcNow;
                                        foundItem._mLastActionID = _mActionSeqNr;

                                        ++nInBlock;
                                        lastFileID = fileID;
                                        ++_mActionReqNrFiles;
                                    }
                                }
                            }
                            if( nInBlock > 0) // add last request bloock start FileID and number of files
                            {
                                actionList += startIDInBlock.ToString() + "n" + nInBlock.ToString() + "; ";
                                if (AbReqTrend)
                                {
                                    UInt16 year, month, day, hour;

                                    bOk &= sbSplitYMDH(out year, out month, out day, out hour, startIDInBlock);
                                    if (bOk)
                                    {
                                        bOk &= _mTzSettings.mbActionRequestTzrFile(buffer, ref index, year, month, day, hour);
                                    }
                                }
                                else
                                {
                                    bOk &= _cbReqScpOneByOne ? _mTzSettings.mbActionRetransmitScpFile(buffer, ref index, startIDInBlock)
                                         : _mTzSettings.mbActionRequestScpBlock(buffer, ref index, startIDInBlock, (UInt16)nInBlock);
                                }
                                nInBlock = 0;
                            }
                            actionList += "= " + _mActionReqNrFiles.ToString() + "req. files";
                            if (bLimit)
                            {
                                actionList += " Limited";
                                actionResult += " Limit " + _mActionReqNrFiles.ToString() + " req. ";
                            }
                            else
                            {
                                actionResult += " All " + _mActionReqNrFiles.ToString() + " req. ";
                            }
                            if( _mScanCalcResult != null)
                            {
                                _mScanCalcResult.Add(actionList);
                            }

                            bool bCopyOk;
                            bOk = _mTzSettings.mbWriteAction(fileName, buffer, ref index, true, out bCopyOk, (UInt16)_mActionSeqNr);

                            if (bOk)
                            {
                                if (false == bCopyOk)
                                {
                                    actionResult += " copy ";
                                    bOk = false;
                                }
                                else
                                {
                                    actionResult += CTzDeviceSettings._sbUseQueue ? " writen to queue" : " writen";
                                    _mbActionGenerated = true;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Send Action", ex);
                }
            }
            if (false == bOk)
            {
                actionResult += " write failed!";
            }
            ArResult += ", " + actionResult;
            return bOk;

        }

        public bool mbSaveMissing(ref string ArResult, string ASerialNr, UInt32 AStudyIX)
        {
            bool bOk = false;
            string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);
            string fileName = mMakeMissingFileName(storeSnr, AStudyIX);
            string studyPath;

            try
            {
                if (AStudyIX != 0 && CDvtmsData.sbGetStudyRecorderDir(out studyPath, AStudyIX, true))
                { 
                    string filePath = Path.Combine(studyPath, fileName);

                    if( File.Exists(filePath))
                    {
                        File.Delete(filePath);
                    }

                    if( _mMissingList == null || _mMissingList.Length == 0)
                    {
                        bOk = true;
                        ArResult += ", Missing empty";
                    }
                    else
                    {
                        File.WriteAllLines(filePath, _mMissingList);
                        bOk = true;
                        ArResult += ", Missing writen";
                    }
                }
            }
            catch (Exception ex)
            {
                bOk = false;
                ArResult += ", Missing Failed";
                CProgram.sLogException("Write fMissing failed " + fileName, ex);
            }
            return bOk;
        }
  
        public bool mbRunCycle(out string ArResult, out bool AbEnabled, out bool AbRunDone, string ADevicePath, string ASerialNr, 
            UInt32 AStudyIX, DateTime AStudyStartUtc, DateTime AStudyEndUtc, bool AbRunAutomatic)
        {
            bool bOk = true;
            bool bRun = false;
            bool bEnabled = false;
            string result = "";
            bool bPromptUser = false == AbRunAutomatic;
            bool bPromptEdit = false == AbRunAutomatic;
            bool bCheckStudyEnded = AbRunAutomatic;
            bool bCheckLastScanTime = AbRunAutomatic;
            bool bCheckParamDoScan = AbRunAutomatic;
            bool bCheckActionEmpty = AbRunAutomatic;
            bool bStudyEnded = AStudyEndUtc <= DateTime.UtcNow;
            string runSummery = "";
            bool bUsesQueue = false;
            UInt32 nInQueue = 0;
            string strQueue = "";
            string serialNr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);


            try
            {
                if(serialNr == null || serialNr.Length == 0)
                {
                    if (result.Length > 0) result += ", ";
                    result += "Bad serial nr " + serialNr;
                    bOk = false;
                }
                if (AStudyIX == 0)
                {
                    if (result.Length > 0) result += ", ";
                    result += "NO study";
                    bOk = false;
                }
                if( bOk && bStudyEnded)
                {
                    result += "End Study";
                    if( bCheckStudyEnded)
                    {
                        bOk = false;
                    }
                }
                if(bOk && bCheckActionEmpty)
                {
                    // check action queue is empty
                    // Not done
                }
                // load parameters
                bool bLoad = _mParamSerialNr == null || _mParamSerialNr.Length == 0
                    || _mParamSerialNr != serialNr;

                if (bOk && bLoad)
                {
                    bool bPresent = false;

                    bOk = mbLoadParamFile(out bPresent, ADevicePath, serialNr);

                    if (result.Length > 0) result += ", ";
                    if (false == bPresent)
                    {
                        result += "missing params file";
                        bOk = false;
                    }
                    else
                    {
                        result += bOk ? "" /*"parms loaded"*/ : "load params failed";
                    }
                }
                if( bOk )
                {
                    bEnabled = true;
                    if (bCheckParamDoScan)
                    {
                        // check if scanning must be done
                        // 
                        if (false == _mbEnabled)
                        {
                            bEnabled = false;
                            result += " AC disabled";
                            bOk = false;
                            bRun = false;
                        }
                    }
                }
                // load old scan results
                if ( bOk )
                {
                    bool bPresent = false;
                    bool bWait = false;

                    bOk = mbLoadScanFile(out bPresent, out bWait, serialNr, AStudyIX, bCheckLastScanTime );

                    if (result.Length > 0) result += ", ";
                    if( bWait )
                    {
                        if( bPromptUser)
                        {
                            result += "Wait for next scan";
                        }
                        else
                        {
                            result = "skip wait time";    // skip scan due to scan cycle time is not passed
                            bOk = false;
                        }

                    }
                    else if (false == bPresent)
                    {
                        result += "new scan";
                        bOk = true;
                    }
                    else
                    {
                        result += bOk ? _mScanNrFilesPresent.ToString() + " prev files" : " load scan failed";
                        CProgram.sLogError("Collect scan file " + serialNr + " at S" + AStudyIX.ToString() + "result= " + result);
                        // continue if scan file is missing;
                        bOk = true;
                    }
                }
                // scan study for files
                FileInfo[] scanFileList = null;
                if ( bOk )
                {
                    string scanResult = "";

                    bOk = mbScanFilesStudyRecord(ref scanResult, out scanFileList, serialNr, AStudyIX);

                    if ( bOk )
                    {
                    }
                    else
                    {
                        if (result.Length > 0) result += ", ";
                        result += " list files failed "+ scanResult;
                        CProgram.sLogError("Collect scan study error " + serialNr + " at S" + AStudyIX.ToString() + "result= " + result);
                    }

                }
                // add files to list
                if (bOk && scanFileList != null)
                {
                    bOk = mbAddAllFileInfos(scanFileList, _sMinFileSize);

                    if (result.Length > 0) result += ", ";
                    if (bOk)
                    {
                        result += _mScanNrFilesAdded.ToString() + " files added";
                    }
                    else
                    {
                        result += " file info not added";
                    }
                }

                // find first & last 
                if( bOk )
                {
                    bRun = true;    // now trying to do a scan run
                    bOk = mbCalcCurrentRange(ref result);
                 
                }
                // load device settings: needed for scp nr calculation and creating an action;
                if (bOk)
                {
                    bOk = mbLoadDeviceSettings(ADevicePath, serialNr);
                    if( false == bOk )
                    {
                        result += " Settings file missing";
                        bRun = false;
                    }
                }

                // cals start and end and if a run must be done
                if (bOk && bRun)
                {
                    switch (_mFileFormat)
                    {
                        case DTzCollectFileFormat.D6:
                            bOk = mbCalcRunRangeD6(ref result, out bRun, AbRunAutomatic, AStudyIX, AStudyStartUtc, AStudyEndUtc);
                            break;
                        case DTzCollectFileFormat.YYYYMMDD_HH:
                            bOk = mbCalcRunRangeYYYYMMDDHH(ref result, out bRun, AbRunAutomatic, AStudyIX, AStudyStartUtc, AStudyEndUtc);
                            break;
                        default:
                            CProgram.sPromptError(false, "Auto collect " + _mName, "Unknown file format " + ((int)_mFileFormat).ToString());
                            bOk = false;
                            bRun = false;
                            break;
                    }

                }
                // run from start to end and calc missing
                if (bOk && bRun)
                {
                    bOk = mbRunRangeCalcMissing(ref result, ref bRun, out runSummery);
                }

                // check number of actions in queue
                if (bOk && bRun)
                {
                    strQueue = "No Queue";
                    bUsesQueue = CTzDeviceSettings._sbUseQueue;
                    if (bUsesQueue )
                    {
                        DateTime lastDT;
                        strQueue = "Failed Queue";
                        string storeSnr = CDvtmsData.sGetTzStoreSnrName(serialNr);

                        bOk = CTzDeviceSettings.sbCheckActionQueue(out nInQueue, out lastDT, ADevicePath, storeSnr);

                        if (bOk)
                        {
                            if (AbRunAutomatic)
                            {
                                bRun = nInQueue< _sMaxActionsInQueue;   // only run when nothing is in the queue
                            }
                            strQueue = "Action Queue = " + nInQueue.ToString() + (AbRunAutomatic ? " run" : " Skip" );
                        }
                    }
                    if (result.Length > 0) result += ", ";
                    result += strQueue;
                }

                // prompt scan result and missing
                if (bPromptUser)
                {
                    string text = result + "\r\n";

                    if (_mScanCalcResult != null)
                    {
                        foreach (string s in _mScanCalcResult)
                        {
                            text += s + "\r\n";
                        }
                    }
                    text += runSummery + "\r\n";
                    if (false == ReqTextForm.sbReqText("Auto Collect " + _mName + " " + serialNr + (bRun ? " Run " : " Skip ") + strQueue, "Scan calc result", ref text))
                    {
                        bOk = false;
                        bRun = false;
                        result += " CANCELED";
                    }
                }
                // prompt collect list and ask for edit
                if (bOk && bPromptEdit && _mRunCollect != null && _mRunNCollectNow > 0)
                {
                    string text = "";

                    if (_mRunCollect != null)
                    {
                        foreach (string s in _mRunCollect)
                        {
                            text += s + "\r\n";
                        }
                    }
                    if (false == ReqTextForm.sbReqText("Auto Collect " + _mName + " " + serialNr + (bRun ? " Run " : " Skip ") + strQueue, 
                        "Collect now " + _mRunNCollectNow.ToString() + " list", ref text))
                    {
                        bOk = false;
                        bRun = false;
                        result += " CANCELED";
                    }
                    else
                    {
                        string[] lines = text.Split();
                        int nLines = lines == null ? 0 : lines.Length;
                        UInt32 fId;

                        _mRunCollect.Clear();
                        _mRunNCollectNow = 0;
                        if ( nLines > 0)
                        {
                            for(int i = 0; i < nLines; ++i )
                            {
                                string s = lines[i].Trim();
                                if (UInt32.TryParse(s, out fId))
                                {
                                    _mRunCollect.Add(fId.ToString());
                                    ++_mRunNCollectNow;
                                }
                            }
                        }
                        if( bRun)
                        {
                            bRun = _mRunNCollectNow > 0;
                        }
                        string collect2 = ", Edited: collect " + _mRunNCollectNow.ToString();
                        result += collect2;
                    }
                }

                // make action if study is active and queue
                if (bOk && bRun )
                {
                    bool bTrend = _mFileFormat ==  DTzCollectFileFormat.YYYYMMDD_HH;
                    bOk = mbMakeActionFile(ref result, ADevicePath, serialNr, bTrend);
                }

                // save scan result
                if ( bOk )
                {
                    bOk = mbSaveScanFile(serialNr, AStudyIX, AbRunAutomatic, result);
                }

                // save missing files

                if (bOk)
                {
                    bOk = mbSaveMissing(ref result, serialNr, AStudyIX);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Collect test cycle " + serialNr + " result= "+ result, ex);
                result += "!ex!";
                bOk = false;
            }

            ArResult = result;
            AbRunDone = bRun;
            AbEnabled = bEnabled;
            return bOk;
        }

    }
}
