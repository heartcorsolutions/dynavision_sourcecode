﻿using Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventBoard.Event_Base
{

#if Old_TZ_Part
    public enum DTzCollectFileFormat
    {
        D6,
        YYYYMMDD_HH

    };

    public enum DTzCollectStudyMode
    {
        No,
        All,
        One
    };
    public enum DTzCollectStartMode
    {
        Start,
        First,
        Nr
    };
    public enum DTzCollectStopMode
    {
        End,
        Last,
        Nr
    };

    public class CSirCollectItem
    {
        public const string _cFID = "FID";
        public UInt32 _mFileID;
        public bool _mbPresent;
        public UInt16 _mTryRequestCounter;
        public DateTime _mLastRequestUTC;
        public UInt32 _mLastActionID;


        // vars for last scan
        public UInt16 _mFileCount;
        public string _mFileName;
        public DateTime _mFileUTC;
        public UInt32 _mFileSize;

        public bool _mbDoCollect;

        public CSirCollectItem()
        {
            _mFileID = 0;
            _mbPresent = false;
            _mTryRequestCounter = 0;
            _mLastRequestUTC = DateTime.MinValue;
            _mLastActionID = 0;
            _mFileCount = 0;
            _mFileName = "";
            _mFileUTC = DateTime.MinValue;
            _mbDoCollect = false;
        }
        public void mClear()
        {
            _mFileID = 0;
            _mbPresent = false;
            _mTryRequestCounter = 0;
            _mLastActionID = 0;
            _mLastRequestUTC = DateTime.MinValue;
            _mFileCount = 0;
            _mFileName = "";
            _mFileUTC = DateTime.MinValue;
            _mbDoCollect = false;
        }
        public void mResetBeforeScan()
        {
            _mFileCount = 0;
  //          _mFileName = "";
  //          _mFileUTC = DateTime.MinValue;
            _mbDoCollect = false;
            _mbPresent = false;
        }
        public bool mbReCalcPresent()
        {
            _mbPresent = _mFileCount > 0;
            return _mbPresent;
        }

        public bool mbMakeVarItem(out string ArVarName, out string ArVarValue)
        {
            string varName = "";
            string varValue = "";
            bool bOk = false;

            if (_mFileID > 0)
            {
                varName = _cFID + _mFileID.ToString("00000000");

                varValue = CItemKey.sMakeBoolTextString(_mbPresent)
                    + CItemKey._cCharSplit + _mTryRequestCounter.ToString()
                    + CItemKey._cCharSplit + CItemKey.sMakeDateTimeString(_mLastRequestUTC)
                    + CItemKey._cCharSplit + _mFileCount.ToString()
                    + CItemKey._cCharSplit + _mFileName
                    + CItemKey._cCharSplit + CItemKey.sMakeDateTimeString(_mFileUTC)
                    + CItemKey._cCharSplit + _mFileSize.ToString()
                    + CItemKey._cCharSplit + CItemKey.sMakeBoolTextString(_mbDoCollect)
                    + CItemKey._cCharSplit + _mLastActionID.ToString()
                    ;
                bOk = true;
            }

            ArVarName = varName;
            ArVarValue = varValue;
            return bOk;
        }

        public static CSirCollectItem sParseVarItem(string AVarName, string AVarValue)
        {
            CSirCollectItem item = null;

            if (AVarName != null && AVarValue != null && AVarName.Length > 0 && AVarValue.Length > 0)
            {
                string varName = AVarName.ToUpper().Trim();
                if (varName.StartsWith(_cFID))
                {
                    UInt32 fileID;
                    if (UInt32.TryParse(varName.Substring(3), out fileID)
                        && fileID > 0)
                    {
                        string[] varValues = AVarValue.Split(CItemKey._cCharSplit);
                        int nValues = varValues == null ? 0 : varValues.Length;
                        bool bParsed = false;
                        int iParse = 0;

                        if (nValues > 1)
                        {
                            item = new CSirCollectItem();

                            if (item != null)
                            {
                                item._mFileID = fileID;
                                bParsed = true;

                                if (iParse < nValues) bParsed &= CItemKey.sbParseBoolString(out item._mbPresent, varValues[iParse++]);
                                if (iParse < nValues) bParsed &= UInt16.TryParse(varValues[iParse++], out item._mTryRequestCounter);
                                if (iParse < nValues) bParsed &= CItemKey.sbParseDateTimeString(out item._mLastRequestUTC, varValues[iParse++]);
                                if (iParse < nValues) bParsed &= UInt16.TryParse(varValues[iParse++], out item._mFileCount);
                                if (iParse < nValues) item._mFileName = varValues[iParse++].Trim();
                                if (iParse < nValues) bParsed &= CItemKey.sbParseDateTimeString(out item._mFileUTC, varValues[iParse++]);
                                if (iParse < nValues) bParsed &= UInt32.TryParse(varValues[iParse++], out item._mFileSize);
                                if (iParse < nValues) bParsed &= CItemKey.sbParseBoolString(out item._mbDoCollect, varValues[iParse++]);
                                if (iParse < nValues) bParsed &= UInt32.TryParse(varValues[iParse++], out item._mLastActionID);
                            }
                        }
                    }
                }
            }
            return item;
        }
    }

#endif 
    public class CSirAutoCollect
    {
        public static UInt32 _sMinFileSize = 10;        // only add files that are a minimum size
        public static UInt32 _sReqAgeMin = 5;     // Only request for files that are at least x minutes old
        public static UInt32 _sScanTimeMin = 10;      // wait to do a scan after last scan
//        public static UInt32 _sMaxTryRequest = 5;       // maximum number of request for a file
        //public static UInt32 _sWaitTryRequestMin = 120; // wait time before requesting the same file again

        public static UInt32 _sActionMaxNrReq = 250;       // max nr File request in a action
        public static UInt32 _sMaxActionsInQueue = 1;   // do not add action if queue is full occupied 

        public static string _sParamExtention = ".AcParam";
        public static string _sScanExtention = ".AcScan";

        public static string _cParmGroup = "SirParam";
        public static string _cScanGroup = "SirScan";

        public const UInt32 _cFileSeed = 0x73862390;

        // definition type on constructor
        public string _mName;   // ECG or trend

        public bool _mbMergeHolter;

        // parameter variables -> param file device\Sirona\<deviceID>\<deviceID>_<name>.AcParam
        public bool _mbEnabled;
        public bool _mbGenEvent;

        // scan file variables
        public UInt32 _mStudy_IX;   // active study number

        public DateTime _mStartProcedureUTC;
        public Int32 _mDeviceTZoneMin;
        public UInt32 _mProcedureLengthSec;     // hole seconds    

        public bool _mbHolterStarted;
        public string _mHolterFileName;
        public DateTime _mHolterStartUTC;
        public UInt32 _mHolterStartOffsetSec;    // hole seconds
        public DateTime _mHolterEndUTC;
        public UInt32 _mHolterLengthSec;    // hole seconds

        public UInt32 _mNrBytesPerSample;   // needed for filling gap (taken from first read ecg recording
        public UInt32 _mSamplesPerSec;
        public UInt32 _mNrChannels; 
        public UInt32 _mMitFormat; //8, 16, 212 

        public string _mLastPartFile; // last ECG file used for holter
        public DateTime _mLastPartUTC;
        public DateTime _mLastPartStartUTC;    // hole seconds
        public UInt32 _mLastPartLengthSec;    // hole seconds

        public DateTime _mRequestEndUTC;
        public UInt32 _mRequestTotalSec;    // from proc time

        public DateTime _mLastReqUTC;       // when last request was created
        public DateTime _mLastReqStartUTC;  // start of request
        public UInt32 _mLastReqStartSec;    // from proc start time
        public UInt32 _mLastReqLengthSec;

         // param file load status
        public DateTime _mParamFileUTC;
        public bool _mbParamFileLoaded;
        public string _mParamSerialNr;

        // scan file load status
        public DateTime _mStatusFileUTC;
        public bool _mbStatusFileLoaded;
        public CSironaStateSettings _mSirLastStatus = null;
        public CSironaStateSettings _mSirNextSettings = null;

        // scan file scanned from Study Recorder => <deviceID>_<name>.AcScan
        public DateTime _mScanFileUTC;
        public bool _mbScanFileLoaded;

        // calc next
        public bool _mbNextNewProcedure;
        public DateTime _mNextRequestStartUTC;
        public UInt32 _mNextRequestStartSec;
        public UInt32 _mNextRequestLengthSec;

        public DateTime _mNextRequestEndUTC;
        public UInt32 _mNextRequestTotalSec;    // from proc time




        public CSirAutoCollect(string AName, bool AbMergeHolter)
        {
            _mName = AName;
            _mbMergeHolter = AbMergeHolter;


      // parameter variables -> param file device\Sirona\<deviceID>\<deviceID>_<name>.AcParam
        _mbEnabled = false;
        _mbGenEvent = false;

        // scan file variables
        _mStudy_IX = 0;   // active study number

        _mStartProcedureUTC = DateTime.MinValue;
        _mDeviceTZoneMin = 0;
        _mProcedureLengthSec = 0;

        _mbHolterStarted = false;
        _mHolterFileName = "";
        _mHolterStartUTC = DateTime.MinValue;
        _mHolterStartOffsetSec = 0;    // hole seconds

        _mNrBytesPerSample = 0;   // needed for filling gap (taken from first read ecg recording
        _mSamplesPerSec = 0;
        _mNrChannels = 0;
        _mMitFormat = 0; //8, 16, 212 

        _mLastPartFile = ""; // last ECG file for holter
        _mLastPartUTC = DateTime.MinValue;
        _mLastPartStartUTC = DateTime.MinValue;    // hole seconds
        _mLastPartLengthSec = 0;    // hole seconds
        _mHolterEndUTC = DateTime.MinValue;
        _mHolterLengthSec = 0;

        _mRequestEndUTC = DateTime.MinValue;
        _mRequestTotalSec = 0;

            _mLastReqUTC = DateTime.MinValue;
            _mLastReqStartUTC = DateTime.MinValue;
            _mLastReqStartSec = 0;
        _mLastReqLengthSec = 0;

        // param file load status
        _mParamFileUTC = DateTime.MinValue;
        _mbParamFileLoaded = false;
        _mParamSerialNr = "";

            mClearRun();

       }
    
        public void mClearRun()
        {
            // scan file load status
            _mStatusFileUTC = DateTime.MinValue;
            _mbStatusFileLoaded = false;

            _mSirLastStatus = null;
            _mSirNextSettings = null;

            // scan file scanned from Study Recorder => <deviceID>_<name>.AcScan
            _mScanFileUTC = DateTime.MinValue;
            _mbScanFileLoaded = false;

            _mSirLastStatus = null;
            _mSirNextSettings = null;

            _mScanFileUTC = DateTime.MinValue;
            _mbScanFileLoaded = false;

        // calc next
        _mbNextNewProcedure = false;
            _mNextRequestStartUTC = DateTime.MinValue;
        _mNextRequestStartSec = 0;
        _mNextRequestLengthSec = 0;

            _mNextRequestEndUTC = DateTime.MinValue;
        _mNextRequestTotalSec = 0;    // from proc time
    }




    public static UInt32 sMakeUInt(UInt16 AYear, UInt16 AMonth, UInt16 ADay, uint AHour)
        {
            // 2^31 = 2147483648 = 2147/48/36-48 max date 2147/12/31-23
            return (UInt32)(((AYear * 100 + AMonth) * 100 + ADay) * 100 + AHour);
        }
        public static UInt32 sMakeUInt(DateTime ADate)
        {
            // 2^31 = 2147483648 = 2147/48/36-48 max date 2147/12/31-23
            return (UInt32)(((ADate.Year * 100 + ADate.Month) * 100 + ADate.Day) * 100 + ADate.Hour);
        }
        public static string sMakeShowYMDH(UInt32 AValueYMDH)
        {
            UInt32 i = AValueYMDH;
            UInt32 hour = i % 100;
            i = i / 100;
            UInt32 day = i % 100;
            i = i / 100;
            UInt32 month = i % 100;
            UInt32 year = i / 100;

            return year.ToString("0000") + "/" + month.ToString("00") + "/" + day.ToString("00") + "-" + hour.ToString("00");
        }
        public static string mMakeFileYMDH(UInt32 AValueYMDH)
        {
            UInt32 i = AValueYMDH;
            UInt32 hour = i % 100;
            i = i / 100;

            return i.ToString("00000000") + "_" + hour.ToString("00");
        }
        public static bool sbMakeDate(out DateTime ArDateTime, UInt32 AValueYMDH)
        {
            bool bOk = false;
            DateTime dt = DateTime.MinValue;

            try
            {
                UInt32 i = AValueYMDH;
                UInt32 hour = i % 100;
                i = i / 100;
                UInt32 day = i % 100;
                i = i / 100;
                UInt32 month = i % 100;
                UInt32 year = i / 100;

                dt = new DateTime((int)year, (int)month, (int)day, (int)hour, 0, 0);
                bOk = true;
            }
            catch (Exception)
            {

            }
            ArDateTime = dt;
            return bOk;
        }
        public static bool sbSplitYMDH(out UInt16 ArYear, out UInt16 ArMonth, out UInt16 ArDay, out UInt16 ArHour, UInt32 AValueYMDH)
        {
            bool bOk = false;
            UInt16 year = 0;
            UInt16 month = 0;
            UInt16 day = 0;
            UInt16 hour = 0;

            try
            {
                UInt32 i = AValueYMDH;
                hour = (UInt16)(i % 100);
                i = i / 100;
                day = (UInt16)(i % 100);
                i = i / 100;
                month = (UInt16)(i % 100);
                year = (UInt16)(i / 100);

                DateTime dt = new DateTime((int)year, (int)month, (int)day, (int)hour, 0, 0);   // check date is ok
                bOk = true;
            }
            catch (Exception)
            {

            }
            ArYear = year;
            ArMonth = month;
            ArDay = day;
            ArHour = hour;
            return bOk;
        }
/*        public bool mbIsNextValue(UInt32 AFirstValue, UInt32 ANextValue)
        {
            bool bIsNext = false;

            if (_mFileFormat == DTzCollectFileFormat.YYYYMMDD_HH)
            {
                DateTime firstDT;
                if (sbMakeDate(out firstDT, AFirstValue))
                {
                    DateTime nextDT = firstDT.AddHours(1);  // calc next hour
                    UInt32 nextYMDH = sMakeUInt(nextDT);
                    bIsNext = nextYMDH == ANextValue;
                }
            }
            else
            {
                bIsNext = AFirstValue + 1 == ANextValue;
            }

            return bIsNext;
        }
        public UInt32 mGetNextValue(UInt32 AValue)
        {
            UInt32 nextValue = UInt32.MaxValue;

            if (_mFileFormat == DTzCollectFileFormat.YYYYMMDD_HH)
            {
                DateTime firstDT;
                if (sbMakeDate(out firstDT, AValue))
                {
                    DateTime nextDT = firstDT.AddHours(1);  // calc next hour
                    nextValue = sMakeUInt(nextDT);
                }
                
            }
            else
            {
                nextValue = AValue + 1;
            }

            return nextValue;
        }
*/
        public string mMakeParamFileName(string ASerialNr)
        {
            return ASerialNr + "_Sir_" + _mName + _sParamExtention;
        }
        public string mMakeScanFileName(string ASerialNr, UInt32 AStudyIX)
        {
            string ymdhms = _mStartProcedureUTC == DateTime.MinValue ? "00000000000000" : CProgram.sDateTimeToYMDHMS(_mStartProcedureUTC);
            return ASerialNr + "_Sir_" + _mName + "_S" + AStudyIX.ToString() + /*"_" + ymdhms+*/ _sScanExtention;
        }
        public string mMakeHolterFileName(string ASerialNr, UInt32 AStudyIX, string AExtention)
        {
            string ymdhms = _mStartProcedureUTC == DateTime.MinValue ? "00000000000000" : CProgram.sDateTimeToYMDHMS(_mStartProcedureUTC);
            return "Holter_"+ ASerialNr + "_Sir_" + _mName + "_S" + AStudyIX.ToString() + "_" + ymdhms + AExtention ;
        }
        /*        public string mMakeMissingFileName(string ASerialNr, UInt32 AStudyIX)
                        {
                            return ASerialNr + "_TzAutoCollect" + _mName + "_S" + AStudyIX.ToString() + ".missing";
                        }

                        public bool mbParseFileID(out UInt32 ArFileID, string AFileName)
                        {
                            bool bID = false;
                            UInt32 fileID  = 0;
                            int len;

                            if( AFileName != null && (len=AFileName.Length) > 3)
                            {
                                string fileName = AFileName.ToLower();
                                string snr = _mParamSerialNr.ToLower() + "_";

                                if( fileName.EndsWith( _mFileExtention))
                                {
                                    if( fileName.StartsWith( snr))
                                    {
                                        string s = fileName.Substring(snr.Length, len - snr.Length - _mFileExtention.Length);
                                        len = s.Length;                       

                                        switch(_mFileFormat)
                                        {
                                            case DTzCollectFileFormat.D6:
                                                if( len >= 6 )
                                                {
                                                    bID = UInt32.TryParse(s.Substring(0,6), out fileID);
                                                    if( bID )
                                                    {
                                                        bID = fileID > 0;
                                                    }
                                                }

                                                break;
                                            case DTzCollectFileFormat.YYYYMMDD_HH:
                                                if( len >= 11)
                                                {
                                                    int ymd = -1, h = -1;

                                                    bID = int.TryParse(s.Substring(0, 8), out ymd) && int.TryParse(s.Substring(9, 2), out h);
                                                    if( bID )
                                                    {
                                                        bID = ymd > 20000000 && ymd < 21470000 && h >= 0 && h < 24;
                                                        fileID = (UInt32)(ymd * 100 + h);
                                                    }
                                                }
                                                break;

                                        }
                                    }
                                }
                            }


                            ArFileID = fileID;
                            return bID;
                        }
                */
        public string mParamFileInfoLoadNew(string ADevicePath, string ASerialNr, bool AbLoadNew)
        {
            string info = "";
            string fileName = mMakeParamFileName(ASerialNr);
            string filePath = Path.Combine(ADevicePath, fileName);

            try
            {
                FileInfo fi = new FileInfo(filePath);

                if (false == fi.Exists)
                {
                    info = "not present";
                    _mbParamFileLoaded = false;
                    _mParamSerialNr = "";
                }
                else
                {
                    info = CProgram.sDateTimeToString(fi.LastWriteTime);

                    if (AbLoadNew)
                    {
                        if (false == _mbParamFileLoaded || fi.LastWriteTimeUtc > _mParamFileUTC)
                        {
                            bool bPresent = false;
                            bool bOk = mbLoadParamFile(out bPresent, ADevicePath, ASerialNr);

                            info += bOk ? " load ok" : (bPresent ? " load failed!" : "missing file" );
                            _mParamSerialNr = bOk ? ASerialNr : "";
                        }
                    }
                }
            }
            catch (Exception)
            {
                info = "error on find " + fileName;
            }
            return info;
        }

        public string mScanInfoLoadMissing(string ADevicePath, string ASerialNr, UInt32 AStudyIX, bool AbLoadMissing, DateTime AStatusFileDT)
        {
            string info = "";
            string fileName = mMakeScanFileName(ASerialNr, AStudyIX);
            string studyPath;

            try
            {
                if (AStudyIX == 0)
                {
                    info = "S0 no study";
                }
                else
                {
                    info = "S" + AStudyIX + ": ";

                    if (false == CDvtmsData.sbGetStudyRecorderDir(out studyPath, AStudyIX, true))
                    {
                        info += "no recordings";
                    }
                    else
                    {
                        string filePath = Path.Combine(studyPath, fileName);
                        FileInfo fi = new FileInfo(filePath);

                        if (false == fi.Exists)
                        {
                            info += "not scanned";
                        }
                        else
                        {
                            info += CProgram.sDateTimeToString(fi.LastWriteTime);

                            if (_mProcedureLengthSec == 0 || AbLoadMissing && _mScanFileUTC != fi.LastWriteTimeUtc)
                            {
                                bool bPresent;
                                Int32 waitSec;

                                if (false == mbLoadScanFile(out bPresent, out waitSec, ASerialNr, AStudyIX, false))
                                {
                                    info += " ? ";
                                }
                                info += bPresent ? " = " : " - ";
                                if (waitSec > 0) info += "w" + CProgram.sPrintTimeSpan_dhmSec(waitSec, ""); 
                            }
                            if (_mProcedureLengthSec == 0)
                            {
                                info += " Proc 0";
                            }
                            else
                            {
                                DateTime startProcUTC = _mStartProcedureUTC;
                                DateTime endProcUTC = startProcUTC.AddSeconds(_mProcedureLengthSec);

                                info += " Proc " + CProgram.sPrintTimeSpan_dhmSec(_mProcedureLengthSec, "") + "\r\n";

                                UInt32 holterSec = CSironaStateSettings.sGetHolterReqLengthSec();

                                if (holterSec == 0)
                                {
                                    info += " No Holter";
                                }
                                else if (_mRequestEndUTC < startProcUTC)
                                {
                                    info += " -R";
                                }
                                else
                                {
                                    UInt32 reqMissingSec = (UInt32)((endProcUTC - _mRequestEndUTC).TotalSeconds);
                                    UInt32 nrReq = reqMissingSec / holterSec;

                                    DateTime reqEndDT = _mRequestEndUTC.AddMinutes(_mDeviceTZoneMin);

                                    info += " Req=" + (_mRequestTotalSec == 0 ? "0" : CProgram.sDateTimeToString(reqEndDT));
//                                    info += " Req=" + (_mRequestTotalSec == 0 ? "0" : CProgram.sPrintTimeSpan_dhmSec(_mRequestTotalSec, ""));
                                    if (reqMissingSec >= holterSec)
                                    {
                                        info += "Δ" + CProgram.sPrintTimeSpan_dhmSec(reqMissingSec, "")
                                                                          + "->" + nrReq.ToString() + " req " + CProgram.sPrintTimeSpan_dhmSec(holterSec, "");
                                    }
                                    else
                                    {
                                        info += "...";
                                    }

                                    if (_mHolterEndUTC < startProcUTC)
                                    {
                                        info += " -H";
                                    }
                                    else
                                    {
                                        UInt32 holterMissingSec = (UInt32)((_mRequestEndUTC - _mHolterEndUTC).TotalSeconds + 0.5);

                                        info += " Holter=" + (_mHolterLengthSec == 0 ? "0" : CProgram.sPrintTimeSpan_dhmSec(_mHolterLengthSec, ""));
                                        if (holterMissingSec >= holterSec) info += "Δ" + CProgram.sPrintTimeSpan_dhmSec(holterMissingSec, "");
                                    }
                                }
                            }
                            if (AStatusFileDT != DateTime.MinValue)
                            {

                                Int32 status = (Int32)(DateTime.Now - AStatusFileDT).TotalSeconds;
//                                info += " status<" + CProgram.sPrintTimeSpan_dhmSec(status, "");
                            }

                        }
                    }
                }
            }
            catch (Exception)
            {
                info += ", error on find " + fileName;
            }
            return info;
        }

        public bool mbSaveParamFile(string ADevicePath, string ASerialNr)
        {
            bool bOk = false;

            string fileName = mMakeParamFileName(ASerialNr);
            string toFile = Path.Combine(ADevicePath, fileName);

            try
            {
                if (ASerialNr == null || ASerialNr.Length == 0)
                {
                    CProgram.sLogError("Collect save param No Serial");
                }
                else
                {
                    if (_mParamSerialNr == null || _mParamSerialNr.Length == 0)
                    {
                        _mParamSerialNr = ASerialNr;
                    }
                    if (_mParamSerialNr != ASerialNr)
                    {
                        CProgram.sLogError("Collect save param Serial " + _mParamSerialNr + "!=" + _mParamSerialNr);
                    }
                    else
                    {
                        CItemKey ik = new CItemKey(_cParmGroup + _mName);

                        if (ik != null)
                        {
                            ik.mbAddItemBoolText("Enabled", _mbEnabled);
                            ik.mbAddItemBoolText("GenEvent", _mbGenEvent);
                            

                            // info not read                           
                            ik.mbAddItemString("U_PC", CProgram.sGetUserAtPcString());
                            ik.mbAddItemDateTimeZone("saveUtcZ", DateTime.UtcNow, CProgram.sGetLocalTimeZoneOffsetMin());
                            ik.mbAddItemString("Name", _mName);  // just for info, is not read
                            ik.mbAddItemString("SerialNr", _mParamSerialNr);  // just for info, is not read
                            ik.mbAddItemBoolText("MergeHolter", _mbMergeHolter);

                            ik.mbAddItemUInt32("StudyIX", _mStudy_IX);

                            bOk = ik.mbWriteToFile(toFile, _cFileSeed, CProgram.sGetOrganisationLabel(), true, 0);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Collect save " + toFile, ex);
            }

            return bOk;
        }
        public bool mbLoadParamFile(out bool ArbPresent, string ADevicePath, string ASerialNr)
        {
            bool bOk = false;
            bool bPresent = false;
            string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);

            string fileName = mMakeParamFileName(storeSnr);
            string fromFile = Path.Combine(ADevicePath, fileName);


            try
            {
                _mbParamFileLoaded = false;
                FileInfo fi = new FileInfo(fromFile);

                if (fi == null)
                {
                    _mParamFileUTC = DateTime.MinValue;
                }
                else
                {
                    _mParamFileUTC = fi.LastWriteTimeUtc;
                    bPresent = fi.Exists;

                    CItemKey ik = new CItemKey(_cParmGroup + _mName);

                    if (ik != null && bPresent)
                    {
                        bool bChecksum;

                        bOk = ik.mbReadFromFile(out bChecksum, fromFile, _cFileSeed, CProgram.sGetOrganisationLabel(), false);

                        if (bOk)
                        {
                            bOk &= ik.mbGetItemBool(ref _mbEnabled, "Enabled");
                            bOk &= ik.mbGetItemBool(ref _mbGenEvent, "GenEvent");
                        }
                        if (bOk)
                        {
                            _mbParamFileLoaded = true;
                            _mParamSerialNr = ASerialNr;
                        }
                    }
                }
                bOk = true;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Collect load " + fromFile, ex);
            }
            ArbPresent = bPresent;
            return bOk;
        }

/*        public bool mbScanFilesStudyRecord(ref string ArResult, out FileInfo[] ArFileList, string ASerialNr, UInt32 AStudyIX)
        {
            bool bOk = false;
            FileInfo[] fileList = null;
            string studyPath;

            try
            {
                if (ArResult.Length > 0) ArResult += ", ";

                if (AStudyIX == 0)
                {
                    ArResult += "S0 no study";
                }
                else
                {
                    ArResult = "S" + AStudyIX + ": ";

                    if (false == CDvtmsData.sbGetStudyRecorderDir(out studyPath, AStudyIX, true))
                    {
                        ArResult += "no recordings";
                    }
                    else
                    {
                        DirectoryInfo di = new DirectoryInfo(studyPath);

                        if (di != null)
                        {
                            fileList = di.GetFiles();
                            int n = fileList == null ? 0 : fileList.Length;

                            ArResult += n.ToString() + " files";
                            bOk = n > 0;
                        }
                        else
                        {
                            ArResult += " failed scan files";
                        }
                    }
                }
            }
            catch (Exception)
            {
                ArResult += ", error on scan files S" + AStudyIX.ToString();
            }
            ArFileList = fileList;
            return bOk;
        }
*/
        public bool mbSaveScanFile(string ASerialNr, UInt32 AStudyIX, bool AbRunAutomatic, string AResult)
        {
            bool bOk = false;
            string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);


            string fileName = mMakeScanFileName(storeSnr, AStudyIX);
            string toFile = "";

            string studyPath;

            try
            {
                if (AStudyIX > 0 && CDvtmsData.sbCreateStudyRecorderDir(out studyPath, AStudyIX))
                {
                    _mStudy_IX = AStudyIX;
                    toFile = Path.Combine(studyPath, fileName);

                    CItemKey ik = new CItemKey(_cScanGroup + _mName);

                    if (ik != null)
                    {
                        // add the settings used, only for info
                        ik.mbAddItemString("Name", _mName);  // just for info, is not read
                        ik.mbAddItemDateTime("ParamFileUTC", _mParamFileUTC);
                        ik.mbAddItemString("U_PC", CProgram.sGetUserAtPcString());
                        ik.mbAddItemString("U_Prog", CProgram.sMakeUniqueProgName());
                        ik.mbAddItemDateTimeZone("saveUtcZ", DateTime.UtcNow, CProgram.sGetLocalTimeZoneOffsetMin());

                        ik.mbAddItemBoolText("Enabled", _mbEnabled);
                        ik.mbAddItemBoolText("GenEvent", _mbGenEvent);


                        ik.mbAddItemUInt32("StudyIX", _mStudy_IX);
                        ik.mbAddItemString("SerialNR", ASerialNr);


                        // scan items
                        // save scan settings

                        ik.mbAddItemUInt32("MinFileSize", _sMinFileSize);
                        ik.mbAddItemUInt32("AskBeforeNowMin", _sReqAgeMin);
                        ik.mbAddItemUInt32("ScanTimeMin", _sScanTimeMin);

                        ik.mbAddItemDateTime("StartProcedureUTC", _mStartProcedureUTC);
                        ik.mbAddItemInt32("DeviceTZoneMin", _mDeviceTZoneMin);
                        ik.mbAddItemUInt32("ProcedureLengthSec", _mProcedureLengthSec);


                        ik.mbAddItemBoolText("HolterStarted", _mbHolterStarted);
                        ik.mbAddItemString("HolterFileName", _mHolterFileName);
                        ik.mbAddItemDateTime("HolterStartUTC", _mHolterStartUTC);
                        ik.mbAddItemUInt32("HolterStartOffsetSec", _mHolterStartOffsetSec);

                        ik.mbAddItemDateTime("HolterEndUTC", _mHolterEndUTC);
                        ik.mbAddItemUInt32("HolterLengthSec", _mHolterLengthSec);

                        ik.mbAddItemUInt32("NrBytesPerSample", _mNrBytesPerSample);
                        ik.mbAddItemUInt32("SamplesPerSec", _mSamplesPerSec);
                        ik.mbAddItemUInt32("NrChannels", _mNrChannels);
                        ik.mbAddItemUInt32("_mMitFormat", _mMitFormat);

                        ik.mbAddItemString("LastPartFile", _mLastPartFile);
                        ik.mbAddItemDateTime("LastPartUTC", _mLastPartUTC);
                        ik.mbAddItemDateTime("LastPartStartUTC", _mLastPartStartUTC);
                        ik.mbAddItemUInt32("LastPartLengthSec", _mLastPartLengthSec);

                        ik.mbAddItemDateTime("RequestEndUTC", _mRequestEndUTC);
                        ik.mbAddItemUInt32("RequestTotalSec", _mRequestTotalSec);
                        ik.mbAddItemDateTime("LastReqUTC", _mLastReqUTC);
                        ik.mbAddItemDateTime("LastReqStartUTC", _mLastReqStartUTC);
                        ik.mbAddItemUInt32("LastReqStartSec", _mLastReqStartSec);
                        ik.mbAddItemUInt32("LastReqLengthSec", _mLastReqLengthSec);

                        ik.mbAddItemDateTime("LastStatusUTC", _mStatusFileUTC);
  
                        bOk = ik.mbWriteToFile(toFile, _cFileSeed, CProgram.sGetOrganisationLabel(), true, 0);
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Collect scan save " + toFile, ex);
            }

            return bOk;
        }
        public bool mbLoadScanFile(out bool ArbPresent, out Int32 ArWaitSec, string ASerialNr, UInt32 AStudyIX, bool AbCheckLastScanTime)
        {
            bool bOk = false;
            bool bPresent = false;
            Int32 waitSec = 0;
            string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);

            string fileName = mMakeScanFileName(storeSnr, AStudyIX);
            string fromFile = "";

            string studyPath;
            _mbScanFileLoaded = false;

            try
            {
                if (AStudyIX > 0 && CDvtmsData.sbGetStudyRecorderDir(out studyPath, AStudyIX, true))
                {
                    fromFile = Path.Combine(studyPath, fileName);

                    FileInfo fi = new FileInfo(fromFile);

                    bPresent = fi != null && fi.Exists;

                    if (bPresent)
                    {
                        _mScanFileUTC = fi.LastWriteTimeUtc;
                        if (AbCheckLastScanTime)
                        {
                            double sec = (DateTime.UtcNow - fi.LastWriteTimeUtc).TotalSeconds;
                            waitSec = (int) ( _sScanTimeMin * 60 - sec );
                        }
                    }

                    if (waitSec <= 0 && bPresent)
                    {
                        CItemKey ik = new CItemKey(_cScanGroup + _mName);

                        if (ik != null )
                        {
                            bool bChecksum;

                            _mScanFileUTC = fi.LastWriteTimeUtc;

                            bOk = ik.mbReadFromFile(out bChecksum, fromFile, _cFileSeed, CProgram.sGetOrganisationLabel(), false);
                            _mbScanFileLoaded = bOk;
                            if (bOk)
                            {
                                bOk &= ik.mbGetItemDateTime(ref _mStartProcedureUTC, "StartProcedureUTC");
                                bOk &= ik.mbGetItemInt32(ref _mDeviceTZoneMin, "DeviceTZoneMin");
                                bOk &= ik.mbGetItemUInt32(ref _mProcedureLengthSec, "ProcedureLengthSec");

                                bOk &= ik.mbGetItemBool(ref _mbHolterStarted, "HolterStarted");
                                bOk &= ik.mbGetItemString(ref _mHolterFileName, "HolterFileName");
                                bOk &= ik.mbGetItemDateTime(ref _mHolterStartUTC, "HolterStartUTC");
                                bOk &= ik.mbGetItemUInt32(ref _mHolterStartOffsetSec, "HolterStartOffsetSec");
                                bOk &= ik.mbGetItemDateTime(ref _mHolterEndUTC, "HolterEndUTC");
                                bOk &= ik.mbGetItemUInt32(ref _mHolterLengthSec, "HolterLengthSec");

                                bOk &= ik.mbGetItemUInt32(ref _mNrBytesPerSample, "NrBytesPerSample");
                                bOk &= ik.mbGetItemUInt32(ref _mSamplesPerSec, "SamplesPerSec");
                                bOk &= ik.mbGetItemUInt32(ref _mNrChannels, "NrChannels");
                                bOk &= ik.mbGetItemUInt32(ref _mMitFormat, "MitFormat");

                                bOk &= ik.mbGetItemString(ref _mLastPartFile, "LastPartFile");
                                bOk &= ik.mbGetItemDateTime(ref _mLastPartUTC, "LastPartUTC");
                                bOk &= ik.mbGetItemDateTime(ref _mLastPartStartUTC, "LastPartStartUTC");
                                bOk &= ik.mbGetItemUInt32(ref _mLastPartLengthSec, "LastPartLengthSec");

                                bOk &= ik.mbGetItemDateTime(ref _mRequestEndUTC, "RequestEndUTC");
                                bOk &= ik.mbGetItemUInt32(ref _mRequestTotalSec, "RequestTotalSec");
                                bOk &= ik.mbGetItemDateTime(ref _mLastReqUTC, "LastReqUTC");
                                bOk &= ik.mbGetItemDateTime(ref _mLastReqStartUTC, "LastReqStartUTC");
                                bOk &= ik.mbGetItemUInt32(ref _mLastReqStartSec, "LastReqStartSec");
                                bOk &= ik.mbGetItemUInt32(ref _mLastReqLengthSec, "LastReqLengthSec");
                            }
                        }
                    }
                    bOk = true;
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Collect scan load " + fromFile, ex);
            }
            ArbPresent = bPresent;
            ArWaitSec = waitSec;
            return bOk;
        }

        public bool mbLoadDeviceSettings(string ADevicePath, string ASerialNr)
        {
            bool bOk = false;

            string fileName = "";

            try
            {
                if (_mSirNextSettings == null)
                {
                    _mSirNextSettings = new CSironaStateSettings();
                }
                if (_mSirNextSettings != null)
                {
                    if (CSironaStateSettings.sbMakeSettingsFileName(out fileName, ASerialNr))
                    {
                        string filePath = Path.Combine(ADevicePath, fileName);

                        if( false == File.Exists(fileName))
                        {
                            CSironaStateSettings.sbMakeSettingsFileName(out fileName, CSironaStateSettings._cDefaultsName);

                            filePath = Path.Combine(ADevicePath, "..", fileName);
                        }

                        bOk = _mSirNextSettings.mbReadFile(filePath); // needs a default to read file
                    }
                    _mSirNextSettings._mDeviceSerial = ASerialNr;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read failed from " + fileName, ex);
                bOk = false;
            }           
            return bOk;
        }
        /*
                bool mbMakeActionFile(ref string ArResult, string ADevicePath, string ASerialNr, bool AbReqTrend)
                {
                    bool bOk = _mTzSettings != null;
                    string actionResult = "";
                    string actionList = "Action "+ _mRunNCollectNow.ToString() + "reqested, blocks: ";
                    Int32 nBlocks = 0;
                    UInt16 nInBlock = 0;
                    UInt32 fileID, lastFileID = 0, startIDInBlock = 0;
                    UInt32 maxNrInBlock = (UInt32)(AbReqTrend ? 1 
                                    : (_cbReqScpOneByOne ? 1 : _sActionMaxNrInBlock));
                    bool bIsNext;
                    bool bLimit = false;
                    string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);

                    _mbActionGenerated = false;
                    _mActionSendUTC = DateTime.UtcNow;
                    _mActionSeqNr = 0;
                    _mActionReqNrFiles = 0;

                    //_sActionMaxNrReq


                    if (bOk)
                    {
                        bOk = CSirDeviceSettings.sbIncActionSeqNr(out _mActionSeqNr, ADevicePath, storeSnr);

                        actionResult = bOk ? "Action #" + _mActionSeqNr.ToString() : "Action bad seqNr";
                    }
                    if (bOk)
                    {
                        string fileName = "";
                        DateTime utcNow = DateTime.UtcNow;
                        bool bNew;
                        UInt32 index;
                        Byte[] buffer = null;
                        try
                        {
                            bOk = false;

                            if (CSirDeviceSettings.sbMakeActionFileName(out fileName, storeSnr))
                            {
                                fileName = Path.Combine(ADevicePath, fileName);
                                buffer = _mTzSettings.mActionCreateBuffer(out index, storeSnr, (UInt16)_mActionSeqNr);

                                if (buffer != null)
                                {
                                    //bOk &= _mSettings.mbActionRequestScpBlock(buffer, ref index, scpStart1, (UInt16)scpCount1);
                                    //bOk &= _mSettings.mbActionRequestTzrFile(buffer, ref index, (UInt16)tzrYear, (UInt16)tzrMonth, (UInt16)tzrDay, (UInt16)tzrHour);

                                    // change list of FileID to a action request 
                                    bOk = true;

                                    foreach( string s in _mRunCollect )
                                    {
                                        if( UInt32.TryParse( s, out fileID ))
                                        {
                                            if(_mActionReqNrFiles  >= _sActionMaxNrReq)
                                            {
                                                bLimit = true;
                                                break;
                                            }
                                            if( nBlocks == 0 )
                                            {
                                                bIsNext = false;    // first
                                                nBlocks = 1;
                                                nInBlock = 0;
                                            }
                                            else if( nInBlock >= maxNrInBlock)
                                            {
                                                bIsNext = false;    // block is full
                                            }
                                            else
                                            {
                                                bIsNext =  mbIsNextValue(lastFileID, fileID);

                                            }
                                            if (nInBlock > 0 && false == bIsNext)   // add request bloock start FileID and number of files
                                            {
                                                actionList += startIDInBlock.ToString() + "n" + nInBlock.ToString() + "; ";
                                                if (AbReqTrend)
                                                {
                                                    UInt16 year, month, day, hour;

                                                    bOk &= sbSplitYMDH(out year, out month, out day, out hour, startIDInBlock);
                                                    if (bOk)
                                                    {
                                                        bOk &= _mTzSettings.mbActionRequestTzrFile(buffer, ref index, year, month, day, hour);
                                                    }
                                                }
                                                else
                                                {
                                                    bOk &= _cbReqScpOneByOne ? _mTzSettings.mbActionRetransmitScpFile(buffer, ref index, startIDInBlock )
                                                        : _mTzSettings.mbActionRequestScpBlock(buffer, ref index, startIDInBlock, (UInt16)nInBlock);
                                                }
                                                nInBlock = 0;
                                                ++nBlocks;

                                                if( nBlocks >= _sActionMaxNrOfBlocks)
                                                {
                                                    bLimit = true;
                                                    break;
                                                }
                                            }
                                           CSirCollectItem foundItem = mFindCreateNew(out bNew, fileID, true);

                                            if (foundItem == null)
                                            {
                                                CProgram.sLogError("Can't create Collect Item at write action for " + fileID.ToString());
                                            }
                                            else
                                            {
                                                if (nInBlock == 0)
                                                {
                                                    startIDInBlock = fileID;
                                                }
                                                ++foundItem._mTryRequestCounter;
                                                foundItem._mLastRequestUTC = utcNow;
                                                foundItem._mLastActionID = _mActionSeqNr;

                                                ++nInBlock;
                                                lastFileID = fileID;
                                                ++_mActionReqNrFiles;
                                            }
                                        }
                                    }
                                    if( nInBlock > 0) // add last request bloock start FileID and number of files
                                    {
                                        actionList += startIDInBlock.ToString() + "n" + nInBlock.ToString() + "; ";
                                        if (AbReqTrend)
                                        {
                                            UInt16 year, month, day, hour;

                                            bOk &= sbSplitYMDH(out year, out month, out day, out hour, startIDInBlock);
                                            if (bOk)
                                            {
                                                bOk &= _mTzSettings.mbActionRequestTzrFile(buffer, ref index, year, month, day, hour);
                                            }
                                        }
                                        else
                                        {
                                            bOk &= _cbReqScpOneByOne ? _mTzSettings.mbActionRetransmitScpFile(buffer, ref index, startIDInBlock)
                                                 : _mTzSettings.mbActionRequestScpBlock(buffer, ref index, startIDInBlock, (UInt16)nInBlock);
                                        }
                                        nInBlock = 0;
                                    }
                                    actionList += "= " + _mActionReqNrFiles.ToString() + "req. files";
                                    if (bLimit)
                                    {
                                        actionList += " Limited";
                                        actionResult += " Limit " + _mActionReqNrFiles.ToString() + " req. ";
                                    }
                                    else
                                    {
                                        actionResult += " All " + _mActionReqNrFiles.ToString() + " req. ";
                                    }
                                    if( _mScanCalcResult != null)
                                    {
                                        _mScanCalcResult.Add(actionList);
                                    }

                                    bool bCopyOk;
                                    bOk = _mTzSettings.mbWriteAction(fileName, buffer, ref index, true, out bCopyOk, (UInt16)_mActionSeqNr);

                                    if (bOk)
                                    {
                                        if (false == bCopyOk)
                                        {
                                            actionResult += " copy ";
                                            bOk = false;
                                        }
                                        else
                                        {
                                            actionResult += CSirDeviceSettings._sbUseQueue ? " writen to queue" : " writen";
                                            _mbActionGenerated = true;
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("Send Action", ex);
                        }
                    }
                    if (false == bOk)
                    {
                        actionResult += " write failed!";
                    }
                    ArResult += ", " + actionResult;
                    return bOk;

                }
        */
        public bool mbLoadStatusFile(out string ArFileInfo, string ADeviceUnitPath, string ASerialNr)
        {
            bool bOk = false;
            string s = "";
            string name;

            _mStatusFileUTC = DateTime.MinValue;
            _mSirLastStatus = new CSironaStateSettings();

            if (_mSirLastStatus != null && ADeviceUnitPath != null && ASerialNr != null && ASerialNr.Length > 0 
                && CSironaStateSettings.sbMakeStatusFileName(out name, ASerialNr))
            {

                //string fileName = Path.Combine(_mDevicePath, name);
                string fromDir = Path.Combine(ADeviceUnitPath, CSironaStateSettings._csFromServerName);
                string fromFileName = Path.Combine(fromDir, name);

                try
                {
                    FileInfo fi = new FileInfo(fromFileName);

                    if( fi != null && fi.Exists)
                    {
                        _mStatusFileUTC = fi.LastWriteTimeUtc;

                        if (_mStatusFileUTC.Year > 2000)
                        {
                            s += CProgram.sDateTimeToString(fi.LastWriteTime);
                        }
                        bOk = _mSirLastStatus.mbReadFile(fromFileName);

                        if (false == bOk)
                        {
                            s += ", failed load!";
                        }
                    }
                    else
                    {
                        s += "Not present";
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed collect info for " + fromFileName, ex);
                }

            }
            ArFileInfo = s;
            _mbScanFileLoaded = bOk;
            return bOk;
        }

        public bool mbRunCycle(out string ArResult, out bool AbEnabled, out bool AbRunDone, string ADeviceUnitPath, string ASerialNr, 
            UInt32 AStudyIX, DateTime AStudyStartUtc, DateTime AStudyEndUtc, bool AbRunAutomatic)
        {
            bool bOk = true;
            bool bRun = false;
            bool bEnabled = false;
            string result = "";
            bool bPromptUser = false == AbRunAutomatic;
            bool bPromptEdit = false == AbRunAutomatic;
            bool bCheckStudyEnded = AbRunAutomatic;
            bool bCheckLastScanTime = AbRunAutomatic;
            bool bCheckParamDoScan = AbRunAutomatic;
            bool bCheckActionEmpty = AbRunAutomatic;
            bool bStudyEnded = AStudyEndUtc <= DateTime.UtcNow;
            string runSummery = "";
            bool bUsesQueue = false;
            UInt32 nInQueue = 0;
            string strQueue = "";
            string serialNr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);

            /*
             * get param + check active
             * 
             * get device status + in range active study
             * check new holter
             * calc next request 
             */


            try
            {
                mClearRun();

                if(serialNr == null || serialNr.Length == 0)
                {
                    if (result.Length > 0) result += ", ";
                    result += "Bad serial nr " + serialNr;
                    bOk = false;
                }
                if (AStudyIX == 0)
                {
                    if (result.Length > 0) result += ", ";
                    result += "NO study";
                    bOk = false;
                }
                if( bOk && bStudyEnded)
                {
                    result += "End Study";
                    if( bCheckStudyEnded)
                    {
                        bOk = false;
                    }
                }
                if(bOk && bCheckActionEmpty)
                {
                    // check action queue is empty
                    // not done
                }

                // load parameters
                bool bLoad = _mParamSerialNr == null || _mParamSerialNr.Length == 0
                    || _mParamSerialNr != serialNr;

                if (bOk && bLoad)
                {
                    bool bPresent = false;

                    bOk = mbLoadParamFile(out bPresent, ADeviceUnitPath, serialNr);

                    if (result.Length > 0) result += ", ";
                    if (false == bPresent)
                    {
                        result += "missing params file";
                        bOk = false;
                    }
                    else
                    {
                        result += bOk ? "" /*"parms loaded"*/ : "load params failed";
                    }
                }
                if( bOk )
                {
                    bEnabled = true;
                    if (bCheckParamDoScan)
                    {
                        // check if scanning must be done
                        // 
                        if (false == _mbEnabled)
                        {
                            bEnabled = false;
                            result += " AC disabled";
                            bOk = false;
                            bRun = false;
                        }
                    }
                }
                
                // Get device status file 
                if (bOk)
                {
                    string fileInfo;
                    bOk = mbLoadStatusFile(out fileInfo, ADeviceUnitPath, ASerialNr);

//                    result += " status= " + fileInfo;
                    if( bOk && _mSirLastStatus != null )
                    {
                        if( _mStatusFileUTC < AStudyStartUtc)
                        {
                            result += " status= " + fileInfo + " to old!";
                            bOk = false;
                        }
                        
                    }
                }
                bRun = bOk;

                // load old scan results
                if ( bOk )
                {
                    bool bPresent = false;
                    Int32 waitSec = 0;

                    bOk = mbLoadScanFile(out bPresent, out waitSec, serialNr, AStudyIX, bCheckLastScanTime );

                    if (result.Length > 0) result += ", ";
                    if( waitSec > 0 )
                    {
                        if ( bPromptUser)
                        {
                            result += "Wait for next scan in " + CProgram.sPrintTimeSpan_dhmSec(waitSec, "");
                        }
                        else
                        {
                            result = "skip, wait for " + CProgram.sPrintTimeSpan_dhmSec(waitSec, "");    // skip scan due to scan cycle time is not passed
                            bRun = false;
                        }
                    }
                    else if (false == bPresent)
                    {
                        result += "new scan";
                        bOk = true;
                    }
                    else
                    {
                        result += bOk ? " loaded" : " load scan failed";
  //                      CProgram.sLogError("Collect scan file " + serialNr + " at S" + AStudyIX.ToString() + "result= " + result);
                        // continue if scan file is missing;
                        bOk = true;
                    }
                }
                // check if this is a new holter
                if( bOk )
                {

                    _mbNextNewProcedure = false == _mbScanFileLoaded || _mStartProcedureUTC == DateTime.MinValue 
                        || _mSirLastStatus == null || _mSirLastStatus._mProcedureStartTimeDTO.Year < 0;

                    if( false == _mbNextNewProcedure)
                    {
                        double min = (_mSirLastStatus._mProcedureStartTimeDTO.UtcDateTime - _mStartProcedureUTC).TotalMinutes;

                        _mbNextNewProcedure = min < -1 || min > 1;  // not the same procedure

                        if(_mbNextNewProcedure)
                        {
                            result += " new Procedure" ;
                        }
                    }
                }
                // load device settings: 
                if (bOk)
                {
                    bOk = mbLoadDeviceSettings(ADeviceUnitPath, serialNr);
                    if( false == bOk )
                    {
                        result += " Settings file missing";
                        bRun = false;
                    }
                }
                if( false == AbRunAutomatic)
                {
                    result += "$";
                }
                // run from start to end and calc missing
                if (bOk && bRun)
                {
                   bOk = mbRunCalcNextRequest(ref result, ref bRun, out runSummery, ASerialNr);
                }
                if( bOk )
                {
                    mRunUseState();
                }

                // check number of actions in queue
                if (bOk && bRun)
                {
                    strQueue = "No Queue";
                    bUsesQueue = CSironaStateSettings._sbUseQueue;
                    if (bUsesQueue )
                    {
                        DateTime lastDT;
                        strQueue = "Failed Queue";

                        bOk = CSironaStateSettings.sbCheckSettingsFileQueue(out strQueue, out nInQueue, out lastDT, ASerialNr);

                        if (bOk)
                        {
                            if (AbRunAutomatic)
                            {
                                bRun = nInQueue< _sMaxActionsInQueue;   // only run when the queue has space
                            }
                            strQueue = "Queue= " + nInQueue.ToString();
                            strQueue += nInQueue < _sMaxActionsInQueue ? "<": ">=";
                            strQueue += _sMaxActionsInQueue.ToString();
                            strQueue += (bRun ? " " : " Skip" );
                        }
                    }
                    if (result.Length > 0) result += ", ";
                    result += strQueue;
                }

                // check state of Sirona device
                if( bOk )
                {

                }

                // prompt scan result and missing
                if (bPromptUser)
                {
                    string text = result + "\r\n";

/*                    if (_mScanCalcResult != null)
                    {
                        foreach (string s in _mScanCalcResult)
                        {
                            text += s + "\r\n";
                        }
                    }
*/                    text += runSummery + "\r\n" + (bRun ? " Run " : " Skip ");
                    if (false == ReqTextForm.sbReqText("Auto Collect " + _mName + " " + serialNr + (bRun ? " Run " : " Skip ") + strQueue, "Scan calc result", ref text))
                    {
                        bOk = false;
                        bRun = false;
                        result += " CANCELED";
                    }
                }
/*                // prompt collect list and ask for edit
                if (bOk && bPromptEdit )//&& _mRunCollect != null && _mRunNCollectNow > 0)
                {
                    string text = "";

                    text = result;

                    if (false == ReqTextForm.sbReqText("Auto Collect " + _mName + " " + serialNr + (bRun ? " Run " : " Skip ") + strQueue, 
                        "Collect now " + /*_mRunNCollectNow.ToString()* / " list", ref text))
                    {
                        bOk = false;
                        bRun = false;
                        result += " CANCELED";
                    }
                    else
                    {

                    }
                    
                }
*/
                // make action if study is active and queue
                if (bOk && bRun )
                {
                    bOk = mbMakeActionFile(ref result, ADeviceUnitPath, serialNr);
                    if( bOk )
                    {
                        mRunUseNext();
                    }
                }

                // save scan result
                if ( bOk && bRun )
                {
                    bOk = mbSaveScanFile(serialNr, AStudyIX, AbRunAutomatic, result);
                }

                // save missing files

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Collect test cycle " + serialNr + " result= "+ result, ex);
                result += "!ex!";
                bOk = false;
            }

            ArResult = result;
            AbRunDone = bRun;
            AbEnabled = bEnabled;
            return bOk;
        }
        public bool mbRunCalcNextRequest(ref string ArResult, ref bool ArbRun, out string ArSummery, string ASerialNR)
        {
            bool bOk = true;
            bool bRun = false;
            string scanResult = "";
            string summery = "";

            try
            {
                _mNextRequestLengthSec = 0;
                if (_mSirLastStatus == null)
                {
                    bOk = false;
                }
                else
                {
                    DateTime startProcDT = _mSirLastStatus._mProcedureStartTimeDTO.DateTime;
                    DateTime endProcDT = startProcDT.AddSeconds(_mSirLastStatus._mProcedureLengthSec);
                    Int16 timeZoneMin = (Int16)_mSirLastStatus._mProcedureStartTimeDTO.Offset.TotalMinutes;

                    summery = "Study " + _mStudy_IX.ToString() + " device " + _mParamSerialNr
                        + " mode= " + _mSirLastStatus._mProcedureType + " state= " + _mSirLastStatus._mProcedureState + "\r\n";

                    if (_mbNextNewProcedure) summery += "New ";
                    summery += "Procedure " + CProgram.sDateTimeToString(startProcDT)
                        + CProgram.sTimeZoneOffsetStringLong(timeZoneMin)
                        + " " + CProgram.sPrintTimeSpan_dhmSec(_mSirLastStatus._mProcedureLengthSec, "")
                         + " End " + CProgram.sDateTimeToString(endProcDT) + "\r\n";


                    DateTime startProcUTC = _mSirLastStatus._mProcedureStartTimeDTO.UtcDateTime;
                    DateTime endProcUTC = startProcUTC.AddSeconds(_mSirLastStatus._mProcedureLengthSec);

                    if (_mRequestEndUTC < startProcUTC)
                    {
                        _mRequestEndUTC = startProcUTC;
                        _mRequestTotalSec = 0;
                    }
                    DateTime requestStartDT = _mRequestEndUTC.AddMinutes(timeZoneMin ).AddSeconds( - _mRequestTotalSec);
                    DateTime requestEndDT = _mRequestEndUTC.AddMinutes(timeZoneMin);
                    summery += "Request start " + CProgram.sDateTimeToString(requestStartDT)
                        + CProgram.sTimeZoneOffsetStringLong(timeZoneMin)
                        + " " + CProgram.sPrintTimeSpan_dhmSec(_mRequestTotalSec, "")
                         + " End " + CProgram.sDateTimeToString(requestEndDT) + "\r\n";

                    UInt32 holterSec = CSironaStateSettings.sGetHolterReqLengthSec();

                    if ( _mHolterStartUTC == DateTime.MinValue || holterSec == 0)
                    {

                    }
                    else
                    {
                        DateTime startHolterDT = _mHolterStartUTC.AddMinutes(timeZoneMin);
                        DateTime endHolterDT = _mHolterEndUTC.AddMinutes(timeZoneMin);
                        UInt32 holterMissingSec = (UInt32)((endProcUTC - _mHolterEndUTC).TotalSeconds + 0.5);
                        UInt32 nrHolter = holterMissingSec / holterSec;

                        summery += "Holter " + CProgram.sDateTimeToString(startHolterDT)
                            + CProgram.sTimeZoneOffsetStringLong(timeZoneMin)
                            + " " + CProgram.sPrintTimeSpan_dhmSec(_mHolterLengthSec, "")
                             + " End " + CProgram.sDateTimeToString(endHolterDT) 
                             + "missing " + CProgram.sPrintTimeSpan_dhmSec( holterMissingSec, "" )
                             + " " + nrHolter.ToString() + " blocks\r\n";
                    }

                    Int32 missingSec = (Int32)((endProcUTC - _mRequestEndUTC).TotalSeconds + 0.5);


                    UInt32 waitSec = _sReqAgeMin * 60;

                    summery += "Missing " + CProgram.sPrintTimeSpan_dhmSec(missingSec, "");



                    if( holterSec == 0)
                    {
                        summery += "Holter off!\r\n";
                        scanResult = "  Holter off";
                    }
                    else if( missingSec < 0 )
                    {
                        summery += " Req past end Procedure!\r\n";
                        scanResult = " Past End";
                    }
                    else if (missingSec < holterSec )
                    {
                        summery += " < holter strip " + CProgram.sPrintTimeSpan_dhmSec(holterSec, "") + "\r\n";
                        scanResult = " Complete";
                    }
                    else
                    {
                        UInt32 nrRequests = (UInt32)missingSec / holterSec;
                        UInt32 afterSec = (UInt32)missingSec - holterSec;

                        summery += " req " + nrRequests.ToString() + " strip(" + CProgram.sPrintTimeSpan_dhmSec(holterSec, "") + ")";

                        if( afterSec < waitSec)
                        {
                            UInt32 statusSec = (UInt32)(DateTime.UtcNow - _mStatusFileUTC).TotalSeconds;

                            scanResult = " Wait " + CProgram.sPrintTimeSpan_dhmSec(waitSec - afterSec, "")
                                + " extra procedure length (" + CProgram.sPrintTimeSpan_dhmSec(_mProcedureLengthSec, "")
                                + " status " + CProgram.sPrintTimeSpan_dhmSec(statusSec, "") + ")";
                            summery += scanResult + "\r\n";

                        }
                        else
                        {
                            bRun = true;
                            _mNextRequestLengthSec = holterSec;
                            _mNextRequestStartUTC = _mRequestEndUTC;
                            _mNextRequestEndUTC = _mNextRequestStartUTC.AddSeconds(_mNextRequestLengthSec);

                            DateTime nextRequestStartDT = _mNextRequestStartUTC.AddMinutes(timeZoneMin);
                            scanResult = "->" + nrRequests.ToString() + " Req " + CProgram.sDateTimeToString(nextRequestStartDT)
                                + " strip " + CProgram.sPrintTimeSpan_dhmSec(holterSec, "")
                                + " =" + CProgram.sPrintTimeSpan_dhmSec(_mRequestTotalSec, "");
                            summery += "\r\n" + scanResult + "\r\n";
                        }
                    }
                }
            } 
            catch (Exception ex)
            {
                CProgram.sLogException("mbRunCalcNextRequest " + scanResult, ex);
                ArResult += "!ex!";
                bOk = false;
                bRun = false;
            }
            ArResult += scanResult;
            ArbRun = bRun;
            ArSummery = summery;
            return bOk;
        }

        public bool mbMakeActionFile(ref string ArResult, string ADeviceUnitPath, string ASerialNr)
        {
            bool bOk = true;
            string result = "";

            try
            {
                if (_mSirLastStatus == null || _mNextRequestLengthSec == 0)
                {
                    bOk = false;
                    result = "error";
                }
                else
                {
                    UInt32 startOffset = (UInt32)(_mNextRequestStartUTC - _mSirLastStatus._mProcedureStartTimeDTO.UtcDateTime).TotalSeconds;
                    DateTime startDT = _mNextRequestStartUTC.Add(_mSirLastStatus._mProcedureStartTimeDTO.Offset);

                    bOk = CSironaStateSettings.sbRequestSironaData(true, out result, ASerialNr, startDT, _mNextRequestLengthSec, true);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("mbMakeActionFile " + result, ex);
                ArResult += "!ex!";
                bOk = false;
            }
            if( ArResult != null && ArResult.Length > 0)
            {
                ArResult += ", ";
            }
            ArResult += result;
            return bOk;
        }

        private void mRunUseState()
        {
            if( _mSirLastStatus != null)
            {
                _mStartProcedureUTC = _mSirLastStatus._mProcedureStartTimeDTO.UtcDateTime;
                _mDeviceTZoneMin = (Int16)_mSirLastStatus._mProcedureStartTimeDTO.Offset.TotalMinutes;
                _mProcedureLengthSec = _mSirLastStatus._mProcedureLengthSec;
                if( _mParamSerialNr != _mSirLastStatus._mDeviceSerial)
                {
                    CProgram.sLogError("serial not set in Sirona AutoCollect");
                }
            }
        }
        private void mRunUseNext()
        {
            
            _mRequestEndUTC = _mNextRequestEndUTC;
            _mRequestTotalSec += _mNextRequestLengthSec;
        }
    }
}
