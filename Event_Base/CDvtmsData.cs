﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Program_Base;
using System.IO;
using EventboardEntryForms;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace Event_Base
{
    public enum DDeviceType { Unknown, TZ, Sirona, DV2, DVX, NR_ENUMS }
    public enum DTzType { Unknown, TzAera, TzClarus }

    public class CDvtmsData
    {
        private static CSqlDBaseConnection _sDBaseServer = null;
        private static string _sEnumFilePath = null;
        private static bool _sbInitDone = false;

        private static string _sRecordingDir = null;
        private static string _sDataDir = null;
        private static string _sDeviceDir = null;
        private static string _sStudyDir = null;
        private static string[] _sDeviceTypeDir = { null, null, null, null, null, null };  //DDeviceType.NR_ENUMS;
        private static DTzType _sTzDefaultType = DTzType.TzAera;

        public static CSqlEnumList _sEnumListMonthName = null;
        public static CSqlEnumList _sEnumListStudyType = null;
        public static CSqlEnumList _sEnumListReportInterval = null;
        public static CSqlEnumList _sEnumListMctInterval = null;
        public static CSqlEnumList _sEnumListPatientHistories = null;
        public static CSqlEnumList _sEnumListCountries = null;
        public static CSqlEnumList _sEnumListStates = null;
        public static CSqlEnumList _sEnumListStudyProcedures = null;
        public static CSqlEnumList _sEnumListFindingsClass = null;
        public static CSqlEnumList _sEnumListStudyRhythms = null;
        //        public static CSqlEnumList _sEnumListPdfReports = null;   // no longer used-> use _sEnumListReportInterval
        //        public static CSqlEnumList _sEnumListMctReports = null;

        public static CSqlEnumList _sEnumListStudyPermissions = null;
        public static string _stStudyPermissionsText = "Permissions";
        public const string _cStudyPermSiteCode = "$Site";
        public static UInt16 _sStudyPermSiteGroup = 0;
        public static string _sStudyPermSiteText = ".Site";
        public const string _cStudyPermSkilGradeEvent = "$GEvent";
        public static UInt16 _sStudyPermSkilGradeEventGroup = 0;
        public static string _sStudyPermSkilGradeEventText = ".Site";
        public const string _cStudyPermSkilGradeMCT = "$GMCT";
        public static UInt16 _sStudyPermSkilGradeMCTGroup = 0;
        public static string _sStudyPermSkilGradeMctText = ".Site";

        public static CBitSet64 _sStudyPermSetSite = null;
        public static CBitSet64 _sStudyPermSetSkilGradeEvent = null;
        public static CBitSet64 _sStudyPermSetSkilGradeMCT = null;

        public const string _cFindings_Tachy = "Tachy";     // const labels for program parts that do not use the enum list
        public const string _cFindings_Brady = "Brady";
        public const string _cFindings_Pause = "Pause";
        public const string _cFindings_AF = "AF";   // AFib
        public const string _cFindings_BL = "BL";   // Base line
        public const string _cFindings_Other = "Other";
        public const string _cFindings_PAC = "PAC"; // SVEB
        public const string _cFindings_PVC = "PVC"; // VEB
        public const string _cFindings_Pace = "Pace";
        public const string _cFindings_VT = "VT";   // VTach
        public const string _cFindings_VF = "VF";   // used later
        public const string _cFindings_AFL = "AFl"; // AFlutter
        public const string _cFindings_ASys = "ASys";   // used later
        public const string _cFindings_Manual = "Man";
        public const string _cFindings_Normal = "Norm";  // Normal
        public const string _cFindings_AbNormal = "AbN";  // Normal


        public const string _cStudyFolder_Recorder = "Recorder";
        public const string _cStudyFolder_Pdf = "Pdf";
        public const string _cStudyFolder_Holter = "Holter";

        public static string _sPrintStudyLabel = "";
        public static string _sPrintStudyCode = "";

        public static Color _sDisplayGridColor = Color.LightPink;
        public static Color _sDisplaySignalColor = Color.Black;
        public static float _sDisplaySignalWidth = 0.0F;

        public static Color _sPrintGridColor = Color.LightPink;
        public static Color _sPrintSignalColor = Color.Black;
        public static float _sPrintSignalWidth = 0.0F;

        private static string _sPrintCenterLine1 = "";
        private static string _sPrintCenterLine2 = "";
        private static Image _sPrintCenterImage = null;
        private static Image _sPrintCenterImage_BW = null;
        public static bool _sbPrintCenterLoaded = false;

        private static string[] _sUserInitialNrList = null;
        private static UInt32 sUserInitialsID = 0;
        private static string sUserInitals = "";

        private static UInt32 _sUsersCount = 0;
        private static UInt32 _sUsersLastID = 0;


        public static bool _sbUserListEnfored = true;
        public static UInt16 _sUserPinLength = 4;

        public static bool _sbPrintPhysicianAskName = false;
        public static UInt16 _sPrintPhysicianNameMode = 5; // 0 = off, 1 = label, 2 = text, 3 = img, 4 = both, 5 = line
        public static UInt16 _sPrintPhysicianSignMode = 5; // 0 = off, 1 = label, 2 = text, 3 = img, 4 = both 5 = line
        public static UInt16 _sPrintPhysicianDateMode = 2; // 0 = off, 1 = label, 2 = today, 3 = line, 99 = ask
        public static string _sPrintPhysicianLabel = "Physician";

        public static void sSetDBaseServer(string ARecordingDir, CSqlDBaseConnection ADBaseServer, string AEnumPath)
        {
            string path;

            _sRecordingDir = ARecordingDir;
            _sDeviceDir = null;

            _sDBaseServer = ADBaseServer;
            _sEnumFilePath = AEnumPath;

            int n = (int)DDeviceType.NR_ENUMS;
            if (_sDeviceTypeDir.Length < n)
            {
                CProgram.sPromptError(true, "Init program", "software error: _mDeviceTypeDir not big enough");
            }
            else
            {
                for (int i = 1; i < n; ++i)
                {
                    string subDirName = sGetDeviceTypeDirName((DDeviceType)i);

                    if (sbGetDeviceSubDirByName(out path, subDirName))
                    {
                        _sDeviceTypeDir[i] = path;  // prefill the device type subdir with validated values
                    }
                }
            }
        }
        public static bool sbTestDBaseServer()
        {
            bool bOk = _sDBaseServer != null;

            if (bOk)
            {
                bOk = false;
                CSqlEnumList testList = null;
                try
                {
                    bOk = sbLoadEnumListDb(ref testList, true, "StudyType", false, false);     // test by loading a known enum list
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Test DdBase server", ex);
                }
            }
            return bOk;
        }

        public static string sGetRecordingDir()
        {
            return _sRecordingDir;
        }

        public static string sGetDataDir()
        {
            if (_sDataDir == null && _sRecordingDir != null && _sRecordingDir.Length > 1)
            {
                try
                {
                    string dir = Path.GetDirectoryName(_sRecordingDir);
                    string parent = Path.GetDirectoryName(dir);

                    if (Directory.Exists(parent))
                    {
                        _sDataDir = parent;
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed get data dir", ex);
                }
            }
            return _sDataDir;
        }

        public static string sGetRecordingCaseDir(CRecordMit ARecord)
        {
            string caseDir = "";

            if (_sRecordingDir != null && _sRecordingDir.Length > 1 && ARecord != null && ARecord.mIndex_KEY > 0 && ARecord.mReceivedUTC > DateTime.MinValue)
            {
                string ym, d;

                ym = "YM" + ARecord.mReceivedUTC.Year.ToString() + ARecord.mReceivedUTC.Month.ToString("00");
                d = "D" + ARecord.mReceivedUTC.Day.ToString("00");

                caseDir = Path.Combine(_sRecordingDir, ym, d, ARecord.mStoredAt);
            }
            return caseDir;
        }
        public static string sGetStudyDir()
        {
            try
            {
                if (_sStudyDir == null && _sRecordingDir != null && _sRecordingDir.Length > 1)
                {
                    string path = sGetDataDir();
                    if (path != null)
                    {
                        string studyDir = Path.Combine(path, "Study");
                        if (false == sbCreateDir(studyDir))
                        {
                            CProgram.sLogError("Failed create study dir " + studyDir);
                            studyDir = null;
                        }
                        else
                        {
                            _sStudyDir = studyDir;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed get study dir", ex);
                _sStudyDir = null;
            }
            return _sStudyDir;
        }

        public static bool sbCreateDir(string ADir)
        {
            bool bOk = false;

            try
            {
                if (Directory.Exists(ADir))
                {
                    bOk = true;
                }
                else
                {
                    Directory.CreateDirectory(ADir);

                    bOk = Directory.Exists(ADir);
                }
            }
            catch (Exception ex)
            {
                bOk = false;
            }
            return bOk;

        }
        public static bool sbGetStudyCaseDir(out string ArPath, UInt32 AStudyNr, bool AbCheckExist)
        {
            string studyDir = null;

            try
            {
                if (AStudyNr > 0)
                {
                    studyDir = sGetStudyDir();
                    if (studyDir != null)
                    {
                        UInt32 s6 = AStudyNr / 1000000;
                        UInt32 s4 = (AStudyNr / 10000) % 100;
                        UInt32 s2 = (AStudyNr / 100) % 100;

                        string chDir = s6.ToString("D02") + "\\" + s4.ToString("D02") + "\\" + s2.ToString("D02") + "\\S" + AStudyNr.ToString("D08") + "\\";

                        studyDir = Path.Combine(studyDir, chDir);
                        if (AbCheckExist)
                        {
                            if (false == Directory.Exists(studyDir))
                            {
                                CProgram.sLogError("Study directory does not exist " + chDir);
                                studyDir = null;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                studyDir = null;
            }
            ArPath = studyDir;
            return studyDir != null;
        }
        private static bool sbGetStudyCaseSubDir(out string ArPath, UInt32 AStudyNr, string ASubDir, bool AbCheckExist)
        {
            string studyDir = null;

            try
            {
                if (sbGetStudyCaseDir(out studyDir, AStudyNr, AbCheckExist))
                {
                    if (ASubDir != null && ASubDir.Length > 0)
                    {
                        studyDir = Path.Combine(studyDir, ASubDir);

                        if (AbCheckExist)
                        {
                            if (false == Directory.Exists(studyDir))
                            {
                                CProgram.sLogError("Study sub directory does not exist " + studyDir);
                                studyDir = null;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                studyDir = null;
            }
            ArPath = studyDir;
            return studyDir != null;
        }

        public static void sOpenStudyDir(UInt32 AStudyNr, string ASubDir)
        {
            if (AStudyNr > 0)
            {
                string studyPath;

                if (CDvtmsData.sbGetStudyCaseSubDir(out studyPath, AStudyNr, ASubDir, true))
                {
                    // open explorer wit file at cursor
                    try
                    {
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.FileName = @"explorer";
                        startInfo.Arguments = studyPath;
                        Process.Start(startInfo);
                    }
                    catch (Exception e2)
                    {
                        CProgram.sLogException("Failed open explorer", e2);
                    }
                }
            }
        }



        public static bool sbCreateStudyCaseDir(out string ArStudyPath, UInt32 AStudyNr)
        {
            string studyDir = null;
            bool bOk = false;

            try
            {
                if (AStudyNr > 0)
                {
                    studyDir = sGetStudyDir();
                    if (studyDir != null)
                    {
                        UInt32 s6 = AStudyNr / 1000000;
                        UInt32 s4 = (AStudyNr / 10000) % 100;
                        UInt32 s2 = (AStudyNr / 100) % 100;

                        //                        if (Directory.Exists(studyDir))
                        {
                            studyDir = Path.Combine(studyDir, s6.ToString("D02"));

                            if (sbCreateDir(studyDir))
                            {
                                studyDir = Path.Combine(studyDir, s4.ToString("D02"));
                                if (sbCreateDir(studyDir))
                                {
                                    studyDir = Path.Combine(studyDir, s2.ToString("D02"));
                                    if (sbCreateDir(studyDir))
                                    {
                                        studyDir = Path.Combine(studyDir, "S" + AStudyNr.ToString("D08"));
                                        bOk = sbCreateDir(studyDir);
                                    }
                                }
                            }
                        }
                        if (bOk == false)
                        {
                            string chDir = s6.ToString("D02") + "\\" + s4.ToString("D02") + "\\" + s2.ToString("D02") + "\\" + "\\S" + AStudyNr.ToString("D08") + "\\";

                            CProgram.sLogError("Failed to create study dir:" + chDir);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                studyDir = null;
                bOk = false;
            }
            ArStudyPath = studyDir;
            return bOk;
        }
        private static bool sbCreateStudyCaseSubDir(out string ArStudyPath, UInt32 AStudyNr, string AStudySubFolder)
        {
            string studyDir = null;
            bool bOk = false;

            try
            {
                if (AStudySubFolder != null && AStudySubFolder.Length > 0)
                {
                    if (sbCreateStudyCaseDir(out studyDir, AStudyNr))
                    {
                        studyDir = Path.Combine(studyDir, AStudySubFolder);
                        bOk = sbCreateDir(studyDir);
                    }
                }
                if (bOk == false)
                {
                    CProgram.sLogError("Failed to create study dir:" + studyDir);
                    studyDir = null;
                }
            }
            catch (Exception ex)
            {
                studyDir = null;
                bOk = false;
            }
            ArStudyPath = studyDir;
            return bOk;
        }

        public static bool sbGetStudyRecorderDir(out string ArStudyPath, UInt32 AStudyNr, bool AbCheckExist)
        {
            return sbGetStudyCaseSubDir(out ArStudyPath, AStudyNr, _cStudyFolder_Recorder, AbCheckExist);
        }
        public static bool sbGetStudyPdfDir(out string ArStudyPath, UInt32 AStudyNr, bool AbCheckExist)
        {
            return sbGetStudyCaseSubDir(out ArStudyPath, AStudyNr, _cStudyFolder_Pdf, AbCheckExist);
        }

        public static bool sbCreateStudyRecorderDir(out string ArStudyPath, UInt32 AStudyNr)
        {
            return sbCreateStudyCaseSubDir(out ArStudyPath, AStudyNr, _cStudyFolder_Recorder);
        }

        public static bool sbCreateStudyPdfDir(out string ArStudyPath, UInt32 AStudyNr)
        {
            return sbCreateStudyCaseSubDir(out ArStudyPath, AStudyNr, _cStudyFolder_Pdf);
        }
        public static bool sbCreateStudyHolterDir(out string ArStudyPath, UInt32 AStudyNr)
        {
            return sbCreateStudyCaseSubDir(out ArStudyPath, AStudyNr, _cStudyFolder_Holter);
        }

        public static string sMakePdfName(UInt32 AstudyNr, string ATypeDoc, string ARefCode, UInt32 ARefID)
        {
            DateTime dt = DateTime.UtcNow;
            string s = "S" + AstudyNr.ToString("D08") + "_"
                + dt.Year.ToString("D04") + dt.Month.ToString("D02") + dt.Day.ToString("D02") + dt.Hour.ToString("D02") + dt.Minute.ToString("D02") + dt.Second.ToString("D02")
                + ATypeDoc + "_" + ARefCode + ARefID.ToString("D08");

            return s;
        }

        public static bool sbCreateDeviceDir(out string ArPath)
        {
            string deviceDir = null;
            bool bOk = false;

            try
            {
                if (_sDeviceDir != null && _sDeviceDir.Length > 0)
                {
                    deviceDir = _sDeviceDir;
                    bOk = true;
                }
                else if (_sRecordingDir != null)
                {
                    if (Directory.Exists(_sRecordingDir))
                    {
                        string dir = Path.GetDirectoryName(_sRecordingDir);
                        string parent = Path.GetDirectoryName(dir);
                        deviceDir = Path.Combine(parent, "Device");
                        if (false == sbCreateDir(deviceDir))
                        {
                            CProgram.sLogError("Failed create device dir " + deviceDir);
                            deviceDir = null;
                        }
                        else
                        {
                            bOk = true; // returns ../Device/ from recotding dir
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                deviceDir = null;
                bOk = false;
                CProgram.sLogException("Failed to create Device dir", ex);
            }
            ArPath = bOk ? deviceDir : null;

            return bOk;
        }

        public static string sGetDeviceTypeName(DDeviceType AType)
        {
            switch (AType)
            {
                case DDeviceType.TZ: return "TZ";
                case DDeviceType.Sirona: return "Sirona";
                case DDeviceType.DV2: return "DV2";
                case DDeviceType.DVX: return "DVX";
            }
            return "";
        }
        public static string sGetDeviceTypeShortName(DDeviceType AType)
        {
            switch (AType)
            {
                case DDeviceType.TZ: return "TZ";
                case DDeviceType.Sirona: return "Sir";
                case DDeviceType.DV2: return "DV2";
                case DDeviceType.DVX: return "DVX";
            }
            return "";
        }
        public static string sGetDeviceTypeDirName(DDeviceType AType)
        {
            switch (AType)
            {
                case DDeviceType.TZ: return "TzAera";
                case DDeviceType.Sirona: return "Sirona";
                case DDeviceType.DV2: return "DV2";
                case DDeviceType.DVX: return "DVX";
            }
            return "";
        }

        public static string sGetDeviceTypeMctExt(DDeviceType AType)
        {
            switch (AType)
            {
                case DDeviceType.TZ: return "tzr";
                case DDeviceType.Sirona: return "atr";
                case DDeviceType.DV2: return "";
                case DDeviceType.DVX: return "tnd";

            }
            return "";
        }

        public static bool sbGetDeviceDir(out string ArPath)
        {
            string deviceDir = null;
            bool bOk = false;

            try
            {
                if (_sDeviceDir != null && _sDeviceDir.Length > 0)
                {
                    deviceDir = _sDeviceDir;
                    bOk = true;
                }
                else if (_sRecordingDir != null)
                {
                    if (Directory.Exists(_sRecordingDir))
                    {
                        string dir = Path.GetDirectoryName(_sRecordingDir);
                        string parent = Path.GetDirectoryName(dir);
                        deviceDir = Path.Combine(parent, "Device");
                        if (false == Directory.Exists(deviceDir))
                        {
                            CProgram.sLogError("Failed exist device dir " + deviceDir);
                            deviceDir = null;
                        }
                        else
                        {
                            _sDeviceDir = deviceDir;
                            bOk = true; // returns ../Device/ from recotding dir
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                deviceDir = null;
                bOk = false;
                CProgram.sLogException("Failed to create Device dir", ex);
            }
            ArPath = bOk ? deviceDir : null;

            return bOk;
        }
        private static bool sbCreateDeviceSubDirByName(out string ArPath, string ATypeName)
        {
            string deviceDir = null;
            bool bOk = false;

            try
            {
                if (ATypeName != null && ATypeName.Length > 0)
                {
                    if (sbCreateDeviceDir(out deviceDir))
                    {
                        deviceDir = Path.Combine(deviceDir, ATypeName);

                        if (sbCreateDir(deviceDir))
                        {
                            bOk = true; //returns  device/Type/snr
                        }
                        else
                        {
                            CProgram.sLogError("Failed create device type sub dir " + deviceDir);
                        }
                    }
                }
                else
                {
                    CProgram.sLogError("Empty device type name" + deviceDir);

                }
            }
            catch (Exception ex)
            {
                deviceDir = null;
                bOk = false;
                CProgram.sLogException("Failed to create Device dir", ex);
            }
            ArPath = bOk ? deviceDir : null;

            return bOk;
        }
        public static bool sbCreateDeviceTypeDir(out string ArPath, DDeviceType AType)
        {
            string path = _sDeviceTypeDir[(int)AType];

            if (path == null)
            {
                string subDirName = sGetDeviceTypeDirName(AType);

                if (subDirName != null && subDirName.Length > 0)
                {
                    if (sbCreateDeviceSubDirByName(out path, subDirName))
                    {
                        _sDeviceTypeDir[(int)AType] = path;
                    }
                }
            }
            ArPath = path;

            return path != null;
        }

        public static bool sbCreateDeviceUnitDir(out string ArPath, DDeviceType AType, string AUnitSNr)
        {
            string path = null;

            try
            {
                if (sbCreateDeviceTypeDir(out path, AType))
                {
                    if (AUnitSNr != null && AUnitSNr.Length > 0)
                    {
                        string storeSnr = sGetDeviceStoreSnrName(AType, AUnitSNr);
                        path = Path.Combine(path, storeSnr);
                        if (false == sbCreateDir(path))
                        {
                            CProgram.sLogError("Failed create device dir " + path);
                            path = null;
                        }
                    }
                    else
                    {
                        CProgram.sLogError("Empty device snr " + path);
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed to create Device Unit " + AUnitSNr + ": " + path, ex);
                path = null;
            }
            ArPath = path;

            return path != null;
        }

        private static bool sbGetDeviceSubDirByName(out string ArPath, string ATypeName)
        {
            string deviceDir = null;
            bool bOk = false;

            try
            {
                if (sbGetDeviceDir(out deviceDir))
                {
                    if (ATypeName != null && ATypeName.Length > 0)
                    {
                        deviceDir = Path.Combine(deviceDir, ATypeName);

                        if (Directory.Exists(deviceDir))
                        {
                            bOk = true;
                        }
                        /*                        else
                                                {
                                                    CProgram.sLogError("Failed get device type dir " + deviceDir);
                                                }
                        */
                    }
                    else
                    {
                        CProgram.sLogError("Empty device type ");
                    }
                }
            }
            catch (Exception ex)
            {
                deviceDir = null;
                bOk = false;
                CProgram.sLogException("Failed to get Device dir " + deviceDir + " / " + ATypeName, ex);
            }
            ArPath = bOk ? deviceDir : "";

            return bOk;
        }
        public static bool sbGetDeviceTypeDir(out string ArPath, DDeviceType AType)
        {
            ArPath = _sDeviceTypeDir[(int)AType];   // return stored quick way of 

            return ArPath != null;
        }

        public static bool sbIsTzClarusSnrName(string AUnitSNr)
        {
            bool bClarus = false;

            if (_sTzDefaultType == DTzType.TzAera)
            {
                if (AUnitSNr != null)
                {
                    int len = AUnitSNr.Length;

                    if (len == 7)
                    {
                        bClarus = Char.IsLetter(AUnitSNr[0]);
                    }
                    else if (len > 7)
                    {
                        string snr = AUnitSNr.ToUpper();

                        bClarus = snr.StartsWith("H3R");
                        // H3Rx123456 -> <'H'+x>123456
                    }
                }
            }
            return bClarus;
        }

        public static string sGetTzStoreSnrName(string AUnitSNr)
        {
            string storeSnr = AUnitSNr;

            if (_sTzDefaultType == DTzType.TzAera)
            {
                if (AUnitSNr != null && AUnitSNr.Length > 7)
                {
                    string snr = AUnitSNr.ToUpper();

                    if (snr.StartsWith("H3R"))
                    {
                        // H3Rx123456 -> <'H'+x>123456
                        string number = snr.Substring(3, 1);
                        int i = 0;

                        if (int.TryParse(number, out i))
                        {
                            storeSnr = (char)('H' + i) + AUnitSNr.Substring(4);
                        }
                    }
                }
            }
            return storeSnr;
        }
        public static string sGetTzAltSnrName(string AUnitSNr)
        {
            string altSnr = "";
            int len = AUnitSNr == null ? 0 : AUnitSNr.Length;

            if (len == 7)
            {
                char c = Char.ToUpper(AUnitSNr[0]);
                if (c >= 'H' && c <= 'Q')
                {
                    //<'H'+x>123456 ->  H3Rx123456 snr TzAerus to TZ Clarus
                    int i = c - 'H';
                    altSnr = "H3R" + i.ToString() + AUnitSNr.Substring(1);
                }
            }
            else if (len == 10)
            {
                string snr = AUnitSNr.ToUpper();

                if (snr.StartsWith("H3R"))
                {
                    // H3Rx123456 -> <'H'+x>123456 snr TZ Clarus -> TZ Aera
                    string number = snr.Substring(3, 1);
                    int i = 0;

                    if (int.TryParse(number, out i))
                    {
                        altSnr = (char)('H' + i) + AUnitSNr.Substring(4);
                    }
                }
            }
            return altSnr;
        }

        public static string sGetDeviceStoreSnrName(DDeviceType AType, string AUnitSNr)
        {
            return AType == DDeviceType.TZ ? sGetTzStoreSnrName(AUnitSNr) : AUnitSNr;
        }

        public static bool sbGetDeviceUnitDir(out string ArPath, DDeviceType AType, string AUnitSNr, bool AbCheckExists)
        {
            string path = null;

            try
            {
                if (sbGetDeviceTypeDir(out path, AType))
                {
                    if (AUnitSNr != null && AUnitSNr.Length > 0)
                    {
                        string storeSnr = sGetDeviceStoreSnrName(AType, AUnitSNr);
                        path = Path.Combine(path, storeSnr);
                        if (AbCheckExists)
                        {
                            if (false == Directory.Exists(path))
                            {
                                //CProgram.sLogError("Device dir " + path + "  does not exist");
                                path = null;
                            }
                        }
                    }
                    else
                    {
                        CProgram.sLogError("Empty device snr " + path);
                    }

                }
            }
            catch (Exception ex)
            {
                path = null;
                CProgram.sLogException("Failed to get Device Unit " + AUnitSNr + ": " + path, ex);
            }
            ArPath = path;

            return path != null;
        }

        public static DDeviceType sFindDeviceUnitType(out string ArDevicePath, string ASnr)
        {
            DDeviceType deviceType = DDeviceType.Unknown;
            string devicePath = null;

            if (CDvtmsData.sbGetDeviceUnitDir(out devicePath, DDeviceType.TZ, ASnr, true))
            {
                deviceType = DDeviceType.TZ;       // device snr is in TZ directory
            }
            else if (CDvtmsData.sbGetDeviceUnitDir(out devicePath, DDeviceType.Sirona, ASnr, true))
            {
                deviceType = DDeviceType.Sirona;       // device snr is in Sirona directory
            }
            else if (CDvtmsData.sbGetDeviceUnitDir(out devicePath, DDeviceType.DV2, ASnr, true))
            {
                deviceType = DDeviceType.DV2;       // device snr is in DV2 directory
            }
            else if (CDvtmsData.sbGetDeviceUnitDir(out devicePath, DDeviceType.DVX, ASnr, true))
            {
                deviceType = DDeviceType.DVX;       // device snr is in DVX directory
            }
            ArDevicePath = deviceType == DDeviceType.Unknown ? null : devicePath;
            return deviceType;
        }

        public static CSqlDBaseConnection sGetDBaseConnection()
        {
            return _sDBaseServer;
        }

        public static bool sbInitData()
        {
            bool bOk = _sbInitDone;

            //            return false; // turn off for release beta1.0.0.353

            if (_sbInitDone == false)
            {
                bool bLoadOnce = true;
                bool bLogOk = true;
                bool bLogList = false; // true;
                bOk = true;

                bOk &= sbLoadEnumListDb(ref _sEnumListStudyType, bLoadOnce, "StudyType", bLogOk, bLogList);
                bOk &= sbLoadEnumListFile(ref _sEnumListReportInterval, bLoadOnce, "PdfReports", 2, bLogList);
                //                bOk &= sbLoadEnumListDb(ref _sEnumListReportInterval, bLoadOnce, "StudyReportInterval", bLog);
                bOk &= sbLoadEnumListFile(ref _sEnumListMonthName, bLoadOnce, "MonthName", 2, bLogList);
                bOk &= sbLoadEnumListFile(ref _sEnumListCountries, bLoadOnce, "Countries", 6, bLogList);
                bOk &= sbLoadEnumListFile(ref _sEnumListStates, bLoadOnce, "States", 2, bLogList);
                bOk &= sbLoadEnumListFile(ref _sEnumListPatientHistories, bLoadOnce, "PatientHistory", 2, bLogList);
                bOk &= sbLoadEnumListFile(ref _sEnumListStudyProcedures, bLoadOnce, "StudyProcedures", 2, bLogList);
                //                bOk &= sbLoadEnumListFile(ref _sEnumListPdfReports, bLoadOnce, "PdfReports", 2, bLog);
                bOk &= sbLoadEnumListFile(ref _sEnumListMctInterval, bLoadOnce, "MctReports", 2, bLogList);
                bOk &= sbLoadEnumListFile(ref _sEnumListFindingsClass, bLoadOnce, "FindingsClass", 2, bLogList);
                bOk &= sbLoadEnumListFile(ref _sEnumListStudyRhythms, bLoadOnce, "FindingsRhythms", 2, bLogList);

                bOk &= sbLoadEnumListDb(ref _sEnumListStudyPermissions, bLoadOnce, "StudyPermissions", bLogOk, bLogList);

                if (_sEnumListStudyPermissions != null)
                {
                    int group;
                    string label;

                    label = _sEnumListStudyPermissions.mGetLabel(_cStudyPermSiteCode);
                    if (label != null && label.Length > 0) _sStudyPermSiteText = label;
                    group = _sEnumListStudyPermissions.mGetIntValue(_cStudyPermSiteCode);
                    _sStudyPermSiteGroup = group > 0 && group < 32000 ? (UInt16)group : (UInt16)0;

                    label = _sEnumListStudyPermissions.mGetLabel(_cStudyPermSkilGradeEvent);
                    if (label != null && label.Length > 0) _sStudyPermSkilGradeEventText = label;
                    group = _sEnumListStudyPermissions.mGetIntValue(_cStudyPermSkilGradeEvent);
                    _sStudyPermSkilGradeEventGroup = group > 0 && group < 32000 ? (UInt16)group : (UInt16)0;

                    label = _sEnumListStudyPermissions.mGetLabel(_cStudyPermSkilGradeMCT);
                    if (label != null && label.Length > 0) _sStudyPermSkilGradeMctText = label;
                    group = _sEnumListStudyPermissions.mGetIntValue(_cStudyPermSkilGradeMCT);
                    _sStudyPermSkilGradeMCTGroup = group > 0 && group < 32000 ? (UInt16)group : (UInt16)0;

                    _sStudyPermSetSite = new CBitSet64();
                    _sStudyPermSetSkilGradeEvent = new CBitSet64();
                    _sStudyPermSetSkilGradeMCT = new CBitSet64();

                    CBitSet64 found = new CBitSet64();

                    if (found != null)
                    {
                        foreach (CSqlEnumRow node in _sEnumListStudyPermissions.mGetEnumList())
                        {
                            if (node._mGroup > 0)   // group =0: list of groups
                            {
                                int value = node.mGetIntValue();

                                if (value < 0 || value > CBitSet64._cNrValidBits)
                                {
                                    CProgram.sLogLine("Invalid StudyPermissions flag nr" + value.ToString() + ", code= " + node._mCode + ", " + node._mLabel);
                                }
                                else if (found.mbIsBitSet((UInt16)value))
                                {
                                    CProgram.sLogLine("Double StudyPermissions flag nr" + value.ToString() + ", code= " + node._mCode + ", " + node._mLabel);

                                }
                                else
                                {
                                    found.mSetOneBit((UInt16)value);

                                    if (node._mGroup == _sStudyPermSiteGroup) _sStudyPermSetSite.mSetOneBit((UInt16)value);
                                    if (node._mGroup == _sStudyPermSkilGradeEventGroup) _sStudyPermSetSkilGradeEvent.mSetOneBit((UInt16)value);
                                    if (node._mGroup == _sStudyPermSkilGradeMCTGroup) _sStudyPermSetSkilGradeMCT.mSetOneBit((UInt16)value);
                                }
                            }
                        }
                    }

                }

                /*if(_sEnumListReportInterval != null)
                {
                    List<CSqlEnumRow> reportList = _sEnumListReportInterval.mGetEnumList();
                    _sEnumListMctInterval = new CSqlEnumList("MctReportInterval");

                    if(_sEnumListMctInterval != null && reportList != null)
                    {
                        List<CSqlEnumRow>mctList = _sEnumListMctInterval.mGetEnumList();
                        if (mctList != null)
                        {
                            bool bAdd = false;  // skip first
                            foreach (CSqlEnumRow row in reportList)
                            {
                                if (bAdd)
                                {
                                    CSqlEnumRow copy = (CSqlEnumRow)row.mCreateNewCopy();

                                    mctList.Add(copy);
                                }
                                bAdd = true;
                            }
                        }
                    }
                }
            */
                _sbInitDone = bOk;
            }
            return bOk;
        }

        public static void mWriteTablesToCvs()
        {
            if (_sbInitDone && _sDBaseServer != null)
            {
                bool bSave = false;
                if (bSave)
                {
                    if (_sEnumListPatientHistories != null) _sEnumListPatientHistories.mSaveEnumList(_sDBaseServer);
                    if (_sEnumListStudyProcedures != null) _sEnumListStudyProcedures.mSaveEnumList(_sDBaseServer);
                    if (_sEnumListCountries != null) _sEnumListCountries.mSaveEnumList(_sDBaseServer);
                }
            }
        }

        public static string sGetStudyPermissionsGroupName(UInt16 AGroupNr)
        {
            string s = "";

            if (_sEnumListStudyPermissions != null)
            {
                foreach (CSqlEnumRow node in _sEnumListStudyPermissions.mGetEnumList())
                {
                    if (node._mGroup == 0)   // group =0: list of groups
                    {
                        int value = node.mGetIntValue();

                        if (value == AGroupNr)
                        {
                            s = node._mLabel;
                            break;
                        }
                    }
                }
            }
            return s;
        }
        public static string sGetStudyPermissionsBitCode(UInt16 ABitNr)
        {
            string s = "?" + ABitNr.ToString() + "?";

            if (_sEnumListStudyPermissions != null)
            {
                foreach (CSqlEnumRow node in _sEnumListStudyPermissions.mGetEnumList())
                {
                    if (node._mGroup != 0)   // group =0: list of groups
                    {
                        int value = node.mGetIntValue();

                        if (value == ABitNr)
                        {
                            s = node._mCode;
                            break;
                        }
                    }
                }
            }
            return s;
        }
        public static string sGetStudyPermissionsBitLabel(UInt16 ABitNr)
        {
            string s = "?" + ABitNr.ToString() + "?";

            if (_sEnumListStudyPermissions != null)
            {
                foreach (CSqlEnumRow node in _sEnumListStudyPermissions.mGetEnumList())
                {
                    if (node._mGroup != 0)   // group =0: list of groups
                    {
                        int value = node.mGetIntValue();

                        if (value == ABitNr)
                        {
                            s = node._mLabel;
                            break;
                        }
                    }
                }
            }
            return s;
        }
        public static string sGetStudyPermissionsBitCodes(CBitSet64 ABitSet, string ASeperator, UInt16 AGroupOnly = 0)
        {
            string s = "";
            bool bAddSeperator = false;

            if (_sEnumListStudyPermissions != null && ABitSet != null && ABitSet.mbIsNotEmpty())
            {
                foreach (CSqlEnumRow node in _sEnumListStudyPermissions.mGetEnumList())
                {
                    if (node._mGroup != 0 && (AGroupOnly == 0 || AGroupOnly == node._mGroup))   // group =0: list of groups
                    {
                        int value = node.mGetIntValue();

                        if (ABitSet.mbIsBitSet((UInt16)value))
                        {
                            if (bAddSeperator)
                            {
                                s += ASeperator;
                            }
                            s += node._mCode;
                            break;
                        }
                    }
                }
            }
            return s;
        }
        public static string sGetStudyPermissionsBitLabels(CBitSet64 ABitSet, string ASeperator, UInt16 AGroupOnly = 0)
        {
            string s = "";
            bool bAddSeperator = false;

            if (_sEnumListStudyPermissions != null && ABitSet != null && ABitSet.mbIsNotEmpty())
            {
                foreach (CSqlEnumRow node in _sEnumListStudyPermissions.mGetEnumList())
                {
                    if (node._mGroup != 0 && (AGroupOnly == 0 || AGroupOnly == node._mGroup))   // group =0: list of groups
                    {
                        int value = node.mGetIntValue();

                        if (ABitSet.mbIsBitSet((UInt16)value))
                        {
                            if (bAddSeperator)
                            {
                                s += ASeperator;
                            }
                            s += node._mLabel;
                            bAddSeperator = true;
                        }
                    }
                }
            }

            return s;
        }

        public static bool sbEditStudyPermissionsBits(string AVarName, ref CBitSet64 ArBitSet, UInt16 AGroupOnly = 0)
        {
            bool bOk = false;

            bOk = ReqBitsForm.sbReqBits("Study Permissions", AVarName, ref ArBitSet, _sEnumListStudyPermissions, AGroupOnly);

            return bOk;
        }

        public static int sFillStudyPermissionsComboBox(ComboBox AComboBox, string AFirstLine, CBitSet64 ABitSet, UInt16 AGroupOnly)
        {
            int nList = 0;

            if (AComboBox != null)
            {
                String cursor = "";
                bool bFirst = true;

                AComboBox.Items.Clear();
                if (AFirstLine != null && AFirstLine.Length != 0)
                {
                    cursor = AFirstLine;
                    AComboBox.Items.Add(AFirstLine);
                }

                foreach (CSqlEnumRow node in _sEnumListStudyPermissions.mGetEnumList())
                {
                    if (node._mGroup != 0 && (AGroupOnly == 0 || AGroupOnly == node._mGroup))   // group =0: list of groups
                    {
                        string s = "";

                        if (AGroupOnly == 0)
                        {
                            s = _sEnumListStudyPermissions.mGetGroupName(node._mGroup) + ".";
                        }

                        s += node._mLabel;

                        if (bFirst && ABitSet != null)
                        {
                            int value = node.mGetIntValue();

                            if (ABitSet.mbIsBitSet((UInt16)value))
                            {
                                cursor = s;
                                bFirst = false;
                            }
                        }
                        AComboBox.Items.Add(s);
                        ++nList;

                    }
                }
                AComboBox.Text = cursor;
            }
            return nList;
        }
        public static bool sbSetStudyPermissionsComboBox(ComboBox AComboBox, CBitSet64 ABitSet, UInt16 AGroupOnly)
        {
            bool bChanged = false;

            if (AComboBox != null && AComboBox.Items != null && AComboBox.Items.Count > 0)
            {
                String newCursor = "";
                String oldCursor = AComboBox.Text;
                bool bFirst = true;

                foreach (CSqlEnumRow node in _sEnumListStudyPermissions.mGetEnumList())
                {
                    if (node._mGroup != 0 && (AGroupOnly == 0 || AGroupOnly == node._mGroup))   // group =0: list of groups
                    {
                        string s = "";

                        if (AGroupOnly == 0)
                        {
                            s = _sEnumListStudyPermissions.mGetGroupName(node._mGroup) + ".";
                        }

                        s += node._mLabel;

                        if (bFirst && ABitSet != null)
                        {
                            int value = node.mGetIntValue();

                            if (ABitSet.mbIsBitSet((UInt16)value))
                            {
                                newCursor = s;
                                bFirst = false;
                            }
                        }
                    }
                }
                if (bFirst)
                {
                    // no bits -> select first;
                    bChanged = AComboBox.SelectedIndex != 0;
                    if (bChanged)
                    {
                        AComboBox.SelectedIndex = 0;
                    }
                }
                else if (newCursor == oldCursor)
                {
                }
                else
                {
                    AComboBox.Text = newCursor;
                    bChanged = true;
                }
            }
            return bChanged;
        }
        public static CBitSet64 sGetStudyPermissionsComboBox(ComboBox AComboBox, UInt16 AGroupOnly = 0)
        {
            CBitSet64 result = new CBitSet64();


            if (AComboBox != null && result != null)
            {
                string value = AComboBox.Text;

                foreach (CSqlEnumRow node in _sEnumListStudyPermissions.mGetEnumList())
                {
                    if (node._mGroup != 0 && (AGroupOnly == 0 || AGroupOnly == node._mGroup))   // group =0: list of groups
                    {
                        string s = "";

                        if (AGroupOnly == 0)
                        {
                            s = _sEnumListStudyPermissions.mGetGroupName(node._mGroup) + ".";
                        }

                        s += node._mLabel;
                        if (value == s)
                        {
                            int bit = node.mGetIntValue();

                            result.mSetOneBit((UInt16)bit);
                            break;
                        }
                    }
                }
            }
            return result;
        }

        public static void sLogEnumList(CSqlEnumList AEnumList)
        {
            if (AEnumList != null)
            {
                AEnumList.mLogEnumList();
            }
        }
        public static bool sbLoadEnumListDb(ref CSqlEnumList ArEnumList, bool AbLoadOnlyOnEmpty, string AEnumTableName,
            bool AbLogOk, bool AbLogList)
        {
            bool bOk = false;
            bool bLoad = false == AbLoadOnlyOnEmpty;

            if (ArEnumList == null)
            {
                bLoad = true;
                ArEnumList = new CSqlEnumList(AEnumTableName);
            }
            if (ArEnumList != null)
            {
                bLoad |= ArEnumList.mCount() == 0;

                bOk = bLoad ? ArEnumList.mbLoadFromDb(_sDBaseServer, AbLogOk) : true;

                if (bLoad && AbLogList)
                {
                    ArEnumList.mLogEnumList();
                }
            }
            return bOk;
        }
        public static bool sbLoadEnumListFile(ref CSqlEnumList ArEnumList, bool AbLoadOnlyOnEmpty, string AEnumTableName, UInt16 ANrColumns, bool AbLog)
        {
            bool bOk = false;
            bool bLoad = false == AbLoadOnlyOnEmpty;

            if (ArEnumList == null)
            {
                bLoad = true;
                ArEnumList = new CSqlEnumList(AEnumTableName);
            }
            if (ArEnumList != null)
            {
                bLoad |= ArEnumList.mCount() == 0;
                bOk = bLoad ? ArEnumList.mbLoadFromFile(_sEnumFilePath, ANrColumns) : true;
                if (bLoad && AbLog)
                {
                    ArEnumList.mLogEnumList();
                }
            }

            return bOk;
        }
        public static string sGetStudyTypeLabel(string ACode)
        {
            return _sEnumListStudyType == null ? "" : _sEnumListStudyType.mGetLabel(ACode);
        }
        public static string sGetStudyTypeCode(string ALabel)
        {
            return _sEnumListStudyType == null ? "" : _sEnumListStudyType.mGetCode(ALabel);
        }
        public static string sGetReportIntervalLabel(string ACode)
        {
            return _sEnumListReportInterval == null ? "" : _sEnumListReportInterval.mGetLabel(ACode);
        }
        public static string sGetReportIntervalCode(string ALabel)
        {
            return _sEnumListReportInterval == null ? "" : _sEnumListReportInterval.mGetCode(ALabel);
        }
        public static string sGetMctIntervalLabel(string ACode)
        {
            return _sEnumListMctInterval == null ? "" : _sEnumListMctInterval.mGetLabel(ACode);
        }
        public static string sGetMctIntervalCode(string ALabel)
        {
            return _sEnumListMctInterval == null ? "" : _sEnumListMctInterval.mGetCode(ALabel);
        }
        public static string sGetCountryLabel(string ACode)
        {
            return _sEnumListCountries == null ? ACode : _sEnumListCountries.mGetLabel(ACode);
        }
        public static string sGetCountryCode(string ALabel)
        {
            return _sEnumListCountries == null ? "" : _sEnumListCountries.mGetCode(ALabel);
        }
        public static string sGetStateCode(string ALabel)
        {
            return _sEnumListStates == null ? "" : _sEnumListStates.mGetCode(ALabel);
        }
        public static string sGetStudyProcedureLabel(string ACode)
        {
            return _sEnumListStudyProcedures == null ? "" : _sEnumListStudyProcedures.mGetLabel(ACode);
        }
        public static string sGetStudyProcedureCode(string ALabel)
        {
            return _sEnumListStudyProcedures == null ? "" : _sEnumListStudyProcedures.mGetCode(ALabel);
        }

        public static bool sbParseBirthDate(out UInt32 ArYMD, string AYearText, UInt16 AAddYearBelow100, string AMonthText, string ADayText)
        {
            bool bOk = false;
            UInt32 ymd = 0;

            try
            {
                // a lot of test to protect entry of invalid date time

                UInt16 day = 0, month = 0, year = 0;

                if (CProgram.sbParseYear(AYearText, out year, AAddYearBelow100)
                    && CProgram.sbParseDay(ADayText, out day))
                {
                    if (day == 0 || year == 0)
                    {
                        if (day == 0 && year == 0)
                        {
                            ymd = 0;
                            bOk = true;
                        }
                    }
                    else
                    {
                        bOk = UInt16.TryParse(AMonthText, out month)
                         || _sEnumListMonthName != null
                            && UInt16.TryParse(_sEnumListMonthName.mGetCode(AMonthText), out month);

                        if (bOk)
                        {
                            bOk = month > 0 && month <= 12;

                            if (bOk)
                            {
                                DateTime dt = new DateTime(year, month, day, 0, 0, 0, DateTimeKind.Local);

                                // if not a valid date time the exception will return false
                                bOk = dt != null;
                                ymd = CProgram.sCalcYMD(year, month, day);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bOk = false;
            }
            ArYMD = ymd;

            return bOk;
        }



        public static string sGetPrintStudyLabel()
        {
            string s = _sPrintStudyLabel;

            if (s == null || s.Length == 0)
            {
                s = "Study";
            }
            return s;
        }
        public static string sGetPrintStudyCode()
        {
            string s = _sPrintStudyCode;

            if (s == null || s.Length == 0)
            {
                s = "S";
            }
            return s;
        }
        public static void sSetPrintStudyLabel(string AStudyLabel, string AStudyCode)
        {
            _sPrintStudyLabel = AStudyLabel;
            _sPrintStudyCode = AStudyCode;

        }
        public static Color sGetPrintColor(DChartDrawTo ADrawTo, Color AColor)
        {
            Color color = AColor;

            if (ADrawTo == DChartDrawTo.Print_BW)
            {
                int sum = color.R;
                int min = sum;

                sum += color.G;
                if (min > color.G) min = color.G;
                sum += color.B;
                if (min > color.B) min = color.B;
                sum += min;
                sum = (sum + 2) / 4;
                color = Color.FromArgb(sum, sum, sum);
            }
            return color;
        }

        public static Color sGetGridColor(DChartDrawTo ADrawTo)
        {
            return ADrawTo == DChartDrawTo.Display ? _sDisplayGridColor : sGetPrintColor(ADrawTo, _sPrintGridColor);
        }

        public static Color sGetSignalColor(DChartDrawTo ADrawTo)
        {
            return ADrawTo == DChartDrawTo.Display ? _sDisplaySignalColor : sGetPrintColor(ADrawTo, _sPrintSignalColor);
        }
        public static float sGetSignalWidth(DChartDrawTo ADrawTo)
        {
            return ADrawTo == DChartDrawTo.Display ? _sDisplaySignalWidth : _sPrintSignalWidth;
        }

        private static void sPrintCenterLoad()
        {
            if (false == _sbPrintCenterLoaded)
            {
                _sPrintCenterLine1 = "";
                _sPrintCenterLine2 = "";
                _sPrintCenterImage = null;
                _sPrintCenterImage_BW = null;


                try
                {
                    string fileName = Path.Combine(CProgram.sGetProgDir(), "PrintCenter.png");
                    if (File.Exists(fileName))
                    {
                        _sPrintCenterImage = Image.FromFile(fileName);
                    }

                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed to load PrintCenter.png", ex);
                }
                _sPrintCenterImage_BW = _sPrintCenterImage;
                try
                {
                    string fileName = Path.Combine(CProgram.sGetProgDir(), "PrintCenterBW.png");
                    if (File.Exists(fileName))
                    {
                        _sPrintCenterImage_BW = Image.FromFile(fileName);
                    }

                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed to load PrintCenter.png", ex);
                }


                try
                {
                    string fileName = Path.Combine(CProgram.sGetProgDir(), "PrintCenter.txt");
                    if (File.Exists(fileName))
                    {

                        string[] lines = File.ReadAllLines(fileName);

                        if (lines != null)
                        {
                            if (lines.Length >= 1) _sPrintCenterLine1 = lines[0];
                            if (lines.Length >= 2) _sPrintCenterLine2 = lines[1];
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed to load PrintCenter.txt", ex);
                }
                _sbPrintCenterLoaded = true;
            }
        }
        public static string sGetPrintCenterLine1()
        {
            sPrintCenterLoad();
            return _sPrintCenterLine1;
        }
        public static string sGetPrintCenterLine2()
        {
            sPrintCenterLoad();
            return _sPrintCenterLine2;
        }
        public static Image sGetPrintCenterImage(bool AbBlackWhite)
        {
            sPrintCenterLoad();
            return AbBlackWhite ? _sPrintCenterImage_BW : _sPrintCenterImage;
        }

        private static void sCheckSumLine(ref UInt32 ArCheckSum, string ALine, UInt16 AXorIndex)
        {
            int n = ALine == null ? 0 : ALine.Length;

            for (int i = 0; i < n; ++i)
            {
                char c = ALine[i];
                UInt32 v = ((UInt32)c) << ((i ^ AXorIndex) & 0xF);
                ArCheckSum += v;
            }
        }

        public static bool sbLoadUserInitialIdNrList(bool AbForceRead, bool AbChecksum)
        {
            bool bOk = _sUserInitialNrList != null;
            string centerName = CProgram.sGetOrganisationLabel();

            if (AbForceRead || false == bOk)
            {
                if (_sRecordingDir == null || _sRecordingDir.Length == 0)
                {
                    CProgram.sLogLine("Recording dir not set ");
                }
                else if (centerName == null || centerName.Length == 0)
                {
                    CProgram.sLogLine("Center name not set ");
                }
                else
                {
                    string filePath = Path.Combine(_sRecordingDir, "..", centerName + ".UserIDs");

                    int n;
                    UInt32 nUsers = 0;
                    UInt32 checkSum = 0x4F72B135;
                    UInt32 fileCheckSum = 0;

                    _sUsersCount = 0;
                    _sUsersLastID = 0;
                    try
                    {
                        if (false == File.Exists(filePath))
                        {
                            filePath = Path.Combine(_sRecordingDir, centerName + ".UserIDs");
                        }
                        _sUserInitialNrList = File.ReadAllLines(filePath);
                        if (_sUserInitialNrList != null)
                        {
                            n = _sUserInitialNrList == null ? 0 : _sUserInitialNrList.Length;

                            sCheckSumLine(ref checkSum, "UID_"+ centerName, 2);

                            for (int i = 0; i < n; ++i)
                            {
                                string line = _sUserInitialNrList[i];

                                if (line != null && line.Length > 0)
                                {
                                    string[] parts = line.Split('=');

                                    if (line[0] == '$')
                                    {
                                        if (line.Length > 1)
                                        {
                                            UInt32.TryParse(line.Substring(1), out fileCheckSum);
                                        }
                                        break;
                                    }
                                    sCheckSumLine(ref checkSum, line, 2);

                                    if (parts != null && parts.Length >= 2 && parts[0].Length > 0 && Char.IsLetterOrDigit(parts[0][0]))
                                    {
                                        ++nUsers;

                                        UInt32 id = 0;

                                        if (UInt32.TryParse(parts[1], out id))
                                        {
                                            if (id > _sUsersLastID)     // 1 = programmer
                                            {
                                                _sUsersLastID = id; // determin last id for add user
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        bOk = nUsers > 0;
                        _sUsersCount = nUsers;

                        if (bOk && AbChecksum)
                        {
                            bOk = checkSum == fileCheckSum;

                            if (false == bOk)
                            {
                                CProgram.sPromptError(false, "Read user list " + nUsers.ToString(), "Checksum error file, fix in i-reader!");
                            }
                        }
                        if (bOk)
                        {
                            CProgram.sLogLine("Read user initial & ID list, found users: " + nUsers.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Failed to read " + filePath, ex);
                    }
                }
            }
            if (false == bOk)
            {
                _sUsersCount = 0;
                _sUserInitialNrList = null;
            }
            return bOk;
        }

        public static bool sbWriteUserInitials(List<string> ALines)
        {
            bool bOk = false;
            List<string> storeLines = new List<string>();


            if (ALines != null && storeLines != null)
            {
                string centerName = CProgram.sGetOrganisationLabel();

                if (_sRecordingDir == null || _sRecordingDir.Length == 0)
                {
                    CProgram.sLogLine("Recording dir not set ");
                }
                else if (centerName == null || centerName.Length == 0)
                {
                    CProgram.sLogLine("Center name not set ");
                }
                else
                {
                    string fileName = centerName + ".UserIDs";
                    string filePath = Path.Combine(_sRecordingDir, "..", fileName);
                    string firstFilePath = filePath;

                    int n, nUsers = 0;
                    UInt32 checkSum = 0x4F72B135;
                    bool bExists = false;

                    try
                    {
                        bExists = File.Exists(filePath);
                        if (false == bExists)
                        {
                            filePath = Path.Combine(_sRecordingDir, fileName);
                            bExists = File.Exists(filePath);
                        }
                        if (bExists)
                        {
                            string userPath;

                            if (sbGetUsersPath(out userPath))
                            {
                                string toFileName = "old" + CProgram.sDateTimeToYMDHMS(DateTime.Now) + "_" + fileName;
                                string toFilePath = Path.Combine(userPath, toFileName);

                                try
                                {
                                    File.Move(filePath, toFilePath);    // make safety backup
                                }
                                catch (Exception ex)
                                {
                                    CProgram.sLogException("Failed move to " + toFileName, ex);
                                }
                            }
                        }
                        else
                        {
                            filePath = firstFilePath;
                        }
                        n = ALines == null ? 0 : ALines.Count;

                        sCheckSumLine(ref checkSum, "UID_" + centerName, 2);

                        for (int i = 0; i < n; ++i)
                        {
                            string line = ALines[i];

                            if (line != null && line.Length > 0)
                            {
                                line = line.Trim();
                            }
                            if (line != null && line.Length > 0)
                            {

                                if (line[0] != '$' && line[0] != '@' )
                                {
                                    string[] parts = line.Split('=');

                                    sCheckSumLine(ref checkSum, line, 2);
                                    storeLines.Add(line.Trim());

                                    if (parts != null && parts.Length >= 2 && parts[0].Length > 0 && Char.IsLetterOrDigit(parts[0][0]))
                                    {
                                        ++nUsers;
                                    }
                                }
                            }
                        }
                        bOk = nUsers > 0;

                        if (bOk)
                        {
                            string atLine = "@" + CProgram.sGetProgNameVersionNow();
                            sCheckSumLine(ref checkSum, atLine, 2);
                            storeLines.Add(atLine);
                            storeLines.Add("$" + checkSum.ToString());
                            File.WriteAllLines(filePath, storeLines);
                            CProgram.sLogLine("Write user initial & ID list, found users: " + nUsers.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Failed to write " + filePath, ex);
                        bOk = false;
                    }
                }
            }
            if (bOk)
            {
                _sUserInitialNrList = null; // reread on next request
            }
            return bOk;
        }


        public static bool sbLoadUserInitialIdNrList(ComboBox.ObjectCollection AComboList, bool AbForceRead, bool AbChecksum)
        {
            bool bOk = sbLoadUserInitialIdNrList(AbForceRead, AbChecksum);

            AComboList.Clear();
            if (bOk && _sUserInitialNrList != null)
            {
                int n = _sUserInitialNrList.Length;
                int nUsers = 0;
                try
                {

                    for (int i = 0; i < n; ++i)
                    {
                        string line = _sUserInitialNrList[i];
                        string[] parts = line.Split('=');

                        if (parts != null && parts.Length >= 2 && parts[0].Length > 0 && Char.IsLetterOrDigit(parts[0][0]))
                        {
                            AComboList.Add(parts[0]);
                            ++nUsers;
                        }
                    }
                    bOk = nUsers > 0;
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed to fill combo box", ex);
                }
            }
            return bOk;
        }
        public static bool sbSelectUserInitialIdNrList(string ATitle, ref string ArUserInitials, bool AbForceRead, bool AbChecksum)
        {
            bool bOk = sbLoadUserInitialIdNrList(AbForceRead, AbChecksum);
            List<string> userList = new List<string>();
            string userInitials = ArUserInitials == null ? "" : ArUserInitials;

            if (bOk && _sUserInitialNrList != null && userList != null)
            {
                int n = _sUserInitialNrList.Length;
                int nUsers = 0;
                UInt16 index = 0;

                try
                {
                    for (int i = 0; i < n; ++i)
                    {
                        string line = _sUserInitialNrList[i];
                        string[] parts = line.Split('=');

                        if (parts != null && parts.Length >= 2 && parts[0].Length > 0 && Char.IsLetterOrDigit(parts[0][0]))
                        {
                            userList.Add(parts[0]);
                            if (parts[0] == userInitials)
                            {
                                index = (UInt16)nUsers;
                            }
                            ++nUsers;
                        }
                    }
                    bOk = nUsers > 0;
                    if (bOk)
                    {
                        bOk = ReqEnumForm.sbReqEnumFromSqlList(ATitle + ": Select user", "User", userList, ref index, "");

                        if (bOk && index < nUsers)
                        {
                            ArUserInitials = userList[index];
                        }
                    }

                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed to fill combo box", ex);
                }
            }
            return bOk;
        }

        public static bool sbFindUserInitialIdNr(ref string AUserInitials, out UInt32 AUserIdNr)
        {
            bool bOk = false;
            UInt32 userIdNr = 0;
            string userInitials = AUserInitials;

            if (userInitials != null)
            {
                userInitials = userInitials.Trim();
            }

            if (_sUserInitialNrList != null && userInitials != null && userInitials.Length >= 1)
            {
                string searchInitials = userInitials.ToUpper();
                try
                {
                    int n = _sUserInitialNrList == null ? 0 : _sUserInitialNrList.Length;

                    for (int i = 0; i < n; ++i)
                    {
                        string line = _sUserInitialNrList[i];
                        string[] parts = line.Split('=');

                        if (parts != null && parts.Length >= 2)
                        {
                            string initials = parts[0].Trim();

                            if (searchInitials == initials.ToUpper())
                            {
                                UInt32 id = 0;

                                if (UInt32.TryParse(parts[1], out id))
                                {
                                    if (id > 1)     // 1 = programmer
                                    {
                                        userIdNr = id;
                                        userInitials = initials;    // use the entry from the file to get correct capitals
                                        bOk = true;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    if (bOk)
                    {
                        CProgram.sLogLine("Found user initial " + userInitials + "= " + userIdNr.ToString());
                    }
                    else
                    {
                        CProgram.sLogLine("Failed to find user initial " + userInitials);
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed to find user" + userInitials, ex);
                }
            }
            AUserInitials = userInitials;
            AUserIdNr = userIdNr;
            return bOk;
        }

        public static string sFindUserInitial(UInt32 AUserIdNr)
        {
            string userInitials = "";

            if (_sUserInitialNrList != null && AUserIdNr > 0)
            {
                if (AUserIdNr == sUserInitialsID)
                {
                    userInitials = sUserInitals;    // if last id give quick responce
                }
                else
                {
                    try
                    {
                        int n = _sUserInitialNrList == null ? 0 : _sUserInitialNrList.Length;

                        for (int i = 0; i < n; ++i)
                        {
                            string line = _sUserInitialNrList[i];
                            string[] parts = line.Split('=');

                            if (parts != null && parts.Length >= 2)
                            {
                                string initials = parts[0].Trim();

                                UInt32 id = 0;

                                if (UInt32.TryParse(parts[1], out id))
                                {
                                    if (id > 1 && id == AUserIdNr)     // 1 = programmer
                                    {
                                        userInitials = initials;    // use the entry from the file to get correct capitals

                                        sUserInitals = userInitials;    // cash last value
                                        sUserInitialsID = AUserIdNr;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Failed to find user IX" + AUserIdNr, ex);
                    }
                }
            }
            return userInitials;
        }

        public static string sGetChangedBy(DateTime AUtc, UInt32 AUserIdNr)
        {
            string line = "";

            if (AUtc == DateTime.MinValue)
            {
                line += "-";
            }
            else if (AUtc == DateTime.MaxValue)
            {
                line += "-";
            }
            else
            {
                line += CProgram.sUtcToLocalString(AUtc);
            }
            if (AUserIdNr > 0)
            {
                string userInitials = sFindUserInitial(AUserIdNr);

                if (userInitials == null || userInitials.Length == 0)
                {
                    userInitials = "?" + AUserIdNr.ToString();
                }
                line += " " + userInitials;
            }
            return line;
        }

        public static bool sbUserUseNoList()
        {
            bool bOk = false;

            if( false == _sbUserListEnfored)
            {
//                CProgram.sPromptWarning(true, "Select User", "User List not present, setup in i-BinaryReader!");
                bOk = true;
            }
            return bOk;
        }
        public static bool sbUserCheckPin( ref string ArPin, string ATitle)
        {
            bool bOk = false;
            string pin = ArPin == null ? "" : ArPin.Trim();
            int n = pin == null ? 0 : pin.Length;
            
            if( n >= _sUserPinLength)
            {
                bOk = true;
            }
            else
            {
                CProgram.sPromptWarning(true, ATitle, "User Pin length to short!");
            }
            ArPin = pin;
            return bOk;
        }

        public static bool sbCheckUserInitials(UInt32 AUserIX, string AUserInitials, string APin)
        {
            bool bOk = false;

            if (APin != null && APin.Length > 0 && AUserIX > 0 && AUserInitials != null && AUserInitials.Length > 0)
            {

            }
            return bOk;
        }
        public static bool sbGetUserPwFileName(UInt32 AUserIX, string AUserInitials, out string ArPwFileName)
        {
            bool bOk = false;
            string fileName = "";

            if (AUserIX > 0 && AUserInitials != null && AUserInitials.Length > 0)
            {
                fileName = "UID" + AUserIX.ToString("000000")+ "_";
                int n = AUserInitials.Length;
                for (int i = 0; i < n; ++i)
                {
                    char c = AUserInitials[i];

                    fileName += Char.IsLetterOrDigit(c) ? c : '_';
                }
                bOk = true;
            }
            ArPwFileName = fileName;
            return bOk;
        }

        public static bool sbGetUsersPath(out string ArUserPath)
        {
            bool bOk = false;
            string dataPath = sGetDataDir();
            string userPath = "";

            if (dataPath != null && dataPath.Length > 0)
            {
                userPath = Path.Combine(dataPath, "Users");

                bOk = sbCreateDir(userPath);
            }
            ArUserPath = userPath;
            return bOk;
        }
        public static bool sbGetUserPwFile(UInt32 AUserIX, string AUserInitials, out string ArPwFile)
        {
            bool bOk = false;
            string pwFile = "";
            string userPath;
            string fileName;

            if (sbGetUsersPath(out userPath)
                && AUserIX > 0 && AUserInitials != null && AUserInitials.Length > 0
                && sbGetUserPwFileName(AUserIX, AUserInitials, out fileName))

            {
                bOk = true;
                fileName += ".UserPW";
                pwFile = Path.Combine(userPath, fileName);
            }
            ArPwFile = pwFile;
            return bOk;
        }

        public static bool sbReadUserPW(UInt32 AUserIX, string AUserInitials, out string ArPin)
        {
            bool bOk = false;
            string pw = "";
            string pwFile;
            string name;

            try
            {
                if (sbGetUserPwFile(AUserIX, AUserInitials, out pwFile)
                && sbGetUserPwFileName(AUserIX, AUserInitials, out name))
                {
                    if (File.Exists(pwFile))
                    {
                        try
                        {
                            string encryptedPW = File.ReadAllText(pwFile);

                            if (encryptedPW != null && encryptedPW.Length > 0)
                            {
                                string centerName = CProgram.sGetOrganisationLabel();
                                name += "_pin_" + centerName;

                                CPasswordString encrypted = new CPasswordString(name, centerName, 16);

                                if (encrypted != null)
                                {
                                    if (encrypted.mbSetEncrypted(encryptedPW))
                                    {
                                        pw = encrypted.mDecrypt();
                                        bOk = true;
                                    }
                                    else
                                    {
                                        CProgram.sPromptWarning(true, "Check User " + AUserInitials, "Password not ok, reset in IReader");
                                    }
                                }
                            }

                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        CProgram.sPromptWarning(true, "Check User " + AUserInitials, "Password not initialized, setup in IReader");
                        bOk = false;
                    }
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed Read user pw", ex);
            }
            ArPin = pw;
            return bOk;
        }

        public static bool sbWriteUserPW(UInt32 AUserIX, string AUserInitials, string APin)
        {
            bool bOk = false;
            string pw = APin != null ? APin.Trim() : "";
            string pwFile;
            string name;

            try
            {
                if (sbGetUserPwFile(AUserIX, AUserInitials, out pwFile)
                && sbGetUserPwFileName(AUserIX, AUserInitials, out name)
                && sbUserCheckPin( ref pw, "Write pin " + AUserInitials))
                {
                    try
                    {
                        string centerName = CProgram.sGetOrganisationLabel();
                        name += "_pin_" + centerName;

                        CPasswordString encrypted = new CPasswordString(name, centerName, 16);
                        if (encrypted != null)
                        {
                            if (encrypted.mbEncrypt(pw))
                            {
                                string encryptedPW = encrypted.mGetEncrypted();
                                File.WriteAllText(pwFile, encryptedPW);
                                CProgram.sLogLine("Pin file creaded for user " + AUserInitials);
                                bOk = true;
                            }
                            else
                            {
                                CProgram.sPromptWarning(true, "Set User pin", "Pin not saved");

                            }
                        }
                    }
                    catch (Exception)
                    {
                        CProgram.sPromptWarning(true, "Set User pin", "Pin not saved");

                    }
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed Read user pin", ex);
            }
            return bOk;
        }

        public static bool sbSetUserInitials(string AUserInitials, string APin)
        {
            UInt32 userID = 0;
            string pw = APin;
            string userInitials = AUserInitials == null ? "" : AUserInitials.Trim();
            bool bOk = userInitials != null && userInitials.Length > 0
                && sbUserCheckPin(ref pw, "Select user " + userInitials);

            if (bOk)
            {
                bOk = false;
                if (_sUserInitialNrList == null)
                {
                    if (sbUserUseNoList())
                    {
                        CProgram.sSetProgUser(0, userInitials, userInitials, APin);
                        CProgram.sLogLine("Set user direct= " + userInitials);
                        bOk = true;
                    }
                }
                else
                {
                    if (sbFindUserInitialIdNr(ref userInitials, out userID))
                    {
                        string filePin;
                        if (sbReadUserPW(userID, userInitials, out filePin))
                        {
                            bOk = filePin == APin;
                            if (bOk)
                            {
                                CProgram.sSetProgUser(userID, userInitials, userInitials, APin);
                                CProgram.sLogLine("Set user from list= " + userInitials);
                            }
                        }
                    }
                }
            }
            if( false == bOk)
            {
                CProgram.sSetProgUser(0, "", "", "");   // reset user.
            }
            return bOk;
        }

        public static bool sbChangeUserPW()
        {
            bool bOk = false;

            if (CProgram.sbEnterProgUserArea(true))
            {
                UInt32 userIX = CProgram.sGetProgUserIX();
                string userInitials = CProgram.sGetProgUserInitials();

                if (userIX == 0 || userInitials == null || userInitials.Length == 0)
                {
                    CProgram.sPromptWarning(true, "Change User pin", "No user active");

                }
                else
                {
                    string pw = "";

                    if (CProgram.sbReqLabel("Change user " + userInitials, "old pin", ref pw, "", true))
                    {
                        if (_sUserInitialNrList == null)
                        {
                            if (sbUserUseNoList())
                            {
                                CProgram.sSetProgUser(0, userInitials, userInitials, pw);
                                CProgram.sLogLine("Set pw for user = " + userInitials);
                            }
                        }
                        else
                        {
                            UInt32 userID;
                            if (sbFindUserInitialIdNr(ref userInitials, out userID))
                            {
                                if (userID != userIX && userID > 0)
                                {
                                    CProgram.sPromptWarning(true, "Change pin for user: " + userInitials, "Incorrect user number");
                                }
                                else
                                {
                                    string filePin;
                                    if (sbReadUserPW(userID, userInitials, out filePin))
                                    {
                                        if (filePin == pw)
                                        {
                                            pw = "";

                                            if (CProgram.sbReqLabel("Change user " + userInitials, "new pin", ref pw, "", true))
                                            {
                                                if (sbUserCheckPin(ref pw, "Change user pin"))
                                                {
                                                    if (sbWriteUserPW(userID, userInitials, pw))
                                                    {
                                                        CProgram.sSetProgUser(userID, userInitials, userInitials, pw);
                                                        CProgram.sAskOk("Change pin for user: " + userInitials, "pin changed");
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            CProgram.sPromptWarning(true, "Change User pin", "Old pin not ok!");

                                        }
                                    }
                                }
                            }
                            else
                            {
                                CProgram.sPromptWarning(true, "Change User pin", "No user number");
                            }
                        }
                    }
                }
            }
            return bOk;
        }

        public static bool sbCreateUserPW(string AUserInitials, UInt32 AUserIX = 0)
        {
            bool bOk = false;
            UInt32 userIX = AUserIX;
            string userInitials = AUserInitials == null ? "" : AUserInitials;

            if (userInitials != null && userInitials.Length > 0
                || sbSelectUserInitialIdNrList("Create pin", ref userInitials, true, false))

            {
                if (_sUserInitialNrList == null)
                {
                    CProgram.sPromptWarning(true, "Create user " + userInitials, "No user list");
                }
                else
                {
                    if (userIX > 0 || sbFindUserInitialIdNr(ref userInitials, out userIX) && userIX > 1)
                    {
                        string pw = "";
                        if (CProgram.sbReqLabel("Create user " + userInitials, "pin", ref pw, "", true))
                        {
                            if (sbUserCheckPin(ref pw, "Create user pin"))
                            {
                                if (sbWriteUserPW(userIX, userInitials, pw))
                                {
                                    //CProgram.sSetProgUser(userIX, userInitials, userInitials, pw);
                                    CProgram.sAskOk("Create pin for user: " + userInitials, "pin created");
                                    bOk = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        CProgram.sPromptWarning(true, "Create pin for user: " + userInitials, "No user number");
                    }
                }
            }
            return bOk;
        }

        public static bool sbCreateUserIX()
        {
            bool bOk = false;

            if (sbLoadUserInitialIdNrList(true, false))
            {
                UInt32 userIX = _sUsersLastID + 1;
                string userInitials = "";
                if (CProgram.sbReqLabel("Create User", "Initials", ref userInitials, "", false)
                && CProgram.sbReqUInt32("Create User", "number", ref userIX, "", 2, 999999))
                {
                    List<string> lines = new List<string>();

                    if (lines != null)
                    {
                        bool bCont = true;
                        string searchInitials = userInitials.ToLower();

                        try
                        {
                            int n = _sUserInitialNrList == null ? 0 : _sUserInitialNrList.Length;

                            for (int i = 0; i < n; ++i)
                            {
                                string line = _sUserInitialNrList[i];

                                if (line != null && line.Length > 0 && line[0] != '$')
                                {
                                    string[] parts = line.Split('=');

                                    lines.Add(line);

                                    if (parts != null && parts.Length >= 2)
                                    {
                                        string initials = parts[0].Trim().ToLower();

                                        if (initials == searchInitials)
                                        {
                                            CProgram.sPromptWarning(true, "Create User " + userInitials, "User already exists");
                                            bCont = false;
                                            break;
                                        }
                                        UInt32 id = 0;

                                        if (UInt32.TryParse(parts[1], out id))
                                        {
                                            if (id == userIX)
                                            {
                                                CProgram.sPromptWarning(true, "Create User " + userInitials, "User numer already exists");
                                                bCont = false;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            if (bCont)
                            {
                                string newLine = userInitials + "=" + userIX.ToString();
                                lines.Add(newLine);
                                // write 
                                if (sbCreateUserPW(userInitials, userIX))
                                {
                                    bOk = sbWriteUserInitials(lines);
                                    if( bOk )
                                    {
                                        CProgram.sAskOk("Create User " + userInitials, "User created and added to user list");
                                    }
                                    else
                                    {
                                        CProgram.sPromptWarning(true, "Create User " + userInitials, "User not added to list");
                                    }
                                }
                                else
                                {
                                    bOk = false;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("Failed create user IX" + userInitials, ex);
                        }
                    }
                }
            }
            return bOk;
        }

        public static bool sbUserListRewrite()
        {
            bool bOk = sbLoadUserInitialIdNrList(true, false);
            List<string> lines = new List<string>();

            if ( bOk && _sUserInitialNrList != null && lines != null)
            {
                int n = _sUserInitialNrList.Length;
                string text = "User List:\r\n";

                foreach (string line in _sUserInitialNrList)
                {
                    text += line + "\r\n";
                    lines.Add(line);
                }

                if( ReqTextForm.sbShowText( "Rewite User List", "Rewite User List", text ))
                {
                    bOk = sbWriteUserInitials(lines);

                    if( bOk )
                    {
                        CProgram.sAskOk("Rewrite User List", "Rewrite User list succeded");
                    }
                    else
                    {
                        CProgram.sPromptError(true, "Rewrite User list", "Failed, check file user file!");
                    }
                }
            }
            else
            {
                CProgram.sPromptError(true, "Rewrite User list", "Failed reading user list");
            }
            return bOk;
        }
        public static bool sbUserListTest()
        {
            string userInitials = "";
            bool bOk = sbSelectUserInitialIdNrList("Test User List", ref userInitials, true, true);

            if (bOk)
            {
                UInt32 userIX = 0;

                bOk = sbFindUserInitialIdNr(ref userInitials, out userIX);

                if (bOk && userIX > 1)
                {
                    CProgram.sAskOk("Test OK User " + userInitials, "Found User number " + userIX.ToString());
                }
                else
                {
                    CProgram.sPromptError(true, "Test User " + userInitials, "Failed, user not found!");
                }
            }
            else
            {
                CProgram.sPromptError(true, "Test User list", "Failed reading user list");
            }
            return bOk;
        }

        public static Image sLoadPrintPhysicianTxtImg(out string ArText, UInt32 AUserIX, string AUserInitials, string ATypeName)
        {
            Image bmp = null;
            string text = "";
            string pwFile, name;


            try
            {
                if (sbGetUserPwFile(AUserIX, AUserInitials, out pwFile)
                && sbGetUserPwFileName(AUserIX, AUserInitials, out name))
                {
                    string txtFile = Path.ChangeExtension(pwFile, ATypeName+".txt");
                    if (File.Exists(txtFile))
                    {
                        text = File.ReadAllText(txtFile);
                    }

                    string imgFile = Path.ChangeExtension(pwFile, ATypeName + ".png");
                    if (File.Exists(imgFile))
                    {
                        bmp = new Bitmap(imgFile); // Bitmap.FromFile(imgFile);
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed load user img", ex);
            }
            ArText = text;
            return bmp;
        }

        public static Bitmap mResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        public static void sFillPrintPhysician(UInt32 AUserIX, string AUserInitials, string ATypeName, 
            UInt16 AViewMode,// 0 = off, 1 = label, 2 = text, 3 = img, 4 = both, 5 = line
            String ALabelText, bool AbQC, Label APhysiscianLabel, Label APhysicianText)
        {
            
            try
            {
                string text = "";
                string physLabel = _sPrintPhysicianLabel;
                Image img = sLoadPrintPhysicianTxtImg(out text, AUserIX, AUserInitials, ATypeName);
                string label = (AbQC ? "QC\r\n" : "");

                if(_sPrintPhysicianLabel != null && _sPrintPhysicianLabel.Length > 0)
                {
                    label += _sPrintPhysicianLabel + " ";
                }
                label += ALabelText + ":";

                if (APhysiscianLabel != null)
                {
                    if ( AViewMode == 0) label = "";
                    APhysiscianLabel.Text = label;  // hide label
                }

                if (APhysicianText != null)
                {
                    if( AViewMode == 5 ) text = "_________";
                    if (AViewMode != 2 && AViewMode != 4 ) text = "";
                    if (AViewMode != 3 && AViewMode != 4) img = null;

                    APhysicianText.Visible = false;
                    APhysicianText.Text = text;
                    APhysicianText.BackgroundImage = mResizeImage(img, APhysicianText.Width, APhysicianText.Height);
                    APhysicianText.Visible = true;

                    APhysicianText.Update();
                    APhysicianText.Refresh();
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed init print physician", ex);
            }

        }

        public static void sFillPrintPhysicianLabels(UInt32 AUserIX, string AUserInitials, string APrintPageTitle, bool AbQC,
            Label APhysiscianNameLabel, Label APhysicianNameText,
            Label APhysiscianSignLabel, Label APhysicianSignText,
            Label APhysicianDateLabel, Label APhysicianDateText
            )
        {
            UInt32 userIX = AUserIX;
            string userInitials = AUserInitials;
            string date = CProgram.sDateToString(DateTime.Now);

            try
            {
                string extraLabel = "";

                if( _sPrintPhysicianLabel != null && _sPrintPhysicianLabel.Length > 0 && _sPrintPhysicianLabel != "Phycician")
                {
                    extraLabel = "(" + _sPrintPhysicianLabel + ")";
                }
                if (_sbPrintPhysicianAskName)
                {
                    if (CProgram.sbReqUInt32("Change physician"+ extraLabel + " on print " + APrintPageTitle, "Physician number", ref userIX, "", 0, 999999))
                    {
                        if (CProgram.sbReqLabel("Change physician" + extraLabel + " on print " + APrintPageTitle, "Physician initials", ref userInitials, "", false))
                        {

                        }
                        else
                        {
                            userIX = AUserIX;
                        }
                    }
                }
                // _sPrintPhysicianDateMode 0 = off, 1 = label, 2 = today, , 3 = line, 99 = ask
                if (_sPrintPhysicianDateMode <= 1) date = "";
                else if (_sPrintPhysicianDateMode == 99)
                {
                    CProgram.sbReqLabel("Change date on print " + APrintPageTitle, "Date", ref date, "", false);
                }
                else if (_sPrintPhysicianDateMode == 3) date = "_________";


                sFillPrintPhysician(userIX, userInitials, "name", _sPrintPhysicianNameMode, "name", AbQC, APhysiscianNameLabel, APhysicianNameText);
                sFillPrintPhysician(userIX, userInitials, "sign", _sPrintPhysicianSignMode, "signature", AbQC, APhysiscianSignLabel, APhysicianSignText);

                if (APhysicianDateLabel != null)
                {
                    APhysicianDateLabel.Text = _sPrintPhysicianDateMode == 0 ? "" : "Date";

                }
                if (APhysicianDateText != null)
                {
                    APhysicianDateText.Text = date;
                }

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed init print physician", ex);
            }
        }
        public static Color sPrintClassificationColor(string AClassification, bool AbBlackWhite)
        {
            Color color = Color.Black;

            if (AClassification != null)
            {
                switch (AClassification)
                {
                    case _cFindings_Tachy: color = AbBlackWhite ? Color.Gainsboro : Color.FromArgb(255, 128, 0); break;
                    case _cFindings_Brady: color = AbBlackWhite ? Color.DarkGray : Color.Green; break;
                    case _cFindings_Pause: color = AbBlackWhite ? Color.Gray : Color.Gray; break;
                    case _cFindings_AF: color = AbBlackWhite ? Color.Silver : Color.Blue; break;
                    case _cFindings_BL: color = AbBlackWhite ? Color.LightGray : Color.LightGreen; break;
                    case _cFindings_Other: color = AbBlackWhite ? Color.Black : Color.RosyBrown; break;
                    case _cFindings_PAC: color = AbBlackWhite ? Color.DimGray : Color.DimGray; break;
                    case _cFindings_PVC: color = AbBlackWhite ? Color.Silver : Color.Gray; break;
                    case _cFindings_Pace: color = AbBlackWhite ? Color.Silver : Color.Silver; break;
                    case _cFindings_VT: color = AbBlackWhite ? Color.Gainsboro : Color.Gainsboro; break;
                    case _cFindings_VF: color = AbBlackWhite ? Color.LightSlateGray : Color.Orange; break;
                    case _cFindings_AFL: color = AbBlackWhite ? Color.White : Color.White; break;
                    case _cFindings_ASys: color = AbBlackWhite ? Color.LightGray : Color.Red; break;
                    case _cFindings_Manual: color = AbBlackWhite ? Color.Black : Color.Black; break;
                    case _cFindings_Normal: color = AbBlackWhite ? Color.Black : Color.DarkGray; break;
                    case _cFindings_AbNormal: color = AbBlackWhite ? Color.Black : Color.Cyan; break;
                }
            }
            return color;
        }
        public static UInt16 sFindNthChar(string AString, char AChar, UInt16 ANthChar)
        {
            int pos = 0;
            int len = AString == null ? 0 : AString.Length;

            if (len > 0 && ANthChar > 0)
            {
                UInt16 n = 0;

                for (int i = 0; ++i < len;)
                {
                    if (AString[i] == AChar)
                    {
                        if (++n == ANthChar)
                        {
                            pos = i;
                            break;
                        }
                    }
                }
            }

            return (UInt16)pos;
        }

        public static UInt32 sSortWipeDoubleFiles(ref string[] ArFiles, UInt16 ANrStartChars)
        {
            Int32 nFiles = (Int32)(ArFiles == null ? 0 : ArFiles.Length);

            if (nFiles > 0)
            {
                Array.Sort(ArFiles);

                nFiles = (Int32)(ArFiles == null ? 0 : ArFiles.Length);

                if (nFiles > 0)
                {
                    Int32 i = nFiles;
                    string filePath = ArFiles[--i];
                    string fileName = Path.GetFileName(filePath);
                    string startsWith = fileName.Substring(0, ANrStartChars);
                    string lastStart = startsWith;

                    nFiles = 1;
                    while (--i >= 0)
                    {
                        filePath = ArFiles[i];
                        fileName = Path.GetFileName(filePath);
                        startsWith = fileName.Substring(0, ANrStartChars);

                        if (startsWith == lastStart)
                        {
                            ArFiles[i] = ""; // same and wipe
                        }
                        else
                        {
                            lastStart = startsWith; // different;
                            ++nFiles;
                        }
                    }
                }
            }
            return (UInt32)nFiles;
        }
    }
}

