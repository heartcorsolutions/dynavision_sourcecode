﻿using Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventBoard.Event_Base
{
    public enum DSironaOperation
    {
        Unknown,
        None, 
        Start,
        Stop,
        Erase,
        ParamChange,
        DataRequest,
        ReceivedState
    };
    public enum DSironaMode
    {
        None,
        Holter,
        Event,
        Post,
        MCT
    }

    public class CSironaStateSettings
    {
        public static bool _sbUseQueue = false;

        public const string _cSettingNumberExt = "settingNumber";
        public const string _cActionNumberExt = "actionNumber";
        public const string _cSettingFileExt = "SirSet";
        public const string _cStatusFileExt = "SirStatus";
        public const string _cDefaultsName = "defaults";


        public const string _csToServerName = "ToServer";
        public const string _csFromServerName = "FromServer";
        public const string _csSironaName = "Sirona";
        public const string _csDataNextLine = ",\r\n";//", ";
        public const string _csTextNextLine = "\r\n";//", ";

        private const UInt32 _c_SironaMaxReqLengthSec = 3600;

        private static UInt32 _sMaxReqLengthSec = 59 * 60;
        private static UInt32 _sHolterReqLengthSec = 60 * 60;    // Holter = 0 or Holter > MaxReq

        // State only: read only 
        public string _mDeviceSerial;
        public string _mModelNumber;
        public string _mFirmwareVersion;
        public string _mAppVersion;
        public string _mServerBaseUrl;
        public UInt16 _mBatteryLevel;
        public string _mCableType;
        public UInt32 _mLeadOffBits;
        public string _mUpdateSource;
        public DateTimeOffset _mProcedureStartTimeDTO;
        public UInt32 _mProcedureLengthSec;
        public string _mProcedureType;
        public string _mProcedureState;
        public UInt32 _mCommunicationDelay;

        public string _mDevice_address;

        // Parameters: Read & Write
        public bool _mbAudioMute;

        public bool _mbUiLock;

        public string _mPatientId;
        public string _mPatientFirstName;
        public string _mPatientMiddleName;
        public string _mPatientLastName;
        public string _mPhysicianName;
        public DateTime _mPatientDOB;
        public string _mPatientComment;
        public UInt16 _mTachyThresholdBpm;
        public UInt16 _mBradyThresholdBpm;
        public float _mPauseThresholdSec;
        public bool _mbAfibOn;
        public UInt16 _mPreTriggerSec;
        public UInt16 _mPostTriggerSec;
        public UInt16 _mRecordLengthDays;
        public UInt16 _mSampleRate;
        public UInt16 _mAutoEventLimit;
        public UInt16 _mManualEventLimit;
        public bool _mbTtm3x;

        // Operational: Write

        public string _mOperationCommand;
        public string _mOperationType;
        public string _mStateResult;
        public string _mStateError;

        public bool _mbReqAnotations;
        public bool _mbReqEcg;
        public string _mReqUploadUrl;
        public UInt32 _mReqStartAtSec;
        public UInt32 _mReqLengthSec;

        // work
        public DSironaOperation _mOperationMode;
        private string _mDataLine;
        private Int32 _mDataCursor;
        private Int32 _mDataLineLength;
        private string _mDataLabel;
        private string _mDataValue;

        public CSironaStateSettings()
        {
            mClear("", "!");
        }

        public void mClear(String ASerialNr, String ADefaultRO)
        {
            // State only: read only 
            _mDeviceSerial = ASerialNr;
            _mModelNumber = ADefaultRO;
            _mFirmwareVersion = ADefaultRO;
            _mAppVersion = ADefaultRO;
            _mServerBaseUrl = "";
            _mBatteryLevel = 0;
            _mCableType = ADefaultRO;
            _mLeadOffBits = 0xFFFF;
            _mUpdateSource = ADefaultRO;
            _mProcedureStartTimeDTO = DateTimeOffset.MinValue;
            _mProcedureLengthSec = 0;
            _mProcedureType = ADefaultRO;
            _mProcedureState = ADefaultRO;
            _mCommunicationDelay = 0;
            _mDevice_address = "";

            // Parameters: Read & Write

            _mbAudioMute = false;
            _mbUiLock = false;
            _mPatientId = "";
            _mPatientFirstName = "";
            _mPatientMiddleName = "";
            _mPatientLastName = "";
            _mPhysicianName = "";
            _mPatientDOB = DateTime.MinValue;
            _mPatientComment = "";
            _mTachyThresholdBpm = 160;
            _mBradyThresholdBpm = 35;
            _mPauseThresholdSec = 3.0F;
            _mbAfibOn = true;
            _mPreTriggerSec = 60;
            _mPostTriggerSec = 30;
            _mRecordLengthDays = 30;
            _mSampleRate = 256;
            _mAutoEventLimit = 1000;
            _mManualEventLimit = 1000;
            _mbTtm3x = true;
            // Operational: Write
            _mbReqAnotations = true;
            _mbReqEcg = true;
            _mReqUploadUrl = "";
            _mReqStartAtSec = 0;
            _mReqLengthSec = 0;
            _mStateResult = "";
            _mStateError = "";
            _mOperationType = "";
            _mOperationCommand = "";

            // work

            _mOperationMode = DSironaOperation.Unknown;
            _mDataLine = "";
            _mDataLineLength = 0;
            _mDataCursor = 0;
            _mDataLabel = "";
            _mDataValue = "";
        }
        public static void sSetHolterReqLength(UInt32 AMaxReqLengthMin, UInt32 AHolterReqLengthMin)
        {
            _sMaxReqLengthSec = 59 * 60;
            _sHolterReqLengthSec = 60 * 60;    // Holter = 0 or Holter > MaxReq


            if( AMaxReqLengthMin > 0 || AHolterReqLengthMin > 0)
            {
               _sMaxReqLengthSec = 60 * AMaxReqLengthMin;
               _sHolterReqLengthSec = 60 * AHolterReqLengthMin;    // Holter = 0 or Holter > MaxReq
            }

            if (_sHolterReqLengthSec > _c_SironaMaxReqLengthSec)
            {
                _sHolterReqLengthSec = _c_SironaMaxReqLengthSec;
            }
            if (_sMaxReqLengthSec > _c_SironaMaxReqLengthSec)
            {
                _sMaxReqLengthSec = _c_SironaMaxReqLengthSec;
            }
            if (_sHolterReqLengthSec < 30 )
            {
                _sHolterReqLengthSec = 0;
            }
            if (_sHolterReqLengthSec > 0 && _sMaxReqLengthSec > _sHolterReqLengthSec - 30)
            {
                _sMaxReqLengthSec = _sHolterReqLengthSec - 30; // holter always 30 sec bigger for testing is holter
            }
        }
        public static bool sbIsHolterActive()
        {
            return _sHolterReqLengthSec > 0;
        }
        public static bool sbIsHolter( UInt32 AStripLengthSec)
        {
            return _sHolterReqLengthSec > 0 && AStripLengthSec > _sHolterReqLengthSec - 30; // events with strip length close to holter are asumed holter
        }
        public static UInt32 sGetHolterReqLengthSec()
        {
            return _sHolterReqLengthSec;
        }
        public static UInt32 sGetMaxReqLengthSec()
        {
            return _sMaxReqLengthSec;
        }

        public void mSetOperation( DSironaOperation AOperation )
        {
            string operation = "";
//            string type = "";
            string state = "NONE";

            switch (AOperation)
            {
                case DSironaOperation.Unknown: break;
                case DSironaOperation.None:     operation = "NONE"; break;
                case DSironaOperation.Start:    operation = "START"; state = "Active"; break;
                case DSironaOperation.Stop:     operation = "STOP"; state = "Finished"; break;
                case DSironaOperation.Erase:    operation = "ERASE"; state = "Idle"; break;
                case DSironaOperation.ParamChange: operation = "PARAM_CHANGE"; break;
                case DSironaOperation.DataRequest: operation = "DATA_REQUEST"; break;
                case DSironaOperation.ReceivedState: operation = "NONE"; break;
            }
/*            switch (AMode)
            {
                case DSironaMode.None: type = ""; break;
                case DSironaMode.Holter: type = "Holter"; break;
                case DSironaMode.Event: type = "Event"; break;
                case DSironaMode.Post: type = "Post"; break;
                case DSironaMode.MCT: type = "MCT"; break;
            }
 */           _mProcedureState = state;
//            _mOperationType = type;
            _mOperationCommand = operation;
            _mOperationMode = AOperation;
        }
        public static string sGetOperationLabel(DSironaOperation AOperation)
        {
            string operation = "";

            switch (AOperation)
            {
                case DSironaOperation.Unknown: break;
                case DSironaOperation.None: operation = "NONE"; break;
                case DSironaOperation.Start: operation = "START";  break;
                case DSironaOperation.Stop: operation = "STOP"; break;
                case DSironaOperation.Erase: operation = "ERASE"; break;
                case DSironaOperation.ParamChange: operation = "PARAM_CHANGE"; break;
                case DSironaOperation.DataRequest: operation = "DATA_REQUEST"; break;
                case DSironaOperation.ReceivedState: operation = "NONE"; break;
            }
            return operation;
        }

        public bool mbStateIsIdle()
        {
            string state = _mProcedureState.ToLower();

            return state == "idle";
        }

        public bool mbStateIsActive()
        {
            string state = _mProcedureState.ToLower();

            return state == "active";
        }

        public bool mbStateIsFinished()
        {
            string state = _mProcedureState.ToLower();

            return state == "finished";
        }

        public bool mbTypeIsMct()
        {
            string type = _mProcedureType.ToLower();

            return type == "mct";
        }
        public bool mbTypeIsHolter()
        {
            string type = _mProcedureType.ToLower();

            return type == "holter";
        }

        public static DSironaOperation sParseOperation(String AOperation)
        {
            DSironaOperation operation = DSironaOperation.Unknown;

            if (AOperation != null)
            {
                string s = AOperation.Trim();

                if (s != null && s.Length > 0)
                {
                    s = s.ToUpper();

                    if (s == "NONE") operation = DSironaOperation.None;
                    else if (s == "START") operation = DSironaOperation.Start;
                    else if (s == "STOP") operation = DSironaOperation.Stop;
                    else if (s == "ERASE") operation = DSironaOperation.Erase;
                    else if (s == "PARAM_CHANGE") operation = DSironaOperation.ParamChange;
                    else if (s == "DATA_REQUEST") operation = DSironaOperation.DataRequest;
                }
            }
            return operation;
        }


        public bool mbReadToNextChar( out char ArChar)
        {
            char nextChar = '\0';

            while( _mDataCursor < _mDataLineLength)
            {
                char c = _mDataLine[_mDataCursor];
                    
                if( c > ' ')
                {
                    nextChar = c;
                    break;
                }
                ++_mDataCursor;
            }
            ArChar = nextChar;
            return _mDataCursor < _mDataLineLength;
        }

        public bool mbReadDataString( out string ArString)
        {
            bool bOk = false;
            string str = "";
            char c;

            if (mbReadToNextChar( out c ))
            {
                bool bString = c == '"';

                if (bString)
                {
                    ++_mDataCursor;
                    // read string until '"'
                    while (_mDataCursor < _mDataLineLength)
                    {
                        c = _mDataLine[_mDataCursor++];
                        if (c == '"')
                        {
                            break;
                        }
                        str += c;
                    }
                }
                if (c == '[')   // read set into string
                {
                    ++_mDataCursor;
                    // read string until '"'
                    while (_mDataCursor < _mDataLineLength)
                    {
                        c = _mDataLine[_mDataCursor++];
                        if (c == ']')
                        {
                            break;
                        }
                        str += c;
                    }
                }
                else
                {
                    // read one word like true or false
                    while (_mDataCursor < _mDataLineLength)
                    {
                        c = _mDataLine[_mDataCursor];
                        if (false == Char.IsLetterOrDigit(c) && c != '.')
                        {
                            break;
                        }
                        str += c;
                        ++_mDataCursor;
                    }
                }
                bOk = true;
            }

            ArString = str;
            return bOk;
        }
 
        public bool mbReadFile( string AFilePath )
        {
            //"device_serial":"76A0001000",     "model_number":"RX93030-000",     "firmware_version":"1.2.3",     "app_version":"1.0.abcdefg",     "battery_level":"100",     "audio_mute":false,     "cable_type":"2Ch,3Lead",     "lead_off":"0",     "update_source":"Android",     "ui_lock":"false",     "procedure_start_time":"2017-08-02T11:32:53-0500",     "procedure_length":"86400",     "procedure_type":"MCT",     "procedure_state":"Active",     "patient_id":"009876",     "patient_first":"Jane",     "patient_middle":"E",     "patient_last":"Smith",     "physician_name":"Jones",     "patient_dob":"1968-12-31",     "patient_comment":"Patient reports dizziness.\\nNo meds.",     "tachy_threshold":"160",     "brady_threshold":"35",     "pause_threshold":"3.0",     "afib_on":true,     "pre_trigger":"60",     "post_trigger":"30",     "record_length":"30",     "sample_rate":"256",     "auto_event_limit":"1000",     "manual_event_limit":"1000",     "ttm_3x":true 
            bool bOk = false;
            char c;

            try
            {
                //"device_serial":"76A0001000",     "model_number":"RX93030-000",     "firmware_version":"1.2.3",     "app_version":"1.0.abcdefg",     "battery_level":"100",     "audio_mute":false,     "cable_type":"2Ch,3Lead",     "lead_off":"0",     "update_source":"Android",     "ui_lock":"false",     "procedure_start_time":"2017-08-02T11:32:53-0500",     "procedure_length":"86400",     "procedure_type":"MCT",     "procedure_state":"Active",     "patient_id":"009876",     "patient_first":"Jane",     "patient_middle":"E",     "patient_last":"Smith",     "physician_name":"Jones",     "patient_dob":"1968-12-31",     "patient_comment":"Patient reports dizziness.\\nNo meds.",     "tachy_threshold":"160",     "brady_threshold":"35",     "pause_threshold":"3.0",     "afib_on":true,     "pre_trigger":"60",     "post_trigger":"30",     "record_length":"30",     "sample_rate":"256",     "auto_event_limit":"1000",     "manual_event_limit":"1000",     "ttm_3x":true 
                _mOperationMode = DSironaOperation.ReceivedState;
                _mDataLine = "";
                _mDataCursor = 0;
                _mDataLabel = "";
                _mDataValue = "";

                _mDataLine = System.IO.File.ReadAllText(AFilePath);

                _mDataLineLength = _mDataLine == null ? 0 : _mDataLine.Length;
                if (_mDataLineLength == 0)
                {
                    CProgram.sLogError("Empty Sirona file " + AFilePath);
                }
                else if (mbReadToNextChar(out c))
                {
                    if (c != '{')
                    {
                        CProgram.sLogError("missing start '{'  Sirona file " + AFilePath);
                    }
                    else
                    {
                        ++_mDataCursor;

                        while (mbReadToNextChar(out c))
                        {
                            if (c == ',')
                            {
                                // next label variable pair
                                ++_mDataCursor;
                                _mDataLabel = "";
                                _mDataValue = "";
                            }
                            else if (c == '\"')
                            {
                                // label
                                if (mbReadDataString(out _mDataLabel))
                                {
                                    if (mbReadToNextChar(out c) && c == ':')
                                    {
                                        ++_mDataCursor;
                                        if (mbReadDataString(out _mDataValue))
                                        {
                                            _mDataLabel = _mDataLabel.ToLower();    // make lower for compare
                                            if( false == mbParseLabelValue() && CLicKeyDev.sbDeviceIsProgrammer())
                                            {
                                                CProgram.sLogLine("unknown label " + _mDataLabel + " = " + _mDataValue);
                                            }
                                            bOk = true;
                                        }
                                        else
                                        {
                                            CProgram.sLogError("label " + _mDataLabel + " not followed by a value in Sirona file " + AFilePath);
                                            bOk = false;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        CProgram.sLogError("label " + _mDataLabel + " not followed by ':' in Sirona file " + AFilePath);
                                        bOk = false;
                                        break;
                                    }
                                }
                                else
                                {
                                    CProgram.sLogError("unexpected label at " + _mDataCursor.ToString() + " in Sirona file " + AFilePath);
                                    bOk = false;
                                    break;
                                }
                            }
                            else if (c == '}')
                            {
                                // should be end of line
                                bOk = true;
                                break;
                            }
                            else
                            {
                                CProgram.sLogError("unexpected char " + c + " @" + _mDataCursor.ToString() + " in Sirona file " + AFilePath);
                                bOk = false;
                                break;
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Sirona Write error to " + AFilePath, ex);
            }
            return bOk;
        }

        public bool mbParseString( string ALabel, ref string ArString )
        {
            bool bOk = false;

            if (_mDataLabel != null && ALabel != null)
            {
                if( 0 == ALabel.ToLower().CompareTo(_mDataLabel))
                {
                    bOk = true;
                    ArString = _mDataValue;
                }
            }
            return bOk;
        }
        public bool mbParseUInt16(string ALabel, ref UInt16 ArValue)
        {
            bool bOk = false;

            if (_mDataLabel != null && ALabel != null && _mDataValue != null && _mDataValue.Length > 0)
            {
                if (0 == ALabel.ToLower().CompareTo(_mDataLabel))
                {
                    bOk = UInt16.TryParse( _mDataValue, out ArValue );
                }
            }
            return bOk;
        }
        public bool mbParseUInt32(string ALabel, ref UInt32 ArValue)
        {
            bool bOk = false;

            if (_mDataLabel != null && ALabel != null && _mDataValue != null && _mDataValue.Length > 0)
            {
                if (0 == ALabel.ToLower().CompareTo(_mDataLabel))
                {
                    bOk = UInt32.TryParse(_mDataValue, out ArValue);
                }
            }
            return bOk;
        }
        public bool mbParseFloat(string ALabel, ref float ArValue)
        {
            bool bOk = false;

            if (_mDataLabel != null && ALabel != null && _mDataValue != null && _mDataValue.Length > 0)
            {
                if (0 == ALabel.ToLower().CompareTo(_mDataLabel))
                {
                    bOk = CProgram.sbParseFloat(_mDataValue, ref ArValue);
//                    CProgram.sLogLine(ALabel + " parse " + _mDataLabel + " = " + ArValue.ToString());
                }
            }
            return bOk;
        }
        public bool mbParseBool(string ALabel, ref bool ArbValue)
        {
            bool bOk = false;

            if (_mDataLabel != null && ALabel != null && _mDataValue  != null && _mDataValue.Length > 0)
            {
                if (0 == ALabel.ToLower().CompareTo(_mDataLabel))
                {
                    bOk = bool.TryParse(_mDataValue, out ArbValue);

                    if( bOk == false )
                    {
                        if( _mDataValue == "0")
                        {
                            ArbValue = false;
                            bOk = true;
                        }
                        else if (_mDataValue == "1")
                        {
                            ArbValue = true;
                            bOk = true;
                        }
                    }
                }
            }
            return bOk;
        }

        public string mMakeDTZ(DateTimeOffset ArValue)
        {
            //2017-07-24T18:13:51+0000
            string s = ArValue.Year.ToString("D4") + "-" + ArValue.Month.ToString("D2") + "-" + ArValue.Day.ToString("D2") + " "
                + ArValue.Hour.ToString("D2") + ":" + ArValue.Minute.ToString("D2") + ":" + ArValue.Second.ToString("D2");

            int hour = ArValue.Offset.Hours;
            int min = ArValue.Offset.Minutes;

            if( hour > 0 )
            {
                s += "+";
            }
            s += hour.ToString("D2") + min.ToString("D2");

            return s;
        }

        public bool mbParseDTZ(string ALabel, ref DateTimeOffset ArValue)
        {
            bool bOk = false;

            if (_mDataLabel != null && ALabel != null)
            {
                if (0 == ALabel.ToLower().CompareTo(_mDataLabel))
                {
                    //2017-07-24T18:13:51+0000
                    //0    5  8  11 14 17 2022  
                    UInt16 year, month, day, hour, min, sec, tzMin;
                    Int16 tzHour;
                    year = month = day = hour = min = sec = tzMin = 0;
                    tzHour = 0;

                    if (_mDataValue == null || _mDataValue.Length == 0)
                    {
                        ArValue = DateTimeOffset.MinValue;
                        bOk = true;
                    }
                    else
                    {
                        try
                        {
                            int len = _mDataValue.Length;
                            bOk = len >= 19
                                && UInt16.TryParse(_mDataValue.Substring(0, 4), out year)
                                && UInt16.TryParse(_mDataValue.Substring(5, 2), out month)
                                && UInt16.TryParse(_mDataValue.Substring(8, 2), out day)
                                && UInt16.TryParse(_mDataValue.Substring(11, 2), out hour)
                                && UInt16.TryParse(_mDataValue.Substring(14, 2), out min)
                                && UInt16.TryParse(_mDataValue.Substring(17, 2), out sec)
                                && Int16.TryParse(_mDataValue.Substring(19, 3), out tzHour)
                                && (len < 24 || UInt16.TryParse(_mDataValue.Substring(22, 2), out tzMin));
                            if (false == bOk)
                            {
                                CProgram.sLogError("invalid Date time offset " + _mDataValue.ToString() + " in Sirona file for label " + _mDataLabel);
                            }
                            else
                            {
                                TimeSpan offset = new TimeSpan(tzHour, (tzHour < 0 ? -tzMin : tzMin), 0);
                                ArValue = new DateTimeOffset(year, month, day, hour, min, sec, offset);
                            }
                        }
                        catch (Exception)
                        {
                            CProgram.sLogError("invalid Date time offset " + _mDataValue.ToString() + " in Sirona file for label " + _mDataLabel);
                            bOk = false;
                        }
                    }
                }
            }
            return bOk;
        }
        public bool mbParseDate(string ALabel, ref DateTime ArValue)
        {
            bool bOk = false;

            if (_mDataLabel != null && ALabel != null)
            {
                if (0 == ALabel.ToLower().CompareTo(_mDataLabel))
                {
                    //2017-07-24
                    //0    5  8 
                    UInt16 year, month, day;
                    year = month = day = 0;

                    
                    if (_mDataValue == null || _mDataValue.Length == 0)
                    {
                        ArValue = DateTime.MinValue;
                        bOk = true;
                    }
                    else
                    {
                        bOk = UInt16.TryParse(_mDataValue.Substring(0, 4), out year)
                            && UInt16.TryParse(_mDataValue.Substring(5, 2), out month)
                            && UInt16.TryParse(_mDataValue.Substring(8, 2), out day);
                        if (false == bOk)
                        {
                            CProgram.sLogError("invalid Date" + _mDataValue.ToString() + " in Sirona file for label " + _mDataLabel);
                        }
                        else
                        {
                            try
                            {
                                ArValue = new DateTime(year, month, day);
                            }
                            catch (Exception)
                            {
                                bOk = false;
                            }
                        }
                    }
                }
            }
            return bOk;
        }

        public bool mbParseLabelValue()
        {
            bool bOk = false;

            try
            {


                bOk = mbParseString("device_serial", ref _mDeviceSerial)
                || mbParseString("model_number", ref _mModelNumber)
                || mbParseString("firmware_version", ref _mFirmwareVersion)
                || mbParseString("app_version", ref _mAppVersion)
                || mbParseString("server_base_url", ref _mServerBaseUrl)
                || mbParseUInt16("battery_level", ref _mBatteryLevel)
                || mbParseString("cable_type", ref _mCableType)
                || mbParseUInt32("lead_off", ref _mLeadOffBits)
                || mbParseString("update_source", ref _mUpdateSource)
                || mbParseDTZ("procedure_start_time", ref _mProcedureStartTimeDTO)
                || mbParseUInt32("procedure_length", ref _mProcedureLengthSec)
                || mbParseString("procedure_type", ref _mProcedureType)
                || mbParseString("procedure_state", ref _mProcedureState)
                || mbParseUInt32("communication_delay", ref _mCommunicationDelay)
                || mbParseBool("audio_mute", ref _mbAudioMute)
                || mbParseString("device_address", ref _mDevice_address)

                || mbParseBool("ui_lock", ref _mbUiLock)

                || mbParseString("patient_id", ref _mPatientId)
                || mbParseString("patient_first", ref _mPatientFirstName)
                || mbParseString("patient_middle", ref _mPatientMiddleName)
                || mbParseString("patient_last", ref _mPatientLastName)
                || mbParseString("physician_name", ref _mPhysicianName)
                || mbParseDate("patient_dob", ref _mPatientDOB)
                || mbParseString("patient_comment", ref _mPatientComment)
                || mbParseUInt16("tachy_threshold", ref _mTachyThresholdBpm)
                || mbParseUInt16("brady_threshold", ref _mBradyThresholdBpm)
                || mbParseFloat("pause_threshold", ref _mPauseThresholdSec)
                || mbParseBool("afib_on", ref _mbAfibOn)
                || mbParseUInt16("pre_trigger", ref _mPreTriggerSec)
                || mbParseUInt16("post_trigger", ref _mPostTriggerSec)
                || mbParseUInt16("record_length", ref _mRecordLengthDays)
                || mbParseUInt16("sample_rate", ref _mSampleRate)
                || mbParseUInt16("auto_event_limit", ref _mAutoEventLimit)
                || mbParseUInt16("manual_event_limit", ref _mManualEventLimit)
                || mbParseBool("ttm_3x", ref _mbTtm3x)

                // Operational: Write

                || mbParseString("operation", ref _mOperationCommand)
                || mbParseString("result", ref _mStateResult)
                || mbParseString("error", ref _mStateError)
                || mbParseString("type", ref _mOperationType)
                || mbParseBool("annotations", ref _mbReqAnotations)
                || mbParseBool("ecg", ref _mbReqEcg)
                || mbParseString("url", ref _mReqUploadUrl)
                || mbParseUInt32("start", ref _mReqStartAtSec)
                || mbParseUInt32("length", ref _mReqLengthSec);
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Sirona parse error " + _mDataLabel + "="+ _mDataValue, ex);
            }

            return bOk;

        }

       public void mAddDataString( bool AbAddEmpty, String ALabel, string AValue )
        {
            if( AbAddEmpty || AValue != null && AValue.Length > 0)
            {
                if (_mDataLine != null && _mDataLine.Length > 0)
                {
                    _mDataLine += _csDataNextLine;
                }
                _mDataLine += "\"" + ALabel + "\": \"" + AValue + "\"";
            }
        }
        public void mAddDataInt(bool AbAddEmpty, String ALabel, UInt32 AValue)
        {
            if ( AbAddEmpty || AValue != 0)
            {
                if (_mDataLine != null && _mDataLine.Length > 0)
                {
                    _mDataLine += _csDataNextLine;
                }
//                _mDataLine += "\"" + ALabel + "\":\"" + AValue.ToString() + "\"";
                _mDataLine += "\"" + ALabel + "\": " + AValue.ToString();
            }
        }
        public void mAddDataFloat(bool AbAddEmpty, String ALabel, float AValue)
        {
            if ( AbAddEmpty || AValue != 0)
            {
                if (_mDataLine != null && _mDataLine.Length > 0)
                {
                    _mDataLine += _csDataNextLine;
                }
//                _mDataLine += "\"" + ALabel + "\":\"" + AValue.ToString() + "\"";
                string strFloat = CProgram.sCorrectFloatFromProg(DFloatDecimal.Dot, AValue.ToString("0.00"));
                _mDataLine += "\"" + ALabel + "\": " + strFloat;
            }
        }
        public void mAddDataBool(String ALabel, bool AbValue)
        {
            if (_mDataLine != null && _mDataLine.Length > 0)
            {
                string strBool = AbValue ? "true" : "false";
                
                _mDataLine += _csDataNextLine;
                _mDataLine += "\"" + ALabel + "\": " + strBool;
            }
        }

        public bool mbWriteFile(string AFilePath, bool AbSaveState)
        {
            bool bOk = false;

            try
            {
                //"device_serial":"76A0001000",     "model_number":"RX93030-000",     "firmware_version":"1.2.3",     "app_version":"1.0.abcdefg",     "battery_level":"100",     "audio_mute":false,     "cable_type":"2Ch,3Lead",     "lead_off":"0",     "update_source":"Android",     "ui_lock":"false",     "procedure_start_time":"2017-08-02T11:32:53-0500",     "procedure_length":"86400",     "procedure_type":"MCT",     "procedure_state":"Active",     "patient_id":"009876",     "patient_first":"Jane",     "patient_middle":"E",     "patient_last":"Smith",     "physician_name":"Jones",     "patient_dob":"1968-12-31",     "patient_comment":"Patient reports dizziness.\\nNo meds.",     "tachy_threshold":"160",     "brady_threshold":"35",     "pause_threshold":"3.0",     "afib_on":true,     "pre_trigger":"60",     "post_trigger":"30",     "record_length":"30",     "sample_rate":"256",     "auto_event_limit":"1000",     "manual_event_limit":"1000",     "ttm_3x":true 
                _mDataLine = "";

                if( AbSaveState)
                {
                    mAddDataString(true, "device_serial", _mDeviceSerial);
                    mAddDataString(true, "model_number", _mModelNumber);
                    mAddDataString(true, "firmware_version", _mFirmwareVersion);
                    mAddDataString(true, "app_version", _mAppVersion);
                    mAddDataString(true, "server_base_url", _mServerBaseUrl);
                    mAddDataInt(true, "battery_level", _mBatteryLevel);
                    mAddDataString(true, "cable_type", _mCableType);
                    mAddDataInt(true, "lead_off", _mLeadOffBits);
                    mAddDataString(true, "update_source", _mUpdateSource);
                    mAddDataString(true, "procedure_start_time", mMakeDTZ( _mProcedureStartTimeDTO ));
                    mAddDataInt(true, "procedure_length", _mProcedureLengthSec);
                    mAddDataString(true, "procedure_type", _mProcedureType);
                    mAddDataString(true, "procedure_state", _mProcedureState);
                    mAddDataInt(true, "communication_delay", _mCommunicationDelay);
                    mAddDataString(true, "device_address", _mDevice_address);
//                    mAddDataBool("audio_mute", _mbAudioMute);

                }

                mAddDataBool("audio_mute", _mbAudioMute);

                mAddDataBool("ui_lock", _mbUiLock);

                mAddDataString(true, "patient_id", _mPatientId);
                mAddDataString(true, "patient_first", _mPatientFirstName);
                mAddDataString(true, "patient_middle", _mPatientMiddleName);
                mAddDataString(true, "patient_last", _mPatientLastName);
                mAddDataString(true, "physician_name", _mPhysicianName);
                mAddDataString(true, "patient_dob", _mPatientDOB.Year.ToString("D4") + "-" + _mPatientDOB.Month.ToString("D2") + "-" + _mPatientDOB.Day.ToString("D2"));
                mAddDataString(true, "patient_comment", _mPatientComment);
                mAddDataInt(true, "tachy_threshold", _mTachyThresholdBpm);
                mAddDataInt(true, "brady_threshold", _mBradyThresholdBpm);
                mAddDataFloat(true, "pause_threshold", _mPauseThresholdSec);
                mAddDataBool("afib_on", _mbAfibOn);
                mAddDataInt(true, "pre_trigger", _mPreTriggerSec);
                mAddDataInt(true, "post_trigger", _mPostTriggerSec);
                mAddDataInt(true, "record_length", _mRecordLengthDays);
                mAddDataInt(true, "sample_rate", _mSampleRate);
                mAddDataInt(true, "auto_event_limit", _mAutoEventLimit);
                mAddDataInt(true, "manual_event_limit", _mManualEventLimit);
                mAddDataBool("ttm_3x", _mbTtm3x);

                // Operational: Write

                mAddDataString(true, "operation", _mOperationCommand);

                switch (_mOperationMode)
                {
                    case DSironaOperation.None:
                        mAddDataString(false, "error", _mStateError);
                        break;
                    case DSironaOperation.Start:
                        mAddDataString(true, "type", _mOperationType);
                        break;
                    case DSironaOperation.Stop:
                        break;
                    case DSironaOperation.Erase:
                        break;
                    case DSironaOperation.ParamChange:
                        break;
                    case DSironaOperation.DataRequest:
                        mAddDataBool("annotations", _mbReqAnotations);
                        mAddDataBool("ecg", _mbReqEcg);
                        mAddDataString(false, "url", _mReqUploadUrl);
                        mAddDataInt(true, "start", _mReqStartAtSec);
                        mAddDataInt(true, "length", _mReqLengthSec);
                        break;
                }
                System.IO.File.WriteAllText(AFilePath, "{" +_csTextNextLine + _mDataLine + _csTextNextLine + "}" + _csTextNextLine);
                bOk = true;
            }
            catch ( Exception ex )
            {
                CProgram.sLogException("Sirona Write error to " + AFilePath, ex);
            }

            return bOk;
        }

        public static bool sbGetSironaDevicePath(out string ArDevicePath, string ASerialNumber)
        {
            bool bOk = false;
            string devicePath = "";

            if (CDvtmsData.sbGetDeviceTypeDir(out devicePath, DDeviceType.Sirona )) //_csSironaName))
            {
                if (ASerialNumber != null && ASerialNumber.Length > 0)
                {
                    devicePath = Path.Combine(devicePath, ASerialNumber);

                    /*                    if (false == Directory.Exists(_mSirDevicePath) && CProgram.sbAskYesNo("Sir Aera Device Settings", "Create Sir device directory " + _mSerialNr + "?"))
                                        {
                                            CDvtmsData.sbCreateDir(_mSirDevicePath);
                                        }
                    */
                    bOk = Directory.Exists(devicePath);
                }
                else
                {
                    bOk = true;
                }
            }
            ArDevicePath = devicePath;
            return bOk;
        }

        public static bool sbCreateSironaDevicePath(out string ArDevicePath, string ASerialNumber)
        {
            // Sir device directory should only be made by i-reader
            bool bOk = false;
            string devicePath = "";

            if (CDvtmsData.sbGetDeviceTypeDir(out devicePath, DDeviceType.Sirona ))//_csSironaName))
            {
                if (ASerialNumber != null && ASerialNumber.Length > 0)
                {
                    devicePath = Path.Combine(devicePath, ASerialNumber);

                    if (false == Directory.Exists(devicePath) && CProgram.sbAskYesNo("Sirona Device Settings", "Create Sirona device directory " + ASerialNumber + "?"))
                    {
                        CDvtmsData.sbCreateDir(devicePath);
                    }

                    bOk = Directory.Exists(devicePath);
                }
                else
                {
                    bOk = true;
                }
            }
            ArDevicePath = devicePath;
            return bOk;
        }

        public static bool sbMakeSettingsFileName(out string AFileName, string ASerialNumber)
        {
            bool bOk = ASerialNumber != null && ASerialNumber.Length > 0;
            string name = "";

            if (bOk)
            {
                name = Path.ChangeExtension(ASerialNumber, _cSettingFileExt);
            }
            AFileName = name;
            return bOk;
        }
        public static bool sbMakeStatusFileName(out string AFileName, string ASerialNumber)
        {
            bool bOk = ASerialNumber != null && ASerialNumber.Length > 0;
            string name = "";

            if (bOk)
            {
                name = Path.ChangeExtension(ASerialNumber, _cStatusFileExt);
            }
            AFileName = name;
            return bOk;
        }

        public static bool sbMakeCommandFileName(out string AFileName, string ASerialNumber)
        {
            bool bOk = ASerialNumber != null && ASerialNumber.Length > 0;
            string name = "";

            if (bOk)
            {
                name = Path.ChangeExtension(ASerialNumber, _cSettingFileExt);
            }
            AFileName = name;
            return bOk;
        }

        
        public static bool sbRequestSironaDataSingle(bool AbHolter, CSironaStateSettings ASirConfig, out String ArResult, string ASerialNr, 
            DateTime AStartStripDT, UInt32 ABlockLengthSec, bool AbEcgData, DateTime AUseDT, UInt16 AIndex)
        {
            bool bOk = false;
            string result = "?";
            string toFileName = "";


            // load status
            if (ASirConfig != null)
            {
                // check request in device start -> end

                try
                {
                    result = (AbEcgData ? "Req ECG, " : "Req Trend, ");

                    if ((ASirConfig._mProcedureStartTimeDTO - DateTime.Now).TotalDays <= -3650)
                    {
                        result += ", No start";
                    }
                    else
                    {
                        UInt32 blockLengthSec = ABlockLengthSec;
                        UInt32 maxStripLenSec = sGetMaxReqLengthSec();
                        int startSec = (int)((AStartStripDT - ASirConfig._mProcedureStartTimeDTO.DateTime).TotalSeconds + 0.5);
                        string cmdInfo = "Q" + CProgram.sDateTimeToYMDHMS(AStartStripDT);

                        if (startSec < 0)
                        {
                            result += ", Before start " + ASirConfig._mProcedureStartTimeDTO.ToString();
                        }
                        else if( AbHolter && false == sbIsHolter(ABlockLengthSec))
                        {
                            result += ", Holter strip length (" + ABlockLengthSec .ToString()+ ") not ok ";
                        }
                        else
                        {
                            if( AbHolter )
                            {
                                cmdInfo += "H";
                                blockLengthSec = sGetHolterReqLengthSec();
                            }
                            else if(blockLengthSec > maxStripLenSec)
                            {
                                cmdInfo += "B";
                                blockLengthSec = maxStripLenSec;
                            }
                            else
                            {
                                cmdInfo += "L";
                            }
                            cmdInfo += ABlockLengthSec.ToString("0000")
                                + (AbEcgData ? "E" : "T");

                            ASirConfig._mReqStartAtSec = (UInt32)startSec;
                            ASirConfig._mReqLengthSec = blockLengthSec;

                            if (startSec + blockLengthSec > ASirConfig._mProcedureLengthSec)
                            {
                                DateTimeOffset dt = ASirConfig._mProcedureStartTimeDTO.DateTime.AddSeconds(ASirConfig._mProcedureLengthSec);
                                result += ", Past End " + dt.ToString();
                            }
                            else
                            {
                                result += ", req " + blockLengthSec.ToString() + " sec @" + CProgram.sPrintTimeSpan_dhmSec(startSec, "");
                                    //CProgram.sDateTimeToString(AStartStrip);

                                // setup command 
                                ASirConfig._mOperationMode = DSironaOperation.DataRequest;
                                ASirConfig._mOperationCommand = CSironaStateSettings.sGetOperationLabel(ASirConfig._mOperationMode);
                                ASirConfig._mbReqEcg = AbEcgData;
                                ASirConfig._mbReqAnotations = true;

                                // save command

                                string name = "";
                                string devicePath;

                                if (ASerialNr == null || ASerialNr.Length == 0
                                    || false == CSironaStateSettings.sbGetSironaDevicePath(out devicePath, ASerialNr)
                                    || false == CSironaStateSettings.sbMakeStatusFileName(out name, ASerialNr))
                                {
                                    result = "No device";
                                }
                                else
                                {
                                    try
                                    {
                                        if (CSironaStateSettings.sbMakeSettingsFileName(out name, ASerialNr))
                                        {
                                            string fileName = Path.Combine(devicePath, name);

                                            // do not overwrite  current settings file                                            if (sirConfig.mbWriteFile(fileName, true))
                                            {
//                                                result += " command " + CProgram.sDateTimeToYMDHMS(AUseDT) + "_" + AIndex.ToString();
                                                    //Path.GetFileNameWithoutExtension(fileName);

                                                string toDir = Path.Combine(devicePath, CSironaStateSettings._csToServerName);
                                                toFileName = sMake2FileName(name, AUseDT, AIndex, cmdInfo);
                                                string toFilePath = Path.Combine(toDir, toFileName);

                                                //                                                result += " " + Path.GetFileNameWithoutExtension(toFileName);
                                                result += " " + cmdInfo;

                                                if (ASirConfig.mbWriteFile(toFilePath, false))
                                                {
                                                    result += " writen.";
                                                    bOk = true;
                                                }
                                                else
                                                {
                                                    result += ", failed write to server!";
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        CProgram.sLogException("SaveToServer failed for " + name, ex);
                                    }
                                }
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed Sirona single request for " + ASerialNr, ex);
                    result += ", error!";
                }
            }
            CProgram.sLogLine("ReqSironaData: " + result + " >>" + toFileName );
            ArResult = result;
            return bOk;

        }

        public static bool sbRequestSironaData(bool AbHolterRequest, out String ArResult, string ASerialNr, DateTime AStartStrip, UInt32 ABlockLengthSec, bool AbEcgData)
        {
            bool bOk = false;
            string result = "?";

            CSironaStateSettings sirConfig;

            // load status
            if (sbCheckSironaStatus(out result, out sirConfig, ASerialNr))
            {
                // check request in device start -> end

                try
                {
                    result = (AbEcgData ? "Req ECG, " : "Req Trend, ") + result;

                    if ((sirConfig._mProcedureStartTimeDTO - DateTime.Now).TotalDays <= -3650)
                    {
                        result += ", No start";
                    }
                    else
                    {
                        UInt32 startSec = (UInt32)((AStartStrip - sirConfig._mProcedureStartTimeDTO).TotalSeconds + 0.5);

                        if (startSec < 0)
                        {
                            result += ", Before start " + sirConfig._mProcedureStartTimeDTO.ToString();
                        }
                        if (ABlockLengthSec > 24 * 60 * 60)
                        {
                            result += ", longer then 24 hours";
                        }
                        else
                        {
                            UInt32 maxStripLenSec = AbHolterRequest ? sGetHolterReqLengthSec() : sGetMaxReqLengthSec();

                            DateTime useDT = DateTime.Now;

                            if (ABlockLengthSec <= maxStripLenSec || false == CSironaStateSettings._sbUseQueue)
                            {
                                bOk = sbRequestSironaDataSingle(AbHolterRequest, sirConfig, out result, ASerialNr, AStartStrip, ABlockLengthSec, AbEcgData, useDT, 1);
                            }
                            else
                            {
                                UInt32 blockSize = maxStripLenSec;

                                UInt32 nrBlocks = (UInt32)((ABlockLengthSec + blockSize - 1) / blockSize);
                                string resultBlock = "?";
                                UInt32 iStart = 0;
                                UInt32 iSize;
                                DateTime startDT;

                                for (int iBlock = 0; ++iBlock <= nrBlocks;)
                                {
                                    iSize = (ABlockLengthSec - iStart);
                                    if (iSize > blockSize) iSize = blockSize;

                                    startDT = AStartStrip.AddSeconds(iStart);
                                    bOk = sbRequestSironaDataSingle(AbHolterRequest, sirConfig, out resultBlock, ASerialNr, startDT, iSize, AbEcgData, useDT, (UInt16)iBlock);

                                    if (iBlock == 1)
                                    {
                                        result = resultBlock;
                                    }
                                    if (false == bOk)
                                    {
                                        result += ", failed " + iBlock.ToString() + "/" + nrBlocks.ToString() + " block(" + ABlockLengthSec.ToString() + "sec)";
                                        break;
                                    }

                                    iStart += blockSize;
                                }
                                CProgram.sLogLine("ReqSironaMultiBlock: " + nrBlocks.ToString() + " blocks, total " + ABlockLengthSec.ToString() + "sec "
                                    + (bOk ? "" : "failed") + result);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed Sirona request for " + ASerialNr + "" + result, ex);
                }
            }
            ArResult = result;
            return bOk;

        }
        public static bool sbCheckSironaStatus(out String ArResult, out CSironaStateSettings ArStatus, string ASerialNr)
        {
            bool bOk = false;
            string result = "?";
            CSironaStateSettings sirStatus = new CSironaStateSettings();


            string s = "";
            string name;
            string devicePath;

            if (ASerialNr == null || ASerialNr.Length == 0
                || false == CSironaStateSettings.sbGetSironaDevicePath(out devicePath, ASerialNr)
                || false == CSironaStateSettings.sbMakeStatusFileName(out name, ASerialNr))
            {
                result = "No device";
            }
            else
            {

                //string fileName = Path.Combine(_mDevicePath, name);
                string fromDir = Path.Combine(devicePath, CSironaStateSettings._csFromServerName);
                string fromFileName = Path.Combine(fromDir, name);

                try
                {
                    if (File.Exists(fromFileName))
                    {
                        DateTime dt = File.GetLastWriteTime(fromFileName);

                        result = "Status ";
                        if (dt.Year > 2000)
                        {
                            result += CProgram.sDateTimeToString(dt);
                        }
                        bOk = sirStatus.mbReadFile(fromFileName);

                        if (false == bOk)
                        {
                            result += ", failed load!";
                        }
                    }
                    else
                    {
                        result = "No status";

                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed collect info for " + fromFileName, ex);
                }

            }
            ArResult = result;
            ArStatus = sirStatus;
            return bOk;
        }
        public static string sMake2FileName(string AFileName, DateTime AUtc, UInt16 AIndex, string ACmdInfo)
        {
            string fileName = AFileName;

            if (CSironaStateSettings._sbUseQueue)
            {
                string name = Path.GetFileNameWithoutExtension(AFileName);
                string ext = Path.GetExtension(AFileName);

                // add time stamp and pc name to make it unique 
                fileName = name + "_" + CProgram.sDateTimeToYMDHMS(AUtc) + "_" + AIndex.ToString("000")
                    + "_" + ACmdInfo + "_" + CProgram.sGetPcName() + ext;
            }
            return fileName;
        }
        public static bool sbCheckSettingsFileQueue(out string ArFileInfo, out UInt32 ArNrInQueue, out DateTime ArOldFileDT, string ASerialNr)
        {
            bool bOk = false;
            string s = "";
            UInt32 nrInQueue = 0;
            DateTime oldDT = DateTime.MaxValue;
            string devicePath, name;

            if (ASerialNr == null || ASerialNr.Length == 0
            || false == CSironaStateSettings.sbGetSironaDevicePath(out devicePath, ASerialNr)
            || false == CSironaStateSettings.sbMakeSettingsFileName(out name, ASerialNr))
            {
                s = "No device " + ASerialNr;
            }
            else
            {
                //string fileName = Path.Combine(_mDevicePath, name);
                string fromDir = Path.Combine(devicePath, CSironaStateSettings._csToServerName);

                try
                {
                    string fromFile = Path.Combine(devicePath, name);
                    FileInfo fromInfo = new FileInfo(fromFile);

                    if (fromInfo == null || false == fromInfo.Exists)
                    {
                        s = "Not present, ";
                    }
                    else
                    {
                        s = CProgram.sDateTimeToString(fromInfo.LastWriteTime) + ", ";
                    }

                    DirectoryInfo dirInfo = new DirectoryInfo(fromDir);

                    if (dirInfo == null || false == dirInfo.Exists)
                    {
                        s = "No Dir " + ASerialNr;
                    }
                    else
                    {
                        FileInfo[] fileList = dirInfo.GetFiles("*." + CSironaStateSettings._cSettingFileExt);

                        if (fileList == null || fileList.Length == 0)
                        {
                            s += "Queue empty";
                            bOk = true;
                        }
                        else
                        {
                            foreach (FileInfo fi in fileList)
                            {
                                if (fi.LastWriteTime < oldDT)
                                {
                                    oldDT = fi.LastWriteTime;
                                }
                                ++nrInQueue;
                            }
                            s += nrInQueue.ToString() + " in Queue " + CProgram.sDateTimeToYMDHMS(oldDT);
                            bOk = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed collect Sirona queueo for " + ASerialNr, ex);
                }

            }
            ArFileInfo = s;
            ArNrInQueue = nrInQueue;
            ArOldFileDT = oldDT;

            return bOk;
        }
        public static bool sbClearQueue( out string ArResultText, string ASerialNr)
        {
            bool bOk = false;
            string s = "Clear Queue: ";
            UInt32 nrInQueue = 0;
            string devicePath;

            if (ASerialNr == null || ASerialNr.Length == 0
            || false == CSironaStateSettings.sbGetSironaDevicePath(out devicePath, ASerialNr))
            {
                s += "No device " + ASerialNr;
            }
            else
            {
                //string fileName = Path.Combine(_mDevicePath, name);
                string fromDir = Path.Combine(devicePath, CSironaStateSettings._csToServerName);

                try
                {
                    DirectoryInfo dirInfo = new DirectoryInfo(fromDir);

                    if (dirInfo == null || false == dirInfo.Exists)
                    {
                        s += "No Dir " + ASerialNr;
                    }
                    else
                    {
                        FileInfo[] fileList = dirInfo.GetFiles("*." + CSironaStateSettings._cSettingFileExt);

                        if (fileList == null || fileList.Length == 0)
                        {
                            s += "empty";
                            bOk = true;
                        }
                        else
                        {
                            foreach (FileInfo fi in fileList)
                            {
                                fi.Delete();
                                ++nrInQueue;
                            }
                            s += nrInQueue.ToString() + " deleted";
                            bOk = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed clear Sirona queue for " + ASerialNr, ex);
                }

            }
            ArResultText = s;
            return bOk;
        }




    }
}
