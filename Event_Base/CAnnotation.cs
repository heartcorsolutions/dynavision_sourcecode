﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Program_Base;

namespace Event_Base
{

    public class CAnnotationLine
    {
        public const UInt16 _cSirona_LEAD_OFF = 42;
        public const UInt16 _cSirona_MANUAL = 43;
        public const UInt16 _cSirona_ONSET = 44;
        public const UInt16 _cSirona_OFFSET = 45;
        public const UInt16 _cNormalBeat = 1;  // N
        public const UInt16 _cUnclassifiableBeat = 13; // Q

        public const UInt16 _cPlethSystole = 50;
        public const UInt16 _cPlethDiastole = 51;
        public const UInt16 _cPlethTrans = 52;

        public UInt16 mAnnotationCode;
        public UInt32 mAnnotationTimeTck;
        public UInt16 mAnnotationSubCode;
        public UInt16 mAnnotationNumField;
        public UInt16 mChannel;
        public string mLabel;

        public bool mbExtraValueSet;
        public double mExtraValue;  // ?=>Int32?

        public CAnnotationLine()
        {
            mAnnotationCode = 0;
            mAnnotationTimeTck = 0;
            mAnnotationSubCode = 0;
            mAnnotationNumField = 0;
            mChannel = 0;
            mbExtraValueSet = false;
            mExtraValue = 0;
        }
        public CAnnotationLine(UInt16 ACode, UInt32 AcurTicks, UInt16 ACurNum, UInt16 ACurChannel)
        {
            mSetAll(ACode, AcurTicks, ACurNum, ACurChannel);
        }
        public CAnnotationLine(UInt16 ACode, UInt32 AcurTicks, UInt16 ACurNum, UInt16 ACurChannel, double AExtraValue)
        {
            mSetAllValue(ACode, AcurTicks, ACurNum, ACurChannel, AExtraValue);
        }

        public void mSetAll(UInt16 ACode, UInt32 AcurTicks, UInt16 ACurNum, UInt16 ACurChannel)
        {
            mAnnotationCode = ACode;
            mAnnotationTimeTck = AcurTicks;
            mAnnotationSubCode = 0;
            mAnnotationNumField = ACurNum;
            mChannel = ACurChannel;

            mbExtraValueSet = false;
            mExtraValue = 0; ;
        }
        public void mSetAllValue(UInt16 ACode, UInt32 AcurTicks, UInt16 ACurNum, UInt16 ACurChannel, double AExtraValue)
        {
            mAnnotationCode = ACode;
            mAnnotationTimeTck = AcurTicks;
            mAnnotationSubCode = 0;
            mAnnotationNumField = ACurNum;
            mChannel = ACurChannel;

            mbExtraValueSet = true;
            mExtraValue = AExtraValue;
        }

        public static bool sbIsBeat(UInt16 ACode)
        {
            switch (ACode)
            {
                case 0: return false; // "";
                case 1: return true; // "N";
                case 2: return true; //"L";
                case 3: return true; //"R";
                case 4: return true; //"a";
                case 5: return true; //"V";
                case 6: return true; //"F";
                case 7: return true; //"J";
                case 8: return true; //"A";
                case 9: return true; //"S";
                case 10: return true; //"E";
                case 11: return true; //"j";
                case 12: return true; //"/";
                case 13: return true; //"Q";
                case 14: return false; //"~";
                //case 15: return "";
                case 16: return false; //"|";
                //                case 17: return "";
                case 18: return true; //"s";
                case 19: return false; //"T";
                case 20: return false; //"*";
                case 21: return false; //"D";
                case 22: return false; //"\"";
                case 23: return false; //"=";
                case 24: return false; //"p";
                case 25: return false; //"B";
                case 26: return false; //"^";
                case 27: return false; //"t";
                case 28: return false; //"+";
                case 29: return false; //"u";
                case 30: return true; //"?";
                case 31: return false; //"!";
                case 32: return false; //"[";
                case 33: return false; //"]";
                case 34: return true; //"e";
                case 35: return true; // "n";
                case 36: return false; // "@";
                case 37: return false; //"x";
                case 38: return true; //"f";
                case 39: return false; //"(";
                case 40: return false; //")";
                case 41: return true; //"r";
                                      /*                case 42: return "";
                                                      case 43: return "";
                                                      case 44: return "";
                                                      case 45: return "";
                                                      case 46: return "";
                                                      case 47: return "";
                                                      case 48: return "";
                                                      case 49: return "";
                                                      case 50: return "";
                                                      case 51: return "";
                                                      case 52: return "";
                                                      case 53: return "";
                                                      case 54: return "";
                                                      case 55: return "";
                                                      case 56: return "";
                                                      case 57: return "";
                                                      case 58: return "";
                                                      case 59: return ";";
                                                      case 60: return "<";
                                                      case 61: return "=";
                                                      case 62: return "";
                                                      case 63: return "";
                                      */
                                      /*
                                       * Sirona old:
                                       42 - Lead off detected 
                                       43 - End of lead off 
                                       44 - Noisy signal detected 
                                       45 - End of noisy signal 
                                       46 - Event start (Used during holter recording) 
                                       47 - Event end (Used during holter recording)

                                      Sirona Rev 1.1
                                      42 – Change in signal type. Each non-zero bit in the ‘subtyp’ field indicates that the corresponding signal is in a lead-off state 
                                              (the least significant bit corresponds to signal 0). 
                                      43 – Patient initiated event, corresponding to when the patient pressed the Record button.
                                      44 – Arrhythmia start, with ‘subtyp’ field set to ‘t’, ‘b’, ‘p’, or ‘a’ for tachy, brady, pause, or afib respectively. (event mode only) 
                                      45 – Arrhythmia end, with ‘subtyp’ field set to ‘t’, ‘b’, ‘p’, or ‘a’ for tachy, brady, pause, or afib respectively. (event mode only) 

                                       * */
                case 50: return false;  // normal beat must be added first if not present  true;
                case 51: return false;
                case 52: return false;
            }
            return false;
        }
        public static string sGetAnnotationLabel(UInt16 ACode, UInt16 ASubCode)
        {
            switch (ACode)
            {
                case 0: return "NOTQRS";
                case 1: return "NORMAL";
                case 2: return "LBBB";
                case 3: return "RBBB";
                case 4: return "ABERR";
                case 5: return "PVC";
                case 6: return "FUSION";
                case 7: return "NPC";
                case 8: return "APC";
                case 9: return "SVPB";
                case 10: return "VESC";
                case 11: return "NESC";
                case 12: return "PACE";
                case 13: return "UNKNOWN";
                case 14: return "NOISE";
                //                case 15: return "";
                case 16: return "ARFCT";
                //                case 17: return "";
                case 18: return "STCH";
                case 19: return "TCH";
                case 20: return "SYSTOLE";
                case 21: return "DIASTOLE";
                case 22: return "NOTE";
                case 23: return "MEASURE";
                case 24: return "PWAVE";
                case 25: return "BBB";
                case 26: return "PACESP";
                case 27: return "TWAVE";
                case 28: return "RHYTHM";
                case 29: return "UWAVE";
                case 30: return "LEARN";
                case 31: return "FLWAV";
                case 32: return "VFON";
                case 33: return "VFOFF";
                case 34: return "AESC";
                case 35: return "SVESC";
                case 36: return "LINK";
                case 37: return "NAPC";
                case 38: return "PFUS";
                case 39: return "WFON";
                case 40: return "WFOFF";
                case 41: return "RONT";
                /*             case 42: return "";
                               case 43: return "";
                               case 44: return "";
                               case 45: return "";
                               case 46: return "";
                               case 47: return "";
                               case 48: return "";
                               case 49: return "";
                               case 50: return "";
                               case 51: return "";
                               case 52: return "";
                               case 53: return "";
                               case 54: return "";
                               case 55: return "";
                               case 56: return "";
                               case 57: return "";
                               case 58: return "";
               */
                case 42: return "SLOff" + sLeadOffLetters(ASubCode);
                case 43: return "SMan";
                case 44: return "SAS-" + sArithmiaLetter(ASubCode);
                case 45: return "SAE-" + sArithmiaLetter(ASubCode);
                case 46: return "SES";
                case 47: return "SEE";

                case 50: return "PSys";
                case 51: return "PDias";
                case 52: return "PTran";
                // standard
                case 59: return "SKIP";
                case 60: return "NUM";
                case 61: return "SUB";
                case 62: return "CHN";
                case 63: return "AUX";
            }
            return "[" + ACode.ToString() + "]";

        }

        public static string sGetAnnotationLetter(UInt16 ACode, UInt16 ASubCode)
        {
            switch (ACode)
            {
                case 0: return "";
                case 1: return "N";
                case 2: return "L";
                case 3: return "R";
                case 4: return "a";
                case 5: return "V";
                case 6: return "F";
                case 7: return "J";
                case 8: return "A";
                case 9: return "S";
                case 10: return "E";
                case 11: return "j";
                case 12: return "/";
                case 13: return "Q";
                case 14: return "~";
                //case 15: return "";
                case 16: return "|";
                //                case 17: return "";
                case 18: return "s";
                case 19: return "T";
                case 20: return "*";
                case 21: return "D";
                case 22: return "\"";
                case 23: return "=";
                case 24: return "p";
                case 25: return "B";
                case 26: return "^";
                case 27: return "t";
                case 28: return "+";
                case 29: return "u";
                case 30: return "?";
                case 31: return "!";
                case 32: return "[";
                case 33: return "]";
                case 34: return "e";
                case 35: return "n";
                case 36: return "@";
                case 37: return "x";
                case 38: return "f";
                case 39: return "(";
                case 40: return ")";
                case 41: return "r";
                // special
                /*                case 42: return "";
                                case 43: return "";
                                case 44: return "";
                                case 45: return "";
                                case 46: return "";
                                case 47: return "";
                                case 48: return "";
                                case 49: return "";
                                case 50: return "";
                                case 51: return "";
                                case 52: return "";
                                case 53: return "";
                                case 54: return "";
                                case 55: return "";
                                case 56: return "";
                                case 57: return "";
                                case 58: return "";
                                case 59: return ";";
                                case 60: return "<";
                                case 61: return "=";
                                case 62: return "";
                                case 63: return "";
                */
                /*
                 * Sirona old:
                 42 - Lead off detected 
                 43 - End of lead off 
                 44 - Noisy signal detected 
                 45 - End of noisy signal 
                 46 - Event start (Used during holter recording) 
                 47 - Event end (Used during holter recording)

                Sirona Rev 1.1
                42 – Change in signal type. Each non-zero bit in the ‘subtyp’ field indicates that the corresponding signal is in a lead-off state 
                        (the least significant bit corresponds to signal 0). 
                43 – Patient initiated event, corresponding to when the patient pressed the Record button.
                44 – Arrhythmia start, with ‘subtyp’ field set to ‘t’, ‘b’, ‘p’, or ‘a’ for tachy, brady, pause, or afib respectively. (event mode only) 
                45 – Arrhythmia end, with ‘subtyp’ field set to ‘t’, ‘b’, ‘p’, or ‘a’ for tachy, brady, pause, or afib respectively. (event mode only) 

                 * */
                case 42: return "¢" + sLeadOffLetters(ASubCode);
                case 43: return "§";
                case 44: return "«" + sArithmiaLetter(ASubCode);
                case 45: return sArithmiaLetter(ASubCode) + "»";
                case 46: return "≤";
                case 47: return "≥";

                //  DVXSPO pleth 

                case 50: return "¯";
                case 51: return "_";
                case 52: return "¦";
                    // standard

            }
            return "'" + ACode.ToString() + "'";
        }
        private static string sArithmiaLetter(UInt16 ASubCode)
        {
            string s = "";

            if (ASubCode > 32)
            {
                s += (Char)ASubCode;
            }
            else
            {
                s = "<" + ASubCode.ToString() + ">";
            }
            return s;
        }
        private static string sLeadOffLetters(UInt16 AFlags)
        {
            string s = "";
            UInt16 mask = 1;

            for (int i = 0; i < 16; ++i)
            {
                if (0 != (AFlags & mask))
                {
                    s += i.ToString("X");
                }
            }
            return s;
        }

        public static string sGetAnnotationName(UInt16 ACode, UInt16 ASubCode)
        {
            switch (ACode)
            {
                case 0: return "EOF or Not a QRS code";
                case 1: return "normal beat ";
                case 2: return "left bundle branch block beat";
                case 3: return "right bundle branch block beat";
                case 4: return "aberrated atrial premature beat";
                case 5: return "premature ventricular contraction";
                case 6: return "fusion of ventricular and normal beat";
                case 7: return "nodal (junctional) premature beat";
                case 8: return "atrial premature contraction ";
                case 9: return "premature or ectopic supraventricular beat";
                case 10: return "ventricular escape beat";
                case 11: return "nodal (junctional) escape beat";
                case 12: return "paced beat";
                case 13: return "unclassifiable beat";
                case 14: return "signal quality change";
                //                case 15: return "";
                case 16: return "isolated QRS-like artifact";
                //                case 17: return "";
                case 18: return "ST change ";
                case 19: return "T-wave change ";
                case 20: return "systole";
                case 21: return "diastole";
                case 22: return "comment annotation";
                case 23: return "measurement annotation";
                case 24: return "P-wave peak";
                case 25: return "left or right bundle branch block";
                case 26: return "non-conducted pacer spike";
                case 27: return "T-wave peak";
                case 28: return "rhythm change";
                case 29: return "U-wave peak";
                case 30: return "learning";
                case 31: return "ventricular flutter wave";
                case 32: return "start of ventricular flutter/fibrillation";
                case 33: return "end of ventricular flutter/fibrillation";
                case 34: return "atrial escape beat";
                case 35: return "supra ventricular escape beat";
                case 36: return "link to external data (aux contains URL)";
                case 37: return "non-conducted P-wave (blocked APB)";
                case 38: return "fusion of paced and normal beat";
                case 39: return "waveform onset";
                case 40: return "waveform end";
                case 41: return "R-on-T premature ventricular contraction";
                // special           
                /*                case 42: return "";
                            case 43: return "";
                            case 44: return "";
                            case 45: return "";
                            case 46: return "";
                            case 47: return "";
                            case 48: return "";
                            case 49: return "";
                            case 50: return "";
                            case 51: return "";
                            case 52: return "";
                            case 53: return "";
                            case 54: return "";
                            case 55: return "";
                            case 56: return "";
                            case 57: return "";
                            case 58: return "";
            */
                case 42: return "LeadOff=" + sLeadOffLetters(ASubCode);
                case 43: return "Manual";
                case 44: return "Arithm-Start-" + sArithmiaLetter(ASubCode);
                case 45: return "Arithm-End-" + sArithmiaLetter(ASubCode);
                case 46: return "Event-Start";
                case 47: return "Event-End";

                //  DVXSPO pleth 

                case 50: return "PlethSystole";
                case 51: return "PlethDiastole";
                case 52: return "PlethTrans";
                // standard
                case 59: return "Skip time";
                case 60: return "Num field";
                case 61: return "Sub type";
                case 62: return "Channel";
                case 63: return "Auxillary Info";
            }
            return "<" + ACode.ToString() + ">";
        }
    }

    public class CAnnotation
    {

        public List<CAnnotationLine> mAnnotationsList;

        public DateTime mBaseDeviceDT;    // device time 
        public float mUnitT;
        public string mFileName;
        public string mAnnotationExtention;
        public uint mMaxNrChannels;
        public uint mLastTimeTck;

        public CAnnotation()
        {
            mDispose();
        }

        public void mDispose()
        {
            mAnnotationsList = null;    // indirectly dispose array

            mUnitT = 0.0F;
            mFileName = null;
            mAnnotationExtention = "";
            mMaxNrChannels = 0;
            mLastTimeTck = 0;
        }

        public bool mSetAnnotationTime(DateTime ABaseDateTime, Int16 ATimeZoneOffsetMin, float AUnitT)
        {
            mBaseDeviceDT = ABaseDateTime.AddMinutes(ATimeZoneOffsetMin);
            mUnitT = AUnitT;

            return mUnitT > 0.0F;
        }
        public float mGetTimeSec(uint ATimeTck)
        {

            return ATimeTck * mUnitT;
        }

        public DateTime mGetDeviceTimeDT(uint ATimeTck)
        {
            DateTime dt = mBaseDeviceDT;

            return dt.AddSeconds(ATimeTck * mUnitT);
        }
        public float mGetLastTimeSec()
        {

            return mLastTimeTck * mUnitT;
        }

        public void mNewList()
        {
             mAnnotationsList = new List<CAnnotationLine>();
        }
        public bool mAddTail( CAnnotationLine AAnnotationLine)
        {
            bool bAdded = false;

            if (AAnnotationLine != null)
            {
                if (mAnnotationsList == null)
                {
                    mAnnotationsList = new List<CAnnotationLine>();
                }
                if (mAnnotationsList != null)
                {
                    mAnnotationsList.Add(AAnnotationLine);
                    bAdded = true;
                }
            }
            return bAdded;
        }
        public bool mbAddSorted(CAnnotationLine AAnnotationLine)
        {
            bool bAdded = false;

            if (AAnnotationLine != null)
            {
                if (mAnnotationsList == null)
                {
                    mAnnotationsList = new List<CAnnotationLine>();

                    mAnnotationsList.Add(AAnnotationLine);
                    bAdded = true;
                }
                else
                {
                    int pos = 0;
                    UInt32 tTick = AAnnotationLine.mAnnotationTimeTck;

                    foreach (CAnnotationLine ann in mAnnotationsList)
                    {
                        if( tTick < ann.mAnnotationTimeTck )
                        {
                            mAnnotationsList.Insert(pos, AAnnotationLine);   // add sorted by annotation time 
                            bAdded = true;
                            break;
                        }
                        ++pos;

                    }
                    if (false == bAdded )
                    {
                        mAnnotationsList.Add(AAnnotationLine);  // time after list, add to tail
                        bAdded = true;
                    }
                }
            }
            return bAdded;
        }


        public float mGetEventTimeSec(out UInt16 ACode, out UInt16 AValue)
        {
            UInt32 tTck = mLastTimeTck;
            UInt16 code = 0;
            UInt16 value = 0;
            uint iOff = 0xFFFFFFFF;
            uint i43 = iOff;
            uint i44 = iOff;
            uint i45 = iOff;
            UInt16 value43 = 0;
            UInt16 value44 = 0;
            UInt16 value45 = 0;


            /*         43 – Patient initiated event, corresponding to when the patient pressed the Record button.
                      44 – Arrhythmia start, with ‘subtyp’ field set to ‘t’, ‘b’, ‘p’, or ‘a’ for tachy, brady, pause, or afib respectively. (event mode only) 
                      45 – Arrhythmia end, with ‘subtyp’ field set to ‘t’, ‘b’, ‘p’, or ‘a’ for tachy, brady, pause, or afib respectively. (event mode only) 
                      */
            if (mAnnotationsList != null)
            {
                foreach (CAnnotationLine ann in mAnnotationsList)
                {
                    code = ann.mAnnotationCode;

                    if (code == 43) // patient manual event
                    {
                        if (i43 == iOff)
                        {
                            i43 = ann.mAnnotationTimeTck;       // first manual event
                            value43 = ann.mAnnotationSubCode;
                        }
                    }
                    else if (code == 44)   // start event
                    {
                        if (i44 == iOff)
                        {
                            i44 = ann.mAnnotationTimeTck;   // first start event 
                            value44 = ann.mAnnotationSubCode;
                        }
                    }
                    else if (code == 45) // end event
                    {
                        if (i45 == iOff)
                        {
                            i45 = ann.mAnnotationTimeTck; // first end event
                            value45 = ann.mAnnotationSubCode;
                        }
                    }
                }
                code = 0;
                if (i43 != iOff)
                {
                    code = 43;
                    tTck = i43;
                    value = value43;
                }
                else if (i44 != iOff)
                {
                    code = 44;
                    tTck = i44;
                    value = value44;
                }
                else if (i45 != iOff)
                {
                    code = 45;
                    tTck = i45;
                    value = value45;
                }
            }

            ACode = code;
            AValue = value;
            return tTck * mUnitT;
        }

        public bool mbReadAnnotationFile(string AFilePath, string AAnnotationExtention, DateTime ABaseDateTime, Int16 ATimeZoneOffsetMin, float AUnitT)
        {
            string fileName = Path.ChangeExtension(AFilePath, AAnnotationExtention);
            bool bOk = true;    // return true even when file does not exist

            mDispose();
            mBaseDeviceDT = ABaseDateTime.AddMinutes(ATimeZoneOffsetMin);
            mUnitT = AUnitT;
            mFileName = Path.GetFileNameWithoutExtension(fileName);
            mAnnotationExtention = AAnnotationExtention;

            try
            {
                if (File.Exists(fileName))
                {

                    FileStream fs = File.OpenRead(fileName);

                    int fileSize = (int)fs.Length;

                    if (fileSize > 0)
                    {
                        uint n = 0;
                        mAnnotationsList = new List<CAnnotationLine>();

                        if (mAnnotationsList != null)
                        {
                            int buffer = 0;
                            int b, i, c, t;
                            int nBitsCode = 6;
                            int nBitsData = 16 - nBitsCode;
                            int maskCode = (int)((1 << (nBitsCode)) - 1);
                            int maskTime = (int)((1 << nBitsData) - 1);
                            int curTicks = 0;
                            int curNum = 0;
                            int curChannel = 0;
                            UInt32 nAnn = 0;
                            CAnnotationLine curAnn = null;

                            bool bLogStr = true;
                            bool bLogStrDump = false;
                            string logStr = "ATR File = " + fileSize.ToString() + " " + fileName + "\r\n";
                            string logLine = "";

                            while ((b = fs.ReadByte()) >= 0)
                            {
                                buffer = b & 0x00FF;

                                if ((b = fs.ReadByte()) >= 0)
                                {
                                    buffer |= (b & 0x00FF) << 8;
                                    c = (buffer >> nBitsData) & maskCode;
                                    t = buffer & maskTime;
                                    if (bLogStr)
                                    {
                                        logLine = curTicks.ToString("D8") + " " + c.ToString("D2");
                                        logLine += c >= ' ' ? (char)(c) : ' ';
                                        logLine += " " + t.ToString("D6") + ":";

                                        string codeStr = CAnnotationLine.sGetAnnotationLetter((UInt16)c, (UInt16)t);

                                        if (codeStr != null && codeStr.Length > 0)
                                        {
                                            logLine += codeStr + (CAnnotationLine.sbIsBeat((UInt16)c) ? "+" : " ") + CAnnotationLine.sGetAnnotationLabel((UInt16)c, (UInt16)t) + " ";
                                        }
                                    }


                                    switch (c)
                                    {
                                        case 0:     // EOF -> do nothing
                                            break;
                                        case 22:    // NOTE read next <t> chars as text
                                            for (i = 0; i < t; ++i)
                                            {
                                                if ((b = fs.ReadByte()) >= 0)
                                                {
                                                    if (curAnn != null)
                                                    {
                                                        curAnn.mLabel += (char)(b & 0x00FF);
                                                    }
                                                }
                                            }
                                            if (bLogStr)
                                            {
                                                logLine += "Notes " + t.ToString() + " bytes " + (curAnn != null ? curAnn.mLabel : "");
                                            }
                                            if ((t & 1) == 1)
                                            {
                                                fs.ReadByte(); // read patch byte to make file index even
                                            }
                                            break;
                                        case 23:    // measure notes: 
                                            for (i = 0; i < t; ++i)
                                            {
                                                if ((b = fs.ReadByte()) >= 0)
                                                {
                                                    if (curAnn != null)
                                                    {
                                                        curAnn.mLabel += (char)(b & 0x00FF);
                                                    }
                                                }
                                            }
                                            if (bLogStr)
                                            {
                                                logLine += "Measure Notes " + t.ToString() + " bytes " + (curAnn != null ? curAnn.mLabel : "");
                                            }
                                            if ((t & 1) == 1)
                                            {
                                                fs.ReadByte(); // read patch byte to make file index even
                                            }
                                            break;
                                        case 59:    // SKIP time
                                            if (t == 0)
                                            {
                                                t = 0;
                                                // next 4 bytes is the time in ticks
                                                if ((b = fs.ReadByte()) >= 0) { t |= (b & 0x00FF) << 16; }
                                                if ((b = fs.ReadByte()) >= 0) { t |= (b & 0x00FF) << 24; }
                                                if ((b = fs.ReadByte()) >= 0) { t |= (b & 0x00FF); }
                                                if ((b = fs.ReadByte()) >= 0) { t |= (b & 0x00FF) << 8; }
                                                if (bLogStr)
                                                {
                                                    logLine += "Skip large time =" + t.ToString();
                                                }
                                            }
                                            else if (bLogStr)
                                            {
                                                logLine += "Skip small time =" + t.ToString();
                                            }
                                            curTicks += t;
                                            break;
                                        case 60:    // NUM: change number to <t>
                                            curNum = t;
                                            if (curAnn != null)
                                            {
                                                curAnn.mAnnotationNumField = (UInt16)curNum;
                                            }
                                            if (bLogStr)
                                            {
                                                logLine += "Number =" + t.ToString();
                                                if (curAnn == null) logLine += " no AnnLine";
                                            }
                                            break;
                                        case 61:    // SUB: sub code for current annotation;
                                            if (curAnn != null)
                                            {
                                                curAnn.mAnnotationSubCode = (UInt16)t;
                                            }
                                            if (bLogStr)
                                            {
                                                logLine += "SubCode=" + t.ToString();
                                                if (curAnn == null) logLine += " no AnnLine";
                                            }
                                            break;
                                        case 62:    // CHN: channel change to channel
                                            curChannel = t;
                                            if (curAnn != null)
                                            {
                                                curAnn.mChannel = (UInt16)curChannel;
                                            }
                                            if (bLogStr)
                                            {
                                                logLine += "Channel= " + curChannel.ToString();
                                            }
                                            if (curChannel > mMaxNrChannels)
                                            {
                                                mMaxNrChannels = (UInt16)curChannel;
                                                if (curAnn == null) logLine += " no AnnLine";
                                            }
                                            break;
                                        case 63:    // AUX: next <n> bytes is data                            
                                            for (i = 0; i < t; ++i)
                                            {
                                                if ((b = fs.ReadByte()) >= 0)
                                                {
                                                    if (curAnn != null)
                                                    {
                                                        if (i == 0)
                                                        {
                                                            curAnn.mLabel += '$';
                                                        }
                                                        else
                                                        {
                                                            curAnn.mLabel += ' ';
                                                        }
                                                        curAnn.mLabel += (b & 0x00FF).ToString("XX");
                                                    }
                                                }
                                            }
                                            if (bLogStr)
                                            {
                                                logLine += "Data " + t.ToString() + " bytes " + (curAnn != null ? curAnn.mLabel : "");
                                            }
                                            if ((t & 1) == 1)
                                            {
                                                fs.ReadByte(); // read patch byte to make file index even
                                            }
                                            break;
                                        default:    // code for next annotation at <t> ticks from current

                                            curTicks += t;  // move time
                                            curAnn = new CAnnotationLine((UInt16)c, (UInt32)curTicks, (UInt16)curNum, (UInt16)curChannel);
                                            if (curAnn != null)
                                            {
                                                mAnnotationsList.Add(curAnn);
                                                ++nAnn;
                                            }
                                            if (bLogStr)
                                            {
                                                logLine += "other, skip time " + t.ToString();
                                            }
                                            break;
                                    }

                                    if (bLogStr)
                                    {
                                        logStr += logLine + "\r\n";
                                    }
                                }
                            }
                            mLastTimeTck = (uint)curTicks;
                            bOk = nAnn > 0;
                            if (false == bOk)
                            {
                                CProgram.sLogInfo("Annotation file is empty: " + fileName);
                            }
                            if (bLogStrDump)
                            {
                                logStr += "lastTck = " + mLastTimeTck.ToString() + ", nAnn = " + nAnn.ToString() + "\r\n";
                                CProgram.sLogInfo("ATR dump\r\n" + logStr);
                            }
                        }
                    }
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("ATR Read failed " + fileName, ex);
            }
            finally
            {
            }

            return bOk;
        }
        public bool mbWriteAnnotationTextFile(string AFilePath)
        {
            bool bOk = false;
            string fileName = Path.ChangeExtension(AFilePath, mAnnotationExtention + "Txt");

            try
            {
                if (mAnnotationsList != null && mAnnotationsList.Count > 0)
                {
                    StreamWriter sw = new StreamWriter(fileName);

                    if (sw != null)
                    {
                        DateTime dt;
                        float tSec;
                        uint tTck;
                        UInt16 code, subCode;
                        string s;


                        foreach (CAnnotationLine ann in mAnnotationsList)
                        {
                            tTck = ann.mAnnotationTimeTck;
                            code = ann.mAnnotationCode;
                            subCode = ann.mAnnotationSubCode;
                            dt = mGetDeviceTimeDT(tTck);
                            tSec = mGetTimeSec(tTck);
                            s = dt.ToString("yyyy/MM/dd HH:mm:ss.ffff") + "\t" + tSec.ToString("0.000") + "\t" + tTck.ToString();
                            s += "\t" + CAnnotationLine.sGetAnnotationLetter(code, subCode) + "\t" + CAnnotationLine.sGetAnnotationName(code, subCode) + "\t[" + code.ToString();
                            s += "]\t" + ann.mAnnotationSubCode.ToString() + "\t" + ann.mAnnotationNumField.ToString();
                            s += "\t" + ann.mLabel;

                            sw.WriteLine(s);
                        }
                    }
                    sw.Close();
                }
            }
            catch (Exception)
            {
            }
            finally
            {
            }
            return bOk;
        }
        public UInt32 mCalcMinMaxValue(out double ArMinValue, out double ArMaxValue, out double ArMeanValueValue, UInt16 ABeatCode)
        {
            UInt32 n = 0;
            double min = 1e9;
            double max = -1e9;
            double sum = 0;

            try
            {
                if (mAnnotationsList != null && mAnnotationsList.Count > 0)
                {
                    foreach (CAnnotationLine ann in mAnnotationsList)
                    {
                        if (ann.mbExtraValueSet)
                        {
                            if (ABeatCode == 0 || ABeatCode == ann.mAnnotationCode)
                            {
                                if (ann.mExtraValue < min) min = ann.mExtraValue;
                                if (ann.mExtraValue > max) max = ann.mExtraValue;
                                sum += ann.mExtraValue;
                                ++n;
                            }
                        }

                    }
                }
            }
            catch (Exception)
            {

            }

            ArMinValue = min;
            ArMaxValue = max;
            ArMeanValueValue = n == 0 ? 0 : sum / n;

            return n;
        }
    }
}
