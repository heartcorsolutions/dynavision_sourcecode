﻿using Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Nonin XPOD integration guide 7602-000-10-1.pdf
 * 
 *  plethysmographic waveform (pulse waveform) from pulse oximeter infra-red signal. 

 read nonin file: Serial Data Format #7
This data format provides the same information as Data Format 2, except that the waveform value
provides the full resolution of 16 bits instead of 8 bits. This data format must be selected by hardware
resistor (4.3K ± 5%) select or software select.
Serial 9600 Baud, 8 data bits, One Start bit (Start bit =0), One Stop bit (Stop bit = 1), No Parity.
Packet Description
A frame consists of 5 bytes; a packet consists of 25 frames. Three packets (75 frames) are transmitted
each second.
Packet Frame:
Byte 1 Byte 2 Byte 3 Byte 4 Byte 5
1 STATUS PLETH_MSB PLETH_LSB PR MSB CHK
2 STATUS PLETH_MSB PLETH_LSB PR LSB CHK
3 STATUS PLETH_MSB PLETH_LSB SpO 2 CHK
4 STATUS PLETH_MSB PLETH_LSB SREV CHK
5 STATUS PLETH_MSB PLETH_LSB reserved CHK
6 STATUS PLETH_MSB PLETH_LSB reserved CHK
7 STATUS PLETH_MSB PLETH_LSB reserved CHK
8 STATUS PLETH_MSB PLETH_LSB STAT2 CHK
9 STATUS PLETH_MSB PLETH_LSB SpO 2 -D CHK
10 STATUS PLETH_MSB PLETH_LSB SpO 2 Fast CHK
11 STATUS PLETH_MSB PLETH_LSB SpO 2 B-B CHK
12 STATUS PLETH_MSB PLETH_LSB reserved CHK
13 STATUS PLETH_MSB PLETH_LSB reserved CHK
14 STATUS PLETH_MSB PLETH_LSB E-PR MSB CHK
15 STATUS PLETH_MSB PLETH_LSB E-PR LSB CHK
16 STATUS PLETH_MSB PLETH_LSB E-SpO 2 CHK
17 STATUS PLETH_MSB PLETH_LSB E-SpO 2 -D CHK
18 STATUS PLETH_MSB PLETH_LSB reserved CHK
19 STATUS PLETH_MSB PLETH_LSB reserved CHK
20 STATUS PLETH_MSB PLETH_LSB PR-D MSB CHK
21 STATUS PLETH_MSB PLETH_LSB PR-D LSB CHK
22 STATUS PLETH_MSB PLETH_LSB E-PR-D MSB CHK
23 STATUS PLETH_MSB PLETH_LSB E-PR-D LSB CHK
24 STATUS PLETH_MSB PLETH_LSB reserved CHK
25 STATUS PLETH_MSB PLETH_LSB reserved CHK
Notes:
Byte number 1 in each frame is greater than 127.
Reserved bytes are undefined (range of 0 to 255).

Byte 1 – STATUS BYTE
This byte provides status information at a rate of 1/75 of a second.
Range: 128 to 255 B7 a;ways 1
Byte 1 - Status
BIT7 BIT6 BIT5 BIT4 BIT3 BIT2 BIT1 BIT0
1 SNSD ARTF OOT SNSF YPRF SYNC RPRF GPRF
*Note: Bit 7 is always set.
The following are all active high:
SNSD: Sensor Disconnect Sensor is not connected to oximeter or sensor is inoperable.
ARTF: Artifact Indicates artifact condition of each pulse (occurs only during pulse).
OOT: Out Of Track An absence of consecutive good pulse signals.
SNSA: Sensor Alarm Device is providing unusable data for analysis (set when the finger is removed or sensor is disconnected).
RPRF: *Red Perfusion Amplitude representation of low/no pulse signal (occurs only during pulse).
YPRF: *Yellow Perfusion Amplitude representation of low/marginal signal quality (occurs only during pulse).
GPRF: *Green Perfusion Amplitude representation of high signal quality (occurs only during pulse).
SYNC: Frame Sync = 1 to Frame 1 (=0 on frames 2 through 25).
* The oximeter reports each pulse by setting/clearing the RPRF and GPRF bits for a period of 12 frames (160 ms).
The table below describes the condition and state of the pulse perfusion bits.
Condition                   RPRF    GPRF 
Green – high pulse signal   0       1
Yellow – low/marginal pulse 1       1
Red – low/no pulse signal   1       0


Byte 2 & 3 – PLETH BYTE
These two bytes consist of a 16 bit plethysmographic waveform (pulse waveform).
Range: 0 to 65535 (MSB:LSB )    Byte 2 = MSB Pulse Waveform, Byte 3 = LSB Pulse Waveform
Pulse waveform value = (Byte 2 decimal value * 256) + Byte 3 decimal value

Byte 4 – FLOAT BYTE => Info
This byte is used for SpO 2 , Pulse Rate, and information that can be processed at a rate of 1/3 of a second.
Range: 00 to 127 B7= always 0
SREV: Oximeter Firmware Revision Level
STAT2: Status Byte 2 
Standard Mode - Formatted for Recording Purposes:
These values are formatted for recording purposes and are updated every 1/3 of second. When the
sensor is removed from the site, these values will be formatted with the missing data value. The following
output options are available in standard mode:
PR: 4-beat Pulse Rate Average (PR MSB1|PR MSB0 | PR LSB6 |...| PR LSB0 ) 9 bit 0..511 pulse per second
E-PR: 8-beat Pulse Rate Extended Average (see PR)
SpO2 : 4-beat SpO 2 Average (B6..B0) 7 bit 0...127 SpO2%
E- SpO2 : 8-beat SpO 2 Extended Average (See SpO2)
SpO 2 Fast: 4-beat Average optimized for fast responding (See SpO2)
SpO 2 B-B: Beat to Beat value – No Average (See SpO2)
When SpO 2 and PR cannot be computed, the system will send a missing data indicator. For missing data,
the PR equals 511 and the SpO 2 equals 127. The missing data could be result of these conditions:
1. Sensor is positioned improperly.
2. Sensor was removed prior to a reading.
3. Signal at the sensor site is not discernable. Warm the site or choose a different site.

Byte 5 – CHK
This byte is used for the checksum of bytes 1 through 4.
Range: 00 to 255
CHK: Checksum = (Byte 1) + (Byte 2) + (Byte 3) + (Byte 4) modulo 256

*/

namespace EventBoard.Event_Base
{
    public enum DSpO2FrameReadResult
    {
        None,               // byte read is not last checksum byte of frame
        First_Frame,   // byte read is checksum byte of first frame 1 (sync bit)
        Other_Frame,   // byte read is checksum byte of a frame  2..24
        Last_Frame,    // byte read is checksum byte of last frame 25
        Overflow_Frame
    }

    public enum DSpO2FrameSequence
    {
        None = 0,   // no frame yet
        PR_MSB,     //  1   4-beat Pulse Rate Average
        PR_LSB,     //  2
        SpO2,       //  3   4-beat SpO 2 Average
        SREV,       //  4   software revision
        Reserved5,  //  5
        Reserved6,  //  6
        Reserved7,  //  7
        STAT2,      //  8   Status byte 2: B5=SPA high quality status
        SpO2_D,     //  9   Display 10sec: 
        SpO2_Fast,  // 10   4-beat Average SpO2 optimized for fast responding
        SpO2_BB,    // 11   Beat to Beat SpO2 value – No Average
        Reserved12, // 12
        Reserved13, // 13
        EPR_MSB,    // 14
        EPR_LSB,    // 15
        ESpO2,      // 16   8-beat SpO 2 Extended Average
        ESpO2_D,    // 17   Display 10sec: 8-beat SpO 2 Extended Average
        Reserved18, // 18
        Reserved19, // 19
        PRD_MSB,    // 20   Display 10sec: 4-beat Pulse Rate Average
        PRD_LSB,    // 21
        EPRD_MSB,   // 22   Display 10sec: 8-beat Pulse Rate Extended Average
        EPRD_LSB,   // 23
        Reserved24, // 24
        Reserved25, // 25
        Overflow    // >= 26 overflow packet with frames
    }

    public class CSpO2Nonin
    {
        const UInt16 _cNrBytesInFrame = 5;
        const UInt16 _cNrFramesInPacket = 25;
        const UInt16 _cNrBytesInPacket = _cNrBytesInFrame * _cNrFramesInPacket;
        const UInt16 _cNrPacketsPerSecond = 3;
        const UInt16 _cNrFramesPerSecond = 75;
        const double _cFrameUnitSec = 1.0 / _cNrFramesPerSecond;
        const UInt32 _cBufferSize = 3 * _cNrBytesInPacket;
        const UInt32 _cFrameSeqBad = _cNrFramesInPacket + 1;

        const byte _cStatusB7_Always_ON = 0x80;    // always on
        const byte _cStatusB6_SNSD = 0x40;    // Sensor Disconnect Sensor is not connected to oximeter or sensor is inoperable.
        const byte _cStatusB5_ARTF = 0x20;    // Artifact Indicates artifact condition of each pulse (occurs only during pulse).
        const byte _cStatusB4_OOT= 0x10;    // Out Of Track An absence of consecutive good pulse signals.
        const byte _cStatusB3_SNSA = 0x08;    //Sensor Alarm Device is providing unusable data for analysis (bad signal noise, no finger)
        const byte _cStatusB2_LOW__SIGNAL = 0x04;    //Red – low/no pulse signa
        const byte _cStatusB2_MEDIUM_SIGNAL = 0x06;    //Yellow – low/marginal pulse signal
        const byte _cStatusB1_HIGH_SIGNAL = 0x02;    //Green – high pulse signal 
        const byte _cStatusB0_SYNC = 0x01;    // sync bit indicating Frame 0
        const byte _cStatusSignalFlags = 0x06;    //Yellow – low/marginal pulse signal
        const byte _cStatusErrorFlags = 0x78;   // if one of the flags is active data is not reliable

        const byte _cInfoB7_Always_Off = 0x80;    // always on

        const Int32 _cBadInfoValue = -10;
        const Int32 _cBadPlethValue = -100;

        const string _cCsvTab = "\t";

        // frame buffer

        private byte[] _mBuffer = null; // last 3 frames
        private UInt32 _mBufferIndex = 0;    // last store space
        private UInt32 _mTotalBytes = 0;
        private UInt32 _mUsedBytes = 0;

        // frame

        private byte[] _mFrameBuffer = null; // last 5 bytes
        private UInt32 _mFrameIndex = 0;     // index in buffer for current frame
        private UInt32 _mFrameSeqNr = _cFrameSeqBad;    // frame sequence number (ok 1.. 25)
        private byte _mFrameStatus = 0;     // frame status byte
        private Int32 _mFramePleth = _cBadPlethValue;    // frame pleth value
        private byte _mFrameInfo = 0;       // 
        private UInt32 _mFrameNextIndex = 0;     // index after frame
        private UInt32 _mFrameNextCount = 0;     // count bytes in new 'frame'
        private bool _mbFrameSensorOk = false;   // good measurement
        private UInt32 _mFramesCount = 0;       // total number of frames => time from start
        private UInt32 _mFramesOkCount = 0;     // number of frames that are ok (no Error)
        private UInt32 _mFramesSignalLow = 0;     // number of frames with a signal strength Low
        private UInt32 _mFramesSignalMedium = 0;     // number of frames with a signal strength Medium
        private UInt32 _mFramesSignalHigh = 0;     // number of frames with a signal strength High

        // packet
        private UInt32 _mPacketIndex = 0;     // index in buffer for current packet
        private UInt32 _mPacketCount = 0;     // total number of full packets

        private Int16 _mLastPulseRate = -1;
        private Int16 _mLastSpO2 = -1;

        // First file

        private DateTime _mFirstFileUTC = DateTime.MinValue;    // base date time
        private string _mFirstFullName = "";
        private string _mFirstFileName = "";

        private bool _mbCsv = false;
        private string _mCsvFullName = "";
        private StreamWriter _mCsvWriter = null;

        private bool _mbDebugRead = true;
        private string _mDebugReadLog = "";
        // Multiple files

        public UInt16 _mFileCount = 0;
        public string _mFileFullName;
        public string _mFileName;
        public DateTime _mFileUTC;
        public UInt32 _mFileSize = 0;
        public UInt32 _mFileBytesAdded = 0;
        public UInt32 _mDeviceNr = 0;

        private UInt32 _mTotalFramesCount = 0;     // number of frames that are ok (no Error)
        private UInt32 _mTotalFramesOkCount = 0;     // number of frames that are ok (no Error)
        private UInt32 _mTotalFramesSignalLow = 0;     // number of frames with a signal strength Low
        private UInt32 _mTotalFramesSignalMedium = 0;     // number of frames with a signal strength Medium
        private UInt32 _mTotalFramesSignalHigh = 0;     // number of frames with a signal strength High
        private UInt32 _mTotalPacketCount = 0;     // total number of full packets


        public CSpO2Nonin(bool AbWrite2Csv)
        {
            _mbCsv = AbWrite2Csv;

        _mBuffer = new byte[_cBufferSize];
            _mFrameBuffer = new byte[_cNrBytesInFrame];

        }
         ~CSpO2Nonin()
        {
            mClose();
        }
        public void mClose()
        {
            if( null != _mCsvWriter)
            {
                _mCsvWriter.Flush();
                _mCsvWriter.Close();
                _mCsvWriter = null;
            }
        }
        UInt32 mBufferNextIndex(UInt32 AIndex)
        {
            UInt32 i = AIndex;
            ++i;
            if (i >= _cBufferSize)
            {
                i = 0;
            }
            return i;
        }
        UInt32 mBufferGetIndex(UInt32 AIndex, Int32 AOffset)
        {
            UInt32 i = (UInt32)(AIndex + _cBufferSize + AOffset);

            i = i % _cBufferSize;

            return i;
        }
        byte mBufferGetByte(UInt32 AIndex, Int32 AOffset)
        {
            UInt32 i = (UInt32)(AIndex + _cBufferSize + AOffset);

            i = i % _cBufferSize;

            return _mBuffer[i];
        }

        public DSpO2FrameReadResult mAddByte( byte AByte )
        {
            DSpO2FrameReadResult result = DSpO2FrameReadResult.None;
            UInt32 storeIndex = _mBufferIndex;
            string logLine;
            // fill buffer
            _mBuffer[storeIndex] = AByte;
            _mBufferIndex = mBufferNextIndex(_mBufferIndex);
            ++_mTotalBytes;
            ++_mFileBytesAdded;

            ++_mFrameNextCount;

            if (_mFrameNextCount >= _cNrBytesInFrame)
            {
                // posisble new frame -> do checksum test
                byte byte1 = mBufferGetByte(_mFrameNextIndex, 0);   // Status
                byte byte2 = mBufferGetByte(_mFrameNextIndex, 1);   // Pleth MSB
                byte byte3 = mBufferGetByte(_mFrameNextIndex, 2);   // Pleth LSB
                byte byte4 = mBufferGetByte(_mFrameNextIndex, 3);   // Info
                byte chkSumAdd = (byte)(byte1 + byte2 + byte3 + byte4);
                byte chkSumOr = (byte)(byte1 | byte2 | byte3 | byte4);
                byte chkSumXor = (byte)(byte1 ^ byte2 ^ byte3 ^ byte4);
                bool bFlagsOk = 0 != (byte1 & _cStatusB7_Always_ON) && 0 == (byte4 & _cInfoB7_Always_Off);
                bool bSync = 0 != (byte1 & _cStatusB0_SYNC);

                if (_mbDebugRead && ( _mFileBytesAdded < 100 || _mPacketCount <= 1))
                {
                    logLine = _mFileBytesAdded + "\t$" + AByte.ToString("X02") + "\t$"
                        + byte1.ToString("X02") + byte2.ToString("X02") + byte3.ToString("X02") + byte4.ToString("X02")
                        + "\t" + (bFlagsOk ? "ok" : "--") 
                        + (bSync ? "S": "_")
                        + "\tf=" + _mFrameSeqNr
                        + "\tCHK$" + AByte.ToString("X02") + (chkSumAdd == AByte ? "=" : "!") 
                        + "\tadd$" + chkSumAdd.ToString("X02")
                        + "\tor$" + chkSumOr.ToString("X02")
                        + "\txor$" + chkSumXor.ToString("X02")
                        ;

                    _mDebugReadLog += logLine + "\r\n";
                }

                if ( 0 == chkSumAdd && 0 == chkSumOr)
                {
                    // 0 0 0 0 0 empty frame => increase frame count to get time right
                    _mFrameStatus = 0;          // no write to csv
                    _mbFrameSensorOk = false;

                    _mFramePleth = _cBadPlethValue;
                    _mFrameInfo = 0;

                    result = DSpO2FrameReadResult.Overflow_Frame;

                    if( ++_mFrameSeqNr < _cFrameSeqBad )
                    {
                        _mFrameSeqNr = _cFrameSeqBad;
                    }
                }
                else if (chkSumAdd == AByte)
                {
                    // checksum OK
                    if (bFlagsOk)
                    {
                        // checksum OK and bits are ok => new frame
                        _mFrameStatus = byte1;
                        _mbFrameSensorOk = 0 == (_mFrameStatus & _cStatusErrorFlags);   // good measurement

                        _mFramePleth = _mbFrameSensorOk ? (Int32)((byte2 << 8) + byte3) : _cBadPlethValue;
                        _mFrameInfo = byte4;

                         if (_mbFrameSensorOk)
                        {
                            ++_mFramesOkCount;     // number of frames that are ok (no Error)
                            ++_mTotalFramesOkCount;
                            byte flags = (byte)(_mFrameStatus & _cStatusSignalFlags);

                            if (_cStatusB2_LOW__SIGNAL == flags)
                            {
                                ++_mFramesSignalLow;
                                ++_mTotalFramesSignalLow;
                            }
                            else if (_cStatusB2_MEDIUM_SIGNAL == flags)
                            {
                                ++_mFramesSignalMedium;
                                ++_mTotalFramesSignalMedium;
                            }
                            else if (_cStatusB1_HIGH_SIGNAL == flags)
                            {
                                ++_mFramesSignalHigh;
                                ++_mTotalFramesSignalHigh;
                            }
                        }
                        ++_mFrameSeqNr;

                        result = _mFrameSeqNr > _cNrFramesInPacket ? DSpO2FrameReadResult.Overflow_Frame :
                            (_mFrameSeqNr == _cNrFramesInPacket ? DSpO2FrameReadResult.Last_Frame : DSpO2FrameReadResult.Other_Frame);


                        if (bSync)//0 != (byte1 & _cStatusB0_SYNC))
                        {
                            _mFrameSeqNr = 1;   // first frame in packet
                            result = DSpO2FrameReadResult.First_Frame;
                            ++_mPacketCount;
                            ++_mTotalPacketCount;
                            _mPacketIndex = _mFrameNextIndex;
                            if (_mPacketCount <= 1)
                            {
                                int i = 3;
                            }

                        }
                    }
                }
                if (result == DSpO2FrameReadResult.None)
                {
                    // frame overflow move next to check next byte to get in sysnc
                    --_mFrameNextCount;
                    _mFrameNextIndex = mBufferNextIndex(_mFrameNextIndex);
                }
                else
                {
                    // use frame => move to next
                    _mUsedBytes += _cNrBytesInFrame;
                    _mFrameIndex = _mFrameNextIndex;
                    ++_mFramesCount;
                    ++_mTotalFramesCount;
                    _mFrameNextIndex = _mBufferIndex;
                    _mFrameNextCount = 0;

                    if (null != _mCsvWriter && 0 != _mFrameStatus)   // only write frames with valid data
                    {
                        mbCsvWriteFrame();
                    }
                }
            }
            return result;
        }

        public bool mbCsvOpenFile(string AFullSpO2Name)
        {
            bool bOk = false;

            try
            {
                if (AFullSpO2Name != null && AFullSpO2Name.Length > 10)
                {
                    _mCsvFullName = AFullSpO2Name +"_" + CProgram.sDateTimeToYMDHMS( DateTime.Now ) + ".csv";
                    _mCsvWriter = new StreamWriter(_mCsvFullName);

                    string s = " _yyyMMddHHmmss " + _cCsvTab + "iFrame" + _cCsvTab + "signal"
                        + _cCsvTab + "sec" + _cCsvTab + "Pleth"
                        + _cCsvTab + "Packet" + _cCsvTab + "seq" + _cCsvTab + "sync"
                        + _cCsvTab + "Info" + _cCsvTab + "value";
                    _mCsvWriter.WriteLine(s);

                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("SpO2Nonin open csv", ex);
            }
            return bOk;
        }

        private UInt16 mFrameSignalStrength()
        {
            UInt16 signal = 0;

            if( _mbFrameSensorOk)
            {
                byte flags = (byte)(_mFrameStatus & _cStatusSignalFlags);

                signal = 1;

                if (_cStatusB2_LOW__SIGNAL == flags)
                {
                    signal = 2;
                }
                else if (_cStatusB2_MEDIUM_SIGNAL == flags)
                {
                    signal = 3;
                }
                else if (_cStatusB1_HIGH_SIGNAL == flags)
                {
                    signal = 4;
                }

            }
            return signal;
        }

        private byte mPacketInfoByte(DSpO2FrameSequence AFrameSequence)
        {
            byte value = 0xFF;

            if (_mTotalPacketCount > 0 && AFrameSequence < DSpO2FrameSequence.Overflow)
            {
                // get byte4 from frame[x]
                Int32 offset = ((Int32)AFrameSequence - 1) * _cNrBytesInFrame + 3;
                value = mBufferGetByte(_mPacketIndex, offset);
            }
            return value;
        }

        private Int16 mPacketInfoValue(DSpO2FrameSequence AFrameSequence)
        {
            Int16 value = _cBadInfoValue;

            if(_mTotalPacketCount > 0 && AFrameSequence < DSpO2FrameSequence.Overflow)
            {
                byte info = mPacketInfoByte(AFrameSequence);

                switch ( AFrameSequence)
                {
                    //None = 0,   // no frame yet
                    // PR_MSB,     //  1   4-beat Pulse Rate Average
                    case DSpO2FrameSequence.PR_LSB:     //  2
                        value = (Int16)(((mPacketInfoByte(DSpO2FrameSequence.PR_MSB) & 0x03 ) << 7 ) +info);
                        break;
                    case DSpO2FrameSequence.SpO2:       //  3   4-beat SpO 2 Average
                        value = info;
                        break;
                    case DSpO2FrameSequence.SREV:       //  4   software revision
                        value = info;
                        break;
                    //Reserved5,  //  5
                    //Reserved6,  //  6
                    //Reserved7,  //  7
                    case DSpO2FrameSequence.STAT2:      //  8   Status byte 2: B5=SPA high quality status
                        value = info;
                        break;
                    case DSpO2FrameSequence.SpO2_D:     //  9   Display 10sec: 
                        value = info;
                        break;
                    case DSpO2FrameSequence.SpO2_Fast:  // 10   4-beat Average SpO2 optimized for fast responding
                        value = info;
                        break;
                    case DSpO2FrameSequence.SpO2_BB:    // 11   Beat to Beat SpO2 value – No Average
                        value = info;
                        break;
                    //Reserved12, // 12
                    //Reserved13, // 13
                    //EPR_MSB,    // 14
                    case DSpO2FrameSequence.EPR_LSB:    // 15
                        value = (Int16)(((mPacketInfoByte(DSpO2FrameSequence.EPR_MSB) & 0x03) << 7) + info);
                        break;
                    case DSpO2FrameSequence.ESpO2:      // 16   8-beat SpO 2 Extended Average
                        value = info;
                        break;
                    case DSpO2FrameSequence.ESpO2_D:    // 17   Display 10sec: 8-beat SpO 2 Extended Average
                        value = info;
                        break;
                    //Reserved18, // 18
                    //Reserved19, // 19
                    //PRD_MSB,    // 20   Display 10sec: 4-beat Pulse Rate Average
                    case DSpO2FrameSequence.PRD_LSB:    // 21
                        value = (Int16)(((mPacketInfoByte(DSpO2FrameSequence.PRD_MSB) & 0x03) << 7) + info);
                        break;
                    //EPRD_MSB,   // 22   Display 10sec: 8-beat Pulse Rate Extended Average
                    case DSpO2FrameSequence.EPRD_LSB:   // 23
                        value = (Int16)(((mPacketInfoByte(DSpO2FrameSequence.EPRD_MSB) & 0x03) << 7) + info);
                        break;
                        //Reserved24, // 24
                        //Reserved25, // 25
                        //Overflow    // >= 26 overflow packet with frames
                }
            }
            return value;
        }

        public bool mbCsvWriteFrame()
        {
            bool bOk = false;

            try
            {
                if( null != _mCsvWriter)
                {
                    /*                   string s = "_yyyMMddHHmmss" + _cCsvTab + "iFrame" + _cCsvTab + "signal"
                                           + _cCsvTab + "sec" + "Pleth"
                                           + _cCsvTab + "Packet" + _cCsvTab + "seq" + _cCsvTab + "sync"
                                           + "Info" + _cCsvTab + "value";
                                           */
                    double t = _mTotalFramesCount * _cFrameUnitSec;
                    DateTime frameUTC = _mFirstFileUTC.AddSeconds(t);
                    DSpO2FrameSequence seq = _mFrameSeqNr <= _cNrFramesInPacket ? (DSpO2FrameSequence)_mFrameSeqNr : DSpO2FrameSequence.Overflow;
                    string info = seq.ToString();
                    Int32 value = mPacketInfoValue(seq);

                    string s = "\"_" + CProgram.sDateTimeToYMDHMS(frameUTC) + "\""
                    + _cCsvTab + _mFramesCount + _cCsvTab + mFrameSignalStrength()
                    + _cCsvTab + t.ToString("0.00") + _cCsvTab + _mFramePleth
                    + _cCsvTab + _mPacketCount + _cCsvTab + _mFrameSeqNr + _cCsvTab + (_mFrameSeqNr == 1 ? "1" : "0")
                    + _cCsvTab + info + _cCsvTab + (value >= 0 ? value.ToString() : "");
                    _mCsvWriter.WriteLine(s);
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("SpO2Nonin write csv", ex);
            }
            return bOk;
        }

        public bool mbTryParseDvxName(string AFileName)
        {
            bool bDvx = false;
            int len = AFileName == null ? 0 : AFileName.Length;
            int pos = len == 0 ? -1 : AFileName.IndexOf('_');
            // 123456_yyyyMMddHHmmss*******
            if( len >= 15 && pos >= 0)
            {
                string deviceName = AFileName.Substring(0, pos);
                string s = AFileName.Substring(pos+1);
                len = s.Length;
                if( len > 14 )
                {
                    s = s.Substring(0, 14);

                    UInt32.TryParse(deviceName, out _mDeviceNr);
                    bDvx = CProgram.sbParseYMDHMS(s, out _mFileUTC);
                }
            }
            return bDvx;
        }

        public bool mbWriteMit16(CRecordMit ARecord)
        {
            bool bOk = false;

            if (null != ARecord && _mFirstFileName != null && _mFirstFileName.Length > 2
                && ARecord.mNrSamples > 0)
            {
                string name = Path.GetFileNameWithoutExtension(_mFirstFileName)
                    +"_" + CProgram.sDateTimeToYMDHMS(DateTime.Now) 
                    + ( _mFileCount <= 1 ? "" : "N"+ _mFileCount);
                string path = Path.GetDirectoryName(_mFirstFullName);
                string fullName = Path.Combine(path, name  + ".hea");

                bOk = ARecord.mbSaveMIT(fullName, 16, true);
            }
            return bOk;
        }

        public CRecordMit mReadOneFile( string AFullName )
        {
            CRecordMit rec = null;

            try
            {
                if(AFullName != null && AFullName.Length > 10)
                {
                    _mFileFullName = AFullName;
                    _mFileName = Path.GetFileName(AFullName);
                    _mFileSize = 0;
                    _mFileBytesAdded = 0;
                    _mFramesCount = 0;
                    _mFramesOkCount = 0;
                    _mFramesSignalLow = 0;
                    _mFramesSignalMedium = 0;
                    _mFramesSignalHigh = 0;
                    _mFileUTC = DateTime.MinValue;
 
                    mbTryParseDvxName(_mFileName);// sets deviceNr and FileUTC

                    byte[] data = File.ReadAllBytes(AFullName);
                    Int32 nData = data == null ? 0 : data.Length;

                    if (_mbDebugRead)
                    {
                        Int32 nFrames = nData / _cNrBytesInFrame;
                        double t = nFrames * _cFrameUnitSec;
                        string s = _mFileCount + "\t" + _mFileName + "\t" + nData + "bytes\t"
                            + nFrames + " frames \t " + t.ToString( "0.00") +  " sec";
                        _mDebugReadLog += s + "\r\n";
                    }

                    if (nData > 0)
                    {
                        _mFileSize = (UInt32)nData;

                        if (++_mFileCount == 1)
                        {
                            _mFirstFileName = _mFileName;
                            _mFirstFileUTC = _mFileUTC;
                            if (_mbCsv)
                            {
                                mbCsvOpenFile(AFullName);
                            }
                        }
                        for ( int i = 0; i < nData; ++i)
                        {
                            DSpO2FrameReadResult result = mAddByte(data[i]);
                            if( result > DSpO2FrameReadResult.None)
                            {
                                // add frame pleth to record
                                Int32 pleth = _mFramePleth;
                                Int32 signal = mFrameSignalStrength();

                                if (result == DSpO2FrameReadResult.Last_Frame)
                                {
                                    _mLastPulseRate = mPacketInfoValue(DSpO2FrameSequence.PR_LSB);
                                    _mLastSpO2 = mPacketInfoValue(DSpO2FrameSequence.PR_LSB);
                                }
                                // add to signals
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("SpO2Nonin read one file " + AFullName, ex);
                rec = null;
            }
            return rec;
        }


        /*
                public static bool sbSplitYMDH(out UInt16 ArYear, out UInt16 ArMonth, out UInt16 ArDay, out UInt16 ArHour, UInt32 AValueYMDH)
                {
                    bool bOk = false;
                    UInt16 year = 0;
                    UInt16 month = 0;
                    UInt16 day = 0;
                    UInt16 hour = 0;

                    try
                    {
                        UInt32 i = AValueYMDH;
                        hour = (UInt16)(i % 100);
                        i = i / 100;
                        day = (UInt16)(i % 100);
                        i = i / 100;
                        month = (UInt16)(i % 100);
                        year = (UInt16)(i / 100);

                        DateTime dt = new DateTime((int)year, (int)month, (int)day, (int)hour, 0, 0);   // check date is ok
                        bOk = true;
                    }
                    catch (Exception)
                    {

                    }
                    ArYear = year;
                    ArMonth = month;
                    ArDay = day;
                    ArHour = hour;
                    return bOk;
                }
                public string mParamFileInfoLoadNew(string ADevicePath, string ASerialNr, bool AbLoadNew)
                {
                    string info = "";
                    string fileName = mMakeParamFileName(ASerialNr);
                    string filePath = Path.Combine(ADevicePath, fileName);

                    try
                    {
                        FileInfo fi = new FileInfo(filePath);

                        if (false == fi.Exists)
                        {
                            info = "not present";
                        }
                        else
                        {
                            info = CProgram.sDateTimeToString(fi.LastWriteTime);

                    }
                    catch (Exception)
                    {
                        info = "error on find " + fileName;
                    }
                    return info;
                }

                public string mScanInfoLoadMissing(string ADevicePath, string ASerialNr, UInt32 AStudyIX, bool AbLoadMissing)
                {
                    string info = "";
                    string fileName = mMakeScanFileName(ASerialNr, AStudyIX);
                    string studyPath;

                    try
                    {
                    }
                    catch (Exception)
                    {
                        info += ", error on find " + fileName;
                    }
                    return info;
                }

                  public bool mbLoadParamFile(out bool ArbPresent, string ADevicePath, string ASerialNr)
                {
                    bool bOk = false;
                    bool bPresent = false;
                    string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);

                    string fileName = mMakeParamFileName(storeSnr);
                    string fromFile = Path.Combine(ADevicePath, fileName);


                    try
                    {
                        _mbParamFileLoaded = false;
                        FileInfo fi = new FileInfo(fromFile);

                        if (fi == null)
                        {
                            _mParamFileUTC = DateTime.MinValue;
                        }
                        else
                        {
                            _mParamFileUTC = fi.LastWriteTimeUtc;
                            bPresent = fi.Exists;

                            }
                        bOk = true;
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Collect load " + fromFile, ex);
                    }
                    ArbPresent = bPresent;
                    return bOk;
                }
                public bool mbScanFilesStudyRecord(ref string ArResult, out FileInfo[] ArFileList, string ASerialNr, UInt32 AStudyIX)
                {
                    bool bOk = false;
                    FileInfo[] fileList = null;
                    string studyPath;

                    try
                    {
                        if (ArResult.Length > 0) ArResult += ", ";

                        if (AStudyIX == 0)
                        {
                            ArResult += "S0 no study";
                        }
                        else
                        {
                            ArResult = "S" + AStudyIX + ": ";

                            if (false == CDvtmsData.sbGetStudyRecorderDir(out studyPath, AStudyIX, true))
                            {
                                ArResult += "no recordings";
                            }
                            else
                            {
                                DirectoryInfo di = new DirectoryInfo(studyPath);

                                if (di != null)
                                {
                                    fileList = di.GetFiles();
                                    int n = fileList == null ? 0 : fileList.Length;

                                    ArResult += n.ToString() + " files";
                                    bOk = n > 0;
                                }
                                else
                                {
                                    ArResult += " failed scan files";
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        ArResult += ", error on scan files S" + AStudyIX.ToString();
                    }
                    ArFileList = fileList;
                    return bOk;
                }



                public bool mbAddAllFileInfos( FileInfo[] ArFileList, UInt32 AMinFileSize)
                {
                    bool bOk = true;

                    try
                    {
                        if (ArFileList != null )
                        {
                            string fileName;
                            UInt32 fileID;
                            bool bNew;

                            bOk = true;

                            foreach( FileInfo fi in ArFileList )
                            {
                                if (fi.Exists && fi.Length >= AMinFileSize)
                                {
                                    fileName = fi.Name;

                                    if (mbParseFileID(out fileID, fileName))
                                    {
                                    }

                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("error on adding file info " + _mParamSerialNr + " S" + _mStudy_IX.ToString(), ex);
                        bOk = false;
                    }
                    return bOk;
                }

                public bool mbCalcCurrentRange(ref string ArResult )
                {
                    bool bOk = false;

         /*           _mRunStartFileID = 0;
                    _mRunEndFileID = 0;

                    _mFirstPresentFileID = UInt32.MaxValue;        // find range of present FileID
                    _mFirstPresentFileUTC = DateTime.MaxValue;
                    _mLastPresentFileID = UInt32.MinValue;
                    _mLastPresentFileUTC = DateTime.MinValue;

                    _mFirstNormalFileID = UInt32.MaxValue;         // find range FileID of normal receceived (not requested )
                    _mFirstNormalFileUTC = DateTime.MaxValue;
                    _mLastNormalFileID = UInt32.MinValue;
                    _mLastNormalFileUTC = DateTime.MinValue;

                    _mFirstRecvFileID = UInt32.MaxValue;           // find received first / last
                    _mFirstRecvFileUTC = DateTime.MaxValue;
                    _mLastRecvFileID = UInt32.MinValue;
                    _mLastRecvFileUTC = DateTime.MinValue;

                    _mScanCalcResult = new List<string>();

                    UInt32 nPresent = 0, nNormal = 0;

                    if (_mScanFileIdList != null)
                    {
                        foreach (CTzCollectItem item in _mScanFileIdList)
                        {
                            if (item._mbPresent)
                            {
                                ++nPresent;
                                if (item._mFileID < _mFirstPresentFileID) { _mFirstPresentFileID = item._mFileID; _mFirstPresentFileUTC = item._mFileUTC; }
                                if (item._mFileID > _mLastPresentFileID) { _mLastPresentFileID = item._mFileID; _mLastPresentFileUTC = item._mFileUTC; }

                                if (item._mTryRequestCounter == 0)
                                {
                                    ++nNormal;
                                    if (item._mFileID < _mFirstNormalFileID) { _mFirstNormalFileID = item._mFileID; _mFirstNormalFileUTC = item._mFileUTC; }
                                    if (item._mFileID > _mLastNormalFileID) { _mLastNormalFileID = item._mFileID; _mLastNormalFileUTC = item._mFileUTC; }
                                }
                                if (item._mFileUTC < _mFirstRecvFileUTC) { _mFirstRecvFileUTC = item._mFileUTC; _mFirstRecvFileID = item._mFileID; }
                                if (item._mFileUTC > _mLastRecvFileUTC) { _mLastRecvFileUTC = item._mFileUTC; _mLastRecvFileID = item._mFileID; }
                            }
                        }
                        if( ArResult.Length > 0  ) ArResult += ", ";
                            ArResult += "last=" + _mLastPresentFileID.ToString() + " " + CProgram.sDateTimeToString(CProgram.sDateTimeToLocal(_mLastPresentFileUTC));
                        if (_mScanCalcResult != null)
                        {
                            _mScanCalcResult.Add("scan loaded " + _mScanNrFilesPresent.ToString() + ", added " + _mScanNrFilesAdded.ToString()
                                + " files, present = " + nPresent.ToString() + ", nNormal=  " + nNormal.ToString());
                            _mScanCalcResult.Add("firstPresentFileID=" + _mFirstPresentFileID.ToString() + " " + CProgram.sDateTimeToString(_mFirstPresentFileUTC)
                                + "UTC , LastPresentFileID=" + _mLastPresentFileID.ToString()+ " " + CProgram.sDateTimeToString(_mLastPresentFileUTC) + " UTC" );
                            _mScanCalcResult.Add("FirstNormalFileID=" + _mFirstNormalFileID.ToString() + " " + CProgram.sDateTimeToString(_mFirstNormalFileUTC)
                                + " UTC, LastNormalFileID=" + _mLastNormalFileID.ToString() + " " + CProgram.sDateTimeToString(_mLastNormalFileUTC) + "UTC ");
                            _mScanCalcResult.Add("FirstRecvFileID=" + _mFirstRecvFileID.ToString() + " " + CProgram.sDateTimeToString(_mFirstNormalFileUTC)
                                + " UTC, LastRecvFileID=" + _mLastRecvFileID.ToString() + " _mLastRecvFileUTC" + CProgram.sDateTimeToString(_mLastRecvFileUTC) + "UTC ");
                            //_mScanCalcResult.Add("=" + .ToString() + ", =" + .ToString());
                        }
                        bOk = true;
                    }
          * /          return bOk;
                }
                */

    }
}
