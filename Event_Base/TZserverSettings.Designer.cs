﻿namespace TzDevice
{
    partial class TZserverSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label89 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.labelDeviceSnr = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.textBoxAESIV = new System.Windows.Forms.TextBox();
            this.checkBoxEnableWireless = new System.Windows.Forms.CheckBox();
            this.label46 = new System.Windows.Forms.Label();
            this.textBoxTZPinCode = new System.Windows.Forms.TextBox();
            this.textBoxAESKey = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.comboBoxDeviceID = new System.Windows.Forms.ComboBox();
            this.checkBoxCheckSettingRanges = new System.Windows.Forms.CheckBox();
            this.textBoxTZFWversion = new System.Windows.Forms.TextBox();
            this.checkBoxUseHTTP = new System.Windows.Forms.CheckBox();
            this.label49 = new System.Windows.Forms.Label();
            this.textBoxTZserverPort = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.textBoxTZserverIP = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelLastError = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelUseTzProxy = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.comboBoxActionsInterval = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.comboBoxScreenSleep = new System.Windows.Forms.ComboBox();
            this.checkBoxDemoMode = new System.Windows.Forms.CheckBox();
            this.checkBoxHuffman = new System.Windows.Forms.CheckBox();
            this.label36 = new System.Windows.Forms.Label();
            this.comboBoxResolution = new System.Windows.Forms.ComboBox();
            this.textBoxTZSCPlength = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.checkBoxDifferenceEncoding = new System.Windows.Forms.CheckBox();
            this.checkBoxServerRequest = new System.Windows.Forms.CheckBox();
            this.label41 = new System.Windows.Forms.Label();
            this.textBoxCenterName = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.textBoxCenterNr = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label89);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(889, 29);
            this.panel1.TabIndex = 1;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Arial", 10F);
            this.label89.ForeColor = System.Drawing.Color.DarkBlue;
            this.label89.Location = new System.Drawing.Point(467, 9);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(288, 16);
            this.label89.TabIndex = 194;
            this.label89.Text = "Blue = TZ Aera only (Not used on TZ Clarus)";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.MediumPurple;
            this.panel6.Controls.Add(this.labelDeviceSnr);
            this.panel6.Controls.Add(this.label51);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 29);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(889, 25);
            this.panel6.TabIndex = 67;
            // 
            // labelDeviceSnr
            // 
            this.labelDeviceSnr.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelDeviceSnr.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.labelDeviceSnr.ForeColor = System.Drawing.Color.White;
            this.labelDeviceSnr.Location = new System.Drawing.Point(184, 0);
            this.labelDeviceSnr.Name = "labelDeviceSnr";
            this.labelDeviceSnr.Size = new System.Drawing.Size(108, 25);
            this.labelDeviceSnr.TabIndex = 187;
            this.labelDeviceSnr.Text = "12345678";
            this.labelDeviceSnr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label51
            // 
            this.label51.Dock = System.Windows.Forms.DockStyle.Left;
            this.label51.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.label51.ForeColor = System.Drawing.Color.White;
            this.label51.Location = new System.Drawing.Point(0, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(184, 25);
            this.label51.TabIndex = 179;
            this.label51.Text = "Device Server settings";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.textBoxAESIV);
            this.panel7.Controls.Add(this.checkBoxEnableWireless);
            this.panel7.Controls.Add(this.label46);
            this.panel7.Controls.Add(this.textBoxTZPinCode);
            this.panel7.Controls.Add(this.textBoxAESKey);
            this.panel7.Controls.Add(this.label37);
            this.panel7.Controls.Add(this.label45);
            this.panel7.Controls.Add(this.label47);
            this.panel7.Controls.Add(this.comboBoxDeviceID);
            this.panel7.Controls.Add(this.checkBoxCheckSettingRanges);
            this.panel7.Controls.Add(this.textBoxTZFWversion);
            this.panel7.Controls.Add(this.checkBoxUseHTTP);
            this.panel7.Controls.Add(this.label49);
            this.panel7.Controls.Add(this.textBoxTZserverPort);
            this.panel7.Controls.Add(this.label44);
            this.panel7.Controls.Add(this.label43);
            this.panel7.Controls.Add(this.textBoxTZserverIP);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 54);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(889, 143);
            this.panel7.TabIndex = 68;
            // 
            // textBoxAESIV
            // 
            this.textBoxAESIV.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAESIV.Location = new System.Drawing.Point(396, 101);
            this.textBoxAESIV.Name = "textBoxAESIV";
            this.textBoxAESIV.Size = new System.Drawing.Size(311, 23);
            this.textBoxAESIV.TabIndex = 171;
            // 
            // checkBoxEnableWireless
            // 
            this.checkBoxEnableWireless.AutoSize = true;
            this.checkBoxEnableWireless.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxEnableWireless.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxEnableWireless.Location = new System.Drawing.Point(332, 43);
            this.checkBoxEnableWireless.Name = "checkBoxEnableWireless";
            this.checkBoxEnableWireless.Size = new System.Drawing.Size(125, 20);
            this.checkBoxEnableWireless.TabIndex = 145;
            this.checkBoxEnableWireless.Text = "Enable wireless";
            this.checkBoxEnableWireless.UseVisualStyleBackColor = true;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Arial", 10F);
            this.label46.Location = new System.Drawing.Point(329, 104);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(51, 16);
            this.label46.TabIndex = 170;
            this.label46.Text = "AES IV";
            // 
            // textBoxTZPinCode
            // 
            this.textBoxTZPinCode.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTZPinCode.Location = new System.Drawing.Point(629, 40);
            this.textBoxTZPinCode.Name = "textBoxTZPinCode";
            this.textBoxTZPinCode.Size = new System.Drawing.Size(78, 26);
            this.textBoxTZPinCode.TabIndex = 179;
            // 
            // textBoxAESKey
            // 
            this.textBoxAESKey.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxAESKey.Location = new System.Drawing.Point(396, 73);
            this.textBoxAESKey.Name = "textBoxAESKey";
            this.textBoxAESKey.Size = new System.Drawing.Size(311, 23);
            this.textBoxAESKey.TabIndex = 169;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 10F);
            this.label37.Location = new System.Drawing.Point(525, 45);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(102, 16);
            this.label37.TabIndex = 178;
            this.label37.Text = "Start PIN code:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 10F);
            this.label45.Location = new System.Drawing.Point(329, 76);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(61, 16);
            this.label45.TabIndex = 168;
            this.label45.Text = "AES key";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Arial", 10F);
            this.label47.Location = new System.Drawing.Point(4, 15);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(72, 16);
            this.label47.TabIndex = 168;
            this.label47.Text = "Device ID:";
            // 
            // comboBoxDeviceID
            // 
            this.comboBoxDeviceID.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxDeviceID.FormattingEnabled = true;
            this.comboBoxDeviceID.Items.AddRange(new object[] {
            "TZMR"});
            this.comboBoxDeviceID.Location = new System.Drawing.Point(77, 11);
            this.comboBoxDeviceID.Name = "comboBoxDeviceID";
            this.comboBoxDeviceID.Size = new System.Drawing.Size(133, 24);
            this.comboBoxDeviceID.TabIndex = 172;
            // 
            // checkBoxCheckSettingRanges
            // 
            this.checkBoxCheckSettingRanges.AutoSize = true;
            this.checkBoxCheckSettingRanges.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxCheckSettingRanges.Location = new System.Drawing.Point(332, 13);
            this.checkBoxCheckSettingRanges.Name = "checkBoxCheckSettingRanges";
            this.checkBoxCheckSettingRanges.Size = new System.Drawing.Size(168, 20);
            this.checkBoxCheckSettingRanges.TabIndex = 177;
            this.checkBoxCheckSettingRanges.Text = "Check Setting Ranges";
            this.checkBoxCheckSettingRanges.UseVisualStyleBackColor = true;
            // 
            // textBoxTZFWversion
            // 
            this.textBoxTZFWversion.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxTZFWversion.Location = new System.Drawing.Point(77, 42);
            this.textBoxTZFWversion.Name = "textBoxTZFWversion";
            this.textBoxTZFWversion.Size = new System.Drawing.Size(133, 23);
            this.textBoxTZFWversion.TabIndex = 174;
            // 
            // checkBoxUseHTTP
            // 
            this.checkBoxUseHTTP.AutoSize = true;
            this.checkBoxUseHTTP.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxUseHTTP.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxUseHTTP.Location = new System.Drawing.Point(528, 13);
            this.checkBoxUseHTTP.Name = "checkBoxUseHTTP";
            this.checkBoxUseHTTP.Size = new System.Drawing.Size(91, 20);
            this.checkBoxUseHTTP.TabIndex = 163;
            this.checkBoxUseHTTP.Text = "Use HTTP";
            this.checkBoxUseHTTP.UseVisualStyleBackColor = true;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Arial", 10F);
            this.label49.Location = new System.Drawing.Point(4, 45);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(70, 16);
            this.label49.TabIndex = 173;
            this.label49.Text = "Firmware:";
            // 
            // textBoxTZserverPort
            // 
            this.textBoxTZserverPort.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxTZserverPort.Location = new System.Drawing.Point(77, 101);
            this.textBoxTZserverPort.Name = "textBoxTZserverPort";
            this.textBoxTZserverPort.Size = new System.Drawing.Size(81, 23);
            this.textBoxTZserverPort.TabIndex = 162;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 10F);
            this.label44.Location = new System.Drawing.Point(4, 104);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(38, 16);
            this.label44.TabIndex = 161;
            this.label44.Text = "Port:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial", 10F);
            this.label43.Location = new System.Drawing.Point(4, 76);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(70, 16);
            this.label43.TabIndex = 159;
            this.label43.Text = "Server IP:";
            // 
            // textBoxTZserverIP
            // 
            this.textBoxTZserverIP.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxTZserverIP.Location = new System.Drawing.Point(77, 73);
            this.textBoxTZserverIP.Name = "textBoxTZserverIP";
            this.textBoxTZserverIP.Size = new System.Drawing.Size(209, 23);
            this.textBoxTZserverIP.TabIndex = 160;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SlateBlue;
            this.panel3.Controls.Add(this.labelLastError);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 331);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(889, 30);
            this.panel3.TabIndex = 69;
            // 
            // labelLastError
            // 
            this.labelLastError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelLastError.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLastError.ForeColor = System.Drawing.Color.DarkOrange;
            this.labelLastError.Location = new System.Drawing.Point(0, 0);
            this.labelLastError.Name = "labelLastError";
            this.labelLastError.Size = new System.Drawing.Size(714, 30);
            this.labelLastError.TabIndex = 3;
            this.labelLastError.Text = "Last Error: gfdhsgfhsgfhjs";
            this.labelLastError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.Dock = System.Windows.Forms.DockStyle.Right;
            this.label24.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(714, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(175, 30);
            this.label24.TabIndex = 2;
            this.label24.Text = "Store and Close";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label24.Click += new System.EventHandler(this.label24_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MediumPurple;
            this.panel2.Controls.Add(this.labelUseTzProxy);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 197);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(889, 25);
            this.panel2.TabIndex = 70;
            // 
            // labelUseTzProxy
            // 
            this.labelUseTzProxy.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelUseTzProxy.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUseTzProxy.ForeColor = System.Drawing.Color.White;
            this.labelUseTzProxy.Location = new System.Drawing.Point(0, 0);
            this.labelUseTzProxy.Name = "labelUseTzProxy";
            this.labelUseTzProxy.Size = new System.Drawing.Size(211, 25);
            this.labelUseTzProxy.TabIndex = 180;
            this.labelUseTzProxy.Text = "Use TZ proxy server";
            this.labelUseTzProxy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelUseTzProxy.Click += new System.EventHandler(this.labelUseTzProxy_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label39);
            this.panel4.Controls.Add(this.comboBoxActionsInterval);
            this.panel4.Controls.Add(this.label38);
            this.panel4.Controls.Add(this.comboBoxScreenSleep);
            this.panel4.Controls.Add(this.checkBoxDemoMode);
            this.panel4.Controls.Add(this.checkBoxHuffman);
            this.panel4.Controls.Add(this.label36);
            this.panel4.Controls.Add(this.comboBoxResolution);
            this.panel4.Controls.Add(this.textBoxTZSCPlength);
            this.panel4.Controls.Add(this.label33);
            this.panel4.Controls.Add(this.checkBoxDifferenceEncoding);
            this.panel4.Controls.Add(this.checkBoxServerRequest);
            this.panel4.Controls.Add(this.label41);
            this.panel4.Controls.Add(this.textBoxCenterName);
            this.panel4.Controls.Add(this.label42);
            this.panel4.Controls.Add(this.textBoxCenterNr);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 222);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(889, 109);
            this.panel4.TabIndex = 71;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial", 10F);
            this.label39.ForeColor = System.Drawing.Color.DarkBlue;
            this.label39.Location = new System.Drawing.Point(329, 78);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(151, 16);
            this.label39.TabIndex = 173;
            this.label39.Text = "Check actions interval:";
            // 
            // comboBoxActionsInterval
            // 
            this.comboBoxActionsInterval.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxActionsInterval.FormattingEnabled = true;
            this.comboBoxActionsInterval.Items.AddRange(new object[] {
            "40"});
            this.comboBoxActionsInterval.Location = new System.Drawing.Point(506, 74);
            this.comboBoxActionsInterval.Name = "comboBoxActionsInterval";
            this.comboBoxActionsInterval.Size = new System.Drawing.Size(51, 24);
            this.comboBoxActionsInterval.TabIndex = 174;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Arial", 10F);
            this.label38.Location = new System.Drawing.Point(329, 46);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(125, 16);
            this.label38.TabIndex = 171;
            this.label38.Text = "Screen sleep time:";
            // 
            // comboBoxScreenSleep
            // 
            this.comboBoxScreenSleep.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxScreenSleep.FormattingEnabled = true;
            this.comboBoxScreenSleep.Items.AddRange(new object[] {
            "20"});
            this.comboBoxScreenSleep.Location = new System.Drawing.Point(460, 42);
            this.comboBoxScreenSleep.Name = "comboBoxScreenSleep";
            this.comboBoxScreenSleep.Size = new System.Drawing.Size(97, 24);
            this.comboBoxScreenSleep.TabIndex = 172;
            // 
            // checkBoxDemoMode
            // 
            this.checkBoxDemoMode.AutoSize = true;
            this.checkBoxDemoMode.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxDemoMode.Location = new System.Drawing.Point(733, 44);
            this.checkBoxDemoMode.Name = "checkBoxDemoMode";
            this.checkBoxDemoMode.Size = new System.Drawing.Size(103, 20);
            this.checkBoxDemoMode.TabIndex = 170;
            this.checkBoxDemoMode.Text = "Demo mode";
            this.checkBoxDemoMode.UseVisualStyleBackColor = true;
            // 
            // checkBoxHuffman
            // 
            this.checkBoxHuffman.AutoSize = true;
            this.checkBoxHuffman.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxHuffman.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxHuffman.Location = new System.Drawing.Point(733, 9);
            this.checkBoxHuffman.Name = "checkBoxHuffman";
            this.checkBoxHuffman.Size = new System.Drawing.Size(141, 20);
            this.checkBoxHuffman.TabIndex = 169;
            this.checkBoxHuffman.Text = "Huffman encoding";
            this.checkBoxHuffman.UseVisualStyleBackColor = true;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 10F);
            this.label36.ForeColor = System.Drawing.Color.DarkBlue;
            this.label36.Location = new System.Drawing.Point(4, 81);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(79, 16);
            this.label36.TabIndex = 167;
            this.label36.Text = "Resolution:";
            // 
            // comboBoxResolution
            // 
            this.comboBoxResolution.Font = new System.Drawing.Font("Arial", 10F);
            this.comboBoxResolution.FormattingEnabled = true;
            this.comboBoxResolution.Items.AddRange(new object[] {
            "12",
            "24"});
            this.comboBoxResolution.Location = new System.Drawing.Point(100, 77);
            this.comboBoxResolution.Name = "comboBoxResolution";
            this.comboBoxResolution.Size = new System.Drawing.Size(111, 24);
            this.comboBoxResolution.TabIndex = 168;
            // 
            // textBoxTZSCPlength
            // 
            this.textBoxTZSCPlength.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxTZSCPlength.Location = new System.Drawing.Point(460, 11);
            this.textBoxTZSCPlength.Name = "textBoxTZSCPlength";
            this.textBoxTZSCPlength.Size = new System.Drawing.Size(97, 23);
            this.textBoxTZSCPlength.TabIndex = 166;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 10F);
            this.label33.ForeColor = System.Drawing.Color.DarkBlue;
            this.label33.Location = new System.Drawing.Point(332, 14);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(83, 16);
            this.label33.TabIndex = 165;
            this.label33.Text = "SCP length:";
            // 
            // checkBoxDifferenceEncoding
            // 
            this.checkBoxDifferenceEncoding.AutoSize = true;
            this.checkBoxDifferenceEncoding.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxDifferenceEncoding.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxDifferenceEncoding.Location = new System.Drawing.Point(572, 44);
            this.checkBoxDifferenceEncoding.Name = "checkBoxDifferenceEncoding";
            this.checkBoxDifferenceEncoding.Size = new System.Drawing.Size(154, 20);
            this.checkBoxDifferenceEncoding.TabIndex = 164;
            this.checkBoxDifferenceEncoding.Text = "Difference encoding";
            this.checkBoxDifferenceEncoding.UseVisualStyleBackColor = true;
            // 
            // checkBoxServerRequest
            // 
            this.checkBoxServerRequest.AutoSize = true;
            this.checkBoxServerRequest.Font = new System.Drawing.Font("Arial", 10F);
            this.checkBoxServerRequest.ForeColor = System.Drawing.Color.DarkBlue;
            this.checkBoxServerRequest.Location = new System.Drawing.Point(572, 9);
            this.checkBoxServerRequest.Name = "checkBoxServerRequest";
            this.checkBoxServerRequest.Size = new System.Drawing.Size(121, 20);
            this.checkBoxServerRequest.TabIndex = 163;
            this.checkBoxServerRequest.Text = "Server request";
            this.checkBoxServerRequest.UseVisualStyleBackColor = true;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial", 10F);
            this.label41.Location = new System.Drawing.Point(4, 14);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(94, 16);
            this.label41.TabIndex = 159;
            this.label41.Text = "Center name:";
            // 
            // textBoxCenterName
            // 
            this.textBoxCenterName.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxCenterName.Location = new System.Drawing.Point(100, 11);
            this.textBoxCenterName.Name = "textBoxCenterName";
            this.textBoxCenterName.Size = new System.Drawing.Size(186, 23);
            this.textBoxCenterName.TabIndex = 160;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 10F);
            this.label42.ForeColor = System.Drawing.Color.DarkBlue;
            this.label42.Location = new System.Drawing.Point(4, 45);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(74, 16);
            this.label42.TabIndex = 161;
            this.label42.Text = "Center tel:";
            // 
            // textBoxCenterNr
            // 
            this.textBoxCenterNr.Font = new System.Drawing.Font("Arial", 10F);
            this.textBoxCenterNr.Location = new System.Drawing.Point(100, 38);
            this.textBoxCenterNr.Name = "textBoxCenterNr";
            this.textBoxCenterNr.Size = new System.Drawing.Size(186, 23);
            this.textBoxCenterNr.TabIndex = 162;
            // 
            // TZserverSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 361);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "TZserverSettings";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox textBoxAESIV;
        private System.Windows.Forms.CheckBox checkBoxEnableWireless;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox textBoxTZPinCode;
        private System.Windows.Forms.TextBox textBoxAESKey;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.CheckBox checkBoxCheckSettingRanges;
        private System.Windows.Forms.TextBox textBoxTZFWversion;
        private System.Windows.Forms.CheckBox checkBoxUseHTTP;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textBoxTZserverPort;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox textBoxTZserverIP;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox comboBoxDeviceID;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBoxCenterName;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox textBoxCenterNr;
        private System.Windows.Forms.CheckBox checkBoxDifferenceEncoding;
        private System.Windows.Forms.CheckBox checkBoxServerRequest;
        private System.Windows.Forms.TextBox textBoxTZSCPlength;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox comboBoxResolution;
        private System.Windows.Forms.CheckBox checkBoxDemoMode;
        private System.Windows.Forms.CheckBox checkBoxHuffman;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox comboBoxActionsInterval;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox comboBoxScreenSleep;
        private System.Windows.Forms.Label labelDeviceSnr;
        private System.Windows.Forms.Label labelLastError;
        private System.Windows.Forms.Label labelUseTzProxy;
        private System.Windows.Forms.Label label89;
    }
}