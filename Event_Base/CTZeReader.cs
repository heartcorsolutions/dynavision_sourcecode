﻿// Based on parse_event.cpp from TZ Medical, Inc
#define CHECK_CRC
//#define TZ_DUMP_BUG_GMED 

using Event_Base;
using EventBoard.Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace i_Reader.Event_Base
{
    public enum DTzEventTags
    {
        START_TAG = 1,              // none
        STOP_TAG = 2,              // none
        BREAK_TAG = 3,              // none
        RESUME_TAG = 4,              // none
        FULL_TAG = 5,              // none
        PACEMAKER_DETECTION_TAG = 11,              // pulse width (microseconds)
        QRS_DETECTION_TAG = 12,              // 0xMMTT, M == channel mask, TT == beat type { N=Normal, S=SVEB-PAC, V=VEB-PVC, F = Fusion, Q=Paced Beast, 
        BPM_COUNT_TAG = 17,              // Samples
        BPM_STDEV_TAG = 18,              // BPM
        BPM_AVERAGE_TAG = 19,              // BPM
        BPM_MIN_TAG = 20,              // BPM 
        BPM_MAX_TAG = 21,              // BPM 
        TACHY_ENTRY_TAG = 22,              // channel mask 
        TACHY_EXIT_TAG = 23,              // channel mask 
        BRADY_ENTRY_TAG = 24,              // channel mask
        BRADY_EXIT_TAG = 25,              // channel mask
        PAUSE_ENTRY_TAG = 26,              // channel mask
        AFIB_ENTRY_TAG = 27,              // channel mask
        AFIB_EXIT_TAG = 28,              // channel mask
        PAUSE_EXIT_TAG = 29,              // channel mask
        TACHY_RATE_CHANGE_TAG = 30,              // BPM
        BRADY_RATE_CHANGE_TAG = 31,              // BPM
        PVC_ENTRY_TAG = 32,              // channel mask
        ANALYSIS_RESUMED_TAG = 49,              // channel mask
        ANALYSIS_PAUSED_TAG = 50,              // channel mask
        LEAD_DISCONNECTED = 51,              // Bitmap of leads (1=disconnected): 0b000EASIG
        MISSING_SCP_TAG = 81,              // none
        MISSING_EVENT_TAG = 82,              // Event count (0 - 999)
        PATIENT_TAG = 101,              // none
        BATTERY_VALUE_TAG = 151,              // Battery Voltage (mV)
        BATTERY_LOW_TAG = 152,              // Battery Voltage (mV)
        CHARGING_STARTED_TAG = 153,              // none
        CHARGING_STOPPED_TAG = 154,             // none
        TIME_ZONE_CHANGE_TAG = 175,              // Offset from UTC in MINUTES (MAX +/- 780)
        TZR_REQUEST_TAG = 190,              // Day and hour of TZR requested
        TZR_BULK_UPLOAD = 198,              // bulk upload activated event 120 files = 1 hour
        SCP_RETRANSMIT_TAG = 199,              // Error Code (ACTIONS_ERR code) !may not be used on files that are not transmitted yet CH20181004
        SERVER_REQUEST_TAG = 200,              // Number of files requested
        SETTINGS_SUCCESS_TAG = 201,              // Setting File ID #
        SMS_COMMAND_ERROR_TAG = 202,              // none
        SMS_MSG_RECEIVED = 203,              // none
        SETTINGS_FAILURE_TAG = 210,              // Error Code (see below)
        ACTIONS_FAILURE_TAG = 211,              // Error Code (see below)
        ACTIONS_SUCCESS_TAG = 212,              // Action File ID #
        FTP_SETTINGS_CHANGED = 225,              // none
        TERMINATOR_TAG = 255              // 0xFF

    };

    class CTZeReader
    {
        const string EVENT_ID_STRING = "TZEVT";
        const string MODEL_DESC_STRING = "TZMR\0\0";
        const UInt16 MAX_FILE_SIZE = 1024;      // Bytes
        const UInt16 MIN_FILE_SIZE = 16;      // Bytes

        public static bool _sbEventBulkUpload = true;
        public static bool _sbEventRequestScp = true;

        bool _mbLogImport = false;

        public string _mDeviceID;
        public string _mPatientID;
        public string _mRemLabel;
        public string _mRemData;
        public DateTime _mEventTimeUTC = DateTime.MinValue;
        public Int16 _mTimeOffsetMin = 0;

        public UInt32 _mFirstFileNr = 0;
        public UInt16 _mNrFiles = 0;
        public UInt32 _mEventFileIndex = 0;
        public string _mEventFileIndexName = null;
        public UInt32 _mEventSampleIndex = 0;
        public UInt16 _mTag = 0;
        public UInt32 _mTagData = 0;
        public bool _mbDoEvent = false;

        private UInt16[] ccitt_crc16_table = {
          0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
          0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
          0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
          0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
          0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
          0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
          0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
          0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
          0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
          0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
          0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
          0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
          0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
          0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
          0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
          0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
          0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
          0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
          0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
          0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
          0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
          0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
          0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
          0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
          0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
          0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
          0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
          0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
          0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
          0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
          0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
          0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
        };

        private Byte[] constKey = {
          0xf5, 0xf2, 0x3c, 0x0c, 0x8e, 0x49, 0x3a, 0xfb,
          0x30, 0x1d, 0xf8, 0x62, 0xa0, 0x30, 0x45, 0xbe
        };

        private Byte[]  constIV = {
          0x79, 0x17, 0xae, 0x23, 0x46, 0xe7, 0x8e, 0x79,
          0xbf, 0x3d, 0xff, 0xa6, 0x2c, 0x05, 0x4b, 0xf9
        };


        public CTZeReader()
        {
//            _mbLogImport = CLicKeyDev.sbDeviceIsProgrammer();
        }

        public void mClear()
        {
        _mDeviceID = "";
            _mPatientID = "";
        _mRemLabel = "";
        _mRemData = "";
        _mEventTimeUTC = DateTime.MinValue;
        _mTimeOffsetMin = 0;

        _mFirstFileNr = 0;
        _mNrFiles = 0;
        _mEventSampleIndex = 0;
        _mTag = 0;
        _mTagData = 0;
        _mbDoEvent = false;

    }

    public DateTime mGetEventUtc()
        {
            //            DateTime utc = _mEventTimeDT == DateTime.MinValue ? DateTime.MinValue : _mEventTimeDT.AddMinutes(-_mTimeOffsetMin);
            //return DateTime.SpecifyKind(utc, DateTimeKind.Utc);
            return DateTime.SpecifyKind(_mEventTimeUTC, DateTimeKind.Utc);
        }


    UInt16 mCalcCrcBlock(Byte[] AData, uint AIndex, uint ALength)
        {
            uint j;
            uint i = AIndex;
            UInt32 crcVal = 0xffff;                        // CRC is based off an initial value of 0xffff

            for (j = 0; j < ALength; ++j, ++i)
            {
                crcVal = ccitt_crc16_table[(AData[i] ^ (crcVal >> 8)) & 0xff] ^ (crcVal << 8);
            }
            return (UInt16)crcVal;
        }

        private void mLogImport(string ALine)
        {
            if (_mbLogImport)
            {
                CProgram.sLogLine(ALine);
            }
        }
        private void mLogError(string ALine, 
                    [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            CProgram.sLogLine(ALine, _AutoLN, _AutoFN, _AutoCN);
        }

        UInt16 mReadUInt16( Byte[] ABuffer, UInt16 AIndex )
        {
            int u = ABuffer[AIndex] + (ABuffer[AIndex + 1] << 8);

            return (UInt16)u;
        }

        UInt32 mReadUInt32(Byte[] ABuffer, UInt16 AIndex)
        {
            int u = ABuffer[AIndex] + (ABuffer[AIndex + 1] << 8) + (ABuffer[AIndex + 2] << 16) + (ABuffer[AIndex + 3] << 24);

            return (UInt32)u;
            // bad !! return (UInt16)u; SV20181027
            // Causes the file scp number to be wrong after 65536*30sec == 22.7 days
            // resulting in scp file read errors resulting in error in ireader 
            //          or event with old strip time in record
        }

        string mReadString(Byte[] ABuffer, UInt16 AIndex, UInt16 ALength)
        {
            string s = "";
            uint i = AIndex;
            uint n = ALength;
            char c;

            while( n-- > 0 )
            {
                c = (char)(ABuffer[i++]);
                if( c == 0 )
                {
                    break;
                }
                s += c;
            }
            return s;
        }
        string mReadString(Byte[] ABuffer, UInt16 AIndex)
        {
            string s = "";
            uint i = AIndex;
            char c;

            while ( 0 != ( c = (char)(ABuffer[i++])))
                { 
                s += c;
            }
            return s;
        }

        void mAddRemLabel( string ALabel, string AValue)
        {
            if( ALabel != null && ALabel.Length > 0)
            {
                if (_mRemLabel != null && _mRemLabel.Length > 0)
                {
                    _mRemLabel += ", ";
                }
                _mRemLabel += ALabel;
                if( AValue != null && AValue.Length > 0 )
                {
                    _mRemLabel += "=" + AValue;
                }
            }
        }
        void mAddRemData(string ALabel, string AValue, bool AbLog)
        {
            if (ALabel != null && ALabel.Length > 0)
            {
                string s = ALabel + "=" + AValue;
                _mRemData += s + "\n";
                if( AbLog )
                {
                    mLogImport("Data: " + s);
                }
            }
        }
        void mAddRemDataLabel(string ALabel, string AValue, bool AbLog )
        {
            mAddRemLabel(ALabel, AValue);
            mAddRemData(ALabel, AValue, AbLog);
        }

        string mGetSymptomActivity(string ASerialNr, UInt16 ASNr, UInt16 AANr)
        {
            string sympAct = "";
            
            if (ASNr != 0 || AANr != 0)
            {
                // get settings file
                CTzDeviceSettings tzSettings = null;

                string fileName = "";
                string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);
                string settingsPath;

                bool bSettings = false;

                try
                {
                    if (CDvtmsData.sbGetDeviceUnitDir(out settingsPath, DDeviceType.TZ, storeSnr, true))
                    {
                        tzSettings = new CTzDeviceSettings();
                        if (tzSettings != null)
                        {
                            if (CTzDeviceSettings.sbMakeSettingFileName(out fileName, storeSnr))
                            {
                                fileName = Path.Combine(settingsPath, fileName);

                                bSettings = tzSettings.mbReadSettings(fileName, true, true, storeSnr); // needs a default to read file
                            }
                            tzSettings.serialNumber = ASerialNr;
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Read failed from " + fileName, ex);
                    bSettings = false;
                }

                if (0 != ASNr)
                {
                    string sText = "";

                    if (bSettings)
                    {
                        UInt32 n = tzSettings.mGetSettingUInt32(DTzSettingTags.SYMPTOM_DIARY_COUNT);

                        if (n > 10) n = 10;
                        if (ASNr <= n)
                        {
                            sText = tzSettings.mGetSettingString(DTzSettingTags.DIARY_SYMPTOM_1 + ASNr - 1);
                        }
                        else
                        {
                            sText = ASNr.ToString() + "!";
                        }
                    }
                    else
                    {
                        sText = ASNr.ToString() + "?";
                    }
                    sympAct = "S=" /*+ ASNr.ToString()*/ + sText;
                }

                if (0 != AANr)
                {
                    string aText = "";

                    if (bSettings)
                    {
                        UInt32 n = tzSettings.mGetSettingUInt32(DTzSettingTags.ACTIVITY_DIARY_COUNT);

                        if (n > 10) n = 10;
                        if (AANr <= n)
                        {
                            aText = tzSettings.mGetSettingString(DTzSettingTags.DIARY_ACTIVITY_1 + AANr - 1);
                        }
                        else
                        {
                            aText = AANr.ToString() + "!";
                        }
                    }
                    else
                    {
                        aText = AANr.ToString() + "?";
                    }
                    sympAct += " A=" /*+ ASNr.ToString()*/ + aText;
                }
                mLogImport("---Patient : " + sympAct + " S= " + ASNr.ToString() + " A=" + AANr.ToString());
            }
            return sympAct;
        }

        public bool mbLoadFile( string AFilePath)
        {
            bool bOk = false;
            string fileName = Path.GetFileName(AFilePath);
            FileStream fstream = null;

            try
            {
                UInt16 ourcrcValue;                  // The CRC we calculate
                UInt16 theircrcValue;                // The CRC from the file
                uint length;                         // The length read from the file

                Byte[] readBuffer;                       // Pointer used for the read command
                UInt16 readIndex = 0;

                Byte[] firstBlock = new Byte[16];
                Byte[] tempKey = new Byte[16];
                Byte[] tempIV = new Byte[16];

                UInt16 i;

                string qrsSet = "";
                string qrsSeq = "";
                int pvcCount = 0;

// turn off aes, not implemented                aes_context aes_ctx;

                mClear();

                fstream = File.OpenRead(AFilePath);
                if (fstream == null)
                {
                    mLogError("Failed open file. Aborting.");
                    return false;
                }
                Int32 fileLength = (Int32)fstream.Length;
                // ReadFully(stream);

                // Make sure the file is not too big for the format
                if (fileLength > MAX_FILE_SIZE)
                {
                    mLogError("File size is SIGNIFICANTLY larger than expected. Aborting.");
                    fstream.Close();
                    return false;
                }
                // Make sure the file is not too small for the format
                if (fileLength < MIN_FILE_SIZE)
                {
                    mLogError("File size is too small for an SCP file. Aborting.");
                    fstream.Close();
                    return false;
                }
                // Read in the first TWO 16 byte blocks to see if the file has been encrypted
                for (i = 0; i < 16; i++)
                {
                    firstBlock[i] = 0;
                }
                fstream.Read(firstBlock, 0, 16);           // Read the first block into RAM

                // Check for the format identifier string
                string str = mReadString( firstBlock, 6, 6);

                if (0 != str.CompareTo(EVENT_ID_STRING))
                {
                    #region AES
#if SCP_AES == false
                    mLogError("File may be encrypted. No decrypt present.");
                    fstream.Close();
                    return false;
#else
                   mLogImport( "File may be encrypted. Attempting to decrypt." );

                    for (i = 0; i < 16; i++)
                    {
                        tempKey[i] = constKey[i];
                        tempIV[i] = constIV[i];
                    }

                    aes_setkey_dec(&aes_ctx, tempKey, 128);
                    // Decrypt the first block of the file
                    if (i = aes_crypt_cbc(&aes_ctx, AES_DECRYPT, 16, tempIV, firstBlock, firstBlock))
                    {
                        mLogImport( "AES Decrypt Error 1. (0x" << hex << i << ") Aborting." );
                        return -1;
                    }

                    // Check to make sure we now have a valid file format string
                    str.assign((const char*) &firstBlock[6]);
                    if (str.compare(0, 6, EVENT_ID_STRING))
                    {
                        mLogImport( "Invalid File Format String. Aborting." );
                        return -1;
                    }

                    mLogImport( "File decryption successful." );

                    i = 0;
                    shortCaster = (UInt16*) &firstBlock[i];
                    theircrcValue = *shortCaster;
                    i += 2;

                    intCaster = (uint*) &firstBlock[i];
                    length = *intCaster;
                    i += 4;

                    if (length > fileLength)
                    {
                        mLogImport( "File Corrupted. Invalid Length value. Aborting." );
                        return -1;
                    }

                    int encryptLength = fileLength;

                    readBuffer = new Byte[encryptLength];   // Allocate enough space to read in the whole file     
                    for (i = 0; i < 16; i++)
                    {
                        readBuffer[i] = firstBlock[i];                 // Copy the first block into the file buffer
                    }
                    inFile.read((char*)&readBuffer[16], encryptLength - 16);    // Store the remainder of the file in memory

                    // Decrypt the remainder of the file
                    if (i = aes_crypt_cbc(&aes_ctx, AES_DECRYPT, encryptLength - 16, tempIV, &readBuffer[16], &readBuffer[16]))
                    {
                        mLogImport( "AES Decrypt Error 2. (0x" << hex << i << ") Aborting." );
                        return -1;
                    }

                    crcBlock(&readBuffer[2], length - 2, &ourcrcValue); // Calculate the CRC for the remainder of the file
#endif
                    #endregion
                }
                else
                {
                    mLogImport( "File not encrypted. Processing" );

                    i = 0;
                    theircrcValue = mReadUInt16(firstBlock, i);
                    i += 2;

                    length = mReadUInt32(firstBlock, i);
                    i += 4;

                    readBuffer = new Byte[fileLength+16];          // Allocate enough space to read in the whole file     
                    for (i = 0; i < 16; i++)
                    {
                        readBuffer[i] = firstBlock[i];                 // Copy the first block into the file buffer
                    }
                    fstream.Read(readBuffer, 16, fileLength - 16);    // Store the remainder of the file in memory

                    ourcrcValue = mCalcCrcBlock(readBuffer, 2, length - 2); // Calculate the CRC for the remainder of the file
                }

#if CHECK_CRC
                if (ourcrcValue == theircrcValue)
                {            // If the CRC doesn't match, something has gone wrong
                    mLogImport( "File CRC Valid: " +  theircrcValue.ToString("X2") );  // Notify user that we are parsing
#else
                    mLogImport( "Ignoring CRC (0x" << hex << theircrcValue
                      << " : 0x" << ourcrcValue << ")" << dec );    // Ignore the CRC values (debugging ONLY)
#endif

                    #region HEADER
                    i = 6;
                    mLogImport( "File Length: " +length.ToString() );            // Print out the file length 

                    mLogImport( "Format Identifier String: " + mReadString( readBuffer, i, 6 ));
                    i += 6;
                    mLogImport("Device Identifier String: " + mReadString(readBuffer, i, 6));
                    i += 6;

                    uint firmwareVersion = readBuffer[i];              // Firmware version (10 == 1.0)
                    mLogImport( "Firmware Version: " + (firmwareVersion / 10).ToString()
                        + "." + (firmwareVersion % 10).ToString() );
                    i += 1;
                    mAddRemData("Firmw", (firmwareVersion / 10).ToString()
                        + "." + (firmwareVersion % 10).ToString(), true);

                    _mDeviceID = mReadString(readBuffer, i, 8); // Parse out the Serial Number String 
                    mAddRemData("DeviceID", _mDeviceID, true); 
                    i += 8;

                    _mPatientID = mReadString(readBuffer, i, 40); // Parse out the Patient ID
                    mAddRemData("PatienteID", _mPatientID, true);
                    i += 40;
 

                    uint tag = readBuffer[i];                                         // Identifier tag
                    i += 1;
                    uint sequenceNumber = mReadUInt32(readBuffer, i);               // Sequence number
                    mAddRemData("SequenceNr", sequenceNumber.ToString(), true);
                    i += 4;
                    uint sampleCount = mReadUInt16(readBuffer, i);      // Sample count
                    mAddRemData("sampleCount", sampleCount.ToString(), true);
                    i += 2;
                    uint year = mReadUInt16(readBuffer, i);             // Year
                    i += 2;
                    uint month = readBuffer[i];                                       // Month
                    i += 1;
                    uint day = readBuffer[i];                                         // Day
                    i += 1;
                    uint hour = readBuffer[i];                                        // Hour
                    i += 1;
                    uint minute = readBuffer[i];                                      // Minute
                    i += 1;
                    uint second = readBuffer[i];                                      // Seconds
                    i += 1;
                    uint milliseconds = (uint)(readBuffer[i] * 4);                            // Milliseconds / 4
                    i += 1;
                    int timeZone = readBuffer[i] + ((short)readBuffer[i + 1] << 8);           // Time zone (minutes from UTC)
                    i += 2;

                    if(milliseconds > 999)
                    {
                        milliseconds = 999;
                    }
                    _mEventTimeUTC = new DateTime((int)year, (int)month, (int)day, (int)hour, (int)minute, (int)second, (int)milliseconds);
                    _mTimeOffsetMin = (Int16)timeZone;
                    mAddRemData("EventUTC", year.ToString("D4")+"\\"+month.ToString("D2") + "\\" +day.ToString("D2") + " " 
                        +hour.ToString("D2") + ":" +minute.ToString("D2") + ":" +second.ToString("D2") + "." +milliseconds.ToString("D3") 
                        + "+TZ" +(timeZone/60).ToString("D2") + ":" + (timeZone >= 0 ? timeZone % 60 : -timeZone % 60).ToString(), true);

                    uint dataLength = readBuffer[i];                                  // Data Length
                    i += 1;
                    uint data = 0, k, mult = 1;
                    for (k = 0; k < dataLength; k++)
                    {
                        data = data + readBuffer[i + k] * mult;                      // Data Value
                        mult *= 256;
                    }
                    i += (UInt16)dataLength;
                    uint fCount = readBuffer[i];                                      // Number of files
                    i += 1;
                    uint fStart = mReadUInt32(readBuffer, i); // First file sent
                    bool fileRead = 0 != readBuffer[i];

                    _mFirstFileNr = fStart;
                    _mNrFiles = (UInt16)fCount;
                    _mEventFileIndex = sequenceNumber;
                    _mEventSampleIndex = sampleCount;
                    _mTag = (UInt16)tag;
                    _mTagData = data;


                    mAddRemData("File", _mFirstFileNr.ToString(), true);
                    mAddRemData("nFile", _mNrFiles.ToString(), true);

                    if(_mNrFiles > 0 )
                    {
                        string fileStr = _mFirstFileNr.ToString();
                        if (_mNrFiles > 1)
                        {
                            fileStr += "n" + _mNrFiles.ToString();
                        }
                        mAddRemLabel("File", fileStr);
                    }

                    mLogImport( "SCP-ECG Sequence Number: " + sequenceNumber.ToString() );
                    mLogImport( "Event occurred at sample " + sampleCount.ToString());
                    mLogImport( "Bytes of Data: " + dataLength.ToString());
                    mLogImport( "Number of attached SCP-ECG files: " + fCount.ToString());
                    mLogImport( "SCP-ECG Sequence Number of first file: " + fStart.ToString());
                    #endregion HEADER

                    mLogImport( "Event Reported: ");
                    bOk = tag != 0;

                    switch ((DTzEventTags)tag)
                    {                                    // Decode the TAG and interpret the data
                        #region TAG
                        case DTzEventTags.BPM_COUNT_TAG:
                            mAddRemData("nHR", data.ToString(), true);
                            mLogImport( "Samples used for Heart Rate calculations: " + data.ToString());
                            break;
                        case DTzEventTags.BPM_STDEV_TAG:
                            mAddRemData("HRstd", ((float)data / 256.0).ToString(), true);
                            mLogImport( "Heart Rate STDEV (BPM): " + ((float)data / 256.0).ToString());
                            break;
                        case DTzEventTags.BPM_AVERAGE_TAG:
                            mAddRemData("HRavg", data.ToString(), true);
                            mLogImport( "Average Heart Rate (BPM): " + data.ToString());
                            break;
                        case DTzEventTags.BPM_MIN_TAG:
                            mAddRemData("HRmin", data.ToString(), true);
                            mLogImport( "Minimum Heart Rate (BPM): " + data.ToString());
                            break;
                        case DTzEventTags.BPM_MAX_TAG:
                            mAddRemData("HRmax", data.ToString(), true);
                            mLogImport( "Maximum Heart Rate (BPM): " + data.ToString());
                            break;
                        case DTzEventTags.START_TAG:
                            mAddRemData("Start", data.ToString(), true);
                            mLogImport( "ECG Recording Started:" + data.ToString());
                            break;
                        case DTzEventTags.STOP_TAG:
                            mAddRemData("Stop", data.ToString(), true);
                            mLogImport( "ECG Recording Stopped:" + data.ToString());
                            break;
                        case DTzEventTags.BREAK_TAG:
                            mAddRemData("Break", data.ToString(), true);
                            mLogImport( "ECG Recording Interrupted:" + data.ToString());
                            break;
                        case DTzEventTags.RESUME_TAG:
                            mAddRemData("Resume", data.ToString(), true);
                            mLogImport( "ECG Recording Resumed:" + data.ToString());
                            break;
                        case DTzEventTags.FULL_TAG:
                            mAddRemData("Full", data.ToString(), true);
                            mLogImport( "ECG Recording Full:" + data.ToString());
                            break;
                        case DTzEventTags.PACEMAKER_DETECTION_TAG:
                            mAddRemLabel("PaceM", "");
                            _mbDoEvent = true;
                            mAddRemData("PaceM", data.ToString(), true);
                            mLogImport( "Number of pacemaker spikes detected: " + data.ToString());
                            break;
                        case DTzEventTags.QRS_DETECTION_TAG:
                            mAddRemLabel("QRS", "" + ((char)data & 0xff));
                            mAddRemData("QRS", ((char)data & 0xff) + "$" + ((int)data >> 8).ToString("X2"), true);
                            {
                                char c = (char)(data & 0x00ff);
                                if( c >= ' ')
                                {
                                    qrsSeq += c;
                                    if (qrsSet == null || qrsSet.IndexOf(c) < 0) qrsSet += c;
                                }
                            }
                            _mbDoEvent = true;
CProgram.sLogLine("QRS Detected(" + ((char)data & 0xff) + "). Mask: 0x" + ((int)data >> 8).ToString("X2"));
                            mLogImport( "QRS Detected(" + ((char)data & 0xff) + "). Mask: 0x" +((int)data >> 8).ToString("X2" ) );
                            break;
                        case DTzEventTags.TACHY_ENTRY_TAG:
                            mAddRemLabel("Tachy On", "");
                            _mbDoEvent = true;
                            mAddRemData("TachyOn$", data.ToString("X2"), true);
                            mLogImport( "Tachycardia Onset. Mask: 0x" + data.ToString("X2") );
                            break;
                        case DTzEventTags.TACHY_EXIT_TAG:
                            mAddRemLabel("Tachy off", "");
                            _mbDoEvent = true;
                            mAddRemData("TachyOff$", data.ToString("X2"), true);
                            mLogImport( "Tachycardia Offset. Mask: 0x" + data.ToString("X2") );
                            break;
                        case DTzEventTags.BRADY_ENTRY_TAG:
                            mAddRemLabel("Brady On", "");
                            _mbDoEvent = true;
                            mAddRemData("BradyOn$", data.ToString(), true);
                            mLogImport( "Bradycardia Onset. Mask: 0x" + data.ToString("X2") );
                            break;
                        case DTzEventTags.BRADY_EXIT_TAG:
                            mAddRemLabel("Brady off", "");
                            _mbDoEvent = true;
                            mAddRemData("BradyOff$", data.ToString("X2"), true);
                            mLogImport( "Bradycardia Offset. Mask: 0x" + data.ToString("X2") );
                            break;
                        case DTzEventTags.PAUSE_ENTRY_TAG:
                            mAddRemLabel("Pause On", "");
                            _mbDoEvent = true;
                            mAddRemData("PauseOn$", data.ToString(), true);
                            mLogImport( "Pause Onset. Mask: 0x" + data.ToString("X2") );
                            break;
                        case DTzEventTags.PAUSE_EXIT_TAG:
                            mAddRemLabel("Pause off", "");
                            _mbDoEvent = true;
                            mAddRemData("PauseOff$", data.ToString("X2"), true);
                            mLogImport( "Pause Offset. Mask: 0x" + data.ToString("X2") );
                            break;
                        case DTzEventTags.AFIB_ENTRY_TAG:
                            mAddRemLabel("Afib On", "");
                            _mbDoEvent = true;
                            mAddRemData("AFibOn$", data.ToString("X2"), true);
                            mLogImport( "Atrial Fibrillation Onset. Mask: 0x" + data.ToString("X2") );
                            break;
                        case DTzEventTags.AFIB_EXIT_TAG:
                            mAddRemLabel("Afib Off", "");
                            _mbDoEvent = true;
                            mAddRemData("AfibOff$", data.ToString("X2"), true);
                            mLogImport( "Atrial Fibrillation Offset. Mask: 0x" + data.ToString("X2") );
                            break;
                        case DTzEventTags.TACHY_RATE_CHANGE_TAG:
                            mAddRemLabel("Tachy", data.ToString());
                            _mbDoEvent = true;
                            mAddRemData("TachyChng", data.ToString(), true);
                            mLogImport( "Tachycardia Rate Change. BPM: " + data.ToString() );
                            break;
                        case DTzEventTags.BRADY_RATE_CHANGE_TAG:
                            mAddRemLabel("Brady", data.ToString());
                            _mbDoEvent = true;
                            mAddRemData("BradyChng", data.ToString(), true);
                            mLogImport( "Bradycardia Rate Change. BPM: " + data.ToString());
                            break;
                        case DTzEventTags.PVC_ENTRY_TAG:
                            mAddRemLabel("PVC On", "");
                            _mbDoEvent = true;
                            ++pvcCount;
                            mAddRemData("PVCon$", data.ToString("X2"), true);
 CProgram.sLogLine("PVC Onset. Mask: 0x" + data.ToString("X2"));
                            mLogImport( "PVC Onset. Mask: 0x" + data.ToString("X2") );
                            break;
                        case DTzEventTags.ANALYSIS_RESUMED_TAG:
                            mAddRemLabel("PVC off", "");
                            _mbDoEvent = true;
                            mAddRemData("AnalRes$", data.ToString("X2"), true);
                            mLogImport( "Arrhythmia Analysis Resumed. Mask: 0x" + data.ToString("X2"));
                            break;
                        case DTzEventTags.ANALYSIS_PAUSED_TAG:
                            mAddRemData("AnalPaused", data.ToString("X2"), true);
                            mLogImport( "Arrhythmia Analysis Paused. Mask: 0x" + data.ToString("X2"));
                            break;
                        case DTzEventTags.LEAD_DISCONNECTED:
                            mAddRemData("LeadOff$", data.ToString("X2"), true);
                            mLogImport( "ECG Electrodes Disconnected. Mask: 0x" + data.ToString("X2")  );
                            break;
                        case DTzEventTags.MISSING_SCP_TAG:
                            mAddRemData("MissScp", data.ToString(), true);
                            mLogImport( "Missing SCP File." );
                            break;
                        case DTzEventTags.MISSING_EVENT_TAG:
                            mAddRemData("MissEvent", data.ToString(), true);
                            mLogImport( "Missing Event File. Event Number: " + data.ToString());
                            break;
                        case DTzEventTags.PATIENT_TAG:
                            mAddRemData("PatEvent$", data.ToString("X2"), true);
                            mLogImport( "Patient Activated Event. Data: 0x" + data.ToString("X2"));
                            if (0 == data)
                            {
                                mLogImport( "---No Diary Info." );
                            }
                            else
                            {
                                UInt16 sNr = (UInt16)(data & 0x00ff);
                                UInt16 aNr = (UInt16)((data >> 8) & 0x00ff);

                                string sympAct = mGetSymptomActivity(_mDeviceID, sNr, aNr);

                                if( sympAct.Length>0)
                                {
                                    mAddRemLabel(sympAct, "");
                                }
                            }
                            break;
                        case DTzEventTags.BATTERY_VALUE_TAG:
                            mAddRemData("Bat-mV", data.ToString(), true);
                            mLogImport( "Battery Voltage (mV): " + data.ToString());
                            break;
                        case DTzEventTags.BATTERY_LOW_TAG:
                            mAddRemData("LowBat-mV", data.ToString(), true);
                            mLogImport( "Low Battery (mV): " + data.ToString());
                            break;
                        case DTzEventTags.CHARGING_STARTED_TAG:
                            mAddRemData("ChrgStart", data.ToString(), true);
                            mLogImport( "Charging Started" );
                            break;
                        case DTzEventTags.CHARGING_STOPPED_TAG:
                            mAddRemData("ChrgStop", data.ToString(), true);
                            mLogImport( "Charging Stopped" );
                            break;
                        case DTzEventTags.TIME_ZONE_CHANGE_TAG:
                            mAddRemData("TimeZone", data.ToString(), true);
                            mLogImport( "Time Zone Changed. New offset (minutes): " + data.ToString());
                            break;
                        case DTzEventTags.SERVER_REQUEST_TAG:
                            mAddRemLabel("Request", "");
                            _mbDoEvent = true;
                            mAddRemData("SVRreq", data.ToString(), true);
                            mLogImport( "Server Requested " + data.ToString() + " SCP Files" );

                            if (false == _sbEventRequestScp)
                            {
                                _mbDoEvent = false;
                                CProgram.sLogLine("TZ Server Request disregarded! " + data.ToString() + " SCP Files ");
                                _mNrFiles = 0;
                            }
                            break;
                        case DTzEventTags.TZR_BULK_UPLOAD:
                            mAddRemLabel("Bulk Dump", "");
                            _mbDoEvent = true;
                            mAddRemData("SVRdump", data.ToString(), true);
                            mLogImport("Server Dump " + data.ToString() + " SCP Files");

                            if( false == _sbEventBulkUpload )
                            {
                                _mbDoEvent = false;
                                CProgram.sLogLine("TZ Bulk Upload disregarded! " + data.ToString() + " SCP Files ");
                                _mNrFiles = 0;
                            }
                            break;

                        case DTzEventTags.TZR_REQUEST_TAG:
                            mAddRemData("TZRreq", ((int)data >> 8).ToString() + "d" + (data & 0xff).ToString() + "h", true);
                            mLogImport( "Interval File Request. Day: " +((int)data >> 8).ToString() + ", Hour: " + (data & 0xff).ToString());
                            break;
                        case DTzEventTags.SCP_RETRANSMIT_TAG:
                            mAddRemData("SCPrx$", data.ToString("X2"), true);

                            mLogImport( "SCP Retransmission: 0x" + data.ToString("X2"));
                            break;
                        case DTzEventTags.SETTINGS_SUCCESS_TAG:
                            mAddRemData("SetOk", data.ToString(), true);
                            mLogImport( "Settings Downloaded Successfully. File ID: "+data.ToString());
                            break;
                        case DTzEventTags.SETTINGS_FAILURE_TAG:
                            mAddRemData("SetErr", data.ToString(), true);
                            mLogImport( "Settings Download Error Code: " + data.ToString());
                            break;
                        case DTzEventTags.ACTIONS_FAILURE_TAG:
                            mAddRemData("ActErr", data.ToString(), true);
                            mLogImport( "Actions Download Error Code: " + data.ToString());
                            break;
                        case DTzEventTags.ACTIONS_SUCCESS_TAG:
                            mAddRemData("ActFile", data.ToString(), true);
                            mLogImport( "Actions Downloaded Successfully. File ID: " + data.ToString());
                            break;
                        case DTzEventTags.SMS_COMMAND_ERROR_TAG:
                            mAddRemData("SMSerr", data.ToString(), true);
                            mLogImport( "SMS Command Error Code: " + data.ToString());
                            break;
                        case DTzEventTags.SMS_MSG_RECEIVED:
                            mAddRemData("SMSrecv", data.ToString(), true);
                            mLogImport( "SMS Message Acknowledge Code: " + data.ToString());
                            break;
                        case DTzEventTags.FTP_SETTINGS_CHANGED:
                            mAddRemData("FTP", data.ToString(), true);
                            mLogImport( "Change in FTP Settings" );
                            break;
                        case DTzEventTags.TERMINATOR_TAG:          // The final entry should always have a value of 0xff
                            mAddRemData("Term$", data.ToString("X2"), true);
                            if (data == 0xffff) mLogImport( "Final Entry." );
                            else mLogImport( "Invalid Terminator." );
                            break;
                        default:
                            mAddRemData("?tag", data.ToString(), true);
                            mLogImport( "ERROR: unknown tag(" + tag.ToString() + ") data(" + data.ToString("X2") + ")"  );
                            bOk = false;
                            break;
                            #endregion TAG
                    }

                    mLogImport( "File Read Flag: " + fileRead.ToString());

                    if (qrsSeq != null && qrsSet.Length > 0 || pvcCount > 0)
                    {
                        mLogImport("File Read qrs: " + qrsSet + ", " + qrsSeq);
                        mAddRemLabel("qrs", qrsSet);
                    }
                    if (pvcCount > 0)
                    {
                        mLogImport("File Read pvc count = " + pvcCount.ToString());
                        mAddRemLabel("pvc", pvcCount.ToString());
                    }

#if CHECK_CRC
                }
                else
                {
                    mLogImport( "File CRC Invalid. Halting" );
                    mLogImport( "their CRC: " + theircrcValue.ToString("X2" ) + " - our CRC: " + ourcrcValue.ToString("X2"));
                    mLogImport( "File Length: " + length.ToString() );            // Print out the file length      
                }
#endif
                mLogImport( "Done.\n" );                 // Signal completion of program
            }
            catch( Exception ex )
            {
                CProgram.sLogException("Failed import TZE file " + fileName, ex);
            }
            if(fstream != null)
            {
                fstream.Close();
                fstream.Dispose();
            }
            return bOk;
        }
    }
}
