﻿using Event_Base;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using Program_Base;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventBoard.Event_Base
{

    public enum DPdfDocRecVars
    {
        // primary keys
        PdfFileName,

        // variables
        Study_IX,
        StudySubNr, 
        Patient_IX,
        ReportType,
        FileUTC,
        CreadedByInitials,
        CreatedAt,
        RefID,
        StartPeriodUtc,
        EndPeriodUtc,
        SentTo,

        NrSqlVars       // keep as last
    };


    public class CPdfDocRec : CSqlDataTableRow
    {
        // dBase variables

        public UInt32 _mStudy_IX;
        public UInt16 _mStudySubNr;     // report number in study 
        public UInt32 _mPatient_IX;
        public string _mReportType; // code for report type
        public string _mPdfFileName;
        public DateTime _mFileUTC;
        public string _mCreadedByInitials;
        public string _mCreatedAt;  // windows user @ PC name
        public UInt32 _mRefID;      // Analysis or Study report record
        public DateTime _mStartPeriodDT;    // device patient time
        public DateTime _mEndPeriodDT;

        public string _mSentTo; // list of email receivers + date time UTC in one string {+a@c.d(YYYYMMDDhhmmss)} +=ok send,-=failed

        // Document Generation variables

        public UInt16 _mPageSize;
        public double _mMarginTop;
        public double _mMarginLeft;
        public double _mMarginRight;
        public double _mMarginBottom;

        // Document Generation defaults

        public static bool _sDefaultsLoaded = false;
        public static UInt16 _sDefaultPageSize = (UInt16)PdfSharp.PageSize.A4;
        public static double _sDefaultMarginTop = 0.05;
        public static double _sDefaultMarginLeft = 0.055;
        public static double _sDefaultMarginRight = 0.05;
        public static double _sDefaultMarginBottom = 0.05;


        // Document creation 
        public PdfDocument _mPdfDocument = null;// new PdfDocument();

        public PdfPage _mPdfPage = null; // document.AddPage();
        public XGraphics _mPdfGfx = null; // XGraphics.FromPdfPage(page);
        public UInt16 _mNrPages = 0;
        public bool _mbSaved = false;

        public CPdfDocRec(CSqlDBaseConnection ASqlConnection) : base(ASqlConnection, "d_PdfDocRec")
        {
            try
            {
                UInt64 maskPrimary = CSqlDataTableRow.sGetMask((UInt16)DPdfDocRecVars.PdfFileName);
                UInt64 maskValid = CSqlDataTableRow.sGetMaskRange((UInt16)0, (UInt16)DPdfDocRecVars.NrSqlVars - 1);// mGetValidMask(false);

                mInitTableMasks(maskPrimary, maskValid);  // set primairyInsertFlags

                mClear();
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init ", ex);
            }
        }
        ~CPdfDocRec()
        {
            if( _mPdfPage != null )
            {
                _mPdfPage.Close();
            }
            if(_mPdfGfx != null)
            {
                _mPdfGfx.Dispose();
            }
            if(_mPdfDocument != null)
            {
                _mPdfDocument.Dispose();
            }
        }
        public override CSqlDataTableRow mCreateNewRow()
        {
            return new CPdfDocRec(mGetSqlConnection());
        }
        public override bool mbCopyTo(CSqlDataTableRow ATo) // derived  public override void mbCopyTo() must call base.mbCopyTo(ATo)
        {
            bool bOk = false;

            if (ATo != null)
            {
                CPdfDocRec to = ATo as CPdfDocRec;

                if (to != null)
                {
                    bOk = base.mbCopyTo(ATo);

                    to._mStudy_IX = _mStudy_IX;
                    to._mStudySubNr = _mStudySubNr;
                    to._mPatient_IX = _mPatient_IX;
                    to._mReportType = _mReportType;
                    to._mPdfFileName = _mPdfFileName;
                    to._mFileUTC = _mFileUTC;
                    to._mCreadedByInitials = _mCreadedByInitials;
                    to._mCreatedAt = _mCreatedAt;
                    to._mRefID = _mRefID;
                    to._mStartPeriodDT = _mStartPeriodDT;
                    to._mEndPeriodDT = _mEndPeriodDT;
                    to._mSentTo = _mSentTo;
                }
            }
            return bOk;
        }

        public override void mClear()
        {
            base.mClear();
            _mStudy_IX = 0;
            _mStudySubNr = 0;
            _mPatient_IX = 0;
            _mReportType = "";
            _mPdfFileName = "";
            _mCreadedByInitials = "";
            _mCreatedAt = "";
            _mRefID = 0;
            _mStartPeriodDT = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local);
            _mEndPeriodDT = _mStartPeriodDT;
            _mFileUTC = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc); ;
            _mSentTo = "";
            mResetMargins();

            if (_mPdfPage != null)
            {
                _mPdfPage.Close();
            }
            if (_mPdfGfx != null)
            {
                _mPdfGfx.Dispose();
            }
            if (_mPdfDocument != null)
            {
                _mPdfDocument.Dispose();
            }

            _mPdfDocument = null;
            _mPdfPage = null; 
            _mPdfGfx = null; 
            _mNrPages = 0;
            _mbSaved = false;
        }

        public void mResetMargins()
        {
            try
            {
                if (false == _sDefaultsLoaded)
                {
                    // read from config file
                    _sDefaultPageSize = (UInt16)Properties.Settings.Default.PdfPageSize;
                    if (_sDefaultPageSize == 0) _sDefaultPageSize = (UInt16)PdfSharp.PageSize.A4;

                    _sDefaultMarginTop = Properties.Settings.Default.PdfMarginTop;
                    _sDefaultMarginLeft = Properties.Settings.Default.PdfMarginLeft;
                    _sDefaultMarginRight = Properties.Settings.Default.PdfMarginRight;
                    _sDefaultMarginBottom = Properties.Settings.Default.PdfMarginBottom;

                    _sDefaultsLoaded = true;
                }
                _mPageSize = _sDefaultPageSize;
                _mMarginTop = _sDefaultMarginTop;
                _mMarginLeft = _sDefaultMarginLeft;
                _mMarginRight = _sDefaultMarginRight;
                _mMarginBottom = _sDefaultMarginBottom = 0.1;

            }
            catch (Exception ex)
            {
                CProgram.sLogException("ResetMargin ", ex);
            }
        }

        public PdfSharp.PageSize mGetPageSize()
        {
            return (PdfSharp.PageSize)_mPageSize;
        }

        public override DSqlDataType mVarGetSet(DSqlDataCmd ACmd, UInt16 AVarIndex, ref string AVarSqlName, ref string AStringValue, out bool ArbValid)
        {
            /*        public UInt32 _mStudy_IX;
                    public UInt32 _mPatient_IX;
                    public string _mReportType; // code for report type
                    public string _mPdfFileName;
                    public string _mCreatedAt;  // windows user @ PC name
                    public UInt32 _mRefID;      // Analysis or Study report record
                    public DateTime _mStartPeriodUTC;
                    public DateTime _mEndPeriodUTC;
                    */
            switch ((DPdfDocRecVars)AVarIndex)
            {
                case DPdfDocRecVars.PdfFileName:
                    return mSqlGetSetString(ref _mPdfFileName, "PdfFileName", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);

                case DPdfDocRecVars.Study_IX:
                    return mSqlGetSetUInt32(ref _mStudy_IX, "Study_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DPdfDocRecVars.StudySubNr:
                    return mSqlGetSetUInt16(ref _mStudySubNr, "StudySubNr", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DPdfDocRecVars.Patient_IX:
                    return mSqlGetSetUInt32(ref _mPatient_IX, "Patient_IX", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DPdfDocRecVars.ReportType:
                    return mSqlGetSetString(ref _mReportType, "ReportType", ACmd, ref AVarSqlName, ref AStringValue, 8, out ArbValid);
                case DPdfDocRecVars.FileUTC:
                    return mSqlGetSetUTC(ref _mFileUTC, "FileUTC", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);

                case DPdfDocRecVars.CreadedByInitials:
                    return mSqlGetSetString(ref _mCreadedByInitials, "CreadedByInitials", ACmd, ref AVarSqlName, ref AStringValue, 8, out ArbValid);
                    
                case DPdfDocRecVars.CreatedAt:
                    return mSqlGetSetString(ref _mCreatedAt, "CreatedAt", ACmd, ref AVarSqlName, ref AStringValue, 64, out ArbValid);
                case DPdfDocRecVars.RefID:
                    return mSqlGetSetUInt32(ref _mRefID, "RefID", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DPdfDocRecVars.StartPeriodUtc:
                    return mSqlGetSetDateTime(ref _mStartPeriodDT, "StartPeriodDT", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DPdfDocRecVars.EndPeriodUtc:
                    return mSqlGetSetDateTime(ref _mEndPeriodDT, "EndPeriodDT", ACmd, ref AVarSqlName, ref AStringValue, out ArbValid);
                case DPdfDocRecVars.SentTo:
                    return mSqlGetSetString(ref _mSentTo, "SentTo", ACmd, ref AVarSqlName, ref AStringValue, 1024, out ArbValid);

            }
            return base.mVarGetSet(ACmd, AVarIndex, ref AVarSqlName, ref AStringValue, out ArbValid);
        }

        public void mSetFileName(UInt32 AStudyIx, UInt16 AStudySubNr, UInt32 APatientIX, string AReportType, string ARefLetter, UInt32 ARefID, string AExtraName)
        {
            _mFileUTC = DateTime.UtcNow;

            string s = "S" + AStudyIx.ToString("D08") + "_" + _mFileUTC.Year.ToString("D04") + _mFileUTC.Month.ToString("D02") + _mFileUTC.Day.ToString("D02")
                + _mFileUTC.Hour.ToString("D02") + _mFileUTC.Minute.ToString("D02") + _mFileUTC.Second.ToString("D02")
                + "_" + AReportType + "_N" + _mStudySubNr.ToString("D04" ) + "_" +  ARefLetter + ARefID.ToString("D08");

            if( AExtraName != null && AExtraName.Length > 0)
            {
                s += "_" + AExtraName;
            }
            _mPdfFileName = s + ".pdf";
        }

        public PdfDocument mbNewDocument( string ACreadedByInitials, UInt32 AStudyIx, UInt16 AStudySubNr, UInt32 APatientIX, string AReportType, string ARefLetter, UInt32 ARefID, string AExtraName, 
            DateTime AStartPeriodDT, DateTime AEndPeriodDT )
        {
            mClear();

            _mCreadedByInitials = ACreadedByInitials;
            _mStudy_IX = AStudyIx;
            _mStudySubNr = AStudySubNr;
            _mPatient_IX = APatientIX;
            _mReportType = AReportType;
            _mRefID = ARefID;
            if(AStartPeriodDT != null && AStartPeriodDT.Kind == DateTimeKind.Utc
                || AEndPeriodDT != null && AEndPeriodDT.Kind == DateTimeKind.Utc)
            {
                CProgram.sPromptError(false, "Print Pdf document", "Invalid Date type for document type " + AReportType);
            }

            _mStartPeriodDT = DateTime.SpecifyKind(AStartPeriodDT, DateTimeKind.Local );
            _mEndPeriodDT = DateTime.SpecifyKind(AEndPeriodDT, DateTimeKind.Local);
            mSetFileName(AStudyIx, AStudySubNr, APatientIX, AReportType, ARefLetter, ARefID, AExtraName);

            if (_mStartPeriodDT == DateTime.MaxValue || _mStartPeriodDT == DateTime.MinValue) _mStartPeriodDT = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local);
            if (_mEndPeriodDT == DateTime.MaxValue || _mEndPeriodDT == DateTime.MinValue) _mEndPeriodDT = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local);
            if(_mStartPeriodDT > _mEndPeriodDT)
            {
                DateTime dt = _mStartPeriodDT;
                _mStartPeriodDT = _mEndPeriodDT;
                _mEndPeriodDT = dt;
            }

            _mPdfDocument = new PdfDocument();

            return _mPdfDocument;
        }

        public bool mbSaveToDir( string APath, out string AFullName, bool AbInsertIntoTable )
        {
            _mbSaved = false;
            string filename = "";

            try
            {

                if (_mPdfDocument != null && _mPdfFileName != null && _mPdfFileName.Length > 3 )
                {
                    filename = Path.Combine(APath, _mPdfFileName);
                    _mPdfDocument.Save(filename);
                    _mbSaved = true;
                    
                    _mCreatedAt = CProgram.sGetPcUserName() + "@" + CProgram.sGetPcName();

                    if(AbInsertIntoTable)
                    {
                        if (false == mbDoSqlInsert())
                        {
                            CProgram.sPromptError(false, "Create Pdf", "Failed to add " + _mPdfFileName + " to the pdfDoc list");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed SavePdf", ex);
            }
            AFullName = filename;
            return _mbSaved;
        }
        public bool mbSaveToStudyPdf( out string AFullName, bool AbInsertIntoTable)
        {
            bool bOk = false;
            string filename = "";

            _mbSaved = false;
            if ( _mStudy_IX != 0 && _mPdfDocument != null )
            {
                string studyDir;
                    
                if( CDvtmsData.sbCreateStudyPdfDir(out studyDir,_mStudy_IX))
                {
                    bOk = mbSaveToDir(studyDir, out filename, AbInsertIntoTable);
                }
            }
            AFullName = filename;
            return bOk;
        }

        public PdfPage mAddPage( PageOrientation AOrientaion )
        {
            if(_mPdfGfx != null )
            {
                _mPdfGfx.Dispose();
            }
            if( _mPdfPage != null)
            {
                _mPdfPage.Close();
            }
            _mPdfPage = null;
            _mPdfGfx = null;

            try
            {
                if (_mPdfDocument != null)
                {
                    _mPdfPage = _mPdfDocument.AddPage();
                    if (_mPdfPage != null)
                    {
                        ++_mNrPages;
                        _mPdfPage.Size = (PdfSharp.PageSize)_mPageSize;
                        _mPdfPage.Orientation = AOrientaion;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed new Pdf page", ex);
            }
            return _mPdfPage;
        }

        public XGraphics mGetGraphics()
        {
            try
            {
                if (_mPdfGfx == null && _mPdfPage != null)
                {
                    _mPdfGfx = XGraphics.FromPdfPage(_mPdfPage);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed new Graphics from Pdf page", ex);
            }
            return _mPdfGfx;

        }

        public double mGetPageWorkLeftX()
        {
            double x = 0.0;

            if( _mPdfPage != null )
            {
                x = _mPdfPage.Width * _mMarginLeft;
            }
            return x;
        }

        public double mGetPageWorkTopY()
        {
            double y = 0.0;

            if (_mPdfPage != null)
            {
                y = _mPdfPage.Width * _mMarginTop;
            }
            return y;
        }
        public double mGetPageWorkWidth()
        {
            double w = 0.0;

            if (_mPdfPage != null)
            {
                w = _mPdfPage.Width * ( 1 - _mMarginLeft - _mMarginRight );
            }
            return w;
        }

        public double mGetPageWorkHeight()
        {
            double h = 0.0;

            if (_mPdfPage != null)
            {
                h = _mPdfPage.Height * ( 1 - _mMarginTop - _mMarginBottom );
            }
            return h;
        }

        public bool mbAddImagePage(PageOrientation APageOrientation, Image AImagePage)
        {
            bool bOk = false;
            try
            {
                PdfPage page = mAddPage(APageOrientation);

                if (_mPdfPage != null && AImagePage != null )
                {
                    double wPage = mGetPageWorkWidth();
                    double hPage = mGetPageWorkHeight();
                    int wBitmap = AImagePage.Width;
                    int hBitmap = AImagePage.Height;

                    if( hBitmap > 0 && wBitmap > 0 && hPage > 0.001 && wBitmap > 0.00 )
                    {
                        double x = mGetPageWorkLeftX();
                        double y = mGetPageWorkTopY();
                        double ratioX = wPage / (double)wBitmap;
                        double ratioY = hPage / (double)hBitmap;
                        double w = wPage;// - x;// - ratioX;
                        double h = hPage;// - y - ratioY;

                        if (ratioX < ratioY)
                        {
                            h = wPage * hBitmap / (double)wBitmap;
                        }
                        else
                        {
                            w = hPage * wBitmap / (double)hBitmap;
                        }

                        if(_mPdfGfx != null)
                        {
                            _mPdfGfx.Dispose();
                        }
                        _mPdfGfx = XGraphics.FromPdfPage(page);
                        if( _mPdfGfx != null )
                        {
                            _mPdfGfx.DrawImage(AImagePage, x, y, w, h);
                            bOk = true;
                        }
                        else
                        {
                            CProgram.sLogLine("Failed Graphics from Pdf page");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed new Graphics from Pdf page", ex);
            }
            return bOk;
        }

        public static void InvertZOrderOfControls(Control.ControlCollection ControlList)
        {
            // do not process empty control list
            if (ControlList.Count == 0)
                return;
            // only re-order if list is writable
            if (!ControlList.IsReadOnly)
            {
                SortedList<int, Control> sortedChildControls = new SortedList<int, Control>();
                // find all none docked controls and sort in to list
                foreach (Control ctrlChild in ControlList)
                {
                    if (ctrlChild.Dock == DockStyle.None)
                        sortedChildControls.Add(ControlList.GetChildIndex(ctrlChild), ctrlChild);
                }
                // re-order the controls in the parent by swapping z-order of first 
                // and last controls in the list and moving towards the center
                for (int i = 0; i < sortedChildControls.Count / 2; i++)
                {
                    Control ctrlChild1 = sortedChildControls.Values[i];
                    Control ctrlChild2 = sortedChildControls.Values[sortedChildControls.Count - 1 - i];
                    int zOrder1 = ControlList.GetChildIndex(ctrlChild1);
                    int zOrder2 = ControlList.GetChildIndex(ctrlChild2);
                    ControlList.SetChildIndex(ctrlChild1, zOrder2);
                    ControlList.SetChildIndex(ctrlChild2, zOrder1);
                }
            }
            // try to invert the z-order of child controls
            foreach (Control ctrlChild in ControlList)
            {
                try { InvertZOrderOfControls(ctrlChild.Controls); }
                catch { }
            }
        }

        public static Bitmap sPanelToImage(Control APanel, bool AbInvert, bool AbToClipBoard, UInt16 AExtraWidth, UInt16 AExtraHeight )
        {
            Bitmap bitmap = null;
//            PixelFormat pixelFormat = PixelFormat.Format16bppArgb1555;


            try
            {
                bitmap = new Bitmap(APanel.Width + AExtraWidth, APanel.Height + AExtraHeight);//, pixelFormat);

                try
                {
                    using (Graphics G = Graphics.FromImage(bitmap))
                    {
                        G.Clear(Color.White);           // needed for init
                        G.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Panel to Image graphics clear error", ex);
                }
  
                if (AbInvert) InvertZOrderOfControls(APanel.Controls);
                APanel.DrawToBitmap(bitmap, new System.Drawing.Rectangle(0, 0, APanel.Width, APanel.Height));
                if (AbInvert) InvertZOrderOfControls(APanel.Controls);

                if (AbToClipBoard)
                {
                    Clipboard.SetImage(bitmap);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Panel to Image error", ex);
                bitmap = null;
            }
            return bitmap;
        }
        public static Bitmap sPanelToImage(Control APanel, bool AbToClipBoard, bool AbWarn)
        {
            Bitmap bitmap = sPanelToImage(APanel, false, AbToClipBoard, 2, 2);

            if( bitmap == null )
            {
                string panelName = APanel == null ? "null" : APanel.Name;

                Application.DoEvents();
                CProgram.sMemoryCleanup(true, true);
                Application.DoEvents();

                bitmap = sPanelToImage(APanel, false, AbToClipBoard, 2, 2);

                if (bitmap == null && AbWarn)
                {
                    CProgram.sPromptError(true, "Print to pdf", "Bitmap capture error of panel " + panelName);
                }
            }
            return bitmap;
        }

        public static bool sbDeterminNrReports(out UInt16 ArNrReports, UInt32 AStudyIX)  // optional to call when an error occurse
        {
            UInt32 studyIX = AStudyIX;
            UInt16 lastReportNr = 0;
            bool bOk = false;

            try
            {
                if (studyIX == 0)
                {
                    CProgram.sLogError("No study present");
                }
                else
                {
                    CPdfDocRec _mPdfDocRec = new CPdfDocRec(CDvtmsData.sGetDBaseConnection());

                    List<CSqlDataTableRow> _mPdfDocList = null;

                    if (_mPdfDocRec != null)
                    {
                        UInt64 loadMask = _mPdfDocRec.mMaskValid;
                        UInt64 whereMask = CSqlDataTableRow.sGetMask((UInt16)DPdfDocRecVars.Study_IX);
                        bool bError;

                        _mPdfDocRec._mStudy_IX = studyIX;

                        bOk = _mPdfDocRec.mbDoSqlSelectList(out bError, out _mPdfDocList, loadMask, whereMask, true, "");   // just load for current study

                        if (bError == false)
                        {
                            uint count = 0;

                            foreach (CPdfDocRec row in _mPdfDocList)
                            {
                                ++count;
                                if (row._mStudySubNr > lastReportNr)
                                {
                                    lastReportNr = row._mStudySubNr;
                                }
                            }
                            if (count == lastReportNr)
                            {
                                CProgram.sLogLine("Nr Reports " + lastReportNr.ToString() + " same as count for study " + studyIX.ToString());
                            }
                            else if (count > lastReportNr)
                            {
                                CProgram.sLogLine("Nr Reports " + lastReportNr.ToString() + " ok, "
                                    + (count - lastReportNr).ToString() + " extra reports for study " + studyIX.ToString());
                            }
                            else
                            {
                                CProgram.sLogLine("Nr Reports " + lastReportNr.ToString() + " ok, "
                                    + (lastReportNr - count).ToString() + " missing reports for study " + studyIX.ToString());
                            }

                            if ( count > lastReportNr)
                            {
                                lastReportNr = count < UInt16.MaxValue ? (UInt16)count : UInt16.MaxValue;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Determin Nr reports failed for study " + studyIX.ToString(), ex);
            }
            ArNrReports = lastReportNr;
            return bOk;
        }

    }
}