﻿using EventBoard.Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event_Base
{
    public class CRecDeviceList
    {
        private static CItemKey _sSironaKeyFile = null;            // items from sirona.itemKey
        private static List<string> _sSironaLicenseList = null;    // active list from sirona.itemKey
        private static List<string> _sSironaDeviceList = null;  // active Sirona with device directories
        private static DateTime _sSironaLicenseReadDT = DateTime.MinValue; // last read and checked for directories
        private static UInt32 _sSironaCheckLicenseHour = 4;       // check every N hours the license file

        private static CItemKey _sTzKeyFile = null;                // items from tz.itemKey
        private static List<string> _sTzLicenseList = null;    // active list from 
        private static List<string> _sTzDeviceList = null;  // active TZ with device directories
        private static DateTime _sTzLicenseReadDT = DateTime.MinValue; // last read and checked for directories
        private static UInt32 _sTzCheckLicenseHour = 4;       // check every N hours the license file

        private static CItemKey _sDV2KeyFile = null;                // items from dv2.itemKey
        private static List<string> _sDV2LicenseList = null;    // active list from 
        private static List<string> _sDV2DeviceList = null;  // active Sirona with device directories
        private static DateTime _sDV2LicenseReadDT = DateTime.MinValue; // last read and checked for directories
        private static UInt32 _sDV2CheckLicenseHour = 4;       // check every N hours the license file

        private static CItemKey _sDVXKeyFile = null;                // items from dvx.itemKey
        private static List<string> _sDVXLicenseList = null;    // active list from 
        private static List<string> _sDVXDeviceList = null;  // active Sirona with device directories
        private static DateTime _sDVXLicenseReadDT = DateTime.MinValue; // last read and checked for directories
        private static UInt32 _sDVXCheckLicenseHour = 4;       // check every N hours the license file

        private static List<string> _sBlackList = null;

        public static void sDeviceListClear()
        {
            _sSironaKeyFile = null;
            _sSironaLicenseList = null;    // active list from 
            _sSironaDeviceList = null;  // active Sirona with device directories
            _sSironaLicenseReadDT = DateTime.MinValue; // last read and checked for directories

            _sTzKeyFile = null;
            _sTzLicenseList = null;    // active list from 
            _sTzDeviceList = null;  // active Sirona with device directories
            _sTzLicenseReadDT = DateTime.MinValue; // last read and checked for directories

            _sDV2KeyFile = null;
            _sDV2LicenseList = null;    // active list from 
            _sDV2DeviceList = null;  // active Sirona with device directories
            _sDV2LicenseReadDT = DateTime.MinValue; // last read and checked for directories

            _sDVXKeyFile = null;
            _sDVXLicenseList = null;    // active list from 
            _sDVXDeviceList = null;  // active Sirona with device directories
            _sDVXLicenseReadDT = DateTime.MinValue; // last read and checked for directories

            CProgram.sLogLine("Device lists cleared");
        }

        public static UInt32 sGetActiveDeviceList(out string ArStringText, List<String> ADeviceList, string AMergeChar)
        {
            UInt32 n = 0;
            string devList = "";
            string mergeStr = AMergeChar == null || AMergeChar.Length == 0 ? "+" : AMergeChar;

            if (ADeviceList != null)
            {
                bool bNext = false;

                foreach (string s in ADeviceList)
                {
                    if (sbNotInBlackList(s))
                    {
                        if (bNext)
                        {
                            devList += mergeStr;
                        }
                        devList += s;
                        ++n;
                        bNext = true;
                    }
                }
            }
            ArStringText = devList;
            return n;
        }




        /* Sirona
         * */

        public static Int32 sGetSironaDeviceCount()
        {
            return _sSironaLicenseList == null ? 0 : _sSironaLicenseList.Count;
        }
        public static List<string> sGetSironaLicenseList()
        {
            return _sSironaLicenseList;
        }
        public static List<string> sGetSironaDeviceList()
        {
            return _sSironaDeviceList;
        }
        public static bool sbSironaIsInLicenseList(string ASnr, bool AbAskAdd)
        {
            bool bFound = false;
            if (_sSironaLicenseList != null && ASnr != null && ASnr.Length > 0)
            {
                string sirona = ASnr.Trim().ToUpper();

                foreach (string s in _sSironaLicenseList)
                {
                    if (s.ToUpper() == sirona)
                    {
                        bFound = true;
                        break;
                    }
                }
                if (bFound == false)
                {
                    CProgram.sLogWarning(">>Missing device in Sirona list: " + ASnr);

                    if (CLicKeyDev.sbDeviceIsProgrammer()
                        && AbAskAdd
                        && CProgram.sbAskYesNo("Test Sirona in License List", "Add device to loaded list: " + ASnr))
                    {
                        _sSironaLicenseList.Add(ASnr);
                        bFound = true;
                    }
                }
            }
            return bFound;
        }
        public static bool mbSironaIsInDeviceList(string ASnr, bool AbAskAdd)
        {
            bool bFound = false;

            if (_sSironaDeviceList != null && ASnr != null && ASnr.Length > 0)
            {
                string sirona = ASnr.Trim().ToUpper();

                foreach (string s in _sSironaDeviceList)
                {
                    if (s.ToUpper() == sirona)
                    {
                        bFound = true;
                        break;
                    }
                }

                if (bFound == false)
                {
                    CProgram.sLogWarning(">>Missing device in Sirona list: " + ASnr);

                    if (CLicKeyDev.sbDeviceIsProgrammer()
                    && AbAskAdd
                    && CProgram.sbAskYesNo("Test Sirona in Device List", "Add device: " + ASnr))
                    {
                        _sSironaDeviceList.Add(ASnr);
                        bFound = true;
                    }
                }
            }
            return bFound;
        }

        public static UInt32 sGetActiveSironaDeviceList(out string ArStringText, string AMergeChar)
        {
            return sGetActiveDeviceList(out ArStringText, _sSironaDeviceList, AMergeChar);
        }

        /* TZ
        */
        public static bool sbTzIsInLicenseList(string ASnr, bool AbAskAdd)
        {
            bool bFound = false;

            if (_sTzLicenseList != null && ASnr != null && ASnr.Length > 0)
            {
                string tz = ASnr.Trim().ToUpper();

                foreach (string s in _sTzLicenseList)
                {
                    if (s.ToUpper() == tz)
                    {
                        bFound = true;
                        break;
                    }
                }
                if (bFound == false && CLicKeyDev.sbDeviceIsProgrammer())
                {
                    if (AbAskAdd && CProgram.sbAskYesNo("Test Tz in License List", "Add device: " + ASnr))
                    {
                        _sTzLicenseList.Add(ASnr);
                        bFound = true;
                    }
                }

            }
            return bFound;
        }

        public static Int32 sGetTzDeviceCount()
        {
            return _sTzLicenseList == null ? 0 : _sTzLicenseList.Count;
        }
        public static List<string> sGetTzLicenseList()
        {
            return _sTzLicenseList;
        }
        public static List<string> sGetTzDeviceList()
        {
            return _sTzDeviceList;
        }
        public static bool sbTzSnrIsInDeviceList(string ASnr, bool AbAskAdd)
        {
            bool bFound = false;

            if (_sTzDeviceList != null && ASnr != null && ASnr.Length > 0)
            {
                string tz = ASnr.Trim().ToUpper();// CDvtmsData.sGetTzStoreSnrName(ASnr.Trim()).ToUpper();

                foreach (string s in _sTzDeviceList)
                {
                    if (s.ToUpper() == tz)
                    {
                        bFound = true;
                        break;
                    }
                }
                if (bFound == false)
                {
                    CProgram.sLogWarning(">>Missing device in TZ list: " + ASnr);

                    if (CLicKeyDev.sbDeviceIsProgrammer()
                    && AbAskAdd
                    && CProgram.sbAskYesNo("Test TZ in Device List", "Add device: " + ASnr))
                    {
                        _sTzDeviceList.Add(ASnr);
                        bFound = true;
                    }
                }
            }
            return bFound;
        }
        public static UInt32 sGetActiveTzDeviceList(out string ArStringText, string AMergeChar)
        {
            return sGetActiveDeviceList(out ArStringText, _sTzDeviceList, AMergeChar);
        }
        /* DV2
         */

        public static Int32 sGetDV2DeviceCount()
        {
            return _sDV2LicenseList == null ? 0 : _sDV2LicenseList.Count;
        }
        public static List<string> sGetDV2DeviceList()
        {
            return _sDV2LicenseList;
        }
        public static bool sbDV2IsInLicenseList(string ASnr, bool AbAskAdd)
        {
            bool bFound = false;

            if (_sDV2LicenseList != null && ASnr != null && ASnr.Length > 0)
            {
                string dv2 = ASnr.Trim().ToUpper();

                foreach (string s in _sDV2LicenseList)
                {
                    if (s.ToUpper() == dv2)
                    {
                        bFound = true;
                        break;
                    }
                }
                if (bFound == false)
                {
                    CProgram.sLogWarning(">>Missing device in DVX list: " + ASnr);

                    if (CLicKeyDev.sbDeviceIsProgrammer()
                    && AbAskAdd
                    && CProgram.sbAskYesNo("Test DV2 in License List", "Add device: " + ASnr))
                    {
                        _sDV2LicenseList.Add(ASnr);
                        bFound = true;
                    }
                }
            }
            return bFound;
        }
        public static bool sbDV2IsInDeviceList(string ASnr, bool AbAskAdd)
        {
            bool bFound = false;

            if (_sDV2DeviceList != null && ASnr != null && ASnr.Length > 0)
            {
                string dv2 = ASnr.Trim().ToUpper();

                foreach (string s in _sDV2DeviceList)
                {
                    if (s.ToUpper() == dv2)
                    {
                        bFound = true;
                        break;
                    }
                }
                if (bFound == false)
                {
                    CProgram.sLogWarning(">>Missing device in DV2 list: " + ASnr);

                    if (CLicKeyDev.sbDeviceIsProgrammer()
                    && AbAskAdd
                    && CProgram.sbAskYesNo("Test DV2 in Device List", "Add device: " + ASnr))
                    {
                        _sDV2DeviceList.Add(ASnr);
                        bFound = true;
                    }
                }

            }
            return bFound;
        }
        public static UInt32 sGetActiveDV2DeviceList(out string ArStringText, string AMergeChar)
        {
            return sGetActiveDeviceList(out ArStringText, _sDV2DeviceList, AMergeChar);
        }

        /* DVX
         */

        public static Int32 sGetDVXDeviceCount()
        {
            return _sDVXLicenseList == null ? 0 : _sDVXLicenseList.Count;
        }
        public static List<string> sGetDVXDeviceList()
        {
            return _sDVXDeviceList;
        }
        public static List<string> sGetDvxLicenseList()
        {
            return _sDVXLicenseList;
        }

        public static bool sbDVXIsInLicenseList(string ASnr, bool AbAskAdd)
        {
            bool bFound = false;

            if (_sDVXLicenseList != null && ASnr != null && ASnr.Length > 0)
            {
                string dv2 = ASnr.Trim().ToUpper();

                foreach (string s in _sDVXLicenseList)
                {
                    if (s.ToUpper() == dv2)
                    {
                        bFound = true;
                        break;
                    }
                }
                if (bFound == false)
                {
                    CProgram.sLogWarning(">>Missing device in DVX list: " + ASnr);

                    if (CLicKeyDev.sbDeviceIsProgrammer()
                    && AbAskAdd
                    && CProgram.sbAskYesNo("Test DVX in License List", "Add device: " + ASnr))
                    {
                        _sDVXLicenseList.Add(ASnr);
                        bFound = true;
                    }
                }
            }
            return bFound;
        }
        public static bool sbDVXIsInDeviceList(string ASnr, bool AbAskAdd)
        {
            bool bFound = false;

            if (_sDVXDeviceList != null && ASnr != null && ASnr.Length > 0)
            {
                string dvx = ASnr.Trim().ToUpper();

                foreach (string s in _sDVXDeviceList)
                {
                    if (s.ToUpper() == dvx)
                    {
                        bFound = true;
                        break;
                    }
                }
                if (bFound == false)
                {
                    CProgram.sLogWarning(">>Missing device in DVX list: " + ASnr);

                    if (CLicKeyDev.sbDeviceIsProgrammer()
                    && AbAskAdd
                    && CProgram.sbAskYesNo("Test DVX in Device List", "Add device: " + ASnr))
                    {
                        _sDVXDeviceList.Add(ASnr);
                        bFound = true;
                    }
                }

            }
            return bFound;
        }
        public static UInt32 sGetActiveDVXDeviceList(out string ArStringText, string AMergeChar)
        {
            return sGetActiveDeviceList(out ArStringText, _sDVXDeviceList, AMergeChar);
        }



        /* load
         */
        public static bool sbLoadDeviceKeyFile(ref CItemKey ArKeyFile, ref List<string> ArLicenseList, string ADeviceDir, string ACollection, UInt64 ASeed, UInt16 AdaysAt)
        {
            bool bOk = false;
            try
            {
                DateTime dt = DateTime.Now;
                CItemKey newKey = new CItemKey(ACollection);
                string keyFile = Path.Combine(ADeviceDir, CItemKey.sMakeItemKeyFileName(ACollection));
                bool bCheckSum = false;
                UInt32 nDevices = 0;
                UInt32 nDevicesAt = 0;
                UInt32 day0 = 0;

                ArLicenseList = null;
                if (newKey != null)
                {
                    bool b = newKey.mbReadFromFile(out bCheckSum, keyFile, ASeed, CProgram.sGetOrganisationLabel(), true);

                    if (false == b)
                    {
                        CProgram.sLogError("Failed read device license keys " + keyFile);
                    }
                    else if (false == bCheckSum)
                    {
                        CProgram.sLogError("Failed read valid device license keys for " + ACollection);
                    }
                    else
                    {
                        b = newKey.mbGetParamUInt32(out day0, CItemKey._cParamDay0);

                        if (b)
                        {
                            ArKeyFile = newKey;
                        }
                        else
                        {
                            CProgram.sLogError("Failed read dated device license keys for " + ACollection);
                        }
                    }
                }
                if (ArKeyFile != null)
                {
                    ArLicenseList = ArKeyFile.mGetValidItems(out nDevicesAt, dt, AdaysAt, day0);

                    nDevices = ArLicenseList == null ? 0 : (UInt32)ArLicenseList.Count;
                }
                string s = "Read " + ACollection + " device lic in " + (DateTime.Now - dt).TotalSeconds.ToString("0.00") + " sec, " + nDevices + " active";
                if (nDevices == 0)
                {
                    s += "!!";
                }
                else if (nDevices != nDevicesAt)
                {
                    s += ", " + (nDevices - nDevicesAt).ToString() + " expires in " + AdaysAt.ToString();
                }
                CProgram.sLogLine(s);
                bOk = nDevices > 0;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Scan " + ACollection + " devices failed", ex);
            }

            return bOk;
        }
        /* Check
         */ 
        public static bool sbSironaDeviceListsCheck(string AServerStoreDir, bool AbLogUnits, bool AbForceReload)
        {
            DateTime dt = DateTime.Now;
            bool bPresent = _sSironaLicenseList != null && _sSironaDeviceList != null && (dt - _sSironaLicenseReadDT).TotalHours < _sSironaCheckLicenseHour;

            if (AbForceReload)
            {
                bPresent = false;
                _sDVXLicenseList = null;
                _sDVXDeviceList = null;
            }
            if (bPresent)
            {
                bPresent = _sSironaDeviceList.Count > 0;
            }
            else
            {
                string deviceDir;
                bool bCheckServer = false; // not all sirona have the folder in httpSir (e.g. Sirona FTP zip) AServerStoreDir != null && AServerStoreDir.Length > 3;

                if (CDvtmsData.sbGetDeviceTypeDir(out deviceDir, DDeviceType.Sirona))
                {
                    string collection = "Sirona";
                    UInt64 seed = 0x8393F6D3840A272B;

                    string logUnits = collection + "=";
                    try
                    {
                        List<string> oldDeviceList = _sSironaDeviceList;
                        _sSironaDeviceList = new List<string>();

                        if (sbLoadDeviceKeyFile(ref _sSironaKeyFile, ref _sSironaLicenseList, deviceDir, collection, seed, 14))
                        {
                            UInt32 nDevices = _sSironaLicenseList == null ? 0 : (UInt32)_sSironaLicenseList.Count;
                            int nDevice = 0;
                            int nTested = 0;
                            bool bExists;
                            string dirStr;

                            _sSironaLicenseReadDT = dt;

                            if (_sSironaLicenseList != null)
                            {
                                foreach (string snr in _sSironaLicenseList)
                                {
                                    bExists = false;

                                    try
                                    {
                                        if (oldDeviceList != null)
                                        {
                                            foreach (string oldDevice in oldDeviceList)
                                            {
                                                if (oldDevice == snr)
                                                {
                                                    bExists = true; // directories tested in previous list
                                                    break;
                                                }
                                            }
                                        }
                                        if (false == bExists)
                                        {
                                            dirStr = Path.Combine(deviceDir, snr);

                                            if ((false == bCheckServer
                                                 || Directory.Exists(Path.Combine(AServerStoreDir, "uploads"))
                                                 )
                                                && Directory.Exists(Path.Combine(dirStr, CSironaStateSettings._csToServerName))
                                            )
                                            {
                                                bExists = true;
                                                ++nTested;
                                            }
                                        }
                                        if (bExists)
                                        {
                                            _sSironaDeviceList.Add(snr);
                                            ++nDevice;
                                            logUnits += "+" + snr.ToUpper();
                                        }
                                        else
                                        {
                                            logUnits += "-" + snr.ToLower();
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        logUnits += "_?" + snr.ToLower() + "_";
                                    }
                                }
                            }
                            CProgram.sLogLine((AbForceReload ? "Reload " : "") + collection + " has " + nDevice.ToString() + " configurable devices "
                                + nTested.ToString() + " / " + nDevices.ToString() + " new tested in " + (DateTime.Now - dt).TotalSeconds.ToString("0.00") + " sec");
                            if (AbLogUnits)
                            {
                                CProgram.sLogInfo(logUnits);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Scan Sirona devices failed " + deviceDir, ex);
                    }
                    if (_sSironaDeviceList == null || _sSironaDeviceList.Count == 0)
                    {
                        CProgram.sLogLine("0 Sirona devices active! " + deviceDir);
                    }
                }
                bPresent = _sSironaDeviceList != null && _sSironaDeviceList.Count > 0;

            }
            return bPresent;
        }

        public static bool sbTzDeviceListsCheck(string AServerStoreDir, bool AbLogUnits, bool AbForceReload)
        {
            DateTime dt = DateTime.Now;
            bool bPresent = _sTzLicenseList != null && _sTzDeviceList != null && (dt - _sTzLicenseReadDT).TotalHours < _sTzCheckLicenseHour;

            if (AbForceReload)
            {
                bPresent = false;
                _sDVXLicenseList = null;
                _sDVXDeviceList = null;
            }
            if (bPresent)
            {
                bPresent = _sTzDeviceList.Count > 0;
            }
            else
            {
                string deviceDir;
                bool bCheckServer = AServerStoreDir != null && AServerStoreDir.Length > 3;

                if (CDvtmsData.sbGetDeviceTypeDir(out deviceDir, DDeviceType.TZ))
                {
                    string collection = "TZ";
                    UInt64 seed = 0x4A2277CE8090D3E4;

                    string logUnits = collection + "=";
                    try
                    {
                        List<string> oldDeviceList = _sTzDeviceList;
                        _sTzDeviceList = new List<string>();

                        if (sbLoadDeviceKeyFile(ref _sTzKeyFile, ref _sTzLicenseList, deviceDir, collection, seed, 14))
                        {
                            UInt32 nDevices = _sTzLicenseList == null ? 0 : (UInt32)_sTzLicenseList.Count;
                            int nDevice = 0;
                            int nTested = 0;
                            bool bExists;
                            string dirStr;
                            string storeSnr;

                            _sTzLicenseReadDT = dt;

                            if (_sTzLicenseList != null)
                            {
                                foreach (string snr in _sTzLicenseList)
                                {
                                    bExists = false;
                                    storeSnr = CDvtmsData.sGetTzStoreSnrName(snr);

                                    try
                                    {
                                        if (oldDeviceList != null)
                                        {
                                            foreach (string oldDevice in oldDeviceList)
                                            {
                                                if (oldDevice == storeSnr)
                                                {
                                                    bExists = true; // directories tested in previous list
                                                    break;
                                                }
                                            }
                                        }
                                        if (false == bExists)
                                        {
                                            dirStr = Path.Combine(deviceDir, storeSnr);

                                            if ((false == bCheckServer
                                                || ( Directory.Exists(Path.Combine(AServerStoreDir, storeSnr, "uploads"))
                                                    && Directory.Exists(Path.Combine(AServerStoreDir, storeSnr, "downloads"))
                                                    )
                                                )
                                                && Directory.Exists(Path.Combine(dirStr, "ToServer"))
                                            )
                                            {
                                                bExists = true;
                                                ++nTested;
                                            }
                                        }
                                        if (bExists)
                                        {
                                            _sTzDeviceList.Add(snr); // store given snr not storeSnr);
                                            logUnits += "+" + snr.ToUpper();
                                            ++nDevice;
                                        }
                                        else
                                        {
                                            logUnits += "-" + snr.ToLower();
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        logUnits += "_?" + snr.ToLower() + "_";
                                        // do not care if directory test fails
                                    }
                                }
                            }
                            CProgram.sLogLine((AbForceReload ? "Reload " : "") + collection + " has " + nDevice.ToString() + " configurable devices, "
                                + nTested.ToString() + " / " + nDevices.ToString() + " new tested in " + (DateTime.Now - dt).TotalSeconds.ToString("0.00") + " sec");
                            if (AbLogUnits)
                            {
                                CProgram.sLogInfo(logUnits);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Scan TZ devices failed " + deviceDir, ex);
                    }
                    if (_sTzDeviceList == null || _sTzDeviceList.Count == 0)
                    {
                        CProgram.sLogLine("0 TZ devices active! " + deviceDir);
                    }
                }
                bPresent = _sTzDeviceList != null && _sTzDeviceList.Count > 0;
            }
            return bPresent;
        }
        public static bool sbDV2DeviceListsCheck(string AServerStoreDir, bool AbLogUnits, bool AbForceReload)
        {
            DateTime dt = DateTime.Now;
            bool bPresent = _sDV2LicenseList != null && _sDV2DeviceList != null && (dt - _sDV2LicenseReadDT).TotalHours < _sDV2CheckLicenseHour;

            if (AbForceReload)
            {
                bPresent = false;
                _sDVXLicenseList = null;
                _sDVXDeviceList = null;
            }
            if (bPresent)
            {
                bPresent = _sDV2DeviceList.Count > 0;
            }
            else
            {
                string deviceDir;
                bool bCheckServer = AServerStoreDir != null && AServerStoreDir.Length > 3;

                if (CDvtmsData.sbGetDeviceTypeDir(out deviceDir, DDeviceType.DV2))
                {
                    string collection = "DV2";
                    UInt64 seed = 0xC653DE78AC890923;

                    string logUnits = collection + "=";
                    try
                    {
                        List<string> oldDeviceList = _sDV2DeviceList;
                        _sDV2DeviceList = new List<string>();

                        if (sbLoadDeviceKeyFile(ref _sDV2KeyFile, ref _sDV2LicenseList, deviceDir, collection, seed, 14))
                        {
                            UInt32 nDevices = _sDV2LicenseList == null ? 0 : (UInt32)_sDV2LicenseList.Count;
                            int nDevice = 0;
                            int nTested = 0;
                            bool bExists;
                            //                          string dirStr;
                            _sDV2LicenseReadDT = dt;

                            if (_sDV2LicenseList != null)
                            {
                                foreach (string snr in _sDV2LicenseList)
                                {
                                    bExists = false;

                                    try
                                    {
                                        if (oldDeviceList != null)
                                        {
                                            foreach (string oldDevice in oldDeviceList)
                                            {
                                                if (oldDevice == snr)
                                                {
                                                    bExists = true; // directories tested in previous list
                                                    break;
                                                }
                                            }
                                        }
                                        if (false == bExists)
                                        {
                                            //                                           

                                            if ((false == bCheckServer
                                                 || Directory.Exists(Path.Combine(AServerStoreDir, snr))
                                                )
                                                && Directory.Exists(Path.Combine(deviceDir, snr))
                                            )
                                            {
                                                bExists = true;
                                                ++nTested;
                                            }
                                        }
                                        if (bExists)
                                        {
                                            _sDV2DeviceList.Add(snr);
                                            ++nDevice;
                                            logUnits += "+" + snr.ToUpper();
                                        }
                                        else
                                        {
                                            logUnits += "-" + snr.ToLower();
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        logUnits += "_?" + snr.ToLower() + "_";
                                    }
                                }
                            }
                            CProgram.sLogLine((AbForceReload ? "Reload " : "") + collection + " has " + nDevice.ToString() + " configurable devices, "
                                + nTested.ToString() + " / " + nDevices.ToString() + " tested in " + (DateTime.Now - dt).TotalSeconds.ToString("0.00") + " sec");
                            if (AbLogUnits)
                            {
                                CProgram.sLogInfo(logUnits);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Scan DV2 devices failed " + deviceDir, ex);
                    }
                    if (_sDV2DeviceList == null || _sDV2DeviceList.Count == 0)
                    {
                        CProgram.sLogLine("0 DV2 devices active! " + deviceDir);
                    }
                }
                bPresent = _sDV2DeviceList != null && _sDV2DeviceList.Count > 0;
            }
            return bPresent;
        }
        public static bool sbDVXDeviceListsCheck(bool AbLogUnits, bool AbForceReload)
        {
            DateTime dt = DateTime.Now;
            bool bPresent = _sDVXLicenseList != null && _sDVXDeviceList != null && (dt - _sDVXLicenseReadDT).TotalHours < _sDVXCheckLicenseHour;

            if(AbForceReload)
            {
                bPresent = false;
                _sDVXLicenseList = null;
                _sDVXDeviceList = null;
            }
            if (bPresent)
            {
                bPresent = _sDVXDeviceList.Count > 0;
            }
            else
            {
                string deviceDir;

                if (CDvtmsData.sbGetDeviceTypeDir(out deviceDir, DDeviceType.DVX))
                {
                    string collection = "DVX";
                    UInt64 seed = 0xA9B487C4523E9102;

                    string logUnits = collection + "=";
                    try
                    {
                        List<string> oldDeviceList = _sDV2DeviceList;
                        _sDVXDeviceList = new List<string>();

                        if (sbLoadDeviceKeyFile(ref _sDVXKeyFile, ref _sDVXLicenseList, deviceDir, collection, seed, 14))
                        {
                            UInt32 nDevices = _sDVXLicenseList == null ? 0 : (UInt32)_sDVXLicenseList.Count;
                            int nDevice = 0;
                            int nTested = 0;
                            bool bExists;
                            //                          string dirStr;
                            _sDVXLicenseReadDT = dt;

                            if (_sDVXLicenseList != null)
                            {
                                foreach (string snr in _sDVXLicenseList)
                                {
                                    bExists = false;

                                    try
                                    {
                                        if (oldDeviceList != null)
                                        {
                                            foreach (string oldDevice in oldDeviceList)
                                            {
                                                if (oldDevice == snr)
                                                {
                                                    bExists = true; // directories tested in previous list
                                                    break;
                                                }
                                            }
                                        }
                                        if (false == bExists)
                                        {
                                            //                                           
                                            if ( Directory.Exists(Path.Combine(deviceDir, snr)))
                                            {
                                                bExists = true;
                                                ++nTested;
                                            }
                                        }
                                        if (true || bExists)    // device folder not yet needed
                                        {
                                            _sDVXDeviceList.Add(snr);
                                            ++nDevice;
                                            logUnits += "+" + snr.ToUpper();
                                        }
                                        else
                                        {
                                            logUnits += "-" + snr.ToLower();
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        logUnits += "_?" + snr.ToLower() + "_";
                                    }
                                }
                            }
                            CProgram.sLogLine((AbForceReload ? "Reload " : "" ) + collection + " has " + nDevice.ToString() + " configurable devices, "
                                + nTested.ToString() + " / " + nDevices.ToString() + " tested in " + (DateTime.Now - dt).TotalSeconds.ToString("0.00") + " sec");
                            if (AbLogUnits)
                            {
                                CProgram.sLogInfo(logUnits);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Scan DVX devices failed " + deviceDir, ex);
                    }
                    if (_sDVXDeviceList == null || _sDVXDeviceList.Count == 0)
                    {
                        CProgram.sLogLine("0 DVX devices active! " + deviceDir);
                    }
                }
                bPresent = _sDVXDeviceList != null && _sDVXDeviceList.Count > 0;
            }
            return bPresent;
        }



        public static bool sbAllDeviceListsCheck(bool AbLogUnits, bool AbForceReload)
        {
            bool bOk = sbDV2DeviceListsCheck(null, AbLogUnits, AbForceReload);

            bOk &= sbSironaDeviceListsCheck(null, AbLogUnits, AbForceReload);
            bOk &= sbTzDeviceListsCheck(null, AbLogUnits, AbForceReload);
            bOk &= sbDVXDeviceListsCheck( AbLogUnits, AbForceReload);

            return bOk;
        }
        public static DDeviceType sFindInLicenseLists(string ASnr, bool AbAskAdd)
        {
            DDeviceType type = DDeviceType.Unknown;

            if (type == DDeviceType.Unknown)
            {
                if (sbTzIsInLicenseList(ASnr, AbAskAdd))
                {
                    type = DDeviceType.TZ;
                }
            }
            if (type == DDeviceType.Unknown)
            {
                if (sbSironaIsInLicenseList(ASnr, AbAskAdd))
                {
                    type = DDeviceType.Sirona;
                }
            }
            if (type == DDeviceType.Unknown)
            {
                if (sbDV2IsInLicenseList(ASnr, AbAskAdd))
                {
                    type = DDeviceType.DV2;
                }
            }
            if (type == DDeviceType.Unknown)
            {
                if (sbDVXIsInLicenseList(ASnr, AbAskAdd))
                {
                    type = DDeviceType.DVX;
                }
            }
            if (type == DDeviceType.Unknown)
            {
                string altName = CDvtmsData.sGetTzAltSnrName(ASnr);
                if (altName != null && altName.Length > 0 && sbTzIsInLicenseList(altName, AbAskAdd))
                {
                    type = DDeviceType.TZ;
                }
            }
            return type;
        }
        public static string sGetAllDeviceCount()
        {
            string s = "";
            Int32 n = sGetTzDeviceCount();
            Int32 nTot = n;

            if (n > 0)
            {
                s += "+" + n.ToString() + "TZ";
            }

            n = sGetSironaDeviceCount();

            if (n > 0)
            {
                s += "+" + n.ToString() + "Sir";
                nTot += n;
            }

            n = sGetDV2DeviceCount();
            if (n > 0)
            {
                s += "+" + n.ToString() + "DV2";
                nTot += n;
            }
            n = sGetDVXDeviceCount();
            if (n > 0)
            {
                s += "+" + n.ToString() + "DVX";
                nTot += n;
            }
            s += " =" + nTot.ToString() + " devices";
            return s;
        }
        public static UInt32 sGetBlackListCount()
        {
            return _sBlackList == null ? 0 : (UInt32)_sBlackList.Count;
        }
        public static UInt32 sSetBlackList(String ABlackListLines)
        {
            _sBlackList = new List<string>();

            if (_sBlackList != null && ABlackListLines != null && ABlackListLines.Length > 0)
            {
                string log = "BL=";
                string[] lines = ABlackListLines.Split();
                int n = lines != null ? lines.Length : 0;

                for (int i = 0; i < n; ++i)
                {
                    string line = lines[i].Trim();
                    if (line != null && line.Length > 0)
                    {
                        _sBlackList.Add(line.ToUpper());
                        log += line + ",";
                    }
                }
                CProgram.sLogLine(_sBlackList.Count > 0 ? log : "BL= cleared");
            }
            return _sBlackList == null ? 0 : (UInt32)_sBlackList.Count;
        }
        public static string sGetBlackList()
        {
            string lines = "";

            if (_sBlackList != null)
            {
                foreach (string snr in _sBlackList)
                {
                    lines += snr + "\r\n";
                }
            }
            return lines;
        }
        public static UInt32 sEditBlackList()
        {
            UInt32 n = 0;
            string lines = sGetBlackList();

            if (ReqTextForm.sbReqText("Edit Device Black List (blocking progress, close quickly! Old BL files are deleted!)", "Black List", ref lines))
            {
                n = sSetBlackList(lines);
            }
            return n;
        }

        public static bool sbNotInBlackList(string ASnr)
        {
            bool bNotFound = true;

            if (_sBlackList != null && ASnr != null && ASnr.Length > 0)
            {
                string search = ASnr.ToUpper();

                foreach (string snr in _sBlackList)
                {
                    if (snr == search)
                    {
                        bNotFound = false;
                        break;
                    }
                }
            }
            return bNotFound;
        }
    }
}
