﻿using Ionic.Zip;
using Program_Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Program_Base
{
    class CCallPreAnalysis
    {
    }

    public class CCardioLogsAPI
    {
        public const bool _cbEnableCardiologs = false;  // not paid

        private String _mApiUrl;
        private String _mApiToken;
        private UInt16 _mUploadTimeOutSec;
        private UInt16 _mUploadMaxLoop;
        private UInt16 _mGetResultsTimeOutSec;
        private UInt16 _mGetResultsMaxLoop;
        private UInt32 _mRecordIX;

        private UInt16 _mUploadNrTries;
        private double _mUploadDurationSec;
        private UInt16 _mGetResultsNrTries;
        private double _mGetResultsDurationSec;

        private string _mEcgID;

        private Int32 _mUploadSleepTcks = 1000;
        private Int32 _mGetResultsSleepTcks = 1000;
        private Int32 _mWaitBetweenSleepTcks = 1000;

        public CCardioLogsAPI(String AApiURL, String AApiToken, UInt16 AUploadTimeOutSec, UInt16 AUploadMaxLoop, UInt16 AGetResultsTimeOutSec, UInt16 AGetResultMaxLoop)
        {
            _mApiUrl = AApiURL;
            _mApiToken = AApiToken;
            _mUploadTimeOutSec = AUploadTimeOutSec;
            _mUploadMaxLoop = AUploadMaxLoop;
            _mGetResultsTimeOutSec = AGetResultsTimeOutSec;
            _mGetResultsMaxLoop = AGetResultMaxLoop;

            _mRecordIX = 0;
            _mUploadNrTries = 0;
            _mUploadDurationSec = 0;
            _mGetResultsNrTries = 0;
            _mGetResultsDurationSec = 0;
        }

        public Int32 mCalcSleep( UInt16 AIteration, Int32 AMinSleep, UInt16 AMaxLoop, UInt16 ATimeoutSec)
        {
            Int32 sleepTck = AMinSleep;

            if( AIteration > 2 && AMaxLoop > 0)
            {
                int tck = ATimeoutSec * 1000 / ( AMaxLoop+1 );

                if( tck > 10000)
                {
                    tck = 10000;
                }
                if( tck > AMinSleep)
                {
                    sleepTck = tck;
                }
            }
            return sleepTck;
        }
        public Int32 mCalcSleepUpload(UInt16 AIteration)
        {
            return mCalcSleep(AIteration, _mUploadSleepTcks, _mUploadMaxLoop, _mUploadTimeOutSec);
        }
        public Int32 mCalcSleepGetResults(UInt16 AIteration)
        {
            return mCalcSleep(AIteration, _mGetResultsSleepTcks, _mGetResultsMaxLoop, _mGetResultsTimeOutSec);
        }

        public static bool sbParseResultToString(out string ArValue, string ATotalResult, string AParamName)
        {
            string value = "";
            int len = ATotalResult == null ? 0 : ATotalResult.Length;
            bool bOk = len > 1;
            string valueChars = "0123456789.,eE+-";

            if (bOk)       // "{\"ecg_id\":\"AjoNk0wa\"}"
            {
                string search = "\"" + AParamName + "\":";
                int pos = ATotalResult.IndexOf(search);

                bOk = false;
                if (pos >= 0)
                {
                    pos += search.Length;

                    //                    while (pos < len && ATotalResult[pos] != ':') ++pos;    // search
                    while (pos < len && ATotalResult[pos] == ' ') ++pos;    // search next char
                    if (pos < len)
                    {
                        char c = ATotalResult[pos];

                        if (c == '\"')
                        {
                            while (++pos < len)
                            {
                                c = ATotalResult[pos];
                                if (c == '\"')
                                {
                                    bOk = true;
                                    break;
                                }
                                value += c;
                            }
                        }
                        else if (c == '{')
                        {
                            while (++pos < len)
                            {
                                c = ATotalResult[pos];
                                if (c == '}')
                                {
                                    bOk = true;
                                    break;
                                }
                                value += c;
                            }
                        }
                        else if (c == '[')
                        {
                            while (++pos < len)
                            {
                                c = ATotalResult[pos];
                                if (c == ']')
                                {
                                    bOk = true;
                                    break;
                                }
                                value += c;
                            }
                        }
                        else if (valueChars.IndexOf(c) >= 0)
                        {
                            value += c;
                            while (++pos < len)
                            {
                                c = ATotalResult[pos];

                                if (valueChars.IndexOf(c) < 0)
                                {
                                    break;
                                }
                                value += c;
                            }
                            int n = value.Length;
                            if( n > 0 )
                            {
                                bOk = true;
                                --n;
                                c = value[n];
                                if( c == ',')
                                {
                                    value = value.Substring(0, n);  // remove last ',' seperator
                                }
                            }
                        }
                    }
                }
            }
            ArValue = value;
            return bOk;
        }

        public static bool sbParseResultToInt32(ref Int32 ArValue, string ATotalResult, string AParamName)
        {
            string strValue = "";
            bool bOk = sbParseResultToString(out strValue, ATotalResult, AParamName);

            if (bOk)
            {
                bOk = Int32.TryParse(strValue, out ArValue);
            }
            return bOk;
        }

        public static bool sbParseResultToDouble(ref double ArValue, string ATotalResult, string AParamName)
        {
            string strValue = "";
            bool bOk = sbParseResultToString(out strValue, ATotalResult, AParamName);

            if (bOk)
            {
                bOk = CProgram.sbParseDouble(strValue, ref ArValue);
            }
            return bOk;
        }

        public static string sParseResultToString( string ATotalResult, string AParamName, string ADefaultValue = "")
        {
            string result = ADefaultValue;
            string value;

            if( sbParseResultToString(out value, ATotalResult, AParamName))
            {
                result = value;
            }
            return result;
        }

        public static Int32 sParseResultToInt32( string ATotalResult, string AParamName, Int32 ADefaultValue = 0)
        {
            Int32 result = ADefaultValue;
            string strValue = "";

            if( sbParseResultToString(out strValue, ATotalResult, AParamName))
            {
                int value = 0;

                if( Int32.TryParse(strValue, out value))
                {
                    result = value;
                }
            }
            return result;
        }
        public static double sParseResultToDouble(string ATotalResult, string AParamName, double ADefaultValue = 0)
        {
            double result = ADefaultValue;
            string strValue = "";

            if (sbParseResultToString(out strValue, ATotalResult, AParamName))
            {
                double value = 0;

                if (CProgram.sbParseDouble(strValue, ref value))
                {
                    result = value;
                }
            }
            return result;
        }

        private bool mbUploadFile(out String ArResultText, out String ArUploadEcgID, String AFilePath, UInt32 ARecordIX, bool AbLogOk, bool AbLogLoop)
        {
            bool bOk = false;
            string result = "";
            string responsebody = "";
            string lastErrorLine = "";
            DateTime startUTC = DateTime.UtcNow;
            string name = Path.GetFileName(AFilePath);

            _mEcgID = "";
            _mRecordIX = ARecordIX;
            _mUploadNrTries = 1;
            _mUploadDurationSec = 0;

            if (_cbEnableCardiologs == false)
            {
                result = "Not implemented";
            }
            else
            {
                do
                {
                    try
                    {
                        responsebody = "";

                        using (WebClient client = new WebClient())
                        {
                            string authorization = _mApiToken + ":";
                            byte[] binaryAuthorization = Encoding.UTF8.GetBytes(authorization);
                            authorization = Convert.ToBase64String(binaryAuthorization);
                            client.Headers.Add("Authorization", "Basic " + authorization);
                            try
                            {
                                //            string url = "https://api.app.cardiologs.com/v1/ecg";

                                byte[] responsebytes = client.UploadFile(_mApiUrl, AFilePath);
                                responsebody = Encoding.UTF8.GetString(responsebytes);

                                if (AbLogLoop && AbLogOk)
                                {
                                    CProgram.sLogLine("CardioLogs R" + ARecordIX + "-" + _mUploadNrTries + " uploadFile " + name + ": " + responsebody);
                                }
                                bOk = sbParseResultToString(out _mEcgID, responsebody, "ecg_id"); // "{\"ecg_id\":\"AjoNk0wa\"}"

                                if (bOk)
                                {
                                    result = "OK";
                                }
                                else
                                {
                                    result = "No ecg_id";
                                }
                            }
                            catch (Exception ex)
                            {
                                lastErrorLine = CProgram.sGetFirstLine(ex.Message);
                                int pos = lastErrorLine == null ? 0 : lastErrorLine.IndexOf(':');
                                if (pos > 0)
                                {
                                    lastErrorLine = lastErrorLine.Substring(pos + 1);
                                }
                                if (responsebody != null && responsebody.Length > 0)
                                {
                                    lastErrorLine += "&" + responsebody;
                                }
                                CProgram.sLogError("CardioLogs " + _mUploadNrTries + "R " + ARecordIX + " " + name + ":" + lastErrorLine);

                                //                            CProgram.sLogException("CardioLogs inner uploadFile " + AFilePath, ex);
                                result = "Failed inner exception";
                                if (_mUploadNrTries > 1)
                                {
                                    bOk = false;
                                }
                                bOk = false;
                                /*                        while (ex != null)
                                                        {
                                                            Console.WriteLine(ex.Message);
                                                            ex = ex.InnerException;
                                                        }
                                */
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("CardioLogs uploadFile " + AFilePath, ex);
                        result = "Failed exception";
                        bOk = false;
                    }
                    _mUploadDurationSec = (DateTime.UtcNow - startUTC).TotalSeconds;
                    if (bOk == false)
                    {
                        if (_mUploadNrTries > _mUploadMaxLoop)
                        {
                            result = "TimeOut loop";
                            break;
                        }
                        if (_mUploadDurationSec > _mUploadTimeOutSec)
                        {
                            result = "TimeOut time";
                            break;
                        }
                        Thread.Sleep(mCalcSleepUpload(_mUploadNrTries));
                        ++_mUploadNrTries;
                    }
                } while (false == bOk) ;

                if (bOk)
                {
                    if (AbLogOk)
                    {
                        CProgram.sLogLine("CardioLogs R" + ARecordIX + "-" + _mUploadNrTries + " uploadFile " + name + " in " + _mUploadDurationSec.ToString("0.000") + " sec, ecg_id= " + _mEcgID);
                    }
                }
                else
                {
                    CProgram.sLogError("CardioLogs R" + ARecordIX + "-" + _mUploadNrTries + " uploadFile " + AFilePath);
                    CProgram.sLogError("Cardiologs Upload failed in " + _mUploadDurationSec.ToString("0.000") + " sec, result: " + result);
                }
            }
            ArResultText = result;
            ArUploadEcgID = _mEcgID;
            return bOk;
        }
        private bool mbGetAnalysisResults(out String ArResultText, out String ArAnalysisJson, String AEcgID, bool AbLogOk, bool AbLogLoop)
        {
            bool bOk = false;
            string result = "";
            string analysisJson = "";
            string responsebody = "";
            string lastErrorLine = "";
            DateTime startUTC = DateTime.UtcNow;

            _mGetResultsNrTries = 1;
            _mGetResultsDurationSec = 0;

            if (_cbEnableCardiologs == false)
            {
                result = "Not implemented";
            }
            else
            {
                do
                {
                    try
                    {
                        responsebody = "";

                        using (WebClient client = new WebClient())
                        {
                            string authorization = _mApiToken + ":";
                            byte[] binaryAuthorization = Encoding.UTF8.GetBytes(authorization);
                            authorization = Convert.ToBase64String(binaryAuthorization);
                            client.Headers.Add("Authorization", "Basic " + authorization);

                            try
                            {
                                //                            string url = "https://api.app.cardiologs.com/v1/ecg/AjoNk0wa/event_metrics";
                                string url = _mApiUrl + "/" + AEcgID + "/event_metrics";

                                byte[] responsebytes = client.DownloadData(url);
                                responsebody = Encoding.UTF8.GetString(responsebytes);

                                if (AbLogLoop)
                                {
                                    CProgram.sLogLine("CardioLogs R" + _mRecordIX + "-" + _mGetResultsNrTries + " ecgID " + AEcgID + "\r\n" + responsebody);
                                }
                                analysisJson = responsebody;
                                bOk = analysisJson != null && analysisJson.Length > 2;
                                if (bOk)
                                {
                                    string abnormalities = "";

                                    bOk = sbParseResultToString(out abnormalities, analysisJson, "abnormalities"); // "abnormalities": ["pause", "av_block", "other_svt"],
                                    if (bOk)
                                    {
                                        if (false == mbCreateStatusLine(out result, analysisJson))
                                        {
                                            result = "??" + result;
                                        }
                                    }
                                    else
                                    {
                                        string status;

                                        if (sbParseResultToString(out status, responsebody, "status"))
                                        {
                                            result = "status=" + status;

                                            if (status.ToLower() == "processing" && _mGetResultsNrTries > 2)
                                            {
                                                _mGetResultsTimeOutSec += (UInt16)(2 + _mGetResultsTimeOutSec / _mGetResultsMaxLoop);
                                            }
                                        }
                                        else
                                        {
                                            result = "unknown status=" + responsebody;
                                        }
                                        CProgram.sLogLine("CardioLogs " + _mGetResultsNrTries + " call ecgID = " + AEcgID + ": " + result);
                                    }
                                }
                                else
                                {
                                    result = "Empty result";
                                }
                            }
                            catch (Exception ex)
                            {
                                lastErrorLine = CProgram.sGetFirstLine(ex.Message);
                                int pos = lastErrorLine == null ? 0 : lastErrorLine.IndexOf(':');
                                if (pos > 0)
                                {
                                    lastErrorLine = lastErrorLine.Substring(pos + 1);
                                }
                                if (responsebody != null && responsebody.Length > 0)
                                {
                                    lastErrorLine += "&" + responsebody;
                                }
                                CProgram.sLogError("CardioLogs " + _mGetResultsNrTries + " call ecgID = " + AEcgID + ":" + lastErrorLine);
                                //                            CProgram.sLogException("CardioLogs inner GetResults ecgID= " + AEcgID, ex);
                                result = "Failed inner exception";
                                if (_mGetResultsNrTries > 1)
                                {
                                    bOk = false;
                                }
                                bOk = false;

                                /*                        while (ex != null)
                                                        {
                                                            Console.WriteLine(ex.Message);
                                                            ex = ex.InnerException;
                                                        }
                                */
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("CardioLogs GetResults ecgID=" + AEcgID, ex);
                        result = "Failed exception";
                        bOk = false;
                    }
                    _mGetResultsDurationSec = (DateTime.UtcNow - startUTC).TotalSeconds;
                    if (bOk == false)
                    {
                        if (_mGetResultsNrTries >= _mGetResultsMaxLoop)
                        {
                            result = "TimeOut loop";
                            break;
                        }
                        if (_mGetResultsDurationSec >= _mGetResultsTimeOutSec)
                        {
                            result = "TimeOut time";
                            break;
                        }

                        Thread.Sleep(mCalcSleepGetResults(_mGetResultsNrTries));
                        ++_mGetResultsNrTries;
                    }
                } while (false == bOk);

                if (bOk)
                {
                    if (AbLogOk)
                    {
                        CProgram.sLogLine("CardioLogs R" + _mRecordIX + "-" + _mGetResultsNrTries + " ecgID= " + AEcgID + " succeded in " + _mGetResultsDurationSec.ToString("0.000") + " sec: " + result);
                    }
                }
                else
                {
                    CProgram.sLogError("CardioLogs R" + _mRecordIX + "-" + _mGetResultsNrTries + " ecgID= " + AEcgID + " failed in " + _mGetResultsDurationSec.ToString("0.000") + " sec, result: " + result);
                }
            }
            ArAnalysisJson = analysisJson;
            ArResultText = result;
            return bOk;
        }

        public bool mbCreateStatusLine(out string ArResultLine, string AJsonMetrics)
        {
            string resultLine = "";
            bool bOk = AJsonMetrics != null && AJsonMetrics.Length > 2;

            if( bOk )
            {
                /*    Example JSON event_metrics response:
{
    "duration": 40.25,
    "mean_hr": 77,
    "beat_count": 80,
    "pac_count": 220,
    "pvc_count": 103,
    "couplet_count": 0,
    "bigeminy_count": 32,
    "trigeminy_count": 0,
    "other_beat_count": 313,
    "noise_level": "little_or_no_noise",
    "abnormalities": ["pause", "av_block", "other_svt"],
    "noise": { 
        "burden": 0.05
    },
    "pause": {
        "count": 5,
        "longest_duration": 3.321
    },
    "av_block": {
        "type": "2nd_degree_II"
    },
    "afib": {
        "max_hr": null,
        "burden": 0
    },
    "vt": {
        "count": 0,
        "burden": 0,
        "longest_beats": null,
        "max_hr": null
    },
    "other_svt": {
        "count": 2,
        "burden": 13.43,
        "longest_duration": 5.1,
        "max_hr": 150
    }
}
                    */
                try// parse JSON event_metrics
                {
                    double duration = sParseResultToDouble(AJsonMetrics, "duration");
                    double mean_hr = sParseResultToDouble(AJsonMetrics, "mean_hr");
                    Int32 beat_count = sParseResultToInt32(AJsonMetrics, "beat_count");
                    Int32 pac_count = sParseResultToInt32(AJsonMetrics, "pac_count");
                    Int32 pvc_count = sParseResultToInt32(AJsonMetrics, "pvc_count");
                    Int32 couplet_count = sParseResultToInt32(AJsonMetrics, "couplet_count");
                    Int32 bigeminy_count = sParseResultToInt32(AJsonMetrics, "bigeminy_count");
                    Int32 trigeminy_count = sParseResultToInt32(AJsonMetrics, "trigeminy_count");
                    Int32 other_beat_count = sParseResultToInt32(AJsonMetrics, "other_beat_count");
                    string noiseLevel = sParseResultToString(AJsonMetrics, "noise_level");
                    string abnormalities = sParseResultToString(AJsonMetrics, "abnormalities");

                    string noiseBlock = sParseResultToString(AJsonMetrics, "noise");
                    string pauseBlock = sParseResultToString(AJsonMetrics, "pause");
                    string av_blockBlock = sParseResultToString(AJsonMetrics, "av_block");
                    string afibBlock = sParseResultToString(AJsonMetrics, "afib");
                    string vtBlock = sParseResultToString(AJsonMetrics, "vt");
                    string other_svtBlock = sParseResultToString(AJsonMetrics, "other_svt");


                    double noiseBurden = sParseResultToDouble(noiseBlock, "burden");


                    Int32 pauseCount = sParseResultToInt32(pauseBlock, "count");
                    double pauseLongestDuration = sParseResultToDouble(pauseBlock, "count");

                    string avBlockType = sParseResultToString(av_blockBlock, "type");

                    double afibMaxHr = sParseResultToDouble(afibBlock, "max_hr");
                    double afibBurden = sParseResultToDouble(afibBlock, "burden"); ;

                    Int32 vtCount = sParseResultToInt32(vtBlock, "count");
                    double vtBurden = sParseResultToDouble(vtBlock, "burden");
                    Int32 vtLongestBeats = sParseResultToInt32(vtBlock, "longest_beats");
                    double vtMaxHr = sParseResultToDouble(vtBlock, "max_hr");

                    Int32 other_svtCount = sParseResultToInt32(other_svtBlock, "count");
                    double other_svtBurden = sParseResultToDouble(other_svtBlock, "burden");
                    double other_svtLongestDuration = sParseResultToDouble(other_svtBlock, "longest_duration");
                    double other_svtMaxHr = sParseResultToDouble(other_svtBlock, "max_hr");

                    bool bPause = abnormalities.Contains("pause");
                    bool bAvBlock = abnormalities.Contains("av_block");
                    bool bAfib = abnormalities.Contains("afib");
                    bool bVt = abnormalities.Contains("\"vt\"");
                    bool bOther_svt = abnormalities.Contains("other_svt");

                    // combine into a result line
                    int meanBPM = (int)(mean_hr + 0.5);
                    resultLine += meanBPM + "";
                    int noisePer = (int)(noiseBurden + 0.5);
                    if (noiseLevel == "little_or_no_noise" || noiseLevel == "null")
                    {
                        resultLine += "+n" + noisePer;
                    }
                    else
                    {
                        resultLine += "+Noise" + noisePer;
                    }
                    if (bPause)
                    {
                        int sec = (Int32)(pauseLongestDuration + 0.5);

                        resultLine += "+Pause"+sec;
                    }
                    if (bAvBlock)
                    {
                        resultLine += "+AV";
                        if (avBlockType == "1st_degree")
                        {
                            resultLine += "1";
                        }
                        if (avBlockType == "2nd_degree_I")
                        {
                            resultLine += "2I";
                        }
                        if (avBlockType == "2nd_degree_II")
                        {
                            resultLine += "2II";
                        }
                        if (avBlockType == "high-grade")
                        {
                            resultLine += "h";
                        }
                        if (avBlockType == "complete")
                        {
                            resultLine += "c";
                        }
                    }
                    if (bAfib)
                    {
                        int burden = (int)(afibBurden + 0.5);

                        resultLine += "+AFib"+burden;
                    }
                    if (bVt)
                    {
                        int burden = (int)(vtBurden + 0.5);
                        resultLine += "+VT" + burden;
                    }
                    if (bOther_svt)
                    {
                        int burden = (int)(other_svtBurden + 0.5);

                        int bpm = (Int32)(other_svtMaxHr+0.5);
                        resultLine += "+SVT" + +burden; // +"@"+ bpm + "bpm";
                    }
                }
                catch ( Exception ex )
                {
                    CProgram.sLogException("failed parse json:" + AJsonMetrics, ex);
                    bOk = false;
                }
            }

            ArResultLine = resultLine;

            return bOk;
        }

        public static bool sbMakeTriageText(out string ArTriageText, string AStatusLine)
        {
            bool bOk = AStatusLine != null && AStatusLine.Length > 0;
            string triageText = "";

            if( bOk )
            {
                string[] lines = AStatusLine.Split('+');
                int nLines = lines == null ? 0 : lines.Length;

                if( nLines == 0)
                {
                    bOk = false;
                }
                else if( nLines >= 1 )
                {
                    string meanHR = lines[0];
                    string noise = "";

                    if (nLines >= 2)
                    {
                        string line = lines[1];

                        if (line != null && line.Length > 0)
                        {
                            line = line.ToLower();
                            if (line == "n0")
                            {
                                noise = "No noise";
                            }
                            else if ( line.StartsWith("noise"))
                            {
                                noise = "Significant Noise(" + line.Substring(5) + "%)";
                            }
                            else if( line.Length > 1 && line[0] == 'n' && Char.IsDigit(line[1]))
                            {
                                noise = "Low Noise(" + line.Substring(1) + "%)";
                            }
                        }

                        triageText = noise + " " + meanHR + " bpm";

                        if (nLines >= 3)
                        {
                            for (int i = 2; i < nLines; ++i)
                            {
                                line = lines[i];
                                if (line != null && line.Length > 0)
                                {
                                    line = line.ToLower();
                                    if (line == "") { }
                                    else if (line == "pause") { triageText += ", Pause" + line.Substring(5) + "s)"; }

                                    else if (line == "av1") { triageText += ", AV block(1st degree)"; }
                                    else if (line == "av2I") { triageText += ", AV block(2nd degree I)"; }
                                    else if (line == "av2II") { triageText += ", AV block(2nd degree II)"; }
                                    else if (line == "avh") { triageText += ", AV block()"; }
                                    else if (line == "avc") { triageText += ", AV block()"; }

                                    else if (line.StartsWith( "afib" )) { triageText += ", AFib("+line.Substring(4)+"%)"; }
                                    else if (line == "vt") { triageText += ", VT(" + line.Substring(2) + "%)"; }
                                    else if (line == "svt") { triageText += ", VT(" + line.Substring(2) + "%)"; }
                                }
                            }
                        }
                    }
                }
            }
            ArTriageText = triageText;
            return bOk;
        }


        private bool mbZipHea( string AToZipFilePath, string AHeaFilePath, string AStoreName)
        {
            bool bZipOk = false;

            if (_cbEnableCardiologs )
            {
                try
                {
                    if (File.Exists(AToZipFilePath))
                    {
                        File.Delete(AToZipFilePath);
                    }

                    string datFilePath = Path.ChangeExtension(AHeaFilePath, ".dat");

                    using (ZipFile newZip = new ZipFile())
                    {

                        newZip.AddFile(AHeaFilePath, "\\"); //name is also in file .FileName = AStoreName + ".hea";
                        newZip.AddFile(datFilePath, "\\");// .FileName = AStoreName + ".hea"; 

                        newZip.Save(AToZipFilePath);
                        bZipOk = true;
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Zip hea" + AHeaFilePath, ex);
                }
            }
            return bZipOk;
        }

        public bool mbWritePaRunFile(String ARecordPath, UInt32 ARecordIX, string AMethod, string ARefID)
        {
            bool bOk = false;

            string fileName = "PA_" + ARecordIX.ToString("000000") + ".run";

            try
            {
                string filePath = Path.Combine(ARecordPath, fileName);
                string s = AMethod + "\r\n" + ARefID;

                File.WriteAllText(filePath, s);
                bOk = true;
            }
            catch (Exception ex)
            {
                CProgram.sLogException("PA failed write RunFile R" + ARecordIX + " " + fileName, ex);
            }
            return bOk;

        }
        public bool mbReadPaRunFile(String ARecordPath, UInt32 ARecordIX, out string ArMethod, out string ArRefID)
        {
            bool bOk = false;
            string method = "";
            string refID  = "";

            string fileName = "PA_" + ARecordIX.ToString("000000") + ".run";

            try
            {
                string filePath = Path.Combine(ARecordPath, fileName);
                string[] lines = File.ReadAllLines(filePath);
                int n = lines == null ? 0 : lines.Length;

                if (n > 0)
                {
                    int iLine = 0;
                    method = lines[iLine++];
                    if( method == null || method.Length == 0 )
                    {
                        if( iLine < n ) method = lines[iLine++];
                    }
                    if (method != null && method.Length > 0 && iLine < n)
                    {
                        refID = lines[iLine++];
                        if (refID == null || refID.Length == 0)
                        {
                            if (iLine < n) refID = lines[iLine++];
                        }
                        if (refID != null && refID.Length > 0)
                        {
                            bOk = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("PA failed write RunFile R" + ARecordIX + " " + fileName, ex);
            }
            ArMethod = method;
            ArRefID = refID;
            return bOk;

        }

        public bool mbCallApi(out String ArResultText, out String ArUploadEcgID, out String ArAnalysisJson, String AFilePath, UInt32 ARecordIX, bool AbLogOk, bool AbLogLoop, bool AbDeleteTempZip)
        {
            bool bOk = false;
            string result = "";
            string ecgID = "";
            string analysisJson = "";

            if (_cbEnableCardiologs == false)
            {
                result = "Not implemented";
                CProgram.sLogLine(result);
            }
            else
            {
                bool bZipMade = false;
                string filePath = AFilePath;
                string path = Path.GetDirectoryName(AFilePath);
                string ext = Path.GetExtension(AFilePath);
                string name = Path.GetFileNameWithoutExtension(AFilePath);

                string rName = ARecordIX == 0 ? name : "R" + ARecordIX.ToString("000000");

                string paName = "PA_" + rName + "_Cardiologs" + CProgram.sDateTimeToYMDHMS(DateTime.Now);

                if (ext == null || ext.Length < 2)
                {
                    result = "Bad File Extention: " + ext;
                }
                else
                {
                    ext = ext.ToLower();

                    if (ext == ".hea")
                    {
                        string zipName = paName + ".zip";
                        filePath = Path.Combine(path, zipName);

                        bOk = mbZipHea(filePath, AFilePath, rName);
                        if (bOk)
                        {
                            bZipMade = bOk;
                        }
                        else
                        {
                            result = "Failed create zip " + zipName;
                        }
                    }
                    else
                    {
                        bOk = true;
                    }
                }
                if (bOk)
                {
                    bOk = mbUploadFile(out result, out ecgID, filePath, ARecordIX, AbLogOk, AbLogLoop);
                }
                if (bOk)
                {
                    if (_mWaitBetweenSleepTcks > 0)
                    {
                        Thread.Sleep(_mWaitBetweenSleepTcks);
                    }
                    bOk = mbGetAnalysisResults(out result, out ArAnalysisJson, ecgID, AbLogOk, AbLogLoop);

                    if (bOk)
                    {
                        try
                        {
                            string jsonName = paName + ".json";
                            string jsonFilePath = Path.Combine(path, jsonName);

                            File.WriteAllText(jsonFilePath, ArAnalysisJson);
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("failed write CardioLogs json file" + filePath, ex);
                        }
                    }
                }
                if (bOk && bZipMade && AbDeleteTempZip)
                {
                    try
                    {
                        File.Delete(filePath);
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("delete zip failed" + filePath, ex);
                    }
                }
            }
            ArResultText = result;
            ArUploadEcgID = ecgID;
            ArAnalysisJson = analysisJson;
            return bOk;
        }

    }
}
