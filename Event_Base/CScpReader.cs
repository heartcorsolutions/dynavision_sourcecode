﻿// CScpReader 
// by Simon Vlaar 
// Techmedic Development B.V
// created 26 August 2016
//
// Based on TZ example 
/******************************************************************************
 *       Copyright (c) 2011, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 *       scpStripper.cpp
 *          - This program locates every *.scp file within a specified
 *             directory, parses it, and then saves the raw data to a *.txt
 *             file with a name matching that of the *.scp file.
 * 
 *
 *
 *****************************************************************************/
#define SCP_PROGRAM
//#define SCP_AES                   // no encryption used in 
#define CHECK_CRC                   // Comment this line to disable CRC Checking (off in example)

using System;
using System.IO;
using System.Collections.Generic;
using DynaVisionGraph.Filter;

#if SCP_PROGRAM
using Program_Base;
#else
namespace System.Runtime.CompilerServices
{
    sealed class CallerMemberNameAttribute : Attribute { }
    sealed class CallerLineNumber : Attribute { };
    sealed class CallerFilePath : Attribute { };
}
#endif

namespace Event_Base
{
    public class CTzQuickFilter
    {
        public static List<Filter> sTzQuickFilters = null;

        /// <summary>
        /// Filters the value based on the selected filters.
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public static int sDoQuickFilter(int Value)
        {
            if (sTzQuickFilters != null)
            {
                foreach (Filter filter in sTzQuickFilters)
                {
                    Value = filter.Filt(Value);
                }
            }
            return Value;
        }
        public static void sDoResetFilter()
        {
            if (sTzQuickFilters != null)
            {
                foreach (Filter filter in sTzQuickFilters)
                {
                    filter.mReset();
                }
            }
        }
    }

    public class CScpReader
    {
        private bool mbFlipSignal = false; //true;
        private static bool mbLog = false;
        private const string SCP_ID_STRING = "SCPECG";
        private const int MIN_FILE_SIZE = (256);
        private const int MAX_FILE_SIZE = (256 * 1024 * 1024); //(256 * 1024);

        public float mSignalUnit = 0.0F;        // amplitude unit
        public float mSampleFrequency = 0.0F;
        public float mTimeUnit = 0.0F;
        public UInt16 mNrSignals = 0;
        public CSignalData[] mSignals = null;
        public UInt32 mNrSamples = 0;

        public string mPatientLastName = "";       // section 1 info
        public string mPatientFirstName = "";
        public string mPatientID = "";
        public string mDeviceID = "";
        public string mDeviceManufact = "";
        //        public DateTime mMeasureDT = DateTime.MinValue;
        public DateTime mMeasureUTC = DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);
        public bool mbReadDT = false;
        public Int16 mTimeZoneOffsetMin = 0;
        public bool mbReadTZ = false;

        // extra variables ScpDV
        public float mWeightKg = 0.0F;
        public float mHeightM = 0.0F;
        public UInt16 mAge = 0;
        public UInt16 mSex = 0;
        public UInt16 mRace = 0;
        public UInt32 mDateOfBirth = 0;
        public string mReferal = "";
        public string mAcquiringInstitution = "";
        public string mAnalyzingInstitution = "";
        public string mAcquiringDepartment = "";
        public string mAnalyzingDepartment = "";
        public string mReferringPhysician = "";
        public string mConfirmingPhysician = "";
        public string mTechnician = "";
        public string mRoom = "";
        public string mFreeText = "";

        public string mFileName = "";
        public UInt32 mFileTZSeqNr = 0;   // extract from file name <snr>_<seqNr>.scp or <snr>_<seqNr>_YYYYMMDDHHSS.scp
                                        // DV2 file name is different


        private static UInt16[] ccitt_crc16_table = new UInt16[] {
  0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
  0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
  0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
  0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
  0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
  0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
  0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
  0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
  0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
  0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
  0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
  0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
  0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
  0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
  0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
  0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
  0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
  0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
  0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
  0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
  0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
  0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
  0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
  0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
  0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
  0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
  0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
  0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
  0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
  0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
  0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
  0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};

        private static byte[] constKey = new byte[] {
  0xf5, 0xf2, 0x3c, 0x0c, 0x8e, 0x49, 0x3a, 0xfb,
  0x30, 0x1d, 0xf8, 0x62, 0xa0, 0x30, 0x45, 0xbe
};

        private static Byte[] constIV = new byte[] {
  0x79, 0x17, 0xae, 0x23, 0x46, 0xe7, 0x8e, 0x79,
  0xbf, 0x3d, 0xff, 0xa6, 0x2c, 0x05, 0x4b, 0xf9
};
        public static void mLogLine(string ALine,
                    [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            if (mbLog)
            {
#if SCP_PROGRAM
                CProgram.sLogLine(ALine, _AutoLN, _AutoFN, _AutoCN);
#else
                Console.WriteLine(_AutoLN.ToString("0000") + ": " + ALine);
#endif
            }
        }
        public static void mLogError(string ALine,
                    [System.Runtime.CompilerServices.CallerLineNumber]int _AutoLN = 0,
                    [System.Runtime.CompilerServices.CallerMemberName]string _AutoFN = "",
                    [System.Runtime.CompilerServices.CallerFilePath] string _AutoCN = "")
        {
            //if (mbLog)
            {
#if SCP_PROGRAM
                CProgram.sLogError(ALine, _AutoLN, _AutoFN, _AutoCN);
#else
                Console.WriteLine(_AutoLN.ToString("0000") + "> ERROR: " + ALine);
#endif
            }
        }

        public static Int32 crcBlock(byte[] data, UInt32 AStartOffset, UInt32 length, ref UInt16 ArCrcVal)
        {
            UInt32 j, i = AStartOffset;
            UInt16 crcVal = ArCrcVal;

            if (0 == length)
            {
                CScpReader.mLogError("Attempting to CRC 0 bytes! Aborting.");
                return -1;
            }
            for (j = 0; j < length; j++, ++i)
            {
                crcVal = (UInt16)(ccitt_crc16_table[(data[i] ^ (crcVal >> 8)) & 0xff] ^ (crcVal << 8));
            }
            ArCrcVal = crcVal;
            return 0;
        }

        public static byte read8(byte[] ADataArray, UInt32 AOffset)
        {
            return ADataArray[AOffset];
            //            unsigned char* pC = &pData[offset];
            //            return *pC;
        }

        public static UInt16 read16(byte[] ADataArray, UInt32 AOffset)
        {
            Int32 b8 = ((Int32)ADataArray[AOffset]) & 0x00FF;
            Int32 result = b8;
            b8 = ((Int32)ADataArray[AOffset + 1]) & 0x00FF;
            result |= b8 << 8;

            return (UInt16)result;
            //            unsigned char* pC = &pData[offset];
            //            return (UInt16)pC[0] + (UInt16)pC[1] * 256;
        }

        public static UInt32 read32(byte[] ADataArray, UInt32 AOffset)
        {
            Int32 b8 = ((Int32)ADataArray[AOffset]) & 0x00FF;
            Int32 result = b8;

            b8 = ((Int32)ADataArray[AOffset + 1]) & 0x00FF; result |= b8 << 8;
            b8 = ((Int32)ADataArray[AOffset + 2]) & 0x00FF; result |= b8 << 16;
            b8 = ((Int32)ADataArray[AOffset + 3]) & 0x00FF; result |= b8 << 24;

            return (UInt32)result;
            //            unsigned char* pC = &pData[offset];
            //            return (((UInt32)pC[3] * 256 + (UInt32)pC[2]) * 256
            //                + (UInt32)pC[1]) * 256 + (UInt32)pC[0];
        }

        public static bool mbSectionHeader(byte[] ADataArray, UInt32 AStartOffset, out UInt32 ArLength)
        {
            UInt16 theircrcValue = read16(ADataArray, AStartOffset);                // CRC for section
            UInt32 id = read16(ADataArray, AStartOffset + 2);                             // Section ID for Section
            UInt32 length = read32(ADataArray, AStartOffset + 4);                              // Length for Section
            UInt32 sVersion = read8(ADataArray, AStartOffset + 8);                        // Section Version (2.2)
            UInt32 pVersion = read8(ADataArray, AStartOffset + 9);                        // Protocol Version (2.2)

            mLogLine("****** Section " + id.ToString() + " Header ******");
            //cout + "\n****** Section " + id + " Header ******" );
            ArLength = length;

            UInt16 ourcrcValue = 0xffff;
            crcBlock(ADataArray, AStartOffset + 2, length - 2, ref ourcrcValue);

#if CHECK_CRC
            if (ourcrcValue != theircrcValue)
            {
                mLogError("CRC error! file: 0x" + theircrcValue.ToString("X2") + " - calculated: 0x" + ourcrcValue.ToString("X2"));
                return false;
            }
            else
            {
                //cout + "CRC Valid: 0x" + hex + theircrcValue + dec );
#else
                //cout + "Ignoring CRC (0x" + hex + theircrcValue 
                //     + " : 0x" + ourcrcValue + ")" + dec );
                {
#endif
                //cout + "Section Length: " + *length );
                //cout + "Section Version: " + sVersion/10 + "." + sVersion%10 );
                //cout + "Protocol Version: " + pVersion/10 + "." + pVersion%10 );
            }
            return true;
        }

        public string readString(byte[] ADataArray, UInt32 AStartOffset, UInt16 ALen)
        {
            string s = "";
            byte b;
            UInt32 index = AStartOffset;

            for (int i = 0; i < ALen; ++i, ++index)
            {
                b = ADataArray[index];
                if (b == 0)
                {
                    break;
                }
                s += (char)b;
            }
            return s;
        }

        public static short[] mInhouseDecode(byte[] buffer, int offset, int nrbytes, int length, byte difference)
        {
            // This safes us some calculations.
            nrbytes += offset;

            // Check if input data makes sense.
            if ((buffer != null)
            && (nrbytes <= buffer.Length))
            {
                // Setting up the variables for decode.
                short[] leadData = new short[length];
                int currentTime = 0;
                int currentBit = (offset << 3);
                int max = 9;
                // copy this to tz code
                while (((currentBit >> 3) < nrbytes)
                    && ((currentTime) < length))
                {
                    int count = currentBit;
                    int cmax = currentBit + max;
                    // Read in bits till 0 bit or defined maximum.
                    for (; (currentBit < cmax) && ((currentBit >> 3) < nrbytes) && (((buffer[currentBit >> 3] >> (0x7 - (currentBit & 0x7))) & 0x1) != 0); currentBit++) ;

                    // determine number of bits
                    count = currentBit - count;

                    // Increase current bit one more time
                    if (count != max)
                    {
                        currentBit++;
                    }

                    // If it doesn't fit stop
                    if ((currentBit >> 3) >= nrbytes)
                    {
                        break;
                    }

                    if (count >= max)
                    {
                        // Read in last bit
                        bool tmp = (((buffer[currentBit >> 3] >> (0x7 - (currentBit & 0x7))) & 0x1) == 0);
                        currentBit++;
                        // store starting point of additional bits.
                        int start = currentBit;
                        // If last bit 0 read in 8 additional bits else 16 additional bits.
                        int stop = currentBit + (tmp ? 8 : 16);

                        // If it doesn't fit return with error
                        if ((stop >> 3) >= nrbytes)
                        {
                            break;
                        }

                        // Reading in number of extra  bits.
                        for (count = 0; currentBit < stop; currentBit++)
                        {
                            count <<= 1;
                            count |= ((buffer[currentBit >> 3] >> (0x7 - (currentBit & 0x7))) & 0x1);
                            if ((start == currentBit)
                                && (count != 0))
                            {
                                count = -1;
                            }
                        }
                    }
                    else if (count != 0)
                    {
                        // If it doesn't fit stop
                        if ((currentBit >> 3) >= nrbytes)
                        {
                            break;
                        }
                        // if last bit is one value is negative.
                        if (((buffer[currentBit >> 3] >> (0x7 - (currentBit & 0x7))) & 0x1) != 0)
                        {
                            count = -count;
                        }
                        currentBit++;
                    }
                    leadData[currentTime] = ((short)count);
                    /*                   // Decode Differences.
                                       switch (difference)
                                       {
                                           case 0:
                                               leadData[currentTime] = ((short)count);
                                               break;
                                           case 1:
                                               leadData[currentTime] = ((currentTime == 0) ? (short)count : (short)(count + leadData[currentTime - 1]));
                                               break;
                                           case 2:
                                               leadData[currentTime] = ((currentTime < 2) ? (short)count : (short)(count + (leadData[currentTime - 1] << 1) - leadData[currentTime - 2]));
                                               break;
                                           default:
                                               // Undefined difference used exit empty.
                                               return null;
                                       }
                   */                    // Increment time by one.
                    currentTime++;
                }
                return leadData;
            }
            return null;
        }


        public static UInt32 get_bits(out UInt32 ArOutData, UInt16 ANrBits,
                    byte[] ArInArray, UInt32 AStartOffset, ref UInt16 ArBitNum, UInt32 sign_extend)  // ????
        {
            UInt16 sign_bit = (UInt16)(ANrBits - 1);
            UInt16 nrBits = ANrBits;
            UInt32 index = AStartOffset;
            UInt32 result = 0;
            UInt32 b8;

            try
            {
                if (nrBits > 32)
                {
                    CProgram.sLogError("SCP reader: getBits(" + ANrBits.ToString());
                    nrBits = 0;
                }
                while (nrBits > 0)
                {
                    if (nrBits <= ArBitNum)
                    {
                        // in some cases index becomes larger then array size
                        // therefor array is allocated with extra bytes to prevent exception
                        // then last signals are not ok
                        b8 = index >= ArInArray.Length ? 0 : ((UInt32)ArInArray[index]) & 0x00FF;   // *in_byte

                        result |= (UInt16)((b8 >> (ArBitNum - nrBits)) & (0x00ff >> (UInt16)(8 - nrBits)));
                        ArBitNum -= nrBits;
                        nrBits = 0;
                    }
                    else
                    {
                        b8 = index >= ArInArray.Length ? 0 : ((UInt32)ArInArray[index]) & 0x00FF;   // *in_byte

                        result |= (UInt32)((b8 & (0x00ff >> (UInt16)(8 - ArBitNum))) << (nrBits - ArBitNum));
                        nrBits -= ArBitNum;
                        ArBitNum = 0;
                    }
                    if (0 == ArBitNum)
                    {
                        ArBitNum += 8;
                        ++index; // in_byte++;
                    }
                }
                // Sign extend the value we read
                if (sign_extend != 0 && 0 != (result & (0x01 << sign_bit)))
                {
                    result |= (UInt32)(-1 << sign_bit);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Scp reader: read section 6 getBits ", ex);
            }
            ArOutData = result;

            return index;
        }

        UInt32 mSetFileName( string AFilePath )
        {
            mFileName = Path.GetFileName(AFilePath);
            mFileTZSeqNr = 0;   // extract from file name <snr>_<seqNr>.scp or <snr>_<seqNr>_YYYYMMDDHHSS.scp

            if( mFileName != null && mFileName.Length > 0)
            {
                string[] parts = mFileName.Split('_');

                if( parts != null && parts.Length >= 2)
                {
                    UInt32.TryParse(parts[1], out mFileTZSeqNr);
                }
            }
            return mFileTZSeqNr;
    }

    // TZ scp file has UTC date time + TimeZone  -> no TimeZoneCor
    // DV2 scp file has local Date Time (no TZ) -> apply default Time zone correction to get UTC+TZ
    public bool mbReadFile(string AFilePath, bool AbWriteCsv, Int16 ADefaultTimeZone, bool AbApplyTimeZoneCor,  bool AbLogResult)
        {
            string ext = Path.GetExtension(AFilePath);
            string fileName = Path.GetFileName(AFilePath);
            bool bSetLog = false;
            bool bLogData = false;

            mbReadDT = false;
            mbReadTZ = false;
            mTimeZoneOffsetMin = ADefaultTimeZone;

            if (ext != ".scp" && ext != ".SCP")
            {
                mLogError("Not an SCP file");
                return false;
            }
            uint seqNr = mSetFileName(AFilePath);
            mLogLine("Opening File: " + fileName + ( seqNr == 0 ? "" : " TZ#"+ seqNr.ToString() ));

            if (bSetLog)
            {
                bLogData = true;
                mbLog = true;
                if (bLogData) AbWriteCsv = true;

            }
            #region readFile
            UInt16 calculatedCrcValue;           // The CRC we calculate
            UInt16 theircrcValue;                // The CRC from the file
            UInt32 length;                         // The length read from the file

            byte[] readBuffer = null;  //pRead         // Pointer used for the read command

            byte[] firstBlock = new byte[32];
            byte[] tempKey = new byte[16];
            byte[] tempIV = new byte[16];

            UInt32 i;

#if SCP_AES
            aes_context aes_ctx;
#endif
            try
            {
                FileStream fstream = File.OpenRead(AFilePath);
                if (fstream == null)
                {
                    mLogError("Failed open file. Aborting.");
                    return false;
                }
                Int32 fileLength = (Int32)fstream.Length;
                // ReadFully(stream);

                // Make sure the file is not too big for the format
                if (fileLength > MAX_FILE_SIZE)
                {
                    mLogError("File size is SIGNIFICANTLY larger than expected. Aborting.");
                    fstream.Close();
                    return false;
                }
                // Make sure the file is not too small for the format
                if (fileLength < MIN_FILE_SIZE)
                {
                    mLogError("File size is too small for an SCP file. Aborting.");
                    fstream.Close();
                    return false;
                }
                // Read in the first TWO 16 byte blocks to see if the file has been encrypted
                for (i = 0; i < 32; i++)
                {
                    firstBlock[i] = 0;
                }
                fstream.Read(firstBlock, 0, 32);           // Read the first block into RAM

                string str0 = readString(firstBlock, 0, 6);
                string str = readString(firstBlock, 16, 6);   // Copy what should be the "SCPECG" string 
                if (0 != str.CompareTo(SCP_ID_STRING))// Check that we are reading an SCP file
                {
#if SCP_AES == false
                    mLogError("File may be encrypted. No decrypt present.");
                    fstream.Close();
                    return false;
#else
                mLogLine( "File may be encrypted. Attempting to decrypt." );

                for (i = 0; i < 16; i++)
                {
                    tempKey[i] = constKey[i];
                    tempIV[i] = constIV[i];
                }

                aes_setkey_dec(&aes_ctx, tempKey, 128);
                // Decrypt the first TWO blocks of the file
                if ((i = aes_crypt_cbc(&aes_ctx, AES_DECRYPT, 32, tempIV, firstBlock, firstBlock)))
                {
                    mLogError("AES Decrypt Error 1. (0x" + i.ToString("X2") + ") Aborting.");
                    fstream.Close();
                    return false;
                }
                // Check to make sure we now have a valid file format string
                str = readString(firstBlock, 16, 6);   // Copy what should be the "SCPECG" string 
                if (0 != str.CompareTo(SCP_ID_STRING))// Check that we are reading an SCP file
                {
                    mLogError("File may be encrypted. No decrypt present.");
                    fstream.Close();
                    return false;
                }
                mLogLine("File decryption successful.");

                theircrcValue = read16(firstBlock, 0);
                length = read32(firstBlock, 2);

                if (length > fileLength)
                {
                    mLogError("File Corrupted. Invalid Length value. Aborting.");
                    fstream.Close();
                    return false;
                }

                readBuffer = new byte[fileLength+16]; // add extra for possible overun   // Allocate enough space to read in the whole file     
                for (i = 0; i < 32; i++)
                {
                    readBuffer[i] = firstBlock[i];                 // Copy the first block into the file buffer
                }
                fstream.Read(readBuffer, 32, fileLength - 32);    // Store the remainder of the file in memory

                // Decrypt the remainder of the file
                if ((i = aes_crypt_cbc(&aes_ctx, AES_DECRYPT, fileLength - 32, tempIV, &readBuffer[32], &readBuffer[32])))
                {
                    mLogError( "AES Decrypt Error 2. (0x"  + i.ToString( "X" ) + ") Aborting.");
                    readBuffer = null;
                    fstream.Close();
                    return false;
                }

                calculatedCrcValue = 0xffff;                        // CRC is based off an initial value of 0xffff
                crcBlock(readBuffer, 2, length - 2, ref calculatedCrcValue); // Calculate the CRC for the remainder of the file
#endif
                }
                else
                {
                    mLogLine("File not encrypted. Processing.");

                    theircrcValue = read16(firstBlock, 0);
                    length = read32(firstBlock, 2);

                    readBuffer = new byte[fileLength + 16]; // add extra for possible overun          // Allocate enough space to read in the whole file     
                    for (i = 0; i < 32; i++)
                    {
                        readBuffer[i] = firstBlock[i];                 // Copy the first block into the file buffer
                    }
                    fstream.Read(readBuffer, 32, fileLength - 32);    // Store the remainder of the file in memory

                    calculatedCrcValue = 0xffff;                        // CRC is based off an initial value of 0xffff
                    crcBlock(readBuffer, 2, length - 2, ref calculatedCrcValue); // Calculate the CRC for the remainder of the file
                }
                fstream.Close();

                mLogLine("File Length: " + length.ToString());            // Print out the file length    

#if CHECK_CRC
                if (calculatedCrcValue != theircrcValue)
                {            // If the CRC doesn't match, something has gone wrong
                    mLogError("ERROR: File CRC Invalid. Abort");
                    mLogLine("their CRC: " + theircrcValue.ToString("X2") + " - our CRC: " + calculatedCrcValue.ToString("X2"));
                    //cout + "File Length: " + length );            // Print out the file length  
                    return false;
                }
                else
                {
                    mLogLine("File CRC Valid: 0x" + theircrcValue.ToString("X2"));  // Notify user that we are parsing
                }

#else
            mLogLine( "Ignoring CRC check: their CRC: " + theircrcValue.ToString("X2") + " - our CRC: " + calculatedCrcValue.ToString( "X2" );

#endif

                //cout + "File Length: " + length );            // Print out the file length    

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Scp reader: read file" + AFilePath, ex);
                return false;
            }
            #endregion
            if (readBuffer == null)
            {
                return false;
            }
            /**********************************************************************************************
            *          Section 0 Pointers
            **********************************************************************************************/
            UInt32 sI = 6;
            UInt32 j;
            subSection[] sections = new subSection[12];                              // Array of sections to store file data
            #region section0
            try
            {


                mLogLine("Reading Section 0");
                if (false == mbSectionHeader(readBuffer, sI, out length))
                {               // Parse the header for section 0
                    mLogError("Failed SectionHeader");
                    return false;
                }
                for (j = 16; j < length; j += 10)
                {
                    UInt16 id = read16(readBuffer, sI + j);         // Section Number: 0 - 11
                    UInt32 len = read32(readBuffer, sI + j + 2);        // Section Length
                    UInt32 ind = read32(readBuffer, sI + j + 6);        // Section Start Index
                                                                        //cout + "Section: " + id + " - Length: " + len + " - Index: " + ind );
                    if (id < 12)
                    {
                        if (sections[id] == null)
                        {
                            sections[id] = new subSection();
                        }
                        if (sections[id] == null)
                        {
                            mLogError("subsection == null");
                            return false;
                        }
                        sections[id].init(id, len, ind, readBuffer);   // Copy data into section classes for later access
                    }
                    else
                    {                                        // We've hit some sort of error
                        mLogError("ERROR: Unexpected section index(" + id.ToString() + ")! Aborting.");
                        return false;
                    }
                }
                readBuffer = null;// Free the buffer we used for file input
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Scp reader: read section 0 from file" + AFilePath, ex);
            }


            #endregion

            /**********************************************************************************************
            *          Section 1        patient info 
            **********************************************************************************************/
            if (sections[1].exists())
            {
                mLogLine("Reading Section 1");
                #region section1 Info
                try
                {


                    subSection sect = sections[1];
                    UInt16 year = 0, month = 0, day = 0, hour = 0, minutes = 0, seconds = 0;
                    string name;
                    if (false == sect.readHeader(out length))
                    {
                        return false;
                    }
                    for (j = 16; j + 2 <= length;)     // tag must be at least 2 byte long
                    {
                        byte tag = sect.read_8(ref j);
                        UInt16 tagLength = sect.read_16(ref j);
                        UInt32 jNext = j + tagLength;
                        //cout + "(tag=" + (int)tag + ") (length=" + (int)tagLength + ") ";
                        switch (tag)
                        {
                            case 0:// patient last name
                                if (tagLength > 0)
                                {
                                    sect.readString(ref j, out mPatientLastName, tagLength);

                                    mLogLine("Patient Last Name= " + mPatientLastName);
                                }
                                break;
                            case 1: // ptaient First Name
                                sect.readString(ref j, out mPatientFirstName, tagLength);
                                mLogLine("Patient First Name= " + mPatientFirstName);
                                break;
                            case 2: // Patient ID
                                sect.readString(ref j, out mPatientID, tagLength);
                                break;
                            case 3:// Patient second last name
                                sect.readString(ref j, out name, tagLength);
                                if (name.Length > 0) mPatientLastName += " " + name;
                                mLogLine("Patient Last Name= " + mPatientLastName);
                                break;
                            case 4:// Patient Age (assuming years)
                                mAge = sect.read_8(ref j);
                                mLogLine("Age=" + mAge.ToString());
                                break;
                            case 5:// Patient DOB
                                {
                                    UInt16 birthYear = sect.read_16(ref j);
                                    UInt16 birthMonth = sect.read_8(ref j);
                                    UInt16 birthDay = sect.read_8(ref j);

                                    mDateOfBirth = CProgram.sCalcYMD(birthYear, birthMonth, birthDay);
                                    //(uint)sect.read_16(ref j) * 10000 + (uint)(sect.read_8(ref j)) * 100 + (uint)(sect.read_8(ref j));
                                    mLogLine("DOB=" + mDateOfBirth.ToString());
                                }
                                break;
                            case 6: // Patient Height
                                {
                                    UInt16 value = sect.read_16(ref j);
                                    byte type = sect.read_8(ref j);
                                    mHeightM = value * 0.01F; // default cm
                                    switch (type)
                                    {
                                        case 0: break; // unkown
                                        case 1: break; //cm
                                        case 2: mHeightM = value * 0.0254F; break; // inc
                                        case 3: mHeightM = value * 0.001F; break; // mm

                                    }
                                    mLogLine("HeightM= " + mHeightM.ToString("0.000"));

                                }
                                break;
                            case 7: // Patient Weight
                                {
                                    UInt16 value = sect.read_16(ref j);
                                    byte type = sect.read_8(ref j);
                                    mWeightKg = value;
                                    switch (type)
                                    {
                                        case 0: break; // unkown
                                        case 1: break; //Kilogram
                                        case 2: mWeightKg = value * 0.001F; break; // gram
                                        case 3: mWeightKg = value * 0.45359237F; break; // pound
                                        case 4: mWeightKg = value * 0.0283495231F; break; // ounce
                                    }
                                    mLogLine("WeightKg= " + mWeightKg.ToString("0.000"));

                                }
                                break;
                            case 8: // Patient Sex
                                mSex = sect.read_8(ref j);
                                mLogLine("Sex=" + mSex.ToString());
                                break;
                            case 9: // Patient Race
                                mRace = sect.read_8(ref j);
                                mLogLine("Race=" + mRace.ToString());
                                break;
                            case 10: // Patient Drugs
                                // not implemented
                                break;
                            case 11: // Patient Systolic blood presure
                                // not implemented
                                break;
                            case 12:// Patient Diastolic blood preasure
                                // not implemented
                                break;
                            case 13: // Patient referal indication
                                sect.readString(ref j, out mReferal, tagLength);
                                mLogLine("Referal= " + mReferal);
                                break;
                            case 14:// Device ID
                                #region TAG14 DeviceID
                                {              // Device ID
                                    UInt16 institution = sect.read_16(ref j);      // Not sure what this is, set to 0
                                    UInt16 department = sect.read_16(ref j);       // Not sure what this is, set to 0
                                    UInt16 deviceID = sect.read_16(ref j);         // Not sure what this is, set to 0
                                    byte deviceType = sect.read_8(ref j);                // 0 = cart, 1 = Host
                                    UInt16 mfgCode = sect.read_8(ref j);           // 255 = check string at end
                                    string modelDesc;
                                    sect.readString(ref j, out modelDesc, 6);      // ASCII description of device
                                    UInt16 protocolRev = sect.read_8(ref j);       // SCP protocol version
                                    UInt16 protocolCompat = sect.read_8(ref j);    // Category I or II
                                    UInt16 languageSupport = sect.read_8(ref j);   // 0 = ASCII
                                    UInt16 deviceCapability = sect.read_8(ref j);  // print, interpret, store, acquire
                                    UInt16 mainsFreq = sect.read_8(ref j);         // 50 Hz, 60 Hz, Unspecified
                                    j += 16;
                                    UInt16 apRevLength = sect.read_8(ref j);       // Length of Revision String
                                    string apRev;
                                    sect.readString(ref j, out apRev, apRevLength);         // ASCII Revision of analysis prog.

                                    UInt32 jRest = j;
                                    UInt32 nRest = sect.bytesLeft(jRest);
                                    string strRest;
                                    sect.readStringBuffer(ref jRest, out strRest, nRest);

                                    string devSerNo;
                                    sect.readAString(ref j, out devSerNo);   // ASCII Device Serial Number

                                    //     UInt16 devSoftNoLength = sect.read_8(ref j);   // Length of software string
                                    string devSoftNo;
                                    sect.readAString(ref j, out devSoftNo); // ASCII Software Version
                                                                            //     UInt16 devSCPidLength = sect.read_8(ref j);    // Length of SCP software ID
                                    string devSCPid;
                                    sect.readAString(ref j, out devSCPid);   // ASCII SCP software ID
                                                                             //                                    UInt16 mfgStringLength = sect.read_8(ref j);   // Length of mfg name
                                    string mfgString;
                                    sect.readAString(ref j, out mfgString); // Manufacturer Name
                                    mDeviceID = /*modelDesc + "_" + */devSerNo;
                                    mDeviceManufact = modelDesc + (modelDesc.Length > 0 ? "_" : "") + mfgString;

                                    mLogLine("[TAG] Device ID:");
                                    mLogLine("---Institution Number: " + institution);
                                    mLogLine("---Department Number: " + department);
                                    mLogLine("---Device ID: " + deviceID);
                                    mLogLine("---Device Type: " + deviceType + (deviceType > 0 ? " (Host)" : " (Cart)"));
                                    mLogLine("---Manufacturer Number: " + mfgCode + ((mfgCode == 255) ? " (See string)" : ""));
                                    mLogLine("---Model Description: " + modelDesc);
                                    mLogLine("---SCP-ECG Version: " + (protocolRev / 10) + "." + (protocolRev % 10));
                                    mLogLine("---SCP-ECG Compatibility: 0x" + protocolCompat.ToString("X4"));
                                    if ((protocolCompat & 0xf0) == 0xd0) mLogLine(" (Category I)");
                                    else if ((protocolCompat & 0xf0) == 0xe0) mLogLine(" (Category II)");
                                    else mLogLine(" (unknown)");
                                    mLogLine("---Language Support: 0x" + languageSupport.ToString("X4")
                                        + (languageSupport > 0 ? " (unknown)" : " (ASCII Only)"));
                                    mLogLine("---Device Capabilities: 0x" + deviceCapability.ToString("X4"));
                                    mLogLine("------Print: " + ((deviceCapability & 0x10) != 0 ? "yes" : "no"));
                                    mLogLine("------Interpret: " + ((deviceCapability & 0x20) != 0 ? "yes" : "no"));
                                    mLogLine("------Store: " + ((deviceCapability & 0x40) != 0 ? "yes" : "no"));
                                    mLogLine("------Acquire: " + ((deviceCapability & 0x80) != 0 ? "yes" : "no"));
                                    mLogLine("---Mains Frequency Environment: " + mainsFreq.ToString());
                                    if (mainsFreq == 0) mLogLine(" (Unspecified)");
                                    else if (mainsFreq == 1) mLogLine(" (50 Hz)");
                                    else if (mainsFreq == 2) mLogLine(" (60 Hz)");
                                    else mLogLine(" (ERROR)");
                                    mLogLine("---Analysis Program Revision:  (" + apRevLength + ")" + apRev);
                                    mLogLine("---Device Serial Number: " + devSerNo);
                                    mLogLine("---Device Software Number:  " + devSoftNo);
                                    mLogLine("---Device SCP Software:  " + devSCPid);
                                    mLogLine("---Device Manufacturer: " + mfgString);

                                    if (modelDesc == "TZMR")
                                    {
                                        // not needed  mbFlipSignal = true;
                                        bool bTest = false;
                                    }
                                }
                                #endregion
                                break;
                            case 15: // analyzing Device 
                                // not implemented
                                break;
                            case 16: 
                                sect.readString(ref j, out mAcquiringInstitution, tagLength);
                                mLogLine("AcquiringInstitution= " + mAcquiringInstitution);
                                break;
                            case 17:
                                sect.readString(ref j, out mAnalyzingInstitution, tagLength);
                                mLogLine("AnalyzingInstitution= " + mAnalyzingInstitution);
                                break;
                            case 18:
                                sect.readString(ref j, out mAcquiringDepartment, tagLength);
                                mLogLine("AcquiringDepartment= " + mAcquiringDepartment);
                                break;
                            case 19:
                                sect.readString(ref j, out mAnalyzingDepartment, tagLength);
                                mLogLine("AnalyzingDepartment= " + mAnalyzingDepartment);
                                break;
                            case 20:
                                sect.readString(ref j, out mReferringPhysician, tagLength);
                                mLogLine("ReferringPhysician= " + mReferringPhysician);
                                break;
                            case 21:
                                sect.readString(ref j, out mConfirmingPhysician, tagLength);
                                mLogLine("ConfirmingPhysician= " + mConfirmingPhysician);
                                break;
                            case 22:
                                sect.readString(ref j, out mTechnician, tagLength);
                                mLogLine("Technician " + mTechnician);
                                break;
                            case 23:
                                sect.readString(ref j, out mRoom, tagLength);
                                mLogLine("Room= " + mRoom);
                                break;
                            case 24: // stat code
                                // not implemented
                                break;
                            case 25:// Date of Acquisition
                                #region TAG25 Date
                                {              // Date of Acquisition
                                    if (tagLength != 4)
                                    {
                                        mLogError("Error Parsing Date! Length = " + (int)tagLength);
                                        return false;
                                    }
                                    else
                                    {
                                        year = sect.read_16(ref j);
                                        month = sect.read_8(ref j);
                                        day = sect.read_8(ref j);
                                        mbReadDT = true;
                                        mLogLine("[TAG] Date: " + year.ToString() + "/" + month.ToString("00") + "/" + day.ToString("00"));
                                    }
                                }
                                #endregion
                                break;
                            case 26:// Time of Acquisition
                                #region TAG26 Time
                                {              // Time of Acquisition (UTC)
                                    if (tagLength != 3)
                                    {
                                        mLogError("Error Parsing Time! Length = " + tagLength);
                                        return false;
                                    }
                                    else
                                    {
                                        hour = sect.read_8(ref j);
                                        minutes = sect.read_8(ref j);
                                        seconds = sect.read_8(ref j);
                                        mLogLine("[TAG] Time: " + hour.ToString() + ":" + minutes.ToString() + ":" + seconds.ToString());
                                        mbReadDT = true;
                                    }
                                }
                                #endregion
                                break;
                            case 27:
                                #region TAG27 High pass filter
                                {              // High Pass Filter
                                    if (tagLength != 2)
                                    {
                                        mLogError("Error Parsing HP Filter! Length = " + (int)tagLength);
                                        return false;
                                    }
                                    else
                                    {
                                        UInt16 hpFilter = sect.read_16(ref j);
                                        mLogLine("[TAG] High Pass Filter: " + (hpFilter * 0.01F).ToString("0.00") + " Hz");
                                    }
                                }
                                #endregion
                                break;
                            case 28:
                                #region TAG28 Low pass filter
                                {              // Low Pass Filter
                                    if (tagLength != 2)
                                    {
                                        mLogError("Error Parsing LP Filter! Length = " + (int)tagLength);
                                        return false;
                                    }
                                    else
                                    {
                                        UInt16 lpFilter = sect.read_16(ref j);
                                        mLogLine("[TAG] Low Pass Filter: " + lpFilter.ToString() + " Hz");
                                    }
                                }
                                #endregion
                                break;
                            case 29:
                                // not implemented
                                break;
                            case 30: // free text
                                {
                                    string line;
                                    if (mFreeText.Length > 0) mFreeText += "\n";
                                    sect.readString(ref j, out line, tagLength);
                                    if (line.Length > 0) mFreeText += line;
                                    mLogLine("FreeText= " + line);
                                }
                                break;
                            case 31: // sequence number
                                {
                                    string ecgSequence;
                                    sect.readString(ref j, out ecgSequence, tagLength);
                                    mLogLine("[TAG] ECG Sequence Number: " + ecgSequence);
                                }
                                break;
                            case 32: // medical history codes
                                // not implemented
                                break;
                            case 33: // electrode config
                                // not implemented
                                break;
                            case 34: // Time Zone of Acquisition
                                #region TAG34 TimeZone
                                {               // Time Zone
                                    mTimeZoneOffsetMin = (Int16)sect.read_16(ref j);
                                    Int16 index = (Int16)sect.read_16(ref j);
                                    string desc;
                                    sect.readString(ref j, out desc, (UInt16)(tagLength - 4));
                                    mbReadTZ = true;

                                    mLogLine("[TAG] Date Time Zone:");
                                    mLogLine("---Minutes Offset from UTC: " + mTimeZoneOffsetMin);
                                    mLogLine("---Time Zone Index (unused): " + index);
                                    mLogLine("---Time Zone Description (unused): \"" + desc + "\"");
                                }
                                #endregion
                                break;
                            case 35: // free medical History
                                // not implemented
                                break;
                            case 255:
                                #region TAG255 Terminator
                                {             // Terminator - we're done.
                                    if (tagLength != 0)
                                    {
                                        mLogError("Error Parsing Terminator! Length = " + (int)tagLength);
                                    }
                                    else
                                    {
                                        mLogLine("[TAG] Terminator.");
                                    }
                                    j = length;
                                }
                                #endregion
                                break;
                            default:
                                {
                                    mLogLine("[TAG] " + (int)tag + " (unsupported)");
                                    j += tagLength;
                                }
                                break;
                        }
                        if (j != jNext)
                        {
                            j = jNext;
                        }
                    }
                    if (mbReadDT)
                    {
                        if (year == 0)
                        {
                            DateTime dt = DateTime.UtcNow;
                            year = (ushort)dt.Year;
                            month = (ushort)dt.Month;
                            day = (ushort)dt.Day;
                        }
                        mMeasureUTC = DateTime.SpecifyKind(new DateTime(year, month, day, hour, minutes, seconds),
                            DateTimeKind.Utc);
                        //                    mMeasureUTC = DateTime.SpecifyKind(mMeasureDT.AddMinutes(-mTimeZoneOffsetMin), DateTimeKind.Utc);
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Scp reader: read section 1 from file" + AFilePath, ex);
                }

                #endregion
            }

            /**********************************************************************************************
             *          Section 2 reading Huffman tables
             **********************************************************************************************/
            HuffmanTable[] hHuffmanTables = null;
            UInt32 huffmanTableCount = 0;
            UInt16 prefix_min_bits = 0xff;
            UInt16 prefix_max_bits = 0;
            if (sections[2].exists())
            {
                mLogLine("Reading Section 2");
                #region section2
                try
                {


                    if (false == sections[2].readHeader(out length))
                    {
                        return false;
                    }
                    j = 16;
                    huffmanTableCount = sections[2].read_16(ref j);

                    if (19999 != huffmanTableCount)
                    {
                        #region loadHuffman
                        UInt32 iTable;
                        HuffmanTable ht;
                        bool bTablePresent = false;
                        //cout + "Number of Huffman Tables: " + tableCount );

                        hHuffmanTables = new HuffmanTable[huffmanTableCount];

                        for (iTable = 0; iTable < huffmanTableCount; iTable++)
                        {
                            ht = new HuffmanTable();
                            hHuffmanTables[iTable] = ht;
                            uint nCreate = sections[2].read_16(ref j);
                            uint bytesLeft = sections[2].bytesLeft(j);
                            if (nCreate > 0 && bytesLeft >= nCreate)    // table must be at least 
                            {
                                bTablePresent = true;
                                ht.mCreate(nCreate);
                                //cout + "Number of codes in Table #" + (l+1) + ": " + codeCount[l] );

                                UInt32 m;
                                for (m = 0; m < ht.codeCount; m++)
                                {
                                    ht.prefixBits[m] = sections[2].read_8(ref j);
                                    if (ht.prefixBits[m] < prefix_min_bits) prefix_min_bits = (UInt16)ht.prefixBits[m];
                                    if (ht.prefixBits[m] > prefix_max_bits) prefix_max_bits = (UInt16)ht.prefixBits[m];
                                    ht.totalBits[m] = sections[2].read_8(ref j);
                                    ht.switchByte[m] = sections[2].read_8(ref j);
                                    ht.baseValue[m] = sections[2].read_16(ref j);
                                    ht.baseCode[m] = sections[2].read_32(ref j);

                                    // Reverse the bit-order for ease of use later
                                    UInt16 k;
                                    Int32 temp = 0;
                                    for (k = 0; k < ht.prefixBits[m]; k++)
                                    {
                                        temp = (temp << 1) | (0 != (ht.baseCode[m] & ((UInt32)1 << k)) ? 1 : 0);
                                    }
                                    ht.baseCode[m] = (UInt32)temp;
                                    #region log bits
                                    /*
                                       cout + "---Code #" + (m+1) + " details:" );
                                       cout + "------Prefix Bits: " + prefixBits[l][m] );
                                       cout + "------Total Bits: " + totalBits[l][m] );
                                       cout + "------Switch Byte: " + switchByte[l][m] );
                                       cout + "------Base Value: " + baseValue[l][m] );
                                       cout + "------Base Code: 0b";// + baseCode[m] + " (0b";

                                       UInt32 n;
                                       UInt32 bitmask = 1;
                                       for(n = 0; n < prefixBits[l][m]; n++){
                                       cout + ((baseCode[l][m] & bitmask)?1:0);
                                       bitmask = bitmask<<1;
                                       }
                                       cout + " (" + baseCode[l][m] + ")" );
                                       */
                                    #endregion
                                }
                            }
                        }
                        #endregion loadHuffman
                        if (bTablePresent == false)
                        {
                            //                    hHuffmanTables = null; // empty huffman tables
                        }
                    }
                    else
                    {
                        #region defaultHuffman
                        mLogLine("Default Huffman table used");

                        //leave value so that we can test on it an do default                         huffmanTableCount = 1;
                        hHuffmanTables = new HuffmanTable[1]; // huffmanTableCount];
                        HuffmanTable ht = new HuffmanTable();
                        hHuffmanTables[0] = ht;
                        ht.mCreateDefaultTable();

                        UInt32 m;
                        UInt16 bits;
                        for (m = 0; m < ht.codeCount; m++)
                        {
                            bits = (UInt16)ht.prefixBits[m];
                            if (bits < prefix_min_bits) prefix_min_bits = bits;
                            if (bits > prefix_max_bits) prefix_max_bits = bits;
                        }

                        #endregion defaultHuffman
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Scp reader: read section 1 from file" + AFilePath, ex);
                }

                #endregion
            }
            /**********************************************************************************************
             *          Section 3 signal info
             **********************************************************************************************/
            UInt32 leadCount = 0;       // signal info
            UInt32[] sampleStart = null;
            UInt32[] sampleEnd = null;
            UInt32[] leadID = null;
            #region section3
            if (sections[3].exists())
            {
                try
                {


                    mLogLine("Reading Section 3");
                    if (false == sections[3].readHeader(out length))
                    {
                        mLogError("Failed read section 3 header");
                        return false;
                    }
                    j = 16;
                    leadCount = sections[3].read_8(ref j);
                    UInt32 flags = sections[3].read_8(ref j);
                    sampleStart = new UInt32[leadCount];
                    sampleEnd = new UInt32[leadCount];
                    leadID = new UInt32[leadCount];
                    for (i = 0; i < leadCount; i++)
                    {
                        sampleStart[i] = sections[3].read_32(ref j);
                        sampleEnd[i] = sections[3].read_32(ref j);
                        leadID[i] = sections[3].read_8(ref j);
                        string ls = "lead " + i.ToString() + " ID= " + leadID[i].ToString();
                        if (leadID[i] == 131) ls += " (ES)";
                        else if (leadID[i] == 132) ls += " (AS)";
                        else if (leadID[i] == 133) ls += " (AI)";
                        ls += " start=" + sampleStart[i].ToString() + " len=" + (sampleEnd[i] - sampleStart[i]).ToString();
                        mLogLine(ls);
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Scp reader: read section 3 from file" + AFilePath, ex);
                }

            }
            #region log leads
            /*
               cout + "Number of Leads: " + leadCount );
               cout + "Flags: 0x" + hex + flags + dec );
               cout + ((flags&0x01)?"---Reference beat subtraction used for compression":
               "---Reference beat subtraction not used for compression") );
               cout + ((flags&0x04)?"---Leads recorded simultaneously":
               "---Leads not recorded simultaneously") );
               cout + "---Number of simultaneously recorded leads: " + (flags/8) );
               for(i = 0; i < leadCount; i++){
               cout + "Details for Lead #" + i );
               cout + "---Starting Sample Number: " + sampleStart[i] );
               cout + "---Ending Sample Number: " + sampleEnd[i] );
               cout + "---Lead Identification Code: " + leadID[i];
               if(leadID[i] == 131) cout + " (ES)" );
               else if(leadID[i] == 132) cout + " (AS)" );
               else if(leadID[i] == 133) cout + " (AI)" );
               else cout );
               }
               */
            #endregion

            #endregion

            /**********************************************************************************************
            *          Section 4 QRS locations, reference beat subtraction zones and protected areas
            **********************************************************************************************/
            #region section4
            if (sections[4].exists())
            {
                mLogLine("Reading Section 4");
                if (false == sections[4].readHeader(out length))
                {
                    mLogError("Failed read section 4 header");
                    return false;
                }
                else
                {
                    mLogLine("Section 4 QRS length = " + length.ToString());
                }
                j = 16;
            }
            #endregion

            /**********************************************************************************************
            *          Section 5 Encoded type 0 reference beat data
            **********************************************************************************************/
            #region section5
            if (sections[5].exists())
            {
                mLogLine("Reading Section 5");
                if (false == sections[5].readHeader(out length))
                {
                    mLogError("Failed read section 5 header");
                    return false;
                }
                j = 16;
            }
            #endregion

            /**********************************************************************************************
            *          Section 7 Global measurements
            **********************************************************************************************/
            #region section7
            if (sections[7].exists())
            {
                mLogLine("Reading Section 7");
                if (false == sections[7].readHeader(out length))
                {
                    mLogError("Failed read section 7 header");
                    return false;
                }
                j = 16;

                /*
                                unsigned int* paceTime, *paceAmplitude;
                                unsigned int* paceType, *paceSource, *paceIndex, *paceWidth;
                                if (sections[7].exists())
                                {
                                    if (sections[7].readHeader(&length)) return -1;
                                    j = 16;
                                    unsigned int referenceCount = sections[7].read_8(&j);
                                    unsigned int paceCount = sections[7].read_8(&j);
                                    unsigned int rrInterval = sections[7].read_16(&j);
                                    unsigned int ppInterval = sections[7].read_16(&j);
                                    paceTime = new unsigned int[paceCount];
                                    paceAmplitude = new unsigned int[paceCount];
                                    for (i = 0; i < paceCount; i++)
                                    {
                                        paceTime[i] = sections[7].read_16(&j);
                                        paceAmplitude[i] = sections[7].read_16(&j);
                                    }
                                    paceType = new unsigned int[paceCount];
                                    paceSource = new unsigned int[paceCount];
                                    paceIndex = new unsigned int[paceCount];
                                    paceWidth = new unsigned int[paceCount];
                                    for (i = 0; i < paceCount; i++)
                                    {
                                        paceType[i] = sections[7].read_8(&j);
                                        paceSource[i] = sections[7].read_8(&j);
                                        paceIndex[i] = sections[7].read_16(&j);
                                        paceWidth[i] = sections[7].read_16(&j);
                                    }
                                    cout << "Number of reference beat types: " << referenceCount << endl; ;
                                    cout << "Number of pacemaker spikes: " << paceCount << endl;
                                    cout << "Average RR interval: ";
                                    if (rrInterval == 29999) cout << "not calculated" << endl;
                                    else cout << rrInterval << " (ms)" << endl;
                                    cout << "Average PP interval: ";
                                    if (ppInterval == 29999) cout << "not calculated" << endl;
                                    else cout << ppInterval << " (ms)" << endl;
                                    for (i = 0; i < paceCount; i++)
                                    {
                                        cout << "Details for pacemaker spike #" << i + 1 << endl;
                # ifdef PACEMAKER_EXTENDED_INFO
                                        cout << "(" << paceTime[i] << " " << paceAmplitude[i] << " "
                                             << paceType[i] << " " << paceSource[i] << " "
                                             << paceIndex[i] << " " << paceWidth[i] << ")" << endl;
                                        cout << "---Time: " << paceTime[i] << " (ms)" << endl;
                                        cout << "---Amplitude: ";
                                        if (paceAmplitude[i] == 29999) cout << "not calculated" << endl;
                                        else cout << paceAmplitude[i] << " (uV)" << endl;
                                        cout << "---Type: ";
                                        if (paceType[i] == 255) cout << "not calculated" << endl;
                                        else if (paceType[i] == 0) cout << "unknown" << endl;
                                        else if (paceType[i] == 1) cout << "triggers neither P-wave nor QRS" << endl;
                                        else if (paceType[i] == 2) cout << "triggers a QRS" << endl;
                                        else if (paceType[i] == 3) cout << "triggers a P-wave" << endl;
                                        else cout << "ERROR" << endl;
                                        cout << "---Source: ";
                                        if (paceSource[i] == 0) cout << "unknown" << endl;
                                        else if (paceSource[i] == 1) cout << "internal" << endl;
                                        else if (paceSource[i] == 2) cout << "external" << endl;
                                        else cout << "ERROR" << endl;
                                        cout << "---Triggered QRS complex: ";
                                        if (paceIndex[i] == 0) cout << "none" << endl;
                                        else cout << paceIndex[i];
                                        cout << "---Pulse Width: ";
                                        if (paceWidth[i] == 0) cout << "unknown" << endl;
                                        else cout << paceWidth[i] << " (us)" << endl;
                #else
                                        cout << "---Pulse Width: ";
                                        if (paceWidth[i] == 0) cout << "unknown";
                                        else cout << paceWidth[i] << " (us)";
                                        cout << " at time: " << paceTime[i] << " (ms)" << endl;
                #endif
                                    }
                                }


                */
            }
            #endregion

            /**********************************************************************************************
            *          Section 8 Storage of full text interpretive statements
            **********************************************************************************************/
            #region section8
            if (sections[8].exists())
            {
                mLogLine("Reading Section 8");
                if (false == sections[8].readHeader(out length))
                {
                    mLogError("Failed read section 8 header");
                    return false;
                }
                j = 16;
            }
            #endregion

            /**********************************************************************************************
            *          Section 9 manufacturer specific interpretive statements
            **********************************************************************************************/
            #region section9
            if (sections[9].exists())
            {
                mLogLine("Reading Section 9");
                if (false == sections[9].readHeader(out length))
                {
                    mLogError("Failed read section 9 header");
                    return false;
                }
                j = 16;
            }
            #endregion
            /**********************************************************************************************
            *          Section 10 Lead measurement block
            **********************************************************************************************/
            #region section10
            if (sections[10].exists())
            {
                mLogLine("Reading Section 10");
                if (false == sections[10].readHeader(out length))
                {
                    mLogError("Failed read section 10 header");
                    return false;
                }
                j = 16;
            }
            #endregion
            /**********************************************************************************************
                        *          Section 11 universal ECG interpretive statement codes
                        **********************************************************************************************/
            #region section11
            if (sections[11].exists())
            {
                mLogLine("Reading Section 11");
                if (false == sections[11].readHeader(out length))
                {
                    mLogError("Failed read section 11 header");
                    return false;
                }
                j = 16;
            }
            #endregion


            /**********************************************************************************************
             *          Section 6 Signal config
             **********************************************************************************************/
            //            UInt32[] leadLength = null;
            //            UInt32[][] dataArrays = null;
            UInt32 ampMult = 0;
            UInt32 samplePeriod = 0;
            UInt32 encoding = 0;
            UInt32 compression = 0;
            string lineStr;

            string readResult = "SCP(" + fileName + ")";

            if (mbReadDT) readResult += " read ";
            if (AbApplyTimeZoneCor)
            {
                readResult += " cor";
                mMeasureUTC = DateTime.SpecifyKind(mMeasureUTC.AddMinutes(-mTimeZoneOffsetMin), DateTimeKind.Utc);
            }
            else
            {
                readResult += "";
            }
            readResult += CProgram.sDateTimeToYMDHMS(mMeasureUTC);
            
            readResult += mbReadTZ ? " readTZ " : " def TZ";
            readResult += CProgram.sTimeZoneOffsetStringLong(mTimeZoneOffsetMin);
            readResult += " D=" + mDeviceID + " P="+ mPatientID;


            CScpData[] sda = null;
            if (sections[6].exists())
            {
                mLogLine("Reading Section 6 (flip=" + mbFlipSignal.ToString() + ")");
                #region section 6
                try
                {
                    if (false == sections[6].readHeader(out length))
                    {
                        mLogError("Failed reading Section 6 header");
                        return false;
                    }
                    j = 16;
                    ampMult = sections[6].read_16(ref j);    // nV 
                    samplePeriod = sections[6].read_16(ref j); // uS
                    encoding = sections[6].read_8(ref j);    // 0 = real, 1 = 1st derived, 2 = second derived
                    compression = sections[6].read_8(ref j); // none, 1 = bimodel compression used

                    sda = new CScpData[leadCount];
                    CScpData sd;
                    //                leadLength = new UInt32[leadCount];
                    //                dataArrays = new UInt32[][leadCount];
                    for (i = 0; i < leadCount; i++)
                    {
                        sda[i] = new CScpData();
                        UInt16 ll = sections[6].read_16(ref j);
                        UInt32 size = sampleEnd[i] - sampleStart[i] + 1;
                        sda[i].init(ll, size);
                    }
                    for (i = 0; i < leadCount; i++)
                    {
                        sd = sda[i];

                        //cout + "leadLength: " + leadLength[i] + ", index: 0x" + hex + j + dec );
                        byte[] rawData = new byte[sd.leadLength + 16]; // add extra for possible overun
                        UInt32 l2, iRaw = 0;

                        for (l2 = 0; l2 < sd.leadLength; l2++)
                        {
                            rawData[l2] = sections[6].read_8(ref j);
                        }
                        if (bLogData)
                        {
                            #region logData
                            try
                            {
                                mLogLine("RAW data: Lead " + i.ToString() + " size= " + sd.leadLength);
                                lineStr = "";
                                uint nLog = sd.leadLength < 1600 ? sd.leadLength : 1600;
                                for (l2 = 0; l2 < nLog; l2++)
                                {
                                    if (l2 % 16 == 0)
                                    {
                                        if (lineStr.Length > 0)
                                        {
                                            mLogLine(lineStr);
                                        }
                                        lineStr = l2.ToString("X04") + ":";
                                    }
                                    lineStr += " " + rawData[l2].ToString("X02");
                                }
                                if (lineStr.Length > 0)
                                {
                                    mLogLine(lineStr);
                                }
                            }
                            catch (Exception ex)
                            {
                                CProgram.sLogException("Scp reader: read section 6 log raw data from file" + AFilePath, ex);
                            }
                            #endregion
                        }

                        if (false == sections[2].exists() || hHuffmanTables == null)
                        {
                            #region section6 no section2
                            // UInt16* shortCaster = (UInt16*)rawData;
                            UInt32 data;
                            for (l2 = 0; l2 < sd.arraySize/*(sampleEnd[i] - sampleStart[i] + 1)*/; l2++)
                            {
                                if (l2 < (sd.leadLength / 2))
                                {
                                    data = ((UInt32)rawData[iRaw++]) & 0x00FF;
                                    data |= (((UInt32)rawData[iRaw++]) & 0x00FF) << 8;

                                    if (0 != (data & 0x00008000))
                                    {
                                        data |= 0xFFFF0000; // sign extend
                                    }
                                    sd.dataArray[l2] = data;
                                }
                                else
                                {
                                    sd.dataArray[l2] = 0;
                                }
                            }
                            #endregion
                        }
                        else if (19999 == huffmanTableCount)
                        {
                            // default huffman code
                            //public static short[] InhouseDecode(byte[] buffer, int offset, int nrbytes, int length, byte difference);
                            try
                            {
                                uint sdlLeadLength = sd.leadLength;
                                UInt32 size = sampleEnd[i] - sampleStart[i] + 1;
                                uint maskSign = 0xFFFF0000;

                                short[] decoded = mInhouseDecode(rawData, 0, (int)sdlLeadLength, (int)size, (byte)encoding);
                                for (l2 = 0; l2 < sd.arraySize /*(sampleEnd[i] - sampleStart[i] + 1)*/; l2++)
                                {
                                    if (l2 < sd.dataArray.Length)
                                    {
                                        int data = decoded[l2];

                                        //                                      if (0 != (data & 0x00008000))
                                        {
                                            //                                            data |= (int)(maskSign); // sign extend
                                        }
                                        sd.dataArray[l2] = (uint)data;
                                    }
                                    else
                                    {
                                        sd.dataArray[l2] = 0;
                                    }
                                    //                                    leads[loper] = tables.Decode(_Data[loper], 0, _Data[loper].Length, (ushort)((length * 1000) / _TimeInterval), _Difference);
                                }
                            }
                            catch (Exception ex)
                            {
                                CProgram.sLogException("Scp reader: read section 6 decode standard huffman from file" + AFilePath, ex);
                            }
                        }
                        else
                        {
                            #region section6 with section2 present
                            UInt16 currentBit = 8;
                            UInt32 currentByte = 0;
                            UInt32 currentTable = 0;
                            UInt32 dataValue;
                            UInt32 totalBits, prefixBits;

                            for (l2 = 0; l2 < sd.arraySize /*(sampleEnd[i] - sampleStart[i] + 1)*/; l2++)
                            {
                                UInt32 k = 0;
                                UInt32 prefixValue = 0, bits = 0;
                                bool matchFound = false;
                                // Match the next code in the bitstream to our Huffman
                                // table from Section 2
                                #region section6 Match huffmancode
                                while (!matchFound)
                                {
                                    if (prefix_min_bits == prefix_max_bits)
                                    {
                                        if (0 == bits)
                                        {
                                            currentByte = get_bits(out prefixValue, prefix_max_bits,
                                                rawData, currentByte, ref currentBit, 0);
                                            bits += prefix_max_bits;
                                        }
                                        else break;
                                    }
                                    else
                                    {
                                        currentByte = get_bits(out prefixValue, 1,
                                            rawData, currentByte, ref currentBit, 0);
                                        bits += 1;
                                        //                                        if (bits > prefix_max_bits) break;
                                        if (bits >= prefix_max_bits)
                                        {
                                            bits = prefix_max_bits;
                                            break;
                                        }
                                    }

                                    if ((bits >= prefix_min_bits) && (bits <= prefix_max_bits))
                                    {
                                        HuffmanTable ht = hHuffmanTables[currentTable];
                                        for (k = 0; k < ht.codeCount; k++)
                                        {
                                            if (0 == ht.prefixBits[k])
                                            {
                                                matchFound = true;
                                                break;
                                            }
                                            else if (ht.prefixBits[k] == bits)
                                            {
                                                if (ht.baseCode[k] == prefixValue)
                                                {
                                                    matchFound = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if (currentByte > sd.leadLength)
                                    {
                                        sampleEnd[i] = l2 + sampleStart[i];
                                        break;
                                    }
                                }
#if SCP_LOG
                if(l == (sampleEnd[i] - sampleStart[i])){
                  cout + "-" + i + "-Sample: " + l 
                       + ", currentByte: " + ((int64_t)currentByte - (int64_t)rawData)
                       + ", currentBit: " + (Int32)currentBit );
                }
#endif
                                #endregion
                                // SwitchByte == 1 means we decode a value
                                if (hHuffmanTables[currentTable].switchByte != null)
                                {
                                    try
                                    {
                                        HuffmanTable ht = hHuffmanTables[currentTable];

                                        if (k >= ht.codeCount)
                                        {
                                            //                                            CProgram.sLogError("k=" + k.ToString() + " >= " + ht.codeCount.ToString());
                                            k = ht.codeCount - 1;
                                        }
                                        totalBits = ht.totalBits[k];
                                        prefixBits = ht.prefixBits[k];

                                        currentByte = get_bits(out dataValue,
                                            (UInt16)(totalBits > prefixBits ? totalBits - prefixBits : 0),
                                            rawData, currentByte, ref currentBit, 1);
                                        sd.dataArray[l2] = dataValue;
                                    }
                                    catch (Exception ex)
                                    {
                                        CProgram.sLogException("Scp reader: read section 6 decode huffman from file" + AFilePath, ex);
                                    }

                                }
                                // SwitchByte == 0 means we switch to a different
                                // table. (We don't use this on the Aera CT).
                                else
                                {
                                    // This is where we would switch tables
                                }
#if SCP_LOG
                if(l == (sampleEnd[i] - sampleStart[i])){ cout + "-" + i + "-Sample: " + l + ", currentByte: " + ((int64_t)currentByte - (int64_t)rawData) + ", currentBit: " + (Int32)currentBit );}
#endif
                            }
                            #endregion
#if SCP_LOG
              cout + "---Data Length (Ch " + i + "): " + ((int64_t) currentByte - (int64_t) rawData) + " != " + leadLength[i] );cout + "+++Diff: " + leadLength[i] - ((int64_t) currentByte - (int64_t) rawData)  );
#endif
                        }

                        if (bLogData)
                        {
                            #region logData2
                            int iData;
                            try
                            {
                                mLogLine("ArrayData: Lead " + i.ToString() + " size= " + sd.leadLength);
                                lineStr = "";
                                uint nLog = sd.arraySize < 1600 ? sd.arraySize : 1600;
                                for (l2 = 0; l2 < nLog; l2++)
                                {
                                    if (l2 % 16 == 0)
                                    {
                                        if (lineStr.Length > 0)
                                        {
                                            mLogLine(lineStr);
                                        }
                                        lineStr = l2.ToString("X04") + ":";
                                    }
                                    iData = (int)sd.dataArray[l2];
                                    lineStr += " " + iData.ToString("D4");
                                }
                                if (lineStr.Length > 0)
                                {
                                    mLogLine(lineStr);
                                }
                            }
                            catch (Exception ex)
                            {
                                CProgram.sLogException("Scp reader: read section 6 log dataArray from file" + AFilePath, ex);
                            }
                            #endregion
                        }

                        rawData = null;
                    }
                    mLogLine("Processing Section 6");
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Scp reader: read section 6 from file" + AFilePath, ex);
                }
                try
                {
                    #region remark
                    /*
                       cout + "Amplitude multiplier: " + ampMult + " (nV/count)" );
                       cout + "---ADC Gain: " + (1000000./(float)ampMult) + " (counts/mV)" );
                       cout + "Sample Period: " + samplePeriod + " (us)" );
                       cout + "---Sample Rate: " + (1000000/samplePeriod) + " (Hz)" );
                       cout + "Data encoding: " + encoding );
                       if(encoding == 0) cout + "---Real data" );
                       else if(encoding == 1) cout + "---First difference data" );
                       else if(encoding == 2) cout + "---Second difference data" );
                       else cout + "---Unknown data" );
                       cout + "Compression: " + compression
                       + (compression?" (Bimodal compression)":" (No compression)") );
                       */
                    #endregion
                    mSignalUnit = 1e-6F * ampMult;        // amplitude multiplier is in nV / unit => mV/unit
                    mTimeUnit = 1e-6F * samplePeriod;       // samplePeriod is in uS / unit => S/unit
                    mSampleFrequency = samplePeriod != 0 ? 1e6F / samplePeriod : 0.0F;
                    mNrSignals = (UInt16)leadCount;
                    mSignals = new CSignalData[mNrSignals];
                    #region write csv

                    StreamWriter outFile = null;
                    string outName = Path.ChangeExtension(AFilePath, "csv");
                    if (AbWriteCsv)
                    {
                        outFile = new StreamWriter(outName);
                        mLogLine("Creating File: " + Path.GetFileName(outName));

                        outFile.WriteLine("Sample\tiCH1\tiCH2\tiCH3\tCH1\tCH2\tCH3");
                    }
                    Int32[] prevD1 = new Int32[leadCount];
                    Int32[] prevD2 = new Int32[leadCount];
                    UInt32 maxSamples = 0, l;
                    UInt32 nSamples;

                    for (i = 0; i < leadCount; i++)
                    {
                        nSamples = sampleEnd[i];
                        //cout + "Samples (" + i + "): " + sampleEnd[i] );
                        if (nSamples > maxSamples) maxSamples = nSamples;
                    }

                    for (i = leadCount; --i > 0;)
                    {
                        nSamples = sampleEnd[i];
                        if (nSamples > 1)
                        {
                            break;
                        }
                        // remove empty leads (TZ Clarus gives 8 leads even with 2 wires
                        --leadCount;
                    }

                    for (i = 0; i < leadCount; i++)
                    {
                        mSignals[i] = new CSignalData();
                        mSignals[i].mbCreateData(maxSamples);
                        prevD1[i] = 0;
                        prevD2[i] = 0;
                    }
                    mNrSamples = maxSamples;
                    mNrSignals = (UInt16)leadCount;

                    float f, t;
                    string line1, line2;
                    uint iStore;
                    for (l = 0; l < maxSamples; l++)
                    {
                        t = l * mTimeUnit;
                        line1 = l.ToString();
                        line2 = "\t" + t.ToString();
                        for (i = 0; i < leadCount; i++)
                        {
                            UInt32 relSample = 0;
                            if ((sampleStart[i] - 1) <= l) relSample = 1 + l - sampleStart[i];
                            if (((sampleStart[i] - 1) <= l) && ((sampleEnd[i] - 1) >= l))
                            {
                                Int32 tempData = (Int32)(sda[i].dataArray[l + sampleStart[i] - 1]);
                                if (tempData > 0x08000)
                                {
                                    tempData = -tempData & 0x007FFF;
                                }

                                Int32 outputData = 0;
                                if (encoding == 1)
                                {   // First differences
                                    outputData = (relSample == 0) ? tempData : prevD1[i] + tempData;
                                    prevD1[i] = outputData;
                                }
                                else if (encoding == 2)
                                {   // Second Differences
                                    outputData = (relSample < 2) ? tempData : tempData + (2 * prevD1[i]) - prevD2[i];
                                    prevD2[i] = prevD1[i];
                                    prevD1[i] = outputData;
                                }
                                else
                                {
                                    outputData = tempData;
                                }
                                if (mbFlipSignal) outputData = -outputData;
                                iStore = mChannelNr(i, leadID[i]);
                                mSignals[iStore].mAddValue(outputData);
                                line1 += "\t" + outputData.ToString();
                                f = outputData * mSignalUnit;
                                line2 += "\t" + f.ToString();
                            }
                        }
                        if (outFile != null)
                        {
                            outFile.WriteLine(line1 + line2);
                        }
                    }
                    if (outFile != null)
                    {
                        outFile.Close();
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Scp reader: process section 6 from file" + AFilePath, ex);
                }

                #endregion

                #region clean up 
                /*            if (sections[2].exists())
                            {
                                UInt32 l;
                                for (l = 0; l < tableCount; l++)
                                {
                                    delete[] prefixBits[l];
                                    delete[] totalBits[l];
                                    delete[] switchByte[l];
                                    delete[] baseValue[l];
                                    delete[] baseCode[l];
                                }
                                delete[] codeCount;
                                delete[] prefixBits;
                                delete[] totalBits;
                                delete[] switchByte;
                                delete[] baseValue;
                                delete[] baseCode;
                            }

                            if (sections[3].exists())
                            {
                                delete[] sampleStart;
                                delete[] sampleEnd;
                                delete[] leadID;
                            }
                            if (sections[6].exists())
                            {
                                delete[] leadLength;
                                UInt32 l;
                                for (l = 0; l < leadCount; l++)
                                {
                                    delete[] dataArrays[l];
                                }
                                delete[] dataArrays;
                            }
                */
                #endregion
                #endregion
                mLogLine("Done.");                 // Signal completion of program


                float tStrip = mNrSamples * mTimeUnit;
                readResult += " " + mNrSignals + "x" + tStrip.ToString("0.00") + "sec@" + mSampleFrequency + "Sps";
                if( AbLogResult)
                {
                    CProgram.sLogLine(readResult);
                }
                return true;
            }
            if (AbLogResult)
            {
                CProgram.sLogLine("No data in file " + fileName);
            }
            mLogError("No data in file " + fileName);
            return false;
        }

        private uint mChannelNr(uint AIndex, uint ASignalCode)
        {
            uint iStore = AIndex;

            switch (ASignalCode)
            {
                case 1: iStore = 0; break;  // lead I
                case 2: iStore = 1; break; // lead II
                case 3: iStore = 6; break; // lead V1
                case 4: iStore = 7; break; // lead V2
                case 5: iStore = 8; break; // lead V3
                case 6: iStore = 9; break; // lead V4
                case 7: iStore = 10; break; // lead V5
                case 8: iStore = 11; break; // lead V6
                case 61: iStore = 2; break; // lead III
                case 62: iStore = 3; break; // lead aVR
                case 63: iStore = 4; break; // lead aVL
                case 64: iStore = 5; break; // lead aVF
            }
            return iStore;
        }

        public static CRecordMit sReadOneScp(string AFilePath, UInt32 ASetInvChannels, CSqlDBaseConnection ADbConnection, Int16 ADefaultTimeZoneOffsetMin,
            DImportConverter AImportConverter, bool AbApplyTimeZoneCor, bool AbLogResult)
        {
            CRecordMit rec = null;

            try
            {
                string fileName = Path.GetFileName(AFilePath);
                CScpReader scp = new CScpReader();

                if (scp != null)
                {
                    bool bOk = true;

                    if (scp.mbReadFile(AFilePath, false, ADefaultTimeZoneOffsetMin, AbApplyTimeZoneCor, AbLogResult))
                    {
                        rec = new CRecordMit(ADbConnection);

                        if (rec != null)
                        {
                            rec.mTimeZoneOffsetMin = scp.mTimeZoneOffsetMin; //sv20190327 ADefaultTimeZoneOffsetMin;
                            if (rec.mbCreateSignals(scp.mNrSignals)
                                && rec.mbCreateAllSignalData(scp.mSignals[0].mNrValues))
                            {
                                for (ushort j = 0; j < scp.mNrSignals; ++j)
                                {
                                    bOk &= rec.mSignals[j].mbCopyFrom(scp.mSignals[j]);
                                }
                                if (bOk == false)
                                {
                                    rec = null;
                                }
                            }
                            else
                            {
                                rec = null;
                            }
                        }
                        bOk = rec != null;
                        if (bOk)
                        {
                            DateTime utc = scp.mMeasureUTC;

                            if (utc == DateTime.MinValue)
                            {
                                utc = DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc);
                            }
                            // convert Csp to recordMIT
                            rec.mSampleFrequency = (uint)scp.mSampleFrequency;
                            rec.mSampleUnitT = scp.mTimeUnit;             // 1 / mFrequency
                            rec.mSampleUnitA = scp.mSignalUnit;            // 1 / mAdcGain
                            rec.mNrSamples = scp.mNrSamples;                // nr sample values in the file
                                                                            //private CAnnotation mAnnotationAtr;     // annotation file

                            // From Header in dBase 
                            rec.mReceivedUTC = DateTime.UtcNow;           // received time = read time
                            rec.mFileName = fileName;
                            //rec.mStoredAt = AStoreName;
                            rec.mBaseUTC = utc;                // start time of strip (functions are given in sec. from this point
                            rec.mRecDurationSec = scp.mSignals[0].mNrValues * scp.mTimeUnit;
                            rec.mEventUTC = utc;              // Event time
                            rec.mTimeZoneOffsetMin = scp.mTimeZoneOffsetMin;
                            rec.mEventTimeSec = 0.0F;            // event time relative to base
                            rec.mImportConverter = (UInt16)AImportConverter;         // import converter enum
                                                                                     //                        rec.mImportMethod = (UInt16)AImportMethod;            // Import method enum
                            rec.mSignalLabels = "";

                            rec.mbReadUTC = scp.mbReadDT;
                            rec.mbReadDT = scp.mbReadDT;
                            rec.mbReadTZ = scp.mbReadTZ;

                            //
                            if (scp.mDeviceID != null && scp.mDeviceID.Length > 0 ) //&& scp.mDeviceID.ToLower() != "unknown")
                            {
                                rec.mDeviceID = scp.mDeviceID;
                            }
                            else
                            {
                                string devID = rec.mDeviceID;   // need value

                                if (devID == null || devID.Length == 0)
                                {
                                    devID = "unknown";
                                }
                                rec.mDeviceID = devID;
                            }
                            if (scp.mPatientID != null && scp.mPatientID.Length > 0)
                            {
                                rec.mPatientID.mbEncrypt(CProgram.sTrimString(scp.mPatientID));
                            }
                            else if (scp.mReferal != null && scp.mReferal.Length > 0)
                            {
                                rec.mPatientID.mbEncrypt(CProgram.sTrimString(scp.mReferal));
                            }
                            if (scp.mReferringPhysician != null && scp.mReferringPhysician.Length > 0)
                            {
                                rec.mPhysicianName.mbEncrypt(CProgram.sTrimString(scp.mReferringPhysician));
                            }

                            string totalName = CRecordMit.sCombineNames(scp.mPatientLastName, "", scp.mPatientFirstName);

                            rec.mPatientTotalName.mbEncrypt(totalName);
                            if (scp.mDateOfBirth > 0)
                            {
                                rec.mPatientBirthDate.mEncrypt((int)scp.mDateOfBirth);
                                rec.mPatientBirthYear = CProgram.sGetYear(scp.mDateOfBirth);
                            }
                            if (scp.mSex > 0)
                            {
                                rec.mPatientGender = scp.mSex;
                            }
                            if (scp.mConfirmingPhysician != null && scp.mConfirmingPhysician.Length > 0)
                            {
                                rec.mPhysicianName.mbEncrypt(scp.mConfirmingPhysician);
                            }
                            if (scp.mFreeText != null && scp.mFreeText.Length > 0)
                            {
                                rec.mRecRemark.mbEncrypt(scp.mFreeText);
                            }

                            //rec.mEventTypeString;
                            rec.mStudyStartRecUTC = DateTime.MinValue; // no study info utc;

                            //rec.mTransmitTimeUTC;
                            //rec.mTransmitTimeLocal;
                            rec.mModifyInvertMask(ASetInvChannels);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                CProgram.sLogException("Failed import Scp file", e);
                rec = null;

            }
            return rec;
        }

        public static CRecordMit sReadAllScp(string APath, UInt32 ASetInvChannels, CSqlDBaseConnection ADbConnection, Int16 ADefaultTimeZoneMin,
            ref DateTime ArSampleUTC, string AEventFileIndexName, UInt32 AEventSampleIndex, bool AbApplyTimeZoneCor, bool AbLogResult)  // combines SCP files to one file
        {
            CRecordMit rec = null;

            try
            {
                List<CScpReader> list = new List<CScpReader>();
                List<string> names = new List<string>();

                if (list != null)
                {
                    DateTime utcNow = DateTime.UtcNow;

                    string[] files = System.IO.Directory.GetFiles(APath);

                    int nMaxFillGap = 20;

                    float sampleTime = 0;
                    UInt32 nSamples = 0;
                    UInt16 nSignals = 0;
                    string firstName = "";
                    float tTotal = 0, tStrip;

                    bool bTestSample = ArSampleUTC != null && AEventFileIndexName != null && AEventFileIndexName.Length > 0;

 // read the files with the same name.
                    foreach (string s in files)                     // copy 
                    {
                        if (s.EndsWith(".scp"))
                        {
                            CScpReader scp = new CScpReader();

                            if (scp != null)
                            {
                                if (scp.mbReadFile(s, false, ADefaultTimeZoneMin, AbApplyTimeZoneCor, AbLogResult))
                                {
                                    int n = list.Count;

                                    tStrip = scp.mTimeUnit * scp.mNrSamples;

                                    if (scp.mMeasureUTC == DateTime.MinValue) { scp.mMeasureUTC = DateTime.SpecifyKind(utcNow, DateTimeKind.Utc); }

                                    if (n == 0)
                                    {
                                        firstName = Path.GetFileName(s);
                                        list.Add(scp);
                                        names.Add(firstName);
                                        sampleTime = scp.mTimeUnit; // just asume all sample times are the same
                                        nSamples = scp.mNrSamples;
                                        nSignals = scp.mNrSignals;
                                        tTotal += tStrip;
                                    }
                                    else
                                    {
                                        string name = Path.GetFileName(s); 
                                        if (nSignals != scp.mNrSignals)
                                        {
                                            CProgram.sLogLine("TZe incompatibel nrSignals in " + name);
                                        }                               
                                        else if (Math.Abs(sampleTime - scp.mTimeUnit) > 0.001)
                                        {
                                            CProgram.sLogLine("TZe incompatibel sampleTime in " + name);
                                        }
                                        else
                                        {
                                            int insertAt = n;

                                            if (scp.mFileTZSeqNr > 0)
                                            {
                                                // sort by sequence nr if there is a TZ sequence number extrected from file
                                                for (int i = 0; i < n; ++i)
                                                {
                                                    if (scp.mFileTZSeqNr < list[i].mFileTZSeqNr) //compare before
                                                    {
                                                        insertAt = i;
                                                        break;
                                                    }
                                                    else if(scp.mFileTZSeqNr == list[i].mFileTZSeqNr)
                                                    {
                                                        if( scp.mFileName.CompareTo( list[i].mFileName )> 0)
                                                        {
                                                            // when same sequence number put the more recent file first 
                                                            // <snr>_<seqNr>_YYYYMMDDHHSS.scp
                                                            insertAt = i;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            // put in list sorted by sequence number
                                            if (insertAt < n)
                                            {
                                                list.Insert(insertAt, scp);
                                                names.Insert(insertAt, name);
                                            }
                                            else
                                            {
                                                list.Add(scp);
                                                names.Add(name);
                                            }
                                            nSamples += scp.mNrSamples;
                                            tTotal += tStrip;
                                        }
                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                    int nFiles = list.Count;

                    if (nFiles > 0 && nSignals > 0 && nSamples > 0 && sampleTime > 0.001F)
                    {
                        rec = new CRecordMit(ADbConnection);

                        if (rec != null)
                        {
                            rec.mTimeZoneOffsetMin = ADefaultTimeZoneMin;
                            DateTime startUTC = list[0].mMeasureUTC;
                            double sampleUnitSec = list[0].mTimeUnit;
                            UInt32 samplesPerFile = list[0].mNrSamples;

                            CScpReader scp = list[nFiles - 1];

                            DateTime endUTC = scp.mMeasureUTC.AddSeconds(scp.mNrSamples * sampleTime);
                            double durationUTCSec = (endUTC - startUTC).TotalSeconds;
                            int maxDuration = 2 * 24 * 3600;
                            bool bLogInfo = CLicKeyDev.sbDeviceIsProgrammer();
                            UInt32 nRecSamples = 0;

                            UInt32 lastSeqNr = UInt32.MaxValue;

                            string fileInfo = "";
                            for (UInt16 f = 0; f < nFiles; ++f) // merge files
                            {
                                scp = list[f];
                                tStrip = scp.mNrSamples * scp.mTimeUnit;
                                fileInfo += f.ToString() + ": " + names[f] + " " + scp.mFileTZSeqNr.ToString("000000")
                                    + " " + CProgram.sDateTimeToYMDHMS(scp.mMeasureUTC) + " " + scp.mNrSignals.ToString()
                                    + " " + tStrip.ToString("0.000") + " sec " + scp.mNrSamples.ToString() + " x " + scp.mTimeUnit.ToString("0.00000");


                                if (scp.mFileTZSeqNr == 0)
                                {
                                    nRecSamples += scp.mNrSamples;
                                }
                                else if ( f == 0 ) // first
                                {
                                    nRecSamples += scp.mNrSamples;
                                }
                                else // 
                                {
                                    int delta = (int)scp.mFileTZSeqNr - (int)lastSeqNr;

                                    if( delta == 0 )
                                    {
                                        fileInfo += " Skip same";
                                        scp.mFileName = "$" + scp.mFileName;    // mark scp file for skip
                                    }
                                    else if( delta > 1 )
                                    {
                                        int nFill = (delta <= nMaxFillGap ? delta : nMaxFillGap)-1;
                                        fileInfo += " Gap " + (delta-1).ToString() + " files, fill " + nFill.ToString() + "!";
                                        nRecSamples += (UInt32)( nFill * samplesPerFile) + scp.mNrSamples;

                                    }
                                    else if (delta < 0)
                                    {
                                        fileInfo += " Skip Before " + (-delta).ToString() + " files!";
                                        scp.mFileName = "$" + scp.mFileName;    // mark scp file for skip
                                    }
                                    else //delta == 1 as expected
                                    {
                                        nRecSamples += scp.mNrSamples;
                                    }

                                }
                                fileInfo += "\r\n";
                                lastSeqNr = scp.mFileTZSeqNr;
                            }
                            UInt32 index = 0;
                            UInt32 iStart;
                            double durationSec = nRecSamples * sampleUnitSec;
                            float diffSec = (float)durationSec - tTotal;
                            double difUTC = (float)durationSec - durationUTCSec;
                            fileInfo += "total = " + nRecSamples + " samples " + tTotal.ToString("0.000") + " sec, duration = " + durationSec.ToString("0.000") 
                                + " sec, diff = " + diffSec.ToString("0.000") + "sec, diffUTC = " + difUTC.ToString( "0.000" ) + " sec";

                            if (bLogInfo || diffSec < nFiles || durationSec >= maxDuration)
                            {
                                CProgram.sLogInfo(fileInfo);
                            }
                            if (rec.mbCreateSignals(nSignals)
                                && durationSec < maxDuration     // if file is bigger then 24 hours something is wrong
                                && rec.mbCreateAllSignalData(nRecSamples))
                            {
                                lastSeqNr = UInt32.MaxValue;

                                rec.mDeviceID = scp.mDeviceID;

                                // fill RecordMit with scp values
                                rec.mSampleFrequency = (uint)scp.mSampleFrequency;
                                rec.mSampleUnitT = scp.mTimeUnit;             // 1 / mFrequency
                                rec.mSampleUnitA = scp.mSignalUnit;            // 1 / mAdcGain
                                rec.mNrSamples = nRecSamples;                // adjust later nr sample values in the file

                                // just fill in known values
                                rec.mReceivedUTC = utcNow;           // received time = read time
                                rec.mFileName = firstName;
                                rec.mStoredAt = "";
                                rec.mBaseUTC = startUTC;                // start time of strip (functions are given in sec. from this point
                                rec.mEventUTC = startUTC;              // Event time
                                rec.mEventTimeSec = 0;            // event time relative to base
                                rec.mImportConverter = (UInt16)DImportConverter.TZAeraScp;         // import converter enum
                                rec.mImportMethod = (UInt16)DImportMethod.Unknown;            // Import method enum

                                rec.mbReadUTC = scp.mbReadDT;
                                rec.mbReadDT = scp.mbReadDT;
                                rec.mbReadTZ = scp.mbReadTZ;

                                rec.mSignalLabels = "";
                                rec.mDeviceID = scp.mDeviceID;
                                rec.mPatientID.mbEncrypt(CProgram.sTrimString(scp.mPatientID));
                                rec.mDeviceModel = scp.mDeviceManufact == null || scp.mDeviceManufact.Length == 0 ? "TZe" : scp.mDeviceManufact;

                                string totalName = CRecordMit.sCombineNames(scp.mPatientLastName, "", scp.mPatientFirstName);

                                rec.mPatientTotalName.mbEncrypt(totalName);
                                //rec.mPatientBirthDate;
                                //rec.mPhysicianName;
                                //rec.mPatientComment;
                                //rec.mEventTypeString;
                                rec.mStudyStartRecUTC = DateTime.MinValue; // no start of study  startUTC;
                                rec.mRecDurationSec = nRecSamples * sampleTime;
                                rec.mTimeZoneOffsetMin = scp.mTimeZoneOffsetMin;
                                //rec.mTransmitTimeUTC;
                                //rec.mTransmitTimeLocal;
                                // rec.mPatientBirthYear;
                                //                            rec.mRecState = (UInt16)DRecState.Received

                               Int32 toggleValue = rec.mSampleUnitA > 0.00001 ? (Int32)( 1 / rec.mSampleUnitA / 10 ): 10;

                                if (toggleValue < 1) toggleValue = 1;

                                bool bFilter = CTzQuickFilter.sTzQuickFilters != null && CTzQuickFilter.sTzQuickFilters.Count > 0;
                                int value = 0;
                                rec.mNrSamples = 0; // determin max nr samples

                                {
                                    for (ushort j = 0; j < nSignals; ++j)
                                    {
                                        CTzQuickFilter.sDoResetFilter();
                                        index = 0;

                                        for (UInt16 f = 0; f < nFiles; ++f) // merge files
                                        {
                                            scp = list[f];

                                            if (scp.mFileName.StartsWith("$"))
                                            {
                                                // skip file
                                            }
                                            else
                                            {
                                                if (scp.mFileTZSeqNr != 0 && f > 0)
                                                {
                                                    int delta = (int)scp.mFileTZSeqNr - (int)lastSeqNr;

                                                    if (delta == 0)
                                                    {
                                                        CProgram.sLogError(f.ToString() + " same should have been skipped " + scp.mFileName);
                                                    }
                                                    else if (delta > 1)
                                                    {
                                                        int nFill = (delta <= nMaxFillGap ? delta : nMaxFillGap) - 1;
                                                        int nFillPoints = (int)(nFill * samplesPerFile);

                                                        while (nFillPoints > 0)
                                                        {
                                                            // fill missing with latest value with a squere +/- 0.1 mV at frequency sample rate / 4
                                                            Int32 v = value + ((index & 2) == 0 ? toggleValue : -toggleValue);
                                                            rec.mSignals[j].mAddValue(v);
                                                            if (bFilter)
                                                            {
                                                                value = CTzQuickFilter.sDoQuickFilter(value);   //fill filter with same value in time but do not effect square
                                                            }
                                                            ++index;
                                                            --nFillPoints;
                                                        }
                                                    }
                                                    else if (delta < 0)
                                                    {
                                                        CProgram.sLogError(f.ToString() + " before should have been skipped " + scp.mFileName);
                                                    }
                                                    else //delta == 1 as expected
                                                    {
                                                    }
                                                }
                                                if (bTestSample && scp.mFileName.StartsWith(AEventFileIndexName) && j == 0)
                                                {
                                                    // set Event time by sample in given file
                                                    float relEvent = AEventSampleIndex * scp.mTimeUnit;
                                                    float relStrip = scp.mNrSamples * scp.mTimeUnit;
                                                    ArSampleUTC = scp.mMeasureUTC.AddSeconds(relEvent);
                                                    string line = "TZ file[" + f.ToString() + "] event at " + AEventSampleIndex.ToString()
                                                        + ",  " + relEvent.ToString("0.000") + " sec UTC "
                                                        + CProgram.sDateTimeToYMDHMS(ArSampleUTC) + " in " + AEventFileIndexName
                                                        + "( " + scp.mNrSamples.ToString() + ",  " + relStrip.ToString() + " sec)";
                                                    CProgram.sLogLine(line);
                                                }
                                                for (int i = 0; i < scp.mNrSamples; ++i)
                                                {
                                                    value = scp.mSignals[j].mValues[i];
                                                    if (bFilter)
                                                    {
                                                        value = CTzQuickFilter.sDoQuickFilter(value);
                                                    }
                                                    rec.mSignals[j].mAddValue(value);
                                                    ++index;
                                                }
                                            }
                                            lastSeqNr = scp.mFileTZSeqNr;

                                        }
                                        if( index > rec.mNrSamples) rec.mNrSamples = index;  // nr sample values in the combined file

                                        if (index != nRecSamples)
                                        {
                                            CProgram.sLogError("ReadAllScp signal[" + j.ToString() + "] points not filled " + (index - nRecSamples).ToString());
                                        }
                                    }
                                }
                            }
                            else
                            {
                                rec = null;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                CProgram.sLogException("Failed import all Scp file", e);
                rec = null;
            }
            if( rec != null ) rec.mModifyInvertMask(ASetInvChannels);
            return rec;
        }

        public static CRecordMit sReadAllScpByTime(string APath, UInt32 ASetInvChannels, CSqlDBaseConnection ADbConnection, Int16 ADefaultTimeZoneMin,
            ref DateTime ArSampleUTC, string AEventFileIndexName, UInt32 AEventSampleIndex, bool AbApplyTimeZoneCor, bool AbLogResult)  // combines SCP files to one file
        {
            CRecordMit rec = null;

            try
            {
                List<CScpReader> list = new List<CScpReader>();
                List<string> names = new List<string>();

                if (list != null)
                {
                    DateTime utcNow = DateTime.UtcNow;

                    string[] files = System.IO.Directory.GetFiles(APath);

                    float sampleTime = 0;
                    UInt32 nSamples = 0;
                    UInt16 nSignals = 0;
                    string firstName = "";
                    float tTotal = 0, tStrip;

                    bool bTestSample = ArSampleUTC != null && AEventFileIndexName != null && AEventFileIndexName.Length > 0;

                    // read the files with the same name.
                    foreach (string s in files)                     // copy 
                    {
                        if (s.EndsWith(".scp"))
                        {
                            CScpReader scp = new CScpReader();

                            if (scp != null)
                            {
                                if (scp.mbReadFile(s, false, ADefaultTimeZoneMin, AbApplyTimeZoneCor, AbLogResult))
                                {
                                    int n = list.Count;

                                    tStrip = scp.mTimeUnit * scp.mNrSamples;

                                    if (scp.mMeasureUTC == DateTime.MinValue) { scp.mMeasureUTC = DateTime.SpecifyKind(utcNow, DateTimeKind.Utc); }

                                    if (bTestSample && Path.GetFileName(s).StartsWith(AEventFileIndexName))
                                    {
                                        ArSampleUTC = scp.mMeasureUTC.AddSeconds(AEventSampleIndex * scp.mTimeUnit);
                                    }
                                    if (n == 0)
                                    {
                                        firstName = Path.GetFileName(s);
                                        list.Add(scp);
                                        names.Add(firstName);
                                        sampleTime = scp.mTimeUnit; // just asume all sample times are the same
                                        nSamples = scp.mNrSamples;
                                        nSignals = scp.mNrSignals;
                                        tTotal += tStrip;
                                    }
                                    else
                                    {
                                        string name = Path.GetFileName(s);
                                        if (nSignals != scp.mNrSignals)
                                        {
                                            CProgram.sLogLine("TZe incompatibel nrSignals in " + name);
                                        }
                                        else if (Math.Abs(sampleTime - scp.mTimeUnit) > 0.001)
                                        {
                                            CProgram.sLogLine("TZe incompatibel sampleTime in " + name);
                                        }
                                        else
                                        {
                                            int insertAt = n;
                                            for (int i = 0; i < n; ++i)
                                            {
                                                if (scp.mMeasureUTC < list[i].mMeasureUTC) //compare before
                                                {
                                                    insertAt = i;
                                                    break;
                                                }
                                            }
                                            // put in list sorted by start strip time
                                            if (insertAt < n)
                                            {
                                                list.Insert(insertAt, scp);
                                                names.Insert(insertAt, name);
                                            }
                                            else
                                            {
                                                list.Add(scp);
                                                names.Add(name);
                                            }
                                            nSamples += scp.mNrSamples;
                                            tTotal += tStrip;
                                        }
                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                    int nFiles = list.Count;

                    if (nFiles > 0 && nSignals > 0 && nSamples > 0 && sampleTime > 0.001F)
                    {
                        rec = new CRecordMit(ADbConnection);

                        if (rec != null)
                        {
                            rec.mTimeZoneOffsetMin = ADefaultTimeZoneMin;
                            DateTime startUTC = list[0].mMeasureUTC;

                            CScpReader scp = list[nFiles - 1];

                            DateTime endUTC = scp.mMeasureUTC.AddSeconds(scp.mNrSamples * sampleTime);
                            double durationSec = (endUTC - startUTC).TotalSeconds;
                            int maxDuration = 2 * 24 * 3600;
                            UInt32 nRecSamples = (UInt32)(durationSec / sampleTime + 0.99F);
                            UInt32 index = 0;
                            UInt32 iStart;
                            bool bLogInfo = CLicKeyDev.sbDeviceIsProgrammer();

                            string fileInfo = "";
                            for (UInt16 f = 0; f < nFiles; ++f) // merge files
                            {
                                scp = list[f];
                                tStrip = scp.mNrSamples * scp.mTimeUnit;
                                fileInfo += f.ToString() + ": " + names[f] + " " + CProgram.sDateTimeToYMDHMS(scp.mMeasureUTC) + " " + scp.mNrSignals.ToString()
                                    + " " + tStrip.ToString("0.000") + " sec " + scp.mNrSamples.ToString() + " x " + scp.mTimeUnit.ToString("0.00000") + "\r\n";

                            }
                            float diffSec = (float)durationSec - tTotal;
                            fileInfo += "total = " + tTotal.ToString("0.000") + " sec, duration = " + durationSec.ToString("0.000") + " sec, diff = " + diffSec.ToString("0.000") + "sec";
                            if (bLogInfo || diffSec < nFiles || diffSec > nFiles || durationSec >= maxDuration)
                            {
                                CProgram.sLogInfo(fileInfo);
                            }
                            if (rec.mbCreateSignals(nSignals)
                                && durationSec < maxDuration     // if file is bigger then 24 hours something is wrong
                                && rec.mbCreateAllSignalData(nRecSamples))
                            {
                                rec.mDeviceID = scp.mDeviceID;

                                // fill RecordMit with scp values
                                rec.mSampleFrequency = (uint)scp.mSampleFrequency;
                                rec.mSampleUnitT = scp.mTimeUnit;             // 1 / mFrequency
                                rec.mSampleUnitA = scp.mSignalUnit;            // 1 / mAdcGain
                                rec.mNrSamples = nRecSamples;                // adjust later nr sample values in the file

                                // just fill in known values
                                rec.mReceivedUTC = utcNow;           // received time = read time
                                rec.mFileName = firstName;
                                rec.mStoredAt = "";
                                rec.mBaseUTC = startUTC;                // start time of strip (functions are given in sec. from this point
                                rec.mEventUTC = startUTC;              // Event time
                                rec.mEventTimeSec = 0;            // event time relative to base
                                rec.mImportConverter = (UInt16)DImportConverter.TZAeraScp;         // import converter enum
                                rec.mImportMethod = (UInt16)DImportMethod.Unknown;            // Import method enum

                                rec.mbReadUTC = scp.mbReadDT;
                                rec.mbReadDT = scp.mbReadDT;
                                rec.mbReadTZ = scp.mbReadTZ;

                                rec.mSignalLabels = "";
                                rec.mDeviceID = scp.mDeviceID;
                                rec.mPatientID.mbEncrypt(CProgram.sTrimString(scp.mPatientID));
                                rec.mDeviceModel = scp.mDeviceManufact == null || scp.mDeviceManufact.Length == 0 ? "TZe" : scp.mDeviceManufact;

                                string totalName = CRecordMit.sCombineNames(scp.mPatientLastName, "", scp.mPatientFirstName);

                                rec.mPatientTotalName.mbEncrypt(totalName);
                                //rec.mPatientBirthDate;
                                //rec.mPhysicianName;
                                //rec.mPatientComment;
                                //rec.mEventTypeString;
                                rec.mStudyStartRecUTC = DateTime.MinValue; // no start of study  startUTC;
                                rec.mRecDurationSec = nRecSamples * sampleTime;
                                rec.mTimeZoneOffsetMin = scp.mTimeZoneOffsetMin;
                                //rec.mTransmitTimeUTC;
                                //rec.mTransmitTimeLocal;
                                // rec.mPatientBirthYear;
                                //                            rec.mRecState = (UInt16)DRecState.Received

                                Int32 toggleValue = rec.mSampleUnitA > 0.00001 ? (Int32)(1 / rec.mSampleUnitA / 10) : 10;

                                if (toggleValue < 1) toggleValue = 1;

                                bool bFilter = CTzQuickFilter.sTzQuickFilters != null && CTzQuickFilter.sTzQuickFilters.Count > 0;
                                int value = 0;
                                rec.mNrSamples = 0; // determin max nr samples

                                {
                                    for (ushort j = 0; j < nSignals; ++j)
                                    {
                                        CTzQuickFilter.sDoResetFilter();
                                        index = 0;

                                        for (UInt16 f = 0; f < nFiles; ++f) // merge files
                                        {
                                            scp = list[f];
                                            iStart = (UInt32)((scp.mMeasureUTC - startUTC).TotalSeconds / sampleTime + 0.3);

                                            if (iStart - index > (1.5F + f) / sampleTime)
                                            {
                                                // only fill in missing data if it is more than a second per file (scp time is in hole seconds)
                                                while (index < iStart)
                                                {
                                                    // fill missing with latest value with a squere +/- 0.1 mV at frequency sample rate / 4
                                                    Int32 v = value + ((index & 2) == 0 ? toggleValue : -toggleValue);
                                                    rec.mSignals[j].mAddValue(v);
                                                    ++index;
                                                }
                                            }
                                            for (int i = 0; i < scp.mNrSamples; ++i)
                                            {
                                                value = scp.mSignals[j].mValues[i];
                                                if (bFilter)
                                                {
                                                    value = CTzQuickFilter.sDoQuickFilter(value);
                                                }
                                                rec.mSignals[j].mAddValue(value);
                                                ++index;
                                            }
                                        }
                                        if (index > rec.mNrSamples) rec.mNrSamples = index;  // nr sample values in the combined file
                                    }
                                }
                            }
                            else
                            {
                                rec = null;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                CProgram.sLogException("Failed import all Scp file", e);
                rec = null;
            }
            if (rec != null) rec.mModifyInvertMask(ASetInvChannels);
            return rec;
        }
    }

    class CScpData
    {
        public UInt32 leadLength = 0;
        public UInt32 arraySize = 0;
        public UInt32[] dataArray;

        public void init(UInt32 ALeadLength, UInt32 AArraySize)
        {
            leadLength = ALeadLength;
            arraySize = AArraySize;
            dataArray = new UInt32[AArraySize];
        }
        public void dispose()
        {
            leadLength = 0;
            dataArray = null;
        }
    }

    class HuffmanTable
    {
        public UInt32[] prefixBits = null;
        public UInt32[] totalBits = null;
        public UInt32[] switchByte = null;
        public Int32[] baseValue = null;
        public UInt32[] baseCode = null;
        public UInt32 codeCount = 0;

        public static UInt32[] default_prefix = new UInt32[]
                                { 1, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 10, 10 };
        public static UInt32[] default_total = new UInt32[]
                                { 1, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 18, 26 };
        public static UInt32[] default_switch = new UInt32[]
                                { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
        public static Int32[] default_value = new Int32[]
                                { 0, 1, -1, 2, -2, 3, -3, 4, -4, 5, -5, 6, -6, 7, -7, 8, -8, 0, 0 };
        public static UInt32[] default_code = new UInt32[]
                                { 0, 1, 5, 3, 11, 7, 23, 15, 47, 31, 95, 63, 191, 127, 383, 255, 767, 511, 1023 };


        public void mDispose()
        {
            prefixBits = null;
            totalBits = null;
            switchByte = null;
            baseValue = null;
            baseCode = null;
            codeCount = 0;
        }
        public void mCreate(UInt32 ACount)
        {
            codeCount = ACount;
            prefixBits = new UInt32[codeCount];
            totalBits = new UInt32[codeCount];
            switchByte = new UInt32[codeCount];
            baseValue = new Int32[codeCount];
            baseCode = new UInt32[codeCount];
        }


        public void mCreateDefaultTable()
        {
            codeCount = 19;

            prefixBits = default_prefix;
            totalBits = default_total;
            switchByte = default_switch;
            baseValue = default_value;
            baseCode = default_code;
        }

    }
    class subSection
    {
        private UInt16 id;
        private UInt32 length;
        private UInt32 index;
        private byte[] dataArray;


        public subSection()
        {
            id = 0;
            length = 0;
            index = 0;
            dataArray = new byte[2];
        }

        ~subSection()
        {
            dataArray = null; ;
        }

        public void dispose()
        {
            dataArray = null;
            length = 0;
        }

        public void init(UInt16 AId, UInt32 ALen, UInt32 AIndex, byte[] AArray)
        {
            id = AId;
            length = ALen;
            index = AIndex;
            dataArray = new byte[length + 16]; // add extra for possible overun 
            UInt32 j;
            for (j = 0; j < length; j++)
            {
                dataArray[j] = AArray[j + index - 1];       // ??
            }
        }

        public UInt32 bytesLeft(UInt32 AOffset)
        {
            return AOffset >= length ? 0 : length - AOffset;
        }

        public byte read_8(ref UInt32 ArOffset)
        {
            int bytesLeft = (int)length - (int)ArOffset;
            byte result = 0;

            if (bytesLeft < 0)
            {
                CScpReader.mLogError("Section Read Overflow!");
                return 0;
            }
            if (bytesLeft >= 1)    // only read when there are bytes
            {
                result = CScpReader.read8(dataArray, ArOffset);
                ArOffset += 1;
            }
            return result;
        }

        public UInt16 read_16(ref UInt32 ArOffset)
        {
            int bytesLeft = (int)length - (int)ArOffset;

            UInt16 result = 0;

            if (bytesLeft < 0)
            {
                CScpReader.mLogError("Section Read Overflow!");
                return 0;
            }
            if (bytesLeft >= 2)  // only read when there are bytes
            {
                result = CScpReader.read16(dataArray, ArOffset);
                ArOffset += 2;
            }
            else if (bytesLeft == 1)    // only read available bytes
            {
                result = CScpReader.read8(dataArray, ArOffset);
                ArOffset += 1;
            }
            else
            {
                result = 0; // no bytes
            }
            return result;
        }

        public UInt32 read_32(ref UInt32 ArOffset)
        {
            int bytesLeft = (int)length - (int)ArOffset;

            UInt32 result = 0;

            if (bytesLeft < 0)
            {
                CScpReader.mLogError("Section Read Overflow!");
                return 0;
            }

            if (bytesLeft >= 0)
            {
                result += CScpReader.read8(dataArray, ArOffset);
                ArOffset += 1;
                --bytesLeft;
            }
            if (bytesLeft >= 0) // only read available bytes
            {
                result += (uint)(CScpReader.read8(dataArray, ArOffset)) << 8;
                ArOffset += 1;
                --bytesLeft;
            }
            if (bytesLeft >= 0)
            {
                result += (uint)(CScpReader.read8(dataArray, ArOffset)) << 16;
                ArOffset += 1;
                --bytesLeft;
            }
            if (bytesLeft >= 0)
            {
                result += (uint)(CScpReader.read8(dataArray, ArOffset)) << 24;
                ArOffset += 1;
                //                --bytesLeft;
            }
            return result;
        }

        public void readString(ref UInt32 ArOffset, out string ArString, UInt32 ALen)
        {
            int bytesLeft = (int)length - (int)ArOffset;
            ArString = "";

            if (bytesLeft < 0)
            {
                CScpReader.mLogError("Section Read Overflow!");
                return;
            }
            uint nLeft = bytesLeft >= (int)ALen ? ALen : (uint)bytesLeft;      // only copy bytes available  

            while (nLeft > 0)
            {
                byte b8 = CScpReader.read8(dataArray, ArOffset);
                ArOffset += 1;
                --nLeft;

                if (b8 == 0)
                {
                    ArOffset += nLeft;  // disregard rest of buffer
                    break;
                }
                char c = (char)(b8);
                ArString += c;
            }
            //str->assign((const char*)&(dataArray[*offset]),(Int32)len);
            //*offset += len;
        }
        public void readCString(ref UInt32 ArOffset, out string ArString)
        {
            int bytesLeft = (int)length - (int)ArOffset;
            ArString = "";

            if (bytesLeft < 0)
            {
                CScpReader.mLogError("Section Read Overflow!");
                return;
            }
            uint nLeft = (uint)bytesLeft;      // only copy bytes available  

            while (nLeft > 0)
            {
                byte b8 = CScpReader.read8(dataArray, ArOffset);
                ArOffset += 1;
                --nLeft;

                if (b8 == 0)
                {
                    break; // Cstring is nul terminated
                }
                char c = (char)(b8);
                ArString += c;
            }
            //str->assign((const char*)&(dataArray[*offset]),(Int32)len);
            //*offset += len;
        }

        public void readAString(ref UInt32 ArOffset, out string ArString)
        {
            int bytesLeft = (int)length - (int)ArOffset;
            ArString = "";

            if (bytesLeft < 0)
            {
                CScpReader.mLogError("Section Read Overflow!");
                return;
            }
            byte len = CScpReader.read8(dataArray, ArOffset);
            ++ArOffset;

            if (len >= 32)
            {
                // DynaVision
                string str;
                readCString(ref ArOffset, out str);   // C string terminated with a '\0'
                ArString = (char)(len) + str;
            }
            else if (len > 0)
            {
                readString(ref ArOffset, out ArString, len);   //string in fixed buffer size, can be terminated with a '\0
            }

        }
        public void readStringBuffer(ref UInt32 ArOffset, out string ArString, UInt32 ALen)
        {
            int bytesLeft = (int)length - (int)ArOffset;
            ArString = "";

            if (bytesLeft < 0)
            {
                CScpReader.mLogError("Section Read Overflow!");
                return;
            }
            uint nLeft = bytesLeft >= (int)ALen ? ALen : (uint)bytesLeft;      // only copy bytes available  

            while (nLeft > 0)
            {
                byte b8 = CScpReader.read8(dataArray, ArOffset);
                ArOffset += 1;
                --nLeft;

                char c = (char)(b8);
                ArString += c;
            }
            //str->assign((const char*)&(dataArray[*offset]),(Int32)len);
            //*offset += len;
        }


        /*        char* getPointer(UInt32* offset)
                {
                    return (char*)&dataArray[*offset];
                }
        */
        public bool readHeader(out UInt32 ArLength)
        {
            return CScpReader.mbSectionHeader(dataArray, 0, out ArLength);
        }

        public bool exists()
        {
            //cout + "exist check on #" + id + " - Length: " + length + " - Index: " + index );
            return length > 0;
        }

    }
}
