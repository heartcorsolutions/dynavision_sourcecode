﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Event_Base
{
    [Flags]
    enum DChartDraw
    {
        TopRuler = 0x0001,
        BottomRuler = 0x0002,
        Border = 0x0004,
        HorizontalGrid = 0x0008,
        VerticalGrid = 0x0010,
        DotGrid = 0x0020,
        Line = 0x0040,
        Point = 0x0080,
        MarkerTime = 0x0100,
        MarkerAmplitude = 0x0200,
        CursorArea = 0x0400
    }


    public class CStripChart
    {
 /*       UInt16 mPlotFlags;

        Padding mImageBorder;
        Padding mChartBorder;
        UInt16 mHeaderHeight;
        UInt32 mFooterHeight;

        // colors
*/



        // x axis (T) in seconds
        private float mStartTimeSec;
        private float mStripTimeSec;
        private float mGridUnitT;

        // y axis (A) in units
        private float mStartA;
        private float mEndA;
        private float mGridUnitA;

        private float mOrgMaxA;
        private float mOrgMinA;

        // chart position
        private int mOriginX;
        private int mOriginY;
        private int mAreaLeft;
        private int mAreaTop;
        private int mAreaWidth;
        private int mAreaHeight;
        //bool mbMatchGrid;

        private float mUnitLineT0;
        private uint mUnitLineTN;
        private float mUnitLineA0;
        private uint mUnitLineAN;


        // setup after mSetStrip(..)
        private float mScaleT;
        private float mScaleA;

        private int mHeightMargin = 2;  // max - min  is at $ pixels from edge

        public CStripChart()
        {
            mStartTimeSec = mStripTimeSec = 0.0F;
            mGridUnitT = 0.2F;  // default time grid 0.2S -> 
            mStartA = mEndA = 0.0F;
            mOrgMaxA = mOrgMinA = 0.0F;
            mGridUnitA = 0.5F;
            mOriginX = mOriginY = mAreaLeft = mAreaTop = mAreaWidth = mAreaHeight = 0;
            mScaleT = mScaleA = 0;
            mUnitLineT0 = mUnitLineA0 = 0.0F;
            mUnitLineTN = mUnitLineAN = 0;

            //            mbMatchGrid = false;
        }
        public int mGetAreaLeft()
        {
            return mAreaLeft;
        }
        public int mGetAreaTop()
        {
            return mAreaTop;
        }
        public int mGetAreaWidth()
        {
            return mAreaWidth;
        }
        public int mGetAreaHeight()
        {
            return mAreaHeight;
        }
        public int mGetOriginX()
        {
            return mOriginX;
        }
        public int mGetOriginY()
        {
            return mOriginY;
        }
        public float mGetStartTimeSec()
        {
            return mStartTimeSec;
        }
        public float mGetStripTimeSec()
        {
            return mStripTimeSec;
        }
        public float mGetStartA()
        {
           return mStartA;
        }
        public float mGetEndA()
        {
            return mEndA;
        }
        public float mGetUnitT()
        {
            return mGridUnitT;
        }
        public float mGetUnitA()
        {
            return mGridUnitA;
        }
        public void mSetUnitT(float AUnitT)
        {
            mGridUnitT = AUnitT;
        }
        public void mSetUnitA(float AUnitA)
        {
            mGridUnitA = AUnitA;
        }
        public bool mbSetGraph(int ALeft, int ATop, int AWidth, int AHeight, float AMinRangeA)
        {
            bool bOk = true;
            float minRangeA = AMinRangeA;

            if (minRangeA <= 0.001) minRangeA = mGridUnitA;

            mOriginX = ALeft;
            mOriginY = ATop + AHeight - mHeightMargin;
            mAreaLeft = ALeft;
            mAreaTop = ATop;
            mAreaWidth = AWidth;
            mAreaHeight = AHeight;

            if (mStripTimeSec > 0.001 && mAreaWidth > 1)
            {
                mScaleT = (mAreaWidth - 1) / mStripTimeSec;   // one pixel smaller so that max value fits on the screen
            }
            else
            {
                mScaleT = 0.0F;
                bOk = false;
            }

            float aRange = mEndA - mStartA;
            int h = AHeight - mHeightMargin - mHeightMargin;

            if (aRange < 0.001F)
            {
                mScaleA = 0.0F; // no graph
                bOk = false;
            }
            else if (aRange < minRangeA)
            {
                float f = minRangeA / aRange;
                float aMid = 0.5F * (mStartA + mEndA);
                aRange = minRangeA;
                mStartA = aMid - aRange * 0.5F;
                mEndA = aMid + aRange * 0.5F;
                mScaleA = h / aRange;
            }
            else
            {
                mScaleA = h / aRange;
            }
            mOrgMaxA = mEndA;
            mOrgMinA = mStartA;

            return bOk;
        }
        public bool mbSetGraphRangeA(int ALeft, int ATop, int AWidth, int AHeight, float AMinRangeA, float ARangeA)
        {
            bool bOk = true;
            float aRange = ARangeA < AMinRangeA ? AMinRangeA : ARangeA;

            if (aRange <= 0.001) aRange = mGridUnitA;

            mOriginX = ALeft;
            mOriginY = ATop + AHeight - mHeightMargin;
            mAreaLeft = ALeft;
            mAreaTop = ATop;
            mAreaWidth = AWidth;
            mAreaHeight = AHeight;

            if (mStripTimeSec > 0.001 && mAreaWidth > 1)
            {
                mScaleT = (mAreaWidth - 1) / mStripTimeSec;   // one pixel smaller so that max value fits on the screen
            }
            else
            {
                mScaleT = 0.0F;
                bOk = false;
            }

            int h = AHeight - mHeightMargin - mHeightMargin;
            float oldRange = mEndA - mStartA;

            float aMid = 0.5F * (mStartA + mEndA);
            mStartA = aMid - aRange * 0.5F;
            mEndA = aMid + aRange * 0.5F;
            mScaleA = h / aRange;
            mOrgMaxA = mEndA;
            mOrgMinA = mStartA;

            return bOk;
        }
        public bool mbSetGraphMaxTA(float AStartTimeSec, float AStripTimeSec, float AMinA, float AMaxA, float AMinRangeA)
        {
            bool bOk = true;
            float minRangeA = AMinRangeA;

            if (minRangeA <= 0.001) minRangeA = mGridUnitA;

            mStartTimeSec = AStartTimeSec;
            mStripTimeSec = AStripTimeSec;
            mStartA = AMinA;
            mEndA = AMaxA;
            mScaleT = 0.0F;     
            mScaleA = 0.0F;

            float aRange = AMaxA - AMinA;

            if (aRange < minRangeA)
            {
                float aMid = 0.5F * (mStartA + mEndA);
                aRange = AMinRangeA;
                mStartA = aMid - aRange * 0.5F;
                mEndA = aMid + aRange * 0.5F;
            }
            mOrgMaxA = mEndA;
            mOrgMinA = mStartA;
            return bOk;
        }
        public bool mbSetGraphMaxA(int ALeft, int ATop, int AWidth, int AHeight, float AStartTimeSec, float AStripTimeSec, float AMinA, float AMaxA, float AMinRangeA )
        {
            bool bOk = true;
            float minRangeA = AMinRangeA;

            if (minRangeA <= 0.001) minRangeA = mGridUnitA;
            
            mOriginX = ALeft;
            mOriginY = ATop + AHeight - mHeightMargin;
            mAreaLeft = ALeft;
            mAreaTop = ATop;
            mAreaWidth = AWidth;
            mAreaHeight = AHeight;
 
            mStartTimeSec = AStartTimeSec;
            mStripTimeSec = AStripTimeSec;
            mStartA = AMinA;
            mEndA = AMaxA;

            if( mStripTimeSec > 0.001 && mAreaWidth > 1)
            {
                mScaleT = ( mAreaWidth - 1 ) / mStripTimeSec;   // one pixel smaller so that max value fits on the screen
            }
            else
            {
                mScaleT = 0.0F;
                bOk = false;
            }

            float aRange = AMaxA - AMinA;
            int h = AHeight - mHeightMargin - mHeightMargin;

            if (aRange < 0.001F)
            {
                mScaleA = 0.0F; // no graph
                bOk = false;
            }
            else if (aRange < minRangeA)
            {
                float f = minRangeA / aRange;
                float aMid = 0.5F * (mStartA + mEndA);
                aRange = minRangeA;
                mStartA = aMid - aRange * 0.5F;
                mEndA = aMid + aRange * 0.5F;
                mScaleA = h / aRange;
            }
            else
            {
                mScaleA = h / aRange;
            }
            mOrgMaxA = mEndA;
            mOrgMinA = mStartA;
            return bOk;
        }
        public bool mbSetGraphMaxOrtho(int ALeft, int ATop, int AWidth, int AHeight, float AStartTimeSec, float AStripTimeSec, float AMinA, float AMaxA, float AMinRangeA)
        {
            bool bOk = true;
            float minRangeA = AMinRangeA;

            if (minRangeA <= 0.001) minRangeA = mGridUnitA;

            mOriginX = ALeft;
            mOriginY = ATop + AHeight - mHeightMargin;
            mAreaLeft = ALeft;
            mAreaTop = ATop;
            mAreaWidth = AWidth;
            mAreaHeight = AHeight;

            mStartTimeSec = AStartTimeSec;
            mStripTimeSec = AStripTimeSec;
            mStartA = AMinA;
            mEndA = AMaxA;

            if (mStripTimeSec > 0.001 && mAreaWidth > 1)
            {
                mScaleT = (mAreaWidth - 1) / mStripTimeSec;   // one pixel smaller so that max value fits on the screen
            }
            else
            {
                mScaleT = 0.0F;
                bOk = false;
            }
            float aRange = AMaxA - AMinA;
            int h = AHeight - mHeightMargin - mHeightMargin;

            if( aRange < 0.001F )
            {
                    mScaleA = 0.0F; // no graph
                    bOk = false;
            }
            else if ( aRange < minRangeA)
            {
                float f = minRangeA / aRange;
                float aMid = 0.5F * (mStartA + mEndA);
                aRange = minRangeA;
                mStartA = aMid - aRange * 0.5F;
                mEndA = aMid + aRange * 0.5F;
                mScaleA = h / aRange;
            }
            else 
            {
                mScaleA = h / aRange;
            }
            if (bOk && h > 0 && mScaleT > 0.00001F )
            {
                float dH = h / aRange * 0.5F;
                float dT = dH / 0.2F;

                if( dT <= mScaleT)
                {
                    //ok time axis will be shorter
                    mScaleT = dT;
                }
                else
                {
                    // shrink A to fit time range
                    float f = dT / mScaleT;
                    float aMid = 0.5F * (mStartA + mEndA);
                    aRange = aRange * f; 
                    mStartA = aMid - aRange * 0.5F;
                    mEndA = aMid + aRange * 0.5F;
                    mScaleA = h / aRange;
                }
            }
            mOrgMaxA = mEndA;
            mOrgMinA = mStartA;
            return bOk;
        }
        public int mGetPixelT( float ATimeSec )
        {
            int x = -10000;

            float f = ATimeSec - mStartTimeSec;
            if( f >= 0.0 )
            {
                int i = (int)(f * mScaleT + 0.5F);

                if( i <= mAreaWidth )
                {
                    x = mOriginX + i;
                }
            }
            return x; 
        }
        public bool mbGetLineT(out int ArX1, out int ArX2, float ATime1Sec, float ATime2Sec)
        {
            int x1 = mOriginX;
            int x2 = mOriginX;
            bool bDraw = false;

            float f1 = ATime1Sec - mStartTimeSec;
            float f2 = ATime2Sec - mStartTimeSec;
            if (f1 >= 0.0)
            {
                x1 = (int)(f1 * mScaleT + 0.5F);

                if (x1 > mAreaWidth)
                {
                    x1 = mAreaWidth;
                }
                else
                {
                    bDraw = true;
                }
                x1 += mOriginX;
            }
            if (f2 >= 0.0)
            {
                x2 = (int)(f2 * mScaleT + 0.5F);

                if (x2 > mAreaWidth)
                {
                    x2 = mAreaWidth;
                }
                else
                {
                    bDraw = true;
                }
                x2 += mOriginX;
            }
            ArX1 = x1;
            ArX2 = x2;
            return bDraw;
        }
        public int mGetPixelA(float AValue)
        {
            float f = AValue - mStartA;
            int y = (int)(f * mScaleA + 0.5F);

            return mOriginY - y;
        }
        public float mGetValueT( int AX)
        {
            float t = 0.0F;

            if( mScaleT > 0.0001)
            {
                t = mStartTimeSec + (AX - mOriginX) / mScaleT;
            }
            return t;
        }
        public float mGetValueA(int AY)
        {
            float a = 0.0F;

            if( mScaleA > 0.0001)
            {
                a = mStartA + (mOriginY - AY) / mScaleA;
            }
            return a;
        }
        public float mGetValuePixelT()
        {
            float t = 0.0F;

            if (mScaleT > 0.0001)
            {
                t = 1 / mScaleT;
            }
            return t;
        }
        public float mGetValuePixelA()
        {
            float a = 0.0F;

            if (mScaleA > 0.0001)
            {
                a = 1 / mScaleA;
            }
            return a;
        }
    }
}
