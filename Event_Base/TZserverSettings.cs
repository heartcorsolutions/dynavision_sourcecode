﻿using EventBoard.Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TzDevice
{
    public partial class TZserverSettings : Form
    {
        public CTzDeviceSettings _mSettings = null;
        public string _mSerialNr;
        public bool bChanged = false;
        private bool _mbIsClarus;

        public TZserverSettings( CTzDeviceSettings ASettings, string ASerialNr, bool AbIsClarus)
        {
            try
            {
                _mSettings = ASettings;
                _mSerialNr = ASerialNr;
                _mbIsClarus = AbIsClarus;

                if ( ASettings != null)
                {
                    string deviceModel = " TZ Aera";

                    InitializeComponent();

                    mFillForm();
                    if(_mbIsClarus)
                    {
                        deviceModel = " TZ Clarus";
                        mDisableClarus();
                    }
                    Text = CProgram.sMakeProgTitle("TZ Server settings for " + ASerialNr + deviceModel, false, true);
                }
                else
                {
                    Close();
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init TzServer Form", ex);
            }

        }
        private void mDisableClarus()
        {
            bool b = false;

            checkBoxEnableWireless.Enabled = b;
            checkBoxUseHTTP.Enabled = b;
            textBoxTZSCPlength.Enabled = b;
            comboBoxResolution.Enabled = b;
//Clarus FW 1.9 uses nr on screen             textBoxCenterNr.Enabled = b;
            checkBoxDifferenceEncoding.Enabled = b;
            checkBoxHuffman.Enabled = b;
            comboBoxActionsInterval.Enabled = b;
            checkBoxServerRequest.Enabled = b;
        }
        private void mFillForm()
        {
            labelDeviceSnr.Text = _mSerialNr;
            _mSettings.serialNumber = _mSerialNr;

            labelLastError.Text = "";

            comboBoxDeviceID.Text = _mSettings.deviceID;

            //            .Checked = _mSettings.mGetSettingBool(DTzSettingTags.);
            //            .Text = _mSettings.mGetSettingUInt32(DTzSettingTags.).ToString();
            //            .Text = _mSettings.mGetSettingString(DTzSettingTags.);
            textBoxTZFWversion.Text = _mSettings.firmwareVersion.ToString();

            string server = _mSettings.mGetSettingString(DTzSettingTags.FTP_ADDRESS);
            string[] parts = server.Split(':');

            string ip = parts.Length >= 1 ? parts[0] : "";
            UInt32 port = 0;
            if (parts.Length >= 2) UInt32.TryParse(parts[1], out port);

            textBoxTZserverIP.Text = ip;
            textBoxTZserverPort.Text = port.ToString();

            checkBoxUseHTTP.Checked = _mSettings.mGetSettingBool(DTzSettingTags.USE_HTTP);
            checkBoxCheckSettingRanges.Checked = _mSettings.checkInputData;
            checkBoxEnableWireless.Checked = _mSettings.mGetSettingBool(DTzSettingTags.WIRELESS_ENABLE);
            textBoxTZPinCode.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.START_OK_CODE).ToString();
            textBoxAESKey.Text = _mSettings.mMakeHexStr(_mSettings.setting_aesKey, 0, 16);
            textBoxAESIV.Text = _mSettings.mMakeHexStr(_mSettings.setting_aesIV, 0, 16);
            textBoxCenterName.Text = _mSettings.mGetSettingString(DTzSettingTags.CENTER_NAME);
            textBoxCenterNr.Text = _mSettings.mGetSettingString(DTzSettingTags.CENTER_NUMBER);

            textBoxTZSCPlength.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.SCP_LENGTH).ToString();
            comboBoxScreenSleep.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.SCREEN_SLEEP_TIME).ToString();
            comboBoxResolution.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.BIT_RESOLUTION).ToString();
            comboBoxActionsInterval.Text = _mSettings.mGetSettingUInt32(DTzSettingTags.CHECK_ACTIONS_INTERVAL).ToString();


            checkBoxServerRequest.Checked = _mSettings.mGetSettingBool(DTzSettingTags.REPORT_SERVER_REQUEST);
            checkBoxDifferenceEncoding.Checked = _mSettings.mGetSettingBool(DTzSettingTags.SCP_DIFFERENCE_ENCODING);
//            checkBoxStreamingMode.Checked = _mSettings.mGetSettingBool(DTzSettingTags.STREAMING_MODE);
            checkBoxHuffman.Checked = _mSettings.mGetSettingBool(DTzSettingTags.HUFFMAN_ENCODING);
            checkBoxDemoMode.Checked = _mSettings.mGetSettingBool(DTzSettingTags.DEMO_MODE);
        }

        private bool mbReadForm()
        {
            bool bOk = true;
            string err = "";
            try
            {

                string deviceID = comboBoxDeviceID.Text.Trim();

                if (deviceID.Length < 2) err += " DeviceID,";

                UInt16 firmWare = 0;
                if (false == UInt16.TryParse(textBoxTZFWversion.Text, out firmWare)) err += " firmWare, ";

                string ip = textBoxTZserverIP.Text;
                if (ip.Length < 4) err += "IP, ";

                UInt16 port = 0;
                if (false == UInt16.TryParse(textBoxTZserverPort.Text, out port) | port == 0) err += " port, ";

                UInt16 pin = 0;
                if (false == UInt16.TryParse(textBoxTZPinCode.Text, out pin)) err += " pin, ";

                Byte[] aesKey = new Byte[16];
                bool bAesKey = false;

                if (false == _mSettings.mbReadHex(textBoxAESKey.Text, ref aesKey, 0, 16, out bAesKey)) err += "AesKey, ";

                Byte[] aesIV = new Byte[16];
                bool bAesIV = false;

                if (false == _mSettings.mbReadHex(textBoxAESIV.Text, ref aesIV, 0, 16, out bAesIV)) err += "AesKey, ";

                string name = textBoxCenterName.Text;
                if (name.Length == 0) err += "Name, ";

                string number = textBoxCenterNr.Text;
                if (number.Length == 0) err += "Number, ";

                UInt16 scpLength = 30;  // scp length of clrus is always 30
                if( false == _mbIsClarus )
                {
                    if (false == UInt16.TryParse(textBoxTZSCPlength.Text, out scpLength)) err += "SCP length, ";
                }

                UInt16 sleep = 0;
                if (false == UInt16.TryParse(comboBoxScreenSleep.Text, out sleep)) err += "Sleep, ";

                UInt16 resolution = 0;
                if (false == UInt16.TryParse(comboBoxResolution.Text, out resolution)) err += "Resolution, ";

                UInt16 interval = 0;
                if (false == UInt16.TryParse(comboBoxActionsInterval.Text, out interval)) err += "Interval, ";

                bOk = err.Length == 0;
                if (bOk)
                {
                    _mSettings.deviceID = deviceID;
                    _mSettings.firmwareVersion = firmWare;
                    _mSettings.mbSetSettingString(DTzSettingTags.FTP_ADDRESS, ip + ":" + port.ToString());

                    _mSettings.mbSetSettingBool(DTzSettingTags.USE_HTTP, checkBoxUseHTTP.Checked);
                    _mSettings.checkInputData = checkBoxCheckSettingRanges.Checked;
                    _mSettings.mbSetSettingBool(DTzSettingTags.WIRELESS_ENABLE, checkBoxEnableWireless.Checked);

                    _mSettings.mbSetSettingUInt32(DTzSettingTags.START_OK_CODE, pin);

                    _mSettings.setting_aesKey = aesKey;
                    _mSettings.setting_aesKeySet = bAesKey;

                    _mSettings.setting_aesIV = aesIV;
                    _mSettings.setting_aesIVSet = bAesIV;

                    _mSettings.mbSetSettingString(DTzSettingTags.CENTER_NAME, name);
                    _mSettings.mbSetSettingString(DTzSettingTags.CENTER_NUMBER, number);

                    _mSettings.mbSetSettingUInt32(DTzSettingTags.SCP_LENGTH, scpLength);
                    _mSettings.mbSetSettingUInt32(DTzSettingTags.SCREEN_SLEEP_TIME, sleep);
                    _mSettings.mbSetSettingUInt32(DTzSettingTags.BIT_RESOLUTION, resolution);

                    _mSettings.mbSetSettingUInt32(DTzSettingTags.CHECK_ACTIONS_INTERVAL, interval);

                    _mSettings.mbSetSettingBool(DTzSettingTags.REPORT_SERVER_REQUEST, checkBoxServerRequest.Checked);
                    _mSettings.mbSetSettingBool(DTzSettingTags.SCP_DIFFERENCE_ENCODING, checkBoxDifferenceEncoding.Checked);
//                    _mSettings.mbSetSettingBool(DTzSettingTags.STREAMING_MODE, checkBoxStreamingMode.Checked);
                    _mSettings.mbSetSettingBool(DTzSettingTags.HUFFMAN_ENCODING, checkBoxHuffman.Checked);
                    _mSettings.mbSetSettingBool(DTzSettingTags.DEMO_MODE, checkBoxDemoMode.Checked);

                    bChanged = true;
                }
                else
                {
                    err = "Invalid: " + err;
                    err = err.Substring(0, err.Length - 2); // remove last ', '

                }
                labelLastError.Text = err;

            }
            catch (Exception ex)
            {
                CProgram.sLogException("Read TzServer Form", ex);
            }

            return bOk;
        }

        private void label24_Click(object sender, EventArgs e)
        {
            if( mbReadForm())
            {
                Close();
            }
        }

        private void labelUseTzProxy_Click(object sender, EventArgs e)
        {
            textBoxTZserverIP.Text = "devices.bitrhythm.io";
            textBoxTZserverPort.Text = "1537";
        }
    }
}
