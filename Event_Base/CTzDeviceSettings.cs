﻿using Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// based on TZ aera-genconfig.cpp
// generates a settings file for the TZMR based on the values in the input file
// Action derived from TZ actionsGenerator.cpp


// SV20170715 needs to rename variables (converted just ti c#, no time, must make it work)

namespace EventBoard.Event_Base
{
    public enum DTzSettingTags       // new 2018 april are not implemented !!
    {
        Unknown,
        BRADY_ONSET_BPM = 1,
        BRADY_OFFSET_BPM = 2,
        BRADY_DURATION = 3,
        TACHY_ONSET_BPM = 4,
        TACHY_OFFSET_BPM = 5,
        TACHY_DURATION = 6,
        AF_DURATION = 7,
        PAUSE_DURATION = 8,
        BRADY_RATE_CHANGE = 10,
        TACHY_RATE_CHANGE = 11,
        //       ARITHMIA_DURATION_THRESHOLD = 12,    // new 2018 april9 do not use for old firmware

        AF_RR_COUNT = 20,
        DERIVE_FOR_ANALYSIS = 21,
        RPEAK_THRESHOLD = 22,

        ANALYSIS_CHANNEL_MASK = 30,
        DETECT_PACEMAKER = 31,
        CROSS_CHANNEL_ANALYSIS = 32,
        CHANNEL_ENABLE_MASK = 33,
        USE_ARRHYTHMIA_STATE_MACHINE = 34,
        PIM_ENABLE = 35,
        PIM_THRESHOLD = 36,
        SAMPLE_CALIBRATION = 37,
        LOG_QRS_COMPLEXES = 38,

        //       SUMMERIZE_QRS_DETECTION = 40,    // new 2018 do not use for old firmware
        //       PEDOMETER_PERIOD = 41,          // new 2018 april do not use for old firmware

        REPORT_START = 51,
        REPORT_STOP = 52,
        REPORT_BREAK = 53,
        REPORT_RESUME = 54,
        REPORT_FULL = 55,
        REPORT_PACE_DETECT = 56,

        REPORT_ANALYSIS_RESUMED = 60,
        REPORT_ANALYSIS_PAUSED = 61,
        REPORT_LEAD_DISCONNECTED = 62,
        REPORT_TACHY_ONSET = 63,
        REPORT_TACHY_OFFSET = 64,
        REPORT_BRADY_ONSET = 65,
        REPORT_BRADY_OFFSET = 66,
        REPORT_AF_ONSET = 67,
        REPORT_AF_OFFSET = 68,
        REPORT_PAUSE_ONSET = 69,
        REPORT_PATIENT_EVENT = 70,
        REPORT_BATTERY_LOW = 71,
        REPORT_CHARGING_STARTED = 72,
        REPORT_CHARGING_STOPPED = 73,
        REPORT_SERVER_REQUEST = 74,
        REPORT_PAUSE_OFFSET = 75,
        REPORT_SCP_RETRANSMIT = 76,
        REPORT_TZR_REQUEST = 77,
        REPORT_PVC_ONSET = 78,

//        STUDY_HOURS = 97,                   // ?? new 2018 april      not compatible!
        INVERT_SCP_ECG = 98,
        INVERT_ONSCREEN_ECG = 99,
        SCP_DIFFERENCE_ENCODING = 100,
        SAMPLE_RATE = 101,                  // ?? new 2018 april
        LP_FILTER = 102,
        SCP_LENGTH = 103,
        LEAD_CONFIG = 104,
        REPORT_ONSET_PRE_TIME = 105,         // nr files // -> nr seconds 2018 april TZ Clarus uses one setting for pre and post
        REPORT_ONSET_POST_TIME = 106,
        TTM_ENABLE = 107,
        STUDY_LENGTH = 108,
        BIT_RESOLUTION = 109,
        HUFFMAN_ENCODING = 110,
        DIGITAL_HP_FILTER = 111,
        START_OK_CODE = 112,
        DIGITAL_LP_FILTER = 113,
        DIGITAL_NOTCH_FILTER = 114,
        FILL_SQUARE_WAVES = 115,
        RECORD_CONTINUOUS = 116,
        //        REPORT_OFFSET_PRE_TIME = 117,         do not use for old firmware// nr files // -> nr seconds// 2018 april Set as the same value as 105
        //        REPORT_OFFSET_POST_TIME = 118,        do not use for old firmware// nr files // -> nr seconds// 2018 april Set as the same value as 106
        REPORT_BPM_PERIOD = 119,
        TTM_MODE = 120,
        TTM_TOP_BYTE = 121,
        TTM_FAILOVER_PERIOD = 122,
        // 123 do not use
        //      ALLOW_LOW_BATTERY_START = 147,      // ?? new 2018 april do not use for old firmware
        //     STUDY_COMPLETE_SLEEP = 148,         // ?? new 2018 april do not use for old firmware
        NAG_ON_LEAD_OFF = 149,
        MAX_STREAMING_DELAY = 150,
        TRIM_EVENTS = 151,
        WIRELESS_ENABLE = 152,
        AUTO_ANSWER = 153,
        SYNC_TIME = 154,
        STREAMING_MODE = 155,               // ?? new 2018 april, functional changes
        TRANSMIT_INTERVAL_REPORTS = 156,
        SILENT_MODE = 157,
        VIB_CLICKS = 158,
        SCREEN_SLEEP_TIME = 159,
        DEMO_MODE = 160,
        SMS_ENABLE = 161,
        VOICE_ENABLE = 162,
        CHECK_ACTIONS_INTERVAL = 163,
        STUDY_END_DATE_TIME = 164,
        SYMPTOM_DIARY_COUNT = 165,
        ACTIVITY_DIARY_COUNT = 166,
        STAGGER_INTERVAL_UPLOADS = 167,
        LONG_EVENT_THRESHOLD = 168,
        LONG_EVENT_VOLTAGE = 169,
        MODEM_VOLTAGE = 170,
        MODEM_TEMPERATURE = 171,
        REMEMBER_LAST_DIARY = 172,

        DIARY_SYMPTOM_1 = 181,
        DIARY_SYMPTOM_2 = 182,
        DIARY_SYMPTOM_3 = 183,
        DIARY_SYMPTOM_4 = 184,
        DIARY_SYMPTOM_5 = 185,
        DIARY_SYMPTOM_6 = 186,
        DIARY_SYMPTOM_7 = 187,
        DIARY_SYMPTOM_8 = 188,
        DIARY_SYMPTOM_9 = 189,
        DIARY_SYMPTOM_10 = 190,
        DIARY_ACTIVITY_1 = 191,
        DIARY_ACTIVITY_2 = 192,
        DIARY_ACTIVITY_3 = 193,
        DIARY_ACTIVITY_4 = 194,
        DIARY_ACTIVITY_5 = 195,
        DIARY_ACTIVITY_6 = 196,
        DIARY_ACTIVITY_7 = 197,
        DIARY_ACTIVITY_8 = 198,
        DIARY_ACTIVITY_9 = 199,
        DIARY_ACTIVITY_10 = 200,

        PATIENT_ID = 201,
        PATIENT_NAME = 202,
        EVENT_STATIC_PAYLOAD = 203,
        INCOMING_NUMBER = 210,
        CENTER_NAME = 211,
        CENTER_NUMBER = 212,

        FTP_ADDRESS = 213,
        FTP_USER_NAME = 214,
        FTP_PASSWORD = 215,
        USE_HTTP = 220,
        CHECK_FOR_CLEAN_SERVER = 221,
        VERIFY_SERVER_CHANGES = 222,
        WAIT_FOR_HTTP_RESPONSE = 223,
        REDOWNLOAD_DELAY = 224,
        CONNECTION_TIMEOUT = 225,
        AES_KEY = 230,
        AES_IV = 231,

        UPDATE_DEFAULTS = 249,
        MENU_LANGUAGE = 250,
        ERROR_RETRIES = 251,
        ERROR_PERIOD = 252,

        COMPATIBLE_AERA = 254,     // new 2018 april
        TERM_TAG = 255
    }; //end enum DTzSettingTags

    public enum DTzActionTags                   // new 2018 april are not implemented !!
    {
        ACTIONS_ENABLE_NEW_STUDY = 10,
        ACTIONS_REQUEST_SCP_BLOCK = 20,
        ACTIONS_RETRANSMIT_SCP_FILE = 21,
        ACTIONS_REQUEST_RECENT_BLOCK = 22,   //new 2018 april
        ACTIONS_REQUEST_TZR_FILE = 30,
        ACTIONS_UPDATE_SETTINGS = 40,
        ACTIONS_DISPLAY_MESSAGE = 50,
        ACTIONS_REQUEST_LOG = 60,   //new 2018 april
        ACTION_UPDATE_FIRMWARE = 100,       //new 2018 april use "/firmware/h3r_update_013.frw"
        ACTIONS_END_STUDY = 200,
        ACTIONS_START_STUDY = 201,
        ACTION_FORMAT_SD = 210,  // new 2018 april
        ACTION_SHUTDOWN = 221,              // new 2018 april
        BAD_TAG = 254,
        ACTIONS_TERMINATOR = 255
    };


    public class CTzSettingItem
    {
        public string parseStr;
        public UInt64 min, max;
        public UInt64 dataVal;
        public DTzSettingTags tag;
        public Byte maxLength;
        public Byte dataLength;
        public Byte binaryFlag;
        public Byte setFlag;
        public string dataStr;

        public CTzSettingItem(string S, DTzSettingTags tg, UInt64 mn, UInt64 mx, Byte len, UInt64 AValue)
        {
            parseStr = S;//        parseStr.assign(pS);
            tag = tg;
            min = mn;
            max = mx;
            dataVal = AValue;
            if (dataVal > max) dataVal = max;
            if (dataVal > max) dataVal = max;
            maxLength = len;
            dataLength = len;
            dataStr = "";
            binaryFlag = 1;
            setFlag = 0;
        }
        public CTzSettingItem(string S, DTzSettingTags tg, Byte len)
        {
            parseStr = S; // parseStr.assign(pS);
            tag = tg;
            min = 0;
            max = 0;
            dataVal = 0;
            maxLength = len;
            dataLength = 0;
            dataStr = "";
            binaryFlag = 0;
            setFlag = 0;
        }

        public bool mbIsBinary()
        {
            return binaryFlag != 0;
        }

        UInt64 parseValue(string S)
        {
            UInt64 value = 0;

            if (S != null)
            {
                int pos = S.IndexOf('=');

                UInt64.TryParse(S.Substring(pos <= 0 ? 0 : pos + 1), out value);
            }

            return value;
        }

        public bool set(string S, bool check_range, UInt32 line_number)
        {
            bool bOk = true;
            if (0 != binaryFlag)
            {
                dataVal = parseValue(S);
                dataStr = "";
                if (check_range && ((dataVal < min) || (dataVal > max)))
                {
                    CProgram.sLogError("Tz " + line_number.ToString() + ": ERROR! Invalid binary data: setting." + S + "" + dataVal.ToString());
                    bOk = false;
                }
            }
            else
            {
                dataStr = S;
                dataVal = 0;
                dataLength = 0;
                if (check_range && (dataStr.Length > maxLength))
                {
                    CProgram.sLogError("Tz " + line_number.ToString() + ": ERROR! Invalid string data: setting." + S + dataStr + " length " + dataStr.Length.ToString() + ">" + maxLength.ToString());
                    bOk = false;
                }
            }
            setFlag = bOk ? (byte)1 : (byte)0;
            return bOk;
        }

        public uint write(Byte[] Buffer, UInt32 ArIndex, bool badAlignment)
        {
            uint i = 0;
            Int32 j;

            if (0 != setFlag)
            {
                Buffer[ArIndex + i++] = (byte)tag;

                if (0 == binaryFlag)
                {
                    Buffer[ArIndex + i++] = (byte)dataStr.Length;

                    for (j = 0; j < dataStr.Length; j++)
                    {
                        Buffer[ArIndex + i++] = (byte)dataStr[j];
                    }
                }
                else
                {
                    Buffer[ArIndex + i++] = dataLength;

                    UInt64 temp = dataVal;
                    for (j = 0; j < dataLength; j++)
                    {
                        Buffer[ArIndex + i++] = (byte)(temp & 0xff);
                        temp = temp >> 8;
                    }
                }
                if (badAlignment) Buffer[ArIndex + i++] = 0xc4;
                else Buffer[ArIndex + i++] = 0;
            }
            return i;
        }

        int get_tag()
        {
            return (int)tag;
        }
    };// end class CTzSettingItem


    public class CTzDeviceSettings
    {
        public const UInt16 _cMaxSettingsFileLength = 4096;
        public const string _cSettingsIdString = "TZSET";
        public const string MODEL_DESC_STRING = "TZMR\0\0";

        public const UInt16 _cMaxActionFileLength = 32768;
        public const string _cActionIdString = "TZACT";

        public const UInt16 _cFileID_NOT_ALLOWED = 0;
        public const UInt16 _cFileID_UNKNOWN = 0x7FFF; // 32767
        public const UInt16 _cFileID_EMPTY = 0x7E00; //  32256

        public const UInt16 _cFileID_BAD = 0x7F00;  // 32512
        public const UInt16 _cFileID_BIG = 0x7D00;  // ‭32000       // one evry minute for 22 days (overflow at 44 days)
        public const UInt16 MAX_SCP_REQUESTS = 255;
        public const UInt16 MAX_TZR_REQUESTS = 255;
        public const UInt16 MAX_RETRANSMIT_REQUESTS = 255;

        public static bool _sbUseQueue = true; 

        private static UInt16[] ccitt_crc16_table = {
    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
    0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
    0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
    0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
    0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
    0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
    0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
    0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
    0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
    0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
    0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
    0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
    0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
    0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
    0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
    0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
    0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
    0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
    0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
    0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
    0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
    0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
    0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
    0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
    0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
    0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
    0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
    0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
    0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
    0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
    0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
    0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};

        public const string _cSettingNumberExt = ".settingNumber";
        public const string _cActionNumberExt = ".actionNumber";
        public const string _cSettingFileExt = ".tzs";
        public const string _cActionFileExt = ".tza";
        public const string _cSettingSdcardName = "config";
        public const string _cSettingDefaultName = "defaults";
        public const string _csToServerName = "ToServer";
        public const string _csTzAeraName = "TzAera";

        public List<CTzSettingItem> settingList = null;

        public UInt16 firmwareVersion = 10;
        public string serialNumber = "";
        public string deviceID = "TZMR\0\0";
        public UInt16 fileID = 0;
        public bool checkInputData = false;
        public bool badAlignment = false;
        public bool badCRC = false;
        public bool badLength = false;

        public Byte[] file_aesKey = new Byte[16];
        public Byte[] file_aesIV = new Byte[16];
        public bool file_aesKeySet = false;
        public bool file_aesIVSet = false;
        public Byte[] setting_aesKey = new Byte[16];
        public Byte[] setting_aesIV = new Byte[16];
        public bool setting_aesKeySet = false;
        public bool setting_aesIVSet = false;
        public bool setting_aesKey_detected = false;
        public bool setting_aesIV_detected = false;

        public UInt32 end_year = 0, end_month = 0, end_day = 0, end_hour = 0;
        public bool study_end_set = false;

        public bool mbLog = true;
        public bool mbCheckCrc = true;

        public UInt16 mActionTzrN = 0;
        public UInt16 mActionScpN = 0;
        public UInt16 mActionRrN = 0;


        public CTzDeviceSettings()
        {
            settingList = new List<CTzSettingItem>();
        }

        public static bool sbGetTzAeraDevicePath(out string ArDevicePath)
        {
            return CDvtmsData.sbGetDeviceTypeDir(out ArDevicePath, DDeviceType.TZ);
        }
        public static bool sbGetTzAeraDeviceUnitPath(out string ArDevicePath, string ASerialNumber)
        {
            return CDvtmsData.sbGetDeviceUnitDir(out ArDevicePath, DDeviceType.TZ, ASerialNumber, true);
        }
        public static bool sbCreateTzAeraDevicePath(out string ArDevicePath, string ASerialNumber)
        {
            // TZ device directory should only be made by i-reader
            return CDvtmsData.sbCreateDeviceUnitDir(out ArDevicePath, DDeviceType.TZ, ASerialNumber);
        }

        public static bool sbMakeSettingFileName( out string AFileName, string ASerialNumber)
        {
            bool bOk = ASerialNumber != null && ASerialNumber.Length > 0;
            string name = "";

            if( bOk )
            {
                name = Path.ChangeExtension(ASerialNumber, _cSettingFileExt);
            }
            AFileName = name;
            return bOk;
        }
        public static bool sbMakeSettingSdFileName(out string AFileName, string ASerialNumber)
        {
            bool bOk = ASerialNumber != null && ASerialNumber.Length > 0;
            string name = "";

            if (bOk)
            {
                name = Path.ChangeExtension(_cSettingSdcardName, _cSettingFileExt);
            }
            AFileName = name;
            return bOk;
        }

        public static bool sbMakeActionFileName(out string AFileName, string ASerialNumber)
        {
            bool bOk = ASerialNumber != null && ASerialNumber.Length > 0;
            string name = "";

            if (bOk)
            {
                name = Path.ChangeExtension(ASerialNumber, _cActionFileExt);
            }
            AFileName = name;
            return bOk;
        }
 
        private static bool sbGetFileSerialSeqNr(out UInt32 ArFileNumber, string ADevicePath, string ASerialNumber, string ATypeNumber)
        {
            bool bOk = false;
            UInt32 fileNumber = 0;
            string fileName = Path.Combine(ADevicePath, Path.ChangeExtension(ASerialNumber, ATypeNumber));

            try
            {
                if (File.Exists(fileName))
                {
                    using (StreamReader sr = new StreamReader(fileName))
                    {
                        if (sr == null)
                        {
                            CProgram.sLogError("Failed to open for read " + fileName);
                        }
                        else
                        {
                            string line = sr.ReadLine();

                            bOk = UInt32.TryParse(line, out fileNumber);
                        }
                    }
                }
                else
                {
                    bOk = true; // new file
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init TzDevice name", ex);
                bOk = false;
                fileNumber = 0;
            }
            ArFileNumber = fileNumber;

            return bOk;
        }

        private static bool sbSetFileSerialSeqNr(UInt32 ArFileNumber, string ADevicePath, string ASerialNumber, string ATypeNumber)
        {
            bool bOk = false;
            string fileName = Path.Combine(ADevicePath, Path.ChangeExtension(ASerialNumber, ATypeNumber));

            try
            {
                using (StreamWriter sw = new StreamWriter(fileName))
                {
                    if (sw == null)
                    {
                        CProgram.sLogError("Failed to open for write " + fileName);
                    }
                    else
                    {
                        string line = ArFileNumber.ToString();

                        sw.WriteLine(line);

                        bOk = true;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Init TzDevice name", ex);
                bOk = false;
            }

            return bOk;
        }

        private static bool sbIncFileSerialSeqNr(out UInt32 ArFileNumber, string ArDevicePath, string ASerialNumber, string ATypeNumber)
        {
            bool bOk = sbGetFileSerialSeqNr(out ArFileNumber, ArDevicePath, ASerialNumber, ATypeNumber);
            if (bOk)
            {
                ++ArFileNumber;
                bOk = sbSetFileSerialSeqNr(ArFileNumber, ArDevicePath, ASerialNumber, ATypeNumber);
            }
            return bOk;
        }

        public static bool sbGetActionSeqNr( out UInt32 ArFileNumber, string ADevicePath, string ASerialNumber)
        {
            return sbGetFileSerialSeqNr(out ArFileNumber, ADevicePath, ASerialNumber, _cActionNumberExt);
        }

        public static bool sbIncActionSeqNr(out UInt32 ArFileNumber, string ADevicePath, string ASerialNumber)
        {
            return sbIncFileSerialSeqNr(out ArFileNumber, ADevicePath, ASerialNumber, _cActionNumberExt);

        }

        public static bool sbSetActionSeqNr(UInt32 AFileNumber, string ADevicePath, string ASerialNumber)
        {
            return sbSetFileSerialSeqNr(AFileNumber, ADevicePath, ASerialNumber, _cActionNumberExt);

        }
        public static bool sbGetSettingSeqNr(out UInt32 ArFileNumber, string ADevicePath, string ASerialNumber)
        {
            return sbGetFileSerialSeqNr(out ArFileNumber, ADevicePath, ASerialNumber, _cSettingNumberExt);

        }

        public static bool sbIncSettingSeqNr(out UInt32 ArFileNumber, string ADevicePath, string ASerialNumber)
        {
            return sbIncFileSerialSeqNr(out ArFileNumber, ADevicePath, ASerialNumber, _cSettingNumberExt);

        }

        public static bool sbSetSettingSeqNr(UInt32 AFileNumber, string ADevicePath, string ASerialNumber)
        {
            return sbSetFileSerialSeqNr(AFileNumber, ADevicePath, ASerialNumber, _cSettingNumberExt);
        }

        public static bool sbMakeToServerFileName(ref string ArFileName)
        {
            bool bOk = false;
            string name = Path.GetFileName(ArFileName);
            string toDir = Path.Combine(Path.GetDirectoryName(ArFileName), _csToServerName);
            
            try
            {
                if (false == Directory.Exists(toDir))
                {
                    Directory.CreateDirectory(toDir);
                }
                if (Directory.Exists(toDir))
                {
                      ArFileName = Path.Combine(toDir, name);

                    bOk = true;
                }
                else
                {
                    CProgram.sLogError("Directory for file does not exist:" + toDir + ", file= " + name);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed make ToServerName " + ArFileName, ex);
            }
            return bOk;
        }
        public static string sCombineFileIDUtc(string AName, UInt16 AActionFileNr, DateTime AUtc)
        {
            string ymdhms = CProgram.sDateTimeToYMDHMS(AUtc);
 
            return AName + "_" + AActionFileNr.ToString("00000") + "_" + ymdhms;

        }
        public static bool sbMakeToServerUtcFileName(ref string ArFileName, UInt16 AActionFileNr, DateTime AUtc)
        {
            bool bOk = false;
            string name = Path.GetFileName(ArFileName);
            string toDir = Path.Combine(Path.GetDirectoryName(ArFileName), _csToServerName);

            try
            {
                if (false == Directory.Exists(toDir))
                {
                    Directory.CreateDirectory(toDir);
                }
                if (Directory.Exists(toDir))
                {
                    if (_sbUseQueue && AUtc != null && AUtc != DateTime.MinValue)
                    {
//                        string ymdhms = CProgram.sDateTimeToYMDHMS(AUtc);
                        string ext = Path.GetExtension(name);
                        name = Path.GetFileNameWithoutExtension(name);

 //                       name = name + "_" + AActionFileNr.ToString("00000") + "_" + ymdhms + ext;
                        name = sCombineFileIDUtc(name, AActionFileNr, AUtc) + ext;
                    }
                    ArFileName = Path.Combine(toDir, name);

                    bOk = true;
                }
                else
                {
                    CProgram.sLogError("Directory for file does not exist:" + toDir + ", file= " + name);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed make ToServerName " + ArFileName, ex);
            }
            return bOk;
        }

        /*public static bool sbCopyFileToServer( string AFileName) // do not use, use saveCopy
        {
            bool bOk = false;
            string name = Path.GetFileName(AFileName);
            string toDir = Path.Combine(Path.GetDirectoryName(AFileName), _csToServerName);
            string toFileName = Path.Combine(toDir, name);

            try
            {
                if (false == Directory.Exists(toDir))
                {
                    Directory.CreateDirectory(toDir);
                }
                if (Directory.Exists(toDir))
                {
                    File.Copy(AFileName, toFileName, true);
                    bOk = true;
                }
                else
                {
                    CProgram.sLogError("Directory for file does not exist:" + toFileName);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed copy to " + toFileName, ex);
            }
            return bOk;
        }
        */
        public static bool sbCheckFileOnServer(string AFileName)
        {
            bool bOk = false;
            string name = Path.GetFileName(AFileName);
            string toDir = Path.Combine(Path.GetDirectoryName(AFileName), _csToServerName);
            string toFileName = Path.Combine(toDir, name);

            try
            {
                if (Directory.Exists(toDir))
                {
                    bOk = File.Exists(toFileName);
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed check to " + toFileName, ex);
            }
            return bOk;
        }

        public static bool sbCheckStatusFile(out UInt32 ArIndex, out string ArFileInfo, string ADevicePath, string ASerialNumber, string ANumberExt, string AFileExt)
        {
            bool bOk = false;

            string s = "";
            UInt32 index = 0;

            if (ADevicePath != null && ASerialNumber != null && ASerialNumber.Length > 0 )
            {
                string name = Path.ChangeExtension(ASerialNumber, AFileExt);
                string fileName = Path.Combine(ADevicePath, name);
                string toDir = Path.Combine(ADevicePath, _csToServerName);
                string toFileName = Path.Combine(toDir, name);

                try
                {
                    if (false == sbGetFileSerialSeqNr(out index, ADevicePath, ASerialNumber, ANumberExt))
                    {
                        s = "?index? ";
                    }
                    if (File.Exists(fileName))
                    {
                        DateTime dt = File.GetLastWriteTime(fileName);

                        if (dt.Year > 2000)
                        {
                            s += CProgram.sDateTimeToString(dt);
                            bOk = true;
                        }

                        if (File.Exists( toFileName))
                        {
                            s += " uploading";
                        }
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed collect info for " + fileName, ex);
                }

            }
            ArIndex = index;
            ArFileInfo = s;

            return bOk;
        }
        public static bool sbCheckStatusList(out UInt32 ArIndex, out string ArFileInfo, out UInt32 ArNrInQue, out DateTime ArOldestDT, 
            string ADevicePath, string ASerialNumber, string ANumberExt, string AFileExt)
        {
            bool bOk = false;

            string s = "";
            UInt32 index = 0;
            UInt32 nFiles = 0;
            DateTime oldestDT = DateTime.MaxValue;

            if (ADevicePath != null && ASerialNumber != null && ASerialNumber.Length > 0)
            {
                string name = Path.ChangeExtension(ASerialNumber, AFileExt);
                string fileName = Path.Combine(ADevicePath, name);
                string toDir = Path.Combine(ADevicePath, _csToServerName);
                string toFileName = Path.Combine(toDir, name);

                try
                {
                    if (false == sbGetFileSerialSeqNr(out index, ADevicePath, ASerialNumber, ANumberExt))
                    {
                        s = "?index? ";
                    }
                    if (File.Exists(fileName))
                    {
                        DateTime dt = File.GetLastWriteTime(fileName);

                        if (dt.Year > 2000)
                        {
                            s += CProgram.sDateTimeToString(dt);
                            bOk = true;
                        }
                    }

                    try
                    {
                        string searchPath = Path.Combine(toDir, "*" + AFileExt);
                        DirectoryInfo dirInfo = new DirectoryInfo(toDir);
                        FileInfo[] fileList = dirInfo.GetFiles("*" + AFileExt);
                        int n = fileList == null ? 0 : fileList.Length;

                        if (n > 0)
                        {
                            foreach (FileInfo fi in fileList)
                            {
                                if (fi.FullName.EndsWith(AFileExt))
                                {
                                    ++nFiles;
                                    if (fi.LastWriteTime != DateTime.MinValue && fi.LastWriteTime < oldestDT)
                                    {
                                        oldestDT = fi.LastWriteTime;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Failed collect toServer uploading list for " + fileName, ex);
                    }
                    // uploading / uploaded 

                    if (nFiles > 0)
                    {
                        s += " " + nFiles.ToString() + " in queue " + CProgram.sDateTimeToYMDHMS(oldestDT);
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed collect list info for " + fileName, ex);
                }

            }
            ArIndex = index;
            ArFileInfo = s;
            ArNrInQue = nFiles;
            ArOldestDT = oldestDT;

            return bOk;
        }
        public static bool sbClearToServer( out UInt32 ArNrInQue,
            string ADevicePath, string ASerialNumber, string AFileExt)
        {
            bool bOk = false;

            string s = "";
            bool bAll = AFileExt == null || AFileExt.Length == 0;
            UInt32 nFiles = 0;
            DateTime oldestDT = DateTime.MaxValue;

            if (ADevicePath != null && ASerialNumber != null && ASerialNumber.Length > 0)
            {
                string toDir = Path.Combine(ADevicePath, _csToServerName);

                try
                {
                    if (Directory.Exists(toDir))
                    {
                        string searchPath = Path.Combine(toDir, "*" + AFileExt);
                        DirectoryInfo dirInfo = new DirectoryInfo(toDir);
                        FileInfo[] fileList = dirInfo.GetFiles(bAll ? "*.*" : "*" + AFileExt);
                        int n = fileList == null ? 0 : fileList.Length;

                        if (n > 0)
                        {
                            foreach (FileInfo fi in fileList)
                            {
                                if (bAll || fi.FullName.EndsWith(AFileExt))
                                {
                                    ++nFiles;
                                    File.Delete(fi.FullName);
                                }
                            }
                        }
                        bOk = true;
                    }
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed clearing toServer uploading list for " + ASerialNumber, ex);
                }
            }
            ArNrInQue = nFiles;

            return bOk;
        }

        public static bool sbCheckSettingsFile(out UInt32 ArIndex, out string ArFileInfo, string ADevicePath, string ASerialNumber)
        {
            return sbCheckStatusFile(out ArIndex, out ArFileInfo, ADevicePath, ASerialNumber, _cSettingNumberExt, _cSettingFileExt);
        }

        public static bool sbCheckActionFile(out UInt32 ArIndex, out string ArFileInfo, string ADevicePath, string ASerialNumber)
        {
            bool bOk = false;

            if (_sbUseQueue)
            {
                uint nInQueue;
                DateTime oldestDT;
                bOk = sbCheckStatusList(out ArIndex, out ArFileInfo, out nInQueue, out oldestDT, ADevicePath, ASerialNumber, _cActionNumberExt, _cActionFileExt);
            }
            else
            {
                bOk = sbCheckStatusFile(out ArIndex, out ArFileInfo, ADevicePath, ASerialNumber, _cActionNumberExt, _cActionFileExt);
            }
            return bOk;
        }
        public static bool sbCheckActionQueue(out UInt32 ArNrInQueue, out DateTime ArOldestDT, string ADevicePath, string ASerialNumber)
        {
            bool bOk = false;
            uint nInQueue = 0;
            DateTime oldestDT = DateTime.MinValue;

            if (_sbUseQueue)
            {
                string fileInfo;
                UInt32 index;
              
                bOk = sbCheckStatusList(out index, out fileInfo, out nInQueue, out oldestDT, ADevicePath, ASerialNumber, _cActionNumberExt, _cActionFileExt);

            }
            ArNrInQueue = nInQueue;
            ArOldestDT = oldestDT;

            return bOk;
        }

        public void mSetDefaults(string ASerialNumber)
        {
            #region SetDefaults
            if (settingList != null)
            {
                settingList.Add(new CTzSettingItem(".brady_onset_bpm", DTzSettingTags.BRADY_ONSET_BPM, 20, 100, 1, 40 ));
                settingList.Add(new CTzSettingItem(".brady_offset_bpm", DTzSettingTags.BRADY_OFFSET_BPM, 20, 100, 1, 45));
                settingList.Add(new CTzSettingItem(".brady_duration", DTzSettingTags.BRADY_DURATION, 0, 600, 2, 30));
                settingList.Add(new CTzSettingItem(".tachy_onset_bpm", DTzSettingTags.TACHY_ONSET_BPM, 100, 250, 1, 130));
                settingList.Add(new CTzSettingItem(".tachy_offset_bpm", DTzSettingTags.TACHY_OFFSET_BPM, 100, 250, 1, 110));
                settingList.Add(new CTzSettingItem(".tachy_duration", DTzSettingTags.TACHY_DURATION, 0, 600, 2, 30));
                settingList.Add(new CTzSettingItem(".af_duration", DTzSettingTags.AF_DURATION, 0, 60, 1, 5));
                settingList.Add(new CTzSettingItem(".pause_duration", DTzSettingTags.PAUSE_DURATION, 1500, 30000, 2, 3000));
                settingList.Add(new CTzSettingItem(".brady_rate_change", DTzSettingTags.BRADY_RATE_CHANGE, 0, 100, 1, 5));
                settingList.Add(new CTzSettingItem(".tachy_rate_change", DTzSettingTags.TACHY_RATE_CHANGE, 0, 250, 2, 10));

                settingList.Add(new CTzSettingItem(".af_rr_count", DTzSettingTags.AF_RR_COUNT, 32, 64, 1, 64));
                settingList.Add(new CTzSettingItem(".derive_for_analysis", DTzSettingTags.DERIVE_FOR_ANALYSIS, 0, 7, 1, 0));
                settingList.Add(new CTzSettingItem(".rpeak_threshold", DTzSettingTags.RPEAK_THRESHOLD, 0, 1023, 2, 0));

                settingList.Add(new CTzSettingItem(".analysis_mask", DTzSettingTags.ANALYSIS_CHANNEL_MASK, 0, 7, 1, 7));
                settingList.Add(new CTzSettingItem(".detect_pacemaker", DTzSettingTags.DETECT_PACEMAKER, 0, 1, 1, 0));
                settingList.Add(new CTzSettingItem(".cross_channel_analysis", DTzSettingTags.CROSS_CHANNEL_ANALYSIS, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".channel_enable_mask", DTzSettingTags.CHANNEL_ENABLE_MASK, 1, 255, 1, 7));
                settingList.Add(new CTzSettingItem(".use_arrhythmia_state_machine", DTzSettingTags.USE_ARRHYTHMIA_STATE_MACHINE, 0, 1, 1, 0));
                settingList.Add(new CTzSettingItem(".pim_enable", DTzSettingTags.PIM_ENABLE, 0, 255, 1, 31));
                settingList.Add(new CTzSettingItem(".pim_threshold", DTzSettingTags.PIM_THRESHOLD, 0, 100, 1, 10));
                settingList.Add(new CTzSettingItem(".sample_calibration", DTzSettingTags.SAMPLE_CALIBRATION, 0, 200, 1, 0));
                settingList.Add(new CTzSettingItem(".log_qrs_complexes", DTzSettingTags.LOG_QRS_COMPLEXES, 0, 1, 1, 0));

                settingList.Add(new CTzSettingItem(".report_start", DTzSettingTags.REPORT_START, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".report_stop", DTzSettingTags.REPORT_STOP, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".report_break", DTzSettingTags.REPORT_BREAK, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".report_resume", DTzSettingTags.REPORT_RESUME, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".report_full", DTzSettingTags.REPORT_FULL, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".report_pace_detected", DTzSettingTags.REPORT_PACE_DETECT, 0, 1, 1, 0));
                settingList.Add(new CTzSettingItem(".report_analysis_resumed", DTzSettingTags.REPORT_ANALYSIS_RESUMED, 0, 3, 1, 0));
                settingList.Add(new CTzSettingItem(".report_analysis_paused", DTzSettingTags.REPORT_ANALYSIS_PAUSED, 0, 3, 1, 1));
                settingList.Add(new CTzSettingItem(".report_lead_disconnected", DTzSettingTags.REPORT_LEAD_DISCONNECTED, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".report_tachy_onset", DTzSettingTags.REPORT_TACHY_ONSET, 0, 3, 1, 1));
                settingList.Add(new CTzSettingItem(".report_tachy_offset", DTzSettingTags.REPORT_TACHY_OFFSET, 0, 3, 1, 1));
                settingList.Add(new CTzSettingItem(".report_brady_onset", DTzSettingTags.REPORT_BRADY_ONSET, 0, 3, 1, 1));
                settingList.Add(new CTzSettingItem(".report_brady_offset", DTzSettingTags.REPORT_BRADY_OFFSET, 0, 3, 1, 1));
                settingList.Add(new CTzSettingItem(".report_af_onset", DTzSettingTags.REPORT_AF_ONSET, 0, 3, 1, 1));
                settingList.Add(new CTzSettingItem(".report_af_offset", DTzSettingTags.REPORT_AF_OFFSET, 0, 3, 1, 1));
                settingList.Add(new CTzSettingItem(".report_pause", DTzSettingTags.REPORT_PAUSE_ONSET, 0, 3, 1, 1));
                settingList.Add(new CTzSettingItem(".report_patient_event", DTzSettingTags.REPORT_PATIENT_EVENT, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".report_battery_low", DTzSettingTags.REPORT_BATTERY_LOW, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".report_charging_started", DTzSettingTags.REPORT_CHARGING_STARTED, 0, 1, 1, 0));
                settingList.Add(new CTzSettingItem(".report_charging_stopped", DTzSettingTags.REPORT_CHARGING_STOPPED, 0, 1, 1, 0));
                settingList.Add(new CTzSettingItem(".report_server_request", DTzSettingTags.REPORT_SERVER_REQUEST, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".report_pause_offset", DTzSettingTags.REPORT_PAUSE_OFFSET, 0, 3, 1, 0));
                settingList.Add(new CTzSettingItem(".report_scp_retransmit", DTzSettingTags.REPORT_SCP_RETRANSMIT, 0, 1, 1, 0));
                settingList.Add(new CTzSettingItem(".report_tzr_request", DTzSettingTags.REPORT_TZR_REQUEST, 0, 1, 1, 0));
                settingList.Add(new CTzSettingItem(".report_pvc_onset", DTzSettingTags.REPORT_PVC_ONSET, 0, 1, 1, 0));

//                settingList.Add(new CTzSettingItem(".study_hours", DTzSettingTags.STUDY_HOURS, 1, 24 * 31, 2, 24*31));
                settingList.Add(new CTzSettingItem(".invert_scp_ecg", DTzSettingTags.INVERT_SCP_ECG, 0, 7, 1, 0));
                settingList.Add(new CTzSettingItem(".invert_onscreen_ecg", DTzSettingTags.INVERT_ONSCREEN_ECG, 0, 7, 1, 0));
                settingList.Add(new CTzSettingItem(".difference_encoding", DTzSettingTags.SCP_DIFFERENCE_ENCODING, 0, 2, 1, 1));
                settingList.Add(new CTzSettingItem(".sample_rate", DTzSettingTags.SAMPLE_RATE, 200, 512, 2, 250));
                settingList.Add(new CTzSettingItem(".lp_filter", DTzSettingTags.LP_FILTER, 30, 100, 1, 40));
                settingList.Add(new CTzSettingItem(".scp_length", DTzSettingTags.SCP_LENGTH, 10, 60, 1, 30));
                settingList.Add(new CTzSettingItem(".lead_config", DTzSettingTags.LEAD_CONFIG, 0, 5, 1, 0));
                settingList.Add(new CTzSettingItem(".report_onset_pre_time", DTzSettingTags.REPORT_ONSET_PRE_TIME, 1, 240, 1, 1));
                settingList.Add(new CTzSettingItem(".report_onset_post_time", DTzSettingTags.REPORT_ONSET_POST_TIME, 0, 240, 1, 1));
                settingList.Add(new CTzSettingItem(".study_length", DTzSettingTags.STUDY_LENGTH, 0, 300000, 4, 300000));
                settingList.Add(new CTzSettingItem(".bit_resolution", DTzSettingTags.BIT_RESOLUTION, 8, 24, 1, 12));
                settingList.Add(new CTzSettingItem(".huffman_encoding", DTzSettingTags.HUFFMAN_ENCODING, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".digital_hp_filter", DTzSettingTags.DIGITAL_HP_FILTER, 0, 100, 1, 0));
                settingList.Add(new CTzSettingItem(".start_pin_code", DTzSettingTags.START_OK_CODE, 0, 9999, 2, 0));
                settingList.Add(new CTzSettingItem(".digital_lp_filter", DTzSettingTags.DIGITAL_LP_FILTER, 0, 100, 1, 0));
                settingList.Add(new CTzSettingItem(".digital_notch_filter", DTzSettingTags.DIGITAL_NOTCH_FILTER, 0, 100, 1, 60));
                settingList.Add(new CTzSettingItem(".fill_square_waves", DTzSettingTags.FILL_SQUARE_WAVES, 0, 255, 1, 7));
                settingList.Add(new CTzSettingItem(".record_continuous", DTzSettingTags.RECORD_CONTINUOUS, 0, 1, 1, 0));
//do not use                settingList.Add(new CTzSettingItem(".report_offset_pre_time", DTzSettingTags.REPORT_OFFSET_PRE_TIME, 1, 240, 1, 1));
//                settingList.Add(new CTzSettingItem(".report_offset_post_time", DTzSettingTags.REPORT_OFFSET_POST_TIME, 0, 240, 1, 1));
                settingList.Add(new CTzSettingItem(".report_bpm_period", DTzSettingTags.REPORT_BPM_PERIOD, 0, 3600, 2, 10));
                settingList.Add(new CTzSettingItem(".ttm_mode", DTzSettingTags.TTM_MODE, 0, 15, 1, 0 ));
                settingList.Add(new CTzSettingItem(".ttm_top_byte", DTzSettingTags.TTM_TOP_BYTE, 0, 0xff, 1, 0x79));
                settingList.Add(new CTzSettingItem(".ttm_failover_period", DTzSettingTags.TTM_FAILOVER_PERIOD, 1, 1440, 2,240 ));
                settingList.Add(new CTzSettingItem(".ttm_enable", DTzSettingTags.TTM_ENABLE, 0, 7, 1, 7));

//                 settingList.Add(new CTzSettingItem(".allow_low_battery_start", DTzSettingTags.ALLOW_LOW_BATTERY_START, 0, 1, 1, 0));
//                 settingList.Add(new CTzSettingItem(".study_complete_sleep", DTzSettingTags.STUDY_COMPLETE_SLEEP, 0, 180, 1, 300));
                settingList.Add(new CTzSettingItem(".nag_on_lead_off", DTzSettingTags.NAG_ON_LEAD_OFF, 0, 3600, 2, 60));
                settingList.Add(new CTzSettingItem(".max_streaming_delay", DTzSettingTags.MAX_STREAMING_DELAY, 0, 240, 1, 16));
                settingList.Add(new CTzSettingItem(".trim_events", DTzSettingTags.TRIM_EVENTS, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".wireless_enable", DTzSettingTags.WIRELESS_ENABLE, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".auto_answer", DTzSettingTags.AUTO_ANSWER, 0, 1, 1, 0));
                settingList.Add(new CTzSettingItem(".sync_time", DTzSettingTags.SYNC_TIME, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".streaming_mode", DTzSettingTags.STREAMING_MODE, 0, 250, 2, 0 ));
                settingList.Add(new CTzSettingItem(".transmit_interval_reports", DTzSettingTags.TRANSMIT_INTERVAL_REPORTS, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".silent_mode", DTzSettingTags.SILENT_MODE, 0, 1, 1, 0));
                settingList.Add(new CTzSettingItem(".vib_clicks", DTzSettingTags.VIB_CLICKS, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".screen_sleep_time", DTzSettingTags.SCREEN_SLEEP_TIME, 5, 180, 1, 20));
                settingList.Add(new CTzSettingItem(".demo_mode", DTzSettingTags.DEMO_MODE, 0, 2, 1, 0));
                settingList.Add(new CTzSettingItem(".sms_enable", DTzSettingTags.SMS_ENABLE, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".voice_enable", DTzSettingTags.VOICE_ENABLE, 0, 1, 1, 0));
                settingList.Add(new CTzSettingItem(".check_actions_interval", DTzSettingTags.CHECK_ACTIONS_INTERVAL, 0, 240, 1, 30));
                settingList.Add(new CTzSettingItem(".symptom_diary_entries", DTzSettingTags.SYMPTOM_DIARY_COUNT, 0, 10, 1, 0));
                settingList.Add(new CTzSettingItem(".activity_diary_entries", DTzSettingTags.ACTIVITY_DIARY_COUNT, 0, 10, 1, 0));
                settingList.Add(new CTzSettingItem(".stagger_interval_uploads", DTzSettingTags.STAGGER_INTERVAL_UPLOADS, 0, 60, 1, 30));
                settingList.Add(new CTzSettingItem(".long_event_threshold", DTzSettingTags.LONG_EVENT_THRESHOLD, 1, 255, 1, 12));
                settingList.Add(new CTzSettingItem(".long_event_voltage", DTzSettingTags.LONG_EVENT_VOLTAGE, 0, 4200, 2, 0));
                settingList.Add(new CTzSettingItem(".modem_voltage", DTzSettingTags.MODEM_VOLTAGE, 3350, 3800, 2, 3500));
                settingList.Add(new CTzSettingItem(".modem_temperature", DTzSettingTags.MODEM_TEMPERATURE, 0, 100, 1, 50));
                settingList.Add(new CTzSettingItem(".remember_last_diary", DTzSettingTags.REMEMBER_LAST_DIARY, 0, 1, 1, 1));

                settingList.Add(new CTzSettingItem(".symptom_entry_1", DTzSettingTags.DIARY_SYMPTOM_1, 28));
                settingList.Add(new CTzSettingItem(".symptom_entry_2", DTzSettingTags.DIARY_SYMPTOM_2, 28));
                settingList.Add(new CTzSettingItem(".symptom_entry_3", DTzSettingTags.DIARY_SYMPTOM_3, 28));
                settingList.Add(new CTzSettingItem(".symptom_entry_4", DTzSettingTags.DIARY_SYMPTOM_4, 28));
                settingList.Add(new CTzSettingItem(".symptom_entry_5", DTzSettingTags.DIARY_SYMPTOM_5, 28));
                settingList.Add(new CTzSettingItem(".symptom_entry_6", DTzSettingTags.DIARY_SYMPTOM_6, 28));
                settingList.Add(new CTzSettingItem(".symptom_entry_7", DTzSettingTags.DIARY_SYMPTOM_7, 28));
                settingList.Add(new CTzSettingItem(".symptom_entry_8", DTzSettingTags.DIARY_SYMPTOM_8, 28));
                settingList.Add(new CTzSettingItem(".symptom_entry_9", DTzSettingTags.DIARY_SYMPTOM_9, 28));
                settingList.Add(new CTzSettingItem(".symptom_entry_10", DTzSettingTags.DIARY_SYMPTOM_10, 28));
                settingList.Add(new CTzSettingItem(".activity_entry_1", DTzSettingTags.DIARY_ACTIVITY_1, 28));
                settingList.Add(new CTzSettingItem(".activity_entry_2", DTzSettingTags.DIARY_ACTIVITY_2, 28));
                settingList.Add(new CTzSettingItem(".activity_entry_3", DTzSettingTags.DIARY_ACTIVITY_3, 28));
                settingList.Add(new CTzSettingItem(".activity_entry_4", DTzSettingTags.DIARY_ACTIVITY_4, 28));
                settingList.Add(new CTzSettingItem(".activity_entry_5", DTzSettingTags.DIARY_ACTIVITY_5, 28));
                settingList.Add(new CTzSettingItem(".activity_entry_6", DTzSettingTags.DIARY_ACTIVITY_6, 28));
                settingList.Add(new CTzSettingItem(".activity_entry_7", DTzSettingTags.DIARY_ACTIVITY_7, 28));
                settingList.Add(new CTzSettingItem(".activity_entry_8", DTzSettingTags.DIARY_ACTIVITY_8, 28));
                settingList.Add(new CTzSettingItem(".activity_entry_9", DTzSettingTags.DIARY_ACTIVITY_9, 28));
                settingList.Add(new CTzSettingItem(".activity_entry_10", DTzSettingTags.DIARY_ACTIVITY_10, 28));

                settingList.Add(new CTzSettingItem(".patient_id", DTzSettingTags.PATIENT_ID, 40));
                settingList.Add(new CTzSettingItem(".event_static_payload", DTzSettingTags.EVENT_STATIC_PAYLOAD, 40));
                settingList.Add(new CTzSettingItem(".incoming_number", DTzSettingTags.INCOMING_NUMBER, 16));
                settingList.Add(new CTzSettingItem(".center_name", DTzSettingTags.CENTER_NAME, 28));
                settingList.Add(new CTzSettingItem(".center_number", DTzSettingTags.CENTER_NUMBER, 28));

                settingList.Add(new CTzSettingItem(".server_address", DTzSettingTags.FTP_ADDRESS, 255));
                settingList.Add(new CTzSettingItem(".server_user_name", DTzSettingTags.FTP_USER_NAME, 255));
                settingList.Add(new CTzSettingItem(".server password", DTzSettingTags.FTP_PASSWORD, 255));
                settingList.Add(new CTzSettingItem(".use_http", DTzSettingTags.USE_HTTP, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".check_for_clean_server", DTzSettingTags.CHECK_FOR_CLEAN_SERVER, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".verify_server_changes", DTzSettingTags.VERIFY_SERVER_CHANGES, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".wait_for_http_response", DTzSettingTags.WAIT_FOR_HTTP_RESPONSE, 0, 1, 1, 1));
                settingList.Add(new CTzSettingItem(".redownload_delay", DTzSettingTags.REDOWNLOAD_DELAY, 0, 240, 1, 0));
                settingList.Add(new CTzSettingItem(".connection_timeout", DTzSettingTags.CONNECTION_TIMEOUT, 0, 240, 1, 5));

                settingList.Add(new CTzSettingItem(".error_retries", DTzSettingTags.ERROR_RETRIES, 0, 250, 1, 10));
                settingList.Add(new CTzSettingItem(".error_period", DTzSettingTags.ERROR_PERIOD, 1, 1440, 2, 30));
                settingList.Add(new CTzSettingItem(".update_defaults", DTzSettingTags.UPDATE_DEFAULTS, 0, 1, 1, 0));
                settingList.Add(new CTzSettingItem(".menu_language", DTzSettingTags.MENU_LANGUAGE, 0, 0, 1, 0));
                settingList.Add(new CTzSettingItem(".Compatible_TzAera", DTzSettingTags.COMPATIBLE_AERA, 0, 1, 1,1 ));
            }
            #endregion
            serialNumber = ASerialNumber;
        }

        public void mClear(string ASerialNumber)
        {
            if( settingList != null ) settingList.Clear();

            firmwareVersion = 10;
            serialNumber = ASerialNumber;
            deviceID = "TZMR\0\0";
            fileID = 0;
 
            file_aesKey = new Byte[16];
            file_aesIV = new Byte[16];
            file_aesKeySet = false;
            file_aesIVSet = false;
            setting_aesKey = new Byte[16];
            setting_aesIV = new Byte[16];
            setting_aesKeySet = false;
            setting_aesIVSet = false;
            setting_aesKey_detected = false;
            setting_aesIV_detected = false;

            end_year = end_month = end_day = end_hour = 0;
            study_end_set = false;
    }

    //-----------------------------------------------------------------------------
    //    crcBlock()
    //
    //    This function calculates the CRC-CCITT value for a block of data, given
    //    a seed value and the number of bytes to process.
    //-----------------------------------------------------------------------------
  /*  int crcBlock(byte[] data, uint AIndex, uint length, ref UInt16 crcVal)
        {
            uint j;
            for (j = 0; j < length; j++)
            {
                crcVal = (UInt16)(ccitt_crc16_table[(data[j] ^ (byte)(crcVal >> 8)) & 0xff] ^ (UInt16)(crcVal << 8));
            }
            return 0;
        }
        */
        public static Int32 crcBlock(byte[] data, UInt32 AStartOffset, UInt32 length, ref UInt16 ArCrcVal)
        {
            UInt32 j, i = AStartOffset;
            UInt16 crcVal = ArCrcVal;

            if (0 == length)
            {
                CProgram.sLogError("Attempting to CRC 0 bytes! Aborting.");
                return -1;
            }
            for (j = 0; j < length; j++, ++i)
            {
                crcVal = (UInt16)(ccitt_crc16_table[(data[i] ^ (crcVal >> 8)) & 0xff] ^ (crcVal << 8));
            }
            ArCrcVal = crcVal;
            return 0;
        }


        ///////////////////////////////////////////////////////////////////////////////
        //       Public Function
        ///////////////////////////////////////////////////////////////////////////////

        /*    void gStrCpyN(char* ApTo, const char* ApFrom, size_t AN)
            // strncpy_s puts chars beyond the \0, they must be \0 for TZ to function!!!!

            {
                if (ApTo != NULL && ApFrom != NULL && AN > 0)
                {
                    while (--AN >= 0 && *ApFrom != '\0')
                    {
                        *ApTo++ = *ApFrom++;
                    }
                    *ApTo = '\0';
                }
            }
            */

        bool mbCharHexToByte(char AChar, ref Byte AByte)
        {
            bool bOk = false;

            if (AChar >= '0' && AChar <= '9')
            {
                AByte |= (Byte)(AChar - '0');
                bOk = true;
            }
            else if (AChar >= 'A' && AChar <= 'F')
            {
                AByte |= (Byte)((AChar - 'A')+10);
                bOk = true;
            }
            else if (AChar >= 'a' && AChar <= 'f')
            {
                AByte |= (Byte)((AChar - 'a')+10);
                bOk = true;
            }
            return bOk;

        }
        public string mMakeHexStr(Byte[] AByteArray, UInt32 AIndex, UInt16 ALength)
        {
            string s = "";
            int i;
            byte xor = 0;
            for( i = 0; i < ALength; ++i)
            {
                xor |= AByteArray[AIndex + i];
            }
            if( 0 != xor )
            {
                for (i = 0; i < ALength; ++i)
                {
                    s += AByteArray[AIndex + i].ToString( "X02");
                }
            }
            return s;
        }
        public bool mbReadHex(string AValue, ref Byte[] AByteArray, UInt32 AIndex, UInt16 ALength, out bool AbSet)
        {
            bool bOk = false;

            AbSet = false;
            if (ALength > 0 && AValue.Length == ALength * 2 && AByteArray.Length >= AIndex + ALength)
            {
                int j = 0;

                bOk = true;
                for (int i = 0; i < ALength; ++i)
                {
                    byte k = 0;

                    if (j + 1 <= AValue.Length)
                    {
                        if (false == mbCharHexToByte(AValue[j++], ref k))
                        {
                            bOk = false;
                            break;
                        }
                        k <<= 4;
                        if (false == mbCharHexToByte(AValue[j++], ref k))
                        {
                            bOk = false;
                            break;
                        }
                    }
                    AByteArray[AIndex + i] = k;
                }
                AbSet = bOk;
            }
            else
            {
                if( AValue.Length == 0)
                {
                    // empty make all 0
                    for (int i = 0; i < ALength; ++i)
                    {
                        AByteArray[AIndex + i] = 0;
                    }
                    bOk = true;
                }
            }
            return bOk;
        }
        public bool mbReadBool(string AValue, ref bool ABool)
        {
            bool bOk = false;
            string value = AValue.ToUpper();

            if (value == "TRUE" || value == "1" || value == "T")
            {
                ABool = true;
                bOk = true;
            }
            else if (value == "FALSE" || value == "0" || value == "F")
            {
                ABool = false;
                bOk = true;
            }
            return bOk;
        }

        public CTzSettingItem mFindSettingItem(string AName)
        {
            CTzSettingItem foundItem = null;

            foreach (CTzSettingItem item in settingList)
            {
                if (AName.Contains(item.parseStr))
                {
                    foundItem = item;
                    break;
                }
            }
            return foundItem;
        }
        public CTzSettingItem mFindSettingTag(Byte ATag)
        {
            CTzSettingItem foundItem = null;

            foreach (CTzSettingItem item in settingList)
            {
                if ((Byte)item.tag == ATag)
                {
                    foundItem = item;
                    break;
                }
            }
            return foundItem;
        }
        public bool mbSetSettingUInt32(DTzSettingTags ATag, UInt32 AValue)
        {
            return mbSetSettingUInt32((Byte)ATag, AValue);
        }
        public bool mbSetSettingUInt32( Byte ATag, UInt32 AValue )
        {
            bool bOk = false;

            CTzSettingItem item = mFindSettingTag(ATag);

            if( item != null)
            {
                if( item.mbIsBinary())
                {
                    UInt32 value = AValue;
                    if (value > item.max) value = (UInt32)item.max;
                    if (value < item.min) value = (UInt32)item.min;
                    item.dataVal = value;
                    item.setFlag = 1;
                    bOk = true;
                }
                else
                {
                    CProgram.sLogError("Tz set variable not Binary, tag = " + ATag.ToString());
                }
            }

            return bOk;
        }
        public bool mbSetSettingBool(DTzSettingTags ATag, bool AValue)
        {
            return mbSetSettingBool((Byte)ATag, AValue);
        }
        public bool mbSetSettingBool(Byte ATag, bool AValue)
        {
            bool bOk = false;

            CTzSettingItem item = mFindSettingTag(ATag);

            if (item != null)
            {
                if (item.mbIsBinary())
                {
                    item.dataVal = (UInt32)(AValue ? 1 : 0);
                    item.setFlag = 1;
                    bOk = true;
                }
                else
                {
                    CProgram.sLogError("Tz set variable not Binary, tag = " + ATag.ToString());
                }
            }
            return bOk;
        }
        public bool mbSetSettingString(DTzSettingTags ATag, string AValue)
        {
            return mbSetSettingString((Byte)ATag, AValue);
        }
        public bool mbSetSettingString(Byte ATag, string AValue)
        {
            bool bOk = false;

            CTzSettingItem item = mFindSettingTag(ATag);

            if (item != null)
            {
                if (false == item.mbIsBinary())
                {
                    int len = AValue != null ? AValue.Length : 0;
                    item.dataStr = len <= item.maxLength ?  AValue : AValue.Substring(0, item.maxLength );
                    item.setFlag = 1;
                    bOk = true;
                }
                else
                {
                    CProgram.sLogError("Tz set variable not string, tag = " + ATag.ToString());
                }
            }
            return bOk;
        }

        public UInt32 mGetSettingUInt32(DTzSettingTags ATag)
        {
            return mGetSettingUInt32((Byte)ATag);
        }
            public UInt32 mGetSettingUInt32(Byte ATag)
        {
            UInt32 value = 0;

            CTzSettingItem item = mFindSettingTag(ATag);

            if (item != null)
            {
                if (item.mbIsBinary())
                {
                    value = (UInt32)item.dataVal;
                    if (item.setFlag != 1)
                    {
                        CProgram.sLogLine("Tz get variable not set, tag = " + ATag.ToString());
                    }
                    if (value > item.max) value = (UInt32)item.max;
                    if (value < item.min) value = (UInt32)item.min;
                }
                else
                {
                    CProgram.sLogError("Tz get variable not Binary, tag = " + ATag.ToString());
                }
            }
            return value;
        }

        public bool mGetSettingBool(DTzSettingTags ATag)
        {
            return mGetSettingBool((Byte)ATag);
        }
        public bool mGetSettingBool(Byte ATag)
        {
            bool value = false;

            CTzSettingItem item = mFindSettingTag(ATag);

            if (item != null)
            {
                if (item.mbIsBinary())
                {
                    value = (UInt32)item.dataVal != 0;
                    if (item.setFlag != 1)
                    {
                        CProgram.sLogLine("Tz get variable not set, tag = " + ATag.ToString());
                    }
                }
                else
                {
                    CProgram.sLogError("Tz get variable not Binary, tag = " + ATag.ToString());
                }
            }
            return value;
        }

        public string mGetSettingString(DTzSettingTags ATag)
        {
            return mGetSettingString((Byte)ATag);
            
        }
        public string mGetSettingString( Byte ATag )
        {
            string value = "";

            CTzSettingItem item = mFindSettingTag(ATag);

            if (item != null)
            {
                if (false == item.mbIsBinary())
                {
                    value = item.dataStr;
                    if( item.setFlag != 1)
                    {
                        CProgram.sLogLine("Tz get variable not set, tag = " + ATag.ToString());
                    }
                }
                else
                {
                    CProgram.sLogError("Tz get variable not string, tag = " + ATag.ToString());
                }
            }
            return value;
        }

        public bool mbReadSettingsTxt(string AFileName)
        {
            bool bOk = false;

            try
            {
                StreamReader sr = new StreamReader(AFileName);

                if (sr == null)
                {
                    CProgram.sLogError("Failed to open " + AFileName);
                }
                else
                {
                    UInt32 line_number = 0;

                    CProgram.sLogLine("Reading file " + AFileName);
                    bOk = true;

                    while (false == sr.EndOfStream)
                    {
                        string line = sr.ReadLine();

                        line_number++;
                        CProgram.sLogLine(line_number.ToString("D03") + ": " + line);

                        if (line != null)
                        {
                            line.Replace('\t', ' ');
                            line = line.Trim();

                        }
                        // Only process lines that have more than 8 characters in them
                        if (line.Length > 8 && line[0] != '#')// Skip the line if it is a comment
                        {
                            String[] parts = line.Split('=');
                            if (parts != null)
                            {
                                string varName = parts[0].Trim();
                                string varValue = parts.Length > 1 ? parts[1] : "";

                                if (varName.StartsWith("file."))
                                {
                                    if (varName.Contains("firmware_version")) UInt16.TryParse(varValue, out firmwareVersion);
                                    else if (varName.Contains("serial_number")) ;// do not overwite  serialNumber = varValue;
                                    else if (varName.Contains("device_id")) deviceID = varValue;
                                    else if (varName.Contains("file_id")) UInt16.TryParse(varValue, out fileID);
                                    else if (varName.Contains("aes_iv"))
                                    {
                                        if (false ==  mbReadHex(varValue, ref file_aesIV, 0, 16, out file_aesKeySet))
                                        {
                                            CProgram.sLogError("Failed to read hex code: " + line);
                                            bOk = false;
                                        }
                                    }
                                    else if (varName.Contains("aes_key"))
                                    {
                                        if (false == mbReadHex(varValue, ref file_aesKey, 0, 16, out file_aesIVSet))
                                        {
                                            CProgram.sLogError("Failed to read hex code: " + line);
                                            bOk = false;
                                        }
                                    }
                                    else if (varName.Contains("check_setting_ranges")) mbReadBool(varValue, ref checkInputData);
                                    else if (varName.Contains("insert_bad_alignment")) mbReadBool(varValue, ref badAlignment);
                                    else if (varName.Contains("insert_bad_crc")) mbReadBool(varValue, ref badCRC);
                                    else if (varName.Contains("insert_bad_length")) mbReadBool(varValue, ref badLength);
                                    else
                                    {
                                        CProgram.sLogError("Unknown File setting: " + line);
                                        bOk = false;
                                    }
                                }
                                else if (varName.StartsWith("setting."))
                                {
                                    CTzSettingItem item = mFindSettingItem(varName);

                                    if (item != null)
                                    {
                                        item.set(varValue, checkInputData, line_number);
                                    }
                                    else if (varName.Contains("aes_key"))
                                    {
                                        if (false == mbReadHex(varValue, ref setting_aesKey, 0, 16, out setting_aesKeySet))
                                        {
                                            CProgram.sLogError("Failed to read hex code: " + line);
                                            bOk = false;
                                        }
                                    }
                                    else if (varName.Contains("aes_iv"))
                                    {
                                        if (false == mbReadHex(varValue, ref file_aesIV, 0, 16, out setting_aesIVSet))
                                        {
                                            CProgram.sLogError("Failed to read hex code: " + line);
                                            bOk = false;
                                        }
                                    }
                                    else if (varName.Contains("study_end_date_time"))
                                    {
                                        char[] splitChars = { '-', '/', '\\', ' ' };
                                        string[] dateParts = varValue.Split(splitChars);
                                        bool bDateOk = dateParts != null && dateParts.Length >= 3;

                                        if (bDateOk)
                                        {
                                            end_year = end_month = end_day = end_hour = 0;
                                            uint.TryParse(dateParts[0], out end_year);
                                            uint.TryParse(dateParts[1], out end_month);
                                            uint.TryParse(dateParts[2], out end_day);
                                            if (dateParts.Length > 3) uint.TryParse(dateParts[3], out end_hour);
                                            study_end_set = bDateOk = false == checkInputData || end_year > 0 && end_month > 0 && end_month <= 12 && end_day > 0 && end_day <= 32;
                                        }
                                        if (false == bDateOk)
                                        {
                                            CProgram.sLogError("Bad YY/MM/DD HH  setting: " + line);
                                            bOk = false;
                                        }
                                    }
                                    else
                                    {
                                        CProgram.sLogError("Unknown setting: " + line);
                                        bOk = false;
                                    }

                                }
                                else if( line.StartsWith("Reading"))
                                {
                                    // first line reading file
                                }
                                else
                                {
                                    CProgram.sLogError("Unknown line: " + line);
                                    bOk = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Error reading " + AFileName, ex);
                bOk = false;
            }
            return bOk;
        }

        public static void sCopyString2Buffer( Byte[] ABuffer, ref UInt32 AIndex, string AString, UInt16 ALength)
        {
            int l = AString.Length;

            for( int i = 0; i < ALength; ++i )
            {
                ABuffer[AIndex++] = i < l ? (Byte)AString[i] : (Byte)0;
            }
        }
        public static void sCopyBuffer2Buffer(Byte[] ABuffer, ref UInt32 AIndex, Byte[] AFrom, UInt16 ALength)
        {
            int l = AFrom.Length;

            for (int i = 0; i < ALength; ++i)
            {
                ABuffer[AIndex++] = i < l ? AFrom[i] : (Byte)0;
            }
        }
        public static void sCopyUInt2Buffer(Byte[] ABuffer, ref UInt32 AIndex, UInt32 AValue, UInt16 ALength)
        {
            UInt32 a = AValue;                 // litle endian, lowest byte first -> intel, STM32

            for (int i = 0; i < ALength; ++i)
            {
                ABuffer[AIndex++] = (Byte)(a & 0x00FF);
                a >>= 8;
            }
        }

        public UInt32 mGenerateSettings(Byte[] ABuffer, UInt32 AMaxSize, string ASerialNr, UInt16 AFileNumber)
        {
            UInt32 i = 0;
            uint length;                         // The length read from the file

            for (i = 0; i < AMaxSize; ++i) ABuffer[i] = 0;

            i = 6;                                       // The first 6 bytes are length and CRC

            sCopyString2Buffer(ABuffer, ref i, _cSettingsIdString, 6);// The format identifier string

            sCopyString2Buffer(ABuffer, ref i, deviceID, 6);   // The device ID string
            
            ABuffer[i++] = (Byte)firmwareVersion;                // This byte is F. Vers. (*10)

            // strncpy_s puts chars beyond the \0, they must be \0 for TZ to function!!!!
            if (deviceID.Contains("TZMR"))
            {
                sCopyString2Buffer(ABuffer, ref i, ASerialNr, 8);      // The next 8 bytes are the serial number
            }
            else if (deviceID.Contains("H3R"))
            {
                sCopyString2Buffer(ABuffer, ref i, ASerialNr, 11);      // The next 11 bytes are the serial number
            }

            UInt32 fileID = mCheckFileID(ASerialNr, "Settings", AFileNumber);

            sCopyUInt2Buffer(ABuffer, ref i, fileID, 2 );

            foreach(CTzSettingItem item in settingList)
            {
                i += item.write(ABuffer, i, badAlignment);
            }

            if (study_end_set)
            {
                ABuffer[i++] = (Byte)DTzSettingTags.STUDY_END_DATE_TIME;
                ABuffer[i++] = 5;
                sCopyUInt2Buffer(ABuffer, ref i, end_year, 2);
                ABuffer[i++] = (Byte)end_month;
                ABuffer[i++] = (Byte)end_day;
                ABuffer[i++] = (Byte)end_hour;
                ABuffer[i++] = 0;
            }

            if (setting_aesKeySet && setting_aesIVSet)
            {
                ABuffer[i++] = (Byte)DTzSettingTags.AES_KEY;
                ABuffer[i++] = 16;
                sCopyBuffer2Buffer( ABuffer, ref i, setting_aesKey, 16 );
                ABuffer[i++] = 0;
                ABuffer[i++] = (Byte)DTzSettingTags.AES_IV;
                ABuffer[i++] = 16;
                sCopyBuffer2Buffer(ABuffer, ref i, setting_aesIV, 16);
                ABuffer[i++] = 0;
            }
            else if (setting_aesKey_detected && setting_aesIV_detected)
            {
                ABuffer[i++] = (Byte)DTzSettingTags.AES_IV;
                ABuffer[i++] = 0;
                ABuffer[i++] = 0;
                ABuffer[i++] = (Byte)DTzSettingTags.AES_KEY;
                ABuffer[i++] = 0;
                ABuffer[i++] = 0;
            }

            ABuffer[i++] = (Byte)DTzSettingTags.TERM_TAG;
            ABuffer[i++] = 1;
            ABuffer[i++] = 255;
            ABuffer[i++] = 0;

            length = i;

            i = 2;
            UInt32 temp = length;
            if (badLength) temp += 255;

            sCopyUInt2Buffer(ABuffer, ref i, temp, 2);
            
            ushort crcValue = 0xffff;

            crcBlock(ABuffer, 2, length - 2, ref crcValue);

            i = 0;
            temp = crcValue;
            if (badCRC) temp++;
            sCopyUInt2Buffer(ABuffer, ref i, temp, 2);
            
            if (file_aesKeySet && file_aesIVSet)
            {
                CProgram.sLogError("File encryption is not supported");
                length = 0;
            }

            return length;   // return 0 if failed
        }

        public bool mbWriteSettings(string AFileName, string ASerialNr, UInt16 AFileNumber, 
            bool AbCopyToServer, out bool ArbCopyOk)
        {
            bool bOk = false;
            bool bCopyOk = false;
            Byte[] buffer;
            UInt32 size = 0;

            try
            {
                buffer = new Byte[_cMaxSettingsFileLength];

                if (buffer != null)
                {
                    size = mGenerateSettings(buffer, (UInt32)buffer.Length, ASerialNr, AFileNumber);

                    if (size == 0)
                    {
                        CProgram.sLogError("Error generating settings for " + AFileName);
                        bOk = false;
                    }
                    else
                    {
                        if (File.Exists(AFileName))
                        {
                            File.Delete(AFileName);
                        }
                        using (FileStream fs = File.Create(AFileName))
                        {
                            fs.Write(buffer, 0, (int)size);
                            fs.Flush();
                            fs.Close();
                            bOk = true;
                        }
                        if( bOk && AbCopyToServer)
                        {
                            string copyFileName = AFileName;
                            try
                            {
                                if (sbMakeToServerFileName(ref copyFileName))
                                {
                                    using (FileStream fs = File.Create(copyFileName))
                                    {
                                        fs.Write(buffer, 0, (int)size);
                                        fs.Flush();
                                        fs.Close();
                                        bCopyOk = true;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                CProgram.sLogException("Error writing copy " + copyFileName, ex);
                                bCopyOk = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Error writing " + AFileName, ex);
                bOk = false;
            }
            ArbCopyOk = bCopyOk;
            return bOk;
        }

        public string mParseBuffer2String(Byte[] ABuffer, ref UInt32 AIndex, UInt16 ALength)
        {
            string s = "";
            bool bEnd = false;

            for (int i = 0; i < ALength; ++i)
            {
                if( false == bEnd)
                {
                    char c = (char)ABuffer[AIndex++];

                    if( c == 0)
                    {
                        bEnd = true;
                    }
                    else
                    {
                        s += c;
                    }
                }
                else
                {
                    ++AIndex;
                }
            }
            return s;
        }
        public Byte[] mParseBuffer(Byte[] ABuffer, ref UInt32 AIndex, UInt16 ALength)
        {
            Byte[] data = new Byte[ALength];
            
            for (int i = 0; i < ALength; ++i)
            {
                data[i] = ABuffer[AIndex++];
            }
            return data;
        }
        public UInt32 mParseBuffer2Uint32(Byte[] ABuffer, ref UInt32 AIndex, UInt16 ALength)
        {
            UInt32 a = 0;                 // litle endian, lowest byte first -> intel, STM32
            UInt16 shift = 0;

            for (int i = 0; i < ALength; ++i)
            {
                a |= (UInt32)(ABuffer[AIndex++] << shift);
                shift += 8;
            }
            return a;
        }


        public bool mbReadSettings(string AFileName, bool AbClear, bool AbSetDefaults, string ASerialNumber)
        {
            bool bOk = false;
            Byte[] buffer;
            UInt32 size = 0;

            try
            {
                if( AbClear)
                {
                    mClear(ASerialNumber);
                }
                if ( AbSetDefaults)
                {
                    mSetDefaults(ASerialNumber);
                }
                buffer = new Byte[_cMaxSettingsFileLength];

                if (buffer != null)
                {
                    if (File.Exists(AFileName))
                    {
                        using (FileStream fs = File.OpenRead(AFileName))
                        {
                            size = (UInt32)fs.Length;
                            if( size > _cMaxSettingsFileLength)
                            {
                                CProgram.sLogError("Settings file " + AFileName + "to big");
                                bOk = false;
                            }
                            else
                            {
                                fs.Read(buffer, 0, (int)size);

                                bOk = mbParseSettings(buffer, size);

                                if (bOk == false)
                                {
                                    CProgram.sLogError("Error parsing settings for " + AFileName);
                                }

                            }
                        }
                    }
                    else
                    {
                        CProgram.sLogError("No settings file " + AFileName);
                        bOk = false;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Error writing " + AFileName, ex);
                bOk = false;
            }
            return bOk;
        }

        private void mLogSettingString(string AName, string AValue)
        {
            if (mbLog)
            {
                CProgram.sLogInfo(AName + "=" + AValue);

            }
        }
        private void mLogSettingRemark(string AName)
        {
            if (mbLog)
            {
                CProgram.sLogInfo("#" + AName);

            }
        }
        private void mLogSettingUInt32(string AName, UInt32 AValue)
        {
            if (mbLog)
            {
                CProgram.sLogInfo(AName + "=" + AValue.ToString());
            }
        }

        private void mLogSettingBytes(string AName, Byte[] AData, UInt32 ALength)
        {
            if (mbLog)
            {
                string value = "";

                if( AData != null )
                {
                    int l = AData.Length;

                    if (ALength < l) l = (int)ALength;

                    for (int i = 0; i < l; ++i )
                    {
                        value += AData[i].ToString("X02");
                    }
                }
                CProgram.sLogInfo(AName + "=" + value);
            }
        }

        bool mbParseSettings(Byte[] ABuffer, UInt32 Aize )
        {
            bool bOk = false;
            UInt32 i = 6;


            //            unsigned char firstBlock[16];
            //          unsigned char tempKey[16];
            //           unsigned char tempIV[16];

            //            unsigned int i;

            //            aes_context aes_ctx;
            string str = mParseBuffer2String(ABuffer, ref i, 6 );

            if (str != _cSettingsIdString)
            {
                CProgram.sLogError("Not a TZ settings file ( or encypted)n");
            }
            else
            {
                bool bIsProgrammer = CLicKeyDev.sbDeviceIsProgrammer();

//                mLogSettingString("file.aes_key", "");
//                mLogSettingString("file.aes_iv", "");

                ushort ourcrcValue = 0xffff;                        // The CRC we calculate, CRC is based off an initial value of 0xffff

                i = 0;
                ushort theircrcValue = (ushort)mParseBuffer2Uint32(ABuffer, ref i, 2);                 // The CRC from the file
                ushort length = (ushort)mParseBuffer2Uint32(ABuffer, ref i, 4);

                crcBlock(ABuffer, 2, (uint)(length - 2), ref ourcrcValue); // Calculate the CRC for the remainder of the file

                if (bIsProgrammer)
                {
                    mLogSettingRemark("crc file=" + theircrcValue.ToString("X04") + " calc=" + ourcrcValue.ToString("X04"));
                }
                bOk = true;
                if (mbCheckCrc)
                {
                    if (ourcrcValue != theircrcValue)
                    {
                        bOk = false;
                        CProgram.sLogError("CRC settings file not ok");
                    }
                }
                if (bOk)
                {
                    UInt32 nTagZero = 0;
                    if (bIsProgrammer)
                    {
                        mLogSettingRemark("Format Identifier String: " + str);
                    }
                    i += 6;
                    deviceID = mParseBuffer2String(ABuffer, ref i, 6);

                    firmwareVersion = ABuffer[i++];                // This byte is F. Vers. (*10)

                    if (deviceID.Contains("TZMR"))
                    {
                        serialNumber = mParseBuffer2String(ABuffer, ref i, 8);    // The next 8 bytes are the serial number
                    }
                    else if (deviceID.Contains("H3R"))
                    {
                        serialNumber = mParseBuffer2String(ABuffer, ref i, 11);    // The next 11 bytes are the serial number
                    }
                    fileID = (ushort)mParseBuffer2Uint32(ABuffer, ref i, 2);

                    while (i < (length - 2))
                    {                 // Parse the events until the end of the file
                        Byte settingTag = ABuffer[i++];                            // Setting Tag
                        uint iNext = i;

                        if (settingTag == 0)
                        {
                            ++nTagZero;
                        }
                        else
                        {
                            ushort settingLength = ABuffer[i++];                         // Setting Length
                            CTzSettingItem item = mFindSettingTag(settingTag);
                            iNext = i + settingLength;

                            if (item != null)
                            {
                                if (item.mbIsBinary())
                                {
                                    item.dataVal = mParseBuffer2Uint32(ABuffer, ref i, settingLength);
                                }
                                else
                                {
                                    item.dataStr = mParseBuffer2String(ABuffer, ref i, settingLength);
                                }
                                item.setFlag = 1;
                            }
                            else
                            {
                                if (settingTag == (Byte)DTzSettingTags.AES_KEY)
                                {
                                    setting_aesKey = mParseBuffer(ABuffer, ref i, settingLength);
                                    setting_aesKeySet = true;
                                }
                                else if (settingTag == (Byte)DTzSettingTags.AES_IV)
                                {
                                    setting_aesIV = mParseBuffer(ABuffer, ref i, settingLength);
                                    setting_aesIVSet = true;
                                }
                                else if (settingTag == (Byte)DTzSettingTags.STUDY_END_DATE_TIME)
                                {
                                    end_year = end_month = end_day = end_hour = 0;
                                    if (settingLength >= 5)
                                    {
                                        end_year = mParseBuffer2Uint32(ABuffer, ref i, 2);
                                        end_month = ABuffer[i++];
                                        end_day = ABuffer[i++];
                                        end_hour = ABuffer[i++];
                                        study_end_set = true;
                                    }
                                }
                                else if (settingTag != (Byte)DTzSettingTags.TERM_TAG)
                                {
                                    //bOk = false;
                                    CProgram.sLogWarning("Read unknown setting tag " + settingTag.ToString("X02") + " length data = " + settingLength);
                                }
                            }
                        }
                        i = iNext;
                    }
                    if (bOk && ABuffer[i] != '\0')
                    {
                        CProgram.sLogError("Missing closing 0 tag ");
                    }
                }
            }
            return bOk;
        }

        public string mGetSettingsFileName( string ASerialNr)
        {
            return ASerialNr+".tzs";
        }
        public string mGetSettingsCardFileName()
        {
            return "setting.tzs";
        }
        public string mGetActionsFileName(string ASerialNr)
        {
            return ASerialNr + ".tza";
        }

        // Action derived from TZ actionsGenerator.cpp

        /*****************************************************************************/

        public UInt16 mCheckFileID( string ASerialNr, string AFileType, UInt16 AFileID)
        {
            UInt16 fileID = AFileID;

            if (CLicKeyDev.sbDeviceIsProgrammer())
            {
                if (fileID == _cFileID_NOT_ALLOWED) // 0 is not allowed
                {
                    CProgram.sPromptError(true, "Write " + AFileType + " to " + ASerialNr, AFileType + " FileId = 0 not allowed");
                    fileID = _cFileID_BAD;
                }
                else
                    if( fileID == _cFileID_UNKNOWN || fileID == _cFileID_EMPTY)
                {
                    // ok this is a default or an empty
                }
                else if (fileID >= _cFileID_BIG)
                {
                    CProgram.sPromptError(true, "Write " + AFileType + " to " + ASerialNr, AFileType + " big FileId = " + fileID);
                }
            }
            else
            {
                if (fileID == _cFileID_NOT_ALLOWED) // 0 is not allowed
                {
                    CProgram.sLogError("Write " + AFileType + " to " + ASerialNr + " FileId = 0 not allowed");
                    fileID = _cFileID_BAD;
                }
                else
                    if (fileID == _cFileID_UNKNOWN || fileID == _cFileID_EMPTY)
                {
                    // ok this is a default or an empty
                }
                else if (fileID >= _cFileID_BIG)
                {
                    CProgram.sLogError("Write " + AFileType + " to " + ASerialNr + " big FileId = " + fileID);
                }
            }

            return fileID;

        }
        public Byte[] mActionCreateBuffer(out UInt32 AIndex, string ASerialNr, UInt16 AActionFileNr )
        {
            uint firmwareVersion = 10;
            uint fileID = mCheckFileID( ASerialNr, "Action", AActionFileNr );
            bool serverReady = true;
            Byte[] buffer = new byte[_cMaxActionFileLength];
            UInt32 i = 0;

            mActionTzrN = 0;
            mActionScpN = 0;
            mActionRrN = 0;

            AIndex = 0;
            if (buffer != null)
            {
                for (int j = 0; j < _cMaxActionFileLength; ++j) buffer[j] = 0;  //clear buffer,  CRC + length
                i = 6;

                sCopyString2Buffer(buffer, ref i, _cActionIdString, 6); 

                sCopyString2Buffer(buffer, ref i, deviceID, 6);   // The device ID string

               buffer[i++] = (Byte)firmwareVersion;                // This byte is F. Vers. (*10)

                // strncpy_s puts chars beyond the \0, they must be \0 for TZ to function!!!!
                if (deviceID.Contains("TZMR"))
                {
                    sCopyString2Buffer(buffer, ref i, ASerialNr, 8);      // The next 8 bytes are the serial number
                }
                else if (deviceID.Contains("H3R"))
                {
                    sCopyString2Buffer(buffer, ref i, ASerialNr, 11);      // The next 11 bytes are the serial number
                }

                sCopyUInt2Buffer(buffer, ref i, fileID, 2);

                // NEW_STUDY - used to pass the clean_server_check process
                if (serverReady)
                {
                    buffer[i++] = (Byte)DTzActionTags.ACTIONS_ENABLE_NEW_STUDY;
                    buffer[i++] = 0;
                    buffer[i++] = 0;
                }
            }
            AIndex = i;
            return buffer;
        }
        public bool mbWriteAction(string AFileName, Byte[] ABuffer, ref UInt32 AIndex, 
            bool AbCopyToServer, out bool ArbCopyOk, UInt16 AActionFileNr)
        {
            bool bOk = false;
            bool bCopyOk = false;
 
            try
            {
                if( ABuffer == null ||  AIndex == 0 )
                {
                    CProgram.sLogError("Bad buffer for " + AFileName);
                }
                else
                 {
                    // Always end the file with the terminator
                    ABuffer[AIndex++] = (Byte)DTzActionTags.ACTIONS_TERMINATOR;
                    ABuffer[AIndex++] = 1;
                    ABuffer[AIndex++] = 0xff;
                    ABuffer[AIndex++] = 0;

                    bOk = true;
                    uint i = 2;
                    UInt32 temp = AIndex;
                    if (badLength) temp += 255;

                    sCopyUInt2Buffer(ABuffer, ref i, temp, 2);

                    ushort crcValue = 0xffff;

                    crcBlock(ABuffer, 2, AIndex - 2, ref crcValue);

                    i = 0;
                    temp = crcValue;
                    if (badCRC) temp++;
                    sCopyUInt2Buffer(ABuffer, ref i, temp, 2);

/*                    if (file_aesKeySet && file_aesIVSet)
                    {
                        CProgram.sLogError("File encryption is not supported");
                        bOk = false;
                    }
*/                  if (File.Exists(AFileName))
                    {
                        File.Delete(AFileName);
                    }
                    using (FileStream fs = File.Create(AFileName))
                    {
                        fs.Write(ABuffer, 0, (int)AIndex);
                        fs.Flush();
                        fs.Close();
                        bOk = true;

                    }
                    if (bOk && AbCopyToServer)
                    {
                        string copyFileName = AFileName;
                        try
                        {
                            if (sbMakeToServerUtcFileName(ref copyFileName, AActionFileNr, DateTime.UtcNow))
                            {
                                using (FileStream fs = File.Create(copyFileName))
                                {
                                    fs.Write(ABuffer, 0, (int)AIndex);
                                    fs.Flush();
                                    fs.Close();
                                    bCopyOk = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            CProgram.sLogException("Error writing copy " + copyFileName, ex);
                            bCopyOk = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Error writing " + AFileName, ex);
                bOk = false;
            }
            ArbCopyOk = bCopyOk;
            return bOk;
        }

        public bool mbActionEnableNewStudy( Byte[]ABuffer, ref UInt32 AIndex )
        {
            bool bOk = false;

            // NEW_STUDY - used to pass the clean_server_check process
            ABuffer[AIndex++] = (Byte)DTzActionTags.ACTIONS_ENABLE_NEW_STUDY;
            ABuffer[AIndex++] = 0;
            ABuffer[AIndex++] = 0;  // extra dummy byte

            bOk = true;
            return bOk;
        }

        public bool mbActionRequestScpBlock(Byte[] ABuffer, ref UInt32 AIndex, UInt32 AScpStartNr, UInt16 ANrScpFiles)
        {
            bool bOk = false;
            UInt32 scpNr = AScpStartNr;
            UInt16 n = ANrScpFiles;
            UInt16 nBlock;
            // SCP_REQUEST - request a block of 1 to 255 SCP files per request 
            // Max 255 SCP requests per file

            while( n > 0 )
            {
                nBlock = n >= 254 ? (UInt16)254 : n;
                ABuffer[AIndex++] = (Byte)DTzActionTags.ACTIONS_REQUEST_SCP_BLOCK;
                ABuffer[AIndex++] = 5;
                sCopyUInt2Buffer(ABuffer, ref AIndex, scpNr, 4);
                ABuffer[AIndex++] = (Byte)nBlock;
                ABuffer[AIndex++] = 0;  // extra dummy byte
                scpNr += nBlock;
                n -= nBlock;
                bOk = true;
            }
            return bOk;
        }

        public bool mbActionRetransmitScpFile(Byte[] ABuffer, ref UInt32 AIndex, UInt32 AScpSNr)
        {
            bool bOk = false;

            // RETRANSMIT_SCP - forces retransmission of a corrupt file, regardless of
            // the upload log (no event associated with this upload...)

            ABuffer[AIndex++] = (Byte)DTzActionTags.ACTIONS_RETRANSMIT_SCP_FILE;
            ABuffer[AIndex++] = 4;
            sCopyUInt2Buffer(ABuffer, ref AIndex, AScpSNr, 4);
            ABuffer[AIndex++] = 0;  // extra dummy byte

            bOk = true;

            return bOk;
        }

        public bool mbActionRequestTzrFile(Byte[] ABuffer, ref UInt32 AIndex, UInt16 AYear, UInt16 AMonth, UInt16 ADay, UInt16 AHour)
        {
            bool bOk = false;

            // TZR_REQUEST - requests a specific TZR file from the device
            if( AYear > 2010 && AYear < 2200 && AMonth >= 1 && AMonth <= 12 && ADay >= 1 && ADay <= 31 && AHour >= 0 && AHour < 24)
            {
                ABuffer[AIndex++] = (Byte)DTzActionTags.ACTIONS_REQUEST_TZR_FILE;
                ABuffer[AIndex++] = 5;
                sCopyUInt2Buffer(ABuffer, ref AIndex, AYear, 2);
                ABuffer[AIndex++] = (Byte)AMonth;
                ABuffer[AIndex++] = (Byte)ADay;
                ABuffer[AIndex++] = (Byte)AHour;
                ABuffer[AIndex++] = 0;  // extra dummy byte
                bOk = true;
            }
            return bOk;
        }

        public bool mbActionUpdateSettings(Byte[] ABuffer, ref UInt32 AIndex)
        {
            bool bOk = false;
            // UPDATE_SETTINGS - tell the device to download the settings file from the
            // server immediately
            ABuffer[AIndex++] = (Byte)DTzActionTags.ACTIONS_UPDATE_SETTINGS;
            ABuffer[AIndex++] = 0;
            ABuffer[AIndex++] = 0;  // extra dummy byte

            bOk = true;
            return bOk;
        }


        public bool mbActionDisplayMessage(Byte[] ABuffer, ref UInt32 AIndex, string AMessage)
        {
            bool bOk = false;
            int l = 0;

            // SEND_MESSAGE - displays an SMS-like message on the secreen
            if( AMessage != null && 0 != (l = AMessage.Length) && l < 255)
            {
                ABuffer[AIndex++] = (Byte)DTzActionTags.ACTIONS_DISPLAY_MESSAGE;
                ABuffer[AIndex++] = (Byte)l;

                for( int j = 0; j < l; ++j)
                {
                    ABuffer[AIndex++] = (Byte)AMessage[j];
                }
                ABuffer[AIndex++] = 0;  // extra dummy byte
                bOk = true;
            }

            return bOk;
        }


        public bool mbActionEndStudy(Byte[] ABuffer, ref UInt32 AIndex)
        {
            bool bOk = false;
            // END_STUDY - the alternative to the early-out procedure
            ABuffer[AIndex++] = (Byte)DTzActionTags.ACTIONS_END_STUDY;
            ABuffer[AIndex++] = 1;
            ABuffer[AIndex++] = 0xa5;
            ABuffer[AIndex++] = 0;  // extra dummy byte

            bOk = true;
             return bOk;
        }

        public bool mbActionStartStudy(Byte[] ABuffer, ref UInt32 AIndex)
        {
            bool bOk = false;

            // START_STUDY - the alternative to entering the PIN code
            ABuffer[AIndex++] = (Byte)DTzActionTags.ACTIONS_START_STUDY;
            ABuffer[AIndex++] = 1;
            ABuffer[AIndex++] = 0x5a;
            ABuffer[AIndex++] = 0;  // extra dummy byte

            bOk = true;
   
            return bOk;
        }
    }
}

