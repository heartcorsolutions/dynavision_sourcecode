﻿#define CHECK_CRC                   // Comment this line to disable CRC Checking
using Program_Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrendParser;

namespace Event_Base
{
    public enum DTzR_Tag
    {
        /***   Possible TAG values for each entry   ***/
        // --- TAGS ---            --- VALUE ---         --- DATA ---
        START_TAG = 1,              // none
        STOP_TAG = 2,              // none
        BREAK_TAG = 3,              // none
        RESUME_TAG = 4,              // none
        FULL_TAG = 5,              // none
        PACEMAKER_DETECTION_TAG = 11,              // pulse width (microseconds)
        QRS_DETECTION_TAG = 12,              // 0xMMTT, M == channel mask, TT == beat type
        RHYTHM_CHANGE = 13,
        BPM_COUNT_TAG = 17,              // Samples
        BPM_STDEV_TAG = 18,              // BPM
        BPM_AVERAGE_TAG = 19,              // BPM
        BPM_MIN_TAG = 20,              // BPM 
        BPM_MAX_TAG = 21,              // BPM 
        TACHY_ENTRY_TAG = 22,              // channel mask 
        TACHY_EXIT_TAG = 23,              // channel mask 
        BRADY_ENTRY_TAG = 24,              // channel mask
        BRADY_EXIT_TAG = 25,              // channel mask
        PAUSE_ENTRY_TAG = 26,              // channel mask
        AFIB_ENTRY_TAG = 27,              // channel mask
        AFIB_EXIT_TAG = 28,              // channel mask
        PAUSE_EXIT_TAG = 29,              // channel mask
        TACHY_RATE_CHANGE_TAG = 30,              // BPM
        BRADY_RATE_CHANGE_TAG = 31,              // BPM
        PVC_ENTRY_TAG = 32,              // channel mask
        ANALYSIS_RESUMED_TAG = 49,              // channel mask
        ANALYSIS_PAUSED_TAG = 50,              // channel mask
        LEAD_DISCONNECTED = 51,              // Bitmap of leads (1=disconnected): 0b000EASIG
        MISSING_SCP_TAG = 81,              // none
        MISSING_EVENT_TAG = 82,              // Event count (0 - 999)
        PATIENT_TAG = 101,              // none
        BATTERY_SOC_TAG = 150,              // Battery state of charge (% * 256)
        BATTERY_VALUE_TAG = 151,              // Battery Voltage (mV)
        BATTERY_LOW_TAG = 152,              // Battery Voltage (mV)
        CHARGING_STARTED_TAG = 153,              // none
        CHARGING_STOPPED_TAG = 154,              // none
        TIME_ZONE_CHANGE_TAG = 175,              // Offset from UTC in MINUTES (MAX +/- 780)
        TZR_REQUEST_TAG = 190,              // Day and hour of TZR requested
        SCP_RETRANSMIT_TAG = 199,              // Error Code (ACTIONS_ERR code)
        SERVER_REQUEST_TAG = 200,              // Number of files requested
        SETTINGS_SUCCESS_TAG = 201,              // Setting File ID #
        SMS_COMMAND_ERROR_TAG = 202,              // none
        SMS_MSG_RECEIVED = 203,              // none
        SETTINGS_FAILURE_TAG = 210,              // Error Code (see below)
        ACTIONS_FAILURE_TAG = 211,              // Error Code (see below)
        ACTIONS_SUCCESS_TAG = 212,              // Action File ID #
        FTP_SETTINGS_CHANGED = 225,              // none
        TERMINATOR_TAG = 255,              // 0xFF
            NrEnums
    }

    public enum DTz_CountQRS
    {
        Normal = 0,
        SVEB_PAC,
        VEB_PVC,
        Fusion,
        QPaced,
        Other,
        NrEnums
    }

    public enum DTz_CountRhythm
    {
        Normal = 0,
        Bradycardia,
        Tachycardia,
        AtrialFirillation,
        Pause,
        Unreadable,
        Other,
        NrEnums
    }

    public class CMctReportPoint
    {
        public DateTime _mDateTime;
        public UInt32 _mScpNumber;
        public UInt16 _mValue;
        public bool _mbIsOn;

        public CMctReportPoint(DateTime ADateTime, UInt32 AScpNumber, UInt16 AValue, bool AbIsOn)
        {
            _mDateTime = ADateTime;
            _mScpNumber = AScpNumber;
            _mValue = AValue;
            _mbIsOn = AbIsOn;
        }
        public CMctReportPoint(CMctReportPoint AReportPoint )
        {
            _mDateTime = AReportPoint._mDateTime;
            _mScpNumber = AReportPoint._mScpNumber;
            _mValue = AReportPoint._mValue;
            _mbIsOn = AReportPoint._mbIsOn;
        }
    }

    public class CMctReportItem
    {
        public List<CMctReportPoint> _mList;
        public DateTime _mFirstDateTime;
        public DateTime _mLastDateTime;
        public double _mDurationTotalSec;
        public double _mDurationOnSec;
        public UInt32 _mCountOff2On;
        public UInt32 _mCountPointsOff;
        public UInt32 _mCountPointsOn;
        public bool _mbLastState;
        UInt16 _mLastValue;
        public UInt16 _mMinValue;
        public UInt16 _mMaxValue;

        public string _mLabel;
        public string _mUnit;
        public CMctReportItem( string ALabel, string AUnit )
        {
            _mList = new List<CMctReportPoint>();
            _mFirstDateTime = DateTime.MinValue;
            _mLastDateTime = DateTime.MinValue;
            _mDurationTotalSec = 0.0F;
            _mDurationOnSec = 0.0F;
            _mCountOff2On = 0;
        _mCountPointsOff = 0;
        _mCountPointsOn = 0;
        _mbLastState = false;
            _mLastValue = 0;
            _mMinValue = UInt16.MaxValue;
            _mMaxValue = 0;

            _mLabel = ALabel;
            _mUnit = AUnit;

  
    }
    public void mStart(bool AbStartState)
        {
            _mFirstDateTime = DateTime.MinValue;
            _mLastDateTime = DateTime.MinValue;
            _mDurationTotalSec = 0.0F;
            _mDurationOnSec = 0.0F;
            _mCountOff2On = 0;
            _mCountPointsOff = 0;
            _mCountPointsOn = 0;
            _mbLastState = AbStartState;
            _mLastValue = 0;
            _mMinValue = UInt16.MaxValue;
            _mMaxValue = 0;
        }

        public void mFirst(DateTime ADateTime)
        {
            if (ADateTime > DateTime.MinValue)
            {
                _mFirstDateTime = ADateTime;
                _mLastDateTime = ADateTime;
            }
        }

        public void mStop( DateTime ADateTime, UInt32 AScpNumber, bool AbStopState)
        {
            if( _mLastDateTime > DateTime.MinValue )
            {
//                mAddTailValue(ADateTime, AScpNumber, _mLastValue, AbStopState);
            }
         }
        public void mAddTailBool(DateTime ADateTime, UInt32 AScpNumber, bool AbIsOn)
        {
            mAddTailValue(ADateTime, AScpNumber, (UInt16)(AbIsOn ? 1 : 0), AbIsOn);
        }

        public void mAddTailValue(DateTime ADateTime, UInt32 AScpNumber, UInt16 AValue, bool AbIsOn)
        {
            if( ADateTime > DateTime.MinValue)
            {
                if (_mList != null)
                {
                    CMctReportPoint point = new CMctReportPoint(ADateTime, AScpNumber, AValue, AbIsOn);
                    if (point != null)
                    {
                        _mList.Add(point);
                    }
                }
                if( _mFirstDateTime == DateTime.MinValue )
                {
                    _mFirstDateTime = ADateTime;
                }
                _mDurationTotalSec = (ADateTime - _mFirstDateTime).TotalSeconds;
            }
            if (_mbLastState == false && AbIsOn)
            {
                ++_mCountOff2On;
            }
            if (_mbLastState)    // measure time while on, this means from onn  until off
            {
                if (ADateTime != DateTime.MinValue && _mLastDateTime > DateTime.MinValue)
                {
                    double dT = (ADateTime - _mLastDateTime).TotalSeconds;

                    _mDurationOnSec += dT;

                }
            }
            if( AbIsOn)
            { 
                ++_mCountPointsOn;
            }
            else
            {
                ++_mCountPointsOff;
            }
            if( ADateTime > DateTime.MinValue ) _mLastDateTime = ADateTime;
            _mbLastState = AbIsOn;
            _mLastValue = AValue;
            if (AValue < _mMinValue) _mMinValue = AValue;
            if (AValue > _mMaxValue) _mMaxValue = AValue;
        }

        public void mAddPeriodOn(DateTime AStartDT, DateTime AStopDT)
        {
            try
            {
                DateTime startDT = AStartDT;
                DateTime stopDT = AStopDT;

                if (startDT > AStopDT)
                {
                    startDT = AStopDT;
                    stopDT = AStartDT;
                }
                if (AStartDT != DateTime.MinValue)
                {
                    if (startDT < _mFirstDateTime) _mFirstDateTime = startDT;
                    if (stopDT > _mLastDateTime) _mLastDateTime = stopDT;

                    int n = _mList.Count;

                    if (n == 0)
                    {
                        // empty list
                        _mList.Add(new CMctReportPoint(startDT.AddMilliseconds(-1), 0, 0, false));
                        _mList.Add(new CMctReportPoint(startDT, 0, 1, true));
                        _mList.Add(new CMctReportPoint(stopDT, 0, 1, true));
                        _mList.Add(new CMctReportPoint(stopDT.AddMilliseconds(1), 0, 0, false));
                    }
                    else
                    {
                        int iStart = -1;   // find before
                        bool bStart = false;

                        for (int i = 0; i < n; ++i)
                        {
                            if (_mList[i]._mDateTime >= startDT)
                            {
                                iStart = i;
                                break;  // before this node
                            }
                            bStart = _mList[i]._mbIsOn;
                        }
                        if (iStart < 0)
                        {
                            // add to end
                            _mList.Add(new CMctReportPoint(startDT, 0, 0, false));
                            _mList.Add(new CMctReportPoint(startDT.AddMilliseconds(1), 0, 1, true));
                            _mList.Add(new CMctReportPoint(stopDT.AddMilliseconds(1), 0, 1, true));
                            _mList.Add(new CMctReportPoint(stopDT.AddMilliseconds(2), 0, 0, false));
                        }
                        else
                        {
                            if (false == bStart)
                            {
                                // insert start
                                _mList.Insert(iStart++, new CMctReportPoint(startDT.AddMilliseconds(-1), 0, 0, false));
                                _mList.Insert(iStart++, new CMctReportPoint(startDT, 0, 1, true));
                            }
                            else
                            {
                                _mList.Insert(iStart++, new CMctReportPoint(startDT, 0, 1, true)); // insert on point so see where it is merged
                            }

                            int iStop = iStart;
                            bool bStop = bStart;
                            n = _mList.Count;
                            //stopDT = stopDT.AddMilliseconds(2);
                            while (iStop < n)
                            {
                                if (_mList[iStop]._mDateTime > stopDT )
                                {
                                    break;  // before this node
                                }
                                bStop = _mList[iStop]._mbIsOn;
                                // remove all points until stop time
                                _mList.RemoveAt(iStop);
                                --n;
                            }
                            if (iStop >= n)
                            {
                                // add to end of list
                                _mList.Add(new CMctReportPoint(stopDT, 0, 1, true));
                                _mList.Add(new CMctReportPoint(stopDT.AddMilliseconds(1), 0, 0, false));
                            }
                            else
                            {
                                if (bStop)
                                {
                                    // stop during on state => done
                                    _mList.Insert(iStop++, new CMctReportPoint(stopDT, 0, 1, true));    // insert on point so see where it is merged
                                }
                                else
                                {
                                    // insert new stop
                                    _mList.Insert(iStop++, new CMctReportPoint(stopDT, 0, 1, true));
                                    _mList.Insert(iStop++, new CMctReportPoint(stopDT.AddMilliseconds(1), 0, 0, false));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed add perios starting at " + AStartDT.ToString() + " to " + AStopDT.ToString(), ex);
            }
        }
        public bool mbCheckTimeline(bool AbLogAllPoints, bool AbLogError)
        {
            bool bOk = true;
            try
            {
                DateTime prevDT = DateTime.MinValue;
                DateTime minDT = DateTime.MaxValue;
                DateTime maxDT = DateTime.MinValue;
                DateTime curDT;
                double maxErrorSec = 0.0;
                int nError = 0;
                int nSwapped = 0;
                int nOn = 0;
                int n = _mList.Count;
                int i = 0;
                UInt32 minValue = UInt32.MaxValue;
                UInt32 maxValue = UInt32.MinValue;
                string addLog;

                double dt;
                double cmpSec = -0.0025;

                bool bLogError = CLicKeyDev.sbDeviceIsProgrammer();

                if (n > 0)
                {
                    prevDT = _mList[i]._mDateTime;
                    minDT = prevDT;
                    maxDT = prevDT;
                    if (_mList[i]._mbIsOn)
                    {
                        ++nOn;
                    }

                    if (AbLogAllPoints)
                    {
                        CProgram.sLogLine(_mLabel + "[" + i.ToString("0000") + "] " + _mList[i]._mDateTime.ToString()
                            + " scpNr = " + _mList[i]._mScpNumber.ToString("00000") +
                           (_mList[i]._mbIsOn ? " ON " : " off") + " value = " + _mList[i]._mValue.ToString());
                    }

                    for (i = 1; i < n; ++i)
                    {
                        addLog = "";
                        curDT = _mList[i]._mDateTime;
                        if (curDT < minDT) minDT = curDT;
                        if (curDT > maxDT) maxDT = curDT;
                        if (_mList[i]._mbIsOn)
                        {
                            ++nOn;
                        }
                        dt = (curDT - prevDT).TotalSeconds;

                        if (dt < 0)
                        {
                            if (dt < cmpSec)
                            {
                                if( nError == 0)
                                {
                                    nError = 0;
                                }
                                ++nError;
                                addLog = "error dT = " + dt.ToString("0.000");
                                if( dt < maxErrorSec)
                                {
                                    maxErrorSec = dt;
                                }
                            }
                            else
                            {
                                bool bPrev = _mList[i - 1]._mbIsOn;
                                UInt16 prevVal = _mList[i - 1]._mValue;
                                UInt32 prevScp = _mList[i - 1]._mScpNumber;

                                // swap values;
                                _mList[i - 1]._mDateTime = curDT;
                                _mList[i - 1]._mbIsOn = _mList[i]._mbIsOn;
                                _mList[i - 1]._mValue = _mList[i]._mValue;
                                _mList[i]._mScpNumber = _mList[i - 1]._mScpNumber;
                                _mList[i]._mDateTime = prevDT;
                                _mList[i]._mbIsOn = bPrev;
                                _mList[i]._mValue = prevVal;
                                _mList[i]._mScpNumber = prevScp;

                                addLog = "swapped dT = " + dt.ToString("0.000");
                                ++nSwapped;
                            }
                        }
                        else
                        {
                            prevDT = curDT;
                        }
                        if (AbLogAllPoints || bLogError && addLog.Length > 2 )
                        {
                            CProgram.sLogLine(_mLabel + "[" + i.ToString("0000") + "] " + _mList[i]._mDateTime.ToString()
                                + "scpNr = " + _mList[i]._mScpNumber.ToString("00000")
                               + (_mList[i]._mbIsOn ? " ON " : " off") + " value = " + _mList[i]._mValue.ToString() + addLog);
                        }

                    }
                    if (minDT < _mFirstDateTime)
                    {
                        bOk = false;
                        CProgram.sLogLine(_mLabel + " start time: " + minDT.ToString() + " < " + _mFirstDateTime.ToString());
                    }
                    if (maxDT > _mLastDateTime)
                    {
                        bOk = false;
                        CProgram.sLogLine(_mLabel + " start time: " + maxDT.ToString() + " > " + _mLastDateTime.ToString());
                    }
                }
                if (nError > 0  || nSwapped > 0 || AbLogAllPoints || false == bOk)
                {
                    CProgram.sLogLine(_mLabel + "[0.." + n.ToString("0000") + "] "
                        + minDT.ToString() + " <= t <=  " + minDT.ToString()
                        + nOn.ToString() + " on "
                        + minValue.ToString() + " <= v <= " + maxValue.ToString());
                    CProgram.sLogLine(_mLabel + "[0.." + n.ToString("0000") + "] "
                        + nError.ToString() + " errors " + maxErrorSec.ToString( "0.000") 
                        + " sec "+ nSwapped.ToString() + " swapped");
                    if( nError > 0 )
                    {
                        int kkk = 4;

                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed check time line for " + _mLabel, ex);
            }
            return bOk;
        }
  
  /*
                        public void mAddAfterwardsBool(DateTime ADateTime, UInt32 AScpNumber, bool AbIsOn)
                        {
                            mAddAfterwardsValue(ADateTime, AScpNumber, (UInt16)(AbIsOn ? 1 : 0), AbIsOn);

                        }
                        public void mAddAfterwardsValue(DateTime ADateTime, UInt32 AScpNumber, UInt16 AValue, bool AbIsOn)
                        {
                            if (ADateTime > DateTime.MinValue)
                            {
                                if (_mList != null)
                                {
                                    CMctReportPoint point = new CMctReportPoint(ADateTime, AScpNumber, AValue, AbIsOn);
                                    if (point != null)
                                    {
                                        int n = _mList.Count;

                                        if (n == 0)
                                        {
                                            _mList.Add(point);
                                        }
                                        else
                                        {
                                            int i = 0;

                                            for( i = 0; i < n; ++ i)
                                            {
                                                if( ADateTime < _mList[i]._mDateTime)
                                                {
                                                    break;  // before this node
                                                }
                                            }
                                            if( i < n )
                                            {
                                                _mList.Insert(i, point);
                                            }
                                            else
                                            {
                                                _mList.Add(point);
                                            }
                                        }
                                    }
                                }
                                if (_mFirstDateTime == DateTime.MinValue)
                                {
                                    _mFirstDateTime = ADateTime;
                                }
                                if( ADateTime > _mLastDateTime)
                                {
                                    _mLastDateTime = ADateTime;
                                    _mDurationTotalSec = (ADateTime - _mFirstDateTime).TotalSeconds;
                                }

                            }
                            if (_mbLastState == false && AbIsOn)
                            {
                                ++_mCountOff2On;
                            }
                           if (AbIsOn)
                            {
                                ++_mCountPointsOn;
                            }
                            else
                            {
                                ++_mCountPointsOff;
                            }
                            _mbLastState = AbIsOn;
                            _mLastValue = AValue;
                            if (AValue < _mMinValue) _mMinValue = AValue;
                            if (AValue > _mMaxValue) _mMaxValue = AValue;
                        }
                */
        public void mSetOff(DateTime ADateTime, UInt32 AScpNumber)
        {
            if (_mbLastState)
            {
                mAddTailValue(ADateTime, AScpNumber, 0 /*_mLastValue*/, false);
            }
        }

        public void mAddPulse(DateTime ADateTime, UInt32 AScpNumber, UInt16 AValue)
        {
            UInt16 value = AValue;
            
            if( AValue == 0 ) value = 1;
            mAddTailValue(ADateTime.AddSeconds(-0.001), AScpNumber, 0, false);
            mAddTailValue(ADateTime, AScpNumber, value, true);
            mAddTailValue(ADateTime.AddSeconds(0.001), AScpNumber, 0, false);
        }
        public void mCheck(DateTime ADateTime, UInt32 AScpNumber, UInt16 AValue, bool AbIsOn)
        {
            if( _mbLastState != AbIsOn )
            {
                mAddTailValue(ADateTime.AddSeconds(0.001), AScpNumber, AValue, AbIsOn);
            }
        }

        public void mLog()
        {
            int n = _mList == null ? 0 : _mList.Count;
            string s = _mLabel + "(" + _mUnit + ")[" + n.ToString();

            s += "]= " + _mLastValue.ToString() + " " + (_mbLastState ? "On" : "Off");
            s += ", Nup= " + _mCountOff2On.ToString() + ", " + _mDurationOnSec.ToString("0.0") + " sec";

            CProgram.sLogLine(s);
            if( n > 0)
            {
   
                s = _mMinValue.ToString() + " <= " + _mLabel + " <= " + _mMaxValue.ToString() + ",  "
                    + CProgram.sDateTimeToString(_mFirstDateTime) + " <= T(" + _mDurationTotalSec.ToString("0.0") + " sec) <= " + CProgram.sDateTimeToString(_mLastDateTime);
                CProgram.sLogLine(s);
            }             
        }
        public UInt32 mCountDurationUseingStateCount(DateTime AFrom, DateTime ATo, out double ArDurationSec, bool AbLogAll)
        {
            UInt32 n = 0;
            double durationSec = 0;
            int nList = 0;

            if (_mList != null)
            {
                nList = _mList.Count;

                if (nList > 0)
                {
                    int nState = 0;
                    DateTime onDT = DateTime.MaxValue;
                    double dT;
                    bool bFirst = true;
                    string strLog = "";

                    if (AbLogAll)
                    {
                        dT = 0.0;
                    }

                    foreach (CMctReportPoint point in _mList)
                    {
                        if (AbLogAll)
                        {
                            strLog = "Counting " + _mLabel + " point " + CProgram.sDateTimeToString(point._mDateTime) + " val=" + point._mValue.ToString()
                                + (point._mbIsOn ? " ON " : " OFF ");
                        }

                        if (point._mDateTime >= AFrom)
                        {
                            if (point._mDateTime > ATo)
                            {
                                // past period
                                break;
                            }
                            if (bFirst)
                            {
                                bFirst = false;
                                if (nState > 0)
                                {
                                    onDT = AFrom;   // on period already started
                                    ++n;
                                }
                            }
                            if (point._mbIsOn)
                            {
                                if (nState == 0)
                                {
                                    // from off to on => count
                                    onDT = point._mDateTime;
                                    ++n;
                                }
                            }
                            else if (nState == 1)
                            {
                                // from on to off =>  calc on duration
                                dT = (point._mDateTime - onDT).TotalSeconds;
                                if (dT > 0)
                                {
                                    durationSec += dT;
                                }
                                if (AbLogAll)
                                {
                                    strLog += "dT= " + CProgram.sPrintTimeSpan_dhmSec(dT, " ");
                                }
                            }
                        }
                        if (point._mbIsOn)
                        {
                            ++nState;
                        }
                        else if (nState > 0)
                        {
                            --nState;
                        }
                        if (AbLogAll)
                        {
                            CProgram.sLogLine(strLog);
                        }
                    }
                    // end period
                    if (bFirst == false && nState > 0)
                    {
                        dT = (ATo - onDT).TotalSeconds; // period ends as true
                        if (dT > 0)
                        {
                            durationSec += dT;
                        }
                        if (AbLogAll)
                        {
                            CProgram.sLogLine("Counting " + _mLabel + " last point ON end " + CProgram.sDateTimeToString(ATo) + "dT= " + CProgram.sPrintTimeSpan_dhmSec(dT, " "));
                        }
                    }
                }
            }
            ArDurationSec = durationSec;

            //if( AbLogAll )
            if (nList > 0)
            {
                double periodSec = (ATo - AFrom).TotalSeconds;
                CProgram.sLogLine("Count  " + _mLabel + " " + nList.ToString() + " from " + CProgram.sDateTimeToString(AFrom) + " to " + CProgram.sDateTimeToString(ATo)
                    + " (" + CProgram.sPrintTimeSpan_dhmSec(periodSec, " ") + ") n= " + n.ToString() + " duration= " + CProgram.sPrintTimeSpan_dhmSec(durationSec, " "));
            }

            return n;
        }
        public UInt32 mCountDuration(DateTime AFrom, DateTime ATo, out double ArDurationSec, bool AbLogAll)
        {
            UInt32 n = 0;
            double durationSec = 0;
            int nList = 0;

            if (_mList != null)
            {
                nList = _mList.Count;

                if (nList > 0)
                {
                    bool bState = false;
                    DateTime onDT = DateTime.MaxValue;
                    double dT;
                    bool bFirst = true;
                    string strLog = "";

                    if (AbLogAll)
                    {
                        dT = 0.0;
                    }

                    foreach (CMctReportPoint point in _mList)
                    {
                        if (AbLogAll)
                        {
                            strLog = "Counting " + _mLabel + " point " + CProgram.sDateTimeToString(point._mDateTime) + " val=" + point._mValue.ToString()
                                + (point._mbIsOn ? " ON " : " OFF ");
                        }

                        if (point._mDateTime >= AFrom)
                        {
                            if (point._mDateTime > ATo)
                            {
                                // past period
                                break;
                            }
                            if (bFirst)
                            {
                                bFirst = false;
                                if (bState == true)
                                {
                                    onDT = AFrom;   // on period already started
                                    ++n;
                                }
                            }
                            if (point._mbIsOn)
                            {
                                if (bState == false)
                                {
                                    // from off to on => count
                                    onDT = point._mDateTime;
                                    ++n;
                                }
                            }
                            else if (bState == true)
                            {
                                // from on to off =>  calc on duration
                                dT = (point._mDateTime - onDT).TotalSeconds;
                                if (dT > 0)
                                {
                                    durationSec += dT;
                                }
                                if (AbLogAll)
                                {
                                    strLog += "dT= " + CProgram.sPrintTimeSpan_dhmSec(dT, " ");
                                }
                            }
                        }
                        /*                        if (point._mbIsOn && bState == false)
                                                {
                                                    // record on time
                                                    onDT = point._mDateTime;
                                                }
                        */
                        bState = point._mbIsOn;
                        if (AbLogAll)
                        {
                            CProgram.sLogLine(strLog);
                        }
                    }
                    // end period
                    if (bFirst == false && bState == true)
                    {
                        dT = (ATo - onDT).TotalSeconds; // period ends as true
                        if (dT > 0)
                        {
                            durationSec += dT;
                        }
                        if (AbLogAll)
                        {
                            CProgram.sLogLine("Counting " + _mLabel + " last point ON end " + CProgram.sDateTimeToString(ATo) + "dT= " + CProgram.sPrintTimeSpan_dhmSec(dT, " "));
                        }
                    }
                }
            }
            ArDurationSec = durationSec;

            //if( AbLogAll )
            if (nList > 0)
            {
                double periodSec = (ATo - AFrom).TotalSeconds;
                CProgram.sLogLine("Count  " + _mLabel + " " + nList.ToString() + " from " + CProgram.sDateTimeToString(AFrom) + " to " + CProgram.sDateTimeToString(ATo)
                    + " (" + CProgram.sPrintTimeSpan_dhmSec(periodSec, " ") + ") n= " + n.ToString() + " duration= " + CProgram.sPrintTimeSpan_dhmSec(durationSec, " "));
            }

            return n;
        }
    }
    public class CTzReport
    {

        const string TZR_ID_STRING = "TZINT";
        const UInt32 MAX_FILE_SIZE = (1000 * 1024);   // Bytes
        const UInt32 MIN_FILE_SIZE = 10;

        private UInt16[] ccitt_crc16_table = {
    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
    0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
    0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
    0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
    0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
    0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
    0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
    0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
    0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
    0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
    0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
    0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
    0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
    0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
    0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
    0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
    0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
    0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
    0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
    0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
    0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
    0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
    0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
    0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
    0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
    0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
    0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
    0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
    0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
    0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
    0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
    0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
        };

        private Byte[] constKey = {
    0xf5, 0xf2, 0x3c, 0x0c, 0x8e, 0x49, 0x3a, 0xfb,
    0x30, 0x1d, 0xf8, 0x62, 0xa0, 0x30, 0x45, 0xbe
        };

        private Byte[] constIV = {
    0x79, 0x17, 0xae, 0x23, 0x46, 0xe7, 0x8e, 0x79,
    0xbf, 0x3d, 0xff, 0xa6, 0x2c, 0x05, 0x4b, 0xf9
        };

        public CMctReportItem _mItemBatt, _mItemBattChrg, _mItemRecording, _mItemHrN, _mItemHrAvg, _mItemHrMin, _mItemHrMax, _mItemHrSd;
        private CMctReportItem _mItemBrady, _mItemTachy, _mItemPause, _mItemAfib, _mItemAflut, _mItemPace, _mItemManual;
        private CMctReportItem _mItemPAC, _mItemPVC, _mItemVtach, _mItemOther, _mItemError, _mItemAbNorm;
        private CMctReportItem _mItemBradyVal, _mItemTachyVal, _mItemPauseVal, _mItemAfibVal, _mItemAflutVal, _mItemPaceVal, 
            _mItemPACVal, _mItemPVCVal, _mItemVtachVal, _mItemOtherVal, _mItemManualVal, _mItemAbNormVal;
        public DateTime _mStartTimeDT, _mEndTimeDT;    // keep time in device time because it is shown as well in device time
        public UInt32 _mStartScpNumber, _mEndScpNumber;
        public string _mDeviceID;
        public string _mPatientID;
        public Int16 _mTimeZoneOffsetMin;
        DDeviceType _mDeviceType;

        public UInt16 _mNrFiles;
        public UInt16 _mNrMissingFiles;
        public string _mLastFilePath;

        public bool _mbLogError = true;
        public bool _mbLogLine = false;
        public bool _mbLogLineDT = false;
        public bool _mbLogLineDetail = false;
        public bool _mbLogInfo = false; //true;

        public UInt32 _mTotalNrTags;
        public UInt32[] _mCounterTags;
        public UInt32[] _mCounterQrs;
        public UInt32[] _mCounterRhythm;

        public CTzReport()
        {

            _mNrFiles = 0;
            _mNrMissingFiles = 0;
            _mLastFilePath = "";
            _mStartTimeDT = DateTime.MaxValue;
            _mEndTimeDT = DateTime.MinValue;
            _mStartScpNumber = 1 << 15;
            _mEndScpNumber = 0;
            _mDeviceType = DDeviceType.Unknown;


            _mItemBatt = new CMctReportItem("Batt", "mV");   // also include batt low signal
            _mItemBattChrg = new CMctReportItem("Chrg", "Act");
            _mItemRecording = new CMctReportItem("Rec", "On");
            _mItemHrN = new CMctReportItem("HrN", "");
            _mItemHrAvg = new CMctReportItem("HrAvg", "BPM");
            _mItemHrMin = new CMctReportItem("HrMin", "BPM");
            _mItemHrMax = new CMctReportItem("HrMax", "BPM");
            _mItemHrSd = new CMctReportItem("HrStd", "BPM/256");

            _mItemBrady = new CMctReportItem("Brady", "Act");
            _mItemTachy = new CMctReportItem("Tachy", "Act");
            _mItemPause = new CMctReportItem("Pause", "Act");
            _mItemAfib = new CMctReportItem("AFib", "Act");
            _mItemAflut = new CMctReportItem("AFlut", "Act");
            _mItemPAC = new CMctReportItem("PAC", "Act");
            _mItemPVC = new CMctReportItem("PVC", "Act");
            _mItemPace = new CMctReportItem("Pace", "Puls");
            _mItemVtach = new CMctReportItem("Vtach", "Act");
            _mItemOther = new CMctReportItem("Other", "Puls");
            _mItemManual = new CMctReportItem("Manual", "Act");
            _mItemError = new CMctReportItem("Error", "Act");
            _mItemAbNorm = new CMctReportItem("AbNorm", "Act");

            _mItemBradyVal = new CMctReportItem("√Brady", "Act");
            _mItemTachyVal = new CMctReportItem("√Tachy", "Act");
            _mItemPauseVal = new CMctReportItem("√Pause", "Act");
            _mItemAfibVal = new CMctReportItem("√AFib", "Act");
            _mItemAflutVal = new CMctReportItem("√AFlut", "Act");
            _mItemPaceVal = new CMctReportItem("√Pace", "Puls");
            _mItemPACVal = new CMctReportItem("√PAC", "Act");
            _mItemPVCVal = new CMctReportItem("√PVC", "Act");
            _mItemVtachVal = new CMctReportItem("√Vtach", "Act");
            _mItemOtherVal = new CMctReportItem("√Other", "Puls");
            _mItemManualVal = new CMctReportItem("√Manual", "Act");
            _mItemAbNormVal = new CMctReportItem("√AbNorm", "Act");

            _mCounterTags = new UInt32[(int)DTzR_Tag.NrEnums];
            _mCounterQrs = new UInt32[(int)DTz_CountQRS.NrEnums];
            _mCounterRhythm = new UInt32[(int)DTz_CountRhythm.NrEnums];

            _mTotalNrTags = 0;
            for (int i = 0; i < (int)DTzR_Tag.NrEnums; ++i)
            {
                _mCounterTags[i] = 0;
            }
            for (int i = 0; i < (int)DTz_CountQRS.NrEnums; ++i)
            {
                _mCounterQrs[i] = 0;
            }
            for (int i = 0; i < (int)DTz_CountRhythm.NrEnums; ++i)
            {
                _mCounterRhythm[i] = 0;
            }
        }

        private string mLogCountIntem(CMctReportItem AItem)
        {
            string s = "";

            if( AItem != null )
            {
                UInt32 n = AItem._mCountPointsOn;

                s = AItem._mLabel + n.ToString() + "; ";
            }
            return s;
        }
        public void mLogCounters()
        {
//            if (_mbLogLine || CLicKeyDev.sbDeviceIsProgrammer())
            {
                int i;
                String lineQrs = "QRS counters: ";
                for (i = 0; i < (int)DTz_CountQRS.NrEnums; ++i)
                {
                    lineQrs += ((DTz_CountQRS)i).ToString().Substring(0, 1) + _mCounterQrs[i].ToString() + "; ";
                }
                CProgram.sLogLine(lineQrs);

                String lineRhythm = "Rhythm  counters: ";
                for (i = 0; i < (int)DTz_CountRhythm.NrEnums; ++i)
                {
                    lineRhythm += ((DTz_CountRhythm)i).ToString().Substring(0, 1) + _mCounterRhythm[i].ToString() + "; ";
                }
                CProgram.sLogLine(lineRhythm);

                string lineItems = "Item Count: ";

                lineItems += mLogCountIntem(_mItemBrady);
                lineItems += mLogCountIntem(_mItemTachy);
                lineItems += mLogCountIntem(_mItemPause);
                lineItems += mLogCountIntem(_mItemAfib);
                lineItems += mLogCountIntem(_mItemPAC);
                lineItems += mLogCountIntem(_mItemPVC);
                lineItems += mLogCountIntem(_mItemPace);
                lineItems += mLogCountIntem(_mItemManual);
                lineItems += mLogCountIntem(_mItemError);
                CProgram.sLogLine(lineItems);
            }
        }
            public void mSetDebug( bool AbLogLine, bool AbLogLineDT, bool AbLogInfo, bool AbLogDetail )
        {
            _mbLogLine = AbLogLine;
            _mbLogLineDT = AbLogLineDT;
            _mbLogInfo = AbLogInfo;
            _mbLogLineDetail = AbLogDetail;
        }

        public CMctReportItem mGetBrady( bool AbValidated)
        {
            return AbValidated ? _mItemBradyVal : _mItemBrady;
        }
        public CMctReportItem mGetTachy(bool AbValidated)
        {
            return AbValidated ? _mItemTachyVal : _mItemTachy;
        }
        public CMctReportItem mGetPause(bool AbValidated)
        {
            return AbValidated ? _mItemPauseVal : _mItemPause;
        }
        public CMctReportItem mGetPace(bool AbValidated)
        {
            return AbValidated ? _mItemPaceVal : _mItemPace;
        }
        public CMctReportItem mGetPAC(bool AbValidated)
        {
            return AbValidated ? _mItemPACVal : _mItemPAC;
        }
        public CMctReportItem mGetPVC(bool AbValidated)
        {
            return AbValidated ? _mItemPVCVal : _mItemPVC;
        }
        public CMctReportItem mGetAfib(bool AbValidated)
        {
            return AbValidated ? _mItemAfibVal : _mItemAfib;
        }
        public CMctReportItem mGetAflut(bool AbValidated)
        {
            return AbValidated ? _mItemAflutVal : _mItemAflut;
        }
        public CMctReportItem mGetVtach(bool AbValidated)
        {
            return AbValidated ? _mItemVtachVal : _mItemVtach;
        }
        public CMctReportItem mGetAbNorm(bool AbValidated)
        {
            return AbValidated ? _mItemAbNormVal : _mItemAbNorm;
        }
        public CMctReportItem mGetOther(bool AbValidated)
        {
            return AbValidated ? _mItemOtherVal : _mItemOtherVal;
        }
        public CMctReportItem mGetManual(bool AbValidated)
        {
            return AbValidated ? _mItemManualVal : _mItemManual;
        }

        public bool mbCheckItems()
        {
            return _mItemHrAvg != null && _mItemHrMin != null && _mItemHrMax != null && _mItemHrSd != null && _mItemHrN != null && _mItemRecording != null
                && _mItemBrady != null && _mItemTachy != null && _mItemBatt != null && _mItemBattChrg != null && _mItemPace != null && _mItemPAC != null && _mItemPVC != null
                &&  _mItemPace != null && _mItemAflut != null && _mItemVtach != null && _mItemOther != null && _mItemAbNorm != null && _mItemAbNormVal != null
                && _mItemPause != null && _mItemAfib != null && _mItemManual != null && _mItemError != null;
        }
        public bool mbStart(DDeviceType AType)
        {
            bool bCheck = mbCheckItems();

            _mStartTimeDT = DateTime.MaxValue;
            _mEndTimeDT = DateTime.MinValue;
            _mDeviceID = "";
            _mPatientID = "";
            _mTimeZoneOffsetMin = 0;
            _mNrFiles = 0;
            _mLastFilePath = "";
            _mDeviceType = AType;

            if (bCheck)
            {
                _mItemBatt.mStart(false);
                _mItemBattChrg.mStart(false);
                _mItemRecording.mStart(false);
                _mItemHrN.mStart(false);
                _mItemHrAvg.mStart(false);
                _mItemHrMin.mStart(false);
                _mItemHrMax.mStart(false);
                _mItemHrSd.mStart(false);
                _mItemBrady.mStart(false);
                _mItemTachy.mStart(false);
                _mItemPause.mStart(false);
                _mItemAfib.mStart(false);
                _mItemAflut.mStart(false);
                _mItemPAC.mStart(false);
                _mItemPVC.mStart(false);
                _mItemPace.mStart(false);
                _mItemVtach.mStart(false);
                _mItemOther.mStart(false);
                _mItemManual.mStart(false);
                _mItemError.mStart(false);
                _mItemAbNorm.mStart(false);

                switch (AType)
                {
                    case DDeviceType.TZ:
                        bCheck = mbStartTZ();
                        break;
                    case DDeviceType.Sirona:
                        bCheck = mbStartSirona();
                        break;
                    case DDeviceType.DV2:
                        break;
                }
            }
            CProgram.sLogLine("MctReport stop(" + CDvtmsData.sGetDeviceTypeName(AType) + ") " + (bCheck ? "ok" : "failed"));
            return bCheck;
        }

        public bool mbStartTZ()
        {
            bool bOk = true;

            return bOk;
        }
        public bool mbStartSirona()
        {
            bool bOk = true;

            return bOk;
        }

        public bool mbStopSignals()
        {
            bool bCheck = mbCheckItems();

            if (bCheck && _mEndTimeDT > DateTime.MinValue)
            {
                _mItemBatt.mStop(_mEndTimeDT, _mEndScpNumber, false);   // set last entry to endtime and end state
                _mItemBattChrg.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemRecording.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemHrN.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemHrAvg.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemHrMin.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemHrMax.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemHrSd.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemBrady.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemTachy.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemPause.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemAfib.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemAflut.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemPAC.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemPVC.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemPace.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemVtach.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemOther.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemManual.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemError.mStop(_mEndTimeDT, _mEndScpNumber, false);
                _mItemAbNorm.mStop(_mEndTimeDT, _mEndScpNumber, false);
            }
            return bCheck;
        }

        public bool mbStop(DDeviceType AType)
        {
            bool bCheck = mbCheckItems();

            if (bCheck && _mEndTimeDT > DateTime.MinValue)
            {
                switch (AType)
                {
                    case DDeviceType.TZ:
                        bCheck = mbStopTZ();
                        break;
                    case DDeviceType.Sirona:
                        bCheck = mbStopSirona();
                        break;
                    case DDeviceType.DV2:
                        break;
                }
                bCheck = mbStopSignals();
            }
            return bCheck;
        }
        public bool mbStopTZ()
        {
            bool bOk = true;

            return bOk;
        }
        public bool mbStopSirona()
        {
            bool bOk = true;

            return bOk;
        }

        public void mFirstTime(DateTime ADateTime)
        {
            bool bCheck = mbCheckItems();

            if (bCheck && ADateTime > DateTime.MinValue)
            {
                _mItemBatt.mFirst(ADateTime);       // set first entry to first time and store state
                _mItemBattChrg.mFirst(ADateTime);
                _mItemRecording.mFirst(ADateTime);
                _mItemHrN.mFirst(ADateTime);
                _mItemHrAvg.mFirst(ADateTime);
                _mItemHrMin.mFirst(ADateTime);
                _mItemHrMax.mFirst(ADateTime);
                _mItemHrSd.mFirst(ADateTime);
                _mItemBrady.mFirst(ADateTime);
                _mItemTachy.mFirst(ADateTime);
                _mItemPause.mFirst(ADateTime);
                _mItemAfib.mFirst(ADateTime);
                _mItemAflut.mFirst(ADateTime);
                _mItemPAC.mFirst(ADateTime);
                _mItemPVC.mFirst(ADateTime);
                _mItemPace.mFirst(ADateTime);
                _mItemVtach.mFirst(ADateTime);
                _mItemOther.mFirst(ADateTime);
                _mItemAbNorm.mFirst(ADateTime);
                _mItemManual.mFirst(ADateTime);
                _mItemError.mFirst(ADateTime);
            }
        }

        public void mLogInfo()
        {
            if( _mbLogInfo )
            {
                CProgram.sLogLine("MCT report: " + _mNrFiles.ToString() + " file from " + _mLastFilePath);
                if( _mNrFiles > 0)
                {
                    double durationSec = (_mEndTimeDT - _mStartTimeDT).TotalSeconds;
                    CProgram.sLogLine( "Recorder " + _mDeviceID + ": TZone(min)=" + _mTimeZoneOffsetMin.ToString() + "  " +
                        CProgram.sDateTimeToString(_mStartTimeDT) + " <= T(" + durationSec.ToString("0.0") + " sec) <= " + CProgram.sDateTimeToString(_mEndTimeDT));
                    _mItemBatt.mLog();
                    _mItemBattChrg.mLog();
                    _mItemRecording.mLog();
                    _mItemHrN.mLog();
                    _mItemHrAvg.mLog();
                    _mItemHrMin.mLog();
                    _mItemHrMax.mLog();
                    _mItemHrSd.mLog();
                    _mItemBrady.mLog();
                    _mItemTachy.mLog();
                    _mItemPause.mLog();
                    _mItemAfib.mLog();
                    _mItemAflut.mLog();
                    _mItemPAC.mLog();
                    _mItemPVC.mLog();
                    _mItemPace.mLog();
                    _mItemVtach.mLog();
                    _mItemAbNorm.mLog();
                    _mItemOther.mLog();
                    _mItemManual.mLog();
                    _mItemError.mLog();
                }
            }
        }

        public bool mbGetScpNumber( DateTime ADateTime, out UInt32 ArScpNumber, out bool AbrEstimate, bool AbRoundNext)
        {
            bool bOk = false;
            uint scpNumber = 0;
            bool bEstimate = true;

            if (_mStartScpNumber <= _mEndScpNumber)
            {
                DateTime lowDT = DateTime.MinValue;
                DateTime highDT = lowDT;
                bool bLowSet = false;
                bool bHighSet = false;
                UInt32 lowScpNumber = 0, highScpNumber = 0;

                foreach(CMctReportPoint point in _mItemHrAvg._mList)
                {
                    if (ADateTime >= point._mDateTime && (false == bLowSet || point._mDateTime > lowDT))
                    {
                        lowDT = point._mDateTime;
                        lowScpNumber = point._mScpNumber;
                        bLowSet = true;
                    }

                    if ((false == bHighSet || point._mDateTime < highDT) && point._mDateTime > ADateTime)
                    {
                        highDT = point._mDateTime;
                        highScpNumber = point._mScpNumber;
                        bHighSet = true;
                    }
                }

                if ( bLowSet && bHighSet && ( highScpNumber - lowScpNumber ) <= 1 )
                {
                    bEstimate = false;  // found the scp in a listed block
                    scpNumber = lowScpNumber;
                    bOk = true;
                }
                else
                {
                    if (false == bLowSet || false == bHighSet)
                    {
                        // not found in HR trend => try with start stop time
                        lowDT = _mStartTimeDT;
                        lowScpNumber = _mStartScpNumber;
                        highDT = _mEndTimeDT;
                        highScpNumber = _mEndScpNumber;
                    }
                    if (ADateTime >= lowDT && ADateTime <= highDT)
                    {
                        double rangeSec = (highDT - lowDT).TotalSeconds;
                        double tSec = (ADateTime - lowDT).TotalSeconds;

                        if( rangeSec < 1.0 || tSec < 1.0)
                        {
                            scpNumber = lowScpNumber;
                        }
                        else
                        {
                            UInt32 dScp = highScpNumber - lowScpNumber;
                            double f = tSec / rangeSec;
                            scpNumber = lowScpNumber + (UInt32)(f * dScp);
                        }
                        bOk = true;
                    }
                }
                if( AbRoundNext)
                {
                    if( scpNumber < _mEndScpNumber )
                    {
                        ++scpNumber;
                    }
                }
            }
            
            ArScpNumber = scpNumber;
            AbrEstimate = bEstimate;
            return bOk;
        }

        public double mGetDurationSec()
        {
            double durationSec = 0.0;

            if( _mEndTimeDT > _mStartTimeDT )
            {
                durationSec = (_mEndTimeDT - _mStartTimeDT).TotalSeconds;

            }

            return durationSec;
        }

        private UInt16 mCalcCrcBlock(Byte[] AData, UInt32 AIndex, UInt32 ALength)
        {
            UInt16 crcVal = 0xFFFF;
            UInt32 i = AIndex;
            UInt32 j;
            for (j = 0; j < ALength; ++i, ++j)
            {
                crcVal = (UInt16)(ccitt_crc16_table[(AData[i] ^ (crcVal >> 8)) & 0xff] ^ (crcVal << 8));
            }
            return crcVal;
        }

        private UInt16 mReadUInt16(Byte[] AData, UInt32 AIndex)
        {
            int val = (UInt16)AData[AIndex];
            int i = AData[AIndex + 1];
            val |= i << 8;

            return (UInt16)val;
        }

        private UInt32 mReadUInt32(Byte[] AData, UInt32 AIndex)
        {
            int val = (UInt16)AData[AIndex];
            int i = AData[AIndex + 1];
            val |= i << 8;
            i = AData[AIndex + 2];
            val |= i << 16;
            i = AData[AIndex + 3];
            val |= i << 24;

            return (UInt32)val;
        }

        private string mReadString(Byte[] AData, UInt32 AIndex, UInt16 AMaxLength)
        {
            string s = "";
            char c;
            uint i = AIndex;
            uint j = 0;

            while (j < AMaxLength)
            {
                c = (char)AData[i];
                if (c == 0)
                {
                    break;
                }
                s += c;
                ++i;
                ++j;
            }
            return s;
        }
        private void mLogLine(string ALine)
        {
            if (_mbLogLine)
            {
                CProgram.sLogLine(ALine);
            }
        }
        private void mLogLineDT(DateTime ADateTime, string ALine)
        {
            if (_mbLogLineDT)
            {
                string dtStr = "";

                if( ADateTime != DateTime.MinValue )
                {
                    dtStr = CProgram.sDateTimeToYMDHMS(ADateTime) + "->";
                }

                CProgram.sLogLine(dtStr + ALine);
            }
        }
        private void mLogLineDetail(string ALine)
        {
            if (_mbLogLineDetail)
            {
                CProgram.sLogLine(ALine);
            }
        }

        private void mLogError(string ALine)
        {
            if (_mbLogError)
            {
                CProgram.sLogError(ALine);
            }
        }
        private void mLogLineInt(string ALabel, int value, string AUnit)
        {
            if (_mbLogLine)
            {
                CProgram.sLogLine(ALabel + ": " + value.ToString() );
            }
        }

        public float mCalcHrStd( UInt16 AData )
        {
            return AData / 256.0F;
        }

    

        /*
         static int curSeq, curCount, curYear, curMonth, curDay, curHour, curMin, curSec, curMilSec, curTzOff;
        static int curHrMin, curHrMean, curHrMax, curHrN,  curNpm;
        static float curHrStd, curBatt;
        static bool bCurChrg,  bCurBrady, bCurTachy, bCurPause, bCurAfib, bCurManual, bCurRec, bCurErr;
        */

        public bool mbReadOneFile(string AFilePath, DDeviceType AType, Int16 ATimeZoneOffsetMin/*used for DVX*/)
        {
            bool bOk = false;

            _mDeviceType = AType;
            switch (AType)
            {
                case DDeviceType.TZ:
                    bOk = mbReadOneFileTZ(AFilePath);
                    break;
                case DDeviceType.Sirona:
                    bOk = mbReadOneFileSirona(AFilePath);
                    break;
                case DDeviceType.DV2: 
//                    bOk = mbReadOneFile(AFilePath);
                    break;
                case DDeviceType.DVX:
                    bOk = mbReadOneFileDvx(AFilePath, ATimeZoneOffsetMin);
                    break;
            }
            CProgram.sLogLine(_mNrFiles.ToString() + ": " + Path.GetFileName( AFilePath ) + " MCT start = " + CProgram.sDateTimeToYMDHMS(_mStartTimeDT)
    + " MCT end= " + CProgram.sDateTimeToYMDHMS(_mEndTimeDT) + ", Ntags=" + _mTotalNrTags .ToString());

            if (false == bOk)
            {
                CProgram.sLogInfo("MCT import error in " + AFilePath);
            }

            return bOk;
        }

        public bool mbReadOneFileSirona(string AFilePath)
        {
            bool bOk = false;
            string fileName = Path.GetFileName(AFilePath);

            try
            {
                string datFile = Path.ChangeExtension(AFilePath, ".dat");
                CRecordMit rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

                if (File.Exists(datFile))
                {
                    bOk = true; // skip file because it belongs to a event
                    if (_mbLogLine)
                    {
                        CProgram.sLogLine(_mNrFiles.ToString() + ": Skip Sirona trend belonging to event " + datFile);
                    }
                }
                else if (rec != null)
                {
                    string heaFile = Path.ChangeExtension(AFilePath, ".hea");

                    if (false == rec.mbReadAnnotationOnly(heaFile, DateTime.Now, 0 /*timezone of device is here unknown and all is relative to patient*/ ))
                    {
                        CProgram.sLogError("Failed trend read Sirona files " + AFilePath);
                    }
                    else if (rec.mAnnotationAtr == null || rec.mAnnotationAtr.mAnnotationsList == null)
                    {
                        CProgram.sLogError("Failed read Sirona trend file " + AFilePath);
                    }
                    else
                    {
                        ++_mNrFiles;
                        bOk = true;

                        if (_mDeviceID == null || _mDeviceID.Length == 0)
                        {
                            if (rec.mDeviceID != null && rec.mDeviceID.Length > 0)
                            {
                                _mDeviceID = rec.mDeviceID;
                            }
                            else
                            {


                                string name = Path.GetFileNameWithoutExtension(heaFile);
                                int pos = name.IndexOf('_');

                                _mDeviceID = pos > 0 ? name.Substring(0, pos - 1) : name;    // set deviceID from filename
                            }
                        }

                        bool bLogBeat = false; // _mNrFiles == 1;
                        bool bLogFile = true;
                        bool bLogDTOld = _mbLogLineDT;
                        if (bLogBeat)
                        {
                            _mbLogLineDT = bLogBeat;
                        }

                        DateTime startStripDT = rec.mGetDeviceTime(rec.mBaseUTC);
                        float stripLengthSec = rec.mRecDurationSec;
                        float tickSec = rec.mAnnotationAtr.mUnitT;
                        DateTime endStripDT = startStripDT.AddSeconds(stripLengthSec);
                        float sampleFreq = rec.mSampleFrequency;


                        DateTime startDT = rec.mAnnotationAtr.mGetDeviceTimeDT(0);

                        //                        if (bLogFile)
                        {
                            CProgram.sLogLine(_mNrFiles.ToString() + ": start " + CProgram.sDateTimeToYMDHMS(startStripDT) + " length= " + stripLengthSec.ToString("0.0")
                                + " freq= " + sampleFreq.ToString("0.0") + "end= " + CProgram.sDateTimeToYMDHMS(endStripDT));
                            CProgram.sLogLine(_mNrFiles.ToString() + ": MCT start = " + CProgram.sDateTimeToYMDHMS(_mStartTimeDT)
                                + " MCT end= " + CProgram.sDateTimeToYMDHMS(_mEndTimeDT));
                        }
                        /*LinkedList<CAnnotationLine> annList = new LinkedList<CAnnotationLine>();

                        CAnnotationLine annNode = new CAnnotationLine();

                        annList.AddLast(annNode);

                        LinkedListNode<CAnnotationLine> annPtr  = annList.First;
                        annPtr.Value.
                        
                        foreach (IEnumerable<CAnnotationLine> ann in rec.mAnnotationAtr.mAnnotationsList)
                        {
                            ann.Obje;
                        }
                        */


                        DateTime lastBeatDT = DateTime.MinValue;
                        DateTime beatDT = DateTime.MinValue;
                        bool _mbLastTimeValid = false;
                        double beatTime = 0, beatHR;
                        double beatMaxTime = rec.mMaxBeatTimeSec;
                        UInt32 nBeats = 0;
                        bool bStopOnEnd = true;
                        bool bFirst = true;

                        bool bWriteCvsBeat = false;
                        string cvsBeatFilePath = "";
                        StreamWriter fsCvsBeat = null;
                        bool bDoCalcMinMax = true;
                        float blockSizeSec = 30;
                        DateTime blockStartDT = DateTime.MinValue;
                        DateTime blockCenterDT;
                        double blockMeanT = 0;
                        double blockMinHR = 99999;
                        double blockMeanHR = 0;
                        double blockMaxHR = 0;
                        double blockRelT;
                        int blockBeatN = 0;
                        int nrBlocks = 1;

                        int maxRelTimeSec = 24 * 3600;
                        int maxAnnTick = rec.mAnnotationAtr.mUnitT > 1e-6 ? (int)( maxRelTimeSec / rec.mAnnotationAtr.mUnitT) : 1000000;

                        if( false == bDoCalcMinMax )
                        {
                            blockSizeSec = 0;   // each HR stored
                        }
                        if( bWriteCvsBeat)
                        {
                            cvsBeatFilePath = AFilePath + "_beat";

                            try
                            {
                                fsCvsBeat = new StreamWriter(cvsBeatFilePath);
                            }
                            catch( Exception ex)
                            {
                                CProgram.sLogException("failed open " + cvsBeatFilePath, ex);
                            }
                        }

                        foreach (CAnnotationLine ann in rec.mAnnotationAtr.mAnnotationsList)
                        {
                            UInt16 code = ann.mAnnotationCode;

                            beatDT = rec.mAnnotationAtr.mGetDeviceTimeDT(ann.mAnnotationTimeTck);

                            if(ann.mAnnotationTimeTck > maxAnnTick)
                            {
                                CProgram.sLogLine(_mNrFiles.ToString() + ": bad tick " + CProgram.sDateTimeToYMDHMS(beatDT) + " code " 
                                    + CAnnotationLine.sGetAnnotationLetter(code, ann.mAnnotationSubCode) + " " + code.ToString("X4"));
    
                                continue;
                            }

                            if (_mStartTimeDT == DateTime.MaxValue)
                            {
                                mFirstTime(beatDT);  // set all measured Items to first time
                            }
                            if (bFirst && _mEndTimeDT != DateTime.MinValue)
                            {
                                beatTime = (beatDT - _mEndTimeDT).TotalSeconds;
                                if (beatTime < -1.0F)
                                {
                                    CProgram.sLogLine(_mNrFiles.ToString() + ": first code of strip " + beatTime.ToString("0.000")
                                        + " sec before last= " + CProgram.sDateTimeToYMDHMS(_mEndTimeDT));

                                } else 
                                {
                                    mCheckFileGapAddHR0(beatDT, 0);
                                }
                                bFirst = false;
                            }
                            if (_mStartTimeDT > beatDT) _mStartTimeDT = beatDT;
                            if (_mEndTimeDT < beatDT) _mEndTimeDT = beatDT;

                            if (CAnnotationLine.sbIsBeat(code))
                            {
                                if (_mbLastTimeValid)
                                {
                                    ++nBeats;
                                    beatTime = (beatDT - lastBeatDT).TotalSeconds;

                                    if (beatTime <= beatMaxTime && beatTime >= 0.1)
                                    {
                                        beatHR = 60.0 / beatTime;

                                        if (fsCvsBeat != null)
                                        {
                                            string line = _mNrFiles.ToString() + "\t" + beatDT.ToString() + "\t" + beatTime.ToString("0.0000") + "\t" 
                                                + beatHR.ToString("0.0") + "\t" + CAnnotationLine.sGetAnnotationLetter(code, ann.mAnnotationSubCode)
                                                + "\t" + nrBlocks.ToString();

                                            fsCvsBeat.WriteLine(line);
                                        }
                                        blockRelT = (beatDT - blockStartDT).TotalSeconds;
                                        if (blockRelT > blockSizeSec)
                                        {
                                            // next block
                                            if (blockBeatN > 0)
                                            {
                                                blockMeanT /= blockBeatN;
                                                blockMeanHR /= blockBeatN;
                                                blockCenterDT = blockStartDT.AddSeconds(blockMeanT);
                                                _mItemHrAvg.mAddTailValue(blockCenterDT, 0, (UInt16)(blockMeanHR + 0.5), false);
                                                if (bDoCalcMinMax)
                                                {
                                                    _mItemHrMin.mAddTailValue(blockCenterDT, 0, (UInt16)(blockMinHR), false);
                                                    _mItemHrMax.mAddTailValue(blockCenterDT, 0, (UInt16)(blockMaxHR + 0.5), false);
                                                }

//                                                mLogLineDT(beatDT, "Heart Rate (BPM): " + blockMinHR.ToString("0.0") + " <= " + blockMeanHR.ToString("0.0")
//                                                    + " <= " + blockMaxHR.ToString("0.0") + " N= " + blockBeatN.ToString());

                                                ++nrBlocks;
                                                blockStartDT = beatDT;
                                                blockRelT = 0;
                                                blockBeatN = 0;
                                                blockMeanT = 0;
                                                blockMinHR = 99999;
                                                blockMeanHR = 0;
                                                blockMaxHR = 0;
                                            }
                                        }
                                        // averidge over blockSizeSec
                                        blockMeanT += blockRelT;
                                        ++blockBeatN;
                                        blockMeanHR += beatHR;
                                        if (beatHR < blockMinHR) blockMinHR = beatHR;
                                        if (beatHR > blockMaxHR) blockMaxHR = beatHR;

                                        if( beatHR > 210 || beatHR < 20)
                                        {
                                            int q = blockBeatN;
                                        }

//                                        mLogLineDT(beatDT, "Heart Rate (BPM): " + beatHR.ToString("0.0"));
                                        if (bLogBeat)
                                        {
                                            CProgram.sLogLine(_mNrFiles.ToString() + ": " + CProgram.sDateTimeToYMDHMS(beatDT) 
                                                + " code " + CAnnotationLine.sGetAnnotationLetter(code, ann.mAnnotationSubCode) + " " + code.ToString("X4")
                                                + " beat time " + beatTime.ToString("0.000") + "Heart Rate(BPM)= " + beatHR.ToString("0.0"));
                                        }

                                    }
                                    else if (bLogBeat)
                                    {
                                        CProgram.sLogLine(_mNrFiles.ToString() + ": " + CProgram.sDateTimeToYMDHMS(beatDT) + " code " 
                                            + CAnnotationLine.sGetAnnotationLetter(code, ann.mAnnotationSubCode) + " " + code.ToString("X4") + " bad beat time " + beatTime.ToString("0.000"));
                                    }
                                }
                                else if (bLogBeat)
                                {
                                    CProgram.sLogLine(_mNrFiles.ToString() + ": " + CProgram.sDateTimeToYMDHMS(beatDT) + " code " 
                                        + CAnnotationLine.sGetAnnotationLetter(code, ann.mAnnotationSubCode) + " " + code.ToString("X4") + " First beat");
                                }
                                _mbLastTimeValid = true;
                                lastBeatDT = beatDT;
                            }
                            #region Sirona_Beats
                            /* Sirona Rev 1.1
                    42 – Change in signal type. Each non-zero bit in the ‘subtyp’ field indicates that the corresponding signal is in a lead-off state
                            (the least significant bit corresponds to signal 0). 43 – Patient initiated event, corresponding to when the patient pressed the Record button. 
                    43 – Patient initiated event, corresponding to when the patient pressed the Record button.
                    44 – Arrhythmia start, with ‘subtyp’ field set to ‘t’, ‘b’, ‘p’, or ‘a’ for tachy, brady, pause, or afib respectively. (event mode only) 
                    45 – Arrhythmia end, with ‘subtyp’ field set to ‘t’, ‘b’, ‘p’, or ‘a’ for tachy, brady, pause, or afib respectively. (event mode only) 
                            public UInt16 _cSirona_LEAD_OFF = 42;
        public UInt16 _cSirona_MANUAL = 43;
        public UInt16 _cSirona_ONSET = 44;
        public UInt16 _cSirona_OFFSET = 45;

                            */
                            else if (code == CAnnotationLine._cSirona_MANUAL)    // manual
                            {
                                beatDT = rec.mAnnotationAtr.mGetDeviceTimeDT(ann.mAnnotationTimeTck);
                                _mItemManual.mAddPulse(beatDT, 0, 1);
                                mLogLineDT(beatDT, "Patient Activated Event.");
                            }

                            else if (code == CAnnotationLine._cSirona_ONSET) // entry
                            {
                                UInt32 subCode = ann.mAnnotationSubCode;

                                switch (subCode)
                                {
                                    case 't':
                                        _mItemTachy.mAddTailBool(beatDT, 0, true);
                                        mLogLineDT(beatDT, "Tachycardia Onset.");

                                        break;
                                    case 'b':
                                        _mItemBrady.mAddTailBool(beatDT, 0, true);
                                        mLogLineDT(beatDT, "Bradycardia Onset.");
                                        break;
                                    case 'p':
                                        _mItemPause.mAddTailBool(beatDT, 0, true);
                                        mLogLineDT(beatDT, "Pause Onset.");
                                        break;
                                    case 'a':
                                        _mItemAfib.mAddTailBool(beatDT, 0, true);

                                        mLogLineDT(beatDT, "Atrial Fibrillation Onset.");
                                        break;
                                    default:
                                        mLogLineDT(beatDT, "Unkown onset '" + subCode.ToString("X2") + "'");
                                        break;
                                }
                            }

                            else if (code == CAnnotationLine._cSirona_OFFSET) // exit
                            {
                                UInt32 subCode = ann.mAnnotationSubCode;

                                switch (subCode)
                                {
                                    case 't':
                                        _mItemTachy.mAddTailBool(beatDT, 0, false);
                                        mLogLineDT(beatDT, "Tachycardia Offset.");

                                        break;
                                    case 'b':
                                        _mItemBrady.mAddTailBool(beatDT, 0, false);
                                        mLogLineDT(beatDT, "Bradycardia Offset");
                                        break;
                                    case 'p':
                                        _mItemPause.mAddTailBool(beatDT, 0, false);
                                        mLogLineDT(beatDT, "Pause Offset.");
                                        break;
                                    case 'a':
                                        _mItemAfib.mAddTailBool(beatDT, 0, false);
                                        mLogLineDT(beatDT, "Atrial Fibrillation Offset");
                                        break;
                                    default:
                                        mLogLineDT(beatDT, "Unkown Offset '" + subCode.ToString("X2") + "'");
                                        break;
                                }
                            }
                            else
                            {
                                //                                mLogLineDT(beatDT, "Unkown code '" + code.ToString("X4") + "'");
                            }
                            #endregion
                            //    bOk &= mbAddTagSirona(ann);
                        }
                        if (blockBeatN > 0)
                        {
                            // add last averidge HR
                            blockMeanT /= blockBeatN;
                            blockMeanHR /= blockBeatN;
                            blockCenterDT = blockStartDT.AddSeconds(blockMeanT);
                            _mItemHrAvg.mAddTailValue(blockCenterDT, 0, (UInt16)(blockMeanHR + 0.5), false);
                            if (bDoCalcMinMax)
                            {
                                _mItemHrMin.mAddTailValue(blockCenterDT, 0, (UInt16)(blockMinHR), false);
                                _mItemHrMax.mAddTailValue(blockCenterDT, 0, (UInt16)(blockMaxHR + 0.5), false);
                            }    
                            mLogLineDT(beatDT, "Heart Rate (BPM): " + blockMinHR.ToString("0.0") + " <= " + blockMeanHR.ToString("0.0")
                                + " <= " + blockMaxHR.ToString("0.0") + " N= " + blockBeatN.ToString());

                        }

                        if (nBeats > 0 && _mbLastTimeValid && lastBeatDT != DateTime.MinValue && beatDT != DateTime.MinValue)
                        {
                            beatTime = (beatDT - lastBeatDT).TotalSeconds;

                            if (beatTime <= beatMaxTime)
                            {
                                bStopOnEnd = false;
                            }
                        }
                        if (fsCvsBeat != null)
                        {
                            fsCvsBeat.Close();
                        }
                            if (bStopOnEnd)
                        {
                            CProgram.sLogLine(_mNrFiles.ToString() + ": stop at end strip nBeats =" + nBeats.ToString() + " last beat time= " + beatTime.ToString("0.000"));
                            mbStopSignals();
                        }
                        if (bLogFile)
                        {
                            CProgram.sLogLine(_mNrFiles.ToString() + ": first " + CProgram.sDateTimeToYMDHMS(_mStartTimeDT) + "last= " + CProgram.sDateTimeToYMDHMS(_mEndTimeDT));
                        }
                        _mbLogLineDT = bLogDTOld;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed import Sirona trend file " + fileName, ex);
                bOk = false;
            }
            return bOk;
        }
        bool mbAddTagTz(DateTime ADateTime, UInt32 AScpNumber, UInt32 ATag, UInt32 AData)
        {
            bool bOk = true;
            float f;
            UInt16 data16 = (UInt16)AData;
            DTzR_Tag tag = (DTzR_Tag)ATag;

            // CTzReportItem _mItemBatt, _mItemBattChrg, _mItemHrAvg, _mItemHrMin, _mItemHrMax, _mItemHrSd, 
            // _mItemBrady, _mItemTachy, _mItemPause, _mItemAfib, _mItemManual, _mItemError;
            if (ADateTime == DateTime.MinValue)
            {
                f = 0.0F;
            }
            else
            {
                if (_mStartTimeDT == DateTime.MaxValue)
                {
                    mFirstTime(ADateTime);  // set all measured Items to first time
                }
                if (_mStartTimeDT > ADateTime) _mStartTimeDT = ADateTime;
                if (_mEndTimeDT < ADateTime) _mEndTimeDT = ADateTime;
                if (AScpNumber < _mStartScpNumber) _mStartScpNumber = AScpNumber;
                if (AScpNumber > _mEndScpNumber) _mEndScpNumber = AScpNumber;
            }

            ++_mTotalNrTags;
            if (ATag < (int)DTzR_Tag.NrEnums)
            {
                ++_mCounterTags[ATag];
            }
            switch (tag)
            {                                    // Decode the TAG and interpret the AData
                #region TAG_Processing_Switch
                case DTzR_Tag.BPM_COUNT_TAG:
                    //                    curHrN = AData;
                    _mItemHrN.mAddTailValue(ADateTime, AScpNumber, data16, false);
                    if( _mbLogLine ) mLogLineDT(ADateTime, "Samples used for Heart Rate calculations: " + AData.ToString());
                    break;
                case DTzR_Tag.BPM_STDEV_TAG:
                    _mItemHrSd.mAddTailValue(ADateTime, AScpNumber, data16, false);

                    //                    mAddDescriptionFloat("HrStd" + f.ToString());

                    if (_mbLogLine)
                    {
                        f = AData / 256.0F; mLogLineDT(ADateTime, "Heart Rate STDEV (BPM): " + f.ToString());
                    }
                    break;
                case DTzR_Tag.BPM_AVERAGE_TAG:
                    //                    curHrMean = AData;
                    _mItemHrAvg.mAddTailValue(ADateTime, AScpNumber, data16, false);
                    if (_mbLogLine) mLogLineDT(ADateTime, "Average Heart Rate (BPM): " + AData.ToString());
                    break;
                case DTzR_Tag.BPM_MIN_TAG:
                    _mItemHrMin.mAddTailValue(ADateTime, AScpNumber, data16, false);
                    //                    curHrMin = AData;
                    if (_mbLogLine) mLogLineDT(ADateTime, "Minimum Heart Rate (BPM): " + AData.ToString());
                    break;
                case DTzR_Tag.BPM_MAX_TAG:
                    _mItemHrMax.mAddTailValue(ADateTime, AScpNumber, data16, false);
                    //                    curHrMax = AData;
                    if (_mbLogLine) mLogLineDT(ADateTime, "Maximum Heart Rate (BPM): " + AData.ToString());
                    break;
                case DTzR_Tag.START_TAG:
                    _mItemRecording.mAddTailBool(ADateTime, AScpNumber, true);
                    //                    _mItem.mAdd(ADateTime, data16, false);
                    //                    bCurRec = true;  
                    if (_mbLogLine) mLogLineDT(ADateTime, "ECG Recording Started:" + AData.ToString());
                    break;
                case DTzR_Tag.STOP_TAG:
                    _mItemRecording.mAddTailBool(ADateTime, AScpNumber, false);
                    //bCurRec = false;
                    if (_mbLogLine) mLogLineDT(ADateTime, "ECG Recording Stopped:" + AData.ToString());
                    break;
                case DTzR_Tag.BREAK_TAG:
                    _mItemRecording.mAddTailBool(ADateTime, AScpNumber, false);
                    //                    bCurRec = false;
                    if (_mbLogLine) mLogLineDT(ADateTime, "ECG Recording Interrupted:" + AData.ToString());
                    break;
                case DTzR_Tag.RESUME_TAG:
                    _mItemRecording.mAddTailBool(ADateTime, AScpNumber, true);
                    //                    bCurRec = true;
                    mLogLineDT(ADateTime, "ECG Recording Resumed:" + AData.ToString());
                    break;
                case DTzR_Tag.FULL_TAG:
                    _mItemRecording.mAddTailBool(ADateTime, AScpNumber, false);
                    _mItemError.mAddPulse(ADateTime, AScpNumber, 1);
                    //                    bCurErr = true;
                    if (_mbLogLine) mLogLineDT(ADateTime, "ECG Recording Full:" + AData.ToString());
                    break;
                case DTzR_Tag.PACEMAKER_DETECTION_TAG:
                    _mItemPace.mAddPulse(ADateTime, AScpNumber, data16);
                    //                    curNpm = AData;
                    if (_mbLogLine) mLogLineDT(ADateTime, "Number of pacemaker spikes detected: " + AData.ToString());
                    break;
                case DTzR_Tag.QRS_DETECTION_TAG:
                    //                    bCurRec = true;
                    /*                    {
                                            char buffer[128];
                                            sprintf(buffer, "QRS=%d, QrsMask=%x", (AData & 0xff), AData >> 8);
                                            mAddDescription(buffer);
                                        }
                    */
                    char letter = (char)((char)AData & 0xff);
                    if (_mbLogLine) mLogLineDT(ADateTime, "QRS Detected(" + letter + "). Mask: 0x"
                        + ((int)AData >> 8).ToString("X2"));

                    switch (letter)
                    {
                        case 'N':
                            ++_mCounterQrs[(int)DTz_CountQRS.Normal];
                            break;
                        case 'S':
                            ++_mCounterQrs[(int)DTz_CountQRS.SVEB_PAC];
                            _mItemPAC.mAddPulse(ADateTime, AScpNumber, data16);

                            break;
                        case 'V':
                            ++_mCounterQrs[(int)DTz_CountQRS.VEB_PVC];
                            _mItemPVC.mAddPulse(ADateTime, AScpNumber, data16);
                            break;
                        case 'F':
                            ++_mCounterQrs[(int)DTz_CountQRS.Fusion];
                            break;
                        case 'Q':
                            ++_mCounterQrs[(int)DTz_CountQRS.QPaced];
                            //_mItemPace.mAddPulse(ADateTime, AScpNumber, data16);
                            break;
                        default:
                            ++_mCounterQrs[(int)DTz_CountQRS.Other];
                            break;
                    }
                    break;
                case DTzR_Tag.RHYTHM_CHANGE:
                    char letter2 = (char)((char)AData & 0xff);
                    if (_mbLogLine) mLogLineDT(ADateTime, "Rhythm change Detected(" + letter2 + "). Mask: 0x"
                        + ((int)AData >> 8).ToString("X2"));

                    switch (letter2)
                    {
                        case 'n':
                            ++_mCounterRhythm[(int)DTz_CountRhythm.Normal];
                            break;

                        case 'b':
                            ++_mCounterRhythm[(int)DTz_CountRhythm.Bradycardia];
                            break;

                        case 't':
                            ++_mCounterRhythm[(int)DTz_CountRhythm.Tachycardia];
                            break;

                        case 'a':
                            ++_mCounterRhythm[(int)DTz_CountRhythm.AtrialFirillation];
                            break;
                        case 'p':
                            ++_mCounterRhythm[(int)DTz_CountRhythm.Pause];
                            break;
                        case 'U':
                            ++_mCounterRhythm[(int)DTz_CountRhythm.Unreadable];
                            break;

                        default:
                            ++_mCounterRhythm[(int)DTz_CountRhythm.Other];
                            break;
                    }
                    break;

                case DTzR_Tag.TACHY_ENTRY_TAG:
                    _mItemTachy.mAddTailBool(ADateTime, AScpNumber, true);

                    //                    _mItemBrady.mCheck(ADateTime, 0, false);
                    //                    _mItemAfib.mCheck(ADateTime, 0, false);
                    //                    _mItemPause.mCheck(ADateTime, 0, false);
                    //                    _mItemPvc.mCheck(ADateTime, 0, false);
                    //                    bCurTachy = true;
                    if (_mbLogLine) mLogLineDT(ADateTime, "Tachycardia Onset. Mask: 0x" + AData.ToString("X2"));
                    break;
                case DTzR_Tag.TACHY_EXIT_TAG:
                    _mItemTachy.mAddTailBool(ADateTime, AScpNumber, false);
                    //                    bCurTachy = false;
                    if (_mbLogLine) mLogLineDT(ADateTime, "Tachycardia Offset. Mask: 0x" + AData.ToString("X2"));
                    break;
                case DTzR_Tag.BRADY_ENTRY_TAG:
                    _mItemBrady.mAddTailBool(ADateTime, AScpNumber, true);
                    //                    _mItemTachy.mCheck(ADateTime, 0, false);
                    //                    _mItemAfib.mCheck(ADateTime, 0, false);
                    //                    _mItemPause.mCheck(ADateTime, 0, false);
                    //                    _mItemPvc.mCheck(ADateTime, 0, false);
                    //                    bCurBrady = true;
                    if (_mbLogLine) mLogLineDT(ADateTime, "Bradycardia Onset. Mask: 0x" + AData.ToString("X2"));
                    break;
                case DTzR_Tag.BRADY_EXIT_TAG:
                    _mItemBrady.mAddTailBool(ADateTime, AScpNumber, false);
                    //                    bCurBrady = false;
                    if (_mbLogLine) mLogLineDT(ADateTime, "Bradycardia Offset. Mask: 0x" + AData.ToString("X2"));
                    break;
                case DTzR_Tag.PAUSE_ENTRY_TAG:
                    _mItemPause.mAddPulse(ADateTime, AScpNumber, 1);
//                    _mItemPause.mAddTailBool(ADateTime, AScpNumber, true);
                    //                    _mItemTachy.mCheck(ADateTime, 0, false);
                    //                    _mItemBrady.mCheck(ADateTime, 0, false);
                    //                    _mItemAfib.mCheck(ADateTime, 0, false);
                    //                    _mItemPvc.mCheck(ADateTime, 0, false);
                    //                    bCurPause = true;
                    mLogLineDT(ADateTime, "Pause Onset. Mask: 0x" + AData.ToString("X2"));
                    break;
                case DTzR_Tag.PAUSE_EXIT_TAG:
                    _mItemPause.mAddTailBool(ADateTime, AScpNumber, false);
                    //                    bCurPause = false;
                    if (_mbLogLine) mLogLineDT(ADateTime, "Pause Offset. Mask: 0x" + AData.ToString("X2"));
                    break;
                case DTzR_Tag.AFIB_ENTRY_TAG:
                    _mItemAfib.mAddTailBool(ADateTime, AScpNumber, true);
                    //                    _mItemTachy.mCheck(ADateTime, 0, false);
                    //                    _mItemBrady.mCheck(ADateTime, 0, false);
                    //                    _mItemPause.mCheck(ADateTime, 0, false);
                    //                    _mItemPvc.mCheck(ADateTime, 0, false);
                    //                    bCurAfib = true;

                    mLogLineDT(ADateTime, "Atrial Fibrillation Onset. Mask: 0x" + AData.ToString("X2"));
                    break;
                case DTzR_Tag.AFIB_EXIT_TAG:
                    _mItemAfib.mAddTailBool(ADateTime, AScpNumber, false);
                    //                    bCurAfib = false;
                    if (_mbLogLine) mLogLineDT(ADateTime, "Atrial Fibrillation Offset. Mask: 0x" + AData.ToString("X2"));
                    break;
                case DTzR_Tag.TACHY_RATE_CHANGE_TAG:
                    _mItemTachy.mAddTailBool(ADateTime, AScpNumber, true);
                    _mItemHrAvg.mAddTailValue(ADateTime, AScpNumber, data16, false);
                    //                    _mItemBrady.mCheck(ADateTime, 0, false);
                    //                    _mItemAfib.mCheck(ADateTime, 0, false);
                    //                    _mItemPause.mCheck(ADateTime, 0, false);
                    //                    _mItemPvc.mCheck(ADateTime, 0, false);
                    //                    bCurTachy = true;
                    if (_mbLogLine) mLogLineDT(ADateTime, "Tachycardia Rate Change. BPM: " + AData.ToString());
                    break;
                case DTzR_Tag.BRADY_RATE_CHANGE_TAG:
                    _mItemBrady.mAddTailBool(ADateTime, AScpNumber, true);
                    _mItemHrAvg.mAddTailValue(ADateTime, AScpNumber, data16, false);
                    //                    _mItemTachy.mCheck(ADateTime, 0, false);
                    //                    _mItemAfib.mCheck(ADateTime, 0, false);
                    //                    _mItemPause.mCheck(ADateTime, 0, false);
                    //                    _mItemPvc.mCheck(ADateTime, 0, false);
                    //bCurBrady = true;
                    if (_mbLogLine) mLogLineDT(ADateTime, "Bradycardia Rate Change. BPM: " + AData.ToString());
                    break;
                case DTzR_Tag.PVC_ENTRY_TAG:
                    _mItemPVC.mAddPulse(ADateTime, AScpNumber, data16);
//                    _mItemPVC.mAddTailBool(ADateTime, AScpNumber, true);
                    //                    _mItemTachy.mCheck(ADateTime, 0, false);
                    //                    _mItemBrady.mCheck(ADateTime, 0, false);
                    //                    _mItemAfib.mCheck(ADateTime, 0, false);
                    //                    _mItemPause.mCheck(ADateTime, 0, false);
                    //?
                    if (_mbLogLine) mLogLineDT(ADateTime, "PVC Onset. Mask: 0x" + AData.ToString("X2"));
                    break;
                case DTzR_Tag.ANALYSIS_RESUMED_TAG:
                    //                    _mItem.mAdd(ADateTime, data16, false);
                    //                    bCurRec = true;
                    if (_mbLogLine) mLogLineDT(ADateTime, "Arrhythmia Analysis Resumed. Mask: 0x" + AData.ToString());
                    break;
                case DTzR_Tag.ANALYSIS_PAUSED_TAG:
                    _mItemError.mAddPulse(ADateTime, AScpNumber, 1);
                    //bCurRec = false;
                    if (_mbLogLine) mLogLineDT(ADateTime, "Arrhythmia Analysis Paused. Mask: 0x" + AData.ToString("X2"));
                    break;
                case DTzR_Tag.LEAD_DISCONNECTED:
                    _mItemError.mAddPulse(ADateTime, AScpNumber, 1);
                    //                    _mItemRecording.mAdd(ADateTime, 0, false);
                    //                    bCurRec = false;
                    if (_mbLogLine) mLogLineDT(ADateTime, "ECG Electrodes Disconnected. Mask: 0x" + AData.ToString("X2"));
                    break;
                case DTzR_Tag.MISSING_SCP_TAG:
                    _mItemError.mAddPulse(ADateTime, AScpNumber, 1);
                    //?:
                    if (_mbLogLine) mLogLineDT(ADateTime, "Missing SCP File.".ToString());
                    break;
                case DTzR_Tag.MISSING_EVENT_TAG:
                    _mItemError.mAddPulse(ADateTime, AScpNumber, 1);
                    //?
                    if (_mbLogLine) mLogLineDT(ADateTime, "Missing Event File. Event Number: " + AData.ToString());
                    break;
                case DTzR_Tag.PATIENT_TAG:
                    _mItemManual.mAddPulse(ADateTime, AScpNumber, 1);
                    //                    bCurManual = true;
                    /*                    {
                                            char buffer[128];

                                            sprintf(buffer, "Man=%x, Symptm=%d, Activity=%d", AData, AData & 0xFF, AData >> 8);
                                            mAddDescription(buffer);
                                        }
                    */
                    if (_mbLogLine) mLogLineDT(ADateTime, "Patient Activated Event. Data: 0x" + AData.ToString("X2"));
                    if (0 == AData)
                    {
                        if (_mbLogLine) mLogLineDT(ADateTime, "---No Diary Info.".ToString());
                    }
                    else
                    {
                        if (0 != (AData & 0xff))
                        {
                            if (_mbLogLine) mLogLineDT(ADateTime, "---Patient Symptom: " + (AData & 0xff).ToString());
                        }
                        if (0 != (AData & 0xff00))
                        {
                            if (_mbLogLine) mLogLineDT(ADateTime, "---Patient Activity Level: " + ((AData >> 8) & 0xff).ToString());
                        }
                    }
                    break;
                case DTzR_Tag.BATTERY_SOC_TAG:
                    //curBat = (AData / 256) + (AData * 0.01F);
                    /*                    {
                                            char buffer[128];

                                            sprintf(buffer, "batPer=%d, Activity=%d.%d%%", AData, AData / 256, (AData % 255));
                                            mAddDescription(buffer);
                                        }
                    */
                    if (_mbLogLine) mLogLineDT(ADateTime, "Battery State of Charge: " + (AData / 256).ToString() + "." + (AData % 256).ToString() + "%");
                    break;
                case DTzR_Tag.BATTERY_VALUE_TAG:
                    _mItemBatt.mAddTailValue(ADateTime, AScpNumber, data16, false);
                    //                    curBatt = AData * 0.001F;

                    if (_mbLogLine) mLogLineDT(ADateTime, "Battery Voltage (mV): " + AData.ToString());
                    break;
                case DTzR_Tag.BATTERY_LOW_TAG:
                    _mItemBatt.mAddTailValue(ADateTime, AScpNumber, data16, true);
                    //                  curBatt = AData * 0.001F;

                    if (_mbLogLine) mLogLineDT(ADateTime, "Low Battery (mV): " + AData.ToString());
                    break;
                case DTzR_Tag.CHARGING_STARTED_TAG:
                    _mItemBattChrg.mAddTailValue(ADateTime, AScpNumber, 100, true);
                    //                    bCurChrg = true;

                    if (_mbLogLine) mLogLineDT(ADateTime, "Charging Started".ToString());
                    break;
                case DTzR_Tag.CHARGING_STOPPED_TAG:
                    _mItemBattChrg.mAddTailValue(ADateTime, AScpNumber, 99, false);
                    //                    bCurChrg = false;

                    if (_mbLogLine) mLogLineDT(ADateTime, "Charging Stopped".ToString());
                    break;
                case DTzR_Tag.TIME_ZONE_CHANGE_TAG:
                    //?

                    if (_mbLogLine) mLogLineDT(ADateTime, "Time Zone Changed. New offset (minutes): " + ((UInt16)(AData)).ToString());
                    break;
                case DTzR_Tag.SERVER_REQUEST_TAG:
                    //?
                    if (_mbLogLine) mLogLineDT(ADateTime, "Server Requested " + AData.ToString() + " SCP Files");
                    break;
                case DTzR_Tag.TZR_REQUEST_TAG:
                    //?
                    /*                    {
                                            char buffer[128];

                                            sprintf(buffer, "TzrDay=%d, TzrHour=%d", AData >> 8, (AData % 255));
                                            mAddDescription(buffer);
                                        }

                    */
                    if (_mbLogLine) mLogLineDT(ADateTime, "Interval File Request. Day: " + ((int)AData >> 8).ToString() + ", Hour: " + ((int)(AData & 0xff)).ToString());
                    break;
                case DTzR_Tag.SCP_RETRANSMIT_TAG:
                    //?
                    if (_mbLogLine) mLogLineDT(ADateTime, "SCP Retransmission: 0x" + AData.ToString("X2"));
                    break;
                case DTzR_Tag.SETTINGS_SUCCESS_TAG:
                    //?
                    if (_mbLogLine) mLogLineDT(ADateTime, "Settings Downloaded Successfully. File ID: " + AData.ToString());
                    break;
                case DTzR_Tag.SETTINGS_FAILURE_TAG:
                    _mItemError.mAddPulse(ADateTime, AScpNumber, 1);
                    //                    bCurErr = true;
                    //                    mAddDescriptionInt("SettingsErr", AData, "");
                    mLogLineDT(ADateTime, "Settings Download Error Code: " + AData.ToString());
                    break;
                case DTzR_Tag.ACTIONS_FAILURE_TAG:
                    _mItemError.mAddPulse(ADateTime, AScpNumber, 1);
                    //                    bCurErr = true;
                    if (_mbLogLine) mLogLineDT(ADateTime, "Actions Download Error Code: " + AData.ToString());
                    break;
                case DTzR_Tag.ACTIONS_SUCCESS_TAG:
                    //mAddDescriptionInt("AcctionsID", AData, "");
                    if (_mbLogLine) mLogLineDT(ADateTime, "Actions Downloaded Successfully. File ID: " + AData.ToString());
                    break;
                case DTzR_Tag.SMS_COMMAND_ERROR_TAG:
                    _mItemError.mAddPulse(ADateTime, AScpNumber, 1);
                    //bCurErr = true;
                    if (_mbLogLine) mLogLineDT(ADateTime, "SMS Command Error Code: " + AData.ToString());
                    break;
                case DTzR_Tag.SMS_MSG_RECEIVED:
                    if (_mbLogLine) mLogLineDT(ADateTime, "SMS Message Acknowledge Code: " + AData.ToString());
                    break;
                case DTzR_Tag.FTP_SETTINGS_CHANGED:
                    if (_mbLogLine) mLogLineDT(ADateTime, "Change in FTP Settings".ToString());
                    break;
                case DTzR_Tag.TERMINATOR_TAG:          // The final entry should always have a value of 0xff

                    if (_mbLogLine)
                    {
                        if (AData == 0xff) mLogLineDT(ADateTime, "Final Entry.".ToString());
                        else mLogLineDT(ADateTime, "Invalid Terminator.".ToString());
                    }
                    break;
                default:
                    //bCurErr = true;
                    if (_mbLogLine) mLogLineDT(ADateTime, "ERROR: unknown tag(" + ATag.ToString() + ") data(" + AData.ToString("X4"));
                    break;
                    #endregion
            }
            return bOk;
        }

        public bool mbReadOneFileTZ(string AFilePath)
        {
            bool bOk = false;
            string fileName = Path.GetFileName(AFilePath);
            System.IO.FileStream fstream = null;

            try
            {
                UInt16 ourcrcValue;                  // The CRC we calculate
                UInt16 theircrcValue;                // The CRC from the file
                UInt16 fileCrcValue;
                uint length;                         // The length read from the file

                Byte[] readBuffer;                       // Pointer used for the read command
 //               UInt16 readIndex = 0;

                Byte[] firstBlock = new Byte[16];
                Byte[] tempKey = new Byte[16];
                Byte[] tempIV = new Byte[16];

                UInt32 i;
                DateTime tagDT;

                ulong nrBadTimes = 0;

                bool bFirstDT = true;

                _mLastFilePath = AFilePath;

                // turn off aes, not implemented                aes_context aes_ctx;

                fstream = File.OpenRead(AFilePath);
                if (fstream == null)
                {
                    mLogError("Failed open file. Aborting.");
                    return false;
                }
                Int32 fileLength = (Int32)fstream.Length;
                // ReadFully(stream);

                // Make sure the file is not too big for the format
                if (fileLength > MAX_FILE_SIZE)
                {
                    mLogError("File size is SIGNIFICANTLY larger than expected. Aborting.");
                    fstream.Close();
                    return false;
                }
                // Make sure the file is not too small for the format
                if (fileLength < MIN_FILE_SIZE)
                {
                    mLogError("File size is too small for an SCP file. Aborting.");
                    fstream.Close();
                    return false;
                }
                // Read in the first TWO 16 byte blocks to see if the file has been encrypted
                for (i = 0; i < 16; i++)
                {
                    firstBlock[i] = 0;
                }
                fstream.Read(firstBlock, 0, 16);           // Read the first block into RAM

                // Check for the format identifier string
                string str = mReadString(firstBlock, 6, 6);

                if (0 != str.CompareTo(TZR_ID_STRING))
                {
                    #region AES
#if SCP_AES == false
                    mLogError("File may be encrypted. No decrypt present.");
                    fstream.Close();
                    return false;
#else
#endif
                    #endregion
                }
                else
                {
                    mLogLine("File not encrypted. Processing");

                    i = 0;
                    theircrcValue = mReadUInt16(firstBlock, i);
                    i += 2;

                    length = mReadUInt32(firstBlock, i);
                    i += 4;

                    if( length == 0)
                    {
                        length = (UInt32)fileLength;
                    }

                    readBuffer = new Byte[fileLength + 16];          // Allocate enough space to read in the whole file     
                    for (i = 0; i < 16; i++)
                    {
                        readBuffer[i] = firstBlock[i];                 // Copy the first block into the file buffer
                    }
                    fstream.Read(readBuffer, 16, fileLength - 16);    // Store the remainder of the file in memory

                    ourcrcValue = mCalcCrcBlock(readBuffer, 2, (UInt32)length - 2); // Calculate the CRC for the remainder of the file

                    fileCrcValue = mCalcCrcBlock(readBuffer, 2, (UInt32)fileLength - 2); // Calculate the CRC for the remainder of the file
                }

#if CHECK_CRC
                if (ourcrcValue == theircrcValue)
                {            // If the CRC doesn't match, something has gone wrong
                    mLogLine("File CRC Valid: " + theircrcValue.ToString("X2"));  // Notify user that we are parsing
#else
                mLogLine("Ignoring CRC (0x" << hex << theircrcValue
                  << " : 0x" << ourcrcValue << ")" << dec);    // Ignore the CRC values (debugging ONLY)
#endif

                    #region Process Read buffer
                    i = 6;
                    mLogLine("File Length: " + length.ToString());            // Print out the file length      
                    mLogLine("Format Identifier String: " + mReadString(readBuffer, i, 6));
                    i += 6;

                    mLogLine("Device Identifier String: " + mReadString(readBuffer, i, 6));
                    i += 6;
                    mLogLine("Firmware Version: " + (readBuffer[i] / 10).ToString() + "." + (readBuffer[i] % 10).ToString());
                                       // Parse out the firmware version
                    i += 1;
                    _mDeviceID = mReadString(readBuffer, i, 8);
                    mLogLine("Serial Number: " + _mDeviceID);       // Parse out the Serial Number String
                    i += 8;
                    _mPatientID = mReadString(readBuffer, i, 40);
                    mLogLine("Patient ID: " + _mPatientID);          // Parse out the Patient ID String
                    i += 40;

                    bOk = true;
                    ++_mNrFiles;
                    
                    while (i < (length - 7))
                    {                              // Parse the events until the end of the file
                        UInt32 tag = readBuffer[i];                                         // Identifier tag
                        i += 1;
                        UInt32 sequenceNumber = mReadUInt32(readBuffer, i);   // Sequence number
                        i += 4;
                        UInt32 sampleCount = mReadUInt16(readBuffer, i);      // Sample count
                        i += 2;
                        UInt32 year = mReadUInt16(readBuffer, i);            // Year
                        i += 2;
                        UInt32 month = readBuffer[i];                                       // Month
                        i += 1;
                        UInt32 day = readBuffer[i];                                         // Day
                        i += 1;
                        UInt32 hour = readBuffer[i];                                        // Hour
                        i += 1;
                        UInt32 minute = readBuffer[i];                                      // Minute
                        i += 1;
                        UInt32 second = readBuffer[i];                                      // Seconds
                        i += 1;
                        UInt32 milliseconds = (UInt32)readBuffer[i] * 4;                            // Milliseconds / 4
                        i += 1;
                        _mTimeZoneOffsetMin = (Int16)mReadUInt16(readBuffer, i);        // Time zone offset (minutes)

                        i += 2;
                        UInt32 dataLength = readBuffer[i];                                  // Data Length
                        i += 1;
                        UInt32 data = 0, k, mult = 1;
                        for (k = 0; k < dataLength; k++)
                        {
                            data = data + readBuffer[i + k] * mult;                                    // Data Value
                            mult *= 256;
                        }
                        i += dataLength;
                        UInt32 value0 = readBuffer[i];                                        // Always 0
                        i += 1;

                        if (value0 == 0)
                        {
                            if (_mbLogLineDetail)
                            {
                                mLogLineDetail("[" + sequenceNumber.ToString("D6") + ", " + sampleCount.ToString("D6") + "] "
                                + year.ToString("D4") + "/" + month.ToString("D2") + "/" + day.ToString("D2") + " "
                                + hour.ToString("D2") + ":" + minute.ToString("D2") + ":" + second.ToString("D2") + "."
                                + milliseconds.ToString("D2") + "+" + _mTimeZoneOffsetMin.ToString("D6") + "min, tag= "
                                + tag.ToString("X2") + "=" + data.ToString("X4"));
                            }
                            try
                            {
                                if(milliseconds > 999)
                                {
                                    if (nrBadTimes == 0)
                                    {
                                        CProgram.sLogError("bad DateTime.msec [" + sequenceNumber.ToString("D6") + ", " + sampleCount.ToString("D6") + "] "
                                             + year.ToString("D4") + "/" + month.ToString("D2") + "/" + day.ToString("D2") + " "
                                             + hour.ToString("D2") + ":" + minute.ToString("D2") + ":" + second.ToString("D2") + "."
                                             + milliseconds.ToString("D2") + "+" + _mTimeZoneOffsetMin.ToString("D6") + "min, tag= "
                                             + tag.ToString("X2") + "=" + data.ToString("X4"));
                                    }
                                    ++nrBadTimes;
                                    milliseconds = 999;
                                }
                                tagDT = new DateTime((int)year, (int)month, (int)day, (int)hour, (int)minute, (int)second, (int)milliseconds);

                                if (year > 0)
                                {
                                    tagDT = tagDT.AddMinutes(_mTimeZoneOffsetMin);

                                    if(bFirstDT)
                                    {
                                        mCheckFileGapAddHR0( tagDT, sequenceNumber);
                                        /*                                          if ( _mNrFiles > 1)
                                                                                {
                                                                                    double deltaMin = (tagDT - _mEndTimeDT ).TotalMinutes;

                                                                                    if( deltaMin > 55)
                                                                                    {
                                                                                        // missing at least 1 hour
                                                                                        ++_mNrMissingFiles;

                                                                                        DateTime dt1 = _mEndTimeDT.AddMinutes(20);

                                                                                        _mItemHrMin.mAddTailValue(dt1, sequenceNumber, 0, false); // move to axis to indicate missing file
                                                                                        _mItemHrAvg.mAddTailValue(dt1, sequenceNumber, 0, false);
                                                                                        _mItemHrMax.mAddTailValue(dt1, sequenceNumber, 0, false);

                                                                                        dt1 = tagDT.AddMinutes(-20);

                                                                                        _mItemHrMin.mAddTailValue(dt1, sequenceNumber, 0, false); // move to axis to indicate missing file
                                                                                        _mItemHrAvg.mAddTailValue(dt1, sequenceNumber, 0, false);
                                                                                        _mItemHrMax.mAddTailValue(dt1, sequenceNumber, 0, false);
                                                                                    }
                                                                                }
                                         */
                                        bFirstDT = false;
                                    }
                                }


                                bOk &= mbAddTagTz(tagDT, sequenceNumber, tag, data);
                            }
                            catch (Exception ex)
                            {
                                CProgram.sLogError("bad DateTime [" + sequenceNumber.ToString("D6") + ", " + sampleCount.ToString("D6") + "] "
                                     + year.ToString("D4") + "/" + month.ToString("D2") + "/" + day.ToString("D2") + " "
                                     + hour.ToString("D2") + ":" + minute.ToString("D2") + ":" + second.ToString("D2") + "."
                                     + milliseconds.ToString("D2") + "+" + _mTimeZoneOffsetMin.ToString("D6") + "min, tag= "
                                     + tag.ToString("X2") + "=" + data.ToString("X4"));
                                CProgram.sLogException("Failed import TZr file due to bad date time " + fileName, ex);
                            }
                        }
                        else
                        {                // File is out of sync, a value was detected where a NULL should be
                            mLogLine("Parse Error at " + i.ToString() + " Aborting.");      // Not worth tryign to recover from. Just abort.
                                                                                            /*                            UInt32 k;
                                                                                                                        for (k = i; k < (i + 20); k++)
                                                                                                                        {
                                                                                                                            mLogLine(hex << setw(2) << ((UInt32)pRead[k] & 0xff) << " " + dec;
                                                                                                                        }
                                                                                            */
                            bOk = false;
                            break;
                        }

                    }
                    #endregion

                    mLogCounters();
#if CHECK_CRC
                }
                else
                {
                    mLogLine("File CRC Invalid. Halting");
                    mLogLine("their CRC: " + theircrcValue.ToString("X2") + " - our CRC: " + ourcrcValue.ToString("X2"));
                    mLogLine("File Length: " + length.ToString());            // Print out the file length      
                }
#endif
                if(nrBadTimes>1)
                {
                    CProgram.sLogLine(nrBadTimes.ToString() + " bad DateTime encountered");
                }
                CProgram.sLogLine("Read TZ " + fileName + " " + _mTotalNrTags + " tags Done.\n");                 // Signal completion of program
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed import TZr file " + fileName, ex);
            }
            if (fstream != null)
            {
                fstream.Close();
                fstream.Dispose();
            }
            return bOk;
        }
        public bool mbReadOneFileDvx(string AFilePath, Int16 ATimeZoneOffsetMin )
        {
            bool bOk = false;
            string fileName = Path.GetFileName(AFilePath);
            TrendFile trend = null;
            UInt32 hrMin = UInt32.MaxValue;
            UInt32 hrMax = UInt32.MinValue;
            UInt32 nOk = 0;
            UInt32 nBad = 0;
            bool bFirst = true;
            Int32 nValues = 0;

            UInt32 dvxMaxHR = 350;

            try
            {
  
                try
                {
                    trend = new TrendFile(AFilePath);
                }
                catch (Exception ex)
                {
                    CProgram.sLogException("Failed loading DVX trend file " + fileName, ex);
                }

                nValues = trend.Values == null ? 0 : trend.Values.Count;

                if( nValues > 0)
                {
                    byte maskAF = 1 << 0;

                    DateTime dt;
                    UInt32 hr;

                    byte manual, heartCondition;

                    if (ATimeZoneOffsetMin != _mTimeZoneOffsetMin)
                    {
                        CProgram.sLogLine("Multiple time zones " + ATimeZoneOffsetMin + "!=" + _mTimeZoneOffsetMin);
                    }
                    ++_mNrFiles;


                    foreach (TrendValues trendValue in trend.Values)
                    {
                        try
                        {
                            hr = trendValue.HeartRate;

                            if (trendValue.Header.TimeStamp.Year > 2000 && hr <= dvxMaxHR)   // skip invalid values
                            {
                                dt = trendValue.Header.TimeStamp.DateTime;
                                {
                                    dt = dt.AddMinutes(ATimeZoneOffsetMin);

                                    manual = trendValue.ManualEvent;
                                    heartCondition = trendValue.HeartCondition;

                                    if (_mStartTimeDT == DateTime.MaxValue)
                                    {
                                        mFirstTime(dt);  // set all measured Items to first time
                                    }
                                    if (_mStartTimeDT > dt) _mStartTimeDT = dt;
                                    if (_mEndTimeDT < dt) _mEndTimeDT = dt;

                                    if (bFirst)
                                    {
                                        mCheckFileGapAddHR0(dt, 0);
                                        bFirst = false;
                                    }
                                    if (hr > hrMax) hrMax = hr;
                                    if (hr < hrMin) hrMin = hr;

                                    _mItemHrAvg.mAddTailValue(dt, 0, (UInt16)hr, false);
                                    _mItemHrMin.mAddTailValue(dt, 0, (UInt16)hr, false);
                                    _mItemHrMax.mAddTailValue(dt, 0, (UInt16)hr, false);
                                    ++nOk;
                                    ++_mTotalNrTags;
                                }
                            }
                            else
                            {
                                ++nBad;
                            }

                        }
                        catch (Exception ex)
                        {
                            ++nBad;
                            mLogCounters();
                            if (nBad > 1)
                            {
                                CProgram.sLogLine("DVX trend " + nBad.ToString() + " bad DateTime encountered");
                            }
                            CProgram.sLogError("Read DVX trend " + fileName + " " + _mTotalNrTags + " tags Done.\n");                 // Signal completion of program
                        }
                    }
//
                }
                bOk = nOk > 0;

                if ( bOk == false)
                {
                    CProgram.sLogLine("DVX trend read " + fileName + " ok=" + nOk + "bad= " + nBad.ToString() + " HRmin=" + hrMin + " HRmax=" + hrMax);
                }
                else if ( _mDeviceID == "")
                {
                    UInt32 snr;
                    DateTime dt;
                    string ext;
                    if (CDvxEvtRec.sbParseFileName(fileName, out snr, out dt, out ext))
                    {
                        _mDeviceID = CDvxEvtRec.sGetDeviceName(snr);
                    }
                }
                trend.Values.Clear();   // dispose trend list 
            }
            catch (Exception ex)
            {
                CProgram.sLogException("Failed import DVX trend file " + fileName, ex);
            }
            return bOk;
        }
        public void mCheckFileGapAddHR0(DateTime ADateTime, UInt32 ASequenceNr)
        {
            if (_mNrFiles > 1 && _mEndTimeDT != DateTime.MinValue)
            {
                double deltaMin = (ADateTime - _mEndTimeDT).TotalMinutes;

                if (deltaMin > 30 ) //55)
                {
                    // missing at least 1 hour
                    ++_mNrMissingFiles;

                    DateTime dt1 = _mEndTimeDT.AddMinutes(10);

                    _mItemHrMin.mAddTailValue(dt1, ASequenceNr, 0, false); // move to axis to indicate missing file
                    _mItemHrAvg.mAddTailValue(dt1, ASequenceNr, 0, false);
                    _mItemHrMax.mAddTailValue(dt1, ASequenceNr, 0, false);

                    dt1 = ADateTime.AddMinutes(-10);

                    _mItemHrMin.mAddTailValue(dt1, ASequenceNr, 0, false); // move to axis to indicate missing file
                    _mItemHrAvg.mAddTailValue(dt1, ASequenceNr, 0, false);
                    _mItemHrMax.mAddTailValue(dt1, ASequenceNr, 0, false);
                }
            }
        }
    }
}

