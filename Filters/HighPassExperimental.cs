﻿using DynaVisionGraph.Filter;

namespace DynaVisionGraph.Filter
{
    public class HighPassExperimental : Filter
    {
        private int y1, y2;
        private /*readonly*/ FilterHistory<int> x = new FilterHistory<int>(20);

        protected override int Process(int x0)
        {
            return x0;
        }
        protected override void mDoReset()
        {
            x = new FilterHistory<int>(20);   // clear by creating new array
        }

        public override string Name => "HighPass Experimental";
    }
}
