﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DynaVisionGraph.Filter;

namespace TechmedicEcgViewer.Filters
{
    public class InvertFilter : Filter
    {
        public override string Name => "Invert filter";

        public override string ToString()
        {
            return "Invert filter";
        }
        protected override void mDoReset()
        {
        }

        protected override int Process(int x0)
        {
            return -x0;
        }
    }
}
