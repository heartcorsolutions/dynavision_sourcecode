﻿using System;
using DynaVisionGraph.Filter;

namespace DynaVisionGraph.Filter
{
    public class HighPassFilter : Filter
    {
        private int y1;

        private FilterHistory<int> lazy_x;
        private  FilterHistory<int> x => lazy_x ?? (lazy_x = new FilterHistory<int>(256));

        protected override int Process(int x0)
        {

            var y0 = y1 + x0 - x[256];
            y1 = y0;
            x.AddSample(x0);

            return x0 - y0 / 256;
        }

        protected override void mDoReset()
        {
            lazy_x = new FilterHistory<int>(256);   // clear by creating new array
        }

        public override string Name => "HighPass Filter " + 256;

        public override string ToString()
        {
            return "HighPass filter";
        }
    }

    public class HighPass1RCFilter : Filter
    {
        // from https://en.wikipedia.org/wiki/High-pass_filter
        // var real α := RC / (RC + dt)
        //   y[i] := α * (y[i-1] + x[i] - x[i-1])
        private int _mPrevX;
        private double _mPrevY; // use double for sub unit precision to get smooth discharge curve 
        private bool _mbFirst;

        private float _mFreqHz;
        private UInt32 _mSampleFreqSps;
        private double _mAlpha;

        public HighPass1RCFilter( float AFreqHz, UInt32 ASampleFreqSps)
        {
            mInit(AFreqHz, ASampleFreqSps);

        }

        public void mInit(float AFreqHz, UInt32 ASampleFreqSps)
        {
            _mPrevX = 0;
            _mPrevY = 0;
            _mbFirst = true;

            _mFreqHz = AFreqHz;
            _mSampleFreqSps = ASampleFreqSps == 0 ? 1 : ASampleFreqSps;

            // a = 1 / ( 2 pi fc/fs + 1)
            _mAlpha = 1.0 / ( 2 * Math.PI * _mFreqHz / _mSampleFreqSps + 1.0);


            // 0.05 / 1000 => a = 
            /*
              freq	    sample	alpha
                Hz	    Sps	
                0,05	200	0,99843167
                0,5	    200	0,98453496
		
                0,05	256	0,99877432
                0,5	    256	0,98787693
		
                0,05	1000	0,99968594
                0,5	    1000	0,99686825

             */
        }

        protected override int Process(int x0)
        {
            double y1;

            if (_mbFirst)
            {
                y1 = x0;
                _mbFirst = false;
            }
            else
            {
                y1 = _mAlpha * (_mPrevY + x0 - _mPrevX);
            }
            _mPrevX = x0;
            _mPrevY = y1;

            return (int)y1;
        }

        public int mDoFilter(int x0)       // directly callable
        {
            double y1;

            if (_mbFirst)
            {
                y1 = x0;
                _mbFirst = false;
            }
            else
            {
                y1 = _mAlpha * (_mPrevY + x0 - _mPrevX);
            }
            _mPrevX = x0;
            _mPrevY = y1;

            return (int)y1;
        }
        public double mGetTauRcSec()
        {
            double rc = _mFreqHz < 0.00001 ? 0.0 :  1 / (2 * Math.PI * _mFreqHz);

            return rc;
            
        }
        public double mExpFactor( double ATimeSec)
        {
            // e ^(-t / RC)  = e^(-t * 2 * pi * fc)    
            double factor = Math.Exp(-ATimeSec * 2 * Math.PI * _mFreqHz);

            return factor;
        }

        protected override void mDoReset()
        {
            mInit(_mFreqHz, _mSampleFreqSps);

        }

        public override string Name => "HighPass 1RC Filter " + _mFreqHz.ToString( "0.00" );

        public override string ToString()
        {
            return "HighPass 1RC filter";
        }
    }
}
