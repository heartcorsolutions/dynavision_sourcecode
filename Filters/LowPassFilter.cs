﻿using System;
using DynaVisionGraph.Filter;

namespace DynaVisionGraph.Filter
{
    public class LowPassFilter : Filter
    {
        private int y1, y2;
        private FilterHistory<int> lazy_x;
        private FilterHistory<int> x => lazy_x ?? (lazy_x = new FilterHistory<int>(8));
        
        protected override int Process(int x0)
        {
            var y0 = 2 * y1 - y2 + x0 - 2 * x[4] + x[8];

            y2 = y1;
            y1 = y0;

            x.AddSample(x0);

            return y0 / 16;
        }
        protected override void mDoReset()
        {
            lazy_x = new FilterHistory<int>(256);   // clear by creating new array
        }

        public override string Name => "LowPass Filter " + 16;
    }

    public class LowPass1RCFilter : Filter
    {
        // from https://en.wikipedia.org/wiki/Low-pass_filter
        // var real α := dt / (RC + dt)
        //   y[i] := y[i-1] + α * (x[i] - y[i-1])
        private int _mPrevX;
        private double _mPrevY; // use double for sub unit precision to get smooth discharge curve 
        private bool _mbFirst;

        private float _mFreqHz;
        private UInt32 _mSampleFreqSps;
        private double _mAlpha;

        public LowPass1RCFilter(float AFreqHz, UInt32 ASampleFreqSps)
        {
            mInit(AFreqHz, ASampleFreqSps);
        }

        public void mInit(float AFreqHz, UInt32 ASampleFreqSps)
        {
            _mPrevX = 0;
            _mPrevY = 0;
            _mbFirst = true;

            _mFreqHz = AFreqHz;
            _mSampleFreqSps = ASampleFreqSps == 0 ? 1 : ASampleFreqSps;

            // a = (2 pi fc/fs)/ ( 2 pi fc/fs + 1)
            double d = 2 * Math.PI * _mFreqHz / _mSampleFreqSps;
            _mAlpha = d / (d + 1.0);   
        }

        protected override int Process(int x0)
        {
            double y1;

            if (_mbFirst)
            {
                y1 = x0;
                _mbFirst = false;
            }
            else
            {
                y1 = _mPrevY + _mAlpha * (x0 - _mPrevY);
            }
            _mPrevX = x0;
            _mPrevY = y1;

            return (int)y1;
        }
        public int mDoFilter(int x0) // direct call
        {
            double y1;

            if (_mbFirst)
            {
                y1 = x0;
                _mbFirst = false;
            }
            else
            {
                y1 = _mPrevY + _mAlpha * (x0 - _mPrevY);
            }
            _mPrevX = x0;
            _mPrevY = y1;

            return (int)y1;
        }
        public double mGetTauRcSec()
        {
            double rc = _mFreqHz < 0.00001 ? 0.0 : 1 / (2 * Math.PI * _mFreqHz);

            return rc;

        }
        public double mExpFactor(double ATimeSec)
        {
            // e ^(-t / RC)  = e^(-t * 2 * pi * fc)    
            double factor = Math.Exp(-ATimeSec * 2 * Math.PI * _mFreqHz);

            return factor;
        }

        protected override void mDoReset()
        {
            mInit(_mFreqHz, _mSampleFreqSps);

        }

        public override string Name => "LowPass 1RC Filter " + _mFreqHz.ToString("0.0");

        public override string ToString()
        {
            return "LowPass 1RC filter";
        }
    }
}
