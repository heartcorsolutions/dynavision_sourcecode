﻿using System;
using DynaVisionGraph.Filter;

namespace DynaVisionGraph.Filter
{

    /*
     *  50Hz Powerline Interference filter. non-adaptive. 200SPS samplerate.
     */
    public class Notch50 : Filter
    {
        private /*readonly*/ FilterHistory<int> x = new FilterHistory<int>(4);

        protected override int Process(int x0)
        {
//            var filtered = (x0 + x2) / 2;                                 // 1st order
            var y0 = (x0 + 2 * x[2] + x[4]) / 4;                        // 2nd order
//            var filtered = (x0 + 3 * x2 + 3 * x4 + x6) / 8;               // 3th order
//            var filtered = (x0 + 4 * x2 + 6 * x4 + 4 * x6 + x8) / 16;     // 4th order

            x.AddSample(x0);

            return y0;
        }
        protected override void mDoReset()
        {
            x = new FilterHistory<int>(4);   // clear by creating new array
        }

        public override string Name => "50Hz Notch FIR";
    }

    public enum DFilterType
    {
        LowPass,        // use 40, 100, 150 Hz
        HighPass,       // use 0.05, 0.5, 3.5 Hz
        BandBlock       // use 50, 100 Hz
    }

    public class Butherworth3Filter : Filter
    {
        // Sample rate use 200, 1000 (toDo 128, 250, 256)

        // /www/usr/fisher/helpers/mkfilter -Bu -Bs -o 2 -a 2.4500000000e-01 2.5500000000e-01
        /* 50Hz @ 200 sps
         * x[n] = x0 / gain
         * 
         y[n] = (  1 * x[n- 4])
             + (  0 * x[n- 3])
             + (  2 * x[n- 2])
             + (  0 * x[n- 1])
             + (  1 * x[n- 0])

             + ( -0.9149758348 * y[n- 4])
             + (  0.0000000000 * y[n- 3])
             + ( -1.9111970674 * y[n- 2])
             + (  0.0000000000 * y[n- 1])
        */
        private const Int32 _cHistorySize = 16;
        private const Int32 _cHistoryMask = 0x0F;
        private const Int32 _cParamSize = 16;

        private Int32[] _mHistoryX = null;
        private double[] _mHistoryY = null;
        private int _mHistoryIndex = 0; // use double for sub unit precision to get smooth discharge curve 

        private double[] _mParamX = null;
        private double[] _mParamY = null;


        private Int32 _mLastParamNr = 0; // depends on filter and size
        private double _mFirstXFactor = 1.0; // calculate the first value from input
        private bool _mbFirst;

        private DFilterType _mFilterType;
        private float _mInitialFreqHz;
        private float _mUseXGainValue;
        private float _mXGainFactor;        // = 1 / _mUseXGainValue
        private float _mUsedFreqHz;
        private UInt32 _mInitialSampleFreqSps;
        private UInt32 _mSampleFreqSps;

        private string _mNameShort = "BBF";
        private string _mNameLong = "BBF 0";


        public Butherworth3Filter(DFilterType AFilterType, float AFreqHz, UInt32 ASampleFreqSps)
        {
            mInit(AFilterType, AFreqHz, ASampleFreqSps);
        }

        private void mSetupFirst( string AName, float AFreqHz, UInt32 ASampleFreqSps, double AFirstXFactor)
        {
            if (_mHistoryX == null) _mHistoryX = new Int32[_cHistorySize];
            if (_mHistoryY == null) _mHistoryY = new double[_cHistorySize];
            if (_mParamX == null) _mParamX = new double[_cParamSize];
            if (_mParamY == null) _mParamY = new double[_cParamSize];

            _mHistoryIndex = 0;

            _mLastParamNr = 0; // depends on filter and size
            _mFirstXFactor = AFirstXFactor;
            _mbFirst = true;

            _mUsedFreqHz = AFreqHz;
            _mSampleFreqSps = ASampleFreqSps;

            _mNameShort = AName;
            _mNameLong = AName + AFreqHz.ToString("0.00" );

            for (int i = 0; i < _cHistorySize; ++i) _mHistoryX[i] = 0;
            for (int i = 0; i < _cHistorySize; ++i) _mHistoryY[i] = 0;
            for (int i = 0; i < _cParamSize; ++i) _mParamX[i] = 0;
            for (int i = 0; i < _cParamSize; ++i) _mParamY[i] = 0;

        }
        private void mSetupParamsX6(double AParam0, double AParam1, double AParam2, double AParam3, double AParam4, double AParam5, double AParam6)
        {
            _mLastParamNr = 6;
            _mParamX[0] = AParam0;
            _mParamX[1] = AParam1;
            _mParamX[2] = AParam2;
            _mParamX[3] = AParam3;
            _mParamX[4] = AParam4;
            _mParamX[5] = AParam5;
            _mParamX[6] = AParam6;
        }
        private void mSetupParamsY6(double AParam1, double AParam2, double AParam3, double AParam4, double AParam5, double AParam6)
        {
            _mLastParamNr = 6;
            _mParamY[0] = 0;
            _mParamY[1] = AParam1;
            _mParamY[2] = AParam2;
            _mParamY[3] = AParam3;
            _mParamY[4] = AParam4;
            _mParamY[5] = AParam5;
            _mParamY[6] = AParam6;
        }
        private void mSetupParamsX5(double AParam0, double AParam1, double AParam2, double AParam3, double AParam4, double AParam5)
        {
            _mLastParamNr = 6;
            _mParamX[0] = AParam0;
            _mParamX[1] = AParam1;
            _mParamX[2] = AParam2;
            _mParamX[3] = AParam3;
            _mParamX[4] = AParam4;
            _mParamX[5] = AParam5;
        }
        private void mSetupParamsY5(double AParam1, double AParam2, double AParam3, double AParam4, double AParam5)
        {
            _mLastParamNr = 6;
            _mParamY[0] = 0;
            _mParamY[1] = AParam1;
            _mParamY[2] = AParam2;
            _mParamY[3] = AParam3;
            _mParamY[4] = AParam4;
            _mParamY[5] = AParam5;
        }

        private void mSetupParamsX3(double AParam0, double AParam1, double AParam2, double AParam3)
        {
            _mLastParamNr = 3;
            _mParamX[0] = AParam0;
            _mParamX[1] = AParam1;
            _mParamX[2] = AParam2;
            _mParamX[3] = AParam3;
        }
        private void mSetupParamsY3(double AParam1, double AParam2, double AParam3)
        {
            _mLastParamNr = 3;
            _mParamY[0] = 0;
            _mParamY[1] = AParam1;
            _mParamY[2] = AParam2;
            _mParamY[3] = AParam3;
        }


        public void mInit( DFilterType AFilterType, float AFreqHz, UInt32 ASampleFreqSps)
        {
            switch(AFilterType)
            {
                case DFilterType.LowPass:
                    mInitLowPass(AFreqHz, ASampleFreqSps);
                    break;
                case DFilterType.HighPass:
                    mInitHighPass(AFreqHz, ASampleFreqSps);
                    break;
                case DFilterType.BandBlock:
                    mInitBandBlock(AFreqHz, ASampleFreqSps);
                    break;
            }
        }
        public void mInitBandBlock(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes Sample rate and filter frequency
            Int32 dFreqSps = 99999;
            Int32 useSps = 1000;
            int dSps;

            _mFilterType = DFilterType.BandBlock;
            _mInitialFreqHz = AFreqHz;
            _mInitialSampleFreqSps = ASampleFreqSps;


            dSps = (Int32)ASampleFreqSps - 1000; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 1000; }
            dSps = (Int32)ASampleFreqSps - 200; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 200; }

            switch (useSps)
            {
                case 200:
                    mInitBandBlock200Sps(AFreqHz, ASampleFreqSps);
                    break;
                case 1000:
                    mInitBandBlock1000Sps(AFreqHz, ASampleFreqSps);
                    break;
            }
        }

        public void mInitLowPass(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes Sample rate and filter frequency
            Int32 dFreqSps = 99999;
            Int32 useSps = 1000;
            int dSps;

            _mFilterType = DFilterType.LowPass;
            _mInitialFreqHz = AFreqHz;
            _mInitialSampleFreqSps = ASampleFreqSps;


            dSps = (Int32)ASampleFreqSps - 1000; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 1000; }
            dSps = (Int32)ASampleFreqSps - 200; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 200; }

            switch (useSps)
            {
                case 200:
                    mInitLowPass200Sps(AFreqHz, ASampleFreqSps);
                    break;
                case 1000:
                    mInitLowPass1000Sps(AFreqHz, ASampleFreqSps);
                    break;
            }
        }
        public void mInitHighPass(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes Sample rate and filter frequency
            Int32 dFreqSps = 99999;
            Int32 useSps = 1000;
            int dSps;

            _mFilterType = DFilterType.HighPass;
            _mInitialFreqHz = AFreqHz;
            _mInitialSampleFreqSps = ASampleFreqSps;


            dSps = (Int32)ASampleFreqSps - 1000; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 1000; }
            dSps = (Int32)ASampleFreqSps - 200; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 200; }

            switch (useSps)
            {
                case 200:
                    mInitHighPass200Sps(AFreqHz, ASampleFreqSps);
                    break;
                case 1000:
                    mInitHighPass1000Sps(AFreqHz, ASampleFreqSps);
                    break;
            }
        }


        public void mInitBandBlock200Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes filter frequency
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 50;

            dHz = (Int32)AFreqHz - 50; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 50; }
            dHz = (Int32)AFreqHz - 60; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 60; }

            switch (useHz)
            {
                case 50:
                    /* butterworth Band Stop 3 order 200Sps 49-51Hz
                     * y[n] = (  1 * x[n- 6])
     + (  0 * x[n- 5])
     + (  3 * x[n- 4])
     + (  0 * x[n- 3])
     + (  3 * x[n- 2])
     + (  0 * x[n- 1])
     + (  1 * x[n- 0])

     + ( -0.8818931306 * y[n- 6])
     + ( -0.0000000000 * y[n- 5])
     + ( -2.7564831952 * y[n- 4])
     + ( -0.0000000000 * y[n- 3])
     + ( -2.8743568927 * y[n- 2])
     + ( -0.0000000000 * y[n- 1])
                    */
                    mSetupFirst("Band Block 3B Filter", 50, 200, 1.0);
                    mSetupParamsX6(1, 0, 3, 0, 3, 0, 1);
                    mSetupParamsY6(-0, -2.8743568927, 0, -2.7564831952, 0, -0.8818931306);

                    break;
                case 60:
                    /* butterworth Band Stop 3 order 200Sps 59-61Hz
y[n] = (  1 * x[n- 6])
     + (  1.8550173053 * x[n- 5])
     + (  4.1470297343 * x[n- 4])
     + (  3.9464523892 * x[n- 3])
     + (  4.1470297343 * x[n- 2])
     + (  1.8550173053 * x[n- 1])
     + (  1 * x[n- 0])

     + ( -0.8818931306 * y[n- 6])
     + ( -1.6701841808 * y[n- 5])
     + ( -3.8110817058 * y[n- 4])
     + ( -3.7037865645 * y[n- 3])
     + ( -3.9740904790 * y[n- 2])
     + ( -1.8161722822 * y[n- 1])
                     */
                    mSetupFirst("Band Block 3B Filter", 60, 200, 1.0);
                    mSetupParamsX6(1, 1.8550173053, 4.1470297343, 3.9464523892, 4.1470297343, 1.8550173053, 1);
                    mSetupParamsY6(-1.8161722822, -3.9740904790, -3.7037865645, -3.8110817058, -1.6701841808, -0.8818931306);

                    break;
            }
        }
        public void mInitBandBlock1000Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes filter frequency
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 50;

            dHz = (Int32)AFreqHz - 50; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 50; }
            dHz = (Int32)AFreqHz - 60; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 60; }

            switch (useHz)
            {
                case 50:
                    /* butterworth Band Stop 3 order 1000Sps 49-51Hz
   y[n] = (  1 * x[n- 6])
        + ( -5.7064517382 * x[n- 5])
        + ( 13.8545304803 * x[n- 4])
        + (-18.2952206239 * x[n- 3])
        + ( 13.8545304803 * x[n- 2])
        + ( -5.7064517382 * x[n- 1])
        + (  1 * x[n- 0])

        + ( -0.9751802955 * y[n- 6])
        + (  5.5881289996 * y[n- 5])
        + (-13.6241305276 * y[n- 4])
        + ( 18.0664564757 * y[n- 3])
        + (-13.7387485794 * y[n- 2])
        + (  5.6825487664 * y[n- 1])


                   */
                    mSetupFirst("Band Block 3B Filter", 50, 1000, 1.0);
                    mSetupParamsX6(1, -5.7064517382, 13.8545304803, -18.2952206239, 13.8545304803, -5.7064517382, 1);
                    mSetupParamsY6(5.6825487664, -13.7387485794, 18.0664564757, -13.6241305276, 5.5881289996, -0.9751802955);
                    break;
                case 60:
                    /* butterworth Band Stop 3 order 1000Sps 59-61Hz
 y[n] = (  1 * x[n- 6])
      + ( -5.5787690355 * x[n- 5])
      + ( 13.3742213170 * x[n- 4])
      + (-17.5881363654 * x[n- 3])
      + ( 13.3742213170 * x[n- 2])
      + ( -5.5787690355 * x[n- 1])
      + (  1 * x[n- 0])

      + ( -0.9751802955 * y[n- 6])
      + (  5.4630937857 * y[n- 5])
      + (-13.1518061972 * y[n- 4])
      + ( 17.3682087989 * y[n- 3])
      + (-13.2624506169 * y[n- 2])
      + (  5.5554008961 * y[n- 1])

                     */
                    mSetupFirst("Band Block 3B Filter", 60, 1000, 1.0);
                    mSetupParamsX6(1, -5.5787690355, 13.3742213170, -17.5881363654, 13.3742213170, -5.5787690355, 1);
                    mSetupParamsY6(5.5554008961, -13.2624506169, 17.3682087989, -13.1518061972, 5.4630937857, -0.9751802955);

                    break;
            }
        }
        public void mInitLowPass200Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes filter frequency
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 150;

            dHz = (Int32)AFreqHz - 40; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 40; }
            dHz = (Int32)AFreqHz - 100; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 100; }
//            dHz = (Int32)AFreqHz - 150; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 150; }

            switch (useHz)
            {
                case 40:
                    /* butterworth Low Pass 3 order 200Sps 40Hz
y[n] = (  1 * x[n- 3])
     + (  3 * x[n- 2])
     + (  3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.0562972365 * y[n- 3])
     + ( -0.4217870487 * y[n- 2])
     + (  0.5772405248 * y[n- 1])                    */
                    mSetupFirst("Low Pass 3B Filter", 40, 200, 1.0);
                    mSetupParamsX3( 1, 3, 3, 1);
                    mSetupParamsY3(0.5772405248, -0.4217870487, 0.0562972365);

                    break;
                case 100:
                    /* butterworth Low Pass 3 order 200Sps 100Hz nyquest freq 100
                     y[n] = (  1 * x[n- 3])     99.9Hz
     + (  3 * x[n- 2])
     + (  3 * x[n- 1])
     + (  1 * x[n- 0])

     + ( -0.9937365101 * y[n- 3])
     + ( -2.9874533582 * y[n- 2])
     + ( -2.9937168173 * y[n- 1])
*/
                    mSetupFirst("Low Pass 3B Filter", 100, 200, 1.0);
                    mSetupParamsX3( 1, 3, 3, 1);
                    mSetupParamsY3(-2.9937168173, -2.9874533582, -0.9937365101);

                    break;
                case 150:
                    /* butterworth Low Pass 3 order 200Sps 150Hz -> not available because > nyquest freq 100
                     */
                    //mSetupFirst("Low Pass 3B Filter", 150, 200, 1.0);
                    //mSetupParamsX3( , , , 1);
                    //mSetupParamsY3( , , );

                    break;
            }
        }
        public void mInitLowPass1000Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 150;

            dHz = (Int32)AFreqHz - 40; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 40; }
            dHz = (Int32)AFreqHz - 100; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 100; }
            dHz = (Int32)AFreqHz - 150; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 150; }

            switch (useHz)
            {
                case 40:
                    /* butterworth Low Pass 3 order 200Sps 40Hz
                    y[n] = (  1 * x[n- 3])
     + (  3 * x[n- 2])
     + (  3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.6041096995 * y[n- 3])
     + ( -2.1152541270 * y[n- 2])
     + (  2.4986083447 * y[n- 1])
     */
                    mSetupFirst("Low Pass 3B Filter", 40, 1000, 0.0);
                    mSetupParamsX3( 1, 3, 3, 1);
                    mSetupParamsY3(2.4986083447, -2.1152541270, 0.6041096995);

                    break;
                case 100:
                    /* butterworth Low Pass 3 order 200Sps 100Hz
                     y[n] = (  1 * x[n- 3])
     + (  3 * x[n- 2])
     + (  3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.2780599176 * y[n- 3])
     + ( -1.1828932620 * y[n- 2])
     + (  1.7600418803 * y[n- 1])
     */
                    mSetupFirst("Low Pass 3B Filter", 100, 1000, 1.0);
                    mSetupParamsX3( 1, 3, 3, 1);
                    mSetupParamsY3(1.7600418803, -1.1828932620, 0.2780599176);

                    break;
                case 150:
                    /* butterworth Low Pass 3 order 200Sps 150kHz
y[n] = (  1 * x[n- 3])
     + (  3 * x[n- 2])
     + (  3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.1377613013 * y[n- 3])
     + ( -0.6959427558 * y[n- 2])
     + (  1.1619174837 * y[n- 1])
     */
                    mSetupFirst("Low Pass 3B Filter", 150, 200, 1.0);
                    mSetupParamsX3( 1, 3, 3, 1);
                    mSetupParamsY3(1.1619174837, -0.6959427558, 0.1377613013);

                    break;
            }
        }
        public void mInitHighPass200Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes filter frequency
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 5;

            dHz = (Int32)AFreqHz - 0.05F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 5; }
            dHz = (Int32)AFreqHz - 0.5F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 500; }
            dHz = (Int32)AFreqHz - 3.5F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 3500; }

            switch (useHz)
            {
                case 5:
                    /* butterworth High Pass 3 order 200Sps 0.05Hz
y[n] = ( -1 * x[n- 3])
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9968633367 * y[n- 3])
     + ( -2.9937217482 * y[n- 2])
     + (  2.9968584077 * y[n- 1])
     */
                    mSetupFirst("High Pass 3B Filter", 0.05F, 200, 0.0);
                    mSetupParamsX3( 1, -3, 3, -1);
                    mSetupParamsY3(2.9968584077, -2.9937217482, 0.9968633367);

                    break;
                case 500:
                    /* butterworth Low Pass 3 order 200Sps 0.5Hz
y[n] = ( -1 * x[n- 3])
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9690721133 * y[n- 3])
     + ( -2.9376603253 * y[n- 2])
     + (  2.9685843964 * y[n- 1])                     */
                    mSetupFirst("High Pass 3B Filter", 0.5F, 200, 1.0);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9685843964, -2.9376603253, 0.9690721133);

                    break;
                case 3500:
                    /* butterworth Low Pass 3 order 200Sps 3.5Hz
y[n] = ( -1 * x[n- 3])
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9569700502 * y[n- 3])
     + ( -2.9129990440 * y[n- 2])
     + (  2.9560185887 * y[n- 1])                     */
                    mSetupFirst("High Pass 3B Filter", 3.5F, 200, 1.0);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9560185887, -2.9129990440, 0.9569700502);

                    break;
            }
        }
        public void mInitHighPass1000Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes filter frequency
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 5;

            dHz = (Int32)AFreqHz - 0.05F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 5; }
            dHz = (Int32)AFreqHz - 0.5F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 500; }
            dHz = (Int32)AFreqHz - 3.5F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 3500; }

            switch (useHz)
            {
                case 5:
                    /* butterworth High Pass 3 order 200Sps 0.05Hz
y[n] = ( -1 * x[n- 3])
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9993718788 * y[n- 3])
     + ( -2.9987435603 * y[n- 2])
     + (  2.9993716815 * y[n- 1])
     */
                    mSetupFirst("High Pass 3B Filter", 0.05F, 1000, 0.0);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9993716815, -2.9987435603, 0.9993718788);

                    break;
                case 500:
                    /* butterworth Low Pass 3 order 200Sps 0.5Hz
y[n] = ( -1 * x[n- 3])
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9993718788 * y[n- 3])
     + ( -2.9987435603 * y[n- 2])
     + (  2.9993716815 * y[n- 1])
     */
                    mSetupFirst("High Pass 3B Filter", 0.5F, 1000, 1.0);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9993716815, -2.9987435603, 0.9993718788);

                    break;
                case 3500:
                    /* butterworth Low Pass 3 order 200Sps 3.5Hz
y[n] = ( -1 * x[n- 3])
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9569700502 * y[n- 3])
     + ( -2.9129990440 * y[n- 2])
     + (  2.9560185887 * y[n- 1])                     */
                    mSetupFirst("High Pass 3B Filter", 3.5F, 1000, 1.0);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9560185887, -2.9129990440, 0.9569700502);

                    break;
            }
        }

        protected override int Process(int x0)
        {
            return mDoFilter(x0);
        }

        public int mDoFilter(int x0)       // directly callable
        {
            double y1;

            if (_mbFirst)
            {
                _mHistoryIndex = 0;

                y1 = _mFirstXFactor * x0;
                for (int i = 0; i < _cHistorySize; ++i) _mHistoryX[i] = x0;
                for (int i = 0; i < _cHistorySize; ++i) _mHistoryY[i] = y1;
                _mbFirst = false;
            }
            else
            {
                // move to next history
                ++_mHistoryIndex;
                _mHistoryIndex &= _cHistoryMask;    // round robin

                _mHistoryX[_mHistoryIndex] = x0;

                y1 = _mParamX[0] * x0;

                int iHistory = _mHistoryIndex;

                for( int i = 1; i <= _mLastParamNr; ++i)
                {
                    --iHistory;
                    iHistory &= _cHistoryMask;    // round robin

                    double dX = _mParamX[i] * _mHistoryX[iHistory];
                    double dY = _mParamY[i] * _mHistoryY[iHistory];
                    y1 += dX + dY;
                }
                _mHistoryY[_mHistoryIndex] = y1;
            }

            return (int)y1;
        }

        protected override void mDoReset()
        {
            _mbFirst = true;

        }

        public override string Name => _mNameLong;

        public override string ToString()
        {
            return _mNameShort;
        }
    }

}
