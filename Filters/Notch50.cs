﻿using System;
using DynaVisionGraph.Filter;

namespace DynaVisionGraph.Filter
{

    /*
     *  50Hz Powerline Interference filter. non-adaptive. 200SPS samplerate.
     */
    public class Notch50 : Filter
    {
        private /*readonly*/ FilterHistory<int> x = new FilterHistory<int>(4);

        protected override int Process(int x0)
        {
//            var filtered = (x0 + x2) / 2;                                 // 1st order
            var y0 = (x0 + 2 * x[2] + x[4]) / 4;                        // 2nd order
//            var filtered = (x0 + 3 * x2 + 3 * x4 + x6) / 8;               // 3th order
//            var filtered = (x0 + 4 * x2 + 6 * x4 + 4 * x6 + x8) / 16;     // 4th order

            x.AddSample(x0);

            return y0;
        }
        protected override void mDoReset()
        {
            x = new FilterHistory<int>(4);   // clear by creating new array
        }

        public override string Name => "50Hz Notch FIR";
    }

    public enum DFilterType
    {
        LowPass,        // use 40, 100, 150 Hz
        HighPass,       // use 0.05, 0.5, 3.5 Hz
        BandBlock       // use 50, 100 Hz
    }

    public class CButherworth3Filter : Filter
    {
        // Sample rate use 200, 1000 (toDo 128, 250, 256)
        // DVX 200 & 1000, Sirona 256, DV2=200, TZ=250
        // https://www-users.cs.york.ac.uk/~fisher/mkfilter/trad.html
        // /www/usr/fisher/helpers/mkfilter -Bu -Bs -o 2 -a 2.4500000000e-01 2.5500000000e-01
        /* 50Hz @ 200 sps
         * x[n] = x0 / gain
         * 
         y[n] = (  1 * x[n- 4]) gain=
             + (  0 * x[n- 3])
             + (  2 * x[n- 2])
             + (  0 * x[n- 1])
             + (  1 * x[n- 0])

             + ( -0.9149758348 * y[n- 4])
             + (  0.0000000000 * y[n- 3])
             + ( -1.9111970674 * y[n- 2])
             + (  0.0000000000 * y[n- 1])
        */
        private const Int32 _cHistorySize = 16;
        private const Int32 _cHistoryMask = 0x0F;
        private const Int32 _cParamSize = 16;

        private double[] _mHistoryX = null;
        private double[] _mHistoryY = null;
        private int _mHistoryIndex = 0; // use double for sub unit precision to get smooth discharge curve 

        private double[] _mParamX = null;
        private double[] _mParamY = null;


        private Int32 _mLastParamNr = 0; // depends on filter and size
        private double _mFirstXFactor = 1.0; // calculate the first value from input
        private bool _mbFirst;

        private DFilterType _mFilterType;
        private float _mInitialFreqHz;
        private double _mUseXGainValue;     // in example x[n] = x0 / _mUseXGainValue
        private double _mXGainFactor;        // = 1 / _mUseXGainValue
        private float _mUsedFreqHz;
        private UInt32 _mInitialSampleFreqSps;
        private UInt32 _mSampleFreqSps;

        private string _mNameShort = "BBF";
        private string _mNameLong = "BBF 0";


        public CButherworth3Filter(DFilterType AFilterType, float AFreqHz, UInt32 ASampleFreqSps)
        {
            mInit(AFilterType, AFreqHz, ASampleFreqSps);
        }

        private void mSetupFirst( string AName, float AFreqHz, UInt32 ASampleFreqSps, double AFirstXFactor, double AXGainFactor)
        {
            if (_mHistoryX == null) _mHistoryX = new double[_cHistorySize];
            if (_mHistoryY == null) _mHistoryY = new double[_cHistorySize];
            if (_mParamX == null) _mParamX = new double[_cParamSize];
            if (_mParamY == null) _mParamY = new double[_cParamSize];

            _mHistoryIndex = 0;

            _mLastParamNr = 0; // depends on filter and size
            _mFirstXFactor = AFirstXFactor;
            _mUseXGainValue = AXGainFactor;
            _mXGainFactor = AXGainFactor > 0 ? 1 / AXGainFactor : 1.0;
            _mbFirst = true;

            _mUsedFreqHz = AFreqHz;
            _mSampleFreqSps = ASampleFreqSps;

            _mNameShort = AName;
            _mNameLong = AName + AFreqHz.ToString("0.00" );

            for (int i = 0; i < _cHistorySize; ++i) _mHistoryX[i] = 0;
            for (int i = 0; i < _cHistorySize; ++i) _mHistoryY[i] = 0;
            for (int i = 0; i < _cParamSize; ++i) _mParamX[i] = 0;
            for (int i = 0; i < _cParamSize; ++i) _mParamY[i] = 0;

        }
        private void mSetupParamsX6(double AParam0, double AParam1, double AParam2, double AParam3, double AParam4, double AParam5, double AParam6)
        {
            _mLastParamNr = 6;
            _mParamX[0] = AParam0;
            _mParamX[1] = AParam1;
            _mParamX[2] = AParam2;
            _mParamX[3] = AParam3;
            _mParamX[4] = AParam4;
            _mParamX[5] = AParam5;
            _mParamX[6] = AParam6;
        }
        private void mSetupParamsY6(double AParam1, double AParam2, double AParam3, double AParam4, double AParam5, double AParam6)
        {
            _mLastParamNr = 6;
            _mParamY[0] = 0;
            _mParamY[1] = AParam1;
            _mParamY[2] = AParam2;
            _mParamY[3] = AParam3;
            _mParamY[4] = AParam4;
            _mParamY[5] = AParam5;
            _mParamY[6] = AParam6;
        }
        private void mSetupParamsX5(double AParam0, double AParam1, double AParam2, double AParam3, double AParam4, double AParam5)
        {
            _mLastParamNr = 6;
            _mParamX[0] = AParam0;
            _mParamX[1] = AParam1;
            _mParamX[2] = AParam2;
            _mParamX[3] = AParam3;
            _mParamX[4] = AParam4;
            _mParamX[5] = AParam5;
        }
        private void mSetupParamsY5(double AParam1, double AParam2, double AParam3, double AParam4, double AParam5)
        {
            _mLastParamNr = 6;
            _mParamY[0] = 0;
            _mParamY[1] = AParam1;
            _mParamY[2] = AParam2;
            _mParamY[3] = AParam3;
            _mParamY[4] = AParam4;
            _mParamY[5] = AParam5;
        }

        private void mSetupParamsX3(double AParam0, double AParam1, double AParam2, double AParam3)
        {
            _mLastParamNr = 3;
            _mParamX[0] = AParam0;
            _mParamX[1] = AParam1;
            _mParamX[2] = AParam2;
            _mParamX[3] = AParam3;
        }
        private void mSetupParamsY3(double AParam1, double AParam2, double AParam3)
        {
            _mLastParamNr = 3;
            _mParamY[0] = 0;
            _mParamY[1] = AParam1;
            _mParamY[2] = AParam2;
            _mParamY[3] = AParam3;
        }


        public void mInit( DFilterType AFilterType, float AFreqHz, UInt32 ASampleFreqSps)
        {
            switch(AFilterType)
            {
                case DFilterType.LowPass:
                    mInitLowPass(AFreqHz, ASampleFreqSps);
                    break;
                case DFilterType.HighPass:
                    mInitHighPass(AFreqHz, ASampleFreqSps);
                    break;
                case DFilterType.BandBlock:
                    mInitBandBlock(AFreqHz, ASampleFreqSps);
                    break;
            }
        }
        public void mInitBandBlock(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes Sample rate and filter frequency
            Int32 dFreqSps = 99999;
            Int32 useSps = 1000;
            int dSps;

            _mFilterType = DFilterType.BandBlock;
            _mInitialFreqHz = AFreqHz;
            _mInitialSampleFreqSps = ASampleFreqSps;


            dSps = (Int32)ASampleFreqSps - 1000; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 1000; }
            dSps = (Int32)ASampleFreqSps - 256; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 256; }
            dSps = (Int32)ASampleFreqSps - 250; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 250; }
            dSps = (Int32)ASampleFreqSps - 200; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 200; }

            switch (useSps)
            {
                case 200:
                    mInitBandBlock200Sps(AFreqHz, ASampleFreqSps);
                    break;
                case 250:
                    mInitBandBlock250Sps(AFreqHz, ASampleFreqSps);
                    break;
                case 256:
                    mInitBandBlock256Sps(AFreqHz, ASampleFreqSps);
                    break;
                case 1000:
                    mInitBandBlock1000Sps(AFreqHz, ASampleFreqSps);
                    break;
            }
        }

        public void mInitLowPass(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes Sample rate and filter frequency
            Int32 dFreqSps = 99999;
            Int32 useSps = 1000;
            int dSps;

            _mFilterType = DFilterType.LowPass;
            _mInitialFreqHz = AFreqHz;
            _mInitialSampleFreqSps = ASampleFreqSps;


            dSps = (Int32)ASampleFreqSps - 1000; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 1000; }
            dSps = (Int32)ASampleFreqSps - 256; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 256; }
            dSps = (Int32)ASampleFreqSps - 250; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 250; }
            dSps = (Int32)ASampleFreqSps - 200; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 200; }

            switch (useSps)
            {
                case 200:
                    mInitLowPass200Sps(AFreqHz, ASampleFreqSps);
                    break;
                case 250:
                    mInitLowPass250Sps(AFreqHz, ASampleFreqSps);
                    break;
                case 256:
                    mInitLowPass256Sps(AFreqHz, ASampleFreqSps);
                    break;
                case 1000:
                    mInitLowPass1000Sps(AFreqHz, ASampleFreqSps);
                    break;
            }
        }
        public void mInitHighPass(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes Sample rate and filter frequency
            Int32 dFreqSps = 99999;
            Int32 useSps = 1000;
            int dSps;

            _mFilterType = DFilterType.HighPass;
            _mInitialFreqHz = AFreqHz;
            _mInitialSampleFreqSps = ASampleFreqSps;


            dSps = (Int32)ASampleFreqSps - 1000; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 1000; }
            dSps = (Int32)ASampleFreqSps - 256; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 256; }
            dSps = (Int32)ASampleFreqSps - 250; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 250; }
            dSps = (Int32)ASampleFreqSps - 200; if (dSps < 0) dSps = -dSps; if (dSps < dFreqSps) { dFreqSps = dSps; useSps = 200; }

            switch (useSps)
            {
                case 200:
                    mInitHighPass200Sps(AFreqHz, ASampleFreqSps);
                    break;
                case 250:
                    mInitHighPass250Sps(AFreqHz, ASampleFreqSps);
                    break;
                case 256:
                    mInitHighPass256Sps(AFreqHz, ASampleFreqSps);
                    break;
                case 1000:
                    mInitHighPass1000Sps(AFreqHz, ASampleFreqSps);
                    break;
            }
        }


        public void mInitBandBlock200Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes filter frequency
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 50;
            UInt32 sps = 200;
            double x0factor = 1.0;

            dHz = AFreqHz - 50; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 50; }
            dHz = AFreqHz - 60; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 60; }

            switch (useHz)
            {
                case 50:
                    /* butterworth Band Stop 3 order 49-51Hz
                     * y[n] = (  1 * x[n- 6])   GAIN   1.064858789e+00
     + (  0 * x[n- 5])
     + (  3 * x[n- 4])
     + (  0 * x[n- 3])
     + (  3 * x[n- 2])
     + (  0 * x[n- 1])
     + (  1 * x[n- 0])

     + ( -0.8818931306 * y[n- 6])
     + ( -0.0000000000 * y[n- 5])
     + ( -2.7564831952 * y[n- 4])
     + ( -0.0000000000 * y[n- 3])
     + ( -2.8743568927 * y[n- 2])
     + ( -0.0000000000 * y[n- 1])
                    */
                    mSetupFirst("Band Block 3B Filter", 50, sps, x0factor, 1.06485878);
                    mSetupParamsX6(1, 0, 3, 0, 3, 0, 1);
                    mSetupParamsY6(-0, -2.8743568927, 0, -2.7564831952, 0, -0.8818931306);

                    break;
                case 60:
                    /* butterworth Band Stop 3 order 59-61Hz
y[n] = (  1 * x[n- 6])  GAIN   1.064858789e+00
     + (  1.8550173053 * x[n- 5])
     + (  4.1470297343 * x[n- 4])
     + (  3.9464523892 * x[n- 3])
     + (  4.1470297343 * x[n- 2])
     + (  1.8550173053 * x[n- 1])
     + (  1 * x[n- 0])

     + ( -0.8818931306 * y[n- 6])
     + ( -1.6701841808 * y[n- 5])
     + ( -3.8110817058 * y[n- 4])
     + ( -3.7037865645 * y[n- 3])
     + ( -3.9740904790 * y[n- 2])
     + ( -1.8161722822 * y[n- 1])
                     */
                    mSetupFirst("Band Block 3B Filter", 60, sps, x0factor, 1.064858789e+00);
                    mSetupParamsX6(1, 1.8550173053, 4.1470297343, 3.9464523892, 4.1470297343, 1.8550173053, 1);
                    mSetupParamsY6(-1.8161722822, -3.9740904790, -3.7037865645, -3.8110817058, -1.6701841808, -0.8818931306);

                    break;
            }
        }
        public void mInitBandBlock250Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes filter frequency
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 50;
            UInt32 sps = 250;
            double x0factor = 1.0;

            dHz = AFreqHz - 50; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 50; }
            dHz = AFreqHz - 60; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 60; }

            switch (useHz)
            {
                case 50:
                    /* butterworth Band Stop 3 order 49-51Hz
y[n] = (  1 * x[n- 6])GAIN   1.051555795e+00
     + ( -1.8546876965 * x[n- 5])
     + (  4.1466221505 * x[n- 4])
     + ( -3.9456671702 * x[n- 3])
     + (  4.1466221505 * x[n- 2])
     + ( -1.8546876965 * x[n- 1])
     + (  1 * x[n- 0])

     + ( -0.9043475314 * y[n- 6])
     + (  1.7053824225 * y[n- 5])
     + ( -3.8760796425 * y[n- 4])
     + (  3.7507324860 * y[n- 3])
     + ( -4.0081587919 * y[n- 2])
     + (  1.8236153700 * y[n- 1])                     * */
                    mSetupFirst("Band Block 3B Filter", 50, sps, x0factor, 1.051555795e+00);
                    mSetupParamsX6(1, -1.8546876965, 4.1466221505, -3.9456671702, 4.1466221505, -1.8546876965, 1);
                    mSetupParamsY6(1.8236153700, -4.0081587919, 3.7507324860, -3.8760796425, 1.7053824225, -0.9043475314);

                    break;
                case 60:
                    /* butterworth Band Stop 3 order 59-61Hz
                     y[n] = (  1 * x[n- 6])
     + ( -0.3768621343 * x[n- 5]) 1.051555795e+00
     + (  3.0473416894 * x[n- 4])
     + ( -0.7557066341 * x[n- 3])
     + (  3.0473416894 * x[n- 2])
     + ( -0.3768621343 * x[n- 1])
     + (  1 * x[n- 0])

     + ( -0.9043475314 * y[n- 6])
     + (  0.3465241403 * y[n- 5])
     + ( -2.8482141066 * y[n- 4])
     + (  0.7183538031 * y[n- 3])
     + ( -2.9452546969 * y[n- 2])
     + (  0.3705484119 * y[n- 1])
*/
                    mSetupFirst("Band Block 3B Filter", 60, sps, x0factor, 1.051555795e+00);
                    mSetupParamsX6(1, -0.3768621343, 3.0473416894, -0.7557066341, 3.0473416894, -0.3768621343, 1);
                    mSetupParamsY6(0.3705484119, -2.9452546969, 0.7183538031, -2.8482141066, 0.3465241403, -0.9043475314);

                    break;
            }
        }
        public void mInitBandBlock256Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes filter frequency
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 50;
            UInt32 sps = 256;
            double x0factor = 1.0;

            dHz = AFreqHz - 50; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 50; }
            dHz = AFreqHz - 60; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 60; }

            switch (useHz)
            {
                case 50:
                    /* butterworth Band Stop 3 order 49-51Hz
 y[n] = (  1 * x[n- 6]) GAIN   1.050317308e+00
     + ( -2.0219480933 * x[n- 5])
     + (  4.3627580307 * x[n- 4])
     + ( -4.3500546313 * x[n- 3])
     + (  4.3627580307 * x[n- 2])
     + ( -2.0219480933 * x[n- 1])
     + (  1 * x[n- 0])

     + ( -0.9064815219 * y[n- 6])
     + (  1.8628456596 * y[n- 5])
     + ( -4.0846678542 * y[n- 4])
     + (  4.1401107430 * y[n- 3])
     + ( -4.2205418738 * y[n- 2])
     + (  1.9888673594 * y[n- 1])                    * */
                    mSetupFirst("Band Block 3B Filter", 50, sps, x0factor, 1.050317308e+00);
                    mSetupParamsX6(1, -2.0219480933, 4.3627580307, -4.3500546313, 4.3627580307, -2.0219480933, 1);
                    mSetupParamsY6(1.9888673594, -4.2205418738, 4.1401107430, -4.0846678542, 1.8628456596, -0.9064815219);

                    break;
                case 60:
                    /* butterworth Band Stop 3 order 59-61Hz
                     y[n] = (  1 * x[n- 6])1.050317308e+00
     + ( -0.5882800209 * x[n- 5])
     + (  3.1153577943 * x[n- 4])
     + ( -1.1841003402 * x[n- 3])
     + (  3.1153577943 * x[n- 2])
     + ( -0.5882800209 * x[n- 1])
     + (  1 * x[n- 0])

     + ( -0.9064815219 * y[n- 6])
     + (  0.5419896224 * y[n- 5])
     + ( -2.9164631704 * y[n- 4])
     + (  1.1269238741 * y[n- 3])
     + ( -3.0134639128 * y[n- 2])
     + (  0.5786552759 * y[n- 1])*/
                    mSetupFirst("Band Block 3B Filter", 60, sps, x0factor, 1.050317308e+00);
                    mSetupParamsX6(1, -0.5882800209, 3.1153577943, -1.1841003402, 3.1153577943, -0.5882800209, 1);
                    mSetupParamsY6(0.5786552759, -3.0134639128, 1.1269238741, -2.916463170, 0.5419896224, -0.9064815219);

                    break;
            }
        }
        public void mInitBandBlock1000Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes filter frequency
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 50;
            UInt32 sps = 1000;
            double x0factor = 1.0;

            dHz = AFreqHz - 50; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 50; }
            dHz = AFreqHz - 60; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 60; }

            switch (useHz)
            {
                case 50:
                    /* butterworth Band Stop 3 order  49-51Hz 
   y[n] = (  1 * x[n- 6])   GAIN   1.012645743e+00
        + ( -5.7064517382 * x[n- 5])
        + ( 13.8545304803 * x[n- 4])
        + (-18.2952206239 * x[n- 3])
        + ( 13.8545304803 * x[n- 2])
        + ( -5.7064517382 * x[n- 1])
        + (  1 * x[n- 0])

        + ( -0.9751802955 * y[n- 6])
        + (  5.5881289996 * y[n- 5])
        + (-13.6241305276 * y[n- 4])
        + ( 18.0664564757 * y[n- 3])
        + (-13.7387485794 * y[n- 2])
        + (  5.6825487664 * y[n- 1])


                   */
                    mSetupFirst("Band Block 3B Filter", 50, sps, x0factor, 1.012645743e+00);
                    mSetupParamsX6(1, -5.7064517382, 13.8545304803, -18.2952206239, 13.8545304803, -5.7064517382, 1);
                    mSetupParamsY6(5.6825487664, -13.7387485794, 18.0664564757, -13.6241305276, 5.5881289996, -0.9751802955);
                    break;
                case 60:
                    /* butterworth Band Stop 3 order 59-61Hz
 y[n] = (  1 * x[n- 6])     GAIN   1.012645743e+00
      + ( -5.5787690355 * x[n- 5])
      + ( 13.3742213170 * x[n- 4])
      + (-17.5881363654 * x[n- 3])
      + ( 13.3742213170 * x[n- 2])
      + ( -5.5787690355 * x[n- 1])
      + (  1 * x[n- 0])

      + ( -0.9751802955 * y[n- 6])
      + (  5.4630937857 * y[n- 5])
      + (-13.1518061972 * y[n- 4])
      + ( 17.3682087989 * y[n- 3])
      + (-13.2624506169 * y[n- 2])
      + (  5.5554008961 * y[n- 1])

                     */
                    mSetupFirst("Band Block 3B Filter", 60, sps, x0factor, 1.012645743e+00);
                    mSetupParamsX6(1, -5.5787690355, 13.3742213170, -17.5881363654, 13.3742213170, -5.5787690355, 1);
                    mSetupParamsY6(5.5554008961, -13.2624506169, 17.3682087989, -13.1518061972, 5.4630937857, -0.9751802955);

                    break;
            }
        }
        public void mInitLowPass200Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes filter frequency
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 150;
            UInt32 sps = 200;
            double x0factor = 1.0;

            dHz = AFreqHz - 40; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 40; }
            dHz = AFreqHz - 100; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 100; }
            //            dHz = AFreqHz - 150; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 150; }

            switch (useHz)
            {
                case 40:
                    /* butterworth Low Pass 3 order 40Hz
y[n] = (  1 * x[n- 3])  GAIN 1.014907356e+01
     + (  3 * x[n- 2])
     + (  3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.0562972365 * y[n- 3])
     + ( -0.4217870487 * y[n- 2])
     + (  0.5772405248 * y[n- 1])                    */
                    mSetupFirst("Low Pass 3B Filter", 40, sps, x0factor, 1.014907356e+01);
                    mSetupParamsX3(1, 3, 3, 1);
                    mSetupParamsY3(0.5772405248, -0.4217870487, 0.0562972365);

                    break;
                case 100:
                    /* butterworth Low Pass 3 order 99.9Hz nyquest freq 100
                     y[n] = (  1 * x[n- 3])      gain 1.003146534e+00
     + (  3 * x[n- 2])
     + (  3 * x[n- 1])
     + (  1 * x[n- 0])

     + ( -0.9937365101 * y[n- 3])
     + ( -2.9874533582 * y[n- 2])
     + ( -2.9937168173 * y[n- 1])
*/
                    mSetupFirst("Low Pass 3B Filter", 100, sps, x0factor, 1.003146534e+00);
                    mSetupParamsX3(1, 3, 3, 1);
                    mSetupParamsY3(-2.9937168173, -2.9874533582, -0.9937365101);

                    break;
                case 150:
                    /* butterworth Low Pass 3 order 150Hz -> not available because > nyquest freq 100
                     */
                    //mSetupFirst("Low Pass 3B Filter", 150, 200, 1.0);
                    //mSetupParamsX3( , , , 1);
                    //mSetupParamsY3( , , );

                    break;
            }
        }
        public void mInitLowPass250Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes filter frequency
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 150;
            UInt32 sps = 250;
            double x0factor = 1.0;

            dHz = AFreqHz - 40; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 40; }
            dHz = AFreqHz - 100; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 100; }
            //            dHz = AFreqHz - 150; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 150; }

            switch (useHz)
            {
                case 40:
                    /* butterworth Low Pass 3 order 40Hz
y[n] = (  1 * x[n- 3]) GAIN   1.727402859e+01
     + (  3 * x[n- 2])
     + (  3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.1180432721 * y[n- 3])
     + ( -0.6252304302 * y[n- 2])
     + (  1.0440641548 * y[n- 1])                     * */
                    mSetupFirst("Low Pass 3B Filter", 40, sps, x0factor, 1.727402859e+01);
                    mSetupParamsX3(1, 3, 3, 1);
                    mSetupParamsY3(1.0440641548, -0.6252304302, 0.1180432721);

                    break;
                case 100:
                    /* butterworth Low Pass 3 order 99.9Hz nyquest freq 100
y[n] = (  1 * x[n- 3])1.895287695e+00
     + (  3 * x[n- 2])
     + (  3 * x[n- 1])
     + (  1 * x[n- 0])

     + ( -0.2780599176 * y[n- 3])
     + ( -1.1828932620 * y[n- 2])
     + ( -1.7600418803 * y[n- 1])
*/
                    mSetupFirst("Low Pass 3B Filter", 100, sps, x0factor, 1.895287695e+00);
                    mSetupParamsX3(1, 3, 3, 1);
                    mSetupParamsY3(-1.7600418803, -1.1828932620, -0.2780599176);

                    break;
                case 150:
                    /* butterworth Low Pass 3 order 150Hz -> not available because > nyquest freq 100
                     */
                    //mSetupFirst("Low Pass 3B Filter", 150, sps, x0factor);
                    //mSetupParamsX3( , , , 1);
                    //mSetupParamsY3( , , );

                    break;
            }
        }
        public void mInitLowPass256Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes filter frequency
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 150;
            UInt32 sps = 256;
            double x0factor = 1.0;

            dHz = AFreqHz - 40; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 40; }
            dHz = AFreqHz - 100; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 100; }
            //            dHz = AFreqHz - 150; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 150; }

            switch (useHz)
            {
                case 40:
                    /* butterworth Low Pass 3 order 40Hz
                     y[n] = (  1 * x[n- 3])GAIN   1.829035153e+01
     + (  3 * x[n- 2])
     + (  3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.1251893421 * y[n- 3])
     + ( -0.6507758128 * y[n- 2])
     + (  1.0881973924 * y[n- 1])* */
                    mSetupFirst("Low Pass 3B Filter", 40, sps, x0factor, 1.829035153e+01);
                    mSetupParamsX3(1, 3, 3, 1);
                    mSetupParamsY3(1.0881973924, -0.6507758128, 0.1251893421);

                    break;
                case 100:
                    /* butterworth Low Pass 3 order 100 Hz
y[n] = (  1 * x[n- 3])GAIN   2.017469365e+00
     + (  3 * x[n- 2])
     + (  3 * x[n- 1])
     + (  1 * x[n- 0])

     + ( -0.2451736717 * y[n- 3])
     + ( -1.0735047027 * y[n- 2])
     + ( -1.6466854318 * y[n- 1])*/
                    mSetupFirst("Low Pass 3B Filter", 100, sps, x0factor, 2.017469365e+00);
                    mSetupParamsX3(1, 3, 3, 1);
                    mSetupParamsY3(-1.6466854318, -1.0735047027, -0.2451736717);

                    break;
                case 150:
                    /* butterworth Low Pass 3 order 150Hz -> not available because > nyquest freq 100
                     */
                    //mSetupFirst("Low Pass 3B Filter", 150, sps, x0factor);
                    //mSetupParamsX3( , , , 1);
                    //mSetupParamsY3( , , );

                    break;
            }
        }
        public void mInitLowPass1000Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 150;
            UInt32 sps = 1000;
            double x0factor = 1.0;

            dHz = AFreqHz - 40; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 40; }
            dHz = AFreqHz - 100; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 100; }
            dHz = AFreqHz - 150; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 150; }

            switch (useHz)
            {
                case 40:
                    /* butterworth Low Pass 3 order 40Hz
                    y[n] = (  1 * x[n- 3]) GAIN   6.381578779e+02
     + (  3 * x[n- 2])
     + (  3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.6041096995 * y[n- 3])
     + ( -2.1152541270 * y[n- 2])
     + (  2.4986083447 * y[n- 1])
     */
                    mSetupFirst("Low Pass 3B Filter", 40, sps, x0factor, 6.381578779e+02);
                    mSetupParamsX3( 1, 3, 3, 1);
                    mSetupParamsY3(2.4986083447, -2.1152541270, 0.6041096995);

                    break;
                case 100:
                    /* butterworth Low Pass 3 order 100Hz
                     y[n] = (  1 * x[n- 3])
     + (  3 * x[n- 2]) GAIN 5.525187588e+01
     + (  3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.2780599176 * y[n- 3])
     + ( -1.1828932620 * y[n- 2])
     + (  1.7600418803 * y[n- 1])
     */
                    mSetupFirst("Low Pass 3B Filter", 100, sps, x0factor, 5.525187588e+01);
                    mSetupParamsX3( 1, 3, 3, 1);
                    mSetupParamsY3(1.7600418803, -1.1828932620, 0.2780599176);

                    break;
                case 150:
                    /* butterworth Low Pass 3 order 150kHz
y[n] = (  1 * x[n- 3]) GAIN 2.018856265e+01
     + (  3 * x[n- 2])
     + (  3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.1377613013 * y[n- 3])
     + ( -0.6959427558 * y[n- 2])
     + (  1.1619174837 * y[n- 1])
     */
                    mSetupFirst("Low Pass 3B Filter", 150, sps, x0factor, 2.018856265e+01);
                    mSetupParamsX3( 1, 3, 3, 1);
                    mSetupParamsY3(1.1619174837, -0.6959427558, 0.1377613013);

                    break;
            }
        }
        public void mInitHighPass200Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes filter frequency
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 5;
            UInt32 sps = 200;
            double x0factor = 0.0;

            dHz = AFreqHz - 0.05F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 5; }
            dHz = AFreqHz - 0.5F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 500; }
            dHz = AFreqHz - 0.67F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 670; }
            dHz = AFreqHz - 3.5F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 3500; }

            switch (useHz)
            {
                case 5:
                    /* butterworth High Pass 3 order 0.05Hz
y[n] = ( -1 * x[n- 3])  GAIN   1.001572031e+00
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9968633367 * y[n- 3])
     + ( -2.9937217482 * y[n- 2])
     + (  2.9968584077 * y[n- 1])
     */
                    mSetupFirst("High Pass 3B Filter", 0.05F, sps, x0factor, 1.001572031e+00);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9968584077, -2.9937217482, 0.9968633367);

                    break;
                case 500:
                    /* butterworth High Pass 3 order 0.5Hz
y[n] = ( -1 * x[n- 3])  GAIN   1.015832146e+00
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9690721133 * y[n- 3])
     + ( -2.9376603253 * y[n- 2])
     + (  2.9685843964 * y[n- 1])                     */
                    mSetupFirst("High Pass 3B Filter", 0.5F, sps, x0factor, 1.015832146e+00);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9685843964, -2.9376603253, 0.9690721133);

                    break;
                case 670:
                    /* butterworth High Pass 3 order 0.67Hz
y[n] = ( -1 * x[n- 3])   GAIN   1.021272153e+00
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9587757021 * y[n- 3])
     + ( -2.9166882696 * y[n- 2])
     + (  2.9579034353 * y[n- 1])*/
                    mSetupFirst("High Pass 3B Filter", 0.67F, sps, x0factor, 1.021272153e+00);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9579034353, -2.9166882696, 0.9587757021);

                    break;
                case 3500:
                    /* butterworth High Pass 3 order 3.5Hz
y[n] = ( -1 * x[n- 3])
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9569700502 * y[n- 3])
     + ( -2.9129990440 * y[n- 2])
     + (  2.9560185887 * y[n- 1])                     */
                    mSetupFirst("High Pass 3B Filter", 3.5F, sps, x0factor, 1.116290672e+00);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9560185887, -2.9129990440, 0.9569700502);

                    break;
            }
        }
        public void mInitHighPass250Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes filter frequency
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 5;
            UInt32 sps = 250;
            double x0factor = 0.0;

            dHz = AFreqHz - 0.05F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 5; }
            dHz = AFreqHz - 0.5F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 500; }
            dHz = AFreqHz - 0.67F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 670; }
            dHz = AFreqHz - 3.5F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 3500; }

            switch (useHz)
            {
                case 5:
                    /* butterworth High Pass 3 order 0.05Hz
y[n] = ( -1 * x[n- 3]) 1.001257427e+00
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9974898813 * y[n- 3])
     + ( -2.9949766094 * y[n- 2])
     + (  2.9974867260 * y[n- 1])
     */
                    mSetupFirst("High Pass 3B Filter", 0.05F, sps, x0factor, 1.001257427e+00);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9974867260, -2.9949766094, 0.9974898813);

                    break;
                case 500:
                    /* butterworth High Pass 3 order 0.5Hz
y[n] = ( -1 * x[n- 3])1.012645743e+00
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9751802955 * y[n- 3])
     + ( -2.9500496793 * y[n- 2])
     + (  2.9748674241 * y[n- 1])*/
                    mSetupFirst("High Pass 3B Filter", 0.5F, sps, x0factor, 1.012645743e+00);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9748674241, -2.9500496793, 0.9751802955);

                    break;
                case 670:
                    /* butterworth High Pass 3 order 0.67Hz
y[n] = ( -1 * x[n- 3])1.016981713e+00
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9668825286 * y[n- 3])
     + ( -2.9332097484 * y[n- 2])
     + (  2.9663225245 * y[n- 1])*/
                    mSetupFirst("High Pass 3B Filter", 0.67F, sps, x0factor, 1.016981713e+00);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9663225245, -2.9332097484, 0.9668825286);

                    break;
                case 3500:
                    /* butterworth High Pass 3 order 3.5Hz
y[n] = ( -1 * x[n- 3])1.091980485e+00
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.8386297076 * y[n- 3])
     + ( -2.6633813877 * y[n- 2])
     + (  2.8241271559 * y[n- 1])*/
                    mSetupFirst("High Pass 3B Filter", 3.5F, sps, x0factor, 1.091980485e+00);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.8241271559, -2.6633813877, 0.8386297076);

                    break;
            }
        }
        public void mInitHighPass256Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes filter frequency
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 5;
            UInt32 sps = 256;
            double x0factor = 0.0;

            dHz = AFreqHz - 0.05F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 5; }
            dHz = AFreqHz - 0.5F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 500; }
            dHz = AFreqHz - 0.67F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 670; }
            dHz = AFreqHz - 3.5F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 3500; }

            switch (useHz)
            {
                case 5:
                    /* butterworth High Pass 3 order 0.05Hz
     y[n] = ( -1 * x[n- 3])1.001227938e+00
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9975486401 * y[n- 3])
     + ( -2.9950942728 * y[n- 2])
     + (  2.9975456309 * y[n- 1])*/
                    mSetupFirst("High Pass 3B Filter", 0.05F, sps, x0factor, 1.001227938e+00);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9975456309, -2.9950942728, 0.9975486401);

                    break;
                case 500:
                    /* butterworth High Pass 3 order 0.5Hz
y[n] = ( -1 * x[n- 3])1.012347532e+00
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9757549044 * y[n- 3])
     + ( -2.9512131915 * y[n- 2])
     + (  2.9754564614 * y[n- 1])*/
                    mSetupFirst("High Pass 3B Filter", 0.5F, sps, x0factor, 1.012347532e+00);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9754564614, -2.9512131915, 0.9757549044);

                    break;
                case 670:
                    /* butterworth High Pass 3 order 0.67Hz
y[n] = ( -1 * x[n- 3])1.016580414e+00
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9676460410 * y[n- 3])
     + ( -2.9347622379 * y[n- 2])
     + (  2.9671118224 * y[n- 1])
*/
                    mSetupFirst("High Pass 3B Filter", 0.67F, sps, x0factor, 1.016580414e+00);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9671118224, -2.9347622379, 0.9676460410);

                    break;
                case 3500:
                    /* butterworth High Pass 3 order 3.5Hz
y[n] = ( -1 * x[n- 3])
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.8420980930 * y[n- 3])
     + ( -2.6709275179 * y[n- 2])
     + (  2.8282466421 * y[n- 1])*/
                    mSetupFirst("High Pass 3B Filter", 3.5F, sps, x0factor, 1.089729372e+00);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.8282466421, -2.6709275179, 0.8420980930);

                    break;
            }
        }
        public void mInitHighPass1000Sps(float AFreqHz, UInt32 ASampleFreqSps)
        {
            // setup according to closes filter frequency
            float dHz, dFreqHz = 1e9F;
            UInt32 useHz = 5;
            UInt32 sps = 1000;
            double x0factor = 0.0;

            dHz = AFreqHz - 0.05F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 5; }
            dHz = AFreqHz - 0.5F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 500; }
            dHz = AFreqHz - 3.5F; if (dHz < 0) dHz = -dHz; if (dHz < dFreqHz) { dFreqHz = dHz; useHz = 3500; }

            switch (useHz)
            {
                case 5:
                    /* butterworth High Pass 3 order 0.05Hz
y[n] = ( -1 * x[n- 3])  GAIN   1.000314209e+00
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9993718788 * y[n- 3])
     + ( -2.9987435603 * y[n- 2])
     + (  2.9993716815 * y[n- 1])
     */
                    mSetupFirst("High Pass 3B Filter", 0.05F, sps, x0factor, 1.000314209e+00);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9993716815, -2.9987435603, 0.9993718788);

                    break;
                case 500:
                    /* butterworth High Pass 3 order 0.5Hz
y[n] = ( -1 * x[n- 3])  GAIN   1.003146534e+00
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9937365101 * y[n- 3])
     + ( -2.9874533582 * y[n- 2])
     + (  2.9937168173 * y[n- 1])
     */
                    mSetupFirst("High Pass 3B Filter", 0.5F, sps, x0factor, 1.003146534e+00);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9937168173, -2.9874533582, 0.9937365101);

                    break;
                case 670:
                    /* butterworth High Pass 3 order 0.67Hz
y[n] = ( -1 * x[n- 3]) GAIN 1.004218611e+00
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9916158700 * y[n- 3])
     + ( -2.9831964822 * y[n- 2])
     + (  2.9915805379 * y[n- 1])
*/
                    mSetupFirst("High Pass 3B Filter", 0.67F, sps, x0factor, 1.004218611e+00);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9915805379, -2.9831964822, 0.9916158700);

                    break;
                case 3500:
                    /* butterworth High Pass 3 order 3.5Hz
y[n] = ( -1 * x[n- 3])  GAIN   1.022235189e+00
     + (  3 * x[n- 2])
     + ( -3 * x[n- 1])
     + (  1 * x[n- 0])

     + (  0.9569700502 * y[n- 3])
     + ( -2.9129990440 * y[n- 2])
     + (  2.9560185887 * y[n- 1])                     */
                    mSetupFirst("High Pass 3B Filter", 3.5F, sps, x0factor, 1.022235189e+00);
                    mSetupParamsX3(1, -3, 3, -1);
                    mSetupParamsY3(2.9560185887, -2.9129990440, 0.9569700502);

                    break;
            }
        }

        protected override int Process(int x0)
        {
            return mDoFilter(x0);
        }

        public int mDoFilter(int x0)       // directly callable
        {
            double y1;

            if (_mbFirst)
            {
                _mHistoryIndex = 0;

                double xn = x0 * _mXGainFactor;
                y1 = _mFirstXFactor * xn;
                for (int i = 0; i < _cHistorySize; ++i) _mHistoryX[i] = xn;
                for (int i = 0; i < _cHistorySize; ++i) _mHistoryY[i] = y1;
                _mbFirst = false;
            }
            else
            {
                // move to next history
                ++_mHistoryIndex;
                _mHistoryIndex &= _cHistoryMask;    // round robin

                double xn = x0 *_mXGainFactor;     // example x / GAIN
                _mHistoryX[_mHistoryIndex] = xn;

                y1 = _mParamX[0] * xn;

                int iHistory = _mHistoryIndex;

                for( int i = 1; i <= _mLastParamNr; ++i)
                {
                    --iHistory;
                    iHistory &= _cHistoryMask;    // round robin

                    double dX = _mParamX[i] * _mHistoryX[iHistory];
                    double dY = _mParamY[i] * _mHistoryY[iHistory];
                    y1 += dX + dY;
                }
                _mHistoryY[_mHistoryIndex] = y1;
            }

            return (int)y1;
        }

        protected override void mDoReset()
        {
            _mbFirst = true;

        }

        public override string Name => _mNameLong;

        public override string ToString()
        {
            return _mNameShort;
        }
    }
    /************************************************************************************************************
     * */
    public class CBandBlockAvgHistFilter : Filter
    {
        float _mFreqHz;
        UInt32 _mSampleFreqSps;
        UInt16 _mNrAvgBlocks;   
        float _mAvgBlocksFactor;
        UInt16 _mAvgNPoints;    //  max 127



        private bool _mbFirst;
        Int32 _mAvgRound = 0;

        private Int32[] _mHistorySum = null;    // history for _mNrAvgBlocks  
                                                // max 127 * 24 bit values
        private Int32[] _mHistoryAvg = null;

        private Int32[] _mHistoryOffsetPos = null;  // offset ((i+O) % N) for positive point of block
        private Int32[] _mHistoryOffsetNeg = null;  // offset ((i+O) % N) for positive point of block

        private int _mHistoryIndex = 0;
        private int _mHistoryN = 0;

        double _mAvgBlockFactor = 0;
        double _mSpsPerPeriod = 0;

        private string _mNameShort = "BBahF";
        private string _mNameLong = "BBahF 0";

        public CBandBlockAvgHistFilter(float AFreqHz, UInt32 ASampleFreqSps, UInt16 ANrAvgBlocks, float AAvgBlocksFactor, UInt16 AAvgNPoints)
        {
            mInit(AFreqHz, ASampleFreqSps, ANrAvgBlocks, AAvgBlocksFactor, AAvgNPoints);
        }

        public void mInit(float AFreqHz, UInt32 ASampleFreqSps, UInt16 ANrAvgBlocks, float AAvgBlocksFactor, UInt16 AAvgNPoints)
        {
            _mFreqHz = AFreqHz < 0.01 ? 0.01F : AFreqHz;
            _mSampleFreqSps = ASampleFreqSps <= 0 ? 100 : ASampleFreqSps;
            _mNrAvgBlocks = ANrAvgBlocks < 1 ? (UInt16)1 : ANrAvgBlocks;
            _mAvgBlocksFactor = AAvgBlocksFactor;
            _mAvgNPoints = AAvgNPoints < 1 ? (UInt16)1 : (UInt16)(AAvgNPoints > 127 ? 127 :  AAvgNPoints);
            _mbFirst = true;
            _mAvgRound = _mAvgNPoints / 2;

            _mSpsPerPeriod = _mSampleFreqSps / _mFreqHz;
            _mHistoryN = (Int32)(_mNrAvgBlocks * _mSampleFreqSps / _mFreqHz + 0.5);

            _mHistorySum = new int[_mHistoryN];    // history for _mNrAvgBlocks  
            _mHistoryAvg = new int[_mHistoryN];

            for (int i = 0; i < _mHistoryN; ++i)
            {
                _mHistorySum[i] = 0;
                _mHistoryAvg[i] = 0;
            }
            _mHistoryOffsetPos = new int[_mNrAvgBlocks];  // offset ((i+O) % N) for positive point of block
            _mHistoryOffsetNeg = new int[_mNrAvgBlocks];  // offset ((i+O) % N) for positive point of block


            for (int i = 0; i < _mNrAvgBlocks; ++i)
            {
                double a = -i;

                _mHistoryOffsetPos[i] = (Int32)(a * _mSpsPerPeriod + 0.5 + _mHistoryN); // add N for round robin
                _mHistoryOffsetNeg[i] = (Int32)((a - 0.5) * _mSpsPerPeriod + 0.5 + _mHistoryN); // negative is extra 180 degrees (T/2)
            }

            _mHistoryIndex = 0;

            _mAvgBlockFactor = AAvgBlocksFactor / _mNrAvgBlocks / 2.0;  // 

            _mNameLong = _mNameShort + " " + AFreqHz.ToString("0.0");

        }
        protected override int Process(int x0)
        {
            return mDoFilter(x0);
        }

        public int mDoFilter(int x0)       // directly callable
        {
            int y1;

            if (_mbFirst)
            {
                _mHistoryIndex = 0;

                Int32 sumX = x0 * _mNrAvgBlocks;
                for (int i = 0; i < _mHistoryN; ++i)
                {
                    _mHistorySum[i] = sumX;
                    _mHistoryAvg[i] = x0;
                }
                y1 = x0;

                _mbFirst = false;
            }
            else
            {
                // move to next history
                ++_mHistoryIndex;
                _mHistoryIndex %= _mHistoryN;    // round robin

                Int32 sumX = _mHistorySum[_mHistoryIndex] - _mHistoryAvg[_mHistoryIndex];
                sumX += x0;
                _mHistorySum[_mHistoryIndex] = sumX;

                int dX = (sumX + _mAvgRound) / _mAvgNPoints; // + pos[0]
                _mHistoryAvg[_mHistoryIndex] = dX;

                int iHistory = (_mHistoryIndex + _mHistoryOffsetNeg[0]) % _mHistoryN;
                dX -= _mHistoryAvg[iHistory];           // - neg[0]

                for (int i = 1; i < _mNrAvgBlocks; ++i)
                {
                    iHistory = (_mHistoryIndex + _mHistoryOffsetPos[i]) % _mHistoryN;
                    dX += _mHistoryAvg[iHistory];           // + pos[n]
                    iHistory = (_mHistoryIndex + _mHistoryOffsetNeg[i]) % _mHistoryN;
                    dX -= _mHistoryAvg[iHistory];           // - neg[n]
                }
                dX = (Int32)(dX * _mAvgBlockFactor + 0.5);  // avarage powerline noise last N periods
                y1 = x0 - dX;
            }

            return y1;
        }

        protected override void mDoReset()
        {
            _mbFirst = true;

        }

        public override string Name => _mNameLong;

        public override string ToString()
        {
            return _mNameShort;
        }
    }
    /************************************************************************************************************
     * */
    public class CClipResetFilter : Filter
    {
        private float _mClipAmplitudemV = 0;
        private float _mResetAmplitudemV  = 0;
        private float _mAmplitudeGain1mV = 1000;

        private bool _mbDoClipReset = false;
        private Int32 _mClipPos = 0;
        private Int32 _mResetPos = 0;
        private Int32 _mClipNeg = 0;
        private Int32 _mResetNeg = 0;
        private Int32 _mOffset = 0;
        private bool _mbFirst = true;

        private string _mNameShort = "CRF";
        private string _mNameLong = "CRF 0";

        public CClipResetFilter(float AClipAmplitudemV, float AResetAmplitudemV, float AAmplitudeGain1mV)
        {
            mInit(AClipAmplitudemV, AResetAmplitudemV, AAmplitudeGain1mV);
        }

        public void mInit(float AClipAmplitudemV, float AResetAmplitudemV, float AAmplitudeGain1mV)
        {
            _mClipAmplitudemV = AClipAmplitudemV;
            _mResetAmplitudemV = AResetAmplitudemV;
            _mAmplitudeGain1mV = AAmplitudeGain1mV;

            _mbDoClipReset = _mClipAmplitudemV > 0.001;

            _mClipPos = _mbDoClipReset ? (Int32)(_mClipAmplitudemV *AAmplitudeGain1mV +0.5 ) : 0;
            _mResetPos = _mbDoClipReset ? (Int32)(_mResetAmplitudemV * AAmplitudeGain1mV + 0.5) : 0;
            _mClipNeg = -_mClipPos;
            _mResetNeg = -_mResetPos;
            _mOffset = 0;

            _mbFirst = true;
            _mNameShort = "CRF";
            _mNameLong = _mNameShort + (_mbDoClipReset ?  " " + AClipAmplitudemV.ToString("0.0") : " off");
        }
        protected override int Process(int x0)
        {
            return mDoFilter(x0);
        }

        public int mDoFilter(int x0)       // directly callable
        {
            int y1;

            if (_mbDoClipReset)
            {
                if (_mbFirst)
                {
                    _mOffset = x0;          // start at 0 line
                    y1 = 0;
                    _mbFirst = false;
                }
                else 
                {
                    y1 = x0 - _mOffset;     // add current offset

                    if( y1 > _mClipPos)
                    {
                        y1 = _mResetPos;    // above ClipPos move to ResetPos value
                        _mOffset = x0 - _mResetPos; 
                    }
                    else if (y1 < _mClipNeg)
                    {
                        y1 = _mResetNeg;    // below ClipNeg move to ResetNeg value
                        _mOffset = x0 - _mResetNeg;
                    }
                }
            }
            else
            {
                y1 = x0;
            }
            return y1;
        }

        protected override void mDoReset()
        {
            _mbFirst = true;

        }

        public override string Name => _mNameLong;

        public override string ToString()
        {
            return _mNameShort;
        }
    }
    /************************************************************************************************************
     * */
    public class CAvgHistoryFilter : Filter
    {
        private bool _mbUseStoredHistory = true;
        private float _mAvgTimeSec;
        private UInt32 _mSampleFreqSps;
        private float _mApplyDeltamV = 0;
        private float _mAmplitudeGain1mV = 0;
        private float _mApplyFactor = 0;

        private Int32 _mAvgRound = 0;
        private Int32 _mAvgN = 0;
        private bool _mbFirst = true;

        private Int64 _mAvgSum = 0;
        private Int32 _mAvgValue = 0;
        private Int32[] _mHistoryX = null;

        private int _mHistoryIndex = 0;

        private Int32 _mDeltaPos = 0;
        private Int32 _mDeltaMin = 0;
        private bool _mbDoDelta = false;

        private string _mNameShort = "BBahF";
        private string _mNameLong = "BBahF 0";

        public CAvgHistoryFilter(bool AbUseStoredHistory, float AAvgTimeSec, UInt32 ASampleFreqSps, float AApplyDeltamV, float AAmplitudeGain1mV, float AApplyFactor)
        {
            mInit(AbUseStoredHistory, AAvgTimeSec, ASampleFreqSps, AApplyDeltamV, AAmplitudeGain1mV, AApplyFactor);
        }

        public void mInit(bool AbUseStoredHistory, float AAvgTimeSec, UInt32 ASampleFreqSps, float AApplyDeltamV, float AAmplitudeGain1mV, float AApplyFactor)
        {
            _mbUseStoredHistory = AbUseStoredHistory;
            _mAvgTimeSec = AAvgTimeSec < 0.01 ? 0.00F : AAvgTimeSec;
            _mSampleFreqSps = ASampleFreqSps <= 0 ? 100 : ASampleFreqSps;
            _mAvgN = (Int32)(_mAvgTimeSec * _mSampleFreqSps + 0.5);
            if (_mAvgN < 1) _mAvgN = 1;
            _mAvgRound = _mAvgN / 2;

            _mAvgSum = 0;
            _mAvgValue = 0;
            if (_mbUseStoredHistory)
            {
                _mHistoryX = new Int32[_mAvgN];

                _mHistoryIndex = 0;

                for (int i = 0; i < _mAvgN; ++i)
                {
                    _mHistoryX[i] = 0;
                }
            }
            else
            {
                _mHistoryX = null;   // not used

                _mHistoryIndex = 0;

            }
            _mApplyDeltamV = AApplyDeltamV;
            _mAmplitudeGain1mV = AAmplitudeGain1mV;
            _mApplyFactor = AApplyFactor;

            _mDeltaPos = AApplyDeltamV > 0.01 && AApplyDeltamV < 9999 ? (Int32)(AApplyDeltamV * AAmplitudeGain1mV) : 0;
            _mDeltaMin = -_mDeltaPos;
            _mbDoDelta = _mDeltaPos > 0;

            _mNameLong = _mNameShort + " " + (_mAvgTimeSec * 1000 + 0.5).ToString("0.0") + "ms";
            _mbFirst = true;
        }
        protected override int Process(int x0)
        {
            return mDoFilter(x0);
        }

        public int mDoFilter(int x0)       // directly callable
        {
            int y1;

            if (_mbFirst)
            {
                _mHistoryIndex = 0;

                _mAvgSum = _mAvgN * (Int64)x0;
                _mAvgValue = x0;

                if (_mbUseStoredHistory)
                {
                    for (int i = 0; i < _mAvgN; ++i)
                    {
                        _mHistoryX[i] = x0;
                    }
                }
                y1 = x0;
                _mbFirst = false;
            }
            else
            {
                if (_mbUseStoredHistory)
                {
                    // move to next history
                    ++_mHistoryIndex;
                    _mHistoryIndex %= _mAvgN;    // round robin

                    _mAvgSum -= _mHistoryX[_mHistoryIndex];
                    _mHistoryX[_mHistoryIndex] = x0;
                }
                else
                {
                    _mAvgSum -= _mAvgValue;

                }
                _mAvgSum += x0;

                _mAvgValue = (Int32)((_mAvgSum + _mAvgRound) / _mAvgN);
                y1 = _mAvgValue;

                if (_mbDoDelta)
                {
                    Int32 dX = x0 - y1;     // change delta

                    if (dX > _mDeltaPos || dX < _mDeltaMin)
                    {
                        int dY = (Int32)(dX * _mApplyFactor);
                        y1 += dY;  //  bigger then _mApplyDeltamV => add part delta (for ECG R top)
                                   //                        _mAvgSum += _mAvgN / 2 * (Int64)dY;
                    }
                }
            }
            return y1;
        }

        protected override void mDoReset()
        {
            _mbFirst = true;
        }

        public override string Name => _mNameLong;

        public override string ToString()
        {
            return _mNameShort;
        }
    }
    /************************************************************************************************************
         * */
    public class CAvgPointIntFilter : Filter
    {
        private bool _mbUseStoredHistory = true;

        private Int32 _mAvgRound = 0;
        private Int32 _mAvgN = 0;
        private bool _mbFirst;

        private Int64 _mAvgSum = 0;
        private Int32 _mAvgValue = 0;
        private Int32[] _mHistoryX = null;

        private int _mHistoryIndex = 0;

        private string _mNameShort = "aiF";
        private string _mNameLong = "aiF 0";

        public CAvgPointIntFilter(bool AbUseStoredHistory, UInt16 ANrAvgPoints)
        {
            mInit(AbUseStoredHistory, ANrAvgPoints);
        }

        public void mInit(bool AbUseStoredHistory, UInt16 ANrAvgPoints)
        {
            _mbUseStoredHistory = AbUseStoredHistory;
            _mAvgN = (Int32)(ANrAvgPoints);
            if (_mAvgN < 1) _mAvgN = 1;
            _mAvgRound = _mAvgN / 2;

            _mAvgSum = 0;
            _mAvgValue = 0;
            if (_mbUseStoredHistory)
            {
                _mHistoryX = new Int32[_mAvgN];

                _mHistoryIndex = 0;

                for (int i = 0; i < _mAvgN; ++i)
                {
                    _mHistoryX[i] = 0;
                }
            }
            else
            {
                _mHistoryX = null;   // not used

                _mHistoryIndex = 0;

            }
            _mNameLong = _mNameShort + " " + _mAvgN;
        }
        protected override int Process(int x0)
        {
            return mDoFilter(x0);
        }

        public int mDoFilter(int x0)       // directly callable
        {
            int y1;

            if (_mbFirst)
            {
                _mHistoryIndex = 0;

                _mAvgSum = _mAvgN * (Int64)x0;
                _mAvgValue = x0;

                if (_mbUseStoredHistory)
                {
                    for (int i = 0; i < _mAvgN; ++i)
                    {
                        _mHistoryX[i] = x0;
                    }
                }
                y1 = x0;
                _mbFirst = false;
            }
            else
            {
                if (_mbUseStoredHistory)
                {
                    // move to next history
                    ++_mHistoryIndex;
                    _mHistoryIndex %= _mAvgN;    // round robin

                    _mAvgSum -= _mHistoryX[_mHistoryIndex];
                    _mHistoryX[_mHistoryIndex] = x0;
                }
                else
                {
                    _mAvgSum -= _mAvgValue;

                }
                _mAvgSum += x0;

                _mAvgValue = (Int32)((_mAvgSum + _mAvgRound) / _mAvgN);
                y1 = _mAvgValue;
            }
            return y1;
        }

        protected override void mDoReset()
        {
            _mbFirst = true;
        }

        public override string Name => _mNameLong;

        public override string ToString()
        {
            return _mNameShort;
        }
    }
    public class CAvgPointDblFilter : Filter
    {
        private bool _mbUseStoredHistory = true;

        private Int32 _mAvgN = 0;
        private bool _mbFirst;

        private double _mAvgSum = 0;
        private double _mAvgValue = 0;
        private double[] _mHistoryX = null;

        private int _mHistoryIndex = 0;

        private string _mNameShort = "adF";
        private string _mNameLong = "apF 0";

        public CAvgPointDblFilter(bool AbUseStoredHistory, UInt16 ANrAvgPoints)
        {
            mInit(AbUseStoredHistory, ANrAvgPoints);
        }

        public void mInit(bool AbUseStoredHistory, UInt16 ANrAvgPoints)
        {
            _mbUseStoredHistory = AbUseStoredHistory;
            _mAvgN = (Int32)(ANrAvgPoints);
            if (_mAvgN < 1) _mAvgN = 1;

            _mAvgSum = 0;
            _mAvgValue = 0;
            if (_mbUseStoredHistory)
            {
                _mHistoryX = new double[_mAvgN];

                _mHistoryIndex = 0;

                for (int i = 0; i < _mAvgN; ++i)
                {
                    _mHistoryX[i] = 0;
                }
            }
            else
            {
                _mHistoryX = null;   // not used

                _mHistoryIndex = 0;

            }
            _mNameLong = _mNameShort + " " + _mAvgN;
        }
        protected override int Process(int x0)
        {
            return x0; // do not use !!! mDoFilter(x0);
        }

        public double mDoFilter(double x0)       // directly callable
        {
            double y1;

            if (_mbFirst)
            {
                _mHistoryIndex = 0;

                _mAvgSum = _mAvgN * x0;
                _mAvgValue = x0;

                if (_mbUseStoredHistory)
                {
                    for (int i = 0; i < _mAvgN; ++i)
                    {
                        _mHistoryX[i] = x0;
                    }
                }
                y1 = x0;
                _mbFirst = false;
            }
            else
            {
                if (_mbUseStoredHistory)
                {
                    // move to next history
                    ++_mHistoryIndex;
                    _mHistoryIndex %= _mAvgN;    // round robin

                    _mAvgSum -= _mHistoryX[_mHistoryIndex];
                    _mHistoryX[_mHistoryIndex] = x0;
                }
                else
                {
                    _mAvgSum -= _mAvgValue;

                }
                _mAvgSum += x0;

                _mAvgValue = _mAvgSum / _mAvgN;
                y1 = _mAvgValue;
            }
            return y1;
        }

        protected override void mDoReset()
        {
            _mbFirst = true;
        }

        public override string Name => _mNameLong;

        public override string ToString()
        {
            return _mNameShort;
        }
    }
}
