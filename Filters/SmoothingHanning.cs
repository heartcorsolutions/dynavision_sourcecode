﻿namespace DynaVisionGraph.Filter
{
    public class SmoothingHanning : Filter
    {
        private int x1, x2;

        protected override int Process(int x0)
        {
            var y0 = (x0 + 2 * x1 + x2) / 4;
            x2 = x1;
            x1 = x0;
            return y0;
        }
        protected override void mDoReset()
        {
        }

        public override string Name => "Hanning Smoothing " + 4;
    }
}
