﻿using DynaVisionGraph.Filter;

namespace DynaVisionGraph.Filter
{
    public class DerivativeFilter : Filter
    {
        private /*readonly*/ FilterHistory<int> x = new FilterHistory<int>(4);

        protected override int Process(int x0)
        {
            var y0 = (2 * x0 + x[1] - x[3] - 2 * x[4]) / 8;
            x.AddSample(x0);
            return y0;
        }
        protected override void mDoReset()
        {
            x = new FilterHistory<int>(4);   // clear by creating new array
        }

        public override string Name => "Deriv test";
    }
}
