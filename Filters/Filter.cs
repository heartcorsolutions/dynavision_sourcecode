﻿namespace DynaVisionGraph.Filter
{
    public abstract class Filter
    {
        public int Filt(int x0)
        {
            // preload functionality; if preload is enabled the filter histories will be filled 
            // with the most recent samples. when the filter is the enabled it will return correct values
            // immediately rather than return values from an uninitialized filter. Without preloading 
            // an unused filter does not have to keep a FilterHistory in memory (lazy).

            if (this.Enabled)
            {
                return this.Process(x0);    // process and return filtered
            }
            if (!this.PreLoad) return x0;   // return unfiltered

            this.Process(x0);               // process and return unfiltered
            return x0;
        }

        protected abstract int Process(int x0);

        protected abstract void mDoReset();

        public void mReset()
        {
            mDoReset();
        }

        public abstract string Name { get; }

        public bool Enabled { get; set; }

        public bool PreLoad { get; set; } 
    }
}
