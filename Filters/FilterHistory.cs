﻿using System;

namespace DynaVisionGraph.Filter
{
    public class FilterHistory<T>
    {
        private readonly T[] buffer;
        private int current;

        public FilterHistory(int size)
        {
            this.buffer = new T[size];
            current = 0;
        }
        
        public void AddSample(T sample)
        {
            if (++current >= buffer.Length)
            {
                current = 0;
            }
            this.buffer[current] = sample;
        }

        /*
         *  Indexer to read from and write to the history.
         *  Indices can be supplied in a range of 1 to size, where 1 is the value 1 sample ago.
         */
        public T this[int i]
        {
            get
            {
                if(i <= 0 || i > buffer.Length)
                    throw new ArgumentOutOfRangeException(nameof(i), $"{nameof(i)} can not be 0 or larger than {buffer.Length}.");
                
                i -= 1;
                if (i > current)
                    return buffer[buffer.Length - (i - current)];
                return buffer[current - i];
            }
        }
    }
}
