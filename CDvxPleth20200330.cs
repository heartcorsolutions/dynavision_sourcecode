﻿using Event_Base;
using Program_Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 *  plethysmographic waveform (pulse waveform) from Nonin pulse oximeter infra-red signal. 
 *  stored as 16 bit at 75 samples per second
*/

namespace EventBoard.Event_Base
{
     public class CDvxSpO2
    {
        const UInt16 _cNrBytesInFrame = 5;
        const UInt16 _cNrFramesInPacket = 25;
        const UInt16 _cNrBytesInPacket = _cNrBytesInFrame * _cNrFramesInPacket;
        const UInt16 _cNrPacketsPerSecond = 3;
        public const UInt16 _cNrFramesPerSecond = 75;
        const double _cFrameUnitSec = 1.0 / _cNrFramesPerSecond;
        const UInt32 _cBufferSize = 3 * _cNrBytesInPacket;
        const UInt32 _cFrameSeqBad = _cNrFramesInPacket + 1;

        const byte _cStatusB7_Always_ON = 0x80;    // always on
        const byte _cStatusB6_SNSD = 0x40;    // Sensor Disconnect Sensor is not connected to oximeter or sensor is inoperable.
        const byte _cStatusB5_ARTF = 0x20;    // Artifact Indicates artifact condition of each pulse (occurs only during pulse).
        const byte _cStatusB4_OOT= 0x10;    // Out Of Track An absence of consecutive good pulse signals.
        const byte _cStatusB3_SNSA = 0x08;    //Sensor Alarm Device is providing unusable data for analysis (bad signal noise, no finger)
        const byte _cStatusB2_LOW__SIGNAL = 0x04;    //Red – low/no pulse signa
        const byte _cStatusB2_MEDIUM_SIGNAL = 0x06;    //Yellow – low/marginal pulse signal
        const byte _cStatusB1_HIGH_SIGNAL = 0x02;    //Green – high pulse signal 
        const byte _cStatusB0_SYNC = 0x01;    // sync bit indicating Frame 0
        const byte _cStatusSignalFlags = 0x06;    //Yellow – low/marginal pulse signal
        const byte _cStatusErrorFlags = 0x78;   // if one of the flags is active data is not reliable

        const byte _cInfoB7_Always_Off = 0x80;    // always on

        const Int32 _cBadInfoValue = -10;
        const Int32 _cBadPlethValue = -100;

        const string _cCsvTab = "\t";

        // frame buffer

        private byte[] _mBuffer = null; // last 3 frames
        private UInt32 _mBufferIndex = 0;    // last store space
        private UInt32 _mTotalBytes = 0;
        private UInt32 _mUsedBytes = 0;

        // frame

        private byte[] _mFrameBuffer = null; // last 5 bytes
        private UInt32 _mFrameIndex = 0;     // index in buffer for current frame
        private UInt32 _mFrameSeqNr = _cFrameSeqBad;    // frame sequence number (ok 1.. 25)
        private byte _mFrameStatus = 0;     // frame status byte
        private Int32 _mFramePleth = _cBadPlethValue;    // frame pleth value
        private byte _mFrameInfo = 0;       // 
        private UInt32 _mFrameNextIndex = 0;     // index after frame
        private UInt32 _mFrameNextCount = 0;     // count bytes in new 'frame'
        private bool _mbFrameSensorOk = false;   // good measurement
        private UInt32 _mFramesCount = 0;       // total number of frames => time from start
        private UInt32 _mFramesOkCount = 0;     // number of frames that are ok (no Error)
        private UInt32 _mFramesSignalLow = 0;     // number of frames with a signal strength Low
        private UInt32 _mFramesSignalMedium = 0;     // number of frames with a signal strength Medium
        private UInt32 _mFramesSignalHigh = 0;     // number of frames with a signal strength High

        // packet
        private UInt32 _mPacketIndex = 0;     // index in buffer for current packet
        private UInt32 _mPacketCount = 0;     // total number of full packets

        private Int16 _mLastPulseRate = -1;
        private Int16 _mLastSpO2 = -1;

        // First file
        private string _mConvertSpO2Path = "";

        private DateTime _mFirstFileUTC = DateTime.MinValue;    // base date time
        private string _mFirstFullName = "";
        private string _mFirstFileName = "";

        private bool _mbWriteCsv = false;
        private bool _mbWriteMit16 = false;
        private string _mCsvFullName = "";
        private StreamWriter _mCsvWriter = null;

        private bool _mbDebugRead = true;
        private string _mDebugReadLog = "";
        // Multiple files

        public UInt16 _mFileCount = 0;
        public string _mFileFullName;
        public string _mFileName;
        public DateTime _mFileUTC;
        public UInt32 _mFileSize = 0;
        public UInt32 _mFileBytesAdded = 0;
        public UInt32 _mDeviceNr = 0;

        private UInt32 _mTotalFramesCount = 0;     // number of frames that are ok (no Error)
        private UInt32 _mTotalFramesOkCount = 0;     // number of frames that are ok (no Error)
        private UInt32 _mTotalFramesSignalLow = 0;     // number of frames with a signal strength Low
        private UInt32 _mTotalFramesSignalMedium = 0;     // number of frames with a signal strength Medium
        private UInt32 _mTotalFramesSignalHigh = 0;     // number of frames with a signal strength High
        private UInt32 _mTotalPacketCount = 0;     // total number of full packets


        public CDvxSpO2(bool AbWrite2Csv, bool AbWriteMit16)
        {
            _mbWriteCsv = AbWrite2Csv;
            _mbWriteMit16 = AbWriteMit16;

        _mBuffer = new byte[_cBufferSize];
            _mFrameBuffer = new byte[_cNrBytesInFrame];

        }
         ~CDvxSpO2()
        {
            mClose();
        }
        public void mClose()
        {
            if( null != _mCsvWriter)
            {
                _mCsvWriter.Flush();
                _mCsvWriter.Close();
                _mCsvWriter = null;
            }
        }
        UInt32 mBufferNextIndex(UInt32 AIndex)
        {
            UInt32 i = AIndex;
            ++i;
            if (i >= _cBufferSize)
            {
                i = 0;
            }
            return i;
        }
        UInt32 mBufferGetIndex(UInt32 AIndex, Int32 AOffset)
        {
            UInt32 i = (UInt32)(AIndex + _cBufferSize + AOffset);

            i = i % _cBufferSize;

            return i;
        }
        byte mBufferGetByte(UInt32 AIndex, Int32 AOffset)
        {
            UInt32 i = (UInt32)(AIndex + _cBufferSize + AOffset);

            i = i % _cBufferSize;

            return _mBuffer[i];
        }

        public DSpO2FrameReadResult mAddByte( byte AByte )
        {
            DSpO2FrameReadResult result = DSpO2FrameReadResult.None;
            UInt32 storeIndex = _mBufferIndex;
            string logLine;
            // fill buffer
            _mBuffer[storeIndex] = AByte;
            _mBufferIndex = mBufferNextIndex(_mBufferIndex);
            ++_mTotalBytes;
            ++_mFileBytesAdded;

            ++_mFrameNextCount;

            if (_mFrameNextCount >= _cNrBytesInFrame)
            {
                // posisble new frame -> do checksum test
                byte byte1 = mBufferGetByte(_mFrameNextIndex, 0);   // Status
                byte byte2 = mBufferGetByte(_mFrameNextIndex, 1);   // Pleth MSB
                byte byte3 = mBufferGetByte(_mFrameNextIndex, 2);   // Pleth LSB
                byte byte4 = mBufferGetByte(_mFrameNextIndex, 3);   // Info
                byte chkSumAdd = (byte)(byte1 + byte2 + byte3 + byte4);
                byte chkSumOr = (byte)(byte1 | byte2 | byte3 | byte4);
                byte chkSumXor = (byte)(byte1 ^ byte2 ^ byte3 ^ byte4);
                bool bFlagsOk = 0 != (byte1 & _cStatusB7_Always_ON) && 0 == (byte4 & _cInfoB7_Always_Off);
                bool bSync = 0 != (byte1 & _cStatusB0_SYNC);

                if (_mbDebugRead && ( _mFileBytesAdded < 100 || _mPacketCount <= 1))
                {
                    logLine = _mFileBytesAdded + "\t$" + AByte.ToString("X02") + "\t$"
                        + byte1.ToString("X02") + byte2.ToString("X02") + byte3.ToString("X02") + byte4.ToString("X02")
                        + "\t" + (bFlagsOk ? "ok" : "--") 
                        + (bSync ? "S": "_")
                        + "\tf=" + _mFrameSeqNr
                        + "\tCHK$" + AByte.ToString("X02") + (chkSumAdd == AByte ? "=" : "!") 
                        + "\tadd$" + chkSumAdd.ToString("X02")
                        + "\tor$" + chkSumOr.ToString("X02")
                        + "\txor$" + chkSumXor.ToString("X02")
                        ;

                    _mDebugReadLog += logLine + "\r\n";
                }

                if ( 0 == chkSumAdd && 0 == chkSumOr)
                {
                    // 0 0 0 0 0 empty frame => increase frame count to get time right
                    _mFrameStatus = 0;          // no write to csv
                    _mbFrameSensorOk = false;

                    _mFramePleth = _cBadPlethValue;
                    _mFrameInfo = 0;

                    result = DSpO2FrameReadResult.Overflow_Frame;

                    if( ++_mFrameSeqNr < _cFrameSeqBad )
                    {
                        _mFrameSeqNr = _cFrameSeqBad;
                    }
                }
                else if (chkSumAdd == AByte)
                {
                    // checksum OK
                    if (bFlagsOk)
                    {
                        // checksum OK and bits are ok => new frame
                        _mFrameStatus = byte1;
                        _mbFrameSensorOk = 0 == (_mFrameStatus & _cStatusErrorFlags);   // good measurement

                        _mFramePleth = _mbFrameSensorOk ? (Int32)((byte2 << 8) + byte3) : _cBadPlethValue;
                        _mFrameInfo = byte4;

                         if (_mbFrameSensorOk)
                        {
                            ++_mFramesOkCount;     // number of frames that are ok (no Error)
                            ++_mTotalFramesOkCount;
                            byte flags = (byte)(_mFrameStatus & _cStatusSignalFlags);

                            if (_cStatusB2_LOW__SIGNAL == flags)
                            {
                                ++_mFramesSignalLow;
                                ++_mTotalFramesSignalLow;
                            }
                            else if (_cStatusB2_MEDIUM_SIGNAL == flags)
                            {
                                ++_mFramesSignalMedium;
                                ++_mTotalFramesSignalMedium;
                            }
                            else if (_cStatusB1_HIGH_SIGNAL == flags)
                            {
                                ++_mFramesSignalHigh;
                                ++_mTotalFramesSignalHigh;
                            }
                        }
                        ++_mFrameSeqNr;

                        result = _mFrameSeqNr > _cNrFramesInPacket ? DSpO2FrameReadResult.Overflow_Frame :
                            (_mFrameSeqNr == _cNrFramesInPacket ? DSpO2FrameReadResult.Last_Frame : DSpO2FrameReadResult.Other_Frame);


                        if (bSync)//0 != (byte1 & _cStatusB0_SYNC))
                        {
                            _mFrameSeqNr = 1;   // first frame in packet
                            result = DSpO2FrameReadResult.First_Frame;
                            ++_mPacketCount;
                            ++_mTotalPacketCount;
                            _mPacketIndex = _mFrameNextIndex;
                            if (_mPacketCount <= 1)
                            {
                                int i = 3;
                            }

                        }
                    }
                }
                if (result == DSpO2FrameReadResult.None)
                {
                    // frame overflow move next to check next byte to get in sysnc
                    --_mFrameNextCount;
                    _mFrameNextIndex = mBufferNextIndex(_mFrameNextIndex);
                }
                else
                {
                    // use frame => move to next
                    _mUsedBytes += _cNrBytesInFrame;
                    _mFrameIndex = _mFrameNextIndex;
                    ++_mFramesCount;
                    ++_mTotalFramesCount;
                    _mFrameNextIndex = _mBufferIndex;
                    _mFrameNextCount = 0;

                    if (null != _mCsvWriter && 0 != _mFrameStatus)   // only write frames with valid data
                    {
                        mbCsvWriteFrame();
                    }
                }
            }
            return result;
        }

        public bool mbCsvOpenFile(string APath, string AFileSpO2Name)
        {
            bool bOk = false;

            try
            {
                if (AFileSpO2Name != null && AFileSpO2Name.Length > 10)
                {
                    _mCsvFullName = Path.Combine( APath, AFileSpO2Name + "_" + CProgram.sDateTimeToYMDHMS( DateTime.Now ) + ".csv");
                    _mCsvWriter = new StreamWriter(_mCsvFullName);

                    string s = " _yyyMMddHHmmss " + _cCsvTab + "iFrame"
                        +_cCsvTab + "sec" + _cCsvTab + "Pleth";
                    _mCsvWriter.WriteLine(s);

                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("DvxSpO2 open csv", ex);
            }
            return bOk;
        }

        private UInt16 mFrameSignalStrength()
        {
            UInt16 signal = 0;

            if( _mbFrameSensorOk)
            {
                byte flags = (byte)(_mFrameStatus & _cStatusSignalFlags);

                signal = 1;

                if (_cStatusB2_LOW__SIGNAL == flags)
                {
                    signal = 2;
                }
                else if (_cStatusB2_MEDIUM_SIGNAL == flags)
                {
                    signal = 3;
                }
                else if (_cStatusB1_HIGH_SIGNAL == flags)
                {
                    signal = 4;
                }

            }
            return signal;
        }

        private byte mPacketInfoByte(DSpO2FrameSequence AFrameSequence)
        {
            byte value = 0xFF;

            if (_mTotalPacketCount > 0 && AFrameSequence < DSpO2FrameSequence.Overflow)
            {
                // get byte4 from frame[x]
                Int32 offset = ((Int32)AFrameSequence - 1) * _cNrBytesInFrame + 3;
                value = mBufferGetByte(_mPacketIndex, offset);
            }
            return value;
        }

        private Int16 mPacketInfoValue(DSpO2FrameSequence AFrameSequence)
        {
            Int16 value = _cBadInfoValue;

            if(_mTotalPacketCount > 0 && AFrameSequence < DSpO2FrameSequence.Overflow)
            {
                byte info = mPacketInfoByte(AFrameSequence);

                switch ( AFrameSequence)
                {
                    //None = 0,   // no frame yet
                    // PR_MSB,     //  1   4-beat Pulse Rate Average
                    case DSpO2FrameSequence.PR_LSB:     //  2
                        value = (Int16)(((mPacketInfoByte(DSpO2FrameSequence.PR_MSB) & 0x03 ) << 7 ) +info);
                        break;
                    case DSpO2FrameSequence.SpO2:       //  3   4-beat SpO 2 Average
                        value = info;
                        break;
                    case DSpO2FrameSequence.SREV:       //  4   software revision
                        value = info;
                        break;
                    //Reserved5,  //  5
                    //Reserved6,  //  6
                    //Reserved7,  //  7
                    case DSpO2FrameSequence.STAT2:      //  8   Status byte 2: B5=SPA high quality status
                        value = info;
                        break;
                    case DSpO2FrameSequence.SpO2_D:     //  9   Display 10sec: 
                        value = info;
                        break;
                    case DSpO2FrameSequence.SpO2_Fast:  // 10   4-beat Average SpO2 optimized for fast responding
                        value = info;
                        break;
                    case DSpO2FrameSequence.SpO2_BB:    // 11   Beat to Beat SpO2 value – No Average
                        value = info;
                        break;
                    //Reserved12, // 12
                    //Reserved13, // 13
                    //EPR_MSB,    // 14
                    case DSpO2FrameSequence.EPR_LSB:    // 15
                        value = (Int16)(((mPacketInfoByte(DSpO2FrameSequence.EPR_MSB) & 0x03) << 7) + info);
                        break;
                    case DSpO2FrameSequence.ESpO2:      // 16   8-beat SpO 2 Extended Average
                        value = info;
                        break;
                    case DSpO2FrameSequence.ESpO2_D:    // 17   Display 10sec: 8-beat SpO 2 Extended Average
                        value = info;
                        break;
                    //Reserved18, // 18
                    //Reserved19, // 19
                    //PRD_MSB,    // 20   Display 10sec: 4-beat Pulse Rate Average
                    case DSpO2FrameSequence.PRD_LSB:    // 21
                        value = (Int16)(((mPacketInfoByte(DSpO2FrameSequence.PRD_MSB) & 0x03) << 7) + info);
                        break;
                    //EPRD_MSB,   // 22   Display 10sec: 8-beat Pulse Rate Extended Average
                    case DSpO2FrameSequence.EPRD_LSB:   // 23
                        value = (Int16)(((mPacketInfoByte(DSpO2FrameSequence.EPRD_MSB) & 0x03) << 7) + info);
                        break;
                        //Reserved24, // 24
                        //Reserved25, // 25
                        //Overflow    // >= 26 overflow packet with frames
                }
            }
            return value;
        }

        public bool mbCsvWriteFrame()
        {
            bool bOk = false;

            try
            {
                if( null != _mCsvWriter)
                {
                    /*                   string s = "_yyyMMddHHmmss" + _cCsvTab + "iFrame" + _cCsvTab + "signal"
                                           + _cCsvTab + "sec" + "Pleth"
                                           + _cCsvTab + "Packet" + _cCsvTab + "seq" + _cCsvTab + "sync"
                                           + "Info" + _cCsvTab + "value";
                                           */
                    double t = _mTotalFramesCount * _cFrameUnitSec;
                    DateTime frameUTC = _mFirstFileUTC.AddSeconds(t);
                    DSpO2FrameSequence seq = _mFrameSeqNr <= _cNrFramesInPacket ? (DSpO2FrameSequence)_mFrameSeqNr : DSpO2FrameSequence.Overflow;
                    string info = seq.ToString();
                    Int32 value = mPacketInfoValue(seq);

                    string s = "\"_" + CProgram.sDateTimeToYMDHMS(frameUTC) + "\""
                    + _cCsvTab + _mFramesCount + _cCsvTab + mFrameSignalStrength()
                    + _cCsvTab + t.ToString("0.00") + _cCsvTab + _mFramePleth
                    + _cCsvTab + _mPacketCount + _cCsvTab + _mFrameSeqNr + _cCsvTab + (_mFrameSeqNr == 1 ? "1" : "0")
                    + _cCsvTab + info + _cCsvTab + (value >= 0 ? value.ToString() : "");
                    _mCsvWriter.WriteLine(s);
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("DvxSpO2 write csv", ex);
            }
            return bOk;
        }
        public bool mbCsvWritePleth(UInt32 AIndex, UInt32 APleth)
        {
            bool bOk = false;

            try
            {
                if (null != _mCsvWriter)
                {
                    /*                   string s = "_yyyMMddHHmmss" + _cCsvTab + "iFrame" + _cCsvTab + "signal"
                                           + _cCsvTab + "sec" + "Pleth"
                                           + _cCsvTab + "Packet" + _cCsvTab + "seq" + _cCsvTab + "sync"
                                           + "Info" + _cCsvTab + "value";
                                           */
                    double t =AIndex /* _mTotalFramesCount*/ * _cFrameUnitSec;
                    DateTime frameUTC = _mFirstFileUTC.AddSeconds(t);

                    string s = "\"_" + CProgram.sDateTimeToYMDHMS(frameUTC) + "\""
                    + _cCsvTab + /*_mFramesCount*/AIndex
                    + _cCsvTab + t.ToString("0.00") + _cCsvTab + APleth /*_mFramePleth*/;
                    _mCsvWriter.WriteLine(s);
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("DvxSpO2 write csv", ex);
            }
            return bOk;
        }

        public bool mbTryParseDvxName(string AFileName)
        {
            bool bDvx = false;
            int len = AFileName == null ? 0 : AFileName.Length;
            int pos = len == 0 ? -1 : AFileName.IndexOf('_');
            // 123456_yyyyMMddHHmmss*******
            if( len >= 15 && pos >= 0)
            {
                string deviceName = AFileName.Substring(0, pos);
                string s = AFileName.Substring(pos+1);
                len = s.Length;
                if( len > 14 )
                {
                    s = s.Substring(0, 14);

                    UInt32.TryParse(deviceName, out _mDeviceNr);
                    bDvx = CProgram.sbParseYMDHMS(s, out _mFileUTC);
                }
            }
            return bDvx;
        }
        public bool mbWriteMit16(CRecordMit ARecord)
        {
            bool bOk = false;

            if( null != ARecord && _mConvertSpO2Path != null && _mConvertSpO2Path.Length > 2
                && ARecord.mNrSamples > 0)
            {
                string name = Path.GetFileNameWithoutExtension(_mFirstFileName)
                    + "_" + CProgram.sDateTimeToYMDHMS(DateTime.Now)
                    + (_mFileCount <= 1 ? "" : "N" + _mFileCount);

                string fullName = Path.Combine(_mConvertSpO2Path, name + ".hea");

                bOk = ARecord.mbSaveMIT(fullName, 16, true);
            }

            return bOk;
        }
        public CRecordMit mReadOneFile( string AFullName )
        {
            CRecordMit rec = null;

            try
            {
                if(AFullName != null && AFullName.Length > 10)
                {
                    _mFileFullName = AFullName;
                    _mFileName = Path.GetFileName(AFullName);
                    _mFileSize = 0;
                    _mFileBytesAdded = 0;
                    _mFramesCount = 0;
                    _mFramesOkCount = 0;
                    _mFramesSignalLow = 0;
                    _mFramesSignalMedium = 0;
                    _mFramesSignalHigh = 0;
                    _mFileUTC = DateTime.MinValue;
 
                    mbTryParseDvxName(_mFileName);// sets deviceNr and FileUTC

                    byte[] data = File.ReadAllBytes(AFullName);
                    Int32 nData = data == null ? 0 : data.Length;

                    if (_mbDebugRead)
                    {
                        Int32 nFrames = nData / _cNrBytesInFrame;
                        double t = nFrames * _cFrameUnitSec;
                        string s = _mFileCount + "\t" + _mFileName + "\t" + nData + "bytes\t"
                            + nFrames + " frames \t " + t.ToString( "0.00") +  " sec";
                        _mDebugReadLog += s + "\r\n";
                    }

                    if (nData > 0)
                    {
                        _mFileSize = (UInt32)nData;

                        if (++_mFileCount == 1)
                        {
                            _mFirstFileName = _mFileName;
                            _mFirstFileUTC = _mFileUTC;

                            if(_mbWriteCsv || _mbWriteMit16)
                            {
                                _mConvertSpO2Path = Path.Combine(Path.GetDirectoryName(AFullName), "convertSpO2");

                                CDvtmsData.sbCreateDir(_mConvertSpO2Path);
                            }
                            if (_mbWriteCsv)
                            {                             
                                mbCsvOpenFile(_mConvertSpO2Path, _mFirstFileName);
                            }
                        }

                        rec = new CRecordMit(CDvtmsData.sGetDBaseConnection());

                        if (rec.mbCreateSignals(1) && rec.mbCreateAllSignalData(_mFileSize / 2 + 100))
                        {
                            CSignalData signal = rec.mSignals[0];
                            UInt32 n = 0;

                            rec.mBaseUTC = DateTime.SpecifyKind( _mFileUTC, DateTimeKind.Utc );
                            rec.mSampleFrequency = _cNrFramesPerSecond;
                            rec.mSampleUnitT = (float)_cFrameUnitSec;
                            rec.mAdcGain = 1000;
                            rec.mSampleUnitA = 1.0F / rec.mAdcGain;
                            rec.mSignalLabels = "Pleth";
                            rec.mSignalUnits = "*";

                            for (UInt32 i = 0; i < nData; ++i)
                            {
                                byte lsb = data[i];

                                if (++i < nData)
                                {
                                    byte msb = data[i];

                                    UInt32 pleth = (((UInt32)(msb)) << 8) | ((UInt32)(lsb));

                                    signal.mAddValue((Int32)pleth);
                                    mbCsvWritePleth(++n, pleth);
                                }

                            }
                            rec.mNrSamples = signal.mNrValues;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("DvxSpO2 read one file " + AFullName, ex);
                rec = null;
            }
            return rec == null || rec.mGetNrSamples() == 0 ? null : rec;
        }

        public CRecordMit mReadMultipleFiles(string[] AMergeFullNames, float AFillGapSec)
        {
            CRecordMit recSum = null;
            Int32 nFiles = AMergeFullNames == null ? 0 : AMergeFullNames.Length;
            double fillGapSec = AFillGapSec < 0.01 ? 999999.999 : AFillGapSec;

            try
            {
                List<CRecordMit> recList = new List<CRecordMit>();
                Int32 nRecords = 0;
                bool bMergeRaw = false;

                if (nFiles > 0 && recList != null)
                {
                    for (Int32 iFile = 0; iFile < nFiles; ++iFile)
                    {
                        string fullName = AMergeFullNames[iFile];

                        if (fullName != null && fullName.Length > 1)
                        {
                            CRecordMit rec = mReadOneFile(fullName);

                            if( rec != null)
                            {
                                DateTime baseUtc = rec.mBaseUTC;

                                if( baseUtc == DateTime.MinValue )
                                {
                                    recList.Add(rec);
                                    ++nRecords;
                                    bMergeRaw = true;   // no time in file => add all file data without check for time gaps
                                }
                                else
                                {
                                    // add by time
                                    int insertPos = 0;

                                    while(insertPos < nRecords)
                                    {
                                        if(baseUtc < recList[insertPos].mBaseUTC)
                                        {
                                            break; // sort by base time
                                        }
                                        ++insertPos;
                                    }
                                    if(insertPos < nRecords)
                                    {
                                        recList.Insert(insertPos, rec); 
                                    }
                                    else
                                    {
                                        recList.Add(rec);
                                    }
                                    ++nRecords;                                   
                                }
                            }
                        }
                    }
                    recSum = new CRecordMit(CDvtmsData.sGetDBaseConnection());
                    if( nRecords > 0 && recSum != null)
                    {
                        if( bMergeRaw)
                        {
                            UInt32 nValues = 0;

                            for (int i = 0; i < nRecords; ++i) nValues += recList[i].mNrSamples;

                            if( recSum.mbCreateSignals( 1 ) && recSum.mbCreateAllSignalData(nValues + 1000))
                            {
                                CSignalData signal = recSum.mSignals[0];
                                Int32 vMin = Int32.MaxValue;
                                Int32 vMax = Int32.MinValue;
                                Int32 v;

                                recSum.mBaseUTC = recList[0].mBaseUTC;
                                recSum.mSampleFrequency = recList[0].mSampleFrequency;
                                recSum.mSampleUnitT = recList[0].mSampleUnitT;
                                recSum.mAdcGain = recList[0].mAdcGain;
                                recSum.mSampleUnitA = 1.0F / recSum.mAdcGain;
                                recSum.mSignalLabels = "Pleth";
                                recSum.mSignalUnits = "*";

                                for ( Int32 iRec = 0; iRec < nRecords; ++iRec)
                                {
                                    Int32[] values = recList[iRec].mSignals[0].mValues;
                                    UInt32 n = recList[iRec].mSignals[0].mNrValues;

                                    for ( int i = 0; i < n; ++i)
                                    {
                                        v = values[i];
                                        signal.mAddValue(v);
                                        if (v < vMin) vMin = v;
                                        if (v > vMax) vMax = v;
                                    }
                                }
                                signal.mMinValue = vMin;
                                signal.mMaxValue = vMax;
                                recSum.mNrSamples = signal.mNrValues;
                            }
                        }
                        else
                        {
                            Int32 iLast = nRecords - 1;
                            UInt32 sampleRate = recList[0].mSampleFrequency;
                            double unitT = 1.0 / sampleRate;
                            DateTime startUTC = recList[0].mBaseUTC;
                            DateTime endUTC = recList[iLast].mBaseUTC.AddSeconds(recList[iLast].mGetSamplesTotalTime());
                            DateTime curUTC = startUTC;

                            UInt32 nValues = (UInt32)((endUTC-startUTC).TotalSeconds * sampleRate + 0.5);
       
                            if (recSum.mbCreateSignals(1) && recSum.mbCreateAllSignalData(nValues + 1000))
                            {
                                CSignalData signal = recSum.mSignals[0];

                                recSum.mBaseUTC = recList[0].mBaseUTC;
                                recSum.mSampleFrequency = recList[0].mSampleFrequency;
                                recSum.mSampleUnitT = recList[0].mSampleUnitT;
                                recSum.mAdcGain = recList[0].mAdcGain;
                                recSum.mSampleUnitA = 1.0F / recSum.mAdcGain;
                                recSum.mSignalLabels = "Pleth";
                                recSum.mSignalUnits = "*";
                                nValues = 0;
                                Int32 vMin = Int32.MaxValue;
                                Int32 vMax = Int32.MinValue;
                                Int32 v;

                                for (Int32 iRec = 0; iRec < nRecords; ++iRec)
                                {
                                    Int32[] values = recList[iRec].mSignals[0].mValues;
                                    UInt32 n = recList[iRec].mSignals[0].mNrValues;
                                    DateTime baseUTC = recList[iRec].mBaseUTC;
                                    Int32 iStart = 0;
                                    double gap = (baseUTC - curUTC).TotalSeconds;
                                    double allowedGapPos = fillGapSec * (1.001 + 0.5 * iRec);
                                    double allowedGapNeg = fillGapSec * (1.001 + 0.05 * iRec);

                                    if( gap >= allowedGapPos)
                                    {
                                        Int32 nGap = (Int32)(gap * sampleRate);
                                        v = 0;

                                        for (int i = 0; i < nGap; ++i)
                                        {                                           
                                            signal.mAddValue(v);    // fill gap
                                        }
                                        if (v < vMin) vMin = v;
                                        if (v > vMax) vMax = v;

                                    }
                                    else if( gap < -allowedGapNeg)
                                    {
                                        iStart = (Int32)(gap * sampleRate); // skip overlap
                                    }
                                    for (int i = iStart; i < n; ++i)
                                    {
                                        v = values[i];
                                        signal.mAddValue(v);
                                        if (v < vMin) vMin = v;
                                        if (v > vMax) vMax = v;
                                    }
                                    nValues = signal.mNrValues;
                                    curUTC = startUTC.AddSeconds(nValues * unitT);
                                }
                                signal.mMinValue = vMin;
                                signal.mMaxValue = vMax;
                                recSum.mNrSamples = nValues;
                            }
                        }
                    }
                    //                            recSum.mNrSamples = signal.mNrValues;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("DvxSpO2 read multiple files " + _mFileFullName, ex);
                recSum = null;
            }
            return recSum == null || recSum.mGetNrSamples() == 0 ? null : recSum;
        }
        public bool mbWriteSpo( string AToFullName, CRecordMit ARecord )
        {
            bool bOk = AToFullName != null && AToFullName.Length > 3 && ARecord != null && ARecord.mSignals != null;

            try
            {
                CSignalData signal = ARecord.mSignals[0];
                Int32[] values = signal == null ? null : signal.mValues;

                FileStream fw = new FileStream(AToFullName, FileMode.Create);

                if( fw != null && values != null)
                {
                    UInt32 n = signal.mNrValues;

                    for (UInt32 i = 0; i < n; ++i)
                    {
                        Int32 v = values[i];

                        fw.WriteByte((byte)(v & 0x0FF));            //LSB
                        fw.WriteByte((byte)((v >> 8 ) & 0x0FF));    //MSB
                    }
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("DvxSpO2 write file " + AToFullName, ex);

            }
            return bOk;
        }
        public static bool sbLoadPlethData(out string ArResultText, out CRecordMit ArRecord, string ADvxSpo2FilePath,
            Int16 ADefaultTimeZoneMin, DMoveTimeZone AMoveTimeZone)
        {
            bool bOk = false;
            CRecordMit rec = null;

            ArResultText = "";
            try
            {
                CDvxSpO2 dvxSpO2 = new CDvxSpO2(false, false);


                if (dvxSpO2 == null)
                {
                    ArResultText = "Failed create dvxSpO2";
                }
                else
                {
                    rec = dvxSpO2.mReadOneFile(ADvxSpo2FilePath);

                    if( rec == null )
                    {
                        ArResultText = "Failed load dvxSpO2";
                    }
                    else if(rec.mNrSamples == 0 )
                    {
                        ArResultText = "Empty dvxSpO2 file";

                    }
                    else
                    {
                        rec.mTimeZoneOffsetMin = ADefaultTimeZoneMin;

                        if (AMoveTimeZone == DMoveTimeZone.Move)
                        {
                            rec.mbMoveTimeZone((Int16)ADefaultTimeZoneMin);
                        }
                        bOk = true;
                    }
                }
            }
            catch (Exception ex)
            {
                CProgram.sLogException("DvxSpO2 read file " + ADvxSpo2FilePath, ex);

            }
            ArRecord = rec;

            return bOk;
        }

        /*
                public static bool sbSplitYMDH(out UInt16 ArYear, out UInt16 ArMonth, out UInt16 ArDay, out UInt16 ArHour, UInt32 AValueYMDH)
                {
                    bool bOk = false;
                    UInt16 year = 0;
                    UInt16 month = 0;
                    UInt16 day = 0;
                    UInt16 hour = 0;

                    try
                    {
                        UInt32 i = AValueYMDH;
                        hour = (UInt16)(i % 100);
                        i = i / 100;
                        day = (UInt16)(i % 100);
                        i = i / 100;
                        month = (UInt16)(i % 100);
                        year = (UInt16)(i / 100);

                        DateTime dt = new DateTime((int)year, (int)month, (int)day, (int)hour, 0, 0);   // check date is ok
                        bOk = true;
                    }
                    catch (Exception)
                    {

                    }
                    ArYear = year;
                    ArMonth = month;
                    ArDay = day;
                    ArHour = hour;
                    return bOk;
                }
                public string mParamFileInfoLoadNew(string ADevicePath, string ASerialNr, bool AbLoadNew)
                {
                    string info = "";
                    string fileName = mMakeParamFileName(ASerialNr);
                    string filePath = Path.Combine(ADevicePath, fileName);

                    try
                    {
                        FileInfo fi = new FileInfo(filePath);

                        if (false == fi.Exists)
                        {
                            info = "not present";
                        }
                        else
                        {
                            info = CProgram.sDateTimeToString(fi.LastWriteTime);

                    }
                    catch (Exception)
                    {
                        info = "error on find " + fileName;
                    }
                    return info;
                }

                public string mScanInfoLoadMissing(string ADevicePath, string ASerialNr, UInt32 AStudyIX, bool AbLoadMissing)
                {
                    string info = "";
                    string fileName = mMakeScanFileName(ASerialNr, AStudyIX);
                    string studyPath;

                    try
                    {
                    }
                    catch (Exception)
                    {
                        info += ", error on find " + fileName;
                    }
                    return info;
                }

                  public bool mbLoadParamFile(out bool ArbPresent, string ADevicePath, string ASerialNr)
                {
                    bool bOk = false;
                    bool bPresent = false;
                    string storeSnr = CDvtmsData.sGetTzStoreSnrName(ASerialNr);

                    string fileName = mMakeParamFileName(storeSnr);
                    string fromFile = Path.Combine(ADevicePath, fileName);


                    try
                    {
                        _mbParamFileLoaded = false;
                        FileInfo fi = new FileInfo(fromFile);

                        if (fi == null)
                        {
                            _mParamFileUTC = DateTime.MinValue;
                        }
                        else
                        {
                            _mParamFileUTC = fi.LastWriteTimeUtc;
                            bPresent = fi.Exists;

                            }
                        bOk = true;
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("Collect load " + fromFile, ex);
                    }
                    ArbPresent = bPresent;
                    return bOk;
                }
                public bool mbScanFilesStudyRecord(ref string ArResult, out FileInfo[] ArFileList, string ASerialNr, UInt32 AStudyIX)
                {
                    bool bOk = false;
                    FileInfo[] fileList = null;
                    string studyPath;

                    try
                    {
                        if (ArResult.Length > 0) ArResult += ", ";

                        if (AStudyIX == 0)
                        {
                            ArResult += "S0 no study";
                        }
                        else
                        {
                            ArResult = "S" + AStudyIX + ": ";

                            if (false == CDvtmsData.sbGetStudyRecorderDir(out studyPath, AStudyIX, true))
                            {
                                ArResult += "no recordings";
                            }
                            else
                            {
                                DirectoryInfo di = new DirectoryInfo(studyPath);

                                if (di != null)
                                {
                                    fileList = di.GetFiles();
                                    int n = fileList == null ? 0 : fileList.Length;

                                    ArResult += n.ToString() + " files";
                                    bOk = n > 0;
                                }
                                else
                                {
                                    ArResult += " failed scan files";
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        ArResult += ", error on scan files S" + AStudyIX.ToString();
                    }
                    ArFileList = fileList;
                    return bOk;
                }



                public bool mbAddAllFileInfos( FileInfo[] ArFileList, UInt32 AMinFileSize)
                {
                    bool bOk = true;

                    try
                    {
                        if (ArFileList != null )
                        {
                            string fileName;
                            UInt32 fileID;
                            bool bNew;

                            bOk = true;

                            foreach( FileInfo fi in ArFileList )
                            {
                                if (fi.Exists && fi.Length >= AMinFileSize)
                                {
                                    fileName = fi.Name;

                                    if (mbParseFileID(out fileID, fileName))
                                    {
                                    }

                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CProgram.sLogException("error on adding file info " + _mParamSerialNr + " S" + _mStudy_IX.ToString(), ex);
                        bOk = false;
                    }
                    return bOk;
                }

                public bool mbCalcCurrentRange(ref string ArResult )
                {
                    bool bOk = false;

         /*           _mRunStartFileID = 0;
                    _mRunEndFileID = 0;

                    _mFirstPresentFileID = UInt32.MaxValue;        // find range of present FileID
                    _mFirstPresentFileUTC = DateTime.MaxValue;
                    _mLastPresentFileID = UInt32.MinValue;
                    _mLastPresentFileUTC = DateTime.MinValue;

                    _mFirstNormalFileID = UInt32.MaxValue;         // find range FileID of normal receceived (not requested )
                    _mFirstNormalFileUTC = DateTime.MaxValue;
                    _mLastNormalFileID = UInt32.MinValue;
                    _mLastNormalFileUTC = DateTime.MinValue;

                    _mFirstRecvFileID = UInt32.MaxValue;           // find received first / last
                    _mFirstRecvFileUTC = DateTime.MaxValue;
                    _mLastRecvFileID = UInt32.MinValue;
                    _mLastRecvFileUTC = DateTime.MinValue;

                    _mScanCalcResult = new List<string>();

                    UInt32 nPresent = 0, nNormal = 0;

                    if (_mScanFileIdList != null)
                    {
                        foreach (CTzCollectItem item in _mScanFileIdList)
                        {
                            if (item._mbPresent)
                            {
                                ++nPresent;
                                if (item._mFileID < _mFirstPresentFileID) { _mFirstPresentFileID = item._mFileID; _mFirstPresentFileUTC = item._mFileUTC; }
                                if (item._mFileID > _mLastPresentFileID) { _mLastPresentFileID = item._mFileID; _mLastPresentFileUTC = item._mFileUTC; }

                                if (item._mTryRequestCounter == 0)
                                {
                                    ++nNormal;
                                    if (item._mFileID < _mFirstNormalFileID) { _mFirstNormalFileID = item._mFileID; _mFirstNormalFileUTC = item._mFileUTC; }
                                    if (item._mFileID > _mLastNormalFileID) { _mLastNormalFileID = item._mFileID; _mLastNormalFileUTC = item._mFileUTC; }
                                }
                                if (item._mFileUTC < _mFirstRecvFileUTC) { _mFirstRecvFileUTC = item._mFileUTC; _mFirstRecvFileID = item._mFileID; }
                                if (item._mFileUTC > _mLastRecvFileUTC) { _mLastRecvFileUTC = item._mFileUTC; _mLastRecvFileID = item._mFileID; }
                            }
                        }
                        if( ArResult.Length > 0  ) ArResult += ", ";
                            ArResult += "last=" + _mLastPresentFileID.ToString() + " " + CProgram.sDateTimeToString(CProgram.sDateTimeToLocal(_mLastPresentFileUTC));
                        if (_mScanCalcResult != null)
                        {
                            _mScanCalcResult.Add("scan loaded " + _mScanNrFilesPresent.ToString() + ", added " + _mScanNrFilesAdded.ToString()
                                + " files, present = " + nPresent.ToString() + ", nNormal=  " + nNormal.ToString());
                            _mScanCalcResult.Add("firstPresentFileID=" + _mFirstPresentFileID.ToString() + " " + CProgram.sDateTimeToString(_mFirstPresentFileUTC)
                                + "UTC , LastPresentFileID=" + _mLastPresentFileID.ToString()+ " " + CProgram.sDateTimeToString(_mLastPresentFileUTC) + " UTC" );
                            _mScanCalcResult.Add("FirstNormalFileID=" + _mFirstNormalFileID.ToString() + " " + CProgram.sDateTimeToString(_mFirstNormalFileUTC)
                                + " UTC, LastNormalFileID=" + _mLastNormalFileID.ToString() + " " + CProgram.sDateTimeToString(_mLastNormalFileUTC) + "UTC ");
                            _mScanCalcResult.Add("FirstRecvFileID=" + _mFirstRecvFileID.ToString() + " " + CProgram.sDateTimeToString(_mFirstNormalFileUTC)
                                + " UTC, LastRecvFileID=" + _mLastRecvFileID.ToString() + " _mLastRecvFileUTC" + CProgram.sDateTimeToString(_mLastRecvFileUTC) + "UTC ");
                            //_mScanCalcResult.Add("=" + .ToString() + ", =" + .ToString());
                        }
                        bOk = true;
                    }
          * /          return bOk;
                }
                */

    }
}
